/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstdint>

#include "library.h"


namespace TE
{

constexpr float Pi = 3.141592653589793238462643383279502884f;


int32_t TENGINE2_API RandInt(int32_t min, int32_t max);


/**
 * Donne la plus petite puissance de 2 sup�rieure ou �gale � un nombre.
 *
 * \param value Entier.
 * \return Nombre.
 ******************************/

inline uint32_t smallestPowerOfTwoGreaterOrEqual(uint32_t value)
{
    value -= 1;

    value |= value >> 1;
    value |= value >> 2;
    value |= value >> 4;
    value |= value >> 8;
    value |= value >> 16;

    return (value + 1);
}

} // Namespace TE
