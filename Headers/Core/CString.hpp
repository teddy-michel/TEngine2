/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <limits>
#include <list>
#include <sstream>
#include <cassert>

#include "Core/CChar.hpp"

#include "os.h"

#ifdef T_SYSTEM_WINDOWS
#  ifdef max
#    undef max
#  endif
#endif


namespace TE
{

/**
 * \class   CString
 * \ingroup Core
 * \brief   La classe CString représente une chaine de caractères.
 *
 * Les caractères sont encodés en UTF-16 avec la classe CChar. Les méthodes
 * disponibles sont similaires à celles de classe std::string de la librairie
 * standard du C++, avec en plus des méthodes pour gérer l'encodage.
 *
 * Les données peuvent être partagées entre plusieurs instances de CString. La
 * copie ne s'effectue que lorsqu'une instance a besoin de modifier le contenu
 * de la chaine (Copy on Write). Les accès concurrents ne sont pas gérés pour
 * le moment.
 ******************************/

class TENGINE2_API CString
{
public:

    typedef CChar * iterator;
    typedef const CChar * const_iterator;

    // Constructeurs et destructeur
    CString();
    inline CString(const CString& other);
    CString(const CChar * other, std::size_t n = std::numeric_limits<std::size_t>::max());
    CString(CChar c, std::size_t n = 1);
    CString(const char * other, int len = -1);
    ~CString();

    // Opérateurs d'affectation
    CString& operator=(CChar c);
    CString& operator=(const CString& str);
    inline CString& operator=(const char * ch);
    inline CString& operator=(char c);
    inline void swap(CString& other);

    // Accès à un caractère
    inline const CChar at(std::size_t i) const;
    inline const CChar operator[](std::size_t i) const;
    inline CChar& operator[](std::size_t i);
    inline CChar * getData();
    inline const CChar * getConstData() const;

    // Itérateurs
    inline iterator begin();
    inline const_iterator begin() const;
    inline const_iterator cbegin() const;
    inline iterator end();
    inline const_iterator end() const;
    inline const_iterator cend() const;

    // Taille de la chaine
    inline std::size_t getSize() const;
    inline std::size_t getCapacity() const;
    /*virtual*/ void clear();
    void reserve(std::size_t s);
    inline bool isEmpty() const;
    /*virtual*/ void resize(std::size_t n, CChar c);
    /*virtual*/ void resize(std::size_t n);
    void squeeze();

    // Transformation
    CString getTrimmed() const;
    CString getSimplified() const;
    CString toLowerCase() const;
    CString toUpperCase() const;
    CString toTitleCase() const;

    // Comparaison
    int compare(const CString& str, bool cs = false) const;

    // Découpage
    std::list<CString> split(const CChar& separator, bool keepEmpty = true)const ;
    std::list<CString> split(const CString& str, bool keepEmpty = true) const;
    CString subString(std::size_t from, std::size_t n = std::numeric_limits<std::size_t>::max()) const;

    // Recherche
    std::ptrdiff_t indexOf(const CString& str, std::ptrdiff_t from = 0, bool cs = false) const;
    std::ptrdiff_t indexOf(const CChar& ch, std::ptrdiff_t from = 0, bool cs = false) const;
    std::ptrdiff_t lastIndexOf(const CString& str, std::ptrdiff_t from = -1, bool cs = false) const;
    std::ptrdiff_t lastIndexOf(const CChar& ch, std::ptrdiff_t from = -1, bool cs = false) const;

    bool startsWith(const CString& str, bool cs = false) const;
    bool startsWith(const CChar& ch, bool cs = false) const;
    bool endsWith(const CString& str, bool cs = false) const;
    bool endsWith(const CChar& ch, bool cs = false) const;
    bool contains(const CString& str, bool cs = false) const;
    bool contains(const CChar& ch, bool cs = false) const;

    // Remplacement
    CString& replace(const CChar& from, const CChar& to);

    CString arg(const CString& a) const;
    inline CString arg(const CChar& a) const;
    inline CString arg(int8_t a, int base = 10) const;
    inline CString arg(uint8_t a, int base = 10) const;
    inline CString arg(int16_t a, int base = 10) const;
    inline CString arg(uint16_t a, int base = 10) const;
    inline CString arg(int32_t a, int base = 10) const;
    inline CString arg(uint32_t a, int base = 10) const;
    inline CString arg(int64_t a, int base = 10) const;
    inline CString arg(uint64_t a, int base = 10) const;
    inline CString arg(float a) const;
    inline CString arg(double a) const;

    // Conversion en nombre
    double toDouble(bool * ok = nullptr) const;
    float toFloat(bool * ok = nullptr) const;
    int8_t toInt8(bool * ok = nullptr, int base = 10) const;
    uint8_t toUnsignedInt8(bool * ok = nullptr, int base = 10) const;
    int16_t toInt16(bool * ok = nullptr, int base = 10) const;
    uint16_t toUnsignedInt16(bool * ok = nullptr, int base = 10) const;
    int32_t toInt32(bool * ok = nullptr, int base = 10) const;
    uint32_t toUnsignedInt32(bool * ok = nullptr, int base = 10) const;
    int64_t toInt64(bool * ok = nullptr, int base = 10) const;
    uint64_t toUnsignedInt64(bool * ok = nullptr, int base = 10) const;

    // Conversion vers un autre encodage
    inline const char * toCharArray() const;
    const char * toLatin1() const;
    const char * toLatin9() const;
    const char * toUTF8() const;

    // Opérateurs de comparaison
    inline bool operator!=(const CString& other) const;
    bool operator!=(const char * other) const;
    bool operator<(const CString& other) const;
    bool operator<(const char * other) const;
    inline bool operator<=(const CString& other) const;
    bool operator<=(const char * other) const;
    bool operator==(const CString& other) const;
    bool operator==(const char * other) const;
    inline bool operator>(const CString& other) const;
    bool operator>(const char * other) const;
    inline bool operator>=(const CString& other) const;
    bool operator>=(const char * other) const;

    // Opérateurs de modification
    CString& operator+=(const CString& other);
    inline CString& operator+=(const char * str);
    CString& operator+=(CChar ch);
    inline CString& operator+=(char ch);

    // Conversion depuis un autre encodage
    static CString fromLatin1(const char * str, int len = -1);
    static CString fromLatin9(const char * str, int len = -1);
    static CString fromUTF8(const char * str, int len = -1);

    // Conversion depuis un nombre
    static CString fromFloat(float number, unsigned int decimals = 2);
    static CString fromFloat(double number, unsigned int decimals = 2);
    static inline CString fromNumber(int8_t number, int base = 10);
    static inline CString fromNumber(uint8_t number, int base = 10);
    static inline CString fromNumber(int16_t number, int base = 10);
    static inline CString fromNumber(uint16_t number, int base = 10);
    static inline CString fromNumber(int32_t number, int base = 10);
    static inline CString fromNumber(uint32_t number, int base = 10);
    static inline CString fromNumber(int64_t number, int base = 10);
    static inline CString fromNumber(uint64_t number, int base = 10);
    static inline CString fromPointer(const void * ptr);


    /**
     * \enum    TEncoding
     * \ingroup Core
     * \brief   Liste des encodages disponibles pour convertir les chaines.
     */

    enum TEncoding
    {
        Latin1, ///< ISO 8859-1, ou Latin-1.
        Latin9, ///< ISO 8859-15, ou Latin-9.
        UTF8    ///< UTF-8.
    };

    static inline TEncoding getDefaultEncoding();
    static inline void setDefaultEncoding(TEncoding encoding);

private:

    /// Structure de stockage des données partagées.
    struct TData
    {
        CChar * m_start;      ///< Pointeur sur le début de la chaîne.
        CChar * m_end;        ///< Pointeur sur la fin de la chaîne.
        CChar * m_alloc;      ///< Pointeur sur la fin de la zone allouée.
        unsigned int m_count; ///< Compteur de références.
        char * m_str;         ///< Tableau de caractères pour les fonctions de conversion.
        char * m_str_alloc;   ///< Pointeur sur la fin de la zone allouée pour la tableau.

        inline ~TData()
        {
            delete[] m_start;
            delete[] m_str;
        }
    };

    static TData sharedEmpty;

    TData * m_data; ///< Pointeur sur la structure de données.

    CString(TData * data);
    int64_t convertToInt64(int base, bool * ok) const;
    uint64_t convertToUnsignedInt64(int base, bool * ok) const;
    void copyData();
    static void allocateArray(TData * data, std::size_t s);
    static TData * fromLatin1Helper(const char * str, int len);
    static TData * fromLatin9Helper(const char * str, int len);
    static TData * fromUTF8Helper(const char * str, int len);

    template<typename T>
    static inline CString ConvertNumber(const T& value, int base = 10);

    static TEncoding m_encoding; ///< Encodage par défaut pour la conversion (Latin-1 par défaut).
};


/*-----------------------------------*
 *   Fonctions liées à CString       *
 *-----------------------------------*/

// Opérateurs de comparaison
inline bool operator!=(const char * str1, const CString& str2);
inline bool operator< (const char * str1, const CString& str2);
inline bool operator<=(const char * str1, const CString& str2);
inline bool operator==(const char * str1, const CString& str2);
inline bool operator> (const char * str1, const CString& str2);
inline bool operator>=(const char * str1, const CString& str2);

// Opérateurs de modification
inline const CString operator+(const CString& str1, const CString& str2);
inline const CString operator+(const CString& str1, const char * str2);
inline const CString operator+(const char * str1, const CString& str2);
inline const CString operator+(const CString& str1, char str2);
inline const CString operator+(char str1, const CString& str2);
inline const CString operator+(const CString& str1, const CChar& str2);
inline const CString operator+(const CChar& str1, const CString& str2);

inline std::ostream& operator<<(std::ostream& flux, const CString& str);


/**
 * Construit une chaine à partir d'une autre chaine.
 * Aucune copie en mémoire n'est effectuée, les deux chaines partagent les
 * mêmes données en mémoire.
 *
 * \param other Chaine à copier.
 ******************************/

inline CString::CString(const CString& other) :
    m_data (other.m_data)
{
    assert(this != &other);

    ++(m_data->m_count);
}



/**
 * Opérateur d'affectation avec un tableau de caractères encodés en Latin-1.
 *
 * \param ch Tableau de caractère terminé par le caractère nul.
 * \return Référence sur la chaine.
 ******************************/

inline CString& CString::operator =(const char * ch)
{
    switch (m_encoding)
    {
        default:
        case Latin1: return (*this = fromLatin1(ch));
        case Latin9: return (*this = fromLatin9(ch));
        case UTF8:   return (*this = fromUTF8(ch));
    }
}


/**
 * Opérateur d'affectation avec un caractère encodé en Latin-1.
 *
 * \param c Caractère.
 * \return Référence sur la chaine.
 ******************************/

inline CString& CString::operator=(char c)
{
    return (*this = CChar(c));
}


/**
 * Intervertit les données de deux chaines.
 * Cette opération est instantannée.
 *
 * \param other Chaine avec laquelle permuter.
 ******************************/

inline void CString::swap(CString& other)
{
    TData * data = other.m_data;
    other.m_data = m_data;
    m_data = data;
}


/**
 * Permet d'accéder à un caractère de la chaine en vérifiant que le numéro demandé est correct.
 *
 * \param i Indice du caractère (à partir de 0).
 * \return Caractère.
 ******************************/

inline const CChar CString::at(std::size_t i) const
{
    if (m_data->m_start + i < m_data->m_end)
    {
        return m_data->m_start[i];
    }
    else
    {
        return CChar();
    }
}


/**
 * Opérateur pour accéder à un caractère de la chaine.
 * Aucune vérification n'est effectuée sur la valeur de i.
 *
 * \param i Indice du caractère (à partir de 0).
 * \return Caractère.
 ******************************/

inline const CChar CString::operator[](std::size_t i) const
{
    assert(m_data->m_start + i < m_data->m_end);

    return m_data->m_start[i];
}


/**
 * Opérateur pour accéder à un caractère de la chaine.
 * Aucune vérification n'est effectuée sur la valeur de i.
 *
 * \param i Indice du caractère (à partir de 0).
 * \return Caractère.
 ******************************/

inline CChar& CString::operator[](std::size_t i)
{
    assert(m_data->m_start + i < m_data->m_end);

    copyData();
    return m_data->m_start[i];
}


/**
 * Retourne un pointeur sur les caractères de la chaine.
 * Le dernier caractère est le caractère nul.
 *
 * \return Pointeur.
 ******************************/

inline CChar * CString::getData()
{
    copyData();
    return m_data->m_start;
}


/**
 * Retourne un pointeur constant sur les caractères de la chaine.
 * Le dernier caractère est le caractère nul.
 *
 * \return Pointeur constant.
 ******************************/

inline const CChar * CString::getConstData() const
{
    return m_data->m_start;
}


/**
 * Donne un itérateur sur le début de la chaine.
 * Les itérateurs s'utilisent de la même façon que ceux de la STL.
 *
 * \return Itérateur sur le début de la chaine.
 ******************************/

inline CString::iterator CString::begin()
{
    copyData();
    return m_data->m_start;
}


/**
 * Donne un itérateur constant sur le début de la chaine.
 * Les itérateurs s'utilisent de la même façon que ceux de la STL.
 *
 * \return Itérateur constant sur le début de la chaine.
 ******************************/

inline CString::const_iterator CString::begin() const
{
    return m_data->m_start;
}


/**
 * Donne un itérateur constant sur le début de la chaine.
 * Les itérateurs s'utilisent de la même façon que ceux de la STL.
 *
 * \return Itérateur constant sur le début de la chaine.
 ******************************/

inline CString::const_iterator CString::cbegin() const
{
    return m_data->m_start;
}


/**
 * Donne un itérateur sur la fin de la chaine.
 * Les itérateurs s'utilisent de la même façon que ceux de la STL.
 *
 * \return Itérateur sur la fin de la chaine.
 ******************************/

inline CString::iterator CString::end()
{
    copyData();
    return m_data->m_end;
}


/**
 * Donne un itérateur constant sur la fin de la chaine.
 * Les itérateurs s'utilisent de la même façon que ceux de la STL.
 *
 * \return Itérateur constant sur la fin de la chaine.
 ******************************/

inline CString::const_iterator CString::end() const
{
    return m_data->m_end;
}


/**
 * Donne un itérateur constant sur la fin de la chaine.
 * Les itérateurs s'utilisent de la même façon que ceux de la STL.
 *
 * \return Itérateur constant sur la fin de la chaine.
 ******************************/

inline CString::const_iterator CString::cend() const
{
    return m_data->m_end;
}


/**
 * Donne le nombre de caractère de la chaine.
 *
 * \return Nombre de caractères de la chaine.
 ******************************/

inline std::size_t CString::getSize() const
{
    return (m_data->m_end - m_data->m_start);
}


/**
 * Donne le nombre de caractère que la chaine peut contenir sans avoir à réallouer la mémoire.
 *
 * \return Nombre de caractères disponibles.
 ******************************/

inline std::size_t CString::getCapacity() const
{
    return (m_data->m_alloc - m_data->m_start);
}


/**
 * Indique si la chaine est vide.
 *
 * \return Booléen indiquant si la chaine est vide.
 ******************************/

inline bool CString::isEmpty() const
{
    return (m_data->m_end == m_data->m_start);
}



inline CString CString::arg(const CChar& a) const
{
    return arg(CString(a));
}


inline CString CString::arg(int8_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(uint8_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(int16_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(uint16_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(int32_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(uint32_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(int64_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(uint64_t a, int base) const
{
    return arg(CString::fromNumber(a, base));
}


inline CString CString::arg(float a) const
{
    return arg(CString::fromFloat(a));
}


inline CString CString::arg(double a) const
{
    return arg(CString::fromFloat(a));
}


/**
 * Retourne un tableau de caractères encodés avec l'encodage par défaut.
 *
 * \return Tableau de caractères.
 ******************************/

inline const char * CString::toCharArray() const
{
    switch (m_encoding)
    {
        default:
        case Latin1: return toLatin1();
        case Latin9: return toLatin9();
        case UTF8:   return toUTF8();
    }
}


/**
 * Opérateur de comparaison !=.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si les deux chaines sont différentes.
 ******************************/

inline bool CString::operator!=(const CString& other) const
{
    return (!operator==(other));
}


/**
 * Opérateur de comparaison <=.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est inférieure ou égale à \a other.
 ******************************/

inline bool CString::operator<=(const CString& other) const
{
    return (!operator>(other));
}


/**
 * Opérateur de comparaison >.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est supérieure à \a other.
 ******************************/

inline bool CString::operator>(const CString& other) const
{
    return (other < *this);
}


/**
 * Opérateur de comparaison >=.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est supérieure ou égale à \a other.
 ******************************/

inline bool CString::operator>=(const CString& other) const
{
    return (!operator<(other));
}


/**
 * Ajoute un tableau de caractères à la fin de la chaine.
 *
 * \param str Tableau de caractères, terminé par le caractère nul.
 * \return Référence sur la chaine créée.
 ******************************/

inline CString& CString::operator+=(const char * str)
{
    return operator+=(CString(str));
}


/**
 * Ajoute un caractère à la fin de la chaine
 *
 * \param ch Caractère à ajouter.
 * \return Référence sur la chaine créée.
 ******************************/

inline CString& CString::operator+=(char ch)
{
    return operator+=(CChar(ch));
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(int8_t number, int base)
{
    char buffer[8 + 1] = { 0 };

    if (base != 10 && number < 0)
    {
        _itoa_s(static_cast<uint8_t>(number), buffer, base);
    }
    else
    {
        _itoa_s(number, buffer, base);
    }

    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(uint8_t number, int base)
{
    char buffer[1 + 8 + 1] = { 0 };
    _itoa_s(number, buffer, base);
    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(int16_t number, int base)
{
    char buffer[16 + 1] = { 0 };

    if (base != 10 && number < 0)
    {
        _itoa_s(static_cast<uint16_t>(number), buffer, base);
    }
    else
    {
        _itoa_s(number, buffer, base);
    }

    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(uint16_t number, int base)
{
    char buffer[16 + 1] = { 0 };
    _itoa_s(number, buffer, base);
    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(int32_t number, int base)
{
    char buffer[32 + 1] = { 0 };
    _itoa_s(number, buffer, base);
    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(uint32_t number, int base)
{
    char buffer[32 + 1] = { 0 };
    _itoa_s(number, buffer, base);
    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(int64_t number, int base)
{
    char buffer[64 + 1] = { 0 };
    _i64toa_s(number, buffer, 64, base);
    return CString(buffer);
}


/**
 * Convertit un nombre en chaine de caractères.
 *
 * \param number Nombre à convertir.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromNumber(uint64_t number, int base)
{
    char buffer[64 + 1] = { 0 };
    _ui64toa_s(number, buffer, 64, base);
    return CString(buffer);
}


/**
 * Convertit un pointeur en chaine de caractères.
 *
 * \param ptr Pointeur à convertir.
 * \return Chaine de caractères.
 ******************************/

inline CString CString::fromPointer(const void * ptr)
{
    return ConvertNumber(ptr, 16);
}


/**
 * Donne l'encodage par défaut utilisé pour convertir un tableau de caractères en CString.
 *
 * \return Encodage par défaut.
 *
 * \sa CString::setDefaultEncoding
 ******************************/

inline CString::TEncoding CString::getDefaultEncoding()
{
    return m_encoding;
}


/**
 * Change l'encodage par défaut utilisé pour convertir un tableau de caractères en CString.
 *
 * \param encoding Encodage par défaut.
 *
 * \sa CString::getDefaultEncoding
 ******************************/

inline void CString::setDefaultEncoding(CString::TEncoding encoding)
{
    m_encoding = encoding;
}


/**
 * Opérateur de comparaison != avec un tableau de caractères.
 *
 * \relates CString
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Booléen indiquant si les deux chaines sont différentes.
 ******************************/

inline bool operator!=(const char * str1, const CString& str2)
{
    return str2.operator!=(str1);
}


/**
 * Opérateur de comparaison < avec un tableau de caractères.
 *
 * \relates CString
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Booléen indiquant si la chaine \a str1 est inférieure à \a str2.
 ******************************/

inline bool operator<(const char * str1, const CString& str2)
{
    return str2.operator>(str1);
}


/**
 * Opérateur de comparaison <= avec un tableau de caractères.
 *
 * \relates CString
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Booléen indiquant si la chaine \a str1 est inférieure ou égale à \a str2.
 ******************************/

inline bool operator<=(const char * str1, const CString& str2)
{
    return str2.operator>=(str1);
}


/**
 * Opérateur de comparaison == avec un tableau de caractères.
 *
 * \relates CString
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Booléen indiquant si les deux chaines sont identiques.
 ******************************/

inline bool operator==(const char * str1, const CString& str2)
{
    return str2.operator==(str1);
}


/**
 * Opérateur de comparaison > avec un tableau de caractères.
 *
 * \relates CString
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Booléen indiquant si la chaine \a str1 est supérieure à \a str2.
 ******************************/

inline bool operator>(const char * str1, const CString& str2)
{
    return str2.operator<(str1);
}


/**
 * Opérateur de comparaison >= avec un tableau de caractères.
 *
 * \relates CString
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Booléen indiquant si la chaine \a str1 est supérieure ou égale à \a str2.
 ******************************/

inline bool operator>=(const char * str1, const CString& str2)
{
    return str2.operator<=(str1);
}


/**
 * Concatène deux chaines en une seule.
 *
 * \param str1 Première chaine de caractères.
 * \param str2 Seconde chaine de caractères.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(const CString& str1, const CString& str2)
{
    CString str_cpy = str1;
    str_cpy += str2;
    return str_cpy;
}


/**
 * Concatène deux chaines de caractères en une seule.
 *
 * \relates CString
 *
 * \param str1 Chaine de caractères.
 * \param str2 Tableau de caractères.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(const CString& str1, const char * str2)
{
    return operator+(str1, CString(str2));
}


/**
 * Concatène deux chaines de caractères en une seule.
 *
 * \relates CString
 *
 * \param str1 Tableau de caractères.
 * \param str2 Chaine de caractères.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(const char * str1, const CString& str2)
{
    return operator+(CString(str1), str2);
}


/**
 * Concatène une chaine de caractère et un caractère.
 *
 * \relates CString
 *
 * \param str1 Chaine de caractères.
 * \param str2 Caractère.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(const CString& str1, char str2)
{
    return operator+(str1, CString(str2));
}


/**
 * Concatène une chaine de caractère et un caractère.
 *
 * \relates CString
 *
 * \param str1 Caractère.
 * \param str2 Chaine de caractères.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(char str1, const CString& str2)
{
    return operator+(CString(str1), str2);
}


/**
 * Concatène une chaine de caractère et un caractère.
 *
 * \relates CString
 *
 * \param str1 Chaine de caractères.
 * \param str2 Caractère.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(const CString& str1, const CChar& str2)
{
    return operator+(str1, CString(str2));
}


/**
 * Concatène une chaine de caractère et un caractère.
 *
 * \relates CString
 *
 * \param str1 Caractère.
 * \param str2 Chaine de caractères.
 * \return Chaine créée.
 ******************************/

inline const CString operator+(const CChar& str1, const CString& str2)
{
    return operator+(CString(str1), str2);
}


/**
 * Surcharge de l'opérateur << entre un flux et une chaine de caractères.
 * L'encodage par défaut est utilisé pour convertir la chaine.
 *
 * \relates CString
 *
 * \param flux Flux de sortie.
 * \param str  Chaine de caractères.
 * \return Référence sur le flux de sortie.
 ******************************/

inline std::ostream& operator<<(std::ostream& flux, const CString& str)
{
    switch (CString::getDefaultEncoding())
    {
        default:
        case CString::Latin1: flux << str.toLatin1(); break;
        case CString::Latin9: flux << str.toLatin9(); break;
        case CString::UTF8:   flux << str.toUTF8();   break;
    }

    return flux;
}


/**
 * Convertit n'importe quel type en chaîne de caractères.
 *
 * \todo Base
 *
 * \param value Objet à transformer.
 * \return Chaine de caractères.
 ******************************/

template<typename T>
inline CString CString::ConvertNumber(const T& value, int base)
{
    std::ostringstream oss;
    oss << value;
    return CString(oss.str().c_str());
}

} // Namespace TE
