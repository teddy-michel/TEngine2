/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <sstream>
#include <string>
#include <cstdarg>
#include <list>

#include "Core/CString.hpp"


namespace TE
{

/**
 * \class   ILogger
 * \ingroup Core
 * \brief   Classe abstraite destinée à crée un logger.
 *
 * Elle peut ensuite être dérivée pour créer un logger dans des fichiers, dans une page HTML,
 * dans une fenêtre, etc.
 ******************************/

class TENGINE2_API ILogger
{
public:

    /// Niveau prédéfinis.
    enum TLevel
    {
        Debug   = 1, ///< Message de debug.
        Info    = 2, ///< Message d'information.
        Warning = 3, ///< Message d'avertissement.
        Error   = 4, ///< Message d'erreur.
        Alert   = 5  ///< Message d'alerte.
    };


    // Constructeur et destructeur
    ILogger();
    virtual ~ILogger();

    // Méthodes publiques
    virtual void write(const CString& message, int level = Info) = 0;

    template<class T> inline ILogger& operator<<(const T& toLog);
    template<class T> inline ILogger& operator<<(const std::list<T>& toLog);
    static CString date();
    static CString time();
};


/**
 * Surcharge de l'opérateur << pour logger des messages.
 *
 * \param toLog Données à inscrire dans le log.
 * \return Référence sur le logger.
 ******************************/

template <class T>
inline ILogger& ILogger::operator<<(const T& toLog)
{
    std::ostringstream stream;
    stream << toLog;
    write(stream.str().c_str());

    return *this;
}


/**
 * Surcharge de l'opérateur << pour logger le contenu d'une liste.
 * Seuls les 20 premiers éléments sont affichés.
 *
 * \param toLog Données à inscrire dans le log.
 * \return Référence sur le logger.
 ******************************/

template <class T>
inline ILogger& ILogger::operator<<(const std::list<T>& toLog)
{
    std::size_t i = 0;
    std::ostringstream stream;

    for (typename std::list<T>::const_iterator it = toLog.begin(); it != toLog.end() && i < 20; ++it, ++i)
    {
        if (i > 0) stream << ", ";
        stream << *it;
    }

    write(stream.str().c_str());

    return *this;
}

} // Namespace TE
