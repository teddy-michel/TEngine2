/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <fstream>

#include "Core/ILogger.hpp"


namespace TE
{

/**
 * \class   CLoggerFile
 * \ingroup Core
 * \brief   Logger qui inscrit les messages dans un fichier.
 ******************************/

class TENGINE2_API CLoggerFile : public ILogger
{
public :

    // Constructeur et destructeur
    CLoggerFile(const CString& file = "output.log");
    virtual ~CLoggerFile();

    // Log un message
    virtual void write(const CString& message, int level = Info) override;

private :

    // Donnée membre
    std::ofstream m_file; ///< Fichier de sortie des logs.
};

} // Namespace TE
