/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <map>
#include <SFML/Window.hpp>

#include "Core/CString.hpp"
#include "Core/ILogger.hpp"
#include "Core/CSettings.hpp"
#include "Core/Events.hpp"
//#include "Core/IEventReceiver.hpp"


namespace TE
{

class CApplication;


/// Instance unique.
extern TENGINE2_API CApplication * gApplication;


/**
 * \class   CApplication
 * \ingroup Core
 * \brief   Gestion de l'application et de la fen�tre.
 *
 * Cette classe est la classe de base du moteur.
 *
 * Singleton accessible avec la variable globale gApplication.
 ******************************/

class TENGINE2_API CApplication
{
public:

    CApplication();
    CApplication(const CApplication&) = delete;
    CApplication(CApplication&&) = delete;
    CApplication& operator=(const CApplication&) = delete;
    CApplication& operator=(CApplication&&) = delete;
    virtual ~CApplication();
    
    virtual unsigned int getWidth() const;
    virtual unsigned int getHeight() const;

    void close();

    virtual bool init();
    virtual void mainLoop();
    virtual void frame();
    virtual void release();

    void log(const CString& message, ILogger::TLevel level = ILogger::Info);

    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CResizeEvent& event);

    bool getKeyState(TKey key) const;
    bool getButtonState(TMouseButton button) const;

protected:

    static TKey getKeyCode(const CString& keycode);
    static TKey getKeyCode(const sf::Keyboard::Key& keycode);
    
    bool m_running;
    CSettings m_settings;                   ///< Param�tres du jeu.
    std::unique_ptr<ILogger> m_logger;      ///< Logger.
    std::map<TKey, bool> m_keyStates;       ///< �tat des touches. TODO: move to IEventReceiver ?
    std::map<TMouseButton, bool> m_buttons; ///< �tat des boutons de la souris. TODO: move to IEventReceiver ?
    float m_fps;                            ///< Nombre de frames affich�es au cours de la derni�re seconde.
    float m_frameTime;                      ///< Nombre de frames affich�es au cours de la derni�re seconde.
};

} // Namespace TE
