/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstdint>
#include <iostream>

#include "library.h"


namespace TE
{

/**
 * \class   CChar
 * \ingroup Core
 * \brief   La classe CChar permet de stocker des caractères au format UTF-16.
 ******************************/

class TENGINE2_API CChar
{
public:

    /**
     * \enum    TCategory
     * \ingroup Core
     * \brief   This enum maps the Unicode character categories.
     ******************************/

#if __cplusplus < 201103L
    enum TCategory
#else
    enum TCategory : uint8_t
#endif
    {
        Mark_NonSpacing = 1, ///< Unicode class name Mn.
        Mark_SpacingCombining = 2, ///< Unicode class name Mc.
        Mark_Enclosing = 3, ///< Unicode class name Me.

        Number_DecimalDigit = 4, ///< Unicode class name Nd.
        Number_Letter = 5, ///< Unicode class name Nl.
        Number_Other = 6, ///< Unicode class name No.

        Separator_Space = 7, ///< Unicode class name Zs.
        Separator_Line = 8, ///< Unicode class name Zl.
        Separator_Paragraph = 9, ///< Unicode class name Zp.

        Other_Control = 10, ///< Unicode class name Cc.
        Other_Format = 11, ///< Unicode class name Cf.
        Other_Surrogate = 12, ///< Unicode class name Cs.
        Other_PrivateUse = 13, ///< Unicode class name Co.
        Other_NotAssigned = 14, ///< Unicode class name Cn.

        Letter_Uppercase = 15, ///< Unicode class name Lu.
        Letter_Lowercase = 16, ///< Unicode class name Ll.
        Letter_Titlecase = 17, ///< Unicode class name Lt.
        Letter_Modifier = 18, ///< Unicode class name Lm.
        Letter_Other = 19, ///< Unicode class name Lo.

        Punctuation_Connector = 20, ///< Unicode class name Pc.
        Punctuation_Dash = 21, ///< Unicode class name Pd.
        Punctuation_Open = 22, ///< Unicode class name Ps.
        Punctuation_Close = 23, ///< Unicode class name Pe.
        Punctuation_InitialQuote = 24, ///< Unicode class name Pi.
        Punctuation_FinalQuote = 25, ///< Unicode class name Pf.
        Punctuation_Other = 26, ///< Unicode class name Po.

        Symbol_Math = 27, ///< Unicode class name Sm.
        Symbol_Currency = 28, ///< Unicode class name Sc.
        Symbol_Modifier = 29, ///< Unicode class name Sk.
        Symbol_Other = 30, ///< Unicode class name So.

        NoCategory = 0  ///< Aucune catégorie.
    };


    // Constructeurs
    inline CChar();
    inline CChar(char ch);
    inline CChar(unsigned char ch);
    inline CChar(uint8_t cell, uint8_t row);
    inline CChar(uint16_t code);
    inline CChar(int16_t code);
    inline CChar(uint32_t code);
    inline CChar(int32_t code);

    TCategory getCategory() const;
    inline uint8_t getCell() const;
    inline uint8_t getRow() const;
    int getDigitValue() const;
    inline uint16_t& unicode();
    inline uint16_t unicode() const;

    inline bool isNull() const;
    inline bool isDigit() const;
    inline bool isNumber() const;
    inline bool isLetter() const;
    inline bool isLetterOrNumber() const;
    inline bool isSpace() const;
    inline bool isLower() const;
    inline bool isUpper() const;
    inline bool isMark() const;
    inline bool isPrint() const;
    inline bool isPunct() const;
    inline bool isSymbol() const;
    inline bool isTitleCase() const;

    CChar toLowerCase() const;
    CChar toUpperCase() const;
    CChar toTitleCase() const;

    inline char toLatin1() const;
    inline char toLatin9() const;

    inline static CChar fromLatin1(char c);
    inline static CChar fromLatin9(char c);

    inline bool operator!=(const CChar& c) const;
    inline bool operator<(const CChar& c) const;
    inline bool operator<=(const CChar& c) const;
    inline bool operator==(const CChar& c) const;
    inline bool operator>(const CChar& c) const;
    inline bool operator>=(const CChar& c) const;

protected:

    // Donnée protégées
    uint16_t m_code; ///< Code UTF-16 du caractère.
};


/*-----------------------------------*
 *   Fonctions liées à CChar         *
 *-----------------------------------*/

inline std::istream& operator>>(std::istream& flux, CChar& c);
inline std::ostream& operator<<(std::ostream& flux, const CChar& c);


/**
 * Crée un caractère nul.
 ******************************/

inline CChar::CChar() :
    m_code(0)
{ }


/**
 * Crée un caractère à partir d'un caractère Latin-1.
 *
 * \param ch Caractère Latin-1.
 ******************************/

inline CChar::CChar(char ch) :
    m_code(uint16_t(uint8_t(ch)))
{ }


/**
 * Crée un caractère à partir d'un caractère Latin-1.
 *
 * \param ch Caractère Latin-1.
 ******************************/

inline CChar::CChar(unsigned char ch) :
    m_code(uint16_t(ch))
{ }


/**
 * Crée un caractère à partir d'un numéro de ligne et de colonne.
 *
 * \param cell Numéro de colonne.
 * \param row  Numéro de ligne.
 ******************************/

inline CChar::CChar(uint8_t cell, uint8_t row) :
    m_code(uint16_t((row << 8) | cell))
{ }


/**
 * Crée un caractère à partir d'un caractère Unicode.
 *
 * \param code Caractère Unicode.
 ******************************/

inline CChar::CChar(uint16_t code) :
    m_code(code)
{ }


/**
 * Crée un caractère à partir d'un caractère Unicode.
 *
 * \param code Caractère Unicode.
 ******************************/

inline CChar::CChar(int16_t code) :
    m_code(uint16_t(code))
{ }


/**
 * Crée un caractère à partir d'un caractère Unicode.
 * Si la conversion est impossible, la caractère nul est crée.
 *
 * \param code Caractère Unicode.
 ******************************/

inline CChar::CChar(uint32_t code) :
    m_code(uint16_t(code & 0xFFFF))
{ }


/**
 * Crée un caractère à partir d'un caractère Unicode.
 * Si la conversion est impossible, la caractère nul est crée.
 *
 * \todo Implémentation.
 *
 * \param code Caractère Unicode.
 ******************************/

inline CChar::CChar(int32_t code) :
    m_code(uint16_t(code & 0xFFFF))
{ }


/**
 * Retourne la colonne du caractère.
 *
 * \return Colonne du caractère.
 ******************************/

inline uint8_t CChar::getCell() const
{
    return (m_code & 0x00FF);
}


/**
 * Retourne la ligne du caractère.
 *
 * \return Ligne du caractère.
 ******************************/

inline uint8_t CChar::getRow() const
{
    return ((m_code >> 8) & 0x00FF);
}


/**
 * Retourne une référence sur le code du caractère.
 *
 * \return Code du caractère.
 ******************************/

inline uint16_t& CChar::unicode()
{
    return m_code;
}


/**
 * Retourne le code du caractère.
 *
 * \return Code du caractère.
 ******************************/

inline uint16_t CChar::unicode() const
{
    return m_code;
}


/**
 * Teste si le caractère est nul (égal à 0).
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isNull() const
{
    return (m_code == 0);
}


/**
 * Teste si le caractère est un chiffre.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isDigit() const
{
    return (getCategory() == Number_DecimalDigit);
}


/**
 * Teste si le caractère est un nombre.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isNumber() const
{
    TCategory c = getCategory();

    return (c == Number_DecimalDigit ||
        c == Number_Letter ||
        c == Number_Other);
}


/**
 * Teste si le caractère est une lettre.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isLetter() const
{
    TCategory c = getCategory();

    return (c == Letter_Uppercase ||
            c == Letter_Lowercase ||
            c == Letter_Titlecase ||
            c == Letter_Modifier ||
            c == Letter_Other);
}


/**
 * Teste si le caractère est un chiffre ou une lettre.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isLetterOrNumber() const
{
    TCategory c = getCategory();

    return (c == Letter_Uppercase ||
            c == Letter_Lowercase ||
            c == Letter_Titlecase ||
            c == Letter_Modifier ||
            c == Letter_Other ||
            c == Number_DecimalDigit ||
            c == Number_Letter ||
            c == Number_Other);
}


/**
 * Teste si le caractère est une espace.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isSpace() const
{
    // On ajoute les sauts de lignes et les tabulations
    if (m_code >= 9 && m_code <= 13)
        return true;

    TCategory c = getCategory();

    return (c == Separator_Space ||
            c == Separator_Line ||
            c == Separator_Paragraph);
}


/**
 * Teste si le caractère est une lettre minuscule.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isLower() const
{
    return (getCategory() == Letter_Lowercase);
}


/**
 * Teste si le caractère est une lettre majuscule.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isUpper() const
{
    return (getCategory() == Letter_Uppercase);
}


/**
 * Teste si le caractère est un marque.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isMark() const
{
    TCategory c = getCategory();

    return (c == Mark_NonSpacing ||
        c == Mark_SpacingCombining ||
        c == Mark_Enclosing);
}


/**
 * Teste si le caractère est affichable.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isPrint() const
{
    TCategory c = getCategory();

    return (c != Other_Control && c != Other_NotAssigned);
}


/**
 * Teste si le caractère est un signe de ponctuation.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isPunct() const
{
    TCategory c = getCategory();

    return (c == Punctuation_Connector ||
            c == Punctuation_Dash ||
            c == Punctuation_Open ||
            c == Punctuation_Close ||
            c == Punctuation_InitialQuote ||
            c == Punctuation_FinalQuote ||
            c == Punctuation_Other);
}


/**
 * Teste si le caractère est un symbole.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isSymbol() const
{
    TCategory c = getCategory();

    return (c == Symbol_Math ||
            c == Symbol_Currency ||
            c == Symbol_Modifier ||
            c == Symbol_Other);
}


/**
 * Teste si le caractère est une lettre de titre.
 *
 * \return Booléen.
 ******************************/

inline bool CChar::isTitleCase() const
{
    return (getCategory() == Letter_Titlecase);
}


/**
 * Retourne le caractère correspondant en Latin-1 (ISO 8859-1).
 *
 * \return Caractère Latin-1, ou 0 si la conversion est impossible.
 ******************************/

inline char CChar::toLatin1() const
{
    if (m_code > 0x00FF)
    {
        return '\0';
    }
    else
    {
        return char(m_code);
    }
}


/**
 * Retourne le caractère correspondant en Latin-9 (ISO 8859-15).
 *
 * \return Caractère Latin-9, ou 0 si la conversion est impossible.
 ******************************/

inline char CChar::toLatin9() const
{
    switch (m_code)
    {
        // Caractères différents du Latin-1
        case 0x20AC: return char(0xA4);
        case 0x0160: return char(0xA6);
        case 0x0161: return char(0xA8);
        case 0x017D: return char(0xB4);
        case 0x017E: return char(0xB8);
        case 0x0152: return char(0xBC);
        case 0x0153: return char(0xBD);
        case 0x0178: return char(0xBE);

        // Ceux-là sont donc invalides
        case 0x00A4:
        case 0x00A6:
        case 0x00A8:
        case 0x00B4:
        case 0x00B8:
        case 0x00BC:
        case 0x00BD:
        case 0x00BE:
            return '\0';

        default:
            return toLatin1();
    }
}


/**
 * Crée un caractère à partir d'un caractère encodé en Latin-1.
 *
 * \param c Caractère Latin-1.
 * \return Caractère UTF-16.
 ******************************/

inline CChar CChar::fromLatin1(char c)
{
    return CChar(c);
}


/**
 * Crée un caractère à partir d'un caractère encodé en Latin-9.
 *
 * \param c Caractère Latin-9.
 * \return Caractère UTF-16.
 ******************************/

inline CChar CChar::fromLatin9(char c)
{
    switch (static_cast<unsigned char>(c))
    {
        // Caractères différents du Latin-1
        case 0xA4: return CChar(0x20AC);
        case 0xA6: return CChar(0x0160);
        case 0xA8: return CChar(0x0161);
        case 0xB4: return CChar(0x017D);
        case 0xB8: return CChar(0x017E);
        case 0xBC: return CChar(0x0152);
        case 0xBD: return CChar(0x0153);
        case 0xBE: return CChar(0x0178);

        default:
            return CChar(c);
    }
}


/**
 * Opérateur de comparaison !=.
 *
 * \param c Caractère à comparer.
 * \return Booléen indiquant si les caractères sont différents.
 ******************************/

inline bool CChar::operator!=(const CChar& c) const
{
    return (m_code != c.m_code);
}


/**
 * Opérateur de comparaison <.
 *
 * \param c Caractère à comparer.
 * \return Booléen indiquant si le caractère est inférieur à \a c.
 ******************************/

inline bool CChar::operator<(const CChar& c) const
{
    return (m_code < c.m_code);
}


/**
 * Opérateur de comparaison <=.
 *
 * \param c Caractère à comparer.
 * \return Booléen indiquant si le caractère est inférieur ou égal à \a c.
 ******************************/

inline bool CChar::operator<=(const CChar& c) const
{
    return (m_code <= c.m_code);
}


/**
 * Opérateur de comparaison ==.
 *
 * \param c Caractère à comparer.
 * \return Booléen indiquant si les deux caractères sont identiques.
 ******************************/

inline bool CChar::operator==(const CChar& c) const
{
    return (m_code == c.m_code);
}


/**
 * Opérateur de comparaison >.
 *
 * \param c Caractère à comparer.
 * \return Booléen indiquant si le caractère est supérieur à \a c.
 ******************************/

inline bool CChar::operator>(const CChar& c) const
{
    return (m_code > c.m_code);
}


/**
 * Opérateur de comparaison >=.
 *
 * \param c Caractère à comparer.
 * \return Booléen indiquant si le caractère est supérieur ou égal à \a c.
 ******************************/

inline bool CChar::operator >=(const CChar& c) const
{
    return (m_code >= c.m_code);
}


/**
 * Surcharge de l'opérateur >> entre un flux et un caractère.
 *
 * \relates CChar
 *
 * \param flux Flux d'entrée.
 * \param c    Caractère.
 * \return Référence sur le flux d'entrée.
 ******************************/

inline std::istream& operator>>(std::istream& flux, CChar& c)
{
    uint16_t u;
    flux >> u;
    c.unicode() = u;
    return flux;
}


/**
 * Surcharge de l'opérateur << entre un flux et un caractère.
 *
 * \relates CChar
 *
 * \param flux Flux de sortie.
 * \param c    Caractère.
 * \return Référence sur le flux de sortie.
 ******************************/

inline std::ostream& operator<<(std::ostream& flux, const CChar& c)
{
    flux << c.unicode();
    return flux;
}

} // Namespace TE
