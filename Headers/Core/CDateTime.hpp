/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Core/CString.hpp"


namespace TE
{

/**
 * \struct  TDuration
 * \ingroup Core
 * \brief   Structure représentant une durée.
 *
 * Le nombre de jours et le nombre de secondes sont normalement du même signe.
 ******************************/

struct TENGINE2_API TDuration
{
    int days;    ///< Nombre de jours.
    int seconds; ///< Nombre de secondes.
};


/**
 * \class   CDateTime
 * \ingroup Core
 * \brief   Méthodes pour gérer la date et l'heure.
 ******************************/

class TENGINE2_API CDateTime
{
public:

    // Constructeur
    explicit CDateTime(int year = 2012, unsigned int month = 1, unsigned int day = 1, unsigned int hour = 0, unsigned int minute = 0, unsigned int second = 0, unsigned int microseconds = 0);

    // Accesseurs
    unsigned int getSecond() const;
    unsigned int getMinute() const;
    unsigned int getHour() const;
    unsigned int getDay() const;
    unsigned int getMonth() const;
    int getYear() const;
    bool isValid() const;
    bool isLeapYear() const;

    // Mutateurs
    bool setTime(unsigned int hour, unsigned int minute, unsigned int second);
    bool setDate(int year, unsigned int month, unsigned int day);
    void fromJulianDay(float day);

    // Méthodes publiques
    bool isNull() const;
    CString toString(const CString& format) const;
    void addSeconds(int add);
    void addMinutes(int add);
    void addHours(int add);
    void addDays(int add);
    void addMonths(int add);
    void addYears(int add);
    float toJulianDay() const;

    // Méthodes statiques
    static CDateTime getCurrentDateTime();
    static CString getCurrentDate();
    static CString getCurrentTime(bool milliseconds = false);
    static bool isValid(int year, unsigned int month, unsigned int day, unsigned int hour = 0, unsigned int minute = 0, unsigned int second = 0, unsigned int milliseconds = 0);
    static CString longDayName(unsigned int weekday);
    static CString longMonthName(unsigned int month);
    static CString shortDayName(unsigned int weekday);
    static CString shortMonthName(unsigned int month);
    static bool isLeapYear(int year);
    static unsigned int daysInMonth(unsigned int month, int year);
    static unsigned int dayOfYear(int year, unsigned int month, unsigned int day);
    static unsigned int dayOfWeek(int year, unsigned int month, unsigned int day);
    static unsigned int weekOfYear(int year, unsigned int month, unsigned int day);
    static unsigned int timestamp(int year, unsigned int month, unsigned int day, unsigned int hour = 0, unsigned int minute = 0, unsigned int second = 0);

    // Opérateurs
    TDuration operator-(const CDateTime& dateTime) const;
    CDateTime operator-(const TDuration& duration) const;
    CDateTime operator+(const TDuration& duration) const;
    bool operator==(const CDateTime& dateTime) const;
    bool operator!=(const CDateTime& dateTime) const;
    bool operator<(const CDateTime& dateTime) const;
    bool operator>(const CDateTime& dateTime) const;
    bool operator<=(const CDateTime& dateTime) const;
    bool operator>=(const CDateTime& dateTime) const;
    CDateTime& operator=(const CDateTime& dateTime);
    operator bool() const;

protected:

    // Données protégées
    unsigned int m_microseconds; ///< Micro-secondes.
    unsigned int m_second;       ///< Secondes.
    unsigned int m_minute;       ///< Minutes.
    unsigned int m_hour;         ///< Heures.
    unsigned int m_day;          ///< Jours.
    unsigned int m_month;        ///< Mois.
    int m_year;                  ///< Années.
};

} // Namespace TE
