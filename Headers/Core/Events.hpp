/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstdint>

#include "library.h"
#include "Core/CFlags.hpp"


namespace TE
{

/**
 * \enum    TEventType
 * \ingroup Core
 * \brief   Types d'évènements.
 ******************************/

enum TEventType
{
    DefaultEvent = 0, ///< Évènement par défaut.
    EventQuit,        ///< Fermeture de l'application.
    EventResize,      ///< Redimensionnement de la fenêtre.
    EventText,        ///< Texte issu du clavier.
    ButtonPressed,    ///< Enfoncement d'un bouton de la souris.
    ButtonReleased,   ///< Relachament d'un bouton de la souris.
    MouseWheel,       ///< Mouvement de la molette.
    DblClick,         ///< Double-clic avec un bouton de la souris.
    MoveMouse,        ///< Déplacement du curseur de la souris.
    KeyPressed,       ///< Enfoncement d'une touche du clavier.
    KeyReleased       ///< Relachement d'une touche du clavier.
};


/**
 * \enum    TMouseButton
 * \ingroup Core
 * \brief   Liste des boutons de la souris.
 ******************************/

enum TMouseButton
{
    MouseNoButton = 0, ///< Aucun bouton.
    MouseButtonLeft,   ///< Bouton gauche.
    MouseButtonRight,  ///< Bouton droit.
    MouseButtonMiddle, ///< Bouton central.
    MouseButtonX1,     ///< Bouton X1 (sur certaines souris).
    MouseButtonX2,     ///< Bouton X2 (sur certaines souris).
    MouseWheelUp,      ///< Coup de molette vers le haut.
    MouseWheelDown     ///< Coup de molette vers le bas.
};


/**
 * \struct  CMouseEvent
 * \ingroup Core
 * \brief   Description d'un évènement de la souris.
 ******************************/

struct TENGINE2_API CMouseEvent
{
    TEventType type;     ///< Type d'évènement (ButtonPressed, ButtonReleased, DblClick, MoveMouse).
    int x;               ///< Position horizontale du curseur.
    int y;               ///< Position verticale du curseur.
    int xrel;            ///< Déplacement horizontal du curseur par rapport à la dernière position.
    int yrel;            ///< Déplacement vertical du curseur par rapport à la dernière position.
    TMouseButton button; ///< Bouton à l'origine de l'évènement.
};


/**
 * \enum    TKey
 * \ingroup Core
 * \brief   Liste des touches du clavier.
 * \todo    Compléter avec toutes les codes ISO 8859-15.
 ******************************/

enum class TENGINE2_API TKey
{
    Key_Unknown  = 0,

    // Chiffres
    Key_0        = '0',
    Key_1        = '1',
    Key_2        = '2',
    Key_3        = '3',
    Key_4        = '4',
    Key_5        = '5',
    Key_6        = '6',
    Key_7        = '7',
    Key_8        = '8',
    Key_9        = '9',

    // Chiffres (pavé numérique)
    Key_Num0,
    Key_Num1,
    Key_Num2,
    Key_Num3,
    Key_Num4,
    Key_Num5,
    Key_Num6,
    Key_Num7,
    Key_Num8,
    Key_Num9,

    // Lettres
    Key_A        = 'a',
    Key_B        = 'b',
    Key_C        = 'c',
    Key_D        = 'd',
    Key_E        = 'e',
    Key_F        = 'f',
    Key_G        = 'g',
    Key_H        = 'h',
    Key_I        = 'i',
    Key_J        = 'j',
    Key_K        = 'k',
    Key_L        = 'l',
    Key_M        = 'm',
    Key_N        = 'n',
    Key_O        = 'o',
    Key_P        = 'p',
    Key_Q        = 'q',
    Key_R        = 'r',
    Key_S        = 's',
    Key_T        = 't',
    Key_U        = 'u',
    Key_V        = 'v',
    Key_W        = 'w',
    Key_X        = 'x',
    Key_Y        = 'y',
    Key_Z        = 'z',

    // Autres caractères ASCII
    Key_Space,
    Key_Exclam,
    Key_Paragraph,  // Key_Exclam
    Key_BracketRight,
    Key_ParenRight, // Key_BracketRight
    Key_Degree,     // Key_BracketRight
    Key_Comma,
    Key_Question,   // Key_Comma
    Key_Colon,
    Key_Slash,      // Key_Colon
    Key_Semicolon,
    Key_Period,     // Key_Semicolon
    Key_Less,
    Key_Greater,    // Key_Less
    Key_Equal,
    Key_Plus,       // Key_Equal
    Key_BraceRight, // Key_Equal
    Key_Euro,       // Key_E
    Key_TwoSuperior,
    Key_At,         // Key_0
    Key_Ampersand,  // Key_1
    Key_Tilde,      // Key_2
    Key_QuoteDbl,   // Key_3
    Key_Apostrophe, // Key_4
    Key_BraceLeft,  // Key_4
    Key_ParenLeft,  // Key_5
    Key_BracketLeft,// Key_5
    Key_Minus,      // Key_6
    Key_Bar,        // Key_6
    Key_Quote,      // Key_7
    Key_Underscore, // Key_8
    Key_Backslash,  // Key_8
    Key_Dollar,
    Key_Sterling,   // Key_Dollar
    Key_Circum,
    Key_Asterisk,
    Key_Mu,         // Key_Asterisk
    Key_Percent,
    Key_Add,
    Key_Subtract,
    Key_Divide,
    Key_Multiply,

    // Touches spéciales
    Key_LAlt,
    Key_RAlt,
    Key_LControl,
    Key_RControl,
    Key_LShift,
    Key_RShift,
    Key_LMeta,
    Key_RMeta,
    Key_Menu,

    Key_Print,
    Key_Pause,
    Key_Tab,
    Key_Backspace,
    Key_Enter,
    Key_NumEnter,
    Key_Insert,
    Key_Delete,
    Key_Home,
    Key_End,
    Key_PageUp,
    Key_PageDown,
    Key_Left,
    Key_Up,
    Key_Right,
    Key_Down,
    Key_CapsLock,
    Key_NumLock,
    Key_ScrollLock,
    Key_Function,
    Key_Escape,

    // Fonctions
    Key_F1,
    Key_F2,
    Key_F3,
    Key_F4,
    Key_F5,
    Key_F6,
    Key_F7,
    Key_F8,
    Key_F9,
    Key_F10,
    Key_F11,
    Key_F12,
    Key_F13,
    Key_F14,
    Key_F15,

	// Boutons de la souris
	// TODO: définir ailleurs ?
	Mouse_Left,
	Mouse_Right,
	Mouse_Middle
};

TENGINE2_API const char * GetKeyName(TKey key);


/**
 * \enum    TKeyModifier
 * \ingroup Core
 * \brief   Modificateurs associés aux touches du clavier.
 ******************************/

enum TKeyModifier
{
    NoModifier      = 0x00, ///< Aucun modificateur.
    ShiftModifier   = 0x01, ///< A Shift key on the keyboard is pressed.
    ControlModifier = 0x02, ///< A Ctrl key on the keyboard is pressed.
    AltModifier     = 0x04, ///< An Alt key on the keyboard is pressed.
    MetaModifier    = 0x08, ///< A Meta key on the keyboard is pressed.
    NumModifier     = 0x10, ///< Le pavé numérique est activé.
    CapsModifier    = 0x20  ///< Les majuscules sont activées.
};

T_DECLARE_FLAGS(TKeyModifiers, TKeyModifier)

T_DECLARE_OPERATORS_FOR_FLAGS(TKeyModifiers)


/**
 * \struct  CKeyboardEvent
 * \ingroup Core
 * \brief   Description d'un évènement du clavier.
 * \todo    Changer la gestion des caractères Unicode.
 ******************************/

struct TENGINE2_API CKeyboardEvent
{
    TEventType type;        ///< Type d'évènement (KeyPressed ou KeyReleased).
    TKey code;              ///< Code de la touche.
    int modif;              ///< Modificateurs.
};


/**
 * \struct  CTextEvent
 * \ingroup Core
 * \brief   Texte rentré au clavier.
 ******************************/

struct TENGINE2_API CTextEvent
{
    TEventType type;        ///< Type d'évènement (toujours EventText).
    uint32_t unicode;       ///< Caractère au format Unicode.
};


/**
 * \struct  CResizeEvent
 * \ingroup Core
 * \brief   Évènement correspondant au redimensionnement de la fenêtre.
 * \todo    Ajouter l'ancienne taille de la fenêtre ?
 ******************************/

struct TENGINE2_API CResizeEvent
{
    TEventType type;        ///< Type d'évènement (toujours EventResize).
    unsigned int width;     ///< Largeur de la fenêtre.
    unsigned int height;    ///< Hauteur de la fenêtre.
};


/**
 * \union   CEvent
 * \ingroup Core
 * \brief   Structure utilisée pour gérer les évènements.
 ******************************/

union TENGINE2_API CEvent
{
    TEventType type;        ///< Type d'évènement.
    CResizeEvent resize;    ///< Redimensionnement.
    CKeyboardEvent key;     ///< Clavier.
    CTextEvent text;        ///< Texte.
    CMouseEvent mouse;      ///< Souris.
};

} // Namespace TE
