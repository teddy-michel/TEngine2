/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <list>
#include <map>

#include "Core/CString.hpp"


namespace TE
{

/**
 * \class   CSettings
 * \ingroup Core
 * \brief   Classe permettant de gérer les paramètres de configuration.
 * \todo    Pouvoir lire les paramètres depuis la base de registre de Windows.
 *
 * Les paramètres sont issus de fichiers de configuration au format INI.
 ******************************/

class TENGINE2_API CSettings
{
public:

    // Constructeur et destructeur
    CSettings();
    ~CSettings();

    // Accesseur
    inline CString getFileName() const;
    bool groupExists(const CString& group) const;
    bool keyExists(const CString& group, const CString& key) const;
    CString getValue(const CString& group, const CString& key, const CString& defval = CString()) const;
    bool getBooleanValue(const CString& group, const CString& key, const bool defval = true) const;

    // Méthodes publiques
    bool loadFromFile(const CString& fileName);
    bool saveToFile(const CString& fileName) const;
    void clear();
    void addGroup(const CString& group);
    void setValue(const CString& group, const CString& key, const CString& value);

private:

    /// Représente un groupe dans le fichier.
    struct TSettingGroup
    {
        CString name;                    ///< Nom du groupe.
        std::map<CString, CString> data; ///< Tableau associatif des clés et valeurs.

        /// Constructeur avec le nom du groupe.
        inline TSettingGroup(const CString& gname) : name(gname) { }
    };

protected:

    // Données privées
    CString m_fileName;                  ///< Adresse du fichier.
    std::list<TSettingGroup *> m_groups; ///< Liste des groupes.
};


/**
 * Donne le nom du fichier de configuration.
 *
 * \return Adresse du fichier.
 ******************************/

inline CString CSettings::getFileName() const
{
    return m_fileName;
}

} // Namespace TE
