/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

/*
Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
All rights reserved.
Contact: Nokia Corporation (qt-info@nokia.com)

This file is part of the QtCore module of the Qt Toolkit.

GNU Lesser General Public License Usage
This file may be used under the terms of the GNU Lesser General Public
License version 2.1 as published by the Free Software Foundation and
appearing in the file LICENSE.LGPL included in the packaging of this
file. Please review the following information to ensure the GNU Lesser
General Public License version 2.1 requirements will be met:
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.

In addition, as a special exception, Nokia gives you certain additional
rights. These rights are described in the Nokia Qt LGPL Exception
version 1.1, included in the file LGPL_EXCEPTION.txt in this package.

GNU General Public License Usage
Alternatively, this file may be used under the terms of the GNU General
Public License version 3.0 as published by the Free Software Foundation
and appearing in the file LICENSE.GPL included in the packaging of this
file. Please review the following information to ensure the GNU General
Public License version 3.0 requirements will be met:
http://www.gnu.org/copyleft/gpl.html.

Other Usage
Alternatively, this file may be used in accordance with the terms and
conditions contained in a signed written agreement between you and Nokia.
*/

#pragma once


namespace TE
{

class CFlag
{
private:

    int flag;

public:

    inline CFlag(int f) : flag (f) { };
    inline operator int() const { return flag; }
};


class CIncompatibleFlag
{
private:

    int flag;

public:

    inline explicit CIncompatibleFlag(int f) : flag (f) { };
    inline operator int() const { return flag; }
};


/**
 * \class   CFlags
 * \ingroup Core
 * \brief   Permet de manipuler des flags de manière sécurisée.
 *
 * \tparam Enum Énumération contenant la liste des flags.
 *
 * Les flags sont généralement stockés dans des entiers, ou chaque bit représente un flag
 * différent. L'inconvénient est que ces variables ne sont pas typées, et on peut donc
 * combiner des flags sans aucun rapport les uns avec les autres.
 *
 * Les valeurs des flags doivent être définies dans une énumération (par exemple Enum).
 * Il suffit ensuite de spécialiser cette classe, en appelant la macro \c T_DECLARE_FLAGS.
 ******************************/

template<typename Enum>
class CFlags
{
private:

    typedef void ** Zero;
    int i;

public:

    typedef Enum enum_type;

    inline CFlags(const CFlags& f) : i(f.i) { }
    inline CFlags(Enum f) : i(f) { }
    inline CFlags(Zero = 0) : i(0) { }
    inline CFlags(CFlag f) : i(f) { }

    inline CFlags &operator=(const CFlags &f) { i = f.i; return *this; }
    inline CFlags &operator&=(int mask) { i &= mask; return *this; }
    inline CFlags &operator&=(unsigned int mask) { i &= mask; return *this; }
    inline CFlags &operator|=(CFlags f) { i |= f.i; return *this; }
    inline CFlags &operator|=(Enum f) { i |= f; return *this; }
    inline CFlags &operator^=(CFlags f) { i ^= f.i; return *this; }
    inline CFlags &operator^=(Enum f) { i ^= f; return *this; }
    inline operator int() const { return i; }
    inline CFlags operator|(CFlags f) const { CFlags g; g.i = i | f.i; return g; }
    inline CFlags operator|(Enum f) const { CFlags g; g.i = i | f; return g; }
    inline CFlags operator^(CFlags f) const { CFlags g; g.i = i ^ f.i; return g; }
    inline CFlags operator^(Enum f) const { CFlags g; g.i = i ^ f; return g; }
    inline CFlags operator&(int mask) const { CFlags g; g.i = i & mask; return g; }
    inline CFlags operator&(unsigned int mask) const { CFlags g; g.i = i & mask; return g; }
    inline CFlags operator&(Enum f) const { CFlags g; g.i = i & f; return g; }
    inline CFlags operator~() const { CFlags g; g.i = ~i; return g; }
    inline bool operator!() const { return !i; }

    inline bool testFlag(Enum f) const { return (i & f) == f && (f != 0 || i == int(f) ); }
};

} // Namespace TE


#define T_DECLARE_FLAGS(Flags, Enum) \
    typedef TE::CFlags<Enum> Flags;


#define T_DECLARE_INCOMPATIBLE_FLAGS(Flags) \
    inline TE::CIncompatibleFlag operator|(Flags::enum_type f1, int f2) \
        { return TE::CIncompatibleFlag(int(f1) | f2); }


#define T_DECLARE_OPERATORS_FOR_PRIVATE_FLAGS(Flags) \
    friend TE::CFlags<Flags::enum_type> operator|(Flags::enum_type f1, Flags::enum_type f2); \
    friend TE::CFlags<Flags::enum_type> operator|(Flags::enum_type f1, TE::CFlags<Flags::enum_type> f2); \
    friend TE::CIncompatibleFlag operator|(Flags::enum_type f1, int f2);


#define T_DECLARE_OPERATORS_FOR_FLAGS(Flags) \
    inline TE::CFlags<Flags::enum_type> operator|(Flags::enum_type f1, Flags::enum_type f2) \
        { return TE::CFlags<Flags::enum_type>(f1) | f2; } \
    inline TE::CFlags<Flags::enum_type> operator|(Flags::enum_type f1, TE::CFlags<Flags::enum_type> f2) \
        { return f2 | f1; } \
    T_DECLARE_INCOMPATIBLE_FLAGS(Flags)
