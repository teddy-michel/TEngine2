/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <zip/zip.h>
#include <vector>

#include "Core/CString.hpp"


namespace TE
{

/**
 * \class   CZipFile
 * \ingroup Core
 * \brief   Gestion d'une archive compressée au format ZIP.
 ******************************/

class TENGINE2_API CZipFile
{
public:

    // Constructeur et destructeur
    explicit CZipFile(const CString& fileName);
    ~CZipFile();

    bool isOpen() const noexcept;
    unsigned int getFilesCount() const noexcept;
    unsigned int getFileIndex(const CString& name) const noexcept;
    const char * getFileName(unsigned int index) const noexcept;
    bool getFileContent(const CString& name, std::vector<char>& buffer);

private:

    zip_t * m_zip;
};

} // Namespace TE
