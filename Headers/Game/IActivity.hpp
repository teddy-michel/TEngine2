/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//#include "Core/IEventReceiver.hpp"
#include "Game/CGameApplication.hpp"


namespace TE
{

struct CMouseEvent;
struct CKeyboardEvent;
struct CTextEvent;
struct CResizeEvent;


/**
 * \class   IActivity
 * \ingroup Game
 * \brief   Une activit� g�re un �tat de l'application.
 *
 * Exemples d'activit� : menu, jeu, vid�o, etc.
 ******************************/

class TENGINE2_API IActivity /*: public IEventReceiver*/
{
public:

    IActivity() { gGameApplication->addActivity(this); }
    virtual ~IActivity() { gGameApplication->removeActivity(this); }
    IActivity(const IActivity&) = delete;
    IActivity(IActivity&&) = delete;
    IActivity& operator=(const IActivity&) = delete;
    IActivity& operator=(IActivity&&) = delete;

    virtual void frame(float frameTime) = 0;

    virtual void onEvent(const CMouseEvent& event) { }
    virtual void onEvent(const CKeyboardEvent& event) { }
    virtual void onEvent(const CTextEvent& event) { }
    virtual void onEvent(const CResizeEvent& event) { }
};

} // Namespace TE
