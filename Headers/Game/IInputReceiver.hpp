/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <SFML/Window.hpp>

#include "library.h"
#include "Game/actions.hpp"


namespace TE
{

struct CMouseEvent;
struct CKeyboardEvent;


/**
 * \class   IInputReceiver
 * \ingroup Game
 * \brief   Classe de base des récepteurs d'évènements, comme les caméras et les joueurs.
 ******************************/

class TENGINE2_API IInputReceiver
{
public:

    // Constructeur et destructeur
    IInputReceiver();
    virtual ~IInputReceiver() = 0;

    // Méthodes publiques
    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);

    //TAction getActionForKey(TKey key) const;
    //TKey getKeyForAction(TAction action) const;
    bool isActionActive(TAction action) const;
    //void setKeyForAction(TAction action, TKey key);

    bool isEventsEnable() const
    {
        return m_eventsEnable;
    }

    void setEventsEnable(bool enable)
    {
        m_eventsEnable = enable;
    }

    bool isKeyPressed(sf::Keyboard::Key key) const;

private:

    bool m_eventsEnable;
};

} // Namespace TE
