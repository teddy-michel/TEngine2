/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once


namespace TE
{

/**
 * \enum    TAction
 * \ingroup Game
 * \brief   Liste des actions possibles pour un joueur ou un v�hicule.
 ******************************/

enum class TAction
{
    ActionNone,            ///< Aucune action.

    // Joueur
    ActionForward,         ///< D�placement vers l'avant.
    ActionBackward,        ///< D�placement vers l'arri�re.
    ActionStrafeLeft,      ///< D�placement sur le c�t� gauche.
    ActionStrafeRight,     ///< D�placement sur le c�t� droit.
    ActionTurnLeft,        ///< Rotation vers la gauche.
    ActionTurnRight,       ///< Rotation vers la droite.
    ActionTurnUp,          ///< Rotation vers le haut.
    ActionTurnDown,        ///< Rotation vers le bas.
    ActionJump,            ///< Saut.
    ActionCrunch,          ///< S'accroupir.
    ActionZoom,            ///< Zoom.
    ActionFire1,           ///< Tir primaire.
    ActionFire2,           ///< Tir secondaire.

    // V�hicule
    ActionVehicleAccel,     ///< Acc�l�ration.
    ActionVehicleBrake,     ///< Freinage
    ActionVehicleTurnLeft,  ///< Rotation vers la gauche.
    ActionVehicleTurnRight, ///< Rotation vers la droite.
    ActionVehicleHandBrake, ///< Frein � main.
    ActionVehicleGearUp,    ///< Rapport de vitesse sup�rieur.
    ActionVehicleGearDown   ///< Rapport de vitesse inf�rieur.
};

} // Namespace TE
