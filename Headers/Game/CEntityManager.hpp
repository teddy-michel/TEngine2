/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <json.hpp>

#include "Core/CString.hpp"


namespace TE
{

class IEntity;
class CMap;
class CRenderer;


typedef uint32_t TEntityId; ///< Identifiant d'entit� (entier sign� de taille fixe).


/**
 * \class   CEntityManager
 * \ingroup Game
 * \brief   Gestion des entit�s. Singleton accessible via l'objet global gEntityManager.
 ******************************/

class TENGINE2_API CEntityManager
{
    friend class IEntity; // pour acc�der � addEntity et removeEntity
    friend class CRenderer; // pour acc�der � getModelMatrices

public:

    CEntityManager();
    ~CEntityManager();

    bool loadFromFile(const CString& fileName, CMap * map);
    bool loadFromBuffer(const char * buffer, CMap * map);

private:

    // M�thodes appell�es par IEntity
    TEntityId addEntity(IEntity * entity);
    void removeEntity(IEntity * entity);

    void getWorldMatrix(TEntityId id, glm::mat4& matrix) const;
    void setWorldMatrix(TEntityId id, const glm::mat4& matrix);
    inline const std::vector<glm::mat4>& getModelMatrices() const;

    bool loadEntityFromJson(const nlohmann::json& json, IEntity * parent, CMap * map);

    // Donn�es priv�es
    std::vector<IEntity *> m_entities; ///< Liste des entit�s.
    std::vector<glm::mat4> m_models;   ///< Liste des matrices de transformation pour chaque entit�.
};


/// Instance unique.
extern TENGINE2_API CEntityManager * gEntityManager;


inline const std::vector<glm::mat4>& CEntityManager::getModelMatrices() const
{
    return m_models;
}

} // Namespace TE
