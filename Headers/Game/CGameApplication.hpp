/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <map>
#include <memory>
#include <SFML/Window.hpp>

#include "Core/CString.hpp"
#include "Core/ILogger.hpp"
#include "Core/Events.hpp"
#include "Core/CSettings.hpp"
#include "Core/CApplication.hpp"
#include "Game/IInputReceiver.hpp"


namespace TE
{

class CMap;
class CPlayer;
class CCamera;
class IActivity;


/**
 * \class   CGameApplication
 * \ingroup Game
 * \brief   Classe permettant de g�rer l'application.
 *
 * Singleton accessible avec la variable globale gGameApplication.
 ******************************/

class TENGINE2_API CGameApplication : public CApplication /*: public IEventReceiver*/
{
    friend class IActivity; // pour acc�der � addActivity et removeActivity

public:
   
    CGameApplication();
    virtual ~CGameApplication();

    CGameApplication(const CGameApplication&) = delete;
    CGameApplication(CGameApplication&&) = delete;
    CGameApplication& operator=(const CGameApplication&) = delete;
    CGameApplication& operator=(CGameApplication&&) = delete;

    void setTitle(const CString& title);

    //void setMap(CMap * map);  // TODO: move to CActivityGame
    //CMap * getMap() const;  // TODO: move to CActivityGame

    //void setPlayer(CPlayer * player);  // TODO: move to CActivityGame
    //inline CPlayer * getPlayer() const noexcept;  // TODO: move to CActivityGame

    //void setCamera(CCamera * camera);  // TODO: move to CActivityGame
    //inline CCamera * getCamera() const noexcept;  // TODO: move to CActivityGame

    void setReceiver(IInputReceiver * receiver);

    TAction getActionForKey(TKey key) const;
    TKey getKeyForAction(TAction action) const;
    bool isActionActive(TAction action) const;
    void setKeyForAction(TAction action, TKey key);

    virtual unsigned int getWidth() const override;
    virtual unsigned int getHeight() const override;
    sf::Vector2i getCursorPosition() const;
    inline bool isFullScreen() const noexcept;
    bool isCursorVisible() const;
    void showMouseCursor(bool show = true);

    inline float getFPS() const noexcept;
    
    void initWindow(const CString& title);

    virtual bool init() override;
    virtual void frame() override;
    void processEvents();
    virtual void release() override;

    //void log(const CString& message, ILogger::TLevel level = ILogger::Info);

    //using IEventReceiver::onEvent;
    virtual void onEvent(const CMouseEvent& event) override;
    virtual void onEvent(const CTextEvent& event) override;
    virtual void onEvent(const CKeyboardEvent& event) override;
    virtual void onEvent(const CResizeEvent& event) override;

    void setActivity(IActivity * activity);

protected:

    CString m_title;
    sf::Window m_window;
    //bool m_running;

private:

    void addActivity(IActivity * activity);
    void removeActivity(IActivity * activity);

    //CMap * m_map;                           ///< Map charg�e. (TODO: move to CActivityGame)
    //CPlayer * m_player;                     ///< Pointeur sur le joueur actif. (TODO: move to CActivityGame)
    //CCamera * m_camera;                     ///< Cam�ra du jeu. (TODO: move to CActivityGame)
    IInputReceiver * m_receiver;            ///< R�cepteur d'�v�nements (joueur ou cam�ra). (TODO: move to CActivityGame)
    IActivity * m_activity;                 ///< Activit� courante.
    std::vector<IActivity *> m_activities;  ///< Liste des activit�s.
    bool m_fullscreen;                      ///< Indique si l'application est en plein �cran.
    bool m_gameActive;                      ///< Indique si le jeu est actif ou pas. (TODO: move to CActivityGame)
    sf::Vector2i m_cursorPos;               ///< Position du curseur.
    bool m_cursorVisible;                   ///< Indique si le curseur de la souris doit �tre affich�.
    std::map<TAction, TKey> m_keyActions;   ///< Touches du clavier associ�es aux actions. \todo Pouvoir utiliser les boutons de la souris et du joystick.
};

/// Instance unique.
extern TENGINE2_API CGameApplication * gGameApplication;


/*
inline CPlayer * CGameApplication::getPlayer() const noexcept
{
    return m_player;
}


inline CCamera * CGameApplication::getCamera() const noexcept
{
    return m_camera;
}
*/

/**
 * Indique si la fen�tre est en plein �cran.
 *
 * \return Bool�en.
 ******************************/

inline bool CGameApplication::isFullScreen() const noexcept
{
    return m_fullscreen;
}


inline float CGameApplication::getFPS() const noexcept
{
    return m_fps;
}

} // Namespace TE
