/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Core/CString.hpp"
#include "Game/CModel.hpp"


namespace TE
{

struct TVertex3D;
template<typename T> class CBuffer;


/**
 * \class   IModelData
 * \ingroup Game
 * \brief   Classe de base des différents types de modèles.
 *
 * Contient les informations propres à chaque modèle : contrôleurs, skins, données physiques, etc.
 * Chaque fichier de modèle correspond à une instance de IModelData, qui peut ensuite être
 * utilisée par plusieurs instances de IModel.
 ******************************/

class TENGINE2_API IModelData
{
public:

    // Constructeur
    inline IModelData();
    inline virtual ~IModelData();

    // Accesseurs
    inline CString getFileName() const;
    inline CString getName() const;
    virtual unsigned int getMemorySize() const = 0;

    // Méthodes publiques
    inline virtual unsigned int getNumSequences() const;
    inline virtual unsigned int getNumSkins() const;
    inline virtual unsigned int getNumGroups() const;
    inline virtual unsigned int getSequenceNumber(const CString& name) const;
    //inline virtual CBoundingBox getBoundingBox(unsigned int sequence = 0) const;
    inline virtual void updateParams(CModel::TModelParams& params, float time) const;

    virtual void updateBuffer(CBuffer<TVertex3D> * buffer, CModel::TModelParams& params) = 0;
    virtual bool loadFromFile(const CString& fileName) = 0;

protected:

    // Données protégées
    CString m_fileName; ///< Adresse du fichier du modèle.
    CString m_name;     ///< Nom du modèle.
};


/**
 * Constructeur par défaut.
 ******************************/

inline IModelData::IModelData()
{ }


/**
 * Destructeur.
 ******************************/

inline IModelData::~IModelData()
{ }


/**
 * Donne l'adresse du fichier du modèle.
 *
 * \return Adresse du fichier du modèle.
 ******************************/

inline CString IModelData::getFileName() const
{
    return m_fileName;
}


/**
 * Donne le nom du modèle.
 *
 * \return Nom du modèle.
 ******************************/

inline CString IModelData::getName() const
{
    return m_name;
}


/**
 * Donne le nombre de séquences du modèle.
 *
 * \return Nombre de séquences du modèle.
 ******************************/

inline unsigned int IModelData::getNumSequences() const
{
    return 0;
}


/**
 * Donne le nombre de skins du modèle.
 *
 * \return Nombre de skins du modèle.
 ******************************/

inline unsigned int IModelData::getNumSkins() const
{
    return 0;
}


/**
 * Donne le nombre de groupes du modèle.
 *
 * \return Nombre de groupes du modèle.
 ******************************/

inline unsigned int IModelData::getNumGroups() const
{
    return 0;
}


/**
 * Donne le numéro d'une séquence à partir de son nom.
 *
 * \param name Nom de la séquence.
 * \return Numéro de la séquence.
 ******************************/

inline unsigned int IModelData::getSequenceNumber(const CString& name) const
{
    T_UNUSED(name);
    return 0;
}


/**
 * Donne le volume englobant le modèle pour un séquence donnée.
 *
 * \param sequence Numéro de la séquence.
 * \return Volume englobant.
 ******************************/
/*
inline CBoundingBox IModelData::getBoundingBox(unsigned int sequence) const
{
    T_UNUSED(sequence);
    return CBoundingBox();
}
*/

/**
 * Met-à-jour les paramètres d'un modèle.
 *
 * \param params Paramètres du modèle.
 * \param time   Durée écoulée depuis la dernière mise-à-jour en millisecondes.
 ******************************/

inline void IModelData::updateParams(CModel::TModelParams& params, float time) const
{
    T_UNUSED(params);
    T_UNUSED(time);
}

} // Namespace TE
