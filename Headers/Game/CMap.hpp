/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Graphic/CShadowMap.hpp"


namespace TE
{

class CPlayer;
class CCamera;
class ILight;
class CScene;
class CMapEntity;


/**
 * \class   CMap
 * \ingroup Game
 * \brief   Gestion d'une map.
 ******************************/

class TENGINE2_API CMap
{
public:

    CMap();
    ~CMap();

    inline CMapEntity * getMapEntity() const noexcept;
    inline CScene * getScene() const noexcept;

    void addLight(ILight * light);
    void removeLight(ILight * light);

    void addPlayer(CPlayer * player);
    void removePlayer(CPlayer * player);

    void update(float frameTime);

    void renderMap(CCamera * camera);

private:

    CMapEntity * m_mapEntity; ///< Entit� repr�sentant la map (racine du graphe de sc�ne).
    CScene * m_scene;         ///< Sc�ne physique associ�e � la map.
    CShadowMap m_shadowMap;   ///< Shadow map.
    //std::vector<IGraphicEntity *> m_graphicEntities; // TODO
};


inline CMapEntity * CMap::getMapEntity() const noexcept
{
    return m_mapEntity;
}


inline CScene * CMap::getScene() const noexcept
{
    return m_scene;
}

} // Namespace TE
