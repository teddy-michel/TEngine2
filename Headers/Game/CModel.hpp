/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <glm/glm.hpp>

#include "Core/CString.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/VertexFormat.hpp"


namespace TE
{

class CShader;
class IModelData;
class CRenderParams;


/**
 * \class   CModel
 * \ingroup Game
 * \brief   Cette classe contient les informations d'un modèle.
 *
 * Un modèle est caractérisé par des propriétés physiques (position, angle, vitesse,
 * vitesse angulaire, masse), un volume englobant principal et des volumes
 * englobants secondaires, un buffer graphique pour l'affichage.
 ******************************/

class TENGINE2_API CModel
{
public:

    /**
     * \enum    TModelSeqMode
     * \ingroup Game
     * \brief   Mode de lecture des séquences.
     ******************************/

    enum TModelSeqMode
    {
        Auto,   ///< Le modèle choisit ce qu'il faut faire.
        Loop,   ///< Lecture en boucle de la séquence.
        Stop,   ///< Arrêter la lecture.
        Next    ///< Passe à la séquence suivante si elle est indiquée, ou arrête la lecture.
    };


    /**
     * \struct  TModelParams
     * \ingroup Physic
     * \brief   Paramètres du modèle à afficher.
     ******************************/

    struct TModelParams
    {
        static const unsigned int NumControllers = 8; ///< Nombre de contrôleurs d'un modèle.
        //static const unsigned int NumBlenders = 2;    ///< Nombre de blenders (?).

        // Données publiques
        unsigned short sequence;           ///< Numéro de la séquence à jouer.
        unsigned short frame;              ///< Numéro de la frame dans la séquence.
        float interpolation;               ///< Interpolation avec la frame suivante.
        TModelSeqMode mode;                ///< Mode de lecture des séquences.
        unsigned short skin;               ///< Numéro du skin à utiliser.
        unsigned short group;              ///< Numéro du groupe à utiliser.
        float controllers[NumControllers]; ///< Valeurs des contrôleurs.
        float blending[2];                 ///< Valeurs du blending.

        /// Constructeur par défaut.
        inline TModelParams() :
            sequence      (0),
            frame         (0),
            interpolation (0.0f),
            mode          (Auto),
            skin          (0),
            group         (0)
        {
            for (unsigned int i = 0; i < NumControllers; ++i)
            {
                controllers[i] = 0.0f;
            }

            blending[0] = blending[1] = 0.0f;
        }
    };


    // Constructeur et destructeur
    explicit CModel(IModelData * modelData);
    ~CModel() = default;

    // Accesseurs
    //glm::vec3 getPosition() const;
    //glm::quat getRotation() const;
    //glm::vec3 getLinearVelocity() const;
    //glm::vec3 getAngularVelocity() const;
    float getMass() const;
    //TMatrix3F getInvInertia() const;
    inline bool isMovable() const noexcept;
    //CBoundingBox getBoundingBox() const;
    //inline std::shared_ptr<CBuffer> getBuffer() const noexcept;
    inline IModelData * getModelData() const noexcept;
    unsigned int getSequence() const;
    TModelSeqMode getSeqMode() const;
    unsigned int getSkin() const;
    unsigned int getGroup() const;
    float getControllerValue(unsigned int controller) const;
    float getBlendingValue(unsigned int blender) const;

    // Mutateurs
    //void setPosition(const glm::vec3& position);
    //void translate(const glm::vec3& v);
    //void setRotation(const glm::quat& rotation);
    //void setLinearVelocity(const glm::vec3& velocity);
    //void setAngularVelocity(const glm::vec3& velocity);
    void setMass(float mass);
    //void setLocalInertia(const glm::vec3& inertia);
    void setMovable(bool movable = true);
    void setSequence(unsigned int sequence);
    void setSequenceByName(const CString& name);
    void setSeqMode(TModelSeqMode mode);
    void setSkin(unsigned int skin);
    void setGroup(unsigned int group);
    void setControllerValue(unsigned int controller, float value);
    void setBlendingValue(unsigned int blender, float value);
    //void setOffset(const glm::vec3& offset);

    // Méthodes publiques
    //void addForce(const glm::vec3& force);
    //void addTorque(const glm::vec3& torque);

    void update(float frameTime);
    void render(CRenderParams * params, const std::shared_ptr<CShader>& shader, const glm::mat4& world);

protected:

    // Données protégées
    float m_mass;               ///< Masse du modèle en kilogrammes.
    bool m_movable;             ///< Indique si le modèle est mobile.
    std::unique_ptr<CBuffer<TVertex3D>> m_buffer; ///< Pointeur sur le buffer permettant l'affichage du modèle.
    IModelData * m_data;        ///< Pointeur sur les données du modèle.
    TModelParams m_params;      ///< Paramètres du modèle.
    //glm::vec3 m_offset;         ///< Décalage entre l'objet physique et le buffer graphique.
};


/**
 * Indique si l'objet est mobile.
 *
 * \return Booléen indiquant si l'objet est mobile.
 *
 * \sa CModel::setMovable
 ******************************/

inline bool CModel::isMovable() const noexcept
{
    return m_movable;
}


/**
 * Retourne le buffer graphique utilisé par le modèle.
 *
 * \return Pointeur sur le buffer graphique du modèle.
 *
 * \sa CModel::setBuffer
 ******************************/
/*
inline std::shared_ptr<CBuffer> CModel::getBuffer() const noexcept
{
    return m_buffer;
}
*/

/**
 * Retourne les données utilisées par le modèle.
 *
 * \return Pointeur sur les données utilisées par le modèle.
 ******************************/

inline IModelData * CModel::getModelData() const noexcept
{
    return m_data;
}

} // Namespace TE
