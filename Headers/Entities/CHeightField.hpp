/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <memory>

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CTextureManager.hpp"


namespace physx
{
    class PxRigidActor;
}

namespace TE
{

class CStaticActor;
struct TVertex3D;
template<typename T> class IBuffer;


/**
 * \class   CHeightField
 * \ingroup Entities
 * \brief   Entit� qui permet de g�rer un terrain.
 *
 * \todo Pouvoir pr�ciser le pourcentage de chaque texture pour chaque point.
 ******************************/

class TENGINE2_API CHeightField : public IGraphicEntity
{
public:

    explicit CHeightField(IEntity * parent, const CString& name = CString{});
    explicit CHeightField(CMap * map, const CString& name = CString{});
    virtual ~CHeightField() = default;

    void setSubdivisions(unsigned int x, unsigned int y);
    void setSize(const glm::vec2& size);
    void setHeightScale(float heightScale);
    void setPoints(const std::vector<int16_t>& points);
    void setTexture(TTextureId textureId, unsigned int unit = 0);

    virtual void renderEntity(CRenderParams * params) override;

    virtual void initEntity() override;

private:

    virtual void notifyWorldMatrixChange() override;

    unsigned int m_subdivisionsX;
    unsigned int m_subdivisionsY;
    glm::vec2 m_size;
    float m_heightScale;
    std::vector<int16_t> m_points;
    std::shared_ptr<CStaticActor> m_actor; ///< Acteur physique.
    std::unique_ptr<IBuffer<TVertex3D>> m_buffer; ///< Buffer graphique pour l'affichage du terrain.
    // TODO: texture scale pour chaque unit�
    // TODO: texture rotation pour chaque unit�
};

} // Namespace TE
