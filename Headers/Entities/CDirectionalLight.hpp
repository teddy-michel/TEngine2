/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IDirectionalLight.hpp"


namespace TE
{

/**
 * \class   CDirectionalLight
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer une lumi�re dynamique directionnelle.
 ******************************/

class TENGINE2_API CDirectionalLight : public IDirectionalLight
{
public:

    explicit CDirectionalLight(IEntity * parent, const CString& name = CString{});
    explicit CDirectionalLight(CMap * map, const CString& name = CString{});
    virtual ~CDirectionalLight() = default;

    inline virtual TLightType getLightType() const noexcept override;

    virtual glm::mat4 getViewMatrix(int face = 0) const noexcept override;
    inline virtual glm::mat4 getProjectionMatrix() const noexcept override;

private:

    glm::mat4 m_projection; ///< Matrice de projection.
};


inline TLightType CDirectionalLight::getLightType() const noexcept
{
    return TLightType::Directional;
}


inline glm::mat4 CDirectionalLight::getProjectionMatrix() const noexcept
{
    return m_projection;
}

} // Namespace TE
