/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <list>
#include <glm/glm.hpp>
#include <json.hpp>

#include "Core/CString.hpp"
#include "Game/CEntityManager.hpp"


namespace TE
{

class CRenderParams;
class IWeapon;
class CMapEntity;


/**
 * \class   IEntity
 * \ingroup Entities
 * \brief   Classe de base des entit�s.
 *
 * Toutes les entit�s poss�de un nom et une matrice de transformation locale et globale.
 *
 * Les entit�s sont plac�s dans un arbre (graphe de sc�ne).
 ******************************/

class TENGINE2_API IEntity
{
    friend class CPhysicEngine; // pour acc�der � updateWorldMatrixByPhysic.
    friend class CScene; // pour acc�der � updateWorldMatrixByPhysic.
    friend class CMap; // pour acc�der � update.
    friend class CActivityGame; // pour acc�der � update.
    friend class CPlayer; // pour acc�der � updateWorldMatrix.

public:

    explicit IEntity(IEntity * parent, const CString& name = CString{});
    explicit IEntity(CMap * map, const CString& name = CString{});
    IEntity(const IEntity&) = delete;
    IEntity(IEntity&&) = delete;
    IEntity& operator=(const IEntity&) = delete;
    IEntity& operator=(IEntity&&) = delete;
    virtual ~IEntity() = 0;

    inline TEntityId getEntityId() const noexcept;
    inline CString getName() const noexcept;
    inline CMap * getMap() const noexcept;
    CScene * getScene() const;

    // Hi�rarchie
    inline IEntity * getParent() const noexcept;
    IEntity * getRoot();
    inline std::list<IEntity *> getChildren() const noexcept;
    bool addChild(IEntity * entity);
    bool setParent(IEntity * entity);
    bool isAncestorOf(IEntity * entity) const;
    void setFixParent(bool fix);

    // Transformations
    inline glm::mat4 getWorldMatrix() const noexcept;
    inline glm::mat4 getLocalMatrix() const noexcept;
    glm::vec3 getWorldPosition() const;
    glm::vec3 getLocalPosition() const;
    glm::quat getWorldOrientation() const;
    glm::quat getLocalOrientation() const;
    glm::vec3 getWorldScale() const;
    glm::vec3 getLocalScale() const;

    void setWorldMatrix(const glm::mat4& worldMatrix);
    void setLocalMatrix(const glm::mat4& localMatrix);
    void setPosition(const glm::vec3& position);
    void translate(const glm::vec3& translation);
    void setOrientation(const glm::quat& orientation);
    void rotate(const glm::quat& rotation);
    void setScale(const glm::vec3& scale);
    void scale(const glm::vec3& scale);

    // Affichage
    void render(CRenderParams * params);
    virtual void renderEntity(CRenderParams * params);
    virtual void renderChildren(CRenderParams * params);

    virtual void initEntity();
    virtual void updateEntity(float frameTime);

    virtual void onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage);

    virtual bool checkInteraction(CPlayer * player, float distance);
    virtual bool interact(CPlayer * player);

    virtual bool loadFromJson(const nlohmann::json& json); // = 0

protected:

    glm::mat4 m_localMatrix;         ///< Matrice de transformation locale.
    IEntity * m_parent;              ///< Entit� parent.
    std::list<IEntity *> m_children; ///< Liste des entit�s enfant.

    void updateWorldMatrix(const glm::mat4& parentWorldMatrix, bool notify = true);
    virtual void notifyWorldMatrixChange();

    static bool parseJsonPoint(const nlohmann::json& json, glm::vec3& point);
    static bool parseJsonPoint(const nlohmann::json& json, glm::vec2& point);

private:

    void initEntities(); // TODO: rename to init() ?
    void update(float frameTime);
    void updateWorldMatrixByPhysic(const glm::mat4& worldMatrix);
    virtual void notifyLocalMatrixChange();

    CMap * m_map;         ///< Map contenant l'entit�.
    CString m_name;       ///< Nom de l'entit�.
    const TEntityId m_id; ///< Identifiant de l'entit�.
    bool m_fixParent;     ///< Indique si l'entit� parent est fixe et ne peut pas �tre modifi�e.
};


#define T_PARSE_ENTITY_ATTR_BOOL(NAME, VAR, B)    do { \
    if (json.contains(NAME))                           \
    {                                                  \
        const auto attribute = json[NAME];             \
        if (!attribute.is_boolean())                   \
        {                                              \
            gApplication->log("Entity attribute \""##NAME"\" should be a boolean.", ILogger::Error);  \
            return false;                              \
        }                                              \
        VAR = attribute;                               \
        B = true;                                      \
    } else { B = false; } } while(0)


#define T_PARSE_ENTITY_ATTR_NUMBER(NAME, VAR, B)  do { \
    if (json.contains(NAME))                           \
    {                                                  \
        const auto attribute = json[NAME];             \
        if (!attribute.is_number())                    \
        {                                              \
            gApplication->log("Entity attribute \""##NAME"\" should be a number.", ILogger::Error);  \
            return false;                              \
        }                                              \
        VAR = attribute;                               \
        B = true;                                      \
    } else { B = false; } } while(0)


#define T_PARSE_ENTITY_ATTR_INTEGER(NAME, VAR, B) do { \
    if (json.contains(NAME))                           \
    {                                                  \
        const auto attribute = json[NAME];             \
        if (!attribute.is_number_integer())            \
        {                                              \
            gApplication->log("Entity attribute \""##NAME"\" should be an integer.", ILogger::Error);  \
            return false;                              \
        }                                              \
        VAR = attribute;                               \
        B = true;                                      \
    } else { B = false; } } while(0)


#define T_PARSE_ENTITY_ATTR_STRING(NAME, VAR, B)  do { \
    if (json.contains(NAME))                           \
    {                                                  \
        const auto attribute = json[NAME];             \
        if (!attribute.is_string())                    \
        {                                              \
            gApplication->log("Entity attribute \""##NAME"\" should be a string.", ILogger::Error);  \
            return false;                              \
        }                                              \
        VAR = attribute.get<std::string>().c_str();    \
        B = true;                                      \
    } else { B = false; } } while(0)


#define T_PARSE_ENTITY_ATTR_VEC(NAME, VAR, B)     do { \
    if (json.contains(NAME))                           \
    {                                                  \
        const auto attribute = json[NAME];             \
        if (!attribute.is_object())                    \
        {                                              \
            gApplication->log("Entity attribute \""##NAME"\" should be an object.", ILogger::Error);  \
            return false;                              \
        }                                              \
        if (!parseJsonPoint(attribute, VAR))           \
        {                                              \
            gApplication->log("Entity attribute \""##NAME"\" is invalid.", ILogger::Error); \
            return false;                              \
        }                                              \
        B = true;                                      \
    } else { B = false; } } while(0)


/**
 * Retourne l'identifiant de l'entit�.
 *
 * \return Identifiant de l'entit�.
 ******************************/

inline TEntityId IEntity::getEntityId() const noexcept
{
    return m_id;
}


/**
 * Retourne le nom de l'entit�.
 *
 * \return Nom de l'entit�.
 ******************************/

inline CString IEntity::getName() const noexcept
{
    return m_name;
}


inline CMap * IEntity::getMap() const noexcept
{
    return m_map;
}


/**
 * Retourne le pointeur sur l'entit� parent.
 *
 * \return Entit� parent. Peut �tre nul.
 ******************************/

inline IEntity * IEntity::getParent() const noexcept
{
    return m_parent;
}


/**
 * Retourne la liste des entit�s enfant.
 *
 * \return Liste des entit�s enfant.
 ******************************/

inline std::list<IEntity *> IEntity::getChildren() const noexcept
{
    return m_children;
}


inline glm::mat4 IEntity::getWorldMatrix() const noexcept
{
    glm::mat4 matrix;
    gEntityManager->getWorldMatrix(m_id, matrix);
    return matrix;
}


inline glm::mat4 IEntity::getLocalMatrix() const noexcept
{
    return m_localMatrix;
}

} // Namespace TE
