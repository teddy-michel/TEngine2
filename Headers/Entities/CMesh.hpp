/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IGraphicEntity.hpp"


namespace TE
{

class CStaticActor;
class CShapeTriMesh;
struct TVertex3D;
template<typename T> class CBuffer;


/**
 * \class   CMesh
 * \ingroup Entities
 * \brief   Entit� permettant d'afficher un maillage.
 ******************************/

class TENGINE2_API CMesh : public IGraphicEntity
{
public:

    explicit CMesh(IEntity * parent, const CString& name = CString{});
    explicit CMesh(CMap * map, const CString& name = CString{});
    virtual ~CMesh();

    virtual void renderEntity(CRenderParams * params) override;

    std::shared_ptr<CBuffer<TVertex3D>> getBuffer() const;
    void setBuffer(const std::shared_ptr<CBuffer<TVertex3D>>& buffer);

    void setTriMesh(const std::shared_ptr<CShapeTriMesh>& trimesh);

protected:

    std::shared_ptr<CBuffer<TVertex3D>> m_buffer; ///< Buffer graphique.
    std::shared_ptr<CStaticActor> m_actor; ///< Acteur physique.

private:

    virtual void notifyWorldMatrixChange() override;
};

} // Namespace TE
