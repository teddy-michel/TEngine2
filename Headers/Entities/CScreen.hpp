/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Graphic/CFrameBuffer.hpp"


namespace TE
{

class CCamera;
struct TVertex3D;
template<typename T> class IBuffer;


/**
 * \class   CScreen
 * \ingroup Entities
 * \brief   Affiche une surface avec la sc�ne dessin�e depuis une cam�ra.
 ******************************/

class TENGINE2_API CScreen : public IGraphicEntity
{
public:

    explicit CScreen(IEntity * parent, const CString& name = CString{});
    explicit CScreen(CMap * map, const CString& name = CString{});
    virtual ~CScreen() = default;

    virtual void renderEntity(CRenderParams * params) override;

    void setCamera(CCamera * camera);
    inline CCamera * getCamera() const noexcept;
    void setSize(float width, float height);
    inline float getWidth() const noexcept;
    inline float getHeight() const noexcept;
    void setFBOSize(unsigned int width, unsigned int height);
    unsigned int getFBOWidth() const;
    unsigned int getFBOHeight() const;
    TTextureId getTextureId() const;

    virtual void initEntity() override;

private:

    void initBuffers();
    void updateBuffer();

protected:

    CCamera * m_camera;            ///< Pointeur sur la cam�ra utilis� par l'�cran. \todo Use an CEntityReference (CString + std::weak_ptr<IEntity>)
    CFrameBuffer m_frameBuffer;    ///< Framebuffer utilis� pour le rendu.
    std::unique_ptr<IBuffer<TVertex3D>> m_buffer; ///< Buffer graphique utilis� pour l'affichage.
    float m_width;                 ///< Largeur de l'�cran.
    float m_height;                ///< Hauteur de l'�cran.
    unsigned int m_fboWidth;       ///< Largeur du buffer.
    unsigned int m_fboHeight;      ///< Hauteur du buffer.
    TTextureId m_colorBufferFront; ///< Identifiant de la texture correspondant au front buffer.
    TTextureId m_colorBufferBack;  ///< Identifiant de la texture correspondant au back buffer.
    bool m_colorBuffersLoaded;     ///< Indique si les Color Buffers sont charg�s.
    bool m_fboIsValid;             ///< Indique si le FBO est valide.
};


inline CCamera * CScreen::getCamera() const noexcept
{
    return m_camera;
}


/**
 * Donne la largeur de l'�cran.
 *
 * \return Largeur de l'�cran.
 ******************************/

inline float CScreen::getWidth() const noexcept
{
    return m_width;
}


/**
 * Donne la hauteur de l'�cran.
 *
 * \return Hauteur de l'�cran.
 ******************************/

inline float CScreen::getHeight() const noexcept
{
    return m_height;
}

} // Namespace TE
