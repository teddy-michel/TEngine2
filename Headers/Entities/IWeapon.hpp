/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IEntity.hpp"


namespace TE
{

class CPlayer;


/**
 * \class   IWeapon
 * \ingroup Entities
 * \brief   Classe de base des armes.
 *
 * Les armes peuvent avoir deux modes de tir :
 * - continu : le calcul des effets du tir se fait dans la m�thode updateEntity, en utilisant la m�thode isFireActive.
 * - coup par coup : le calcul se fait dans les m�thodes primaryFire ou secondaryFire.
 ******************************/

class TENGINE2_API IWeapon : public IEntity
{
    friend class CPlayer; // pour acc�der � m_player

public:

    explicit IWeapon(IEntity * parent, const CString& name = CString{});
    explicit IWeapon(CMap * map, const CString& name = CString{});
    virtual ~IWeapon() = 0;

    inline CPlayer * getPlayer() const noexcept;
    bool isFireActive() const;
    void setFireActive(bool active);

    virtual void primaryFire();
    virtual void secondaryFire();

protected:

    CPlayer * m_player; ///< Joueur qui poss�de l'arme (peut �tre nul).

private:

    bool m_fireActive;  ///< Indique si l'arme est en train de tirer.
};


inline CPlayer * IWeapon::getPlayer() const noexcept
{
    return m_player;
}

} // Namespace TE
