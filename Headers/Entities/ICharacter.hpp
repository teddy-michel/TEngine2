/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IEntity.hpp"


namespace TE
{

class CCamera;
class CBox;


/**
 * \class   ICharacter
 * \ingroup Entities
 * \brief   Classe de base des personnages.
 *
 * Une cam�ra est automatiquement attach�e au personnage.
 *
 * Un personnage poss�de un certains nombre de points de vie (100 par d�faut).
 * Lorsque cette valeur est n�gative, on consid�re que le personnage est mort.
 ******************************/

class TENGINE2_API ICharacter : public IEntity
{
public:

    explicit ICharacter(IEntity * parent, const CString& name = CString{});
    explicit ICharacter(CMap * map, const CString& name = CString{});
    virtual ~ICharacter() = 0;

    CCamera * getCamera() const noexcept;

    // Vie
    float getLife() const noexcept  { return m_life; }
    virtual float setLife(float life) noexcept;
    virtual float addLife(float life);

    // Dimensions
    virtual void setHeight(float height);
    float getHeight() const noexcept;
    virtual void setRadius(float radius);
    float getRadius() const noexcept;

    float getPhi() const { return m_phi; }

    virtual void renderChildren(CRenderParams * params) override;

    virtual void onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage) override;

private:

    void init();
    virtual void notifyLocalMatrixChange() final override;
    void updateCameraDirection(bool needUpdateWorldMatrix);

    float m_theta; ///< Angle de vue horizontal.
    float m_phi;   ///< Angle de vue vertical.

protected:

    void changeViewAngles(float theta, float phi);
    virtual void updateViewDirection(const glm::quat& direction);

    CCamera * m_camera; ///< Cam�ra attach�e au personnage.

private:

    float m_life;   ///< Nombre de points de vie.
    float m_height; ///< Hauteur du joueur.
    float m_radius; ///< Rayon du volume englobant du joueur.
    CBox * m_meshBody; // DEBUG
    CBox * m_meshHead; // DEBUG
    //CModel * m_model;
};

} // Namespace TE
