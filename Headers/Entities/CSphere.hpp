/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CTextureManager.hpp"


namespace TE
{

class IActor;
struct TVertex3D;
template<typename T> class IBuffer;


/**
 * \class   CSphere
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer une sph�re.
 *
 * \todo Rendre les param�tres m_slices et m_rings configurable.
 * \todo Calculer automatiquement m_slices et m_rings en fonction du rayon ?
 ******************************/

class TENGINE2_API CSphere : public IGraphicEntity
{
    static const unsigned int NumUnit = 2; ///< Nombre d'unit�s de texture utilisables.

public:

    explicit CSphere(IEntity * parent, const CString& name = CString{});
    explicit CSphere(CMap * map, const CString& name = CString{});
    virtual ~CSphere();

    inline float getRadius() const noexcept;
    void setRadius(float radius);

    void setTexture(TTextureId textureId, unsigned int unit = 0);

    inline bool isSolid() const noexcept;
    void setSolid(bool solid);

    inline bool isDynamic() const noexcept;
    void setDynamic(bool dynamic);

    inline float getMass() const noexcept;
    void setMass(float mass);

    void setLinearVelocity(const glm::vec3& velocity);

    virtual void initEntity() override;
    virtual void renderEntity(CRenderParams* params) override;

    virtual bool loadFromJson(const nlohmann::json& json) override;
/*
    // DEBUG
    virtual void onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage) override;

    // DEBUG
    virtual bool checkInteraction(CPlayer * player, float distance) override;
    virtual bool interact(CPlayer * player) override;
*/
private:

    void updateBuffer();
    void updateShape();
    virtual void notifyWorldMatrixChange() override;

    bool m_init;           ///< Indique si l'entit� a �t� initialis�e.
    float m_radius;        ///< Rayon de la sph�re.
    unsigned int m_slices; ///< Nombre de tranches pour l'affichage.
    unsigned int m_rings;  ///< Nombre d'anneau pour l'affichage.
    bool m_solid;          ///< Indique si le volume est solide (g�re les collisions).
    bool m_dynamic;        ///< Indique si le volume est dynamique (suit les r�gles de la physique).
    float m_mass;          ///< Masse de la sph�re si le volume est dynamique.
    std::unique_ptr<IBuffer<TVertex3D>> m_buffer; ///< Buffer graphique.
    TTextureId m_textures[NumUnit];  ///< Liste des textures � appliquer � la sph�re.
    std::shared_ptr<IActor> m_actor; ///< Acteur physique associ� � la sph�re.
};


/**
 * Retourne le rayon de la sph�re.
 *
 * \return Rayon de la sph�re.
 ******************************/

inline float CSphere::getRadius() const noexcept
{
	return m_radius;
}


/**
 * Indique si la sph�re est solide, c'est-�-dire qu'elle g�re les collisions avec l'environnement.
 *
 * \return True si la sph�re est solide, false sinon.
 ******************************/

inline bool CSphere::isSolid() const noexcept
{
	return m_solid;
}


/**
 * Indique si la sph�re est dynamique, c'est-�-dire qu'elle suit les r�gles de la physique.
 *
 * \return True si la sph�re est dynamique, false sinon.
 ******************************/

inline bool CSphere::isDynamic() const noexcept
{
	return m_dynamic;
}


/**
 * Donne la masse de la sph�re.
 *
 * \return Masse en kilogrammes.
 ******************************/

inline float CSphere::getMass() const noexcept
{
	return m_mass;
}

} // Namespace TE
