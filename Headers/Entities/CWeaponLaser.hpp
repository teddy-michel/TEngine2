/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IWeapon.hpp"


namespace TE
{

/**
 * \class   CWeaponLaser
 * \ingroup Entities
 * \brief   Arme de type faisceau Laser.
 ******************************/

class TENGINE2_API CWeaponLaser : public IWeapon
{
public:

    explicit CWeaponLaser(IEntity * parent, const CString& name = CString{});
    explicit CWeaponLaser(CMap * map, const CString& name = CString{});
    virtual ~CWeaponLaser() = default;

    virtual void updateEntity(float frameTime) override;
    virtual void renderEntity(CRenderParams * params) override;

private:

    // TODO: buffer pour la vue � la premi�re personne.
    // TODO: buffer pour la vue � la troisi�me personne.
    glm::vec3 m_direction; ///< Point de destination du faisceau, utilis� pour le rendu.
};

} // Namespace TE
