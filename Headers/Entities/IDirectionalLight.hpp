/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/ILight.hpp"


namespace TE
{

/**
 * \class   IDirectionalLight
 * \ingroup Entities
 * \brief   Entit� de base permettant de g�rer les lumi�res directionnelles.
 *
 * Les lumi�res directionnelles sont de deux types :
 * - CDirectionalLight : lumi�re directionnelle comme le soleil,
 * - CSpotLight : faisceau.
 * Ces lumi�res utilisent une shadow map pour calculer les ombres.
 ******************************/

class TENGINE2_API IDirectionalLight : public ILight
{
public:

    explicit IDirectionalLight(IEntity * parent, const CString& name = CString{});
    explicit IDirectionalLight(CMap * map, const CString& name = CString{});
    virtual ~IDirectionalLight() = 0;
};

} // Namespace TE
