/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IEntity.hpp"
#include "Game/IInputReceiver.hpp"


namespace TE
{

class CVehicleController;
class CPlayer;


/**
 * \class   CVehicle
 * \ingroup Entities
 * \brief   Entit� repr�sentant un v�hicule.
 *
 * \todo Affichage
 * \todo D�finition des param�tres.
 ******************************/

class TENGINE2_API CVehicle : public IEntity, public IInputReceiver
{
public:

    explicit CVehicle(IEntity * parent, const CString& name = CString{});
    explicit CVehicle(CMap * map, const CString& name = CString{});
    virtual ~CVehicle();

    inline CPlayer * getPlayer() const noexcept;
    bool setPlayer(CPlayer * player);

    float getSpeed() const;
    float getEngineSpeed() const;
    int getCurrentGear() const;

    virtual void updateEntity(float frameTime) override;
    virtual void renderEntity(CRenderParams * params) override;

private:

    void initController();
    void releaseController();

    virtual void notifyWorldMatrixChange() override;

    CVehicleController * m_controller; ///< Contr�leur physique du v�hicule.
    CPlayer * m_player;                ///< Conducteur du v�hicule.
    // TODO: buffer graphique pour le chassis
    // TODO: buffer graphique pour chacune des roues
    // TODO: buffer graphique pour le volant
};


inline CPlayer * CVehicle::getPlayer() const noexcept
{
    return m_player;
}

} // Namespace TE
