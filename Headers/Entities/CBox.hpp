/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Graphic/VertexFormat.hpp"


namespace TE
{

class IActor;
struct TVertex3D;
template<typename T> class IBuffer;


/**
 * \class   CBox
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer une boite.
 ******************************/

class TENGINE2_API CBox : public IGraphicEntity
{
    static const unsigned int NumUnit = 2; ///< Nombre d'unit�s de texture utilisables.

public:

    explicit CBox(IEntity * parent, const CString& name = CString{});
    explicit CBox(CMap * map, const CString& name = CString{});
    virtual ~CBox();

    inline glm::vec3 getSize() const noexcept;
    void setSize(const glm::vec3& size);

    void setTextureScale(const glm::vec2& scale);

    void setTexture(TTextureId textureId, unsigned int unit = 0);

    inline bool isSolid() const noexcept;
    void setSolid(bool solid);

    inline bool isDynamic() const noexcept;
    void setDynamic(bool dynamic);

    inline float getMass() const noexcept;
    void setMass(float mass);

    virtual void initEntity() override;
    virtual void renderEntity(CRenderParams * params) override;

    // DEBUG
    virtual void onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage) override;

    virtual bool loadFromJson(const nlohmann::json& json) override;

private:

    void updateBuffer();
    void updateShape();
    virtual void notifyWorldMatrixChange() override;

    bool m_init;
    glm::vec3 m_size;         ///< Dimensions de la boite.
    glm::vec2 m_textureScale; ///< Facteur d'aggrandissement de la texture sur chaque axe.
    bool m_solid;             ///< Indique si la boite est solide (g�re les collisions).
    bool m_dynamic;           ///< Indique si le volume est dynamique (suit les r�gles de la physique).
    float m_mass;             ///< Masse de la boite si le volume est dynamique.
    std::shared_ptr<IBuffer<TVertex3D>> m_buffer; ///< Buffer graphique.
    TTextureId m_textures[NumUnit];  ///< Liste des textures � appliquer sur la boite.
    std::shared_ptr<IActor> m_actor; ///< Acteur physique associ� � la boite.
};


/**
 * Retourne les dimensions de la boite.
 *
 * \return Dimensions de la boite.
 ******************************/

inline glm::vec3 CBox::getSize() const noexcept
{
    return m_size;
}


/**
 * Indique si la boite est solide, c'est-�-dire qu'elle g�re les collisions avec l'environnement.
 *
 * \return True si la boite est solide, false sinon.
 ******************************/

inline bool CBox::isSolid() const noexcept
{
    return m_solid;
}


/**
 * Indique si la boite est dynamique, c'est-�-dire qu'elle suit les r�gles de la physique.
 *
 * \return True si la boite est dynamique, false sinon.
 ******************************/

inline bool CBox::isDynamic() const noexcept
{
    return m_dynamic;
}


/**
 * Donne la masse de la boite.
 *
 * \return Masse en kilogrammes.
 ******************************/

inline float CBox::getMass() const noexcept
{
    return m_mass;
}

} // Namespace TE
