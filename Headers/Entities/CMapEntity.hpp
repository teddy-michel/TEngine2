/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IEntity.hpp"
#include "Game/CMap.hpp"


namespace TE
{

class CPlayer;


/**
 * \class   CMapEntity
 * \ingroup Entities
 * \brief   Entit� repr�sentant une map.
 *
 * Cette entit� ne peut pas avoir de parent.
 ******************************/

class TENGINE2_API CMapEntity : public IEntity
{
    friend class CMap; // pour acc�der aux constructeurs, au destructeur, et � addPlayer

private:

    explicit CMapEntity(CMap * map, const CString& name = CString());
    virtual ~CMapEntity() = default;

    virtual void addPlayer(CPlayer * player);

    virtual bool loadFromJson(const nlohmann::json& json) override;
};

} // Namespace TE
