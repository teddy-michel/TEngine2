/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/ICharacter.hpp"
#include "Game/IInputReceiver.hpp"
#include "Core/Events.hpp"


namespace TE
{

class IWeapon;
class CVehicle;
class CCharacterController;


/**
 * \class   CPlayer
 * \ingroup Entities
 * \brief   Entit� repr�sentant un joueur.
 *
 * \todo Pouvoir entrer et sortir d'un v�hicule. Modifier le param�tre m_eventsEnable du v�hicule en cons�quence ?
 ******************************/

class TENGINE2_API CPlayer : public ICharacter, public IInputReceiver
{
    friend class CMap; // pour appeler initController et releaseController

public:

    explicit CPlayer(IEntity * parent, const CString& name = CString{});
    explicit CPlayer(CMap * map, const CString& name = CString{});
    virtual ~CPlayer();

    //void sendToMap(CMapEntity * map);

    // Dimensions
    virtual void setHeight(float height) override;
    virtual void setRadius(float radius) override;

    // Armes
    inline IWeapon * getCurrentWeapon() const noexcept;
    void dropCurrentWeapon();
    void addWeapon(IWeapon * weapon);
    void removeWeapon(IWeapon * weapon);

    virtual void updateEntity(float frameTime) override;
    virtual void onEvent(const CMouseEvent& event) override;
    virtual void onEvent(const CKeyboardEvent& event) override;
    virtual void onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage) override;

    struct TInteraction
    {
        IEntity * entity;
        TKey key;
        CString message;

        TInteraction() : entity{ nullptr }, key{ TKey::Key_Unknown }, message{} { }
        TInteraction(const TInteraction& other) = default;
        TInteraction(TInteraction&& other) = default;
        TInteraction& operator=(const TInteraction& other) = default;
        TInteraction& operator=(TInteraction&& other) = default;
    };

    void checkInteractions();
    void setInteraction(IEntity * entity, TKey key, const CString& message);
    bool getInteraction(TInteraction& interaction) const noexcept;

private:

    void initController();
    void releaseController();

    void movePlayer(const glm::vec3& translation, float frameTime);

protected:

    virtual void notifyWorldMatrixChange() override;
    virtual void updateViewDirection(const glm::quat& direction) override;

    bool m_onGround;                     ///< Indique si le joueur touche le sol.
    float m_fallingDuration;             ///< Dur�e de chute en secondes.
    CCharacterController * m_controller; ///< Contr�leur physique du joueur.
    IWeapon * m_currentWeapon;           ///< Arme utilis�e par le joueur. (DEBUG)
    CVehicle * m_vehicle;                ///< V�hicule actuellement utilis�.
    std::list<IWeapon *> m_weapons;      ///< Liste des armes du joueur.
    TInteraction m_interaction;          ///< Interraction possible avec l'environnement.
};


inline IWeapon * CPlayer::getCurrentWeapon() const noexcept
{
    return m_currentWeapon;
}

} // Namespace TE
