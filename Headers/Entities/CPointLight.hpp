/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/ILight.hpp"


namespace TE
{

/**
 * \class   CPointLight
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer une lumi�re dynamique ponctuelle.
 ******************************/

class TENGINE2_API CPointLight : public ILight
{
public:

    explicit CPointLight(IEntity * parent, const CString& name = CString{});
    explicit CPointLight(CMap * map, const CString& name = CString{});
    virtual ~CPointLight() = default;

    inline virtual TLightType getLightType() const noexcept override;

    void setMaxDistance(float maxDistance);
    inline float getMaxDistance() const noexcept;

    virtual bool loadFromJson(const nlohmann::json& json) override;

    virtual glm::mat4 getViewMatrix(int face = 0) const noexcept override;
    inline virtual glm::mat4 getProjectionMatrix() const noexcept override;

private:

    float m_maxDistance;    ///< Distance maximale du faisceau.
    glm::mat4 m_projection; ///< Matrice de projection.
};


inline TLightType CPointLight::getLightType() const noexcept
{
    return TLightType::Point;
}


inline float CPointLight::getMaxDistance() const noexcept
{
    return m_maxDistance;
}


inline glm::mat4 CPointLight::getProjectionMatrix() const noexcept
{
    return m_projection;
}

} // Namespace TE
