/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CTextureManager.hpp"


namespace TE
{

class IActor;
struct TVertex3D;
template<typename T> class IBuffer;


/**
 * \class   CCylinder
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer un cylindre.
 ******************************/

class TENGINE2_API CCylinder : public IGraphicEntity
{
    static const unsigned int NumUnit = 2; ///< Nombre d'unit�s de texture utilisables.

public:

    explicit CCylinder(IEntity * parent, const CString& name = CString{});
    explicit CCylinder(CMap * map, const CString& name = CString{});
    virtual ~CCylinder();

    inline float getRadius() const noexcept;
    void setRadius(float radius);

    inline float getLength() const noexcept;
    void setLength(float length);

    void setTexture(TTextureId textureId, unsigned int unit = 0);

    inline bool isSolid() const noexcept;
    void setSolid(bool solid);

    inline bool isDynamic() const noexcept;
    void setDynamic(bool dynamic);

    inline float getMass() const noexcept;
    void setMass(float mass);

    virtual void initEntity() override;
    virtual void renderEntity(CRenderParams * params) override;

    virtual bool loadFromJson(const nlohmann::json& json) override;

private:

    void updateBuffer();
    void updateShape();
    virtual void notifyWorldMatrixChange() override;

    bool m_init;           ///< Indique si l'entit� a �t� initialis�e.
    float m_radius;        ///< Rayon du cylindre.
    float m_length;        ///< Longueur du cylindre.
    bool m_solid;          ///< Indique si le volume est solide (g�re les collisions).
    bool m_dynamic;        ///< Indique si le volume est dynamique (suit les r�gles de la physique).
    float m_mass;          ///< Masse de la sph�re si le volume est dynamique.
    std::unique_ptr<IBuffer<TVertex3D>> m_buffer; ///< Buffer graphique.
    TTextureId m_textures[NumUnit];  ///< Liste des textures � appliquer au cylindre.
    std::shared_ptr<IActor> m_actor; ///< Acteur physique associ� au cylindre.
};


/**
 * Retourne le rayon du cylindre.
 *
 * \return Rayon de la cylindre.
 ******************************/

inline float CCylinder::getRadius() const noexcept
{
    return m_radius;
}


/**
 * Retourne la longueur du cylindre.
 *
 * \return Longueur de la cylindre.
 ******************************/

inline float CCylinder::getLength() const noexcept
{
    return m_length;
}


/**
 * Indique si le cylindre est solide, c'est-�-dire qu'elle g�re les collisions avec l'environnement.
 *
 * \return True si le cylindre est solide, false sinon.
 ******************************/

inline bool CCylinder::isSolid() const noexcept
{
    return m_solid;
}


/**
 * Indique si le cylindre est dynamique, c'est-�-dire qu'elle suit les r�gles de la physique.
 *
 * \return True si le cylindre est dynamique, false sinon.
 ******************************/

inline bool CCylinder::isDynamic() const noexcept
{
    return m_dynamic;
}


/**
 * Donne la masse du cylindre.
 *
 * \return Masse en kilogrammes.
 ******************************/

inline float CCylinder::getMass() const noexcept
{
    return m_mass;
}

} // Namespace TE
