/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IGraphicEntity.hpp"
#include "Game/CModel.hpp"


namespace TE
{

/**
 * \class   CModelEntity
 * \ingroup Entities
 * \brief   Entité associée à un modèle.
 ******************************/

class TENGINE2_API CModelEntity : public IGraphicEntity
{
public:

    // Constructeurs et destructeur
    CModelEntity(IModelData * modelData, IEntity * parent, const CString& name = CString());
    CModelEntity(IModelData * modelData, CMap * map, const CString& name = CString());
    virtual ~CModelEntity() = default;

    // Accesseurs
    float getMass() const;
    bool isMovable() const;
    unsigned int getSequence() const;
    CModel::TModelSeqMode getSeqMode() const;
    unsigned int getSkin() const;
    unsigned int getGroup() const;
    float getControllerValue(unsigned int controller) const;
    float getBlendingValue(unsigned int blender) const;

    // Mutateurs
    void setMass(float mass);
    void setMovable(bool movable = true);
    void setSequence(unsigned int sequence);
    void setSequenceByName(const CString& name);
    void setSeqMode(CModel::TModelSeqMode mode);
    void setSkin(unsigned int skin);
    void setGroup(unsigned int group);
    void setControllerValue(unsigned int controller, float value);
    void setBlendingValue(unsigned int blender, float value);

    //virtual void initEntity() override;
    virtual void updateEntity(float frameTime) override;
    virtual void renderEntity(CRenderParams * params) override;

protected:

    // Données protégées
    std::unique_ptr<CModel> m_model;
};

} // Namespace TE
