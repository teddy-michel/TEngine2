/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "Entities/IGraphicEntity.hpp"


namespace TE
{

class CCylinder;


/**
 * \class   CRope
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer une corde.
 *
 * \todo Attacher la corde � deux entit�s physiques avec une matrice de transformation.
 ******************************/

class TENGINE2_API CRope : public IGraphicEntity
{
public:

    explicit CRope(IEntity * parent, const CString& name = CString{});
    explicit CRope(CMap * map, const CString& name = CString{});
    virtual ~CRope() = default;

    //virtual void renderEntity(const CRenderParams& params) override;

    inline float getLength() const noexcept;
    void setLength(float length);
    inline float getRadius() const noexcept;
    void setRadius(float radius);
    inline unsigned int getSegments() const noexcept;
    void setSegments(unsigned int segments);
    inline float getMass() const noexcept;
    void setMass(float mass);
    inline glm::vec3 getAttachment() const noexcept;
    void setAttachment(const glm::vec3& position);

    virtual void initEntity() override;

private:

    void updateBoxes();

    float m_length;          ///< Longueur de la corde.
    float m_radius;          ///< Rayon de la corde.
    unsigned int m_segments; ///< Nombre de segments.
    float m_mass;            ///< Masse de la corde.
    glm::vec3 m_attachment;  ///< Point fixe d'attache. \todo Use PhysicalEntity
    bool m_init;
    std::vector<CCylinder *> m_boxes; ///< \todo: s�parer les acteurs physiques et le rendu.
};


inline float CRope::getLength() const noexcept
{
    return m_length;
}


inline float CRope::getRadius() const noexcept
{
    return m_radius;
};


inline unsigned int CRope::getSegments() const noexcept
{
    return m_segments;
}


inline float CRope::getMass() const noexcept
{
    return m_mass;
}


inline glm::vec3 CRope::getAttachment() const noexcept
{
    return m_attachment;
};

} // Namespace TE
