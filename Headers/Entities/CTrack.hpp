/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "IEntity.hpp"


namespace TE
{

class CTrackPoint;


/**
 * \class   CTrack
 * \ingroup Entities
 * \brief   Cette entit� permet de d�finir un chemin � suivre.
 *
 * Le chemin poss�de une liste de points qui sont parcouru dans l'ordre, ce qui modifie la
 * matrice de transformation locale de l'entit�.
 * Les entit�s enfant sont donc automatiquement d�plac�es.
 * Il est possible d'utiliser un m�me point plusieurs fois dans un chemin.
 *
 * Attention : les points du chemin (repr�sent�s par une entit� CTrackPoint) ne doivent pas �tre
 * d�finis comme entit� enfant du chemin, sinon leur position et leur orientation serait
 * constament modifi�es.
 *
 * La matrice de transformation locale de cette entit� est calcul�e en faisant une interpolation
 * entre le dernier point franchi et le suivant.
 * Il est donc inutile d'essayer de d�placer manuellement une entit� CTrack.
 ******************************/

class TENGINE2_API CTrack : public IEntity
{
public:

    explicit CTrack(IEntity * parent, const CString& name = CString{});
    explicit CTrack(CMap * map, const CString& name = CString{});
    virtual ~CTrack() = default;

    std::vector<CTrackPoint *> getPoints() const noexcept;
    void addPoint(CTrackPoint * point, int position = -1);

    inline bool isRunning() const noexcept { return m_running; }
    void start();
    void stop();
    void reset();
    inline float getSpeed() const noexcept { return m_speed; }
    void setSpeed(float speed);
    inline bool isLoop() const { return m_loop; }
    void setLoop(bool loop);

    virtual void updateEntity(float frameTime) override;

private:

    bool m_running;  ///< Indique si le d�placement est actif ou non.
    bool m_loop;     ///< Indique si le chemin forme une boucle (le dernier point am�ne vers le premier point).
    int m_lastPoint; ///< Indice du dernier point franchi (le premier point a l'indice 0).
    float m_delta;   ///< Pourcentage de d�placement accompli entre le dernier point et le suivant (entre 0 et 1).
    float m_speed;   ///< Vitesse de d�placement, en cm/s (100 par d�faut).
    std::vector<CTrackPoint *> m_points; ///< Liste des points du chemin.
};

} // Namespace TE
