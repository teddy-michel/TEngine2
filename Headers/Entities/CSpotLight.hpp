/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IDirectionalLight.hpp"


namespace TE
{

/**
 * \class   CSpotLight
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer une lumi�re dynamique de type spot (faisceau).
 ******************************/

class TENGINE2_API CSpotLight : public IDirectionalLight
{
public:

    explicit CSpotLight(IEntity * parent, const CString& name = CString{});
    explicit CSpotLight(CMap * map, const CString& name = CString{});
    virtual ~CSpotLight() = default;

    inline virtual TLightType getLightType() const noexcept override;

    inline float getInnerAngle() const noexcept;
    void setInnerAngle(float angle);

    inline float getOuterAngle() const noexcept;
    void setOuterAngle(float angle);

    inline float getAnglesRatio() const noexcept;

    inline float getMaxDistance() const noexcept;
    void setMaxDistance(float distance);

    virtual glm::mat4 getViewMatrix(int face = 0) const noexcept override;
    inline virtual glm::mat4 getProjectionMatrix() const noexcept override;

    virtual bool loadFromJson(const nlohmann::json& json) override;

private:

    float m_innerAngle;     ///< Angle int�rieur du faisceau.
    float m_outerAngle;     ///< Angle ext�rieur du faisceau.
    float m_maxDistance;    ///< Distance maximale du faisceau.
    glm::mat4 m_projection; ///< Matrice de projection.
};


inline TLightType CSpotLight::getLightType() const noexcept
{
    return TLightType::Spot;
}


inline float CSpotLight::getInnerAngle() const noexcept
{
    return m_innerAngle;
}


inline float CSpotLight::getOuterAngle() const noexcept
{
    return m_outerAngle;
}


inline float CSpotLight::getAnglesRatio() const noexcept
{
    if (m_innerAngle < m_outerAngle)
    {
        return (m_innerAngle / m_outerAngle);
    }
    else
    {
        return 1.0f;
    }
}


inline float CSpotLight::getMaxDistance() const noexcept
{
    return m_maxDistance;
}


inline glm::mat4 CSpotLight::getProjectionMatrix() const noexcept
{
    return m_projection;
}

} // Namespace TE
