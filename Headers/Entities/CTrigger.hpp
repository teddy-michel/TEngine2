/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "IEntity.hpp"


namespace TE
{

class CStaticActor;


/**
 * \class   CTrigger
 * \ingroup Entities
 * \brief   Classe de base de triggers.
 *
 * Les triggers sont des volumes invisibles qui d�clenchent un �v�nement
 * lorsqu'une entit� les traverse.
 *
 * \todo Pouvoir utiliser n'importe quelle shape.
 ******************************/

class TENGINE2_API CTrigger : public IEntity
{
    friend class CScene; // pour acc�der � onTrigger.

public:

    explicit CTrigger(IEntity * parent, const CString& name = CString{});
    explicit CTrigger(CMap * map, const CString& name = CString{});
    virtual ~CTrigger();

    glm::vec3 getSize() const;
    void setSize(const glm::vec3& size);

    virtual void updateEntity(float frameTime) override;

    virtual bool loadFromJson(const nlohmann::json& json) override;

private:

    /**
     * \struct  TTriggerEntity
     * \ingroup Entities
     * \brief   Description d'une entit� qui se trouve � l'int�rieur d'un trigger.
     ******************************/

    struct TTriggerEntity
    {
        IEntity * entity; ///< Pointeur sur l'entit�.
        float time;       ///< Dur�e pass�e dans le trigger en secondes.

        /// Constructeur par d�faut.
        TTriggerEntity(IEntity * entity = nullptr, float time = 0.0f) :
            entity{ entity }, time{ time } { }
    };

    typedef std::list<TTriggerEntity> TTriggerEntityList;

    void updateShape();
    virtual void notifyWorldMatrixChange() override;
    void onTrigger(IEntity * entity, bool enter);

    glm::vec3 m_size;               ///< Dimensions du trigger.
    std::shared_ptr<CStaticActor> m_actor;
    TTriggerEntityList m_entities;  ///< Liste des entit�s � l'int�rieur du trigger.
};

} // Namespace TE
