/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CColor.hpp"


namespace TE
{

enum class TENGINE2_API TLightType
{
    Unknown     = 0,
    Spot        = 1,
    Point       = 2,
    Directional = 3,
};


/**
 * \class   ILight
 * \ingroup Entities
 * \brief   Entit� de base permettant de g�rer une lumi�re dynamique.
 ******************************/

class TENGINE2_API ILight : public IGraphicEntity
{
public:

    explicit ILight(IEntity * parent, const CString& name = CString{});
    explicit ILight(CMap * map, const CString& name = CString{});
    virtual ~ILight();

    virtual TLightType getLightType() const noexcept = 0;

    virtual glm::mat4 getViewMatrix(int face = 0) const noexcept = 0;
    virtual glm::mat4 getProjectionMatrix() const noexcept = 0;

    void setColor(const CColor& color);
    inline CColor getColor() const noexcept;

    void setIntensity(float intensity);
    inline float getIntensity() const noexcept;

    void setEnabled(bool enabled);
    inline bool isEnabled() const noexcept;

    virtual bool loadFromJson(const nlohmann::json& json) override;

private:

    CColor m_color;    ///< Couleur de la lumi�re.
    float m_intensity; ///< Intensit� de la lumi�re (entre 0 et 1).
    bool m_enabled;    ///< Indique si la lumi�re est active ou non.
    // TODO: param�tres de baisse d'intensit� avec la distance ?
};


inline CColor ILight::getColor() const noexcept
{
    return m_color;
}


inline float ILight::getIntensity() const noexcept
{
    return m_intensity;
}


inline bool ILight::isEnabled() const noexcept
{
    return m_enabled;
}

} // Namespace TE
