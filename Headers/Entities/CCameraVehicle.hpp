/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <SFML/Window.hpp>

#include "Entities/CCamera.hpp"


namespace TE
{

/**
 * \class   CCameraVehicle
 * \ingroup Entities
 * \brief   Gestion d'une cam�ra qui suit un v�hicule (ou n'importe quelle entit� en mouvement).
 *
 * \todo TESTER !
 ******************************/

class TENGINE2_API CCameraVehicle : public CCamera
{
public:

    explicit CCameraVehicle(IEntity * parent, const CString& name = CString{});
    explicit CCameraVehicle(CMap * map, const CString& name = CString{});
    virtual ~CCameraVehicle() = default;

    virtual void updateEntity(float frameTime) override;

private:

	float			mRotateInputY;
	float			mRotateInputZ;
	float			mMaxCameraRotateSpeed;
	glm::vec3 mLastCarPos;
	glm::vec3 mLastCarVelocity;
	bool mCameraInit;
	float			mCameraRotateAngleY;
	float			mCameraRotateAngleZ;
	float			mCamDist;
};

} // Namespace TE
