/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Entities/IEntity.hpp"


namespace TE
{

class CShader;


/**
 * \class   IGraphicEntity
 * \ingroup Entities
 * \brief   Classe de base des entit�s graphiques (c'est-�-dire li�e � un shader).
 ******************************/

class TENGINE2_API IGraphicEntity : public IEntity
{
public:

    explicit IGraphicEntity(IEntity * parent, const CString& name = CString{});
    explicit IGraphicEntity(CMap * map, const CString& name = CString{});
    virtual ~IGraphicEntity() = 0;

    inline std::shared_ptr<CShader> getShader() const noexcept;
    void setShader(const std::shared_ptr<CShader>& shader);

    //virtual float getBoundingRadius() const noexcept = 0;

    virtual bool loadFromJson(const nlohmann::json& json) override;

    inline bool hasMoved() const noexcept;

protected:

    virtual void notifyWorldMatrixChange() override;

    bool m_hasMoved; ///< Indique si l'entit� a boug� ou si sa forme a chang�.

private:

    std::shared_ptr<CShader> m_shader; ///< Shader.
};


inline std::shared_ptr<CShader> IGraphicEntity::getShader() const noexcept
{
    return m_shader;
}


inline bool IGraphicEntity::hasMoved() const noexcept
{
    return m_hasMoved;
}

} // Namespace TE
