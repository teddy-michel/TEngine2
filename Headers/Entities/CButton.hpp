/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Entities/CBox.hpp"


namespace TE
{

/**
 * \class   CButton
 * \ingroup Entities
 * \brief   Entit� permettant de g�rer un bouton (interrupteur).
 *
 * Pour l'instant l'entit� h�rite de CBox pour le debug.
 ******************************/

class TENGINE2_API CButton : public CBox
{
public:

    explicit CButton(IEntity * parent, const CString& name = CString{});
    explicit CButton(CMap * map, const CString& name = CString{});
    virtual ~CButton() = default;

    virtual bool checkInteraction(CPlayer * player, float distance) override;
    virtual bool interact(CPlayer * player) override;

    void push();

    // TODO: Lua connection
    //void signal_onPush(void * fct);

private:

    // TODO
    //void * m_signal_onPush;
};

} // Namespace TE
