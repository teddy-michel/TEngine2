/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <SFML/Window.hpp>

#include "Entities/CCamera.hpp"
#include "Game/IInputReceiver.hpp"


namespace TE
{

/**
 * \class   CCameraFreeFly
 * \ingroup Entities
 * \brief   Gestion d'une cam�ra libre.
 ******************************/

class TENGINE2_API CCameraFreeFly : public CCamera, public IInputReceiver
{
public:

    explicit CCameraFreeFly(IEntity * parent, const CString& name = CString{});
    explicit CCameraFreeFly(CMap * map, const CString& name = CString{});
    virtual ~CCameraFreeFly() = default;

    virtual void updateEntity(float frameTime) override;

    virtual void onEvent(const CMouseEvent& event) override;

private:

    void updateCameraDirection(bool needUpdateWorldMatrix);

    float m_theta;
    float m_phi;
};

} // Namespace TE
