/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstdlib>

#include "Graphic/CShader.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

template<typename T>
IBuffer<T>::IBuffer() :
    IBufferBase {},
    m_data      {}
{

}


template<typename T>
std::vector<T> IBuffer<T>::getData() const
{
    return m_data;
}


template<typename T>
std::size_t IBuffer<T>::getDataCount() const
{
    return m_data.size();
}


template<typename T>
std::size_t IBuffer<T>::getDataSize() const
{
    return m_data.size() * sizeof(T);
}


/**
 * D�finit l'adresse du buffer de donn�es et sa taille.
 *
 * \param data Liste des sommets.
 ******************************/

template<typename T>
void IBuffer<T>::setData(const std::vector<T>& data)
{
    m_data = data;
}


/**
 * Supprime toutes les donn�es.
 ******************************/

template<typename T>
void IBuffer<T>::clear()
{
    IBufferBase::clear();
    m_data.clear();
}


template<typename T>
void IBuffer<T>::updateAttributeId()
{
    if (m_indices.size() > 0)
    {
        gRenderer->unrefVertexData(m_dataRefs);
        m_dataRefs.clear();

        std::size_t offset = 0;

        for (const auto& texture : m_textures)
        {
            TVertexData vertexData{ m_entityId };
            for (std::size_t i = 0; i < NumUTUsed; ++i)
            {
                vertexData.textures[i] = texture.texture[i];
            }

            auto v = gRenderer->getVertexDataRef(vertexData);
            m_dataRefs.push_back(v);

            for (std::size_t i = 0; i < texture.nbr; ++i)
            {
                assert(m_indices[offset + i] < m_data.size());

                // TODO: check that "id" exists and is valid (integer, 32 bits)
                m_data[m_indices[offset + i]].id = v;
            }

            offset += texture.nbr;
        }
    }
}

} // Namespace TE
