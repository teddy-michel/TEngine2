/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Graphic/IBuffer.hpp"


namespace TE
{

template<typename T> class CMegaBuffer;


/**
 * \class   CSubBuffer
 * \ingroup Graphic
 * \brief   Un sous buffer stock� dans un CMegaBuffer.
 ******************************/

template<typename T>
class /*TENGINE2_API*/ CSubBuffer : public IBuffer<T>
{
    friend class CMegaBuffer<T>; // pour acc�der � m_data

public:

    // Constructeurs et destructeur
    explicit CSubBuffer(CMegaBuffer<T> * megaBuffer);
    virtual ~CSubBuffer();
    CSubBuffer(const CSubBuffer&) = delete;
    CSubBuffer(CSubBuffer&&) = delete;
    CSubBuffer& operator=(const CSubBuffer&) = delete;
    CSubBuffer& operator=(CSubBuffer&&) = delete;

    virtual bool isSubBuffer() const noexcept;

    virtual unsigned int draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const override;
    virtual void update(bool updateId = true) override;

private:

    // Donn�es priv�es
    CMegaBuffer<T> * const m_megaBuffer;
};

} // Namespace TE

#include "Graphic/CSubBuffer.tpl.hpp"
