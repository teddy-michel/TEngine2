/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <vector>
#include <glm/glm.hpp>

#include "Graphic/IBuffer.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/opengl.h"


namespace TE
{

class CShader;


/**
 * \class   CBuffer
 * \ingroup Graphic
 * \brief   Gestion des buffers de sommets de la carte graphique.
 *
 * Un buffer est une partie de la m�moire de la carte graphique qui permet de stocker des donn�es
 * n�cessaire � l'affichage. Ces donn�es peuvent �tre des informations sur les sommets (position,
 * coordonn�es de texture, vecteur normal, couleur, etc.) ou sur n'importe quel param�tre
 * utilis� dans un shader.
 * Les sommets sont r�f�renc�s par des indices, et un buffer contient une liste de textures �
 * utiliser pour chaque indice.
 ******************************/

template<typename T>
class /*TENGINE2_API*/ CBuffer : public IBuffer<T>
{
public:

    // Constructeurs et destructeur
    explicit CBuffer(unsigned int count = 0);
    ~CBuffer();
    CBuffer(const CBuffer&) = delete;
    CBuffer(CBuffer&&) = delete;
    CBuffer& operator=(const CBuffer&) = delete;
    CBuffer& operator=(CBuffer&&) = delete;

    virtual unsigned int draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const override;
    virtual void update(bool updateId = true) override;
    void updateSubData(const std::vector<T>& data, unsigned int offset, int count = -1);

private:

    void optimizeTextures();
    void updateAttributes(const std::shared_ptr<CShader>& shader) const;

    // Donn�es priv�es
    std::size_t m_sizeGPU;               ///< Taille des donn�es envoy�es � la carte graphique.
    GLuint m_vao;                        ///< Identifiant du Vertex Array Object. (TODO: move to impl)
    GLuint m_vertexBuffer;               ///< Identifiant du Vertex Buffer Object. (TODO: move to impl)
    GLuint m_indexBuffer;                ///< Identifiant du Index Buffer Object. (TODO: move to impl)
    mutable std::vector<std::shared_ptr<CShader>> m_shaders;
};

} // Namespace TE

#include "Graphic/CBuffer.tpl.hpp"
