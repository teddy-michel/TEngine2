/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CMegaBuffer.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param megaBuffer Pointeur sur le m�ga buffer contenant ce sous-buffer (non nul).
 ******************************/

template<typename T>
CSubBuffer<T>::CSubBuffer(CMegaBuffer<T> * megaBuffer) :
    IBuffer      {},
    m_megaBuffer { megaBuffer }
{
    assert(m_megaBuffer != nullptr);
    gApplication->log(CString("Create sub buffer (0x%2).").arg(CString::fromPointer(this)), ILogger::Debug);
    m_megaBuffer->addSubBuffer(this);
}


/**
 * Destructeur.
 ******************************/

template<typename T>
CSubBuffer<T>::~CSubBuffer()
{
    gApplication->log(CString("Delete sub buffer (0x%2).").arg(CString::fromPointer(this)), ILogger::Debug);
    m_megaBuffer->removeSubBuffer(this);
}


template<typename T>
bool CSubBuffer<T>::isSubBuffer() const noexcept
{
    return true;
}


/**
 * Dessine le contenu du buffer.
 * Doit �tre appel�e depuis le thread principal.
 *
 * \param displayMode Mode de rendu. \todo R�cup�rer les param�tres depuis le CRenderer.
 * \param shader Shader � utiliser pour l'affichage (non nul).
 * \return Nombre de surfaces dessin�s.
 ******************************/

template<typename T>
unsigned int CSubBuffer<T>::draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const
{
    std::vector<const IBufferBase *> buffers(1);
    buffers.push_back(this);
    return m_megaBuffer->draw(buffers, displayMode, shader);
}


/**
 * Met � jour les buffers.
 * Doit �tre appel�e depuis le thread principal.
 *
 * \param updateId Indique si on doit mettre-�-jour l'attribut "id" de chaque sommet.
 ******************************/

template<typename T>
void CSubBuffer<T>::update(bool updateId)
{
    if (updateId)
    {
        updateAttributeId();
    }

    m_megaBuffer->updateSubBuffer(this);
}

} // Namespace TE
