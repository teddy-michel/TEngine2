/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "Graphic/IBufferBase.hpp"


namespace TE
{

/**
 * \class   IBuffer
 * \ingroup Graphic
 * \brief   Interface des buffers de la carte graphique.
 *
 * Le param�tre template T d�crit la structure d'un sommet (voir VertexFormat.hpp).
 * Cette structure doit comporter au moins le champ "id" de type uint32_t.
 ******************************/

template<typename T>
class /*TENGINE2_API*/ IBuffer : public IBufferBase
{
public:

    // Constructeurs et destructeur
    IBuffer();
    virtual ~IBuffer() = default;
    IBuffer(const IBuffer&) = delete;
    IBuffer(IBuffer&&) = delete;
    IBuffer& operator=(const IBuffer&) = delete;
    IBuffer& operator=(IBuffer&&) = delete;

    // Data
    std::vector<T> getData() const;
    std::size_t getDataCount() const;
    std::size_t getDataSize() const;
    void setData(const std::vector<T>& data);

    virtual void clear() override;

protected:

    void updateAttributeId();

    // Donn�es prot�g�es
    std::vector<T> m_data; ///< Donn�es � envoyer � la carte graphique.
};

} // Namespace TE

#include "Graphic/IBuffer.tpl.hpp"
