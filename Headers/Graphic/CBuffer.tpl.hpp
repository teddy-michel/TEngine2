/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstdlib>
#include <algorithm>

#include "Graphic/CShader.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 *
 * \param count Taille initiale du buffer.
 ******************************/

template<typename T>
CBuffer<T>::CBuffer(unsigned int count) :
    IBuffer             {},
    m_sizeGPU           { 0 },
    m_vao               { 0 },
    m_vertexBuffer      { 0 },
    m_indexBuffer       { 0 },
    m_shaders           {}
{
    // Create Vertex Array Object
    glGenVertexArrays(1, &m_vao);
    checkOpenGLError("glGenVertexArrays", __FILE__, __LINE__);
    glBindVertexArray(m_vao);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);

    // Cr�ation des buffers
    glGenBuffers(1, &m_vertexBuffer);
    checkOpenGLError("glGenBuffers", __FILE__, __LINE__);
    glGenBuffers(1, &m_indexBuffer);
    checkOpenGLError("glGenBuffers", __FILE__, __LINE__);

    // Pour le DEBUG
    glObjectLabel(GL_VERTEX_ARRAY, m_vao, -1, CString("Vertex Array #%1").arg(m_vao).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    glObjectLabel(GL_BUFFER, m_vertexBuffer, -1, CString("Vertex Buffer #%1 (VAO #%2)").arg(m_vertexBuffer).arg(m_vao).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, m_indexBuffer);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    glObjectLabel(GL_BUFFER, m_indexBuffer, -1, CString("Index Buffer #%1 (VAO #%2)").arg(m_indexBuffer).arg(m_vao).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    // Taille initiale
    if (count > 0)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

        glBufferData(GL_ARRAY_BUFFER, count * sizeof(T), nullptr, GL_STATIC_DRAW);
        checkOpenGLError("glBufferData", __FILE__, __LINE__);
        m_sizeGPU = count * sizeof(T);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    }

    glBindVertexArray(0);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);
}


/**
 * D�truit les buffers en m�moire graphique.
 ******************************/

template<typename T>
CBuffer<T>::~CBuffer()
{
    gApplication->log(CString("Delete buffer (0x%2).").arg(CString::fromPointer(this)), ILogger::Debug);

    gRenderer->unrefVertexData(m_dataRefs);
    m_dataRefs.clear();

    glDeleteBuffers(1, &m_indexBuffer);
    checkOpenGLError("glDeleteBuffers", __FILE__, __LINE__);
    glDeleteBuffers(1, &m_vertexBuffer);
    checkOpenGLError("glDeleteBuffers", __FILE__, __LINE__);

    glDeleteVertexArrays(1, &m_vao);
    checkOpenGLError("glDeleteVertexArrays", __FILE__, __LINE__);
}


/**
 * Dessine le contenu du buffer.
 * Doit �tre appel�e depuis le thread principal.
 *
 * \param displayMode Mode de rendu. \todo R�cup�rer les param�tres depuis le CRenderer.
 * \param shader Shader � utiliser pour l'affichage (non nul).
 * \return Nombre de surfaces dessin�s.
 *
 * \todo Prendre en argument un CRenderParams
 ******************************/

template<typename T>
unsigned int CBuffer<T>::draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const
{
    assert(shader != nullptr);

    if (m_vertexBuffer == 0 || m_indexBuffer == 0)
    {
        return 0;
    }

    const unsigned int numIndices = static_cast<unsigned int>(m_indices.size());

    // Buffer
    glBindVertexArray(m_vao);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);

    // Le shader n'a pas encore �t� utilis� par ce buffer
    if (std::find(m_shaders.begin(), m_shaders.end(), shader) == m_shaders.end())
    {
        m_shaders.push_back(shader);
        updateAttributes(shader);
    }

    // Activation du shader
    shader->bind();

    if (numIndices > 0)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    }

    unsigned int nbr = 0; // Nombre de triangles dessin�s
    unsigned int offset = 0;

    switch (displayMode)
    {
        default:
        case Normal:
        {
            glEnable(GL_CULL_FACE); // TODO: move?
            checkOpenGLError("glEnable", __FILE__, __LINE__);

            if (!shader->useBindlessTexture() && m_textures.size() > 0)
            {
                // Pour chaque texture diff�rente
                for (const auto& it : m_textures)
                {
                    // Chargement des textures
                    for (unsigned int i = 0; i < NumUTUsed; ++i)
                    {
                        gTextureManager->bindTexture(it.texture[i], i);
                    }

                    // Affichage des primitives
                    glDrawElements(m_primitive, it.nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));
                    checkOpenGLError("glDrawElements", __FILE__, __LINE__);

                    nbr += it.nbr;
                }
            }
            else
            {
                // On n'utilise pas les textures
                gTextureManager->bindTexture(0, 0);

                // Affichage des primitives
                if (numIndices > 0)
                {
                    glDrawElements(m_primitive, numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
                    checkOpenGLError("glDrawElements", __FILE__, __LINE__);

                    nbr += numIndices;
                }
                else
                {
                    // TODO
                }
            }

            glDisable(GL_CULL_FACE);
            checkOpenGLError("glDisable", __FILE__, __LINE__);

            break;
        }

        // Affichage des textures sans les lightmaps
        case Textured:
        {
            glEnable(GL_CULL_FACE);
            checkOpenGLError("glEnable", __FILE__, __LINE__);

            if (!shader->useBindlessTexture() && m_textures.size() > 0)
            {
                // Pour chaque texture diff�rente
                for (const auto& it : m_textures)
                {
                    // Chargement des textures
                    gTextureManager->bindTexture(it.texture[0], 0);

                    // Affichage des primitives
                    glDrawElements(m_primitive, it.nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));
                    checkOpenGLError("glDrawElements", __FILE__, __LINE__);

                    nbr += it.nbr;
                }
            }
            else
            {
                gTextureManager->bindTexture(0, 0);

                // Affichage des primitives
                glDrawElements(m_primitive, numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
                checkOpenGLError("glDrawElements", __FILE__, __LINE__);

                nbr += numIndices;
            }

            glDisable(GL_CULL_FACE);
            checkOpenGLError("glDisable", __FILE__, __LINE__);

            break;
        }

        // Affichage des lightmaps
        case Lightmaps:
        {
            glEnable(GL_CULL_FACE);
            checkOpenGLError("glEnable", __FILE__, __LINE__);

            if (!shader->useBindlessTexture() && m_textures.size() > 0)
            {
                gTextureManager->bindTexture(0, 0);

                // Pour chaque texture diff�rente
                for (const auto& it : m_textures)
                {
                    // Chargement des textures
                    gTextureManager->bindTexture(it.texture[1], 1);

                    // Affichage des primitives
                    glDrawElements(m_primitive, it.nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));
                    checkOpenGLError("glDrawElements", __FILE__, __LINE__);

                    nbr += it.nbr;
                }
            }
            else
            {
                // On n'utilise pas les textures
                gTextureManager->bindTexture(0, 0);

                // Affichage des primitives
                glDrawElements(m_primitive, numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
                checkOpenGLError("glDrawElements", __FILE__, __LINE__);

                nbr += numIndices;
            }

            glDisable(GL_CULL_FACE);
            checkOpenGLError("glDisable", __FILE__, __LINE__);

            break;
        }

        // Affichage des couleurs
        case Colored:
        {
            glEnable(GL_CULL_FACE);
            checkOpenGLError("glEnable", __FILE__, __LINE__);

            // On n'utilise pas les textures
            gTextureManager->bindTexture(0, 0);

            // Affichage des primitives
            glDrawElements(m_primitive, numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
            checkOpenGLError("glDrawElements", __FILE__, __LINE__);

            nbr += numIndices;

            glDisable(GL_CULL_FACE);
            checkOpenGLError("glDisable", __FILE__, __LINE__);

            break;
        }

        // Mode fil de fer
        case Wireframe:
        {
            gTextureManager->bindTexture(0, 0);
/*
            if (shader == nullptr)
            {
                glColor4ub(255, 255, 255, 255);
                checkOpenGLError("glColor4ub", __FILE__, __LINE__);
            }
*/
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            checkOpenGLError("glPolygonMode", __FILE__, __LINE__);

            // Affichage des triangles
            glDrawElements(m_primitive, numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
            checkOpenGLError("glDrawElements", __FILE__, __LINE__);

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            checkOpenGLError("glPolygonMode", __FILE__, __LINE__);

            nbr += numIndices;
            break;
        }

        // Mode fil de fer avec culling
        case WireframeCulling:
        {
            glEnable(GL_CULL_FACE);
            checkOpenGLError("glEnable", __FILE__, __LINE__);

            gTextureManager->bindTexture(0, 0);
/*
            if (shader == nullptr)
            {
                glColor4ub(255, 255, 255, 255);
                checkOpenGLError("glColor4ub", __FILE__, __LINE__);
            }
*/
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            checkOpenGLError("glPolygonMode", __FILE__, __LINE__);

            // Affichage des triangles
            glDrawElements(m_primitive, numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
            checkOpenGLError("glDrawElements", __FILE__, __LINE__);

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            checkOpenGLError("glPolygonMode", __FILE__, __LINE__);

            glDisable(GL_CULL_FACE);
            checkOpenGLError("glDisable", __FILE__, __LINE__);

            nbr += numIndices;
            break;
        }
    }

    if (numIndices > 0)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    }

    glBindVertexArray(0);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);

    if (m_primitive == PrimLine || m_primitive == PrimPoint)
    {
        nbr = 0;
    }

    return nbr;
}


template<typename T>
void CBuffer<T>::optimizeTextures()
{
    unsigned int prevTexture = 0; // Identifiant de la derni�re texture du tableau
    bool first = false;

    // On parcourt le tableau de textures
    for (TBufferTextureVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        // La texture de cette case est la m�me que celle de la case pr�c�dente, donc on les fusionne
        if (first && it->texture[0] == prevTexture)
        {
            unsigned int nbr = it->nbr; // On conserve le nombre de faces � afficher pour cette texture
            it = m_textures.erase(it); // On supprime la case actuelle du tableau

            // On modifie la case pr�c�dente du tableau
            --it;
            it->nbr += nbr;
        }
        else
        {
            prevTexture = it->texture[0];
            first = true;
        }
    }
}


/**
 * Met � jour les buffers.
 * Doit �tre appel�e depuis le thread principal.
 *
 * \param updateId Indique si on doit mettre-�-jour l'attribut "id" de chaque sommet.
 ******************************/

template<typename T>
void CBuffer<T>::update(bool updateId)
{
    //gApplication->log("Update CBuffer.", ILogger::Debug);

    // Optimisation du tableau de textures
    optimizeTextures();

    // Modification du VBO
    glBindVertexArray(m_vao);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);

    if (updateId)
    {
        updateAttributeId();
    }

    const auto size = getDataSize();

    if (size > 0)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

        if (size <= m_sizeGPU)
        {
            // Keep old buffer
            glBufferSubData(GL_ARRAY_BUFFER, 0, size, &m_data[0]);
            checkOpenGLError("glBufferSubData", __FILE__, __LINE__);
        }
        else
        {
            glBufferData(GL_ARRAY_BUFFER, size, &m_data[0], GL_STATIC_DRAW);
            checkOpenGLError("glBufferData", __FILE__, __LINE__);
            m_sizeGPU = size;
        }

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    }

    const unsigned int numIndices = static_cast<unsigned int>(m_indices.size());

    // Modification du IBO
    if (numIndices > 0)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);
        checkOpenGLError("glBufferData", __FILE__, __LINE__);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    }

    glBindVertexArray(0);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);

    // On n'a plus besoin des donn�es.
    m_data.clear();
}


template<typename T>
void CBuffer<T>::updateSubData(const std::vector<T>& data, unsigned int offset, int count)
{
    // Modification du VBO
    glBindVertexArray(m_vao);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);

    if (count < 0)
    {
        count = static_cast<int>(data.size());
    }

    if (count == 0)
    {
        return; // Nothing to do
    }

    const unsigned int newSize = (offset + count) * sizeof(T);
    if (newSize > m_sizeGPU)
    {
        count = (m_sizeGPU / sizeof(T)) - offset;
    }

    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    glBufferSubData(GL_ARRAY_BUFFER, offset * sizeof(T), count * sizeof(T), &data[0]);
    checkOpenGLError("glBufferSubData", __FILE__, __LINE__);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    glBindVertexArray(0);
    checkOpenGLError("glBindVertexArray", __FILE__, __LINE__);
}

} // Namespace TE
