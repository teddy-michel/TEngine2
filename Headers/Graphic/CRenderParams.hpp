/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>
#include <memory>
#include <vector>

#include "Core/CString.hpp"
#include "Entities/CCamera.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/IBuffer.hpp"


namespace TE
{

class CMap;
class CShader;
class CShadowMap;


enum class TRenderPass
{
    Primary,     ///< Rendu primaire.
    Secondary,   ///< Rendu secondaire (via un �cran ou un miroir).
    ShadowMap,   ///< Rendu des maps d'ombrage.
    //Transparency ///< Rendu des objets transparents.
};


/**
 * \class   CRenderParams
 * \ingroup Graphic
 * \brief   Gestion d'une passe de rendu.
 ******************************/

class TENGINE2_API CRenderParams
{
public:

    CRenderParams(CMap * map, const glm::vec3& position, const glm::mat4& view, const glm::mat4& projection);
    CRenderParams(CMap * map, CCamera * camera);
    CRenderParams(CMap * map, CCamera * camera, const glm::mat4& view, const glm::mat4& projection);
    ~CRenderParams() = default;

    inline CMap * getMap() const noexcept;
    inline CCamera * getCamera() const noexcept;
    inline glm::mat4 getView() const noexcept;
    inline glm::mat4 getProjection() const noexcept;
    inline TRenderPass getType() const noexcept;
    inline TDisplayMode getMode() const noexcept;
    inline std::shared_ptr<CShader> getShader() const noexcept;

    void setType(TRenderPass type);
    void setMode(TDisplayMode mode);
    void setShader(const std::shared_ptr<CShader>& shader);
    void setShadowMap(CShadowMap * shadowMap);
    void setViewport(int x, int y, int width, int height);
    void setClearColor(float red, float green, float blue, float alpha);

    void addBuffer(IBufferBase * buffer, const std::shared_ptr<CShader>& shader, const glm::mat4& world);

    void render(const CString& name = CString());

private:

    struct TRenderBuffer
    {
        IBufferBase * buffer;
        std::shared_ptr<CShader> shader;
        glm::mat4 world;
        float distance;
        // TODO: AABB
        // TODO: entity

        TRenderBuffer(IBufferBase * b, const std::shared_ptr<CShader>& s, const glm::mat4& w, float d) :
            buffer      { b },
            shader      { s },
            world       { w },
            distance    { d }
        {
            assert(buffer != nullptr);
        }

        inline bool operator<(const TRenderBuffer& other) const
        {
            if (buffer->isTransparent())
            {
                if (other.buffer->isTransparent())
                {
                    return distance < other.distance;
                }
                else
                {
                    return true;
                }
            }
            else if (other.buffer->isTransparent())
            {
                return false;
            }
            else if (shader == other.shader)
            {
                return distance < other.distance;
            }
            else
            {
                return shader < other.shader;
            }
        }
    };

    void renderOpaqueBuffers();
    void renderTransparentBuffers();
    void renderBuffer(const TRenderBuffer& buffer);
    void renderMegaBuffer(const std::shared_ptr<CMegaBuffer<TVertex3D>>& megaBuffer, const std::vector<const IBufferBase *>& subBuffers, const std::shared_ptr<CShader>& shader);

    CMap * m_map;                         ///< Map en cours d'affichage.
    CCamera * m_camera;                   ///< Cam�ra utilis�e pour le rendu.
    glm::vec3 m_position;                 ///< Position de la source (cam�ra ou lumi�re).
    glm::mat4 m_view;                     ///< Matrice de vue.
    glm::mat4 m_projection;               ///< Matrice de projection.
    TRenderPass m_type;                   ///< Type de passe de rendu.
    TDisplayMode m_mode;                  ///< Mode d'affichage.
    std::shared_ptr<CShader> m_shader;    ///< Shader � utiliser pour le rendu.
    CShadowMap * m_shadowMap;             ///< Conteneur pour les shadow maps.
    std::vector<TRenderBuffer> m_buffers; ///< Liste des buffers � afficher.
    std::vector<std::shared_ptr<CShader>> m_shaders; ///< Liste des shaders utilis�s pour l'affichage.
    glm::ivec4 m_viewport;                ///< Dimensions du viewport.
    glm::vec4 m_clearColor;               ///< Couleur utilis�e pour effacer l'�cran.
    // TODO: culling near / far
};


inline CMap * CRenderParams::getMap() const noexcept
{
    return m_map;
}


inline CCamera * CRenderParams::getCamera() const noexcept
{
    return m_camera;
}


inline glm::mat4 CRenderParams::getView() const noexcept
{
    return m_view;
}


inline glm::mat4 CRenderParams::getProjection() const noexcept
{
    return m_projection;
}


inline TRenderPass CRenderParams::getType() const noexcept
{
    return m_type;
}


inline TDisplayMode CRenderParams::getMode() const noexcept
{
    return m_mode;
}


inline std::shared_ptr<CShader> CRenderParams::getShader() const noexcept
{
    return m_shader;
}

} // Namespace TE
