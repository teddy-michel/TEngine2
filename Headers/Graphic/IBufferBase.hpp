/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <vector>
#include <glm/glm.hpp>

#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Game/CEntityManager.hpp"
#include "Graphic/VertexData.hpp"
#include "Graphic/opengl.h"


namespace TE
{

class CShader;


/**
 * \enum    TPrimitiveType
 * \ingroup Graphic
 * \brief   Types de primitives � afficher.
 ******************************/

enum TPrimitiveType
{
    PrimTriangle = GL_TRIANGLES,           ///< Chaque triplet de sommets constitue un triangle.
    PrimTriangleFan = GL_TRIANGLE_FAN,     ///< Le premier sommet est commun � tous les triangles.
    PrimTriangleStrip = GL_TRIANGLE_STRIP, ///< Chaque nouveau sommet forme un triangle avec les deux pr�c�dents sommets.
    PrimLine = GL_LINES,                   ///< Chaque couple de sommets forme une ligne.
    PrimPoint = GL_POINTS                  ///< Dessine un point pour chaque sommet transmis.
};


/**
 * \struct  TBufferTexture
 * \ingroup Graphic
 * \brief   Liste des textures utilis�es dans un buffer.
 ******************************/

struct TENGINE2_API TBufferTexture
{
    TTextureId texture[NumUTUsed]; ///< Identifiant de la texture pour chaque unit�.
    unsigned int nbr;              ///< Nombre d'indices concern�s.

    /// Constructeur par d�faut.
    TBufferTexture() :
        nbr{ 0 },
        texture{}
    {
    }

    TBufferTexture(unsigned int nbr, TTextureId text0) :
        nbr{ nbr },
        texture{}
    {
        texture[0] = text0;
    }
};

typedef std::vector<TBufferTexture> TBufferTextureVector;


/**
 * \class   IBufferBase
 * \ingroup Graphic
 * \brief   Interface de base des buffers de la carte graphique.
 *
 * Un buffer est une partie de la m�moire de la carte graphique qui permet de stocker des donn�es
 * n�cessaire � l'affichage. Ces donn�es peuvent �tre des informations sur les sommets (position,
 * coordonn�es de texture, vecteur normal, couleur, etc.) ou sur n'importe quel param�tre
 * utilis� dans un shader.
 * Les sommets sont r�f�renc�s par des indices, et un buffer contient une liste de textures �
 * utiliser pour chaque indice.
 ******************************/

class TENGINE2_API IBufferBase
{
public:

    // Constructeurs et destructeur
    IBufferBase();
    virtual ~IBufferBase() = default;
    IBufferBase(const IBufferBase&) = delete;
    IBufferBase(IBufferBase&&) = delete;
    IBufferBase& operator=(const IBufferBase&) = delete;
    IBufferBase& operator=(IBufferBase&&) = delete;

    virtual inline bool isSubBuffer() const noexcept;

    void setTransparent(bool transparent);
    inline bool isTransparent() const noexcept;

    // Type
    TPrimitiveType getPrimitiveType() const;
    void setPrimitiveType(TPrimitiveType primitive);

    // Indices
    void setIndices(const std::vector<unsigned int>& indices);
    std::vector<unsigned int>& getIndices();
    const std::vector<unsigned int>& getIndices() const;

    // Textures
    void setTextures(const TBufferTextureVector& textures);
    TBufferTextureVector& getTextures();
    const TBufferTextureVector& getTextures() const;

    void setEntityId(TEntityId id);

    virtual void clear();
    virtual unsigned int draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const = 0;
    virtual void update(bool updateId = true) = 0;

private:

    // Donn�es priv�es
    bool m_transparent; ///< Indique si le buffer contient des �l�ments transparents.

protected:

    // Donn�es prot�g�es
    TPrimitiveType m_primitive;             ///< Type de primitive � afficher (par d�faut PrimTriangle).
    std::vector<unsigned int> m_indices;    ///< Tableau des indices.
    TBufferTextureVector m_textures;        ///< Tableau des textures.
    std::vector<TVertexDataRef> m_dataRefs; ///< Liste des r�f�rences utilis�es.
    TEntityId m_entityId;                   ///< Identifiant de l'entit� utilisant le buffer (0 si le buffer est partag�).
};


inline bool IBufferBase::isSubBuffer() const noexcept
{
    return false;
}


/**
 * Indique si le buffer contient des �l�ments transparents.
 *
 * \return Bool�en.
 ******************************/

inline bool IBufferBase::isTransparent() const noexcept
{
    return m_transparent;
}

} // Namespace TE
