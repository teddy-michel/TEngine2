/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <memory>

#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderer.hpp"


namespace TE
{

template<typename T> class CSubBuffer;
class CShader;


/**
 * \class   CMegaBuffer
 * \ingroup Graphic
 * \brief   CMegaBuffer est un conteneur pour CSubBuffer.
 *
 * Un mega buffer permet d'allouer une zone m�moire de taille importante dans la carte graphique,
 * et d'y stocker plusieurs buffers graphiques.
 ******************************/

template<typename T>
class /*TENGINE2_API*/ CMegaBuffer
{
    friend class CSubBuffer<T>; // pour acc�der � addSubBuffer, removeSubBuffer, et updateSubBuffer

public:

    // Constructeurs et destructeur
    explicit CMegaBuffer(unsigned int count);
    ~CMegaBuffer();
    CMegaBuffer(const CMegaBuffer&) = delete;
    CMegaBuffer(CMegaBuffer&&) = delete;
    CMegaBuffer& operator=(const CMegaBuffer&) = delete;
    CMegaBuffer& operator=(CMegaBuffer&&) = delete;

    unsigned int getSize() const { return m_count * sizeof(T); }

    unsigned int draw(const std::vector<const IBufferBase *>& buffers, TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const;

private:

    void addSubBuffer(CSubBuffer<T> * buffer);
    void removeSubBuffer(CSubBuffer<T> * buffer);
    void updateSubBuffer(CSubBuffer<T> * buffer);

    int createChunkForBuffer(CSubBuffer<T> * buffer, unsigned int count);
    void compact();

    struct TChunk
    {
        CSubBuffer<T> * buffer; ///< Pointeur sur le buffer ou nullptr si le chunk est vide.
        unsigned int count;     ///< Nombre d'�l�ments dans le chunk.

        TChunk(CSubBuffer<T> * b, unsigned int c) : buffer{ b }, count{ c } { }
    };

    // Donn�es priv�es
    unsigned int m_count; ///< Nombre d'�l�ments maximal dans le buffer.
    mutable std::unique_ptr<CBuffer<T>> m_buffer; ///< Buffer graphique.
    std::vector<CSubBuffer<T> *> m_subBuffers; ///< Liste des sous-buffers.
    std::vector<TChunk> m_chunks; ///< Liste des chunks de m�moire.
};

} // Namespace TE

#include "Graphic/CMegaBuffer.tpl.hpp"
