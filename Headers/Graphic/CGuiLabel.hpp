/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Graphic/IGuiObject.hpp"
#include "Graphic/CFontManager.hpp"


namespace TE
{

class CBuffer2D;


/**
 * \class   CGuiLabel
 * \ingroup Graphic
 * \brief   Object graphique permettant d'afficher du texte.
 ******************************/

class TENGINE2_API CGuiLabel : public IGuiObject
{
public:

    // Constructeurs et destructeur
    explicit CGuiLabel(const CString& text = CString{});
    virtual ~CGuiLabel();
    CGuiLabel(const CGuiLabel&) = delete;
    CGuiLabel(CGuiLabel&&) = delete;
    CGuiLabel& operator=(const CGuiLabel&) = delete;
    CGuiLabel& operator=(CGuiLabel&&) = delete;

    virtual void update(float frameTime) override;
    virtual void draw(CBuffer2D * buffer) const override;

    void setText(const CString& text);
    inline CString getText() const noexcept;

    void setColor(const CColor& color);
    inline CColor getColor() const noexcept;

    void setFontSize(int size);
    inline int getFontSize() const noexcept;

    void setFont(TFontId font);
    inline TFontId getFont() const noexcept;

    void setSize(int width, int height);
    void setWidth(int width);
    void setHeight(int height);
    inline int getWidth() const noexcept;
    inline int getHeight() const noexcept;

    void setBackgroundColor(const CColor& color);

private:

    // TODO: centrage
    TTextParams m_textParams;
    CColor m_backgroundColor; ///< Couleur de fond.
};


inline CString CGuiLabel::getText() const noexcept
{
    return m_textParams.text;
}


inline CColor CGuiLabel::getColor() const noexcept
{
    return m_textParams.color;
}


inline int CGuiLabel::getFontSize() const noexcept
{
    return m_textParams.size;
}


inline TFontId CGuiLabel::getFont() const noexcept
{
    return m_textParams.font;
}


inline int CGuiLabel::getWidth() const noexcept
{
    return m_textParams.rec.z;
}


inline int CGuiLabel::getHeight() const noexcept
{
    return m_textParams.rec.w;
}

} // Namespace TE
