/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <iostream>

#include "library.h"


namespace TE
{

/**
 * \class   CColor
 * \ingroup Graphic
 * \brief   Classe permettant de gérer les couleurs.
 *
 * Un certain nombre de méthodes sont destinées à modifier ces couleurs :
 * conversion en niveau de gris, modulation, etc. La couleur est stockée sous
 * forme d'un nombre entier de 4 octets au format RGBA.
 ******************************/

class TENGINE2_API CColor
{
public:

    // Constructeurs
    explicit CColor(uint32_t color = 0xFFFFFFFF);
    CColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF);

    // Accesseur
    uint32_t getColor() const;

    // Opérateurs
    bool operator==(const CColor& color) const;
    bool operator!=(const CColor& color) const;
    const CColor& operator+=(const CColor& color);
    const CColor& operator-=(const CColor& color);
    CColor operator+(const CColor& color) const;
    CColor operator-(const CColor& color) const;
    CColor operator*(float k) const;
    const CColor& operator*=(float k);
    CColor operator/(float k) const;
    const CColor& operator/=(float k);

    // Méthodes publiques
    uint8_t toGrey() const;
    uint32_t toARGB() const;
    uint32_t toABGR() const;
    uint32_t toRGBA() const;
    uint8_t getRed() const;
    uint8_t getGreen() const;
    uint8_t getBlue() const;
    uint8_t getAlpha() const;
    void setRed(uint8_t red);
    void setGreen(uint8_t green);
    void setBlue(uint8_t blue);
    void setAlpha(uint8_t alpha);
    void set(float r, float g, float b, float a = 1.0f);
    void set(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF);
    CColor modulate(const CColor& color) const;
    CColor interpolate(const CColor& color, float percentage) const;
    void toFloat(float dest[]) const;
    void toFloatRGB(float dest[]) const;

    // Couleur prédéfinies
    static const CColor White;     ///< Couleur blanche.
    static const CColor Grey;      ///< Couleur grise.
    static const CColor Black;     ///< Couleur noire.
    static const CColor Red;       ///< Couleur rouge.
    static const CColor Green;     ///< Couleur verte.
    static const CColor DarkGreen; ///< Couleur verte foncée.
    static const CColor Blue;      ///< Couleur bleue.
    static const CColor Yellow;    ///< Couleur jaune.
    static const CColor Cyan;      ///< Couleur cyan.
    static const CColor Magenta;   ///< Couleur magenta.
    static const CColor Orange;    ///< Couleur orange.

private:

    // Méthode protégée
    void setInt(int r, int g, int b, int a = 0xFF);

protected:

    // Donnée protégée
    uint32_t m_color; ///< Valeur de la couleur.
};


/*-----------------------------------*
 *   Fonctions communes aux couleurs *
 *-----------------------------------*/

std::istream& operator>>(std::istream& stream, CColor& color);
std::ostream& operator<<(std::ostream& stream, const CColor& color);

} // Namespace TE
