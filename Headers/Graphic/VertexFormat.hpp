/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <glm/glm.hpp>

#include "Graphic/CBuffer.hpp"


namespace TE
{

class CShader;


/// Structure d'un sommet 3D.
struct TENGINE2_API TVertex3D
{
    glm::vec3 position;  ///< Position du point.
    uint32_t id;         ///< Identifiant pour faire le lien avec la matrice de mod�le et la liste des textures.
    glm::vec3 normal;    ///< Vecteur normal.
    //glm::vec3 tangent;   ///< Vecteur tangent.
    glm::vec4 color;     ///< Couleur du point.
    glm::vec2 coords[2]; ///< Coordonn�es de texture.

    TVertex3D() = default;
};

template<>
void TENGINE2_API CBuffer<TVertex3D>::updateAttributes(const std::shared_ptr<CShader>& shader) const;


/// Structure d'un sommet 2D.
struct TENGINE2_API TVertex2D
{
    glm::vec2 position; ///< Position du point.
    glm::vec2 coords;   ///< Coordonn�es de texture.
    glm::vec4 color;    ///< Couleur du point.
    uint32_t id;        ///< Identifiant pour faire le lien avec la matrice de mod�le et la liste des textures.

    TVertex2D() = default;
};

template<>
void TENGINE2_API CBuffer<TVertex2D>::updateAttributes(const std::shared_ptr<CShader>& shader) const;

} // Namespace TE
