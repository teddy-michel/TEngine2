/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Graphic/CTextureManager.hpp"


namespace TE
{

/*
Format du buffer pour RenderDoc :

uint model;
uint texture[8];
*/
#include "Core/struct_alignment_start.h"
struct TVertexData
{
    uint32_t model;
    TTextureId textures[CTextureManager::NumUnit];

    explicit TVertexData(uint32_t m = 0) : model{ m }, textures{ 0 } { }

    inline bool operator==(const TVertexData& other) const
    {
        return (memcmp(&model, &other.model, sizeof(TVertexData)) == 0);
    }
};
#include "Core/struct_alignment_end.h"

typedef uint32_t TVertexDataRef; ///< Entier non sign� de taille fixe.

} // Namespace TE
