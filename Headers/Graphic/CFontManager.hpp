/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "Core/CString.hpp"
#include "Graphic/CColor.hpp"

#ifdef T_USE_FREETYPE
#include <ft2build.h>
#include FT_FREETYPE_H
#else
#include "Graphic/CTextureManager.hpp"
#endif


namespace TE
{

/// Type des identifiants de polices utilisés par le moteur.
typedef unsigned int TFontId;


/**
 * \struct  TTextLine
 * \ingroup Graphic
 * \brief   Une ligne de texte, avec la largeur de chaque caractère.
 ******************************/

struct TENGINE2_API TTextLine
{
    unsigned int offset;    ///< Numéro du premier caractère par rapport au texte entier.
    std::vector<float> len; ///< Largeur de chaque caractère en pixels.

    /// Constructeur par défaut
    inline TTextLine() : offset{ 0 } { }
};


class CFontManager;
class CBuffer2D;

#ifdef T_USE_FREETYPE
class CFont;
#endif

struct TTextParams;


/// Instance unique.
extern TENGINE2_API CFontManager * gFontManager;


/**
 * \class   CFontManager
 * \ingroup Graphic
 * \brief   Gestion des polices de caractère.
 *
 * Singleton accessible avec la variable globale gFontManager.
 ******************************/

class TENGINE2_API CFontManager
{
    friend class CFont;

public:

    static const int minFontSize = 8;
    static const int maxFontSize = 128;

    // Constructeurs
    CFontManager();
    CFontManager(const CFontManager&) = delete;
    CFontManager(CFontManager&&) = delete;
    CFontManager& operator=(const CFontManager&) = delete;
    CFontManager& operator=(CFontManager&&) = delete;
    ~CFontManager();

public:

    // Accesseurs
    TFontId getFontId(const CString& name);
    CString getFontName(const TFontId id) const;

    // Méthodes publiques
    TFontId loadFont(const CString& name, const CString& fileName);
    void unloadFonts();
    void drawText(CBuffer2D * buffer, const TTextParams& params);
    glm::u32vec2 getTextSize(const TTextParams& params);
    void getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines);
    TTextLine getTextLineSize(const TTextParams& params);
    void logFontList() const;

    static const TFontId InvalidFont = static_cast<TFontId>(-1);

private:

    TFontId privLoadFont(const CString& name, const CString& fileName);

#ifdef T_USE_FREETYPE
    typedef std::vector<CFont *> TFontVector;
#else

    /**
     * \struct  TFont
     * \ingroup Graphic
     * \brief   Structure d'une police de caractère.
     ******************************/

    struct TFont
    {
        CString name;           ///< Nom de la police.
        TTextureId texture;     ///< Identifiant de la texture.
        unsigned int width;     ///< Largeur de la texture en pixels.
        unsigned int height;    ///< Hauteur de la texture en pixels.
        glm::u16vec2 size[256]; ///< Dimensions de chaque caractère.

        TFont() :
            name    {},
            texture { CTextureManager::NoTexture },
            width   { 0 },
            height  { 0 },
            size    { 0 }
        { }
    };

    typedef std::vector<TFont> TFontVector;
#endif

    // Données privées

#ifdef T_USE_FREETYPE
    FT_Library m_library; ///< Bibliothèque FreeType.
#endif

    TFontVector m_fonts;  ///< Liste des polices chargées.
};


/**
 * \struct  TTextParams
 * \ingroup Graphic
 * \brief   Paramètres du texte à afficher.
 ******************************/

struct TENGINE2_API TTextParams
{
  //CFormattedString text; ///< Texte à afficher.
    CString text;          ///< Texte à afficher.
    CColor color;          ///< Couleur du texte.

    glm::i32vec4 rec;      ///< Rectangle dans lequel afficher le texte.
    TFontId font;          ///< Police à utiliser.

    uint16_t size:10;      ///< Taille du texte.
    uint16_t newline:1;    ///< Indique si les retours à la ligne sont automatiques.
    uint16_t kerning:1;    ///< Indique si on utilise le kerning (rapprochement de certaines paires de caractères).
    uint16_t _padding:4;

    uint16_t left;         ///< Décalage à gauche en pixels.
    uint16_t top;          ///< Décalage en haut en pixels.

    /// Constructeur par défaut.
    inline TTextParams() :
        text    {},
        color   { CColor::Black },
        rec     { 0 },
        font    { CFontManager::InvalidFont },
        size    { 16 },
        newline { 0 },
        kerning { 0 },
        left    { 0 },
        top     { 0 }
    { }
};

} // Namespace TE
