/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <memory>

#include "Graphic/VertexData.hpp"
#include "Graphic/opengl.h"


namespace TE
{

const unsigned int NumUTUsed = 4; ///< Nombre d'unit�s de texture utilis�es par les buffers.


/**
 * Macro servant � calculer la position dans le buffer
 ******************************/
#define T_BUFFER_OFFSET(n)  (reinterpret_cast<char *>((n)))


/**
 * \enum    TDisplayMode
 * \ingroup Graphic
 * \brief   Modes d'affichage.
 ******************************/

enum TDisplayMode
{
    Normal,          ///< Affichage normal.
    Textured,        ///< Affichage des textures sans les lightmaps.
    Lightmaps,       ///< Affichage des lightmaps uniquement.
    Colored,         ///< Affichage des surfaces color�es.
    Wireframe,       ///< Fil de fer.
    WireframeCulling ///< Fil de fer avec culling activ�.
};


/**
 * \enum    TFunctionnality
 * \ingroup Graphic
 * \brief   Fonctionnalit�s du moteur graphique.
 ******************************/

enum class TFunctionnality
{
    BindlessTexture
};


template<typename T> class CMegaBuffer;
struct TVertex3D;


/**
 * \struct  CRenderer
 * \ingroup Graphic
 * \brief   Moteur graphique.
 ******************************/

class TENGINE2_API CRenderer
{
public:

    CRenderer();
    ~CRenderer();
    CRenderer(const CRenderer&) = delete;
    CRenderer(CRenderer&&) = delete;
    CRenderer& operator=(const CRenderer&) = delete;
    CRenderer& operator=(CRenderer&&) = delete;

    bool init();
    void startFrame();
    void endFrame();
    void startGUI();
    void endGUI();

    bool support(TFunctionnality functionality) const;

    inline TDisplayMode getMode() const noexcept;
    inline void setMode(TDisplayMode mode);

    inline float getFieldOfView() const noexcept { return 70.0f; };

    //void bindShader(const std::shared_ptr<CShader>& shader); // TODO: public interface
    void bindShader(GLuint shader); // TODO: private implementation for OpenGL

    inline std::shared_ptr<CMegaBuffer<TVertex3D>> getMegaBuffer() const noexcept;

    inline GLuint getLinksSSBO() const noexcept;
    inline GLuint getModelsSSBO() const noexcept;

    TVertexDataRef getVertexDataRef(const TVertexData& data);
    void unrefVertexData(const std::vector<TVertexDataRef>& refs);

    void sendVertexData();
    void sendModelData();

private:

    //CRendererImpl * const m_impl; ///< Impl�mentation priv�e. (TODO)
    TDisplayMode m_mode;        ///< Mode d'affichage.
    GLuint m_currentShader;     ///< Identifiant du shader actuellement bind�. (TODO: move to impl)
    // TODO: SSBO models
    std::vector<TVertexData> m_vertexData;
    std::vector<unsigned int> m_vertexDataRefCount;
    GLuint m_linksSSBO;
    GLuint m_modelsSSBO;
    std::shared_ptr<CMegaBuffer<TVertex3D>> m_megaBuffer; ///< Mega buffer. (on ne peut pas utiliser un unique_ptr � cause des d�pendances circulaires.)
    bool m_supportBindlessTexture;
};

/// Instance unique.
extern TENGINE2_API CRenderer * gRenderer;


/**
 * Retourne le mode d'affichage.
 *
 * \return Mode d'affichage.
 ******************************/

inline TDisplayMode CRenderer::getMode() const noexcept
{
    return m_mode;
}


/**
 * Modifie le mode d'affichage.
 *
 * \param mode Mode d'affichage.
 ******************************/

inline void CRenderer::setMode(TDisplayMode mode)
{
    m_mode = mode;
}


inline std::shared_ptr<CMegaBuffer<TVertex3D>> CRenderer::getMegaBuffer() const noexcept
{
    return m_megaBuffer;
}


inline GLuint CRenderer::getLinksSSBO() const noexcept
{
    return m_linksSSBO;
}


inline GLuint CRenderer::getModelsSSBO() const noexcept
{
    return m_modelsSSBO;
}

} // Namespace TE
