/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <map>
#include <memory>

#include "Core/CString.hpp"
#include "Graphic/opengl.h"


namespace TE
{

class CShader;


/**
 * \class   CShaderManager
 * \ingroup Graphic
 * \brief   Gestionnaire de ressources pour les shaders.
 ******************************/

class TENGINE2_API CShaderManager
{
public:

    /// Types de shader.
    enum TShaderType
    {
        VertexShader,
        FragmentShader,
        GeometryShader,
        ComputeShader
    };

    CShaderManager();
    ~CShaderManager();
    CShaderManager(const CShaderManager&) = delete;
    CShaderManager(CShaderManager&&) = delete;
    CShaderManager& operator=(const CShaderManager&) = delete;
    CShaderManager& operator=(CShaderManager&&) = delete;

    void setPath(const CString& path);

    std::shared_ptr<CShader> loadFromFiles(const CString& vertFile, const CString& fragFile);
    std::shared_ptr<CShader> loadVertexShaderFromFile(const CString& fileName);
    std::shared_ptr<CShader> loadFragmentShaderFromFile(const CString& fileName);

private:

    GLuint loadShaderFromFile(const CString& fileName, TShaderType type);
    GLuint loadShaderFromSource(const char * src, TShaderType type);
    CString getShaderInfoLog(GLuint shader, bool& success) const;

    CString m_path; ///< R�pertoire dans lequel chercher les fichiers de shader.
    std::map<CString, GLuint> m_files; ///< Liste des shaders d�j� charg�s (nom de fichier => identifiant).
    std::map<std::pair<GLuint, GLuint>, std::shared_ptr<CShader>> m_shaders; ///< Liste des shaders compil�s (identifiants => CShader).
};

/// Instance unique.
extern TENGINE2_API CShaderManager * gShaderManager;

} // Namespace TE
