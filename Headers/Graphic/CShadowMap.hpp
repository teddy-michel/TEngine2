/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <memory>
#include <glm/glm.hpp>

#include "Graphic/CFrameBuffer.hpp"
#include "Graphic/opengl.h"


namespace TE
{

class ILight;
class CMap;
class CShader;


/**
 * \class   CShadowMap
 * \ingroup Graphic
 * \brief   Gestion des shadow maps pour plusieurs sources de lumi�res directionnelles.
 *
 * Cette classe permet de regrouper plusieurs shadow map dans une seule texture.
 * Les shadow maps sont carr�es et ont toutes les m�mes dimensions (512x512, 1024x1024).
 * La texture est �galement carr�e et peut contenir 1, 2, 4, 8 ou 16 shadow maps selon les
 * dimensions des shadow maps, pour une taille maximale de 8192x8192.
 ******************************/

class TENGINE2_API CShadowMap
{
public:

    enum TShadowMapSize
    {
        ShadowMap_1_512,  ///< 1 shadow map de 512x512 (taille totale : 512x512).
        ShadowMap_1_1024, ///< 1 shadow map de 1024x1024 (taille totale : 1024x1024).
        ShadowMap_2_512,  ///< 4 shadow maps de 512x512 (taille totale : 1024x1024).
        ShadowMap_2_1024, ///< 4 shadow maps de 1024x1024 (taille totale : 2048x2048).
        ShadowMap_4_512,  ///< 16 shadow maps de 512x512 (taille totale : 2048x2048).
        ShadowMap_4_1024, ///< 16 shadow maps de 1024x1024 (taille totale : 4096x4096).
        ShadowMap_8_512,  ///< 64 shadow maps de 512x512 (taille totale : 4096x4096).
        ShadowMap_8_1024, ///< 64 shadow maps de 1024x1024 (taille totale : 8192x8192).
        ShadowMap_16_512  ///< 256 shadow maps de 512x512 (taille toale : 8192x8192).
    };

    // Constructeurs et destructeur
    explicit CShadowMap(TShadowMapSize size);
    ~CShadowMap();
    CShadowMap(const CShadowMap&) = delete;
    CShadowMap(CShadowMap&&) = delete;
    CShadowMap& operator=(const CShadowMap&) = delete;
    CShadowMap& operator=(CShadowMap&&) = delete;

    void update(CMap * map);

    bool addLight(ILight * light);
    bool removeLight(ILight * light);

    void updateShader(const std::shared_ptr<CShader>& shader);

private:

    enum TLightFlags
    {
        LightDisabled = 0x0000, ///< Lumi�re d�sactiv�e ou non configur�e.
        LightEnabled  = 0x1000, ///< Lumi�re activ�e.

        Spot        = 1,
        Directional = 2,
        Point_X_Neg = 3,
        Point_X_Pos = 4,
        Point_Y_Neg = 5,
        Point_Y_Pos = 6,
        Point_Z_Neg = 7,
        Point_Z_Pos = 8
    };

    static float ConvertTypeToFloat(uint32_t type);
    static uint32_t ConvertFloatToType(float value);

    /*
    Format du buffer pour RenderDoc :

    mat4 matrix;
    rgb vec3 color;
    uint type;
    vec3 position;
    float intensity;
    vec4 attenuation
    */
    struct TLight
    {
        glm::mat4 matrix;
        glm::vec4 color;  // alpha = TLightType
        glm::vec4 position; // alpha = intensity
        glm::vec4 attenuation; // x = maxDistance, y = linear attenuation, z = quadratic attenuation, w = spot angles ratio
    };

    void initFrameBuffer();

    TShadowMapSize m_size;            ///< Dimension des shadow maps.
    std::vector<ILight *> m_lights;   ///< Tableau de lumi�res dynamiques.
    std::vector<TLight> m_lightsData; ///< Tableau de lumi�res dynamiques.
    CFrameBuffer m_frameBuffer;       ///< Frame buffer utilis� pour le rendu des shadow maps.
    GLuint m_depthTexture;            ///< Identifiant de la texture de profondeur.
    GLuint m_ubo;                     ///< Uniform buffer object.
};

} // Namespace TE
