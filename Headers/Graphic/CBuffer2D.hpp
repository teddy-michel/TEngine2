/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <vector>
#include <glm/glm.hpp>

#include "Graphic/CTextureManager.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/VertexFormat.hpp"


namespace TE
{

class CShader;


/**
 * \class   CBuffer2D
 * \ingroup Graphic
 * \brief   Gestion des buffers 2D pour l'interface utilisateur.
 ******************************/

class TENGINE2_API CBuffer2D
{
public:

    // Constructeurs et destructeur
    CBuffer2D();
    ~CBuffer2D() = default;
    CBuffer2D(const CBuffer2D&) = delete;
    CBuffer2D(CBuffer2D&&) = delete;
    CBuffer2D& operator=(const CBuffer2D&) = delete;
    CBuffer2D& operator=(CBuffer2D&&) = delete;

    void reserveSize(unsigned int count);

    void addVertex(const TVertex2D& vertex, TTextureId texture = CTextureManager::NoTexture);
    void addRectangle(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color);
    void addRectangle(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color, TTextureId texture, const glm::vec2& coordsMin, const glm::vec2& coordsMax);
    //void addText();

    void clear();
    unsigned int draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const;
    void update();

private:

    std::vector<TVertex2D> m_data;
    std::vector<TTextureId> m_textures;
    std::vector<unsigned int> m_indices; ///< Tableau des indices.
    std::shared_ptr<CBuffer<TVertex2D>> m_buffer; ///< Buffer graphique.
};

} // Namespace TE
