/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "library.h"


namespace TE
{

class CBuffer2D;


/**
 * \class   IGuiObject
 * \ingroup Graphic
 * \brief   Classe de base des objets d'interface graphique.
 ******************************/

class TENGINE2_API IGuiObject
{
public:

    // Constructeurs et destructeur
    IGuiObject();
    virtual ~IGuiObject() = 0;
    IGuiObject(const IGuiObject&) = delete;
    IGuiObject(IGuiObject&&) = delete;
    IGuiObject& operator=(const IGuiObject&) = delete;
    IGuiObject& operator=(IGuiObject&&) = delete;

    virtual void update(float frameTime) = 0;
    virtual void draw(CBuffer2D * buffer) const = 0;

    void setPosition(int x, int y);

protected:

    int m_posX; ///< Coordonn�e horizontale � partir du coin sup�rieur gauche.
    int m_posY; ///< Coordonn�e verticale � partir du coin sup�rieur gauche.
    // TODO: arborescence d'objets
};

} // Namespace TE
