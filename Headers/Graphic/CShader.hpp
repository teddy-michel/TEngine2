/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//#include <string>
#include <glm/glm.hpp>

#include "Core/CString.hpp"
#include "Graphic/opengl.h"


namespace TE
{

enum TShaderBinding
{
    TextureArray = 0, ///< Tableau contenant les sampler2D pour les textures (bindless texture).
    LinkArray    = 1, ///< Tableau contenant les liens entre le champ "id" et les tableaux de models et de textures.
    ModelArray   = 2, ///< Tableau contenant toutes les matrices de transformation.
    LightArray   = 3  ///< Tableau contenant les informations sur les lumi�res dynamiques.
};


/**
 * \class   CShader
 * \ingroup Graphic
 * \brief   Gestion des shaders.
 ******************************/

class TENGINE2_API CShader
{
    friend class CShaderManager; // pour appeller le constructeur

public:

#ifdef T_OLD_SHADER
    CShader();
#endif
    ~CShader();

    void setViewMatrix(const glm::mat4& matrix);
    void setProjectionMatrix(const glm::mat4& matrix);
    void setModelMatrix(const glm::mat4& matrix);

#ifdef T_OLD_SHADER
    bool loadFromFiles(const CString& vertFile, const CString& fragFile);
    bool loadFromSources(const char * vertSrc, const char * fragSrc);
#endif

    GLint getAttributeLocation(const char * name);

    // Binding
    bool setUniformValue(const char * name, bool value);
    bool setUniformValue(const char * name, int value);
    bool setUniformValue(const char * name, unsigned int value);
    bool setUniformValue(const char * name, float value);
    bool setUniformValue(const char * name, const glm::vec2& value);
    bool setUniformValue(const char * name, const glm::vec3& value);
    bool setUniformValue(const char * name, const glm::vec4& value);
    bool setUniformValue(const char * name, const glm::mat4& value);
    bool bindBuffer(const char * name, GLuint buffer, GLuint binding);
    bool bindSSBO(const char * name, GLuint buffer, GLuint binding);

    void bind();
    void unbind();

    inline bool useBindlessTexture() const noexcept;
    void setBindlessTexture(bool use);

private:

    // Constructeur
    CShader(GLuint vertexShader, GLuint fragmentShader);
    CShader(const CShader&) = delete;
    CShader(CShader&&) = delete;
    CShader& operator=(const CShader&) = delete;
    CShader& operator=(CShader&&) = delete;

    void load();
    void unload();

#ifdef T_OLD_SHADER
    CString getShaderInfoLog(GLuint shader, bool& success) const;
#endif

    // Donn�es priv�es
    GLuint m_vertexShader;
    GLuint m_fragmentShader;
    GLuint m_shaderProgram;
    GLint m_uniModel;
    GLint m_uniView;
    GLint m_uniProj;
    bool m_useBindlessTexture;
};


inline bool CShader::useBindlessTexture() const noexcept
{
    return m_useBindlessTexture;
}

} // Namespace TE
