/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <vector>
#include <SFML/System.hpp>

#include "Graphic/CImage.hpp"
#include "Core/CString.hpp"
#include "Graphic/opengl.h"


namespace TE
{

/// Type des identifiants de texture utilisés par le moteur.
typedef uint32_t TTextureId;


/**
 * \class   CTextureManager
 * \ingroup Graphic
 * \brief   Gestionnaire de textures.
 * \todo    Gérer les textures dont les dimensions ne sont pas des puissances de deux.
 *
 * Singleton accessible avec la méthode static instance().
 * TODO: utiliser une variable globale comme pour CApplication et CFontManager ?
 *
 * Le gestionnaire de textures conserve en mémoire les données de chaque texture
 * chargée : l'adresse du fichier contenant la texture et son identifiant
 * OpenGL. Le reste du moteur manipule un identifiant qui permet au
 * CTextureManager de retrouver la texture OpenGL. Lorsqu'une texture est
 * supprimée, seule les données sur la carte graphique sont supprimées par
 * OpenGL. Le CTextureManager conserve ses propres données. Si cette texture
 * devait à nouveau être chargée, elle conserverait le même identifiant.
 * Il est possible d'utiliser jusqu'à 8 unités de textures en même temps, à
 * condition que la carte graphique supporte le multitexturing.
 *
 * Le chargement et le déchargement des textures peut se faire dans n'importe
 * quel thread. Les modifications au niveau de la carte graphique se feront
 * uniquement dans le thread principal, lors de l'appel de la méthode
 * DoPendingTask (typiquement une fois par frame).
 ******************************/

class TENGINE2_API CTextureManager
{
public:

    /**
     * \enum    TFilter
     * \ingroup Graphic
     * \brief   Options de filtrage d'une texture.
     ******************************/

    enum TFilter
    {
        FilterNone           = 0, ///< Aucun filtrage.
        FilterBilinear       = 1, ///< Filtrage bilinéaire.
        FilterBilinearMipmap = 2, ///< Filtrage bilinéaire avec mipmaps.
        FilterTrilinear      = 3  ///< Filtrage trilinéaire.
    };


    // Constructeurs et destructeur
    CTextureManager();
    ~CTextureManager();
    CTextureManager(const CTextureManager&) = delete;
    CTextureManager(CTextureManager&&) = delete;
    CTextureManager& operator=(const CTextureManager&) = delete;
    CTextureManager& operator=(CTextureManager&&) = delete;

public:

    // Accesseurs
    TTextureId getTextureId(const CString& name) const;
    CString getTextureName(TTextureId id) const;

    unsigned int getMemorySize() const;
    unsigned int getNumElements() const;

    int getResourceId(const CString& name)
    {
        return getTextureId(name);
    }

    CString getResourceName(int id) const
    {
        return getTextureName(id);
    }

    // Méthodes publiques
    void init();

    TTextureId loadTexture(const CString& fileName, TFilter filter = FilterTrilinear, bool repeat = true);
    TTextureId loadTexture(const CString& name, const CImage& image, TFilter filter = FilterTrilinear, bool repeat = true);
    bool attachFramebufferTexture(unsigned int attachment, TTextureId texture);
    bool reloadTexture(TTextureId texture, const CImage& image);
    void unloadTextures();
    void deleteTexture(const CString& name);
    void deleteTexture(TTextureId texture);

    void bindTexture(TTextureId texture, unsigned int unit = 0) const;

    void logTexturesList() const;
    void doPendingTasks();

    void setTextureResident(TTextureId id, bool resident);

    inline GLuint getUBO() const noexcept;

    static const TTextureId NoTexture = 0;        ///< Identifiant de la texture vide.
    static const TTextureId DefaultTexture = 1;   ///< Identifiant de la texture par défaut.
    static const unsigned int MaxTextures = 2048; ///< Nombre maximal de textures.
    static const unsigned int NumUnit = 8;        ///< Nombre d'unités de texture utilisables.


    /**
     * \enum    TTextureAction
     * \ingroup Graphic
     * \brief   Liste les actions possibles pour chaque texture.
     ******************************/

    enum TTextureAction
    {
        TextureActionNone   = 0, ///< Aucune action.
        TextureActionLoad   = 1, ///< Chargement de la texture.
        TextureActionDelete = 2  ///< Suppression des données dans la carte graphique.
    };

    /**
     * \struct  TTextureInfo
     * \ingroup Graphic
     * \brief   Contient les informations de chaque texture.
     * \todo    Ajouter une liste de paramètres OpenGL (mipmap, clamp, filtrage anisotropique, etc.).
     ******************************/

    struct TTextureInfo
    {
        GLuint id;             ///< Identifiant de la texture OpenGL.
        CString name;          ///< Nom de la texture.
        uint32_t filter:2;     ///< Filtre (énumération TFilter).
        uint32_t repeat:1;     ///< Indique si la texture doit se répéter sur les bords.
        uint32_t action:2;     ///< Action en attente.
        uint32_t padding:27;
        uint16_t width;        ///< Largeur de l'image.
        uint16_t height;       ///< Hauteur de l'image.
      //TTextureAction action; ///< Action en attente.
        CImage image;          ///< Image à charger dans la carte graphique.
      //unsigned int size;     ///< Taille de la texture en mémoire.

#ifdef T_TEXTURES_KEEP_TIME
        unsigned int time;     ///< Temps de la dernière utilisation de la texture.
#endif

        /// Constructeur par défaut.
        TTextureInfo(GLuint p_id = 0, const CString& p_name = CString(), TFilter p_filter = FilterTrilinear) :
            id     (p_id),
            name   (p_name),
            filter (p_filter),
            repeat { 1 },
            action { TextureActionNone },
            padding{ 0 },
            width  { 0 },
            height { 0 }
#ifdef T_TEXTURES_KEEP_TIME
                      ,
            time   { 0 }
#endif

        { }
    };

    typedef std::vector<TTextureInfo> TTextureInfoVector;

    TTextureInfo getTextureInfos(TTextureId texture) const;
    inline TTextureInfoVector getTextures() const;

private:

    // Méthodes privées
    void PrivLoadTexture(TTextureId id, TTextureInfo& texture);
    void PrivDeleteTexture(TTextureId id, TTextureInfo& texture);
    void updateUBO(TTextureId id, GLuint64 handle);

    // Données privées
    mutable sf::Mutex m_mutex;             ///< Mutex pour permettre l'utilisation dans plusieurs threads.
    mutable TTextureId m_texture[NumUnit]; ///< Identifiant de la texture utilisée pour chaque unité.
    TTextureInfoVector m_textures;         ///< Tableau des textures.
    GLuint m_ubo;                          ///< Uniform buffer object pour stocker les identifiants de texture.
};

/// Instance unique.
extern TENGINE2_API CTextureManager * gTextureManager;


/**
 * Retourne la liste des textures chargées dans le moteur.
 *
 * \return Liste des textures.
 ******************************/

inline CTextureManager::TTextureInfoVector CTextureManager::getTextures() const
{
    return m_textures;
}


inline GLuint CTextureManager::getUBO() const noexcept
{
    return m_ubo;
}

} // Namespace TE
