/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <map>
#include <glm/glm.hpp>

#include "Graphic/CImage.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/CString.hpp"

#ifdef T_USE_FREETYPE
#include <ft2build.h>
#include FT_FREETYPE_H
#endif


namespace TE
{

class CBuffer2D;
struct TTextParams;
struct TTextLine;


/**
 * \class   CFont
 * \ingroup Graphic
 * \brief   Gestion d'un fichier de police de caractère.
 *
 * Cette classe est utilisée en interne par le gestionnaire de police.
 ******************************/

class TENGINE2_API CFont
{
    //friend class CFontManager;

public:

    struct TCharParams
    {
        CImage pixmap;   ///< Image du caractère.
        glm::ivec4 rect; ///< Dimensions du caractère.
#ifdef T_USE_FREETYPE
        FT_UInt glyphIndex;
#endif
        TTextureId texture; ///< Identifiant de la texture.

        inline TCharParams() :
            pixmap     {},
            rect       { 0 },
#ifdef T_USE_FREETYPE
            glyphIndex { 0 },
#endif
            texture    { CTextureManager::NoTexture }
        { }
    };


    // Constructeur de destructeur
    CFont();
    ~CFont();

    bool loadFromFile(const CString& fileName);

    CString getFamilyName() const;
    CString getFontName() const;

    void drawText(CBuffer2D * buffer, const TTextParams& params);
    glm::u32vec2 getTextSize(const TTextParams& params);
    void getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines);
    void getTextLineSize(const TTextParams& params, TTextLine& line);
    TCharParams getCharParams(int size, const CChar& character);

private:

    void setFontSize(int size) const;
    TCharParams loadChar(uint32_t charCode);

    // Données privées
#ifdef T_USE_FREETYPE
    FT_Face m_face;
#endif
    bool m_hasKerning;
    mutable int m_currentSize;
    std::map<int, std::map<CChar, TCharParams> > m_chars;
};

} // Namespace TE
