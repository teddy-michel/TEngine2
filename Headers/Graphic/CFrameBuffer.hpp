/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Graphic/CTextureManager.hpp"
#include "Graphic/opengl.h"


namespace TE
{

enum class TColorFormat
{
    Red,
    RG,
    RGB,
    RGBA,
};


enum class TDepthFormat
{
    Depth16,
    Depth24,
    Depth32,
    Depth24_Stencil8,
    Stencil8,
};


/**
 * \class   CFrameBuffer
 * \ingroup Graphic
 * \brief   Gestion des frame buffers.
 ******************************/

class TENGINE2_API CFrameBuffer
{
public:

    static constexpr unsigned int MaxColorAttachment = 16;

    // Constructeurs et destructeur
    CFrameBuffer();
    ~CFrameBuffer();
    CFrameBuffer(const CFrameBuffer& other) = delete;
    CFrameBuffer(CFrameBuffer&& other) = delete;
    CFrameBuffer& operator=(const CFrameBuffer&) = delete;
    CFrameBuffer& operator=(CFrameBuffer&&) = delete;

    void bind();
    void unbind();

    bool check() const;

    bool createColorBuffer(int width, int height, TColorFormat format, unsigned int attachment);
    bool removeColorBuffer(unsigned int attachment);
    bool createDepthBuffer(int width, int height, TDepthFormat format);
    bool attachTexture(TTextureId texture, unsigned int attachment);

    void setNoColorBufferFlag(bool flag);

    bool checkBufferSize(int width, int height) const;

private:

    GLuint m_fbo;              ///< Identifiant du Frame Buffer Object.
    GLuint m_depthBuffer;      ///< Identifiant du buffer de pronfondeur.
    GLuint m_colorBuffers[MaxColorAttachment]; ///< Identifiants des buffers de rendu.
};

} // Namespace TE
