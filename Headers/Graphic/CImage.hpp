/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "Graphic/CColor.hpp"
#include "Core/CString.hpp"


namespace TE
{

/**
 * \class   CImage
 * \ingroup Graphic
 * \brief   Classe pour gérer les images.
 ******************************/

class TENGINE2_API CImage
{
public:

    // Constructeurs
    CImage();
    CImage(const CImage& image);
    CImage(CImage&& image);
    CImage(unsigned int width, unsigned int height);
    CImage(unsigned int width, unsigned int height, const CColor * pixels);
    ~CImage() = default;

    // Accesseurs
    inline unsigned int getWidth() const noexcept;
    inline unsigned int getHeight() const noexcept;
    const CColor * getPixels() const;

    // Méthodes publiques
    bool loadFromFile(const CString& file);
    bool saveToFile(const CString& file) const;
    void fill(const CColor& color);
    void setPixel(unsigned int x, unsigned int y, const CColor& color);
    CColor getPixel(unsigned int x, unsigned int y) const;
    void flip();
    void mirror();
    void changeColor(const CColor& from, const CColor& to);
    //void insertImage(const CImage& image, const CRectangle& rec);
    //void resize(unsigned int width, unsigned int height);
    CImage& operator=(const CImage& image);
    CImage& operator=(CImage&& image);

private:

    // Données privées
    unsigned int m_width;         ///< Largeur de l'image en pixels.
    unsigned int m_height;        ///< Hauteur de l'image en pixels.
    std::vector<CColor> m_pixels; ///< Tableau de pixels de l'image.
};


/**
 * Accesseur pour width.
 *
 * \return Largeur de l'image
 ******************************/

inline unsigned int CImage::getWidth() const noexcept
{
    return m_width;
}


/**
 * Accesseur pour height.
 *
 * \return Hauteur de l'image.
 ******************************/

inline unsigned int CImage::getHeight() const noexcept
{
    return m_height;
}

} // Namespace TE
