/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Graphic/CTextureManager.hpp"


namespace TE
{

/**
 * \class   CCursor
 * \ingroup Graphic
 * \brief   Gestion d'un curseur de la souris.
 ******************************/

class TENGINE2_API CCursor
{
public:

    static const unsigned int CursorSize = 24;

    // Constructeur et destructeur
    CCursor(const char pixels[CursorSize*CursorSize] = nullptr);
    CCursor(const char pixels[CursorSize*CursorSize], const unsigned int px, const unsigned int py);
    ~CCursor();

    // Accesseurs
    char const * getPixels() const;
    unsigned int getPointerX() const;
    unsigned int getPointerY() const;
    TTextureId getTextureId();

    // Mutateurs
    void setPointerX(unsigned int px);
    void setPointerY(unsigned int py);

private:

    // Méthode privée
    void loadTexture();

    /**
     * \enum    TCursorPixels
     * \ingroup Gui
     * \brief   Liste des couleurs disponibles pour les curseurs.
     ******************************/

#if __cplusplus < 201103L
    enum TCursorPixels
#else
    enum TCursorPixels : uint8_t
#endif
    {
        PixelBlack       = 'X', ///< Pixel de couleur noire.
        PixelWhite       = '.', ///< Pixel de couleur blanche.
        PixelTransparent = ' ', ///< Pixel transparent.
        PixelInverted    = '0'  ///< Couleur inversée (ou noir si indisponible).
    };

protected:

    // Donnée protégée
    char m_pixels[CursorSize*CursorSize]; ///< Pixels composant le curseur.
    unsigned int m_px;    ///< Coordonnée horizontale du pointeur.
    unsigned int m_py;    ///< Coordonnée verticale du pointeur.
    TTextureId m_texture; ///< Identifiant de la texture du curseur.
};

} // Namespace TE
