/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CSubBuffer.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 *
 * \param count Nombre maximal d'�l�ments dans le buffer.
 ******************************/

template<typename T>
CMegaBuffer<T>::CMegaBuffer(unsigned int count) :
    m_count      { count },
    m_buffer     { std::make_unique<CBuffer<T>>(count * sizeof(T)) },
    m_subBuffers {},
    m_chunks     {}
{
    m_chunks.push_back(TChunk{ nullptr, count });
}


/**
 * Destructeur.
 ******************************/

template<typename T>
CMegaBuffer<T>::~CMegaBuffer()
{
    gApplication->log(CString("Delete big buffer (0x%2).").arg(CString::fromPointer(this)), ILogger::Debug);
}


template<typename T>
void CMegaBuffer<T>::addSubBuffer(CSubBuffer<T> * buffer)
{
    assert(buffer != nullptr);
    assert(buffer->m_megaBuffer == this);
    assert(std::find(m_subBuffers.begin(), m_subBuffers.end(), buffer) == m_subBuffers.end());

    m_subBuffers.push_back(buffer);

    createChunkForBuffer(buffer, 0);
}


/**
 * Cr�e un chunk pour stocker un buffer.
 *
 * \param buffer Pointeur sur le buffer � ajouter (non nul).
 * \param count Nombre maximal d'�l�ments dans le buffer.
 * \return Offset du chunk, ou -1 en cas d'erreur.
 ******************************/

template<typename T>
int CMegaBuffer<T>::createChunkForBuffer(CSubBuffer<T> * buffer, unsigned int count)
{
    assert(buffer != nullptr);

    unsigned int totalFreeSpace = 0;
    int offset = 0;

    for (auto it = m_chunks.begin(); it != m_chunks.end(); ++it)
    {
        if (it->buffer == nullptr)
        {
            if (it->count == count)
            {
                it->buffer = buffer;
                return offset;
            }
            else if (it->count > count)
            {
                it->count -= count;
                m_chunks.insert(it, TChunk{ buffer, count });
                return offset;
            }

            totalFreeSpace += it->count;
        }

        offset += it->count;
    }

    if (totalFreeSpace < count)
    {
        gApplication->log("Not enough space in mega buffer.", ILogger::Error);
        return -1;
    }

    // Si on arrive ici, c'est qu'il n'y a pas d'espace libre suffisament grand
    compact();

    // On r�cup�re le dernier chunk (forc�ment vide)
    auto lastChunk = m_chunks.back();
    const unsigned int lastCount = lastChunk.count;
    lastChunk.buffer = buffer;
    lastChunk.count = count;
    m_chunks.push_back(TChunk{ nullptr, lastCount - count });

    return (m_count - lastCount);
}


template<typename T>
void CMegaBuffer<T>::removeSubBuffer(CSubBuffer<T> * buffer)
{
    assert(buffer != nullptr);
    assert(buffer->m_megaBuffer == this);

    auto it1 = std::find(m_subBuffers.begin(), m_subBuffers.end(), buffer);
    if (it1 == m_subBuffers.end())
    {
        return;
    }

    m_subBuffers.erase(it1);

    for (auto it = m_chunks.begin(); it != m_chunks.end(); ++it)
    {
        if (it->buffer == buffer)
        {
            // V�rifie si le chunk pr�c�dent est vide
            if (it != m_chunks.begin())
            {
                auto itPrev = std::prev(it);
                if (itPrev->buffer == nullptr)
                {
                    itPrev->count += it->count;
                    it = std::prev(m_chunks.erase(it));
                }
            }

            it->buffer = nullptr;

            // V�rifie si le chunk suivant est vide
            auto itNext = std::next(it);
            if (it != m_chunks.end())
            {
                if (itNext->buffer == nullptr)
                {
                    it->count += itNext->count;
                    m_chunks.erase(itNext);
                }
            }

            return;
        }
    }
}


template<typename T>
void CMegaBuffer<T>::updateSubBuffer(CSubBuffer<T> * buffer)
{
    assert(buffer != nullptr);
    assert(buffer->m_megaBuffer == this);
    assert(std::find(m_subBuffers.begin(), m_subBuffers.end(), buffer) != m_subBuffers.end());

    const unsigned int newCount = static_cast<unsigned int>(buffer->m_data.size());
    unsigned int offset = 0;

    // On parcourt la liste des chunks jusqu'� trouver celui qui contient le buffer
    for (auto it = m_chunks.begin(); it != m_chunks.end(); ++it)
    {
        if (it->buffer == buffer)
        {
            if (it->count == newCount)
            {
                // La taille n'a pas chang�e
            }
            else if (it->count > newCount)
            {
                // La taille a diminu�, il faut mettre-�-jour les chunks de m�moire
                const unsigned int freeCount = it->count - newCount;

                auto itNext = std::next(it);
                if (itNext == m_chunks.end())
                {
                    // On ajoute un chunk vide � la fin de la liste
                    m_chunks.push_back(TChunk{ nullptr, freeCount });
                }
                else if (itNext->buffer == nullptr)
                {
                    // On peut aggrandir le chunk suivant
                    itNext->count += freeCount;
                }
                else
                {
                    // On ajoute un chunk vide apr�s ce chunk
                    m_chunks.insert(itNext, TChunk{ nullptr, freeCount });
                }

                it->count = newCount;
            }
            else
            {
                // La taille a augment�e
                bool needRealloc = true;

                auto itNext = std::next(it);
                if (itNext != m_chunks.end() && itNext->buffer == nullptr)
                {
                    if (it->count + itNext->count == newCount)
                    {
                        // On prend tout l'espace du chunk suivant
                        it->count = newCount;
                        m_chunks.erase(itNext);
                        needRealloc = false;
                    }
                    else if (it->count + itNext->count > newCount)
                    {
                        // On prend une partie de l'espace du chunk suivant
                        itNext->count -= (newCount - it->count);
                        it->count = newCount;
                        needRealloc = false;
                    }
                }

                if (needRealloc)
                {
                    // On r�alloue un chunk pour le buffer
                    m_chunks.erase(it);
                    offset = createChunkForBuffer(buffer, newCount);
                }
            }

            break;
        }

        offset += it->count;
    }

    if (offset >= 0)
    {
        m_buffer->updateSubData(buffer->m_data, offset);
    }
}


template<typename T>
void CMegaBuffer<T>::compact()
{
    // TODO
}


/**
 * Dessine le contenu du buffer.
 * Doit �tre appel�e depuis le thread principal.
 *
 * \param buffers Liste de buffers � afficher. Seuls les sous-buffers pr�sents dans ce mega buffer seront affich�s.
 * \param displayMode Mode de rendu. \todo R�cup�rer les param�tres depuis le CRenderer.
 * \param shader Shader � utiliser pour l'affichage (non nul).
 * \return Nombre de surfaces dessin�s.
 ******************************/

template<typename T>
unsigned int CMegaBuffer<T>::draw(const std::vector<const IBufferBase *>& buffers, TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const
{
    // Mise-�-jour des indices et des textures
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.clear();
    TBufferTextureVector& textures = m_buffer->getTextures();
    textures.clear();

    unsigned int offset = 0;

    for (const auto& chunk : m_chunks)
    {
        if (chunk.buffer != nullptr)
        {
            // V�rifie si le buffer doit �tre affich�
            for (const auto& buffer : buffers)
            {
                if (chunk.buffer == buffer)
                {
                    const auto subIndices = buffer->getIndices();
                    indices.reserve(indices.size() + subIndices.size());
                    for (const auto& index : subIndices)
                    {
                        indices.push_back(index + offset);
                    }

                    const auto subTextures = buffer->getTextures();
                    textures.reserve(subTextures.size());
                    textures.insert(textures.end(), subTextures.begin(), subTextures.end());

                    break;
                }
            }
        }

        offset += chunk.count;
    }

    m_buffer->update(false);

    // Affichage
    return m_buffer->draw(displayMode, shader);
}

} // Namespace TE
