/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <list>
#include <memory>

#include "Graphic/CBuffer2D.hpp"


namespace TE
{

class CShader;
class IGuiObject;


/**
 * \class   CGuiManager
 * \ingroup Graphic
 * \brief   Gestion des objets d'interface graphique.
 ******************************/

class TENGINE2_API CGuiManager
{
public:

    // Constructeurs et destructeur
    CGuiManager();
    ~CGuiManager() = default;
    CGuiManager(const CGuiManager&) = delete;
    CGuiManager(CGuiManager&&) = delete;
    CGuiManager& operator=(const CGuiManager&) = delete;
    CGuiManager& operator=(CGuiManager&&) = delete;

    void addObject(const std::shared_ptr<IGuiObject>& object);
    void removeObject(const std::shared_ptr<IGuiObject>& object);

    void update(float frameTime);
    void draw();

private:

    CBuffer2D m_buffer;                ///< Buffer graphique.
    std::shared_ptr<CShader> m_shader; ///< Shader.
    std::list<std::shared_ptr<IGuiObject>> m_objects; ///< Liste des objets graphiques.
};

} // Namespace TE
