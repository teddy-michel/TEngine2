/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>

#include "Physic/IShape.hpp"


namespace TE
{

/**
 * \class   CShapeCylinder
 * \ingroup Physic
 * \brief   Volume physique en forme de cylindre.
 ******************************/

class TENGINE2_API CShapeCylinder : public IShape
{
public:

    CShapeCylinder(float radius, float length);
    virtual ~CShapeCylinder() = default;

    inline float getRadius() const noexcept;
    void setRadius(float radius);

    inline float getLength() const noexcept;
    void setLength(float radius);

private:

    float m_radius; ///< Rayon du cylindre.
    float m_length; ///< Longueur du cylindre.
};


inline float CShapeCylinder::getRadius() const noexcept
{
    return m_radius;
}


inline float CShapeCylinder::getLength() const noexcept
{
    return m_length;
}

} // Namespace TE
