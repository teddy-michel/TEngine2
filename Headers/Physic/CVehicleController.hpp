/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>
#include <memory>

#include "library.h"
#include "Core/utils.hpp"


namespace TE
{

class IActor;
class CScene;
class CVehicleControllerImpl;
class IShape;


struct TENGINE2_API CWheelData
{
    float mass;     ///< Masse de la roue en kg.
    float radius;   ///< Rayon de la roue en m.
    float width;    ///< Largeur de la roue en m.
    glm::vec3 centerOffset;
    IShape * shape; ///< Volume de la roue.

    CWheelData() :
        mass         { 20.0f },
        radius       { 0.3f },
        width        { 0.05f },
        centerOffset { 0.0f, 0.0f, 0.0f },
        shape        { nullptr }
    { }
};


struct TENGINE2_API CVehicleData
{
    float mass;           ///< Masse du chassis en kg.
    bool useAutoGearFlag; ///< Indique si la boite de vitesse est automatique ou manuelle.
    float peakTorque;     ///< Couple maximal en Nm.
    float maxOmega;       ///< Vitesse maximale de rotation du moteur en rad/s.
    float gearSwitchTime; ///< Dur�e minimale pour changer de rapport de vitesse en secondes.
    float clutchStrength;
    IShape * shape;       ///< Volume du chassis.
    glm::vec3 dimensions; ///< Dimensions du volume englobant du chassis.
    CWheelData wheels[4]; ///< Description des 4 roues du v�hicule (front left, front right, rear left, rear right).
    // TODO: gears configuration

    CVehicleData() :
        mass            { 1000.0f },
        useAutoGearFlag { true },
        peakTorque      { 1500.0f },
        maxOmega        { 6000.0f * 2 * Pi / 60 },
        gearSwitchTime  { 0.5f },
        clutchStrength  { 10.0f },
        shape           { nullptr },
        dimensions      { 4.0f, 2.0f, 1.0f },
        wheels          {}
    { }
};


/**
 * \class   CVehicleController
 * \ingroup Physic
 * \brief   Classe permettant de g�rer la physique d'un v�hicule.
 ******************************/

class TENGINE2_API CVehicleController
{
    friend class CScene;
    friend class CVehicleControllerImpl;

public:

    glm::mat4 getTransform() const;
    void setTransform(const glm::mat4& transform);

    float getSpeed() const;
    float getEngineSpeed() const;
    int getCurrentGear() const;

    void updateInputs(float frameTime);
    std::shared_ptr<IActor> getActor() const;

    // Keyboard inputs
    inline void setDigitalAccel(bool accelKeyPressed) noexcept
    {
        m_accelKeyPressed = accelKeyPressed;
    }

    inline void setDigitalBrake(bool brakeKeyPressed) noexcept
    {
        m_brakeKeyPressed = brakeKeyPressed;
    }

    inline void setDigitalHandBrake(bool handBrakeKeyPressed) noexcept
    {
        m_handBrakeKeyPressed = handBrakeKeyPressed;
    }

    inline void setDigitalSteerLeft(bool steerLeftKeyPressed) noexcept
    {
        m_steerLeftKeyPressed = steerLeftKeyPressed;
    }

    inline void setDigitalSteerRight(bool steerRightKeyPressed) noexcept
    {
        m_steerRightKeyPressed = steerRightKeyPressed;
    }

    inline void setGearUp(bool gearUpKeyPressed) noexcept
    {
        m_gearUpKeyPressed = gearUpKeyPressed;
    }

    inline void setGearDown(bool gearDownKeyPressed) noexcept
    {
        m_gearDownKeyPressed = gearDownKeyPressed;
    }

    inline void toggleAutoGearFlag() noexcept
    {
        m_toggleAutoGears = true;
    }

private:

    CVehicleController();
    ~CVehicleController() = default;

    //std::unique_ptr<CVehicleControllerImpl> m_impl; ///< Impl�mentation priv�e.
    CVehicleControllerImpl * m_impl; ///< Impl�mentation priv�e.
    std::shared_ptr<IActor> m_actor; ///< Acteur associ� au contr�leur.

    // Keyboard inputs
    bool m_accelKeyPressed;
    bool m_brakeKeyPressed;
    bool m_handBrakeKeyPressed;
    bool m_steerLeftKeyPressed;
    bool m_steerRightKeyPressed;
    bool m_gearUpKeyPressed;
    bool m_gearDownKeyPressed;

    bool m_toggleAutoGears;
};

} // Namespace TE
