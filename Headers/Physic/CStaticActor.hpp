/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Physic/IActor.hpp"


namespace TE
{

/**
 * \class   CStaticActor
 * \ingroup Physic
 * \brief   Acteur statique.
 ******************************/

class TENGINE2_API CStaticActor : public IActor
{
public:

    explicit CStaticActor(IEntity * entity, const char * name = nullptr);
    CStaticActor(IEntity * entity, const glm::mat4& matrix, const char * name = nullptr);
    CStaticActor(IEntity * entity, const glm::mat4& matrix, const std::shared_ptr<IShape>& shape, const char * name = nullptr);
    virtual ~CStaticActor() = default;

    CStaticActor(const CStaticActor&) = delete;
    CStaticActor(CStaticActor&&) = delete;
    CStaticActor& operator=(const CStaticActor&) = delete;
    CStaticActor& operator=(CStaticActor&&) = delete;

private:

    virtual void addShapePriv(const std::shared_ptr<IShape>& shape) override;
    virtual void removeShapePriv(const std::shared_ptr<IShape>& shape) override;
};

} // Namespace TE
