/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <memory>

#include "library.h"


namespace TE
{

class IShape;
class IEntity;
class CScene;
class IActorImpl;


/**
 * \class   IActor
 * \ingroup Physic
 * \brief   Classe de base des objets physiques (acteurs).
 *
 * Un acteur contient des volumes (IShape).
 ******************************/

class TENGINE2_API IActor
{
    friend class CScene;

public:

    explicit IActor(IEntity * entity, const char * name = nullptr);
    IActor(IEntity * entity, std::unique_ptr<IActorImpl>&& impl);
    virtual ~IActor() = 0;

    IActor(const IActor&) = delete;
    IActor(IActor&&) = delete;
    IActor& operator=(const IActor&) = delete;
    IActor& operator=(IActor&&) = delete;

    glm::mat4 getTransform() const;
    void setTransform(const glm::mat4& matrix);

    void setLinearVelocity(const glm::vec3& velocity);

    void addShape(const std::shared_ptr<IShape>& shape);
    void removeShape(const std::shared_ptr<IShape>& shape);
    std::vector<std::shared_ptr<IShape>> getShapes() const;

    IEntity * getEntity() const;

protected:

    virtual void addShapePriv(const std::shared_ptr<IShape>& shape) = 0;
    virtual void removeShapePriv(const std::shared_ptr<IShape>& shape) = 0;

    std::unique_ptr<IActorImpl> m_impl; ///< Implémentation privée.
    std::vector<std::shared_ptr<IShape>> m_shapes;
    IEntity * m_entity;
};

} // Namespace TE
