/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "library.h"


namespace TE
{

class CPlayer;
class IActor;
class CScene;
class CPhysicEngineImpl;


/**
 * \class   CPhysicEngine
 * \ingroup Physic
 * \brief   Classe permettant de g�rer le moteur physique.
 ******************************/

class TENGINE2_API CPhysicEngine
{
public:

    CPhysicEngine();
    ~CPhysicEngine();

    CPhysicEngine(const CPhysicEngine&) = delete;
    CPhysicEngine(CPhysicEngine&&) = delete;
    CPhysicEngine& operator=(const CPhysicEngine&) = delete;
    CPhysicEngine& operator=(CPhysicEngine&&) = delete;

    void init();
    void close();
    void update(float frameTime);

    CScene * createScene(const char * name = nullptr);
    void setCurrentScene(CScene * scene);
    CScene * getCurrentScene() const;
    void removeScene(CScene * scene);

private:

    CPhysicEngineImpl * const m_impl; ///< Impl�mentation priv�e.
    CScene * m_currentScene;          ///< Pointeur sur la sc�ne courante.
    std::vector<CScene *> m_scenes;   ///< Liste des sc�nes.
};

/// Instance unique.
extern TENGINE2_API CPhysicEngine * gPhysicEngine;

} // Namespace TE
