/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <memory>
#include <glm/glm.hpp>

#include "library.h"


namespace TE
{

class CPhysicEngine;
class IActor;
class IEntity;
class CCharacterController;
class CVehicleController;
class CPlayer;
class CTrigger;
class CSceneImpl;
class CRaycastResult;
struct CVehicleData;


/**
 * \class   CScene
 * \ingroup Physic
 * \brief   Une sc�ne permet de g�rer la simulation physique d'objets.
 *
 * Une sc�ne contient des acteurs (IActor) qui interragissent entre-eux.
 ******************************/

class TENGINE2_API CScene
{
    friend class CPhysicEngine;
    friend class CSceneImpl;

public:

    void addActor(const std::shared_ptr<IActor>& actor);
    void removeActor(const std::shared_ptr<IActor>& actor);
    std::vector<std::shared_ptr<IActor>> getActors() const;

    CCharacterController * createCharacterController(CPlayer * player, float height, float radius);
    void removeCharacterController(CCharacterController * controller);

    CVehicleController * createVehicleController(IEntity * entity, const CVehicleData& data);
    void removeVehicleController(CVehicleController * controller);

    void update(float frameTime);

    glm::vec3 getGravity() const;
    void setGravity(const glm::vec3& gravity);

    bool raycast(const glm::vec3& position, const glm::vec3& direction, CRaycastResult& result);

private:

    void updateEntityTransform(IEntity * entity, const glm::mat4& t);
    void triggerEntity(CTrigger * trigger, IEntity * entity, bool enter);

    explicit CScene(const char * name = nullptr);
    ~CScene();

    //std::unique_ptr<CSceneImpl> m_impl;            ///< Impl�mentation priv�e.
    CSceneImpl * m_impl;                           ///< Impl�mentation priv�e.
    std::vector<std::shared_ptr<IActor>> m_actors; ///< Liste des acteurs.
};

} // Namespace TE
