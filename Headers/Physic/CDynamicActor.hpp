/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>

#include "Physic/IActor.hpp"


namespace physx
{
    class PxRigidDynamic;
}

namespace TE
{

class CScene;
class CDynamicActorImpl;


/**
 * \class   CDynamicActor
 * \ingroup Physic
 * \brief   Acteur dynamique, c'est-à-dire soumis à la physique.
 ******************************/

class TENGINE2_API CDynamicActor : public IActor
{
    friend class CScene;

public:

    explicit CDynamicActor(IEntity * entity, const char * name = nullptr);
    CDynamicActor(IEntity * entity, const glm::mat4& matrix, float mass = 1.0f, const char * name = nullptr);
    CDynamicActor(IEntity * entity, const glm::mat4& matrix, const std::shared_ptr<IShape>& shape, float mass, const char * name = nullptr);
    CDynamicActor(IEntity * entity, std::unique_ptr<CDynamicActorImpl>&& impl);
    virtual ~CDynamicActor() = default;

    CDynamicActor(const CDynamicActor&) = delete;
    CDynamicActor(CDynamicActor&&) = delete;
    CDynamicActor& operator=(const CDynamicActor&) = delete;
    CDynamicActor& operator=(CDynamicActor&&) = delete;

    float getMass() const;
    void setMass(float mass);

private:

    virtual void addShapePriv(const std::shared_ptr<IShape>& shape) override;
    virtual void removeShapePriv(const std::shared_ptr<IShape>& shape) override;
};

} // Namespace TE
