/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "Physic/IShape.hpp"


namespace TE
{

/**
 * \class   CShapeHeightField
 * \ingroup Physic
 * \brief   Volume physique permettant de g�rer un terrain.
 ******************************/

class TENGINE2_API CShapeHeightField : public IShape
{
public:

    CShapeHeightField(const std::vector<int16_t>& points, unsigned int subdivisionsX, unsigned int subdivisionsY, float heightScale, float xScale, float yScale);
    virtual ~CShapeHeightField() = default;
};

} // Namespace TE
