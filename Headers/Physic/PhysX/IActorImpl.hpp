/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>


namespace physx
{
    class PxRigidActor;
    class PxShape;
}

namespace TE
{

class IShapeImpl;
class IActor;


class IActorImpl
{
    friend class CSceneImpl;
    friend class CPhysicEngineImpl;

public:

    explicit IActorImpl(IActor * inst);
    explicit IActorImpl(physx::PxRigidActor * actor);
    virtual ~IActorImpl() = 0;

    virtual glm::mat4 getTransform() const;
    virtual void setTransform(const glm::mat4& matrix);

    virtual void setLinearVelocity(const glm::vec3& velocity);

    virtual void addShape(IShapeImpl * shape);
    virtual void removeShape(IShapeImpl * shape);

protected:

    IActor * const m_inst;
    physx::PxRigidActor * m_actor;
};

} // Namespace TE
