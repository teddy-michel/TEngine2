/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <map>


namespace physx
{
    class PxFoundation;
    class PxPhysics;
    class PxCooking;
    class PxDefaultCpuDispatcher;
    class PxPvd;
    class PxRigidActor;
}

namespace TE
{

class CPhysicEngine;
class IActorImpl;
class CPhysicEngineImpl;


extern CPhysicEngineImpl * gPhysicEngineImpl;


// Implémentation de CPhysicEngine pour PhysX.
class CPhysicEngineImpl
{
    friend class CSceneImpl;
    friend class CDynamicActorImpl;
    friend class CKinematicActorImpl;
    friend class CStaticActorImpl;
    friend class IActorImpl;
    friend class IShapeImpl;
    friend class CShapeCylinderImpl;
    friend class CShapeTriMeshImpl;
    friend class CShapeHeightFieldImpl;
    friend class CVehicleControllerImpl;

public:

    explicit CPhysicEngineImpl(CPhysicEngine * inst);
    ~CPhysicEngineImpl() = default;

    CPhysicEngineImpl(const CPhysicEngineImpl&) = delete;
    CPhysicEngineImpl(CPhysicEngineImpl&&) = delete;
    CPhysicEngineImpl& operator=(const CPhysicEngineImpl&) = delete;
    CPhysicEngineImpl& operator=(CPhysicEngineImpl&&) = delete;

    void init();
    void close();

    inline physx::PxFoundation* getFoundation() noexcept {
        return m_foundation;
    }

    IActorImpl * getActor(physx::PxRigidActor * actor) const;

private:

    void addActor(IActorImpl * actor);
    void removeActor(IActorImpl * actor);
    void initVehicleSdk();

    CPhysicEngine * const m_inst;
    physx::PxFoundation * m_foundation;
    physx::PxPhysics * m_physics;
    physx::PxCooking * m_cooking;
    physx::PxDefaultCpuDispatcher * m_dispatcher;
    physx::PxPvd * m_pvd;
    bool m_vehicleSdkInitialized;
    std::map<physx::PxRigidActor *, IActorImpl *> m_actors;
};

} // Namespace TE
