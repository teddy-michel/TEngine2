/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhysX/PxPhysicsAPI.h>
#include <glm/glm.hpp>
#include <memory>
#include <list>


namespace TE
{

class CCharacterController;
class CCharacterControllerImpl;
class CVehicleController;
class CVehicleControllerImpl;
class CRaycastResult;
struct CVehicleData;
class IEntity;
class CPlayer;
class CScene;
class CDynamicActor;
class IActorImpl;
class IShapeImpl;


// Implémentation de CScene pour PhysX.
class CSceneImpl : public physx::PxSimulationEventCallback
{
public:

    CSceneImpl(CScene * inst);
    ~CSceneImpl();

    void addActor(IActorImpl * actor);
    void removeActor(IActorImpl * actor);

    std::shared_ptr<CDynamicActor> initCharacterController(CCharacterControllerImpl * controller, CPlayer * player, float height, float radius);
    void removeCharacterController(CCharacterController * controller);

    std::shared_ptr<CDynamicActor> initVehicleController(CVehicleControllerImpl * controller, IEntity * entity, const CVehicleData& data, IShapeImpl * chassis, IShapeImpl * wheels[]);
    void removeVehicleController(CVehicleControllerImpl* controller);

    void update(float frameTime);

    glm::vec3 getGravity() const;
    void setGravity(const glm::vec3& gravity);

    bool raycast(const glm::vec3& position, const glm::vec3& direction, CRaycastResult& result);

    // From abstract class physx::PxSimulationEventCallback
    virtual void onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair * pairs, physx::PxU32 nbPairs) override {};
    virtual void onTrigger(physx::PxTriggerPair * pairs, physx::PxU32 count) override;
    virtual void onConstraintBreak(physx::PxConstraintInfo *, physx::PxU32) override {}
    virtual void onWake(physx::PxActor **, physx::PxU32) override {}
    virtual void onSleep(physx::PxActor **, physx::PxU32) override {}
    virtual void onAdvance(const physx::PxRigidBody * const *, const physx::PxTransform *, const physx::PxU32) override {}

private:

    CScene * const m_inst;
    physx::PxScene * m_scene;
    physx::PxControllerManager * m_controllerManager;
	std::list<CVehicleControllerImpl *> m_vehicles;
};

} // Namespace TE
