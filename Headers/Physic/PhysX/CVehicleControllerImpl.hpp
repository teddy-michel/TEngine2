/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhysX/PxPhysicsAPI.h>


namespace TE
{

//Tire types.
enum
{
    TIRE_TYPE_WETS = 0,
    TIRE_TYPE_SLICKS,
    TIRE_TYPE_ICE,
    TIRE_TYPE_MUD,
    MAX_NUM_TIRE_TYPES
};

//Drivable surface types.
enum
{
    SURFACE_TYPE_MUD = 0,
    SURFACE_TYPE_TARMAC,
    SURFACE_TYPE_SNOW,
    SURFACE_TYPE_GRASS,
    MAX_NUM_SURFACE_TYPES
};


class CVehicleController;
struct CVehicleData;
class IShapeImpl;

/*
//Make sure that suspension raycasts only consider shapes flagged as drivable that don't belong to the owner vehicle.
enum
{
    VEHICLE_DRIVABLE_SURFACE = 0xffff0000,
    VEHICLE_UNDRIVABLE_SURFACE = 0x0000ffff
};

static physx::PxQueryHitType::Enum SampleVehicleWheelRaycastPreFilter(
    physx::PxFilterData filterData0,
    physx::PxFilterData filterData1,
    const void* constantBlock, physx::PxU32 constantBlockSize,
    physx::PxHitFlags& queryFlags)
{
    //filterData0 is the vehicle suspension raycast.
    //filterData1 is the shape potentially hit by the raycast.
    PX_UNUSED(queryFlags);
    PX_UNUSED(constantBlockSize);
    PX_UNUSED(constantBlock);
    PX_UNUSED(filterData0);
    return ((0 == (filterData1.word3 & VEHICLE_DRIVABLE_SURFACE)) ? physx::PxQueryHitType::eNONE : physx::PxQueryHitType::eBLOCK);
}


//Data structure for quick setup of scene queries for suspension raycasts.
class VehicleSceneQueryData
{
public:

    //Allocate scene query data for up to maxNumWheels suspension raycasts.
    static VehicleSceneQueryData* allocate(const physx::PxU32 maxNumWheels);

    //Free allocated buffer for scene queries of suspension raycasts.
    void free();

    //Create a PxBatchQuery instance that will be used as a single batched raycast of multiple suspension lines of multiple vehicles
    physx::PxBatchQuery* setUpBatchedSceneQuery(physx::PxScene* scene);

    physx::PxBatchQuery* setUpBatchedSceneQuerySweep(physx::PxScene* scene);

    //Get the buffer of scene query results that will be used by PxVehicleNWSuspensionRaycasts
    physx::PxRaycastQueryResult* getRaycastQueryResultBuffer() { return mSqResults; }

    //Get the number of scene query results that have been allocated for use by PxVehicleNWSuspensionRaycasts
    physx::PxU32 getRaycastQueryResultBufferSize() const { return mNumQueries; }


    //Get the buffer of scene query results that will be used by PxVehicleNWSuspensionRaycasts
    physx::PxSweepQueryResult* getSweepQueryResultBuffer() { return mSqSweepResults; }

    //Get the number of scene query results that have been allocated for use by PxVehicleNWSuspensionRaycasts
    physx::PxU32 getSweepQueryResultBufferSize() const { return mNumSweepQueries; }

    //Set the pre-filter shader 
    void setPreFilterShader(physx::PxBatchQueryPreFilterShader preFilterShader) { mPreFilterShader = preFilterShader; }

private:

    //One result for each wheel.
    physx::PxRaycastQueryResult * mSqResults;
    physx::PxU32 mNbSqResults;

    //One hit for each wheel.
    physx::PxRaycastHit * mSqHitBuffer;

    physx::PxSweepQueryResult * mSqSweepResults;
    physx::PxU32 mNbSqSweepResults;
    physx::PxSweepHit * mSqSweepHitBuffer;

    //Filter shader used to filter drivable and non-drivable surfaces
    physx::PxBatchQueryPreFilterShader mPreFilterShader;

    //Maximum number of suspension raycasts that can be supported by the allocated buffers 
    //assuming a single query and hit per suspension line.
    physx::PxU32 mNumQueries;

    physx::PxU32 mNumSweepQueries;

    void init() noexcept
    {
        mPreFilterShader = SampleVehicleWheelRaycastPreFilter;
    }

    VehicleSceneQueryData()
    {
        init();
    }

    ~VehicleSceneQueryData() = default;
};


//Data structure to store reports for each wheel. 
class VehicleWheelQueryResults
{
public:

    //Allocate a buffer of wheel query results for up to maxNumWheels.
    static VehicleWheelQueryResults* allocate(const physx::PxU32 maxNumWheels);

    //Free allocated buffer.
    void free();

    physx::PxWheelQueryResult* addVehicle(const physx::PxU32 numWheels);

private:

    //One result for each wheel.
    physx::PxWheelQueryResult* mWheelQueryResults;

    //Maximum number of wheels.
    physx::PxU32 mMaxNumWheels;

    //Number of wheels 
    physx::PxU32 mNumWheels;

    VehicleWheelQueryResults()
        : mWheelQueryResults(nullptr), mMaxNumWheels(0), mNumWheels(0)
    {
        init();
    }

    ~VehicleWheelQueryResults() = default;

    void init()
    {
        mWheelQueryResults = nullptr;
        mMaxNumWheels = 0;
        mNumWheels = 0;
    }
};
*/

class CVehicleControllerImpl
{
    friend class CSceneImpl;

public:

    explicit CVehicleControllerImpl(CVehicleController * inst);
    ~CVehicleControllerImpl();

    CVehicleControllerImpl(const CVehicleControllerImpl&) = delete;
    CVehicleControllerImpl(CVehicleControllerImpl&&) = delete;
    CVehicleControllerImpl& operator=(const CVehicleControllerImpl&) = delete;
    CVehicleControllerImpl& operator=(CVehicleControllerImpl&&) = delete;

    float getSpeed() const;
    float getEngineSpeed() const;
    int getCurrentGear() const;

    void updateInputs(float frameTime);
    void init(const CVehicleData& data, IShapeImpl * chassis, IShapeImpl * wheels[]);

protected:

    void update(float frameTime, physx::PxScene * scene);

private:

    void createVehicle4WSimulationData(const CVehicleData& data,
                                       physx::PxVehicleWheelsSimData& wheelsData,
                                       physx::PxVehicleDriveSimData4W& driveData,
                                       physx::PxVehicleChassisData& chassisData);

    CVehicleController * const m_inst;
    physx::PxVehicleDrive4W * m_vehicle;
    physx::PxRigidDynamic * m_actor;
    physx::PxVehicleWheelQueryResult m_wheelQueryResults;
    physx::PxVehicleDrivableSurfaceType m_drivableSurfaceTypes[MAX_NUM_SURFACE_TYPES];

    // SDK raycasts (for the suspension lines)
    //VehicleSceneQueryData * mSqData;
    //physx::PxBatchQuery * mSqWheelRaycastBatchQuery;

    // Reports for each wheel
    //VehicleWheelQueryResults * mWheelQueryResults;

    // Cached simulation data of focus vehicle in 4W mode
    //physx::PxVehicleWheelsSimData * mWheelsSimData4W;
    //physx::PxVehicleDriveSimData4W mDriveSimData4W;

    // Friction from combinations of tire and surface types
    physx::PxVehicleDrivableSurfaceToTireFrictionPairs * m_surfacesFriction;
    bool m_inReverseMode;
    bool m_isMovingForwardSlowly;
};

} // Namespace TE
