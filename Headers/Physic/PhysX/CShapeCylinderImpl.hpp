/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Physic/PhysX/IShapeImpl.hpp"


namespace physx
{
	class PxConvexMesh;
}

namespace TE
{

class CShapeCylinder;


class CShapeCylinderImpl : public IShapeImpl
{
public:

	CShapeCylinderImpl(CShapeCylinder * inst);
    ~CShapeCylinderImpl() = default;

	void updateShape(float radius, float length);

private:

	physx::PxConvexMesh * createCylinderConvexMesh(float radius, float length, int numCirclePoints) const;
};

} // Namespace TE
