/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>


namespace physx
{
    class PxCapsuleController;
}

namespace TE
{

class CCharacterController;


class CCharacterControllerImpl
{
    friend class CSceneImpl;

public:

    CCharacterControllerImpl(CCharacterController * inst);
    ~CCharacterControllerImpl();
    
    glm::vec3 getPosition();
    bool move(const glm::vec3& translation, float elapsedTime);
    void setPosition(const glm::vec3& position);

    void resize(float height);
    void setRadius(float radius);

private:

    CCharacterController * const m_inst;
    physx::PxCapsuleController * m_controller; ///< Contr�leur physique du joueur.
    float m_radius; ///< Rayon de la capsule (en cm). \todo : convertir en m�tres ?
    float m_height; ///< Hauteur de la capsule (en cm). \todo : convertir en m�tres ?
};

} // Namespace TE
