/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>
#include <memory>

#include "library.h"


namespace TE
{

class IActor;
class CScene;
class CCharacterControllerImpl;


/**
 * \class   CCharacterController
 * \ingroup Physic
 * \brief   Classe permettant de g�rer la physique d'un personnage.
 ******************************/

class TENGINE2_API CCharacterController
{
    friend class CScene;

public:

    glm::vec3 getPosition();
    bool move(const glm::vec3& translation, float elapsedTime);
    void setPosition(const glm::vec3& position);

    void resize(float height);
    void setRadius(float radius);

    std::shared_ptr<IActor> getActor() const;

private:

    CCharacterController();
    ~CCharacterController() = default;

    //std::unique_ptr<CCharacterControllerImpl> m_impl; ///< Impl�mentation priv�e.
    CCharacterControllerImpl * m_impl; ///< Impl�mentation priv�e.
    std::shared_ptr<IActor> m_actor;   ///< Acteur associ� au contr�leur.
};

} // Namespace TE
