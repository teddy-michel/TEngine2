/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "library.h"


namespace TE
{

class IShapeImpl;


/**
 * \class   IShape
 * \ingroup Physic
 * \brief   Classe de base des volumes physiques.
 ******************************/

class TENGINE2_API IShape
{
    friend class IActor;
    friend class CDynamicActor;
    friend class CKinematicActor;
    friend class CStaticActor;
    friend class CScene;

public:

    IShape();
    virtual ~IShape() = 0;

    void setTrigger(bool trigger);
    bool isTrigger() const;

protected:

    std::unique_ptr<IShapeImpl> m_impl; ///< Implémentation privée.
    bool m_trigger;
};

} // Namespace TE
