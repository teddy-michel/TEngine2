/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp> // pour utiliser glm::vec3

#include "library.h"


namespace TE
{

class IActor;
class IShape;


/**
 * \class   CRaycastResult
 * \ingroup Physic
 * \brief   Encapsule les résultats d'un lancer de rayon.
 ******************************/

class TENGINE2_API CRaycastResult
{
public:

    IActor * actor;
    IShape * shape;
    float distance;
    glm::vec3 position;
    glm::vec3 normal;

    CRaycastResult() : actor{ nullptr }, shape{ nullptr }, distance{ 0.0f }, position{}, normal{} { }
};

} // Namespace TE
