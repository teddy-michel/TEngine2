/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>

#include "Physic/IActor.hpp"


namespace TE
{

/**
 * \class   CKinematicActor
 * \ingroup Physic
 * \brief   Acteur cinématique, c'est-à-dire pouvant être déplace manuellement.
 ******************************/

class TENGINE2_API CKinematicActor : public IActor
{
public:

    explicit CKinematicActor(IEntity * entity, const char * name = nullptr);
    CKinematicActor(IEntity * entity, const glm::mat4& matrix, const char * name = nullptr);
    CKinematicActor(IEntity * entity, const glm::mat4& matrix, const std::shared_ptr<IShape>& shape, const char * name = nullptr);
    virtual ~CKinematicActor() = default;

    CKinematicActor(const CKinematicActor&) = delete;
    CKinematicActor(CKinematicActor&&) = delete;
    CKinematicActor& operator=(const CKinematicActor&) = delete;
    CKinematicActor& operator=(CKinematicActor&&) = delete;

private:

    virtual void addShapePriv(const std::shared_ptr<IShape>& shape) override;
    virtual void removeShapePriv(const std::shared_ptr<IShape>& shape) override;
};

} // Namespace TE
