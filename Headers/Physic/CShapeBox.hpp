/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <glm/glm.hpp>

#include "Physic/IShape.hpp"


namespace TE
{

/**
 * \class   CShapeBox
 * \ingroup Physic
 * \brief   Volume physique en forme de boite.
 ******************************/

class TENGINE2_API CShapeBox : public IShape
{
public:

    explicit CShapeBox(const glm::vec3& size);
    virtual ~CShapeBox() = default;

    glm::vec3 getSize() const;
    void setSize(const glm::vec3& size);

private:

    glm::vec3 m_size; ///< Dimensions du volume.
};

} // Namespace TE
