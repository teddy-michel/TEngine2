/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/type_ptr.hpp>

#include "Physic/PhysX/CSceneImpl.hpp"
#include "Physic/CScene.hpp"
#include "Physic/CCharacterController.hpp"
#include "Physic/CDynamicActor.hpp"
#include "Physic/CRaycastResult.hpp"
#include "Physic/IShape.hpp"
#include "Physic/CVehicleController.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IActorImpl.hpp"
#include "Physic/PhysX/CCharacterControllerImpl.hpp"
#include "Physic/PhysX/CDynamicActorImpl.hpp"
#include "Physic/PhysX/CVehicleControllerImpl.hpp"
#include "Entities/IEntity.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CTrigger.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CSceneImpl::CSceneImpl(CScene * inst) :
    m_inst              { inst },
    m_scene             { nullptr },
    m_controllerManager { nullptr },
    m_vehicles          {}
{
    assert(m_inst != nullptr);

    physx::PxSceneDesc sceneDesc(gPhysicEngineImpl->m_physics->getTolerancesScale());
    sceneDesc.gravity = physx::PxVec3(0.0f, 0.0f, -9.81f * 100);
    sceneDesc.cpuDispatcher = gPhysicEngineImpl->m_dispatcher;
    sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;
    sceneDesc.simulationEventCallback = this;

    m_scene = gPhysicEngineImpl->m_physics->createScene(sceneDesc);

    if (m_scene == nullptr)
    {
        gApplication->log(CString::fromUTF8("Impossible de cr�er la sc�ne physique."), ILogger::Error);
        return;
    }

    gApplication->log("Create physic scene.");

    m_scene->setFlag(physx::PxSceneFlag::eENABLE_ACTIVE_ACTORS, true);
    m_scene->setFlag(physx::PxSceneFlag::eEXCLUDE_KINEMATICS_FROM_ACTIVE_ACTORS, true);

#ifdef T_DEBUG
    physx::PxPvdSceneClient * pvdClient = m_scene->getScenePvdClient();
    if (pvdClient)
    {
        pvdClient->setScenePvdFlag(physx::PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
        pvdClient->setScenePvdFlag(physx::PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
        pvdClient->setScenePvdFlag(physx::PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
    }
#endif

    // Add floor (DEBUG)
    /*
    CGame::instance().log("Create physic floor on (0, 0, -500).");
    m_material = getDefaultMaterial();
    physx::PxRigidStatic * groundPlane = physx::PxCreatePlane(*m_physics, physx::PxPlane(0.0f, 0.0f, 1.0f, 500.0f), *m_material);
    groundPlane->setName("floor");
    addActor(*groundPlane);
    */

    m_scene->userData = this;

    // Character controller manager
    m_controllerManager = PxCreateControllerManager(*m_scene);
    assert(m_controllerManager != nullptr);
    //m_controllerManager->setOverlapRecoveryModule(true);
    //m_controllerManager->setPreciseSweeps(true);
    m_controllerManager->setTessellation(true, 50.0f);
}


/**
 * Destructeur.
 ******************************/

CSceneImpl::~CSceneImpl()
{
    for (auto& vehicle : m_vehicles)
    {
        vehicle->m_vehicle->release();
    }

    m_controllerManager->release();
    m_scene->release();
}


void CSceneImpl::addActor(IActorImpl * actor)
{
    assert(actor != nullptr);
    m_scene->addActor(*actor->m_actor);
}


void CSceneImpl::removeActor(IActorImpl * actor)
{
    assert(actor != nullptr);
    m_scene->removeActor(*actor->m_actor);
}


/**
 * Initialise un contr�leur de personnage.
 *
 * \param controller Contr�leur de personnage (doit �tre non nul).
 * \param player Entit� du joueur associ�e au personnage (doit �tre non nul).
 * \return Acteur dynamique du personnage.
 ******************************/

std::shared_ptr<CDynamicActor> CSceneImpl::initCharacterController(CCharacterControllerImpl * controller, CPlayer * player, float height, float radius)
{
    assert(controller != nullptr);
    assert(player != nullptr);

    physx::PxCapsuleControllerDesc desc;
    desc.height = height - 2 * radius;
    desc.radius = radius;
    desc.userData = player;
    desc.nonWalkableMode = physx::PxControllerNonWalkableMode::ePREVENT_CLIMBING_AND_FORCE_SLIDING;
    desc.stepOffset = 10.0f;
    desc.slopeLimit = std::cos(45.0f * Pi / 180.0f);
    desc.upDirection = physx::PxVec3{ 0.0f, 0.0f, 1.0f };
    desc.material = gPhysicEngineImpl->m_physics->createMaterial(0.5f, 0.5f, 0.6f);

    if (desc.isValid())
    {
        physx::PxController * privController = m_controllerManager->createController(desc);
        assert(privController != nullptr);

        physx::PxRigidDynamic * privActor = privController->getActor();
        assert(privActor != nullptr);

        auto actorImpl = std::make_unique<CDynamicActorImpl>(privActor, player, player->getName().toCharArray());
        auto actor = std::make_shared<CDynamicActor>(player, std::move(actorImpl));

        controller->m_controller = static_cast<physx::PxCapsuleController *>(privController);
        return actor;
    }
    else
    {
        //gApplication->log(CString::fromUTF8("Le descripteur du contr�leur de personnage n'est pas valide."), ILogger::Error);
        return nullptr;
    }
}


void CSceneImpl::removeCharacterController(CCharacterController * controller)
{
    assert(controller != nullptr);
}


std::shared_ptr<CDynamicActor> CSceneImpl::initVehicleController(CVehicleControllerImpl * controller, IEntity * entity, const CVehicleData& data, IShapeImpl * chassis, IShapeImpl * wheels[])
{
    assert(controller != nullptr);
    assert(entity != nullptr);

    m_vehicles.push_back(controller);

    controller->init(data, chassis, wheels);
    auto actorImpl = std::make_unique<CDynamicActorImpl>(controller->m_actor, entity);
    auto actor = std::make_shared<CDynamicActor>(entity, std::move(actorImpl));
    return actor;
}


void CSceneImpl::removeVehicleController(CVehicleControllerImpl * controller)
{
    assert(controller != nullptr);
    m_vehicles.remove(controller);
}


/**
 * Met-�-jour la simulation physique de la sc�ne.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CSceneImpl::update(float frameTime)
{
    if (frameTime > 0.0f)
    {
        m_scene->simulate(frameTime);
        m_scene->fetchResults(true);

        // Update vehicles
        for (auto& vehicle : m_vehicles)
        {
            vehicle->update(frameTime, m_scene);
        }

        // Retrieve array of actors that moved
        physx::PxU32 nbActiveActors;
        physx::PxActor ** actors = m_scene->getActiveActors(nbActiveActors);

        // Update each entity with the new transform
        for (physx::PxU32 i = 0; i < nbActiveActors; ++i)
        {
            if (actors[i] != nullptr && actors[i]->userData != nullptr)
            {
                physx::PxRigidActor * actor = static_cast<physx::PxRigidActor *>(actors[i]); // Danger : static_cast !
                //if (actor != nullptr)
                {
                    IEntity * entity = reinterpret_cast<IEntity *>(actor->userData); // TODO: IActor -> IEntity ?
                    //entity->setMatrix(glm::make_mat4(physx::PxMat44(actor->getGlobalPose()).front()));
                    m_inst->updateEntityTransform(entity, glm::make_mat4(physx::PxMat44(actor->getGlobalPose()).front()));
                    //entity->updateWorldMatrixByPhysic(glm::make_mat4(physx::PxMat44(actor->getGlobalPose()).front()));
                }
            }
        }
    }
}


glm::vec3 CSceneImpl::getGravity() const
{
    auto g = m_scene->getGravity();
    return { g.x, g.y, g.z };
}


void CSceneImpl::setGravity(const glm::vec3& gravity)
{
    m_scene->setGravity({ gravity.x, gravity.y, gravity.z });
}


bool CSceneImpl::raycast(const glm::vec3& position, const glm::vec3& direction, CRaycastResult& result)
{
    // Raycast against all static & dynamic objects (no filtering)
    // The main result from this call is the closest hit, stored in the 'hit.block' structure
    physx::PxRaycastBuffer hit;
    bool status = m_scene->raycast({ position.x, position.y, position.z }, { direction.x, direction.y, direction.z }, 999999.0f, hit);

    if (hit.block.actor != nullptr)
    {
        IActorImpl * actor = gPhysicEngineImpl->getActor(hit.block.actor);
        if (actor != nullptr)
        {
            result.actor = actor->m_inst;
            result.shape = static_cast<IShape *>(hit.block.shape->userData);
        }
    }

    result.distance = hit.block.distance;
    result.position = { hit.block.position.x, hit.block.position.y, hit.block.position.z };
    result.normal = { hit.block.normal.x, hit.block.normal.y, hit.block.normal.z };

    return status;
}


void CSceneImpl::onTrigger(physx::PxTriggerPair * pairs, physx::PxU32 count)
{
    for (physx::PxU32 i = 0; i < count; ++i)
    {
        // ignore pairs when shapes have been deleted
        if (pairs[i].flags & (physx::PxTriggerPairFlag::eREMOVED_SHAPE_TRIGGER | physx::PxTriggerPairFlag::eREMOVED_SHAPE_OTHER))
        {
            continue;
        }

        assert(pairs[i].triggerActor->userData != nullptr);
        assert(pairs[i].otherActor->userData != nullptr);

        CTrigger * trigger = static_cast<CTrigger *>(pairs[i].triggerActor->userData);
        IEntity * entity = static_cast<IEntity *>(pairs[i].otherActor->userData);

        //pairs[i].status = eNOTIFY_TOUCH_FOUND / eNOTIFY_TOUCH_LOST
        gApplication->log(CString("Trigger '%1' with entity '%2' (status = %3).").arg(trigger->getName()).arg(entity->getName()).arg(pairs[i].status), ILogger::Debug);
        m_inst->triggerEntity(trigger, entity, pairs[i].status == physx::PxPairFlag::eNOTIFY_TOUCH_FOUND);
    }
}

} // Namespace TE
