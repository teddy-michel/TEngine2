/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>

#include "Physic/PhysX/CShapeTriMeshImpl.hpp"
#include "Physic/CShapeTriMesh.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param vertices Liste des sommets.
 * \param indices Liste des indices de sommets.
 ******************************/

CShapeTriMeshImpl::CShapeTriMeshImpl(CShapeTriMesh * inst, const std::vector<glm::vec3>& vertices, const std::vector<unsigned int>& indices) :
    IShapeImpl{ inst }
{
    physx::PxTriangleMeshDesc meshDesc;
    meshDesc.points.count = vertices.size();
    meshDesc.points.stride = sizeof(physx::PxVec3);
    meshDesc.points.data = &vertices[0];
    meshDesc.flags = physx::PxMeshFlag::eFLIPNORMALS;

    meshDesc.triangles.count = indices.size() / 3;
    meshDesc.triangles.stride = 3 * sizeof(physx::PxU32);
    meshDesc.triangles.data = &indices[0];

    physx::PxDefaultMemoryOutputStream writeBuffer;
    physx::PxTriangleMeshCookingResult::Enum result;
    bool status = gPhysicEngineImpl->m_cooking->cookTriangleMesh(meshDesc, writeBuffer, &result);
    if (!status)
    {
        gApplication->log("CShapeTriMeshImpl - can't create trimesh.", ILogger::Error);
        return;
    }

    physx::PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
    auto trimesh = gPhysicEngineImpl->m_physics->createTriangleMesh(readBuffer);

    physx::PxTriangleMeshGeometry geometry{ trimesh };
    createShape(geometry);
}

} // Namespace TE
