/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <PhysX/PxPhysicsAPI.h>

#include "Physic/PhysX/IShapeImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 ******************************/

IShapeImpl::IShapeImpl(IShape * inst) :
    m_inst  { inst },
    m_shape { nullptr }
{
    assert(m_inst != nullptr);
}


/**
 * Destructeur.
 ******************************/

IShapeImpl::~IShapeImpl()
{
    if (m_shape != nullptr)
    {
        m_shape->release();
    }
}


void IShapeImpl::setTrigger(bool trigger)
{
    if (trigger)
    {
        m_shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
        m_shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
    }
    else
    {
        m_shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, false);
        m_shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, true);
    }
}


void IShapeImpl::createShape(const physx::PxGeometry& geometry)
{
    if (m_shape == nullptr)
    {
        physx::PxMaterial * material = gPhysicEngineImpl->m_physics->createMaterial(0.5f, 0.5f, 0.6f);
        m_shape = gPhysicEngineImpl->m_physics->createShape(geometry, *material);
        m_shape->userData = m_inst;
    }
}

} // Namespace TE
