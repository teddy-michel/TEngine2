/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>

#include "Physic/PhysX/CShapeHeightFieldImpl.hpp"
#include "Physic/CShapeHeightField.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CShapeHeightFieldImpl::CShapeHeightFieldImpl(CShapeHeightField * inst, const std::vector<int16_t>& points, unsigned int subdivisionsX, unsigned int subdivisionsY, float heightScale, float xScale, float yScale) :
    IShapeImpl{ inst }
{
    physx::PxHeightFieldSample * samples = new physx::PxHeightFieldSample[(subdivisionsX + 1) * (subdivisionsY + 1)];

    for (unsigned int y = 0; y <= subdivisionsY; ++y)
    {
        for (unsigned int x = 0; x <= subdivisionsX; ++x)
        {
            samples[x * (subdivisionsY + 1) + y].height = points[y * (subdivisionsX + 1) + x];
            samples[x * (subdivisionsY + 1) + y].materialIndex0 = 0;
            samples[x * (subdivisionsY + 1) + y].materialIndex1 = 0;
        }
    }

    physx::PxHeightFieldDesc hfDesc;
    hfDesc.format         = physx::PxHeightFieldFormat::eS16_TM;
    hfDesc.nbColumns      = subdivisionsY + 1;
    hfDesc.nbRows         = subdivisionsX + 1;
    hfDesc.samples.data   = samples;
    hfDesc.samples.stride = sizeof(physx::PxHeightFieldSample);

    physx::PxHeightField * heightField = gPhysicEngineImpl->m_cooking->createHeightField(hfDesc, gPhysicEngineImpl->m_physics->getPhysicsInsertionCallback());
    physx::PxHeightFieldGeometry geometry(heightField, physx::PxMeshGeometryFlags(), heightScale, xScale, yScale);

    delete[] samples;

    createShape(geometry);
}

} // Namespace TE
