/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <cassert>

#include "Physic/PhysX/CShapeBoxImpl.hpp"
#include "Physic/CShapeBox.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CShapeBoxImpl::CShapeBoxImpl(CShapeBox * inst) :
    IShapeImpl{ inst }
{
    physx::PxBoxGeometry geometry{ 1.0f, 1.0f, 1.0f };
    createShape(geometry);
}


/**
 * Modifie les dimensions de la boite.
 *
 * \param size Dimensions de la boite.
 ******************************/

void CShapeBoxImpl::setSize(const glm::vec3& size)
{
    assert(m_shape != nullptr);
    physx::PxBoxGeometry geometry{ size.x / 2, size.y / 2, size.z / 2 };
    m_shape->setGeometry(geometry);
}

} // Namespace TE
