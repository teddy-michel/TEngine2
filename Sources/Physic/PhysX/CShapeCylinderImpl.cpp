/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <PhysX/foundation/PxMath.h>
#include <cassert>

#include "Physic/PhysX/CShapeCylinderImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/CShapeCylinder.hpp"


namespace TE
{

constexpr int MaxNumVerticesInCircle = 32;


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CShapeCylinderImpl::CShapeCylinderImpl(CShapeCylinder * inst) :
    IShapeImpl{ inst }
{
    physx::PxConvexMeshGeometry geometry{ createCylinderConvexMesh(1.0f, 1.0f, 8) };
    createShape(geometry);
}


void CShapeCylinderImpl::updateShape(float radius, float length)
{
    assert(m_shape != nullptr);
    physx::PxConvexMeshGeometry geometry{ createCylinderConvexMesh(radius, length, 8) };
    m_shape->setGeometry(geometry);
}


physx::PxConvexMesh * CShapeCylinderImpl::createCylinderConvexMesh(float radius, float length, int numCirclePoints) const
{
    assert(numCirclePoints > 2 && numCirclePoints < MaxNumVerticesInCircle);

    physx::PxVec3 verts[2 * MaxNumVerticesInCircle];
    const float dtheta = 2.0f * physx::PxPi / static_cast<float>(numCirclePoints);
    for (int i = 0; i < numCirclePoints; ++i)
    {
        const float theta = dtheta * i;
        const float cosTheta = radius * std::cos(theta);
        const float sinTheta = radius * std::sin(theta);
        verts[2 * i + 0] = physx::PxVec3{ cosTheta, sinTheta, -0.5f * length };
        verts[2 * i + 1] = physx::PxVec3{ cosTheta, sinTheta,  0.5f * length };
    }

    // Create descriptor for convex mesh
    physx::PxConvexMeshDesc convexDesc;
    convexDesc.points.count = 2 * numCirclePoints;
    convexDesc.points.stride = sizeof(physx::PxVec3);
    convexDesc.points.data = verts;
    convexDesc.flags = physx::PxConvexFlag::eCOMPUTE_CONVEX;

    physx::PxConvexMesh* convexMesh = nullptr;
    physx::PxDefaultMemoryOutputStream buf;
    if (gPhysicEngineImpl->m_cooking->cookConvexMesh(convexDesc, buf))
    {
        physx::PxDefaultMemoryInputData data(buf.getData(), buf.getSize());
        convexMesh = gPhysicEngineImpl->m_physics->createConvexMesh(data);
    }

    return convexMesh;
}

} // Namespace TE
