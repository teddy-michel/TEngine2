/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <cassert>
#include <glm/gtc/type_ptr.hpp>

#include "Physic/PhysX/CStaticActorImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IShapeImpl.hpp"
#include "Physic/IShape.hpp"
#include "Physic/CStaticActor.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CStaticActorImpl::CStaticActorImpl(CStaticActor * inst, IEntity * entity, const char * name) :
    IActorImpl{ inst }
{
    m_actor = gPhysicEngineImpl->m_physics->createRigidStatic(physx::PxTransform{ physx::PxIdentity });
    assert(m_actor != nullptr);
    gPhysicEngineImpl->addActor(this);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param name Nom utilis� pour le debug.
 ******************************/

CStaticActorImpl::CStaticActorImpl(CStaticActor * inst, IEntity * entity, const glm::mat4& matrix, const char * name) :
    IActorImpl{ inst }
{
    m_actor = gPhysicEngineImpl->m_physics->createRigidStatic(physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))));
    assert(m_actor != nullptr);
    gPhysicEngineImpl->addActor(this);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param shape Volume � utiliser pour l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CStaticActorImpl::CStaticActorImpl(CStaticActor * inst, IEntity * entity, const glm::mat4& matrix, IShapeImpl * shape, const char * name) :
    IActorImpl{ inst }
{
    m_actor = physx::PxCreateStatic(*gPhysicEngineImpl->m_physics, physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))), *shape->m_shape);
    assert(m_actor != nullptr);
    gPhysicEngineImpl->addActor(this);
    m_actor->setName(name);
    m_actor->userData = entity;
}

} // Namespace TE
