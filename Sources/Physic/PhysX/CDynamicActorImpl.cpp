/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <cassert>
#include <glm/gtc/type_ptr.hpp>

#include "Physic/PhysX/CDynamicActorImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IShapeImpl.hpp"
#include "Physic/IShape.hpp"
#include "Physic/CDynamicActor.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CDynamicActorImpl::CDynamicActorImpl(CDynamicActor * inst, IEntity * entity, const char * name) :
    IActorImpl   { inst },
    m_canRelease { true },
    m_mass       { 0.0f }
{
    physx::PxRigidDynamic * actor = gPhysicEngineImpl->m_physics->createRigidDynamic(physx::PxTransform{ physx::PxIdentity });
    m_actor = actor;
    gPhysicEngineImpl->addActor(this);
    actor->setMass(1.0f);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param mass Masse de l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CDynamicActorImpl::CDynamicActorImpl(CDynamicActor * inst, IEntity * entity, const glm::mat4& matrix, float mass, const char * name) :
    IActorImpl   { inst },
    m_canRelease { true },
    m_mass       { mass >= 0.0f ? mass : 0.0f }
{
    physx::PxRigidDynamic * actor = gPhysicEngineImpl->m_physics->createRigidDynamic(physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))));
    m_actor = actor;
    gPhysicEngineImpl->addActor(this);
    actor->setMass(m_mass);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param shape Volume � utiliser pour l'acteur.
 * \param mass Masse de l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CDynamicActorImpl::CDynamicActorImpl(CDynamicActor * inst, IEntity * entity, const glm::mat4& matrix, IShapeImpl * shape, float mass, const char * name) :
    IActorImpl   { inst },
    m_canRelease { true },
    m_mass       { mass >= 0.0f ? mass : 0.0f }
{
    m_actor = physx::PxCreateDynamic(*gPhysicEngineImpl->m_physics, physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))), *shape->m_shape, m_mass);
    gPhysicEngineImpl->addActor(this);
    m_actor->setName(name);
    m_actor->userData = entity;
}


CDynamicActorImpl::CDynamicActorImpl(physx::PxRigidDynamic * actor, IEntity * entity, const char * name) :
    IActorImpl   { actor },
    m_canRelease { false },
    m_mass       { 1.0f }
{
    assert(m_actor != nullptr);
    m_actor->userData = entity;
    m_actor->setName(name);
}


/**
 * Destructeur.
 ******************************/

CDynamicActorImpl::~CDynamicActorImpl()
{
    if (m_canRelease && m_actor != nullptr)
    {
        m_actor->release();
    }
}


/**
 * Retourne la masse de l'objet.
 *
 * \return Masse de l'objet.
 ******************************/

float CDynamicActorImpl::getMass() const
{
    return m_mass;
}


/**
 * Modifie la masse de l'acteur.
 *
 * \param mass Masse de l'acteur en kg.
 ******************************/

void CDynamicActorImpl::setMass(float mass)
{
    if (mass >= 0.0f && mass != m_mass)
    {
        m_mass = mass;
        static_cast<physx::PxRigidDynamic *>(m_actor)->setMass(mass);
    }
}


/**
 * Modifie la vitesse lin�aire de l'acteur.
 *
 * \param velocity Vitesse lin�aire.
 ******************************/

void CDynamicActorImpl::setLinearVelocity(const glm::vec3& velocity)
{
    static_cast<physx::PxRigidDynamic*>(m_actor)->setLinearVelocity(physx::PxVec3{ velocity.x, velocity.y, velocity.z });
}


/**
 * Ajoute un volume � l'acteur.
 *
 * \param shape Volume � ajouter.
 ******************************/

void CDynamicActorImpl::addShape(IShapeImpl * shape)
{
    assert(shape != nullptr);

    if (m_actor != nullptr)
    {
        m_actor->attachShape(*shape->m_shape);
        physx::PxRigidBody * actor = static_cast<physx::PxRigidBody *>(m_actor);
        physx::PxRigidBodyExt::setMassAndUpdateInertia(*actor, m_mass);
    }
}


/**
 * Enl�ve un volume � l'acteur.
 *
 * \param shape Volume � enlever.
 ******************************/

void CDynamicActorImpl::removeShape(IShapeImpl * shape)
{
    assert(shape != nullptr);

    if (m_actor != nullptr)
    {
        m_actor->detachShape(*shape->m_shape);
        physx::PxRigidBody * actor = static_cast<physx::PxRigidBody *>(m_actor);
        physx::PxRigidBodyExt::setMassAndUpdateInertia(*actor, m_mass);
    }
}

} // Namespace TE
