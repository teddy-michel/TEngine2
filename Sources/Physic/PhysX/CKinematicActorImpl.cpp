/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <cassert>
#include <glm/gtc/type_ptr.hpp>

#include "Physic/PhysX/CKinematicActorImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IShapeImpl.hpp"
#include "Physic/IShape.hpp"
#include "Physic/CKinematicActor.hpp"


namespace TE
{
    
/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CKinematicActorImpl::CKinematicActorImpl(CKinematicActor * inst, IEntity * entity, const char * name) :
    IActorImpl{ inst }
{
    physx::PxRigidDynamic * actor = gPhysicEngineImpl->m_physics->createRigidDynamic(physx::PxTransform{ physx::PxIdentity });
    m_actor = actor;
    gPhysicEngineImpl->addActor(this);
    actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
    actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eUSE_KINEMATIC_TARGET_FOR_SCENE_QUERIES, true);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param name Nom utilis� pour le debug.
 ******************************/

CKinematicActorImpl::CKinematicActorImpl(CKinematicActor * inst, IEntity * entity, const glm::mat4& matrix, const char * name) :
    IActorImpl{ inst }
{
    physx::PxRigidDynamic * actor = gPhysicEngineImpl->m_physics->createRigidDynamic(physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))));
    m_actor = actor;
    gPhysicEngineImpl->addActor(this);
    actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
    actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eUSE_KINEMATIC_TARGET_FOR_SCENE_QUERIES, true);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param shape Volume � utiliser pour l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CKinematicActorImpl::CKinematicActorImpl(CKinematicActor * inst, IEntity * entity, const glm::mat4& matrix, IShapeImpl * shape, const char * name) :
    IActorImpl{ inst }
{
    physx::PxRigidDynamic * actor = physx::PxCreateDynamic(*gPhysicEngineImpl->m_physics, physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))), *shape->m_shape, 1.0f);
    m_actor = actor;
    gPhysicEngineImpl->addActor(this);
    actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
    actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eUSE_KINEMATIC_TARGET_FOR_SCENE_QUERIES, true);
    m_actor->setName(name);
    m_actor->userData = entity;
}


/**
 * Destructeur.
 ******************************/

CKinematicActorImpl::~CKinematicActorImpl()
{
    if (m_actor != nullptr)
    {
        m_actor->release();
    }
}


void CKinematicActorImpl::setTransform(const glm::mat4& matrix)
{
    if (m_actor != nullptr)
    {
        physx::PxRigidDynamic * actor = static_cast<physx::PxRigidDynamic *>(m_actor);
        // PxMat44 prend un "float[]" en param�tre au lieu d'un "const float[]"...
        actor->setKinematicTarget(physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))));
        //m_actor->setGlobalPose(physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))));
    }
}


/**
 * Modifie la vitesse lin�aire de l'acteur.
 *
 * \param velocity Vitesse lin�aire.
 ******************************/

void CKinematicActorImpl::setLinearVelocity(const glm::vec3& velocity)
{
    static_cast<physx::PxRigidDynamic*>(m_actor)->setLinearVelocity(physx::PxVec3{ velocity.x, velocity.y, velocity.z });
}

} // Namespace TE
