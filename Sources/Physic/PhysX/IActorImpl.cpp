/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <glm/gtc/type_ptr.hpp>

#include "Physic/PhysX/IActorImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IShapeImpl.hpp"
#include "Physic/IShape.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

IActorImpl::IActorImpl(IActor * inst) :
    m_inst  { inst },
    m_actor { nullptr }
{
    assert(m_inst != nullptr);
    gPhysicEngineImpl->addActor(this);
}


/**
 * Constructeur.
 ******************************/

IActorImpl::IActorImpl(physx::PxRigidActor * actor) :
    m_inst  { nullptr },
    m_actor { actor }
{
    gPhysicEngineImpl->addActor(this);
}


/**
 * Destructeur.
 ******************************/

IActorImpl::~IActorImpl()
{
    gPhysicEngineImpl->removeActor(this);
}


glm::mat4 IActorImpl::getTransform() const
{
    if (m_actor != nullptr)
    {
        return glm::make_mat4(physx::PxMat44(m_actor->getGlobalPose()).front());
    }

    return glm::mat4{ 1.0f };
}


void IActorImpl::setTransform(const glm::mat4& matrix)
{
    if (m_actor != nullptr)
    {
        // PxMat44 prend un "float[]" en param�tre au lieu d'un "const float[]"...
        m_actor->setGlobalPose(physx::PxTransform(physx::PxMat44(const_cast<float *>(glm::value_ptr(matrix)))));
    }
}


/**
 * Modifie la vitesse lin�aire de l'acteur.
 *
 * \param velocity Vitesse lin�aire.
 ******************************/

void IActorImpl::setLinearVelocity(const glm::vec3& velocity)
{
    T_UNUSED(velocity);
    return;
}


/**
 * Ajoute un volume � l'acteur.
 *
 * \param shape Volume � ajouter.
 ******************************/

void IActorImpl::addShape(IShapeImpl * shape)
{
    assert(shape != nullptr);

    if (m_actor != nullptr)
    {
        m_actor->attachShape(*shape->m_shape);
        //physx::PxRigidBody * actor = static_cast<physx::PxRigidBody *>(m_actor);
        //physx::PxRigidBodyExt::setMassAndUpdateInertia(*actor, m_mass);
    }
}


void IActorImpl::removeShape(IShapeImpl * shape)
{
    assert(shape != nullptr);

    if (m_actor != nullptr)
    {
        m_actor->detachShape(*shape->m_shape);
    }
}

} // Namespace TE
