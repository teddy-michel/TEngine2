/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <PhysX/vehicle/PxVehicleUtil.h>

#include "Physic/PhysX/CVehicleControllerImpl.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IShapeImpl.hpp"
#include "Physic/CVehicleController.hpp"
#include "Physic/IShape.hpp"


namespace TE
{

/*
VehicleSceneQueryData * VehicleSceneQueryData::allocate(const physx::PxU32 maxNumWheels)
{
    VehicleSceneQueryData * sqData = new VehicleSceneQueryData();
    sqData->init();
    sqData->mSqResults = new physx::PxRaycastQueryResult[maxNumWheels];
    sqData->mNbSqResults = maxNumWheels;
    sqData->mSqHitBuffer = new physx::PxRaycastHit[maxNumWheels];
    sqData->mNumQueries = maxNumWheels;
    sqData->mSqSweepResults = new physx::PxSweepQueryResult[maxNumWheels];
    sqData->mNbSqSweepResults = maxNumWheels;
    sqData->mSqSweepHitBuffer = new physx::PxSweepHit[maxNumWheels];
    sqData->mNumSweepQueries = maxNumWheels;
    return sqData;
}

void VehicleSceneQueryData::free()
{
    delete(this);
}

physx::PxBatchQuery * VehicleSceneQueryData::setUpBatchedSceneQuery(physx::PxScene* scene)
{
    physx::PxBatchQueryDesc sqDesc(mNbSqResults, 0, 0);
    sqDesc.queryMemory.userRaycastResultBuffer = mSqResults;
    sqDesc.queryMemory.userRaycastTouchBuffer = mSqHitBuffer;
    sqDesc.queryMemory.raycastTouchBufferSize = mNumQueries;
    sqDesc.preFilterShader = mPreFilterShader;
    return scene->createBatchQuery(sqDesc);
}

physx::PxBatchQuery * VehicleSceneQueryData::setUpBatchedSceneQuerySweep(physx::PxScene* scene)
{
    physx::PxBatchQueryDesc sqDesc(0, mNbSqResults, 0);
    sqDesc.queryMemory.userSweepResultBuffer = mSqSweepResults;
    sqDesc.queryMemory.userSweepTouchBuffer = mSqSweepHitBuffer;
    sqDesc.queryMemory.sweepTouchBufferSize = mNumQueries;
    sqDesc.preFilterShader = mPreFilterShader;
    return scene->createBatchQuery(sqDesc);
}
*/

/*
//Collision types and flags describing collision interactions of each collision type.
enum
{
    COLLISION_FLAG_GROUND = 1 << 0,
    COLLISION_FLAG_WHEEL = 1 << 1,
    COLLISION_FLAG_CHASSIS = 1 << 2,
    COLLISION_FLAG_OBSTACLE = 1 << 3,
    COLLISION_FLAG_DRIVABLE_OBSTACLE = 1 << 4,

    COLLISION_FLAG_GROUND_AGAINST = COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
    COLLISION_FLAG_WHEEL_AGAINST = COLLISION_FLAG_WHEEL | COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE,
    COLLISION_FLAG_CHASSIS_AGAINST = COLLISION_FLAG_GROUND | COLLISION_FLAG_WHEEL | COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
    COLLISION_FLAG_OBSTACLE_AGAINST = COLLISION_FLAG_GROUND | COLLISION_FLAG_WHEEL | COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
    COLLISION_FLAG_DRIVABLE_OBSTACLE_AGAINST = COLLISION_FLAG_GROUND | COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
};
*/
// ---------------------------------------------------------------------


struct TireFrictionMultipliers
{
    static float getValue(physx::PxU32 surfaceType, physx::PxU32 tireType)
    {
        // Tire model friction for each combination of drivable surface type and tire type
        static physx::PxF32 tireFrictionMultipliers[MAX_NUM_SURFACE_TYPES][MAX_NUM_TIRE_TYPES] =
        {
            //WETS   SLICKS ICE    MUD
            { 0.95f, 0.95f, 0.95f, 0.95f },  // MUD
            { 1.10f, 1.15f, 1.10f, 1.10f },  // TARMAC
            { 0.70f, 0.70f, 0.70f, 0.70f },  // ICE
            { 0.80f, 0.80f, 0.80f, 0.80f }   // GRASS
        };
        return tireFrictionMultipliers[surfaceType][tireType];
    }
};


static const physx::PxVehicleKeySmoothingData gKeySmoothingData =
{
    {
        5.0f,  // rise rate eANALOG_INPUT_ACCEL
        5.0f,  // rise rate eANALOG_INPUT_BRAKE
        10.0f, // rise rate eANALOG_INPUT_HANDBRAKE
        2.5f,  // rise rate eANALOG_INPUT_STEER_LEFT
        2.5f,  // rise rate eANALOG_INPUT_STEER_RIGHT
    },
    {
        7.0f,  // fall rate eANALOG_INPUT_ACCEL
        7.0f,  // fall rate eANALOG_INPUT_BRAKE
        10.0f, // fall rate eANALOG_INPUT_HANDBRAKE
        5.0f,  // fall rate eANALOG_INPUT_STEER_LEFT
        5.0f   // fall rate eANALOG_INPUT_STEER_RIGHT
    }
};


static constexpr physx::PxF32 gSteerVsForwardSpeedData[2 * 8] =
{
    0.0f,       0.75f,
    5.0f,       0.75f,
    30.0f,      0.125f,
    120.0f,     0.1f,
    PX_MAX_F32, PX_MAX_F32,
    PX_MAX_F32, PX_MAX_F32,
    PX_MAX_F32, PX_MAX_F32,
    PX_MAX_F32, PX_MAX_F32
};

physx::PxFixedSizeLookupTable<8> gSteerVsForwardSpeedTable(gSteerVsForwardSpeedData, 4);


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CVehicleControllerImpl::CVehicleControllerImpl(CVehicleController * inst) :
    m_inst                  { inst },
    m_vehicle               { nullptr },
    m_actor                 { nullptr },
    m_wheelQueryResults     {},
    m_surfacesFriction      { nullptr },
    m_inReverseMode         { false },
    m_isMovingForwardSlowly { true }
{
    assert(m_inst != nullptr);

    gPhysicEngineImpl->initVehicleSdk();

    // Scene query data for to allow raycasts for all suspensions of all vehicles
    //VehicleSceneQueryData * mSqData = VehicleSceneQueryData::allocate(4);

    // Data to store reports for each wheel
    //VehicleWheelQueryResults * mWheelQueryResults = VehicleWheelQueryResults::allocate(4);

    const physx::PxF32 restitutions[MAX_NUM_SURFACE_TYPES] = { 0.2f, 0.2f, 0.2f, 0.2f };
    const physx::PxF32 staticFrictions[MAX_NUM_SURFACE_TYPES] = { 0.5f, 0.5f, 0.5f, 0.5f };
    const physx::PxF32 dynamicFrictions[MAX_NUM_SURFACE_TYPES] = { 0.5f, 0.5f, 0.5f, 0.5f };
    physx::PxMaterial * drivableSurfaceMaterials[MAX_NUM_SURFACE_TYPES];

    for (physx::PxU32 i = 0; i < MAX_NUM_SURFACE_TYPES; ++i)
    {
        // Create a new material
        drivableSurfaceMaterials[i] = gPhysicEngineImpl->m_physics->createMaterial(staticFrictions[i], dynamicFrictions[i], restitutions[i]);
        // Set up the drivable surface type that will be used for the new material
        m_drivableSurfaceTypes[i].mType = i;
    }

    // Set up the friction values arising from combinations of tire type and surface type
    m_surfacesFriction = physx::PxVehicleDrivableSurfaceToTireFrictionPairs::allocate(MAX_NUM_TIRE_TYPES, MAX_NUM_SURFACE_TYPES);
    m_surfacesFriction->setup(MAX_NUM_TIRE_TYPES, MAX_NUM_SURFACE_TYPES, (const physx::PxMaterial **) drivableSurfaceMaterials, m_drivableSurfaceTypes);
    for (physx::PxU32 i = 0; i < MAX_NUM_SURFACE_TYPES; ++i)
    {
        for (physx::PxU32 j = 0; j < MAX_NUM_TIRE_TYPES; ++j)
        {
            m_surfacesFriction->setTypePairFriction(i, j, 1.3f * TireFrictionMultipliers::getValue(i, j));
        }
    }
}


/**
 * Destructeur.
 ******************************/

CVehicleControllerImpl::~CVehicleControllerImpl()
{
    if (m_vehicle != nullptr)
    {
        m_vehicle->release();
    }

    delete[] m_wheelQueryResults.wheelQueryResults;
}


/**
 * Calcule la vitesse du v�hicule.
 *
 * \return Vitesse du v�hicule en m/s.
 ******************************/

float CVehicleControllerImpl::getSpeed() const
{
    return m_vehicle->computeForwardSpeed();
}


/**
 * Calcule la vitesse de rotation du moteur.
 *
 * \return Vitesse de rotation du moteur en tours/min.
 ******************************/

float CVehicleControllerImpl::getEngineSpeed() const
{
    return 60 * m_vehicle->mDriveDynData.getEngineRotationSpeed() / (2 * physx::PxPi);
}


/**
 * Retourne le rapport de vitesse actuel.
 *
 * \return Rapport de vitesse. -1 pour la marche arri�re, 0 pour le neutre, et entre 1 et 5 pour les vitesses.
 ******************************/

int CVehicleControllerImpl::getCurrentGear() const
{
    return static_cast<int>(m_vehicle->mDriveDynData.getCurrentGear());
}


constexpr float THRESHOLD_FORWARD_SPEED = 0.1f;
constexpr float THRESHOLD_SIDEWAYS_SPEED = 0.2f;
constexpr float THRESHOLD_ROLLING_BACKWARDS_SPEED = 0.1f;


/**
 * Met-�-jour le v�hicule en fonction des commandes.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CVehicleControllerImpl::updateInputs(float frameTime)
{
    // Toggle autogear flag
    if (m_inst->m_toggleAutoGears)
    {
        m_vehicle->mDriveDynData.toggleAutoGears();
        m_inst->m_toggleAutoGears = false;
    }

    const bool isInAir = physx::PxVehicleIsInAir(m_wheelQueryResults);

    // Store raw inputs in replay stream if in recording mode.
    // Set raw inputs from replay stream if in replay mode.
    // Store raw inputs from active stream in handy arrays so we don't need to worry
    // about which stream (live input or replay) is active.
    // Work out if we are using keys or gamepad controls depending on which is being used
    // (gamepad selected if both are being used).
    physx::PxVehicleDrive4WRawInputData carRawInputs;
    carRawInputs.setDigitalAccel(m_inst->m_accelKeyPressed);
    carRawInputs.setDigitalBrake(m_inst->m_brakeKeyPressed);
    carRawInputs.setDigitalHandbrake(m_inst->m_handBrakeKeyPressed);
    carRawInputs.setDigitalSteerLeft(m_inst->m_steerLeftKeyPressed);
    carRawInputs.setDigitalSteerRight(m_inst->m_steerRightKeyPressed);

    bool toggleAutoReverse = false;
    bool newIsMovingForwardSlowly = false;

    if (m_vehicle->mDriveDynData.getUseAutoGears())
    {
        carRawInputs.setGearDown(false);
        carRawInputs.setGearUp(false);

        // Work out if the car is to flip from reverse to forward gear or from forward gear to reverse
        {
            // If the car is travelling very slowly in forward gear without player input and the player subsequently presses the brake then we want the car to go into reverse gear
            // If the car is travelling very slowly in reverse gear without player input and the player subsequently presses the accel then we want the car to go into forward gear
            // If the car is in forward gear and is travelling backwards then we want to automatically put the car into reverse gear.
            // If the car is in reverse gear and is travelling forwards then we want to automatically put the car into forward gear.
            // (If the player brings the car to rest with the brake the player needs to release the brake then reapply it
            // to indicate they want to toggle between forward and reverse.)

            const bool prevIsMovingForwardSlowly = m_isMovingForwardSlowly;
            bool isMovingForwardSlowly = false;
            bool isMovingBackwards = false;
            if (!isInAir)
            {
                const bool accelRaw = carRawInputs.getAnalogAccel();
                const bool brakeRaw = carRawInputs.getDigitalBrake();
                const bool handbrakeRaw = carRawInputs.getDigitalHandbrake();

                const physx::PxF32 forwardSpeed = m_vehicle->computeForwardSpeed();
                const physx::PxF32 forwardSpeedAbs = physx::PxAbs(forwardSpeed);
                const physx::PxF32 sidewaysSpeedAbs = physx::PxAbs(m_vehicle->computeSidewaysSpeed());
                const physx::PxU32 currentGear = m_vehicle->mDriveDynData.getCurrentGear();
                const physx::PxU32 targetGear = m_vehicle->mDriveDynData.getTargetGear();

                // Check if the car is rolling against the gear (backwards in forward gear or forwards in reverse gear).
                if (physx::PxVehicleGearsData::eFIRST == currentGear && forwardSpeed < -THRESHOLD_ROLLING_BACKWARDS_SPEED)
                {
                    isMovingBackwards = true;
                }
                else if (physx::PxVehicleGearsData::eREVERSE == currentGear && forwardSpeed > THRESHOLD_ROLLING_BACKWARDS_SPEED)
                {
                    isMovingBackwards = true;
                }

                // Check if the car is moving slowly.
                if (forwardSpeedAbs < THRESHOLD_FORWARD_SPEED && sidewaysSpeedAbs < THRESHOLD_SIDEWAYS_SPEED)
                {
                    isMovingForwardSlowly = true;
                }

                // Now work if we need to toggle from forwards gear to reverse gear or vice versa.
                if (isMovingBackwards)
                {
                    if (!accelRaw && !brakeRaw && !handbrakeRaw && (currentGear == targetGear))
                    {
                        // The car is rolling against the gear and the player is doing nothing to stop this.
                        toggleAutoReverse = true;
                    }
                }
                else if (prevIsMovingForwardSlowly && isMovingForwardSlowly)
                {
                    if ((currentGear > physx::PxVehicleGearsData::eNEUTRAL) && brakeRaw && !accelRaw && (currentGear == targetGear))
                    {
                        // The car was moving slowly in forward gear without player input and is now moving slowly with player input that indicates the 
                        // player wants to switch to reverse gear.
                        toggleAutoReverse = true;
                    }
                    else if (currentGear == physx::PxVehicleGearsData::eREVERSE && accelRaw && !brakeRaw && (currentGear == targetGear))
                    {
                        // The car was moving slowly in reverse gear without player input and is now moving slowly with player input that indicates the 
                        // player wants to switch to forward gear.
                        toggleAutoReverse = true;
                    }
                }

                // If the car was brought to rest through braking and the player is still braking then start the process that will lead to toggling 
                // between reverse and forward gears.
                if (isMovingForwardSlowly)
                {
                    if (currentGear >= physx::PxVehicleGearsData::eFIRST && brakeRaw)
                    {
                        newIsMovingForwardSlowly = true;
                    }
                    else if (currentGear == physx::PxVehicleGearsData::eREVERSE && accelRaw)
                    {
                        newIsMovingForwardSlowly = true;
                    }
                }
            }
        }
    }
    else
    {
        carRawInputs.setGearUp(m_inst->m_gearUpKeyPressed);
        carRawInputs.setGearDown(m_inst->m_gearDownKeyPressed);
    }

    m_isMovingForwardSlowly = newIsMovingForwardSlowly;

    // If the car is to flip gear direction then switch gear as appropriate
    if (toggleAutoReverse)
    {
        m_inReverseMode = !m_inReverseMode;

        if (m_inReverseMode)
        {
            m_vehicle->mDriveDynData.forceGearChange(physx::PxVehicleGearsData::eREVERSE);
        }
        else
        {
            m_vehicle->mDriveDynData.forceGearChange(physx::PxVehicleGearsData::eFIRST);
        }
    }

    // If in reverse mode then swap the accel and brake
    if (m_inReverseMode)
    {
        carRawInputs.setDigitalAccel(carRawInputs.getDigitalBrake());
        carRawInputs.setDigitalBrake(carRawInputs.getDigitalAccel());
    }

    // Now filter the raw input values and apply them to focus vehicle
    // as floats for brake,accel,handbrake,steer and bools for gearup,geardown.
    physx::PxVehicleDrive4WSmoothDigitalRawInputsAndSetAnalogInputs(gKeySmoothingData, gSteerVsForwardSpeedTable, carRawInputs, frameTime, isInAir, *m_vehicle);
}


void CVehicleControllerImpl::init(const CVehicleData& data, IShapeImpl * chassis, IShapeImpl * wheels[])
{
    physx::PxVehicleWheelsSimData * wheelsSimData = physx::PxVehicleWheelsSimData::allocate(4);
    physx::PxVehicleDriveSimData4W driveSimData;
    physx::PxVehicleChassisData chassisData;
    createVehicle4WSimulationData(data, *wheelsSimData, driveSimData, chassisData);

    // We need a rigid body actor for the vehicle.
    // Don't forget to add the actor the scene after setting up the associated vehicle.
    m_actor = gPhysicEngineImpl->m_physics->createRigidDynamic(physx::PxTransform(physx::PxIdentity));
    assert(m_actor);

    //physx::PxFilterData wheelCollFilterData;
    //wheelCollFilterData.word0 = COLLISION_FLAG_WHEEL;
    //wheelCollFilterData.word1 = COLLISION_FLAG_WHEEL_AGAINST;
    //wheelCollFilterData.word2 = physx::PxPairFlag::eMODIFY_CONTACTS;

    // We need to add chassis collision shapes, their local poses, a material for the chassis, and a simulation filter for the chassis.
    //physx::PxFilterData chassisCollFilterData;
    //chassisCollFilterData.word0 = COLLISION_FLAG_CHASSIS;
    //chassisCollFilterData.word1 = COLLISION_FLAG_CHASSIS_AGAINST;

    // Create a query filter data for the car to ensure that cars
    // do not attempt to drive on themselves.
    //physx::PxFilterData vehQryFilterData;
    //VehicleSetupVehicleShapeQueryFilterData(&vehQryFilterData);

    // Add all the wheel shapes to the actor.
    for (physx::PxU32 i = 0; i < 4; i++)
    {
        m_actor->attachShape(*(wheels[i]->m_shape));
        wheels[i]->m_shape->release();
        //wheels[i]->m_shape->setQueryFilterData(vehQryFilterData);
        wheels[i]->m_shape->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, false);
        //wheels[i]->m_shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
        //wheels[i]->m_shape->setSimulationFilterData(wheelCollFilterData);
        wheels[i]->m_shape->setLocalPose(physx::PxTransform(physx::PxIdentity));
    }

    // Add the chassis shape to the actor
    m_actor->attachShape(*(chassis->m_shape));
    chassis->m_shape->release();
    chassis->m_shape->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, false);
    //chassis->m_shape->setQueryFilterData(vehQryFilterData);
    //chassis->m_shape->setSimulationFilterData(chassisCollFilterData);
    chassis->m_shape->setLocalPose(physx::PxTransform(physx::PxIdentity));

    m_actor->setMass(chassisData.mMass);
    m_actor->setMassSpaceInertiaTensor(chassisData.mMOI);
    m_actor->setCMassLocalPose(physx::PxTransform(chassisData.mCMOffset, physx::PxQuat(physx::PxIdentity)));

    // Create a car
    physx::PxVehicleDrive4W * car = physx::PxVehicleDrive4W::allocate(4);
    m_vehicle = car;
    car->setup(gPhysicEngineImpl->m_physics, m_actor, *wheelsSimData, driveSimData, 0);

    // Free the sim data because we don't need that any more.
    wheelsSimData->free();

    // Set up the mapping between wheel and actor shape.
    car->mWheelsSimData.setWheelShapeMapping(0, 0);
    car->mWheelsSimData.setWheelShapeMapping(1, 1);
    car->mWheelsSimData.setWheelShapeMapping(2, 2);
    car->mWheelsSimData.setWheelShapeMapping(3, 3);

    // Set up the scene query filter data for each suspension line.
    //physx::PxFilterData vehQryFilterData;
    //VehicleSetupVehicleShapeQueryFilterData(&vehQryFilterData);
    //car->mWheelsSimData.setSceneQueryFilterData(0, vehQryFilterData);
    //car->mWheelsSimData.setSceneQueryFilterData(1, vehQryFilterData);
    //car->mWheelsSimData.setSceneQueryFilterData(2, vehQryFilterData);
    //car->mWheelsSimData.setSceneQueryFilterData(3, vehQryFilterData);

    // Set the autogear mode of the instantiate car.
    car->mDriveDynData.setUseAutoGears(data.useAutoGearFlag);
    car->mDriveDynData.forceGearChange(physx::PxVehicleGearsData::eFIRST);

    // Increment the number of vehicles
    m_wheelQueryResults.nbWheelQueryResults = 4;
    m_wheelQueryResults.wheelQueryResults = new physx::PxWheelQueryResult[4];


    // ???
    physx::PxQuat rotation(physx::PxPi / 2.0f, physx::PxVec3(0.0f, 0.0f, 1.0f));

    physx::PxD6Joint* joint = physx::PxD6JointCreate(*gPhysicEngineImpl->m_physics, m_actor, physx::PxTransform(rotation), nullptr, physx::PxTransform(rotation));

    joint->setMotion(physx::PxD6Axis::eX, physx::PxD6Motion::eFREE);
    joint->setMotion(physx::PxD6Axis::eY, physx::PxD6Motion::eFREE);
    joint->setMotion(physx::PxD6Axis::eZ, physx::PxD6Motion::eFREE);

    joint->setMotion(physx::PxD6Axis::eTWIST, physx::PxD6Motion::eFREE);
    joint->setMotion(physx::PxD6Axis::eSWING1, physx::PxD6Motion::eLIMITED);
    joint->setMotion(physx::PxD6Axis::eSWING2, physx::PxD6Motion::eLIMITED);

    physx::PxJointLimitCone limitCone(physx::PxPi / 4.0f, physx::PxPi / 4.0f);
    joint->setSwingLimit(limitCone);
}


void CVehicleControllerImpl::update(float frameTime, physx::PxScene * scene)
{
    assert(m_vehicle != nullptr);
    physx::PxVehicleWheels * vehicles[1] = { m_vehicle };
    physx::PxVehicleUpdates(frameTime, scene->getGravity(), *m_surfacesFriction, 1, vehicles, &m_wheelQueryResults);
}


void CVehicleControllerImpl::createVehicle4WSimulationData(const CVehicleData& data,
    physx::PxVehicleWheelsSimData& wheelsData,
    physx::PxVehicleDriveSimData4W& driveData,
    physx::PxVehicleChassisData& chassisData)
{
    // The origin is at the center of the chassis mesh.
    // Set the center of mass to be below this point and a little towards the front.
    const physx::PxVec3 chassisCMOffset = physx::PxVec3{ 0.25f, 0.0f, -data.dimensions.z * 0.5f + 0.65f };

    // Now compute the chassis mass and moment of inertia.
    // Use the moment of inertia of a cuboid as an approximate value for the chassis moi.
    physx::PxVec3 chassisMOI{ (data.dimensions.y * data.dimensions.y + data.dimensions.z * data.dimensions.z) * data.mass / 12.0f,
                              (data.dimensions.x * data.dimensions.x + data.dimensions.z * data.dimensions.z) * data.mass / 12.0f,
                              (data.dimensions.x * data.dimensions.x + data.dimensions.y * data.dimensions.y) * data.mass / 12.0f };
    // A bit of tweaking here.  The car will have more responsive turning if we reduce the
    // y-component of the chassis moment of inertia.
    chassisMOI.y *= 0.8f;

    // Let's set up the chassis data structure now.
    chassisData.mMass = data.mass;
    chassisData.mMOI = chassisMOI; // TODO: parametrize?
    chassisData.mCMOffset = chassisCMOffset; // TODO: parametrize?

    // Compute the sprung masses of each suspension spring using a helper function.
    physx::PxF32 suspSprungMasses[4];
    physx::PxVec3 wheelCentreOffsets[4];
    wheelCentreOffsets[0] = physx::PxVec3{ data.wheels[0].centerOffset.x, data.wheels[0].centerOffset.y, data.wheels[0].centerOffset.z };
    wheelCentreOffsets[1] = physx::PxVec3{ data.wheels[1].centerOffset.x, data.wheels[1].centerOffset.y, data.wheels[1].centerOffset.z };
    wheelCentreOffsets[2] = physx::PxVec3{ data.wheels[2].centerOffset.x, data.wheels[2].centerOffset.y, data.wheels[2].centerOffset.z };
    wheelCentreOffsets[3] = physx::PxVec3{ data.wheels[3].centerOffset.x, data.wheels[3].centerOffset.y, data.wheels[3].centerOffset.z };
    physx::PxVehicleComputeSprungMasses(4, wheelCentreOffsets, chassisCMOffset, data.mass, 1, suspSprungMasses);

    // Now compute the wheel masses and inertias components around the axle's axis.
    // http://en.wikipedia.org/wiki/List_of_moments_of_inertia
    physx::PxF32 wheelMOIs[4];
    for (physx::PxU32 i = 0; i < 4; i++)
    {
        wheelMOIs[i] = 0.5f * data.wheels[i].mass * data.wheels[i].radius * data.wheels[i].radius;
    }

    // Let's set up the wheel data structures now with radius, mass, and moi.
    physx::PxVehicleWheelData wheels[4];
    for (physx::PxU32 i = 0; i < 4; i++)
    {
        wheels[i].mRadius = data.wheels[i].radius;
        wheels[i].mMass = data.wheels[i].mass;
        wheels[i].mMOI = wheelMOIs[i];
        wheels[i].mWidth = data.wheels[i].width;
    }

    // Wheels brake torque
    wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mMaxBrakeTorque = 1500.0f;  // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mMaxBrakeTorque = 1500.0f; // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mMaxBrakeTorque = 1500.0f;   // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mMaxBrakeTorque = 1500.0f;  // TODO: parametrize

    // Disable the handbrake from the front wheels and enable for the rear wheels
    wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mMaxHandBrakeTorque = 0.0f;    // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mMaxHandBrakeTorque = 0.0f;   // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mMaxHandBrakeTorque = 4000.0f;  // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mMaxHandBrakeTorque = 4000.0f; // TODO: parametrize

    // Enable steering for the front wheels and disable for the front wheels.
    wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mMaxSteer = physx::PxPi * 0.3333f;  // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mMaxSteer = physx::PxPi * 0.3333f; // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mMaxSteer = 0.0f;                    // TODO: parametrize
    wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mMaxSteer = 0.0f;                   // TODO: parametrize

    // Let's set up the tire data structures now.
    // Put slicks on the front tires and wets on the rear tires.
    physx::PxVehicleTireData tires[4];
    tires[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mType = TIRE_TYPE_SLICKS;  // TODO: parametrize
    tires[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mType = TIRE_TYPE_SLICKS; // TODO: parametrize
    tires[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mType = TIRE_TYPE_WETS;     // TODO: parametrize
    tires[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mType = TIRE_TYPE_WETS;    // TODO: parametrize

    // Let's set up the suspension data structures now.
    physx::PxVehicleSuspensionData susps[4];
    for (physx::PxU32 i = 0; i < 4; i++)
    {
        susps[i].mMaxCompression = 0.3f;      // TODO: parametrize
        susps[i].mMaxDroop = 0.1f;            // TODO: parametrize
        susps[i].mSpringStrength = 35000.0f;  // TODO: parametrize
        susps[i].mSpringDamperRate = 4500.0f; // TODO: parametrize
    }

    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mSprungMass = suspSprungMasses[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT];
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mSprungMass = suspSprungMasses[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT];
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mSprungMass = suspSprungMasses[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT];
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mSprungMass = suspSprungMasses[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT];

    // Set up the camber.
    // Remember that the left and right wheels need opposite camber so that the car preserves symmetry about the forward direction.
    // Set the camber to 0.0f when the spring is neither compressed or elongated.
    const physx::PxF32 camberAngleAtRest = 0.0;
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mCamberAtRest = camberAngleAtRest;
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mCamberAtRest = -camberAngleAtRest;
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mCamberAtRest = camberAngleAtRest;
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mCamberAtRest = -camberAngleAtRest;

    // Set the wheels to camber inwards at maximum droop (the left and right wheels almost form a V shape)
    const physx::PxF32 camberAngleAtMaxDroop = 0.001f;
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mCamberAtMaxDroop = camberAngleAtMaxDroop;
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mCamberAtMaxDroop = -camberAngleAtMaxDroop;
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mCamberAtMaxDroop = camberAngleAtMaxDroop;
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mCamberAtMaxDroop = -camberAngleAtMaxDroop;

    // Set the wheels to camber outwards at maximum compression (the left and right wheels almost form a A shape).
    const physx::PxF32 camberAngleAtMaxCompression = -0.001f;
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mCamberAtMaxCompression = camberAngleAtMaxCompression;
    susps[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mCamberAtMaxCompression = -camberAngleAtMaxCompression;
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mCamberAtMaxCompression = camberAngleAtMaxCompression;
    susps[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mCamberAtMaxCompression = -camberAngleAtMaxCompression;

    // Now add the wheel, tire and suspension data.
    for (physx::PxU32 i = 0; i < 4; i++)
    {
        // We need to set up geometry data for the suspension, wheels, and tires.
        // We already know the wheel centers described as offsets from the actor center and the center of mass offset from actor center.
        // From here we can approximate application points for the tire and suspension forces.
        // Lets assume that the suspension travel directions are absolutely vertical.
        // Also assume that we apply the tire and suspension forces 30cm below the center of mass.
        physx::PxVec3 wheelCentreCMOffsets = wheelCentreOffsets[i] - chassisCMOffset;
        physx::PxVec3 suspForceAppCMOffsets = physx::PxVec3{ wheelCentreCMOffsets.x, wheelCentreCMOffsets.y, -0.3f };
        physx::PxVec3 tireForceAppCMOffsets = physx::PxVec3{ wheelCentreCMOffsets.x, wheelCentreCMOffsets.y, -0.3f };

        wheelsData.setWheelData(i, wheels[i]);
        wheelsData.setTireData(i, tires[i]);
        wheelsData.setSuspensionData(i, susps[i]);
        wheelsData.setSuspTravelDirection(i, physx::PxVec3{ 0.0f, 0.0f, -1.0f });
        wheelsData.setWheelCentreOffset(i, wheelCentreCMOffsets);
        wheelsData.setSuspForceAppPointOffset(i, suspForceAppCMOffsets);
        wheelsData.setTireForceAppPointOffset(i, tireForceAppCMOffsets);
    }

    // Set the car to perform 3 sub-steps when it moves with a forwards speed of less than 5.0
    // and with a single step when it moves at speed greater than or equal to 5.0.
    wheelsData.setSubStepCount(5.0f, 5, 5);


    // Now set up the differential, engine, gears, clutch, and ackermann steering.

    // Diff
    physx::PxVehicleDifferential4WData diff;
    diff.mType = physx::PxVehicleDifferential4WData::eDIFF_TYPE_LS_4WD;
    driveData.setDiffData(diff);

    // Engine
    physx::PxVehicleEngineData engine;
    engine.mPeakTorque = data.peakTorque;
    engine.mMaxOmega = data.maxOmega;
    engine.mMOI = 2.0f;
    driveData.setEngineData(engine);

    // Gears
    physx::PxVehicleGearsData gears;
    gears.mSwitchTime = data.gearSwitchTime;
    driveData.setGearsData(gears);

    // Clutch
    physx::PxVehicleClutchData clutch;
    clutch.mStrength = data.clutchStrength;
    driveData.setClutchData(clutch);

    // Ackermann steer accuracy
    physx::PxVehicleAckermannGeometryData ackermann;
    ackermann.mAccuracy = 1.0f;
    ackermann.mAxleSeparation = wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].x - wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].x;
    ackermann.mFrontWidth = wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].y - wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].y;
    ackermann.mRearWidth = wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].y - wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].y;
    driveData.setAckermannGeometryData(ackermann);
}

} // Namespace TE
