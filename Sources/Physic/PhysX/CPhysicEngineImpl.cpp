/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>

#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/PhysX/IActorImpl.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

physx::PxDefaultAllocator gAllocator;
physx::PxDefaultErrorCallback gErrorCallback;
CPhysicEngineImpl * gPhysicEngineImpl = nullptr;


/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CPhysicEngineImpl::CPhysicEngineImpl(CPhysicEngine * inst) :
    m_inst                  { inst },
    m_foundation            { nullptr },
    m_physics               { nullptr },
    m_cooking               { nullptr },
    m_dispatcher            { nullptr },
    m_pvd                   { nullptr },
    m_vehicleSdkInitialized { false }
{
    assert(m_inst != nullptr);
    gPhysicEngineImpl = this;
}


void CPhysicEngineImpl::init()
{
    m_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gAllocator, gErrorCallback);
    if (m_foundation == nullptr)
    {
        gApplication->log(CString("Can't create PhysX foundation."), ILogger::Error);
        return;
    }

#ifdef T_DEBUG
    m_pvd = physx::PxCreatePvd(*m_foundation);
    if (m_pvd != nullptr)
    {
        gApplication->log(CString("PhysX Visual Debugger available on %1:%2").arg("127.0.0.1").arg(5425));
        physx::PxPvdTransport * transport = physx::PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 100);
        m_pvd->connect(*transport, physx::PxPvdInstrumentationFlag::eALL);
    }
    else
    {
        gApplication->log("Can't initialize PhysX Visual Debugger.", ILogger::Error);
    }
#endif

    physx::PxTolerancesScale scale;
    scale.length *= 100;
    scale.speed *= 100;

    m_cooking = PxCreateCooking(PX_PHYSICS_VERSION, *m_foundation, physx::PxCookingParams(scale));
    if (m_cooking == nullptr)
    {
        gApplication->log("PxCreateCooking failed!", ILogger::Error);
    }

    m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_foundation, scale, true, m_pvd);

    if (m_physics != nullptr)
    {
        //if (!physx::PxInitExtensions(*m_physics, m_pvd))
        //    fatalError("PxInitExtensions failed!");

        m_dispatcher = physx::PxDefaultCpuDispatcherCreate(2);
    }
}


void CPhysicEngineImpl::close()
{
    if (m_vehicleSdkInitialized)
    {
        physx::PxCloseVehicleSDK();
        m_vehicleSdkInitialized = false;
    }

    if (m_dispatcher != nullptr)
    {
        m_dispatcher->release();
        m_dispatcher = nullptr;
    }

    if (m_cooking != nullptr)
    {
        m_cooking->release();
        m_cooking = nullptr;
    }

    if (m_physics != nullptr)
    {
        m_physics->release();
        m_physics = nullptr;
    }

    if (m_pvd != nullptr)
    {
        physx::PxPvdTransport * transport = m_pvd->getTransport();
        m_pvd->release();
        m_pvd = nullptr;
        transport->release();
    }

    if (m_foundation != nullptr)
    {
        m_foundation->release();
        m_foundation = nullptr;
    }
}


IActorImpl * CPhysicEngineImpl::getActor(physx::PxRigidActor * actor) const
{
    assert(actor != nullptr);

    auto it = m_actors.find(actor);
    if (it != m_actors.end())
    {
        return it->second;
    }

    return nullptr;
}


void CPhysicEngineImpl::addActor(IActorImpl * actor)
{
    assert(actor != nullptr);
    gApplication->log(CString("PhysX add actor 0x%1 -> 0x%2").arg(CString::fromPointer(actor->m_actor)).arg(CString::fromPointer(actor)), ILogger::Debug);

    if (actor->m_actor == nullptr)
    {
        return;
    }

    auto it = m_actors.find(actor->m_actor);
    if (it == m_actors.end())
    {
        m_actors[actor->m_actor] = actor;
    }
}


void CPhysicEngineImpl::removeActor(IActorImpl * actor)
{
    assert(actor != nullptr);
    gApplication->log(CString("PhysX remove actor 0x%1 -> 0x%2").arg(CString::fromPointer(actor->m_actor)).arg(CString::fromPointer(actor)), ILogger::Debug);

    if (actor->m_actor == nullptr)
    {
        return;
    }

    auto it = m_actors.find(actor->m_actor);
    if (it != m_actors.end())
    {
        m_actors.erase(it);
    }
}


void CPhysicEngineImpl::initVehicleSdk()
{
    assert(m_physics);

    if (m_vehicleSdkInitialized)
    {
        return;
    }

    // Initialise the SDK
    if (physx::PxInitVehicleSDK(*m_physics, nullptr))
    {
        m_vehicleSdkInitialized = true;

        // Set the basis vectors
        physx::PxVehicleSetBasisVectors(physx::PxVec3{ 0.0f, 0.0f, 1.0f }, physx::PxVec3{ 1.0f, 0.0f, 0.0f });

        // Set the vehicle update mode to be immediate velocity changes
        physx::PxVehicleSetUpdateMode(physx::PxVehicleUpdateMode::eVELOCITY_CHANGE);
    }
}

} // Namespace TE
