/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <cassert>

#include "Physic/PhysX/CCharacterControllerImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param inst Pointeur sur l'instance publique de la classe.
 ******************************/

CCharacterControllerImpl::CCharacterControllerImpl(CCharacterController * inst) :
    m_inst       { inst },
    m_controller { nullptr },
    m_radius     { 30.0f },
    m_height     { 180.0f }
{
    assert(m_inst != nullptr);
}


/**
 * Destructeur.
 ******************************/

CCharacterControllerImpl::~CCharacterControllerImpl()
{
    if (m_controller != nullptr)
    {
        m_controller->release();
    }
}


glm::vec3 CCharacterControllerImpl::getPosition()
{
    auto position = m_controller->getPosition();
    return { position.x, position.y, position.z };
}


/**
 * D�place le contr�leur.
 *
 * \param translation Vecteur de translation d�sir�.
 * \param elapsedTime Dur�e de la frame en secondes.
 * \return True si le personnage touche le sol.
 ******************************/

bool CCharacterControllerImpl::move(const glm::vec3& translation, float elapsedTime)
{
    physx::PxControllerFilters filters{};
    auto collisionFlags = m_controller->move({ translation.x, translation.y, translation.z }, 0.1f, elapsedTime, filters);
    return (collisionFlags & physx::PxControllerCollisionFlag::eCOLLISION_DOWN);
}


void CCharacterControllerImpl::setPosition(const glm::vec3& position)
{
    m_controller->setPosition({ position.x, position.y, position.z });
}


void CCharacterControllerImpl::resize(float height)
{
    if (height > 2 * m_radius && height != m_height)
    {
        m_height = height;
        m_controller->resize(m_height - 2 * m_radius);
    }
}


void CCharacterControllerImpl::setRadius(float radius)
{
    if (radius > 0.0f && radius != m_radius)
    {
        m_radius = radius;
        m_controller->setRadius(m_radius);
    }
}

} // Namespace TE
