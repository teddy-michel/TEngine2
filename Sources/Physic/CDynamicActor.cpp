/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/type_ptr.hpp>

#include "Physic/CDynamicActor.hpp"
#include "Physic/PhysX/CDynamicActorImpl.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Physic/IShape.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CDynamicActor::CDynamicActor(IEntity * entity, const char * name) :
    IActor{ entity, name }
{
    m_impl = std::make_unique<CDynamicActorImpl>(this, entity, name);
}


/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param mass Masse de l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CDynamicActor::CDynamicActor(IEntity * entity, const glm::mat4& matrix, float mass, const char * name) :
    IActor{ entity, name }
{
    m_impl = std::make_unique<CDynamicActorImpl>(this, entity, matrix, mass, name);
}


/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param shape Volume � utiliser pour l'acteur.
 * \param mass Masse de l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CDynamicActor::CDynamicActor(IEntity * entity, const glm::mat4& matrix, const std::shared_ptr<IShape>& shape, float mass, const char * name) :
    IActor{ entity, name }
{
    assert(shape != nullptr);

    m_impl = std::make_unique<CDynamicActorImpl>(this, entity, matrix, shape->m_impl.get(), mass, name);
    m_shapes.push_back(shape);
}


/**
 * Constructeur.
 *
 * \param impl Impl�mentation priv�e � utiliser.
 ******************************/

CDynamicActor::CDynamicActor(IEntity * entity, std::unique_ptr<CDynamicActorImpl>&& impl) :
    IActor{ entity, std::move(impl) }
{

}


/**
 * Retourne la masse de l'objet.
 *
 * \return Masse de l'objet.
 ******************************/

float CDynamicActor::getMass() const
{
    return static_cast<CDynamicActorImpl *>(m_impl.get())->getMass();
}


/**
 * Modifie la masse de l'objet.
 *
 * \param mass Masse de l'objet (sup�rieure � 0).
 ******************************/

void CDynamicActor::setMass(float mass)
{
    static_cast<CDynamicActorImpl *>(m_impl.get())->setMass(mass);
}


/**
 * Ajoute un volume � l'acteur.
 *
 * \param shape Volume � ajouter.
 ******************************/

void CDynamicActor::addShapePriv(const std::shared_ptr<IShape>& shape)
{
    assert(shape != nullptr);
    m_impl->addShape(shape->m_impl.get());
}


/**
 * Enl�ve un volume � l'acteur.
 *
 * \param shape Volume � enlever.
 ******************************/

void CDynamicActor::removeShapePriv(const std::shared_ptr<IShape>& shape)
{
    assert(shape != nullptr);
    m_impl->removeShape(shape->m_impl.get());
}

} // Namespace TE
