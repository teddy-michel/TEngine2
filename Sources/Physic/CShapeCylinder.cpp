/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Physic/CShapeCylinder.hpp"
#include "Physic/PhysX/CShapeCylinderImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param radius Rayon du cylindre.
 * \param length Longueur du cylindre.
 ******************************/

CShapeCylinder::CShapeCylinder(float radius, float length) :
    IShape   {},
    m_radius { radius },
    m_length { length }
{
    m_impl = std::make_unique<CShapeCylinderImpl>(this);

    if (m_radius < 0.001f)
    {
        m_radius = 0.001f;
    }

    if (m_length < 0.001f)
    {
        m_length = 0.001f;
    }

    static_cast<CShapeCylinderImpl *>(m_impl.get())->updateShape(m_radius, m_length);
}


/**
 * Modifie le rayon du cylindre.
 *
 * \param radius Rayon du cylindre.
 ******************************/

void CShapeCylinder::setRadius(float radius)
{
    if (radius >= 0.001f && radius != m_radius)
    {
        m_radius = radius;
        static_cast<CShapeCylinderImpl *>(m_impl.get())->updateShape(m_radius, m_length);
    }
}


/**
 * Modifie la longueur du cylindre.
 *
 * \param length Longueur du cylindre.
 ******************************/

void CShapeCylinder::setLength(float length)
{
    if (length >= 0.001f && length != m_length)
    {
        m_length = length;
        static_cast<CShapeCylinderImpl *>(m_impl.get())->updateShape(m_radius, m_length);
    }
}

} // Namespace TE
