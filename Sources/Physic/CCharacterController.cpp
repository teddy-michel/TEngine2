/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>

#include "Physic/CCharacterController.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Physic/PhysX/CCharacterControllerImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 ******************************/

CCharacterController::CCharacterController() :
    //m_impl { std::make_unique<CCharacterControllerImpl>(this) },
    m_impl   { new CCharacterControllerImpl { this } },
    m_actor  { nullptr }
{
    assert(m_impl != nullptr);
}


glm::vec3 CCharacterController::getPosition()
{
    return m_impl->getPosition();
}


/**
 * D�place le contr�leur.
 *
 * \param translation Vecteur de translation d�sir�.
 * \param elapsedTime Dur�e de la frame en secondes.
 * \return True si le personnage touche le sol.
 ******************************/

bool CCharacterController::move(const glm::vec3& translation, float elapsedTime)
{
    return m_impl->move(translation, elapsedTime);
}


void CCharacterController::setPosition(const glm::vec3& position)
{
    m_impl->setPosition(position);
}


void CCharacterController::resize(float height)
{
    m_impl->resize(height);
}


void CCharacterController::setRadius(float radius)
{
    m_impl->setRadius(radius);
}


std::shared_ptr<IActor> CCharacterController::getActor() const
{
    return m_actor;
}

} // Namespace TE
