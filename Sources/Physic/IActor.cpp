/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Physic/IActor.hpp"
#include "Physic/PhysX/IActorImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

IActor::IActor(IEntity * entity, const char * name) :
    m_impl   { nullptr },
    m_shapes {},
    m_entity { entity }
{
    assert(m_entity != nullptr);
}


/**
 * Constructeur.
 *
 * \param impl Impl�mentation priv�e � utiliser.
 ******************************/

IActor::IActor(IEntity * entity, std::unique_ptr<IActorImpl>&& impl) :
    m_impl   { std::move(impl) },
    m_shapes {},
    m_entity { entity }
{
    assert(m_impl != nullptr);
}


/**
 * Destructeur.
 ******************************/

IActor::~IActor() = default;


glm::mat4 IActor::getTransform() const
{
    return m_impl->getTransform();
}


void IActor::setTransform(const glm::mat4& matrix)
{
    m_impl->setTransform(matrix);
}


/**
 * Modifie la vitesse lin�aire de l'acteur.
 *
 * \param velocity Vitesse lin�aire.
 ******************************/

void IActor::setLinearVelocity(const glm::vec3& velocity)
{
    m_impl->setLinearVelocity(velocity);
}


/**
 * Ajoute un volume � l'acteur.
 *
 * \param shape Volume � ajouter (doit �tre non nul).
 ******************************/

void IActor::addShape(const std::shared_ptr<IShape>& shape)
{
    assert(shape != nullptr);

    auto it = std::find(m_shapes.begin(), m_shapes.end(), shape);
    if (it == m_shapes.end())
    {
        addShapePriv(shape);
        m_shapes.push_back(shape);
    }
}


void IActor::removeShape(const std::shared_ptr<IShape>& shape)
{
    assert(shape != nullptr);

    auto it = std::find(m_shapes.begin(), m_shapes.end(), shape);
    if (it != m_shapes.end())
    {
        removeShapePriv(shape);
        m_shapes.erase(it);
    }
}


std::vector<std::shared_ptr<IShape>> IActor::getShapes() const
{
    return m_shapes;
}


IEntity * IActor::getEntity() const
{
    return m_entity;
}

} // Namespace TE
