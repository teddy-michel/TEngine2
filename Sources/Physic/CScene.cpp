/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Physic/CScene.hpp"
#include "Physic/IActor.hpp"
#include "Physic/PhysX/CSceneImpl.hpp"
#include "Physic/CCharacterController.hpp"
#include "Physic/CVehicleController.hpp"
#include "Physic/CDynamicActor.hpp"
#include "Physic/IShape.hpp"
#include "Core/CApplication.hpp"
#include "Entities/IEntity.hpp"
#include "Entities/CTrigger.hpp"



namespace TE
{

class IShapeImpl;


/**
 * Constructeur.
 *
 * \param name Nom utilis� pour le debug.
 ******************************/

CScene::CScene(const char * name) :
    //m_impl   { std::make_unique<CSceneImpl>(this) },
    m_impl   { new CSceneImpl { this } },
    m_actors {}
{

}


/**
 * Destructeur.
 ******************************/

CScene::~CScene()
{
    delete m_impl;
}


/**
 * Ajoute un acteur � la sc�ne.
 *
 * \param actor Acteur � ajouter (doit �tre non nul).
 ******************************/

void CScene::addActor(const std::shared_ptr<IActor>& actor)
{
    assert(actor != nullptr);
    if (std::find(m_actors.begin(), m_actors.end(), actor) == m_actors.end())
    {
        // Add to scene
        gApplication->log(CString("Add actor to physic scene."));
        m_impl->addActor(actor->m_impl.get());
        m_actors.push_back(actor);
    }
}


/**
 * Enl�ve un acteur � la sc�ne.
 *
 * \param actor Acteur � enlever (doit �tre non nul).
 ******************************/

void CScene::removeActor(const std::shared_ptr<IActor>& actor)
{
    assert(actor != nullptr);
    auto it = std::find(m_actors.begin(), m_actors.end(), actor);
    if (it != m_actors.end())
    {
        // Remove from scene
        gApplication->log(CString("Remove actor 0x%1 from physic scene.").arg(CString::fromPointer(actor.get())));
        m_impl->removeActor(actor->m_impl.get());
        m_actors.erase(it);
    }
}


std::vector<std::shared_ptr<IActor>> CScene::getActors() const
{
    return m_actors;
}


CCharacterController * CScene::createCharacterController(CPlayer * player, float height, float radius)
{
    assert(player != nullptr);

    CCharacterController * controller = new CCharacterController{};
    std::shared_ptr<CDynamicActor> actor = m_impl->initCharacterController(controller->m_impl, player, height, radius);
    controller->m_actor = actor;
    m_actors.push_back(actor);
    // QUESTION: quand l'acteur est-il ajout� � la sc�ne ???
    return controller;
}


void CScene::removeCharacterController(CCharacterController * controller)
{
    assert(controller != nullptr);
    m_impl->removeCharacterController(controller);
    delete controller;
}


CVehicleController * CScene::createVehicleController(IEntity * entity, const CVehicleData& data)
{
    CVehicleController * controller = new CVehicleController{};

    IShapeImpl * chassis = data.shape->m_impl.get();
    IShapeImpl * wheels[4] = { nullptr };
    for (int i = 0; i < 4; ++i)
    {
        wheels[i] = data.wheels[i].shape->m_impl.get();
    }

    std::shared_ptr<CDynamicActor> actor = m_impl->initVehicleController(controller->m_impl, entity, data, chassis, wheels);
    controller->m_actor = actor;
    m_actors.push_back(actor);
    // QUESTION: quand l'acteur est-il ajout� � la sc�ne ???
    return controller;
}


void CScene::removeVehicleController(CVehicleController * controller)
{
    assert(controller != nullptr);
    m_impl->removeVehicleController(controller->m_impl);
    delete controller;
}


/**
 * M�thode appell�e � chaque frame pour mettre � jour la simulation.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CScene::update(float frameTime)
{
    m_impl->update(frameTime);
}


glm::vec3 CScene::getGravity() const
{
    return m_impl->getGravity();
}


void CScene::setGravity(const glm::vec3& gravity)
{
    m_impl->setGravity(gravity);
}


bool CScene::raycast(const glm::vec3& position, const glm::vec3& direction, CRaycastResult& result)
{
    return m_impl->raycast(position, direction, result);
}


void CScene::updateEntityTransform(IEntity * entity, const glm::mat4& t)
{
    assert(entity != nullptr);
    entity->updateWorldMatrixByPhysic(t);
}


void CScene::triggerEntity(CTrigger * trigger, IEntity * entity, bool enter)
{
    assert(trigger != nullptr);
    assert(entity != nullptr);
    trigger->onTrigger(entity, enter);
}

} // Namespace TE
