/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Physic/CShapeSphere.hpp"
#include "Physic/PhysX/CShapeSphereImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param radius Rayon de la sph�re.
 ******************************/

CShapeSphere::CShapeSphere(float radius) :
    IShape   {},
    m_radius { radius }
{
    m_impl = std::make_unique<CShapeSphereImpl>(this);

    if (m_radius < 0.001f)
    {
        m_radius = 0.001f;
    }

    static_cast<CShapeSphereImpl *>(m_impl.get())->setRadius(m_radius);
}


/**
 * Modifie le rayon de la sph�re.
 *
 * \param radius Rayon de la sph�re.
 ******************************/

void CShapeSphere::setRadius(float radius)
{
    if (radius >= 0.001f && radius != m_radius)
    {
        m_radius = radius;
        static_cast<CShapeSphereImpl *>(m_impl.get())->setRadius(m_radius);
    }
}

} // Namespace TE
