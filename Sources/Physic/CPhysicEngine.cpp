/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Physic/CPhysicEngine.hpp"
#include "Physic/PhysX/CPhysicEngineImpl.hpp"
#include "Physic/CScene.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

CPhysicEngine * gPhysicEngine = nullptr;


/**
 * Constructeur.
 ******************************/

CPhysicEngine::CPhysicEngine() :
    m_impl         { new CPhysicEngineImpl{ this } },
    m_currentScene { nullptr }
{
    assert(gPhysicEngine == nullptr);
    gPhysicEngine = this;
}


/**
 * Destructeur.
 ******************************/

CPhysicEngine::~CPhysicEngine()
{
    delete m_impl;
    gPhysicEngine = nullptr;
}


/**
 * Initialisation du moteur physique.
 ******************************/

void CPhysicEngine::init()
{
    gApplication->log("Initialisation du moteur physique.");
    m_impl->init();
}


/**
 * Ferme le moteur physique.
 ******************************/

void CPhysicEngine::close()
{
    gApplication->log("Fermeture du moteur physique.");

    for (auto& scene : m_scenes)
    {
        delete scene;
    }

    m_scenes.clear();
    m_currentScene = nullptr;

    m_impl->close();
}


/**
 * M�thode appell�e � chaque frame pour mettre � jour la simulation.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CPhysicEngine::update(float frameTime)
{
    if (m_currentScene != nullptr)
    {
        m_currentScene->update(frameTime);
    }
}


/**
 * Cr�e une nouvelle sc�ne.
 *
 * \param name Nom de la sc�ne.
 * \return Sc�ne cr��e.
 ******************************/

CScene * CPhysicEngine::createScene(const char * name)
{
    CScene * scene = new CScene{ name };
    m_scenes.push_back(scene);
    return scene;
}


void CPhysicEngine::setCurrentScene(CScene * scene)
{
    assert(scene != nullptr);
    m_currentScene = scene;
}


CScene * CPhysicEngine::getCurrentScene() const
{
    return m_currentScene;
}


void CPhysicEngine::removeScene(CScene * scene)
{
    assert(scene != nullptr);

    if (scene == m_currentScene)
    {
        m_currentScene = nullptr;
    }

    auto it = std::find(m_scenes.begin(), m_scenes.end(), scene);
    if (it != m_scenes.end())
    {
        m_scenes.erase(it);
        delete scene;
    }
}

} // Namespace TE
