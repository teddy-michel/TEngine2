/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/type_ptr.hpp>

#include "Physic/CStaticActor.hpp"
#include "Physic/PhysX/CStaticActorImpl.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Physic/IShape.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param name Nom utilis� pour le debug.
 ******************************/

CStaticActor::CStaticActor(IEntity * entity, const char * name) :
    IActor{ entity, name }
{
    m_impl = std::make_unique<CStaticActorImpl>(this, entity, name);
}


/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param name Nom utilis� pour le debug.
 ******************************/

CStaticActor::CStaticActor(IEntity * entity, const glm::mat4& matrix, const char * name) :
    IActor{ entity, name }
{
    m_impl = std::make_unique<CStaticActorImpl>(this, entity, matrix, name);
}


/**
 * Constructeur.
 *
 * \param entity Pointeur sur l'entit� associ�e � l'acteur.
 * \param matrix Matrice de transformation initiale.
 * \param name Nom utilis� pour le debug.
 ******************************/

CStaticActor::CStaticActor(IEntity * entity, const glm::mat4& matrix, const std::shared_ptr<IShape>& shape, const char * name) :
    IActor{ entity, name }
{
    assert(shape != nullptr);
    
    m_impl = std::make_unique<CStaticActorImpl>(this, entity, matrix, shape->m_impl.get(), name);
    m_shapes.push_back(shape);
}


/**
 * Ajoute un volume � l'acteur.
 *
 * \param shape Volume � ajouter.
 ******************************/

void CStaticActor::addShapePriv(const std::shared_ptr<IShape>& shape)
{
    assert(shape != nullptr);
    m_impl->addShape(shape->m_impl.get());
}


/**
 * Enl�ve un volume � l'acteur.
 *
 * \param shape Volume � enlever.
 ******************************/

void CStaticActor::removeShapePriv(const std::shared_ptr<IShape>& shape)
{
    assert(shape != nullptr);
    m_impl->removeShape(shape->m_impl.get());
}

} // Namespace TE
