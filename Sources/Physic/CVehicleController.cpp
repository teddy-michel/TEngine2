/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhysX/PxPhysicsAPI.h>
#include <cassert>

#include "Physic/CVehicleController.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Physic/IActor.hpp"
#include "Physic/PhysX/CVehicleControllerImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 ******************************/

CVehicleController::CVehicleController() :
    //m_impl                 { std::make_unique<CVehicleControllerImpl>(this) },
    m_impl                 { new CVehicleControllerImpl { this } },
    m_actor                { nullptr },
    m_accelKeyPressed      { false },
    m_brakeKeyPressed      { false },
    m_handBrakeKeyPressed  { false },
    m_steerLeftKeyPressed  { false },
    m_steerRightKeyPressed { false },
    m_gearUpKeyPressed     { false },
    m_gearDownKeyPressed   { false },
    m_toggleAutoGears      { false }
{
    assert(m_impl != nullptr);
}


glm::mat4 CVehicleController::getTransform() const
{
    return m_actor->getTransform();
}


void CVehicleController::setTransform(const glm::mat4& matrix)
{
    return m_actor->setTransform(matrix);
}


/**
 * Calcule la vitesse du v�hicule.
 *
 * \return Vitesse du v�hicule en m/s.
 ******************************/

float CVehicleController::getSpeed() const
{
    return m_impl->getSpeed();
}


/**
 * Calcule la vitesse de rotation du moteur.
 *
 * \return Vitesse de rotation du moteur en tours/min.
 ******************************/

float CVehicleController::getEngineSpeed() const
{
    return m_impl->getEngineSpeed();
}


/**
 * Retourne le rapport de vitesse actuel.
 *
 * \return Rapport de vitesse. -1 pour la marche arri�re, 0 pour le neutre, et entre 1 et 5 pour les vitesses.
 ******************************/

int CVehicleController::getCurrentGear() const
{
    return m_impl->getCurrentGear();
}


/**
 * Met-�-jour le v�hicule en fonction des commandes.
 * M�thode appell�e � chaque frame par l'entit�, apr�s avoir modifi� l'�tat des touches.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CVehicleController::updateInputs(float frameTime)
{
    assert(m_impl != nullptr);
    m_impl->updateInputs(frameTime);
}


std::shared_ptr<IActor> CVehicleController::getActor() const
{
    return m_actor;
}

} // Namespace TE
