/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Physic/CShapeBox.hpp"
#include "Physic/PhysX/CShapeBoxImpl.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param size Dimensions de la boite.
 ******************************/

CShapeBox::CShapeBox(const glm::vec3& size) :
    IShape {},
    m_size { size }
{
    m_impl = std::make_unique<CShapeBoxImpl>(this);

    if (m_size.x <= 0)
    {
        m_size.x = 0.001f;
    }

    if (m_size.y <= 0)
    {
        m_size.y = 0.001f;
    }

    if (m_size.z <= 0)
    {
        m_size.z = 0.001f;
    }

    static_cast<CShapeBoxImpl *>(m_impl.get())->setSize(m_size);
}


/**
 * Retourne les dimensions de la boite.
 *
 * \return Dimensions de la boite.
 ******************************/

glm::vec3 CShapeBox::getSize() const
{
    return m_size;
}


/**
 * Modifie les dimensions de la boite.
 *
 * \param size Dimensions de la boite.
 ******************************/

void CShapeBox::setSize(const glm::vec3& size)
{
    if (size.x > 0 && size.y > 0 && size.z > 0 && m_size != size)
    {
        m_size = size;
        static_cast<CShapeBoxImpl *>(m_impl.get())->setSize(m_size);
    }
}

} // Namespace TE
