/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <ctime>
#include <cassert>
#include <cstdlib>

#include "Core/utils.hpp"


namespace TE
{

uint64_t TRand()
{
    static bool init = false;

    if (!init)
    {
        srand(time(nullptr));
        init = true;
    }

    return rand();
}


/**
 * G�n�re un nombre al�atoire compris entre deux valeurs.
 *
 * \param min Valeur minimale.
 * \param max Valeur maximale.
 * \return Nombre al�atoire.
 ******************************/

int32_t RandInt(int32_t min, int32_t max)
{
    assert(min < max);
    return static_cast<int32_t>(min + (static_cast<float>(TRand() * (max - min + 1) / RAND_MAX)));
}

} // Namespace TE
