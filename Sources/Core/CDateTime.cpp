/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <ctime>
#include <sstream>
#include <cassert>
#include <chrono>

#include "Core/CDateTime.hpp"
//#include "Core/Utils.hpp"


namespace TE
{

/**
 * Constructeur par défaut.
 *
 * \param year         Année.
 * \param month        Mois.
 * \param day          Jour.
 * \param hour         Heure.
 * \param minute       Minute.
 * \param second       Seconde.
 * \param microseconds Micro-secondes.
 ******************************/

CDateTime::CDateTime(int year, unsigned int month, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second, unsigned int microseconds) :
m_microseconds(microseconds),
m_second      (second),
m_minute      (minute),
m_hour        (hour),
m_day         (day),
m_month       (month),
m_year        (year)
{ }


/**
 * Accesseur pour second.
 *
 * \return Secondes.
 ******************************/

unsigned int CDateTime::getSecond() const
{
    return m_second;
}


/**
 * Accesseur pour minute.
 *
 * \return Minutes.
 ******************************/

unsigned int CDateTime::getMinute() const
{
    return m_minute;
}


/**
 * Accesseur pour hour.
 *
 * \return Heures.
 ******************************/

unsigned int CDateTime::getHour() const
{
    return m_hour;
}


/**
 * Accesseurs pour day.
 *
 * \return Jours.
 ******************************/

unsigned int CDateTime::getDay() const
{
    return m_day;
}


/**
 * Accesseur pour month.
 *
 * \return Mois.
 ******************************/

unsigned int CDateTime::getMonth() const
{
    return m_month;
}


/**
 * Accesseur pour year.
 *
 * \return Années.
 ******************************/

int CDateTime::getYear() const
{
    return m_year;
}


/**
 * Indique si la date est valide.
 *
 * \return Booléen.
 ******************************/

bool CDateTime::isValid() const
{
    return isValid(m_year, m_month, m_day);
}


/**
 * Indique si la date correspond à une année bisextile.
 *
 * \return Booléen.
 ******************************/

bool CDateTime::isLeapYear() const
{
    return isLeapYear(m_year);
}


/**
 * Définit l'heure.
 *
 * \param hour Heure.
 * \param minute Minute.
 * \param second Seconde.
 * \return Booléen indiquant si l'heure est valide.
 ******************************/

bool CDateTime::setTime(unsigned int hour, unsigned int minute, unsigned int second)
{
    if (hour < 24 && minute < 60 && second < 60)
    {
        m_hour = hour;
        m_minute = minute;
        m_second = second;

        return true;
    }

    return false;
}


/**
 * Définit la date (qui doit être valide).
 *
 * \param year  Année.
 * \param month Mois.
 * \param day   Jour.
 * \return Booléen indiquant si la date est valide.
 ******************************/

bool CDateTime::setDate(int year, unsigned int month, unsigned int day)
{
    if (isValid(year, month, day))
    {
        m_year = year;
        m_month = month;
        m_day = day;

        return true;
    }

    return false;
}


/**
 * Définit la date à partir du jour julien.
 *
 * \param day Jour julien.
 ******************************/

void CDateTime::fromJulianDay(float day)
{
    day += 0.5f;

    int z = static_cast<int>(day);
    unsigned int f = static_cast<unsigned int>((day - z) * 86400.0f);

    int a;

    if (z < 2299161)
    {
        a = z;
    }
    else
    {
        int aa = static_cast<int>((static_cast<float>(z) - 1867216.25f) / 36524.25f);
        a = z + 1 + aa - aa / 4;
    }

    int c = static_cast<int>((a + 1401.9f) / 365.25f);
    int d = static_cast<int>(365.25f * static_cast<float>(c));
    int e = static_cast<int>((a + 1524.0f - d) / 30.6001f);

    // Jour
    m_day = a + 1524 - d - static_cast<int>(30.6001f * static_cast<float>(e));
    m_month = (e < 14 ? e - 1 : e - 13);
    m_year = (m_month > 2 ? c - 4716 : c - 4715);

    // Heure
    m_second = (f % 3600) % 60;
    m_minute = (f % 3600) / 60;
    m_hour = f / 3600;
}


/**
 * Vérifie si la date est nulle (égale à zéro).
 *
 * \return Booléen indiquant si la date est nulle.
 ******************************/

bool CDateTime::isNull() const
{
    return (m_second == 0 && m_minute == 0 && m_hour == 0 && m_day == 0 && m_month == 0 && m_year == 0);
}


/**
 * Convertit la date en une chaine de caractères.
 *
 * \param format Format de la date (similaire à la fonction date de PHP).
 * \return Chaine de caractères.
 *
 * Les caractères suivants seront transformés :
 * \li %a : Ante meridiem et Post meridiem en minuscules.
 * \li %A : Ante meridiem et Post meridiem en majuscules.
 * \li %d : Jour du mois, sur deux chiffres (avec un zéro initial).
 * \li %D : Jour de la semaine, en trois lettres (et en français).
 * \li %F : Mois, textuel, version longue, en français.
 * \li %g : Heure, au format 12h, sans les zéros initiaux.
 * \li %G : Heure, au format 24h, sans les zéros initiaux.
 * \li %h : Heure, au format 12h, avec les zéros initiaux.
 * \li %H : Heure, au format 24h, avec les zéros initiaux.
 * \li %i : Minutes avec les zéros initiaux.
 * \li %j : Jour du mois sans les zéros initiaux.
 * \li %l : Jour de la semaine, textuel, version longue, en français.
 * \li %L : Est-ce que l'année est bissextile, 1 si bissextile, 0 sinon.
 * \li %m : Mois au format numérique, avec zéros initiaux.
 * \li %M : Mois, en trois lettres, en français.
 * \li %n : Mois sans les zéros initiaux.
 * \li %N : Représentation numérique ISO-8601 du jour de la semaine.
 * \li %s : Secondes, avec zéros initiaux.
 * \li %t : Nombre de jours dans le mois, sans zéros initiaux.
 * \li %U : Secondes depuis l'époque Unix (1er Janvier 1970, 0h00 00s GMT).
 * \li %W : Numéro de semaine dans l'année ISO-8601.
 * \li %y : Année sur 2 chiffres.
 * \li %Y : Année sur 4 chiffres.
 * \li %z : Jour de l'année.
 * \li %% : Caractère %.
 ******************************/

CString CDateTime::toString(const CString& format) const
{
    if (isNull())
    {
        return CString();
    }

    CString str;
    bool special = false;
    const unsigned int len = format.getSize();

    for (unsigned int i = 0 ; i < len ; ++i)
    {
        if (format[i] == CChar('%'))
        {
            special = true;
        }
        else if (special)
        {
            switch (format[i].toLatin1())
            {
                default:
                    str += format[i];
                    break;

                case 'a':
                    str += (m_hour < 12 ? "am" : "pm");
                    break;

                case 'A':
                    str += (m_hour < 12 ? "AM" : "PM");
                    break;

                case 'd':

                    if (m_day < 10)
                        str += '0';

                    str += CString::fromNumber(m_day);
                    break;

                case 'D':
                    str += shortDayName(dayOfWeek(m_year, m_month, m_day));
                    break;

                case 'F':
                    str += longMonthName(m_month);
                    break;

                case 'g':

                    if (m_hour == 0)
                    {
                        str += "12";
                    }
                    else if (m_hour < 13)
                    {
                        str += CString::fromNumber(m_hour);
                    }
                    else
                    {
                        str += CString::fromNumber(m_hour - 12);
                    }

                    break;

                case 'G':
                    str += CString::fromNumber(m_hour);
                    break;

                case 'h':

                    if (m_hour == 0)
                    {
                        str += "12";
                    }
                    else if (m_hour < 13)
                    {
                        if (m_hour < 10)
                        {
                            str += '0';
                        }

                        str += CString::fromNumber(m_hour);
                    }
                    else
                    {
                        if (m_hour < 22)
                        {
                            str += '0';
                        }

                        str += CString::fromNumber(m_hour - 12);
                    }

                    break;

                case 'H':

                    if (m_hour < 10)
                    {
                        str += '0';
                    }

                    str += CString::fromNumber(m_hour);

                    break;

                case 'i':

                    if (m_minute < 10)
                    {
                        str += '0';
                    }

                    str += CString::fromNumber(m_minute);

                    break;

                case 'j':
                    str += CString::fromNumber(m_day);
                    break;

                case 'l':
                    str += longDayName(dayOfWeek(m_year, m_month, m_day));
                    break;

                case 'L':
                    str += (isLeapYear(m_year) ? '1' : '0');
                    break;

                case 'm':

                    if (m_month < 10)
                    {
                        str += '0';
                    }

                    str += CString::fromNumber(m_month);

                    break;

                case 'M':
                    str += shortMonthName(m_month);
                    break;

                case 'n':
                    str += CString::fromNumber(m_month);
                    break;

                case 'N':
                    str += dayOfWeek(m_year, m_month, m_day);
                    break;

                case 's':

                    if (m_second < 10)
                    {
                        str += '0';
                    }

                    str += CString::fromNumber(m_second);

                    break;

                case 't':
                    str += CString::fromNumber(daysInMonth(m_month, m_year));
                    break;

                case 'U':
                    str += CString::fromNumber(timestamp(m_year, m_month, m_day, m_hour, m_minute, m_second));
                    break;

                case 'W':
                    str += CString::fromNumber(weekOfYear(m_year, m_month, m_day));
                    break;

                case 'y':

                    if (m_year < 0)
                    {
                        str += CString::fromNumber(m_year);
                    }
                    else
                    {
                        unsigned int tmp = m_year % 100;

                        if (tmp < 10)
                        {
                            str += "0";
                        }

                        str += CString::fromNumber(tmp);
                    }

                    break;

                case 'Y':

                    if (m_year < 0)
                    {
                        str += CString::fromNumber(m_year);
                    }
                    else
                    {
                        unsigned int tmp = m_year % 10000;

                        if (tmp < 10)
                            str += "000";
                        if (tmp < 100)
                            str += "00";
                        if (tmp < 1000)
                            str += "000";

                        str += CString::fromNumber(tmp);
                    }

                    break;

                case 'z':
                    str += dayOfYear(m_year, m_month, m_day);
                    break;
            }

            special = false;
        }
        else
        {
            str += format[i];
        }
    }

    return str;
}


/**
 * Ajoute un certain nombre de secondes à la date.
 *
 * \param add Nombre de secondes à ajouter ou à enlever.
 ******************************/

void CDateTime::addSeconds(int add)
{
    if (add == 0)
        return;

    // On ajoute les jours entiers séparemment pour alléger les calculs
    int days = add / 86400;
    if (days)
        addDays(days);

    add %= 86400;

    if (add < 0)
    {
        for (int i = 0; i > add; --i)
        {
            // Minute précédente
            if (m_second == 0)
            {
                addMinutes(-1);
                m_second = 59;
            }
            else
            {
                --m_second;
            }
        }
    }
    // Date supérieure
    else if (add > 0)
    {
        for (int i = 0; i < add; ++i)
        {
            // Minute suivante
            if (m_second == 59)
            {
                addMinutes(1);
                m_second = 0;
            }
            else
            {
                ++m_second;
            }
        }
    }
}


/**
 * Ajoute un certain nombre de minutes à la date.
 *
 * \param add Nombre de minutes à ajouter ou à enlever.
 ******************************/

void CDateTime::addMinutes(int add)
{
    // Date inférieure
    if (add < 0)
    {
        for (int i = 0; i > add; --i)
        {
            // Heure précédente
            if (m_minute == 0)
            {
                addHours(-1);
                m_minute = 59;
            }
            else
            {
                --m_minute;
            }
        }
    }
    // Date supérieure
    else if (add > 0)
    {
        for (int i = 0; i < add; ++i)
        {
            // Heure suivante
            if (m_minute == 59)
            {
                addHours(1);
                m_minute = 0;
            }
            else
            {
                ++m_minute;
            }
        }
    }
}


/**
 * Ajoute un certain nombre d'heures à la date.
 *
 * \param add Nombre d'heures à ajouter ou à enlever.
 ******************************/

void CDateTime::addHours(int add)
{
    // Date inférieure
    if (add < 0)
    {
        for (int i = 0; i > add; --i)
        {
            // Jour précédent
            if (m_hour == 0)
            {
                addDays(-1);
                m_hour = 23;
            }
            else
            {
                --m_hour;
            }
        }
    }
    // Date supérieure
    else if (add > 0)
    {
        for (int i = 0; i < add; ++i)
        {
            // Jour suivant
            if (m_hour == 23)
            {
                addDays(1);
                m_hour = 0;
            }
            else
            {
                ++m_hour;
            }
        }
    }
}


/**
 * Ajoute un certain nombre de jours à la date.
 *
 * \param add Nombre de jours à ajouter ou à enlever.
 ******************************/

void CDateTime::addDays(int add)
{
    // Date inférieure
    if (add < 0)
    {
        for (int i = 0; i > add; --i)
        {
            if (m_day == 1)
            {
                if (m_month == 1)
                {
                    m_day = 31;
                    m_month = 12;
                    --m_year;
                }
                else if (m_month == 3)
                {
                    m_month = 2;
                    m_day = (isLeapYear(m_year) ? 29 : 28);
                }
                else
                {
                    --m_month;
                    m_day = (m_month == 4 || m_month == 6 || m_month == 9 || m_month == 11 ? 30 : 31);
                }
            }
            else
            {
                --m_day;
            }
        }
    }
    // Date supérieure
    else if (add > 0)
    {
        for (int i = 0; i < add; ++i)
        {
            if (m_month == 12)
            {
                if (m_day == 31)
                {
                    m_day = 1;
                    m_month = 1;
                    ++m_year;
                }
                else
                {
                    ++m_day;
                }
            }
            else if (m_day == daysInMonth(m_month, m_year))
            {
                m_day = 1;
                ++m_month;
            }
            else
            {
                ++m_day;
            }
        }
    }
}


/**
 * Ajoute un certain nombre de mois à la date.
 *
 * \return Nombre de mois à ajouter ou à enlever.
 ******************************/

void CDateTime::addMonths(int add)
{
    if (add == 0)
        return;

    // Date inférieure
    if (add < 0)
    {
        for (int i = 0; i > add; --i)
        {
            if (m_month == 1)
            {
                m_month = 12;
                --m_year;
            }
            else
            {
                --m_month;
            }
        }
    }
    // Date supérieure
    else if (add > 0)
    {
        for (int i = 0; i < add; ++i)
        {
            if (m_month == 12)
            {
                m_month = 1;
                ++m_year;
            }
            else
            {
                ++m_month;
            }
        }
    }

    unsigned int days = daysInMonth(m_month, m_year);

    // Le jour est trop grand
    if (m_day > days)
        m_day = days;
}


/**
 * Ajoute un certain nombre d'années à la date.
 *
 * \return Nombre d'années à ajouter ou à enlever.
 ******************************/

void CDateTime::addYears(int add)
{
    if (add == 0)
        return;

    m_year += add;

    // Correction du 29 février pour les années non bisextiles
    if (m_day == 29 && m_month == 2 && !isLeapYear(m_year))
    {
        m_day = 28;
    }
}


/**
 * Donne le jour julien correspondant à la date.
 *
 * \return Jour julien (ou -1 si la date est invalide).
 *
 * \sa CDateTime::FromJulianDay
 ******************************/

float CDateTime::toJulianDay() const
{
    if (!isValid())
    {
        return -1.0f;
    }

    int y = m_year;
    int m = m_month;

    if (m <= 2)
    {
        --y;
        m += 12;
    }

    int c = y / 100;
    int b = 2 - c + (c >> 2);

    return (b + static_cast<int>(365.25f * (y + 4716.0f)) + static_cast<int>(30.6001f * (m + 1)) + m_day - 1524.5f);
}


/**
 * Donne la date et l'heure actuelle.
 *
 * \return DateTime actuel.
 ******************************/

CDateTime CDateTime::getCurrentDateTime()
{
	auto now = std::chrono::system_clock::now();
	auto now_ms = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
	std::size_t fractional_seconds = now_ms.count() % 1000000;

    time_t currentTime = time(nullptr);
    char sTime[20];

    int ret = strftime(sTime, sizeof(sTime), "%Y %m %d %H %M %S", localtime(&currentTime));
    assert(ret == 19);

    unsigned int second;
    unsigned int minute;
    unsigned int hour;
    unsigned int day;
    unsigned int month;
    int year;

#if 0

    // Version C++
    std::istringstream iss(sTime);
    iss >> year >> month >> day >> hour >> minute >> second;

#else

    // Version optimisée
    // Chaque élément occupe une taille fixe
    year   = (sTime[ 0] - '0') * 1000 + (sTime[1] - '0') * 100 + (sTime[2] - '0') * 10 + (sTime[3] - '0');
    month  = (sTime[ 5] - '0') * 10 + (sTime[ 6] - '0');
    day    = (sTime[ 8] - '0') * 10 + (sTime[ 9] - '0');
    hour   = (sTime[11] - '0') * 10 + (sTime[12] - '0');
    minute = (sTime[14] - '0') * 10 + (sTime[15] - '0');
    second = (sTime[17] - '0') * 10 + (sTime[18] - '0');

#endif

    return CDateTime(year, month, day, hour, minute, second, fractional_seconds);
}


/**
 * Donne la date actuelle.
 *
 * \return Date actuelle de la forme "jj/MM/yyyy".
 ******************************/

CString CDateTime::getCurrentDate()
{
    time_t current_time = time(nullptr);
    char date[11];

    strftime(date, sizeof(date), "%d/%m/%Y", localtime(&current_time));

    return CString(date);
}


/**
 * Donne l'heure actuelle.
 *
 * \return Heure actuelle de la forme "hh:mm:ss".
 ******************************/

CString CDateTime::getCurrentTime(bool milliseconds)
{
    time_t current_time = time(nullptr);
	static char time[20];
	memset(time, 0, 20);

    strftime(time, sizeof(time), "%H:%M:%S", localtime(&current_time));

	if (milliseconds)
	{
		auto now = std::chrono::system_clock::now();
		auto now_ms = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
		std::size_t fractional_seconds = now_ms.count() % 1000000;
		sprintf(time + 8, ".%06d", fractional_seconds);
	}

	return CString(time, 16);
}


/**
 * Vérifie qu'une date est valide.
 *
 * Une date est valide si elle est postérieure au 1er novembre 1582 (début du
 * calendrier grégorien), si le numéro du mois est compris entre 1 et 12, et le
 * numéro du jour est compris entre 1 et le nombre de jour de ce mois.
 *
 * Les heures doivent être comprises entre 0 et 23, les minutes et les secondes
 * entre 0 et 59, les millisecondes entre 0 et 999999.
 *
 * \param year   Année.
 * \param month  Mois.
 * \param day    Jour.
 * \param hour   Heure.
 * \param minute Minute.
 * \param second Seconde.
 * \return Booléen indiquant si la date est valide.
 ******************************/

bool CDateTime::isValid(int year, unsigned int month, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second, unsigned int milliseconds)
{
    // Numéro du jour ou du mois invalide
    if (month > 12 || day > 31 || month == 0 || day == 0)
    {
        return false;
    }

    // Numéro du jour supérieur au nombre de jour dans le mois
    if ((day == 31 && (month == 2 || month == 4 || month == 6 || month == 9 || month == 11)) ||
        (month == 2 && (day == 30 || (day == 29 && !isLeapYear(year)))))
    {
        return false;
    }

    // Date antérieure au 1er novembre 1582
    if (year < 1582 || (year == 1582 && month < 11))
    {
        return false;
    }

    // Heure invalide
    if (hour > 23 || minute > 59 || second > 59 || milliseconds > 999999)
    {
        return false;
    }

    return true;
}


/**
 * Donne le nom d'un jour de la semaine en français.
 *
 * \todo Internationnalisation.
 * \return Nom du jour.
 ******************************/

CString CDateTime::longDayName(unsigned int weekday)
{
    switch (weekday)
    {
        case 1:  return "Lundi";
        case 2:  return "Mardi";
        case 3:  return "Mercredi";
        case 4:  return "Jeudi";
        case 5:  return "Vendredi";
        case 6:  return "Samedi";
        case 7:  return "Dimanche";
        default: return CString();
    }
}


/**
 * Donne le nom d'un mois en français.
 *
 * \todo Internationnalisation.
 * \return Nom du mois.
 ******************************/

CString CDateTime::longMonthName(unsigned int month)
{
    switch (month)
    {
        case 1:  return "Janvier";
        case 2:  return "Février";
        case 3:  return "Mars";
        case 4:  return "Avril";
        case 5:  return "Mai";
        case 6:  return "Juin";
        case 7:  return "Juillet";
        case 8:  return "Aout";
        case 9:  return "Septembre";
        case 10: return "Octobre";
        case 11: return "Novembre";
        case 12: return "Décembre";
        default: return CString();
    }
}


/**
 * Donne le nom court d'un jour de la semaine.
 *
 * \todo Internationnalisation.
 * \return Nom du jour sur trois lettres.
 ******************************/

CString CDateTime::shortDayName(unsigned int weekday)
{
    switch (weekday)
    {
        case 1:  return "Lun";
        case 2:  return "Mar";
        case 3:  return "Mer";
        case 4:  return "Jeu";
        case 5:  return "Ven";
        case 6:  return "Sam";
        case 7:  return "Dim";
        default: return CString();
    }
}


/**
 * Donne le nom court d'un mois.
 *
 * \todo Internationnalisation.
 * \return Nom du mois sur trois lettres.
 ******************************/

CString CDateTime::shortMonthName(unsigned int month)
{
    switch (month)
    {
        case 1:  return "Jan";
        case 2:  return "Fév";
        case 3:  return "Mar";
        case 4:  return "Avr";
        case 5:  return "Mai";
        case 6:  return "Jui";
        case 7:  return "Jul";
        case 8:  return "Aou";
        case 9:  return "Sep";
        case 10: return "Oct";
        case 11: return "Nov";
        case 12: return "Déc";
        default: return CString();
    }
}


/**
 * Indique si une année est bisextile.
 *
 * \param year Année.
 * \return Booléen.
 ******************************/

bool CDateTime::isLeapYear(int year)
{
    if (year % 4 != 0)
    {
        return false;
    }

    if (year % 100 == 0)
    {
        return (year % 400 == 0);
    }

    return true;
}


/**
 * Donne le nombre de jours d'un mois.
 *
 * \param month Mois.
 * \param year  Année.
 * \return Nombre de jours, ou 0 si le mois est invalide.
 ******************************/

unsigned int CDateTime::daysInMonth(unsigned int month, int year)
{
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
    {
        return 31;
    }

    if (month == 4 || month == 6 || month == 9 || month == 11)
    {
        return 30;
    }

    if (month == 2)
    {
        return (isLeapYear(year) ? 29 : 28);
    }

    return 0;
}


/**
 * Donne le numéro du jour dans l'année.
 *
 * \param year  Année.
 * \param month Mois.
 * \param day   Jour.
 * \return Numéro du jour (entre 1 et 366, ou 0 si la date est invalide).
 ******************************/

unsigned int CDateTime::dayOfYear(int year, unsigned int month, unsigned int day)
{
    if (!isValid(year, month, day))
    {
        return 0;
    }

    switch (month)
    {
        default: return 0; // Pour éviter un avertissement du compilateur
        case  1: return day;
        case  2: return day + 31;
        case  3: return day + 31  + (isLeapYear(year) ? 29 : 28);
        case  4: return day + 62  + (isLeapYear(year) ? 29 : 28);
        case  5: return day + 92  + (isLeapYear(year) ? 29 : 28);
        case  6: return day + 123 + (isLeapYear(year) ? 29 : 28);
        case  7: return day + 153 + (isLeapYear(year) ? 29 : 28);
        case  8: return day + 184 + (isLeapYear(year) ? 29 : 28);
        case  9: return day + 215 + (isLeapYear(year) ? 29 : 28);
        case 10: return day + 245 + (isLeapYear(year) ? 29 : 28);
        case 11: return day + 276 + (isLeapYear(year) ? 29 : 28);
        case 12: return day + 306 + (isLeapYear(year) ? 29 : 28);
    }
}


/**
 * Donne le numéro du jour dans la semaine.
 *
 * \param year Année.
 * \param month Mois.
 * \param day Jour.
 * \return Jour de la semaine (entre 1 et 7, ou 0 si la date est invalide).
 ******************************/

unsigned int CDateTime::dayOfWeek(int year, unsigned int month, unsigned int day)
{
    if (!isValid(year, month, day))
    {
        return 0;
    }

    int n = day + ((year % 100) / 12 + (year % 100) % 12 + ((year % 100) % 12) / 4) % 7;

    switch (month)
    {
        default: break;
        case  1: n += 1; break;
        case  2: n += 4; break;
        case  3: n += 4; break;
        case  5: n += 2; break;
        case  6: n += 5; break;
        case  8: n += 3; break;
        case  9: n += 6; break;
        case 10: n += 1; break;
        case 11: n += 4; break;
        case 12: n += 6; break;
    }

    if ((month == 1 || month == 2) && isLeapYear(year))
    {
        --n;
    }

    int tmp = year / 100;

    switch (tmp)
    {
        default: break;
        case 17: n += 4; break;
        case 18: n += 2; break;
        case 19: n += 0; break;
        case 20: n += 6; break;
    }

    n = (n + 6) % 7;
    if (n == 0) n = 7;

    return n;
}


/**
 * Donne le numéro de la semaine au format ISO 8601.
 *
 * \param year Année.
 * \param month Mois.
 * \param day Jour.
 * \return Numéro de la semaine (entre 1 et 53, ou 0 si la date est invalide).
 ******************************/

unsigned int CDateTime::weekOfYear(int year, unsigned int month, unsigned int day)
{
    if (!isValid(year, month, day))
    {
        return 0;
    }

    CDateTime d1(year, month, day);
    int a = dayOfWeek(year, month, day);
    d1.addDays(4 - a); // Jeudi de cette semaine

    int y1 = d1.getYear();

    CDateTime d2(y1, 1, 4);
    int b = dayOfWeek(y1, 1, 4);
    d2.addDays(1 - b); // Lundi de cette semaine

    int y2 = d2.getYear();

    if (y1 < y2)
    {
        unsigned int a1 = (isLeapYear(y1) ? 366 : 365) - dayOfYear(y1, d1.getMonth(), d1.getDay());
        unsigned int a2 = dayOfYear(y2, d2.getMonth(), d2.getDay());

        return (1 + (a1 + a2) / 7);
    }
    else if (y1 > y2)
    {
        unsigned int a1 = dayOfYear(y1, d1.getMonth(), d1.getDay());
        unsigned int a2 = (isLeapYear(y2) ? 366 : 365) - dayOfYear(y2, d2.getMonth(), d2.getDay());

        return (1 + (a1 + a2) / 7);
    }
    else
    {
        unsigned int a1 = dayOfYear(y1, d1.getMonth(), d1.getDay());
        unsigned int a2 = dayOfYear(y2, d2.getMonth(), d2.getDay());

        return (1 + (a2 > a1 ? a2 - a1 : a1 - a2) / 7);
    }
}


/**
 * Donne le nombre de secondes écoulées entre la date et le 1er janvier 1970.
 * Il peut y avoir un décalage en fonction du fuseau horaire.
 *
 * \param year Année.
 * \param month Mois.
 * \param day Jour.
 * \param hour Heure.
 * \param minute Minute.
 * \param second Seconde.
 * \return Nombre de secondes écoulées (ou -1 si la date est invalide ou
 *         antérieure au 1er janvier 1970).
 ******************************/

unsigned int CDateTime::timestamp(int year, unsigned int month, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second)
{
    // Date incorrectes
    if (year < 1970 || !isValid(year, month, day, hour, minute, second))
    {
        return static_cast<unsigned int>(-1);
    }

    unsigned int n = dayOfYear(year, month, day) - 1;

    for (int a = 1970 ; a < year ; ++a)
    {
        n += (isLeapYear(a) ? 366 : 365);
    }

    return (n * 86400 + 3600 * hour + 60 * minute + second);
}


/**
 * Opération binaire -. Ne tient pas compte de l'heure d'été.
 *
 * \todo Tester.
 *
 * \param datetime Date à soustraire.
 * \return Résultat de l'opération sous forme d'une durée (nulle si l'une des
 *         dates est invalide).
 ******************************/

TDuration CDateTime::operator-(const CDateTime &datetime) const
{
    TDuration d = { 0, 0 };

    if (!isValid() || !datetime.isValid())
    {
        return d;
    }

    int year = datetime.getYear();

    // Différence de jours
    int n1 = dayOfYear(m_year, m_month, m_day) - 1;
    int n2 = dayOfYear(year, datetime.getMonth(), datetime.getDay()) - 1;

    d.days = n1 - n2;

    // Durée positive
    if (year < m_year)
    {
        for (int a = year; a < m_year; ++a)
        {
            d.days += (isLeapYear(a) ? 366 : 365);
        }
    }
    // Durée négative
    else if (year > m_year)
    {
        for (int a = m_year; a < year; ++a)
        {
            d.days -= (isLeapYear(a) ? 366 : 365);
        }
    }

    // Différence de secondes
    int s1 = 3600 * m_hour + 60 * m_minute + m_second;
    int s2 = 3600 * datetime.getHour() + 60 * datetime.getMinute() + datetime.getSecond();

    d.seconds = s1 - s2;

    // Le nombre de secondes et le nombre de jours doivent être du même signe
    if (d.seconds < 0 && d.days > 0)
    {
        --d.days;
        d.seconds += 86400;
    }
    else if (d.seconds > 0 && d.days < 0)
    {
        ++d.days;
        d.seconds -= 86400;
    }

    return d;
}


/**
 * Soustrait une durée à une date.
 *
 * \param duration Durée à soustraire.
 * \return Nouvelle date.
 ******************************/

CDateTime CDateTime::operator-(const TDuration& duration) const
{
    CDateTime t = *this;

    t.addDays(-duration.days);
    t.addSeconds(-duration.seconds);

    return t;
}


/**
 * Ajoute une durée à une date.
 *
 * \param duration Durée à ajouter.
 * \return Nouvelle date.
 ******************************/

CDateTime CDateTime::operator+(const TDuration& duration) const
{
    CDateTime t = *this;

    t.addDays(duration.days);
    t.addSeconds(duration.seconds);

    return t;
}


/**
 * Opérateur de comparaison (==).
 *
 * \param datetime Date à comparer.
 * \return Booléen.
 ******************************/

bool CDateTime::operator ==(const CDateTime& datetime) const
{
    return (datetime.getYear()   == m_year   &&
            datetime.getMonth()  == m_month  &&
            datetime.getDay()    == m_day    &&
            datetime.getHour()   == m_hour   &&
            datetime.getMinute() == m_minute &&
            datetime.getSecond() == m_second );
}


/**
 * Opérateur de comparaison (!=).
 *
 * \param datetime Date à comparer.
 * \return Booléen.
 ******************************/

bool CDateTime::operator!=(const CDateTime& datetime) const
{
    return (datetime.getYear()   != m_year   ||
            datetime.getMonth()  != m_month  ||
            datetime.getDay()    != m_day    ||
            datetime.getHour()   != m_hour   ||
            datetime.getMinute() != m_minute ||
            datetime.getSecond() != m_second );
}


/**
 * Opérateur de comparaison (<).
 *
 * \param datetime Date à comparer.
 * \return Booléen.
 ******************************/

bool CDateTime::operator<(const CDateTime& datetime) const
{
    return (datetime.getYear()   > m_year   ||
            datetime.getMonth()  > m_month  ||
            datetime.getDay()    > m_day    ||
            datetime.getHour()   > m_hour   ||
            datetime.getMinute() > m_minute ||
            datetime.getSecond() > m_second );
}


/**
 * Opérateur de comparaison (>).
 *
 * \param datetime Date à comparer.
 * \return Booléen.
 ******************************/

bool CDateTime::operator>(const CDateTime& datetime) const
{
    return (datetime.getYear()   < m_year   ||
            datetime.getMonth()  < m_month  ||
            datetime.getDay()    < m_day    ||
            datetime.getHour()   < m_hour   ||
            datetime.getMinute() < m_minute ||
            datetime.getSecond() < m_second );
}


/**
 * Opérateur de comparaison (<=).
 *
 * \param datetime Date à comparer.
 * \return Booléen.
 ******************************/

bool CDateTime::operator<=(const CDateTime& datetime) const
{
    return (datetime.getYear()   >= m_year   ||
            datetime.getMonth()  >= m_month  ||
            datetime.getDay()    >= m_day    ||
            datetime.getHour()   >= m_hour   ||
            datetime.getMinute() >= m_minute ||
            datetime.getSecond() >= m_second );
}


/**
 * Opérateur de comparaison (>=).
 *
 * \param datetime Date à comparer.
 * \return Booléen.
 ******************************/

bool CDateTime::operator>=(const CDateTime& datetime) const
{
    return (datetime.getYear()   <= m_year   ||
            datetime.getMonth()  <= m_month  ||
            datetime.getDay()    <= m_day    ||
            datetime.getHour()   <= m_hour   ||
            datetime.getMinute() <= m_minute ||
            datetime.getSecond() <= m_second );
}


/**
 * Opérateur d'affectation.
 *
 * \param datetime Nouvelle date.
 * \return Référence sur la date.
 ******************************/

CDateTime& CDateTime::operator=(const CDateTime& datetime)
{
    m_year   = datetime.getYear();
    m_month  = datetime.getMonth();
    m_day    = datetime.getDay();
    m_hour   = datetime.getHour();
    m_minute = datetime.getMinute();
    m_second = datetime.getSecond();

    return *this;
}


/**
 * Opérateur de conversion en booléen.
 *
 * \return Booléen indiquant si la date est valide.
 ******************************/

CDateTime::operator bool() const
{
    return isValid();
}

} // Namespace TE
