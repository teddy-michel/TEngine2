/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Core/CLoggerFile.hpp"
#include "Core/CDateTime.hpp"


namespace TE
{

/**
 * Constructeur par défaut
 *
 * \param file Adresse du fichier de log
 ******************************/

CLoggerFile::CLoggerFile(const CString& file) :
m_file (file.toCharArray())
{
    // Écriture de l'en-tête du fichier
    m_file << "  ================================================" << std::endl;
    m_file << "   Lancement du programme - " << date() << " - " << time() << std::endl;
    m_file << "  ================================================" << std::endl;
    m_file << std::endl;
}


/**
 * Destructeur.
 ******************************/

CLoggerFile::~CLoggerFile()
{
    // Fermeture du fichier
    m_file << std::endl;
    m_file << "  ================================================" << std::endl;
    m_file << "   Fermeture du programme - " << date() << " - " << time() << std::endl;
    m_file << "  ================================================" << std::endl;
}


/**
 * Log un message.
 *
 * \param message Message à inscrire
 ******************************/

void CLoggerFile::write(const CString& message, int level)
{
    if (m_file.is_open())
    {
        m_file << CDateTime::getCurrentTime(true) << " " << level << " " << message << std::endl << std::flush;
    }
}

} // Namespace TE
