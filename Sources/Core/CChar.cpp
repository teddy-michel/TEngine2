/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Core/CChar.hpp"
#include "Core/utf16.hpp"


namespace TE
{

/**
 * Retourne la catégorie du caractère.
 *
 * \return Catégorie du caractère.
 ******************************/

CChar::TCategory CChar::getCategory() const
{
    return static_cast<CChar::TCategory>(utf16_table[m_code].category);
}


/**
 * Donne la valeur numérique d'un caractère.
 *
 * \return Chiffre correspondant au caractère, ou -1 si aucun chiffre ne correspond.
 ******************************/

int CChar::getDigitValue() const
{
    return utf16_table[m_code].numeric;
}


/**
 * Convertit le caractère en minuscule si possible.
 *
 * \return Caractère en minuscule.
 ******************************/

CChar CChar::toLowerCase() const
{
    uint16_t c = utf16_table[m_code].lowercase;
    return CChar(c == 0 ? m_code : c);
}


/**
 * Convertit le caractère en majuscule si possible.
 *
 * \return Caractère en majuscule.
 ******************************/

CChar CChar::toUpperCase() const
{
    uint16_t c = utf16_table[m_code].uppercase;
    return CChar(c == 0 ? m_code : c);
}


/**
 * Convertit le caractère en lettre de titre si possible.
 *
 * \return Caractère en lettre de titre.
 ******************************/

CChar CChar::toTitleCase() const
{
    uint16_t c = utf16_table[m_code].titlecase;
    return CChar(c == 0 ? m_code : c);
}

} // Namespace TE
