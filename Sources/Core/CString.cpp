/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <sstream>
#include <iomanip>

#include "Core/CString.hpp"


namespace TE
{

CString::TEncoding CString::m_encoding = Latin1;

CChar sharedEmptyChar;
char sharedEmptyArray[1] = "";
CString::TData CString::sharedEmpty = { &sharedEmptyChar, sharedEmpty.m_start, sharedEmpty.m_end, 1, sharedEmptyArray, sharedEmpty.m_str + 1 };


/**
 * Construit une chaine vide.
 ******************************/

CString::CString() :
	m_data{ &sharedEmpty }
{
    ++(m_data->m_count);
}


/**
 * Construit une chaine à partir d'une partie d'un tableau de CChar.
 * Le dernier caractère doit être le caractère nul.
 *
 * \param other Tableau de caractères, terminé par le caractère nul.
 * \param n     Nombre de caractères à copier. Si la longueur de other est
 *              inférieure à n, tous les caractères sont copiés.
 ******************************/

CString::CString(const CChar * other, std::size_t n) :
	m_data{ nullptr }
{
    if (other == nullptr || n == 0)
    {
        m_data = &sharedEmpty;
        ++(m_data->m_count);
    }
    else
    {
        std::size_t s = 0, i = 0;

        const CChar * other_ptr = other;

        while (!other_ptr->isNull() && s < n)
        {
            ++other_ptr;
            ++s;
        }

        m_data = new TData;
        m_data->m_count     = 1;
        m_data->m_str       = nullptr;
        m_data->m_str_alloc = nullptr;

        allocateArray(m_data, s);

        CChar * ptr = m_data->m_start;

        // Copie de la chaine caractère par caractère
        while (!other->isNull() && i < s)
        {
            *ptr++ = *other++;
            ++i;
        }

        *(m_data->m_end) = CChar();
    }
}


/**
 * Crée une chaine avec n fois le caractère c.
 *
 * \param c Caractère à utiliser.
 * \param n Nombre de caractères.
 ******************************/

CString::CString(CChar c, std::size_t n) :
	m_data{ nullptr }
{
    if (n == 0)
    {
        m_data = &sharedEmpty;
        ++(m_data->m_count);
    }
    else
    {
        m_data = new TData;
        m_data->m_count     = 1;
        m_data->m_str       = nullptr;
        m_data->m_str_alloc = nullptr;

        allocateArray(m_data, n);

        if (n > 0)
        {
            std::fill(m_data->m_start, m_data->m_end, c);
        }

        *(m_data->m_end) = CChar();
    }
}


/**
 * Crée une chaine à partir d'un tableau de caractères.
 * L'encodage par défaut est utilisé pour convertir la chaine.
 *
 * \param other Tableau de caractère, terminé par le caractère nul.
 * \paral len   Taille de la chaine. Si négatif, la taille est calculée avec strlen.
 ******************************/

CString::CString(const char * other, int len) :
	m_data{ nullptr }
{
    switch (m_encoding)
    {
        default:
        case Latin1: m_data = fromLatin1Helper(other, len); break;
        case Latin9: m_data = fromLatin9Helper(other, len); break;
        case UTF8:   m_data = fromUTF8Helper  (other, len); break;
    }
}


/**
 * Libère la mémoire occupée par la chaine de caractères.
 * Les données ne sont supprimées que si aucune autre CString ne les utilisent.
 ******************************/

CString::~CString()
{
    if (--(m_data->m_count) == 0)
    {
        // Les données ne sont pas partagées
        delete m_data;
    }
}


/**
 * Opérateur d'affectation avec un caractère.
 *
 * \param c Caractère.
 * \return Référence sur la chaine.
 ******************************/

CString& CString::operator=(CChar c)
{
    return operator=(CString(c));
}


/**
 * Opérateur d'affectation avec une autre chaine.
 *
 * \param str Chaine à copier.
 * \return Référence sur la chaine.
 ******************************/

CString& CString::operator=(const CString& str)
{
    ++(str.m_data->m_count);

    if (--(m_data->m_count) == 0)
    {
        delete m_data;
    }

    m_data = str.m_data;

    return *this;
}


/**
 * Efface le contenu de la chaine.
 * Équivaut à appeler Resize() avec une taille de 0.
 ******************************/

void CString::clear()
{
    if (getSize() > 0)
    {
        copyData();

        m_data->m_end = m_data->m_start;
        *(m_data->m_start) = CChar();
    }
}


/**
 * Alloue de la mémoire pour stocker un nombre prédéfini de caractères.
 * Si s est inférieur au nombre de caractères déjà alloué, rien n'est fait.
 *
 * \param s Nombre de caractères à réserver.
 ******************************/

void CString::reserve(std::size_t s)
{
	if (s == 0)
		return;

    const std::size_t oldSize = getSize();

    if (oldSize < s)
    {
		if (m_data->m_count > 1)
		{
			TData * data = new TData;
			data->m_count = 1;
			data->m_str = nullptr;
			data->m_str_alloc = nullptr;

			// Copie de la chaine
			allocateArray(data, s);
			std::copy(m_data->m_start, m_data->m_end, data->m_start);
			data->m_end = data->m_start + oldSize;
			*(data->m_end) = CChar();

			// Suppression des anciennes données
			if (--(m_data->m_count) == 0)
			{
				delete m_data;
			}

			m_data = data;
		}
		else
		{
			CChar * oldStart = m_data->m_start;
			CChar * oldEnd = m_data->m_end;

			allocateArray(m_data, s);
			m_data->m_end = m_data->m_start + oldSize;

			std::copy(oldStart, oldEnd, m_data->m_start);

			delete[] oldStart;
		}
    }
}


/**
 * Redimensionne la chaine de caractères.
 * Si n est inférieur à la taille actuelle, la chaine est tronquée.
 * Si n est égale à la taille actuelle, rien n'est pas fait.
 * Si n est supérieur à la taille actuelle, la chaine est agrandie avec la caractère c.
 *
 * \param n Taille de la chaine après redimension.
 * \param c Caractère à utiliser pour remplir la chaine, si n est supérieur à la taille actuelle.
 ******************************/

void CString::resize(std::size_t n, CChar c)
{
    const std::size_t s = getSize();

    if (s == n)
        return;

    // Chaine vide
    if (n == 0)
    {
        // Suppression des anciennes données
        if (--(m_data->m_count) == 0)
        {
            delete m_data;
        }

        m_data = &sharedEmpty;
        ++(m_data->m_count);
    }

    // La longueur diminue
    if (n < s)
    {
        // Réallocation
        if (m_data->m_count > 1)
        {
            TData * data = new TData;
            data->m_count     = 1;
            data->m_str       = nullptr;
            data->m_str_alloc = nullptr;

            // Copie de la chaine
            allocateArray(data, n);

            std::copy(m_data->m_start, m_data->m_end, data->m_start);
            *(data->m_end) = CChar();

            // Suppression des anciennes données
            if (--(m_data->m_count) == 0)
            {
                delete m_data;
            }

            m_data = data;
        }
        else
        {
            m_data->m_end = m_data->m_start + n;
            *(m_data->m_end) = CChar();
        }

        return;
    }

    // La longueur augmente mais il y a assez de mémoire allouée
    if (n <= getCapacity())
    {
        // Réallocation
        if (m_data->m_count > 1)
        {
            TData * data = new TData;
            data->m_count     = 1;
            data->m_str       = nullptr;
            data->m_str_alloc = nullptr;

            // Copie de la chaine
            allocateArray(data, n);

            std::copy(m_data->m_start, m_data->m_start + s, data->m_start);
            std::fill(data->m_start + s, data->m_end, c);
            *(data->m_end) = CChar();

            // Suppression des anciennes données
            if (--(m_data->m_count) == 0)
            {
                delete m_data;
            }

            m_data = data;
        }
        else
        {
            std::fill(m_data->m_end, m_data->m_start + n, c);
            m_data->m_end = m_data->m_start + n;
            *(m_data->m_end) = CChar();
        }

        return;
    }

    // Il faut obligatoirement réallouer
    TData * data = new TData;
    data->m_count     = 1;
    data->m_str       = nullptr;
    data->m_str_alloc = nullptr;

    // Copie de la chaine
    allocateArray(data, n);

    std::copy(m_data->m_start, m_data->m_start + s, data->m_start);
    std::fill(data->m_start + s, data->m_end, c);
    *(data->m_end) = CChar();

    // Suppression des anciennes données
    if (--(m_data->m_count) == 0)
    {
        delete m_data;
    }

    m_data = data;
}


/**
 * Redimensionne la chaine de caractères.
 * Si n est inférieur à la taille actuelle, la chaine est tronquée.
 * Si n est égale à la taille actuelle, rien n'est pas fait.
 * Si n est supérieur à la taille actuelle, la chaine est agrandie, mais les
 * caractères ajoutés ne sont pas initialisés.
 *
 * \param n Taille de la chaine après redimension.
 ******************************/

void CString::resize(std::size_t n)
{
    const std::size_t s = getSize();

    if (s == n)
        return;

    // La longueur diminue
    if (n < s)
    {
        // Réallocation
        if (m_data->m_count > 1)
        {
            TData * data;

            if (n == 0)
            {
                data = &sharedEmpty;
                ++(data->m_count);
            }
            else
            {
                data = new TData;
                data->m_count     = 1;
                data->m_str       = nullptr;
                data->m_str_alloc = nullptr;

                // Copie de la chaine
                allocateArray(data, n);

                std::copy(m_data->m_start, m_data->m_end, data->m_start);
                *(data->m_end) = CChar();
            }

            // Suppression des anciennes données
            if (--(m_data->m_count) == 0)
            {
                delete m_data;
            }

            m_data = data;
        }
        else
        {
            m_data->m_end = m_data->m_start + n;
            *(m_data->m_end) = CChar();
        }

        return;
    }

    // La longueur augmente mais il y a assez de mémoire allouée
    if (n <= getCapacity())
    {
        // Réallocation
        if (m_data->m_count > 1)
        {
            TData * data = new TData;
            data->m_count     = 1;
            data->m_str       = nullptr;
            data->m_str_alloc = nullptr;

            // Copie de la chaine
            allocateArray(data, n);

            std::copy(m_data->m_start, m_data->m_start + s, data->m_start);
            *(data->m_end) = CChar();

            // Suppression des anciennes données
            if (--(m_data->m_count) == 0)
            {
                delete m_data;
            }

            m_data = data;
        }
        else
        {
            m_data->m_end = m_data->m_start + n;
            *(m_data->m_end) = CChar();
        }

        return;
    }

    // Il faut obligatoirement réallouer
    TData * data = new TData;
    data->m_count     = 1;
    data->m_str       = nullptr;
    data->m_str_alloc = nullptr;

    // Copie de la chaine
    allocateArray(data, n);

    std::copy(m_data->m_start, m_data->m_start + s, data->m_start);
    *(data->m_end) = CChar();

    // Suppression des anciennes données
    if (--(m_data->m_count) == 0)
    {
        delete m_data;
    }

    m_data = data;
}


/**
 * Libère la mémoire inutilement occupée par la chaine.
 * Utiliser cette méthode si la chaine est fixe et que la quantité de mémoire est limitée.
 ******************************/

void CString::squeeze()
{
    const std::size_t size_str = getSize();
    const std::size_t size_all = getCapacity();

    // Si c'est strictement inférieur, il y a un problème quelque part...
    if (size_str <= size_all)
        return;

    CChar * cpy = new CChar[size_str+1];

    // Copie de la chaine
    std::copy(m_data->m_start, m_data->m_end, cpy);
    delete[] m_data->m_start;

    m_data->m_start = cpy;
    m_data->m_end   = m_data->m_start + size_str;
    m_data->m_alloc = m_data->m_end;
}


/**
 * Simplifie la chaine de caractères, en supprimant les caractères d'espacement au début et à la fin.
 * Les caractères d'espacement sont ceux qui vérifient CChar::isSpace(), y compris les caractères
 * '\\n', '\\r', '\\t', et '\\v'.
 *
 * \return Chaine simplifiée.
 ******************************/

CString CString::getTrimmed() const
{
    const std::size_t len = getSize();

    if (len == 0)
        return CString();

    std::size_t spaces_begin = 0;

    // Nombre d'espaces au début
    for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
    {
        if (str_ptr->isSpace())
        {
            ++spaces_begin;
        }
        else
        {
            break;
        }
    }

    if (spaces_begin == len)
        return CString();

    std::size_t spaces_end = 0;

    // Nombre d'espaces à la fin
    for (CChar * str_ptr = m_data->m_end - 1; str_ptr >= m_data->m_start; --str_ptr)
    {
        if (str_ptr->isSpace())
        {
            ++spaces_end;
        }
        else
        {
            break;
        }
    }

    if (spaces_begin == 0 && spaces_end == 0)
    {
        return CString(*this);
    }

    // Copie de la chaine
    return CString(m_data->m_start + spaces_begin, len - spaces_begin - spaces_end);
}


/**
 * Simplifie la chaine de caractères, en supprimant les caractères d'espacement au début, à la
 * fin, et en remplaçant les caractères d'espacement multiples par une seule espace simple.
 * Les caractères d'espacement sont ceux qui vérifient CChar::isSpace(), y compris les caractères
 * '\\n', '\\r', '\\t', et '\\v'.
 *
 * \return Chaine simplifiée.
 ******************************/

CString CString::getSimplified() const
{
    const std::size_t len = getSize();

    if (len == 0)
        return CString();

    std::size_t spaces = 0;
    bool space_before = true;

    // Nombre d'espaces au début
    for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
    {
        if (str_ptr->isSpace())
        {
            if (space_before)
            {
                ++spaces;
            }
            else
            {
                space_before = true;
            }
        }
        else
        {
            space_before = false;
        }
    }

    // Espace final
    if (space_before && spaces < len)
        ++spaces;

    const std::size_t len_new = len - spaces;

    if (len_new == 0)
        return CString();

    // Création d'une nouvelle chaine
    TData * data = new TData;
    data->m_count     = 1;
    data->m_str       = nullptr;
    data->m_str_alloc = nullptr;

    allocateArray(data, len_new);

    CChar * ptr = data->m_start;
    space_before = true;

    // Copie des caractères
    for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
    {
        if (str_ptr->isSpace())
        {
            if (!space_before)
            {
                *ptr++ = CChar(' ');
                space_before = true;
            }
        }
        else
        {
            *ptr++ = *str_ptr;
            space_before = false;
        }
    }

    // On termine la chaine par le caractère nul
    *(data->m_end) = CChar();

    return CString(data);
}


/**
 * Convertit la chaine en minuscules.
 *
 * \return Chaine en minuscules.
 ******************************/

CString CString::toLowerCase() const
{
    CString str(*this);
    str.copyData();

    for (CChar * str_ptr = str.m_data->m_start; str_ptr < str.m_data->m_end; ++str_ptr)
    {
        *str_ptr = str_ptr->toLowerCase();
    }

    return str;
}


/**
 * Convertit la chaine en majuscules.
 *
 * \return Chaine en majuscules.
 ******************************/

CString CString::toUpperCase() const
{
    CString str(*this);
    str.copyData();

    for (CChar * str_ptr = str.m_data->m_start; str_ptr < str.m_data->m_end; ++str_ptr)
    {
        *str_ptr = str_ptr->toUpperCase();
    }

    return str;
}


/**
 * Convertit la chaine en lettres de titre.
 *
 * \return Chaine en lettres de titre.
 ******************************/

CString CString::toTitleCase() const
{
    CString str(*this);
    str.copyData();

    for (CChar * str_ptr = str.m_data->m_start; str_ptr < str.m_data->m_end; ++str_ptr)
    {
        *str_ptr = str_ptr->toTitleCase();
    }

    return str;
}


/**
 * Compare la chaine avec une autre et retourne un entier.
 *
 * \param str Chaine à utiliser pour la comparaison.
 * \param cs  Booléen indiquant si la conversion est sensible à la casse.
 * \return Entier négatif, nul, ou positif, si la chaine est inférieure, égale, ou supérieure à \a str.
 ******************************/

int CString::compare(const CString& str, bool cs) const
{
    CChar * ptr1 = m_data->m_start;
    CChar * ptr2 = str.m_data->m_start;

    // On parcourt les deux chaines caractère par caractère
    if (cs)
    {
        for ( ; ptr1 < m_data->m_end && ptr2 < str.m_data->m_end; ++ptr1, ++ptr2)
        {
            if (*ptr1 < *ptr2)
                return -1;

            if (*ptr1 > *ptr2)
                return 1;
        }
    }
    else
    {
        for ( ; ptr1 < m_data->m_end && ptr2 < str.m_data->m_end; ++ptr1, ++ptr2)
        {
            CChar ptr1_lower = ptr1->toLowerCase();
            CChar ptr2_lower = ptr2->toLowerCase();

            if (ptr1_lower < ptr2_lower)
                return -1;

            if (ptr1_lower > ptr2_lower)
                return 1;
        }
    }

    // Tous les caractères sont identiques, on compare la longueur des chaines
    return (static_cast<int>(getSize()) - static_cast<int>(str.getSize()));
}


/**
 * Découpe la chaine en utilisant un caractère comme séparateur.
 *
 * \param separator Caractère de séparation entre les chaines.
 * \param keepEmpty Indique si on doit ajouter les chaines vides dans la liste.
 * \return Liste de chaines de caractères.
 ******************************/

std::list<CString> CString::split(const CChar& separator, bool keepEmpty) const
{
    std::list<CString> ret;
    CChar * substr = m_data->m_start;

    // Parcours de la chaine caractère par caractère
    for (CChar * str_ptr = m_data->m_start ; str_ptr < m_data->m_end ; ++str_ptr)
    {
        if (*str_ptr == separator)
        {
            if (str_ptr > substr || keepEmpty)
            {
                ret.push_back(CString(substr, str_ptr - substr));
            }

            substr = str_ptr + 1;
        }
    }

    // Aucun séparateur trouvé
    if (substr == m_data->m_start)
    {
        if (!isEmpty() || keepEmpty)
        {
            ret.push_back(*this);
        }
    }
    // Le dernier caractère est un séparateur
    else if (substr == m_data->m_end)
    {
        if (keepEmpty)
        {
            ret.push_back(CString());
        }
    }
    else
    {
        ret.push_back(CString(substr, m_data->m_end - substr));
    }

    return ret;
}


/**
 * Découpe la chaine en utilisant unr chaine comme séparateur.
 *
 * \param str       Chaine de séparation entre les chaines.
 * \param keepEmpty Indique si on doit ajouter les chaines vides dans la liste.
 * \return Liste de chaines de caractères.
 ******************************/

std::list<CString> CString::split(const CString& str, bool keepEmpty) const
{
    std::list<CString> ret;

    if (m_data == str.m_data)
    {
        ret.push_back(*this);
        return ret;
    }

    const std::size_t this_len = getSize();
    const std::size_t str_len = str.getSize();

    if (this_len < str_len || str_len == 0 || this_len == 0)
    {
        if (this_len > 0 || keepEmpty)
        {
            ret.push_back(*this);
        }

        return ret;
    }

    //CChar * substr = m_data->m_start;
    std::size_t from = 0;

    // Parcours de la chaine
    while (from < this_len)
    {
        int v = indexOf(str, from);

        // Il n'y a plus d'occurences de str
        if (v == - 1)
        {
            if (m_data->m_start + from < m_data->m_end || keepEmpty)
            {
                // On ajoute la fin de la chaine
                ret.push_back(CString(m_data->m_start + from));
            }

            break;
        }
        else
        {
            if (static_cast<std::size_t>(v) > from || keepEmpty)
            {
                ret.push_back(CString(m_data->m_start + from, v - from));
            }

            from = v + str_len;
        }
    }
/*
    if (ret.size() == 0)
    {
        ret.push_back(*this);
    }
*/
    return ret;
}


/**
 * Retourne une partie de la chaine de caractère.
 *
 * \param from Position où commencer la copie.
 * \param n    Nombre de caractères à copier. Si \a n est supérieur au nombre de caractères
 *             disponibles à partir de \a from, tous les caractères après \a from sont copiés.
 * \return Chaine de caractères.
 ******************************/

CString CString::subString(std::size_t from, std::size_t n) const
{
    const std::size_t s = getSize();

    if (from >= s)
        return CString();

    if (n + from > s)
        n = s - from;

    if (from == 0 && n == s)
        return *this;

    return CString(m_data->m_start + from, n);
}


/**
 * Donne l'indice de la première occurence de la chaine \a str dans la chaine de caractères,
 * en recherchant à partir du début.
 *
 * \param str  Chaine à rechercher.
 * \param from Position où commencer la recherche. Si \a from est négatif, la recherche
 *             commence à partir de la fin (-1 est le dernier caractère).
 * \param cs   Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Indice du premier caractère de la chaine, ou -1 s'il n'a pas été trouvé.
 ******************************/

std::ptrdiff_t CString::indexOf(const CString& str, std::ptrdiff_t from, bool cs) const
{
    if (str.m_data == m_data)
        return 0;

    CChar * str_ptr = (from < 0 ? m_data->m_end + from : m_data->m_start + from);

    if (str_ptr < m_data->m_start || str_ptr >= m_data->m_end)
        return -1;

    const std::size_t str_s = str.getSize();

    if (str_s == 0)
        return -1;

    std::size_t correct = 0;

    if (cs)
    {
        // On parcourt la chaine caractère par caractère
        for ( ; str_ptr < m_data->m_end; ++str_ptr)
        {
            // Correspondance
            if (*str_ptr == *(str.m_data->m_start + correct))
            {
                ++correct;

                // Tous les caractères ont été trouvés
                if (correct == str_s)
                {
                    return (str_ptr - str_s - m_data->m_start + 1);
                }
            }
            else
            {
                correct = 0;
            }
        }
    }
    else
    {
        // On parcourt la chaine caractère par caractère
        for ( ; str_ptr < m_data->m_end; ++str_ptr)
        {
            // Correspondance
            if (str_ptr->toLowerCase() == (str.m_data->m_start + correct)->toLowerCase())
            {
                ++correct;

                // Tous les caractères ont été trouvés
                if (correct == str_s)
                {
                    return (str_ptr - str_s - m_data->m_start + 1);
                }
            }
            else
            {
                correct = 0;
            }
        }
    }

    return -1;
}


/**
 * Donne l'indice de la première occurence du caractère \a ch dans la chaine de caractères, en
 * recherchant à partir du début.
 *
 * \param ch   Caractère à rechercher.
 * \param from Position où commencer la recherche. Si \a from est négatif, la recherche
 *             commence à partir de la fin (-1 est le dernier caractère).
 * \param cs   Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Indice du caractère, ou -1 s'il n'a pas été trouvé.
 ******************************/

std::ptrdiff_t CString::indexOf(const CChar& ch, std::ptrdiff_t from, bool cs) const
{
    CChar * str_ptr = (from < 0 ? m_data->m_end + from : m_data->m_start + from);

    if (str_ptr < m_data->m_start || str_ptr >= m_data->m_end)
        return -1;

    if (cs)
    {
        for ( ; str_ptr < m_data->m_end; ++str_ptr)
        {
            if (*str_ptr == ch)
            {
                return (str_ptr - m_data->m_start);
            }
        }
    }
    else
    {
        const CChar c = ch.toLowerCase();

        for ( ; str_ptr < m_data->m_end; ++str_ptr)
        {
            if (str_ptr->toLowerCase() == c)
            {
                return (str_ptr - m_data->m_start);
            }
        }
    }

    return -1;
}


/**
 * Donne l'indice de la dernière occurence de la chaine \a ch dans la chaine de caractères,
 * en recherchant à partir de la fin.
 *
 * \param str  Chaine à rechercher.
 * \param from Position où commencer la recherche. Si \a from est négatif, la recherche
 *             commence à partir de la fin (-1 est le dernier caractère).
 * \param cs   Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Indice du premier caractère de la chaine, ou -1 s'il n'a pas été trouvé.
 ******************************/

std::ptrdiff_t CString::lastIndexOf(const CString& str, std::ptrdiff_t from, bool cs) const
{
    if (str.m_data == m_data)
        return 0;

    CChar * str_ptr = (from < 0 ? m_data->m_end + from : m_data->m_start + from);

    if (str_ptr < m_data->m_start || str_ptr >= m_data->m_end)
        return -1;

    const std::size_t str_s = str.getSize();

    if (str_s == 0)
        return -1;

    std::size_t correct = str_s;

    if (cs)
    {
        // On parcourt la chaine caractère par caractère
        for ( ; str_ptr >= m_data->m_start; --str_ptr)
        {
            // Correspondance
            if (*str_ptr == *(str.m_data->m_start + correct - 1))
            {
                --correct;

                // Tous les caractères ont été trouvés
                if (correct == 0)
                {
                    return (str_ptr - str_s - m_data->m_start + 2);
                }
            }
            else
            {
                correct = str_s;
            }
        }
    }
    else
    {
        // On parcourt la chaine caractère par caractère
        for ( ; str_ptr >= m_data->m_start; --str_ptr)
        {
            // Correspondance
            if (str_ptr->toLowerCase() == (str.m_data->m_start + correct - 1)->toLowerCase())
            {
                --correct;

                // Tous les caractères ont été trouvés
                if (correct == 0)
                {
                    return (str_ptr - str_s - m_data->m_start + 2);
                }
            }
            else
            {
                correct = str_s;
            }
        }
    }

    return -1;
}


/**
 * Donne l'indice de la dernière occurence du caractère \a ch dans la chaine de caractères, en
 * recherchant à partir de la fin.
 *
 * \param ch   Caractère à rechercher.
 * \param from Position où commencer la recherche. Si \a from est négatif, la recherche
 *             commence à partir de la fin (-1 est le dernier caractère).
 * \param cs   Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Indice du caractère, ou -1 s'il n'a pas été trouvé.
 ******************************/

std::ptrdiff_t CString::lastIndexOf(const CChar& ch, std::ptrdiff_t from, bool cs) const
{
    CChar * str_ptr = (from < 0 ? m_data->m_end + from : m_data->m_start + from);

    if (str_ptr < m_data->m_start || str_ptr >= m_data->m_end)
        return -1;

    if (cs)
    {
        for ( ; str_ptr >= m_data->m_start; --str_ptr)
        {
            if (*str_ptr == ch)
            {
                return (str_ptr - m_data->m_start);
            }
        }
    }
    else
    {
        const CChar c = ch.toLowerCase();

        for ( ; str_ptr >= m_data->m_start; --str_ptr)
        {
            if (str_ptr->toLowerCase() == c)
            {
                return (str_ptr - m_data->m_start);
            }
        }
    }

    return -1;
}


/**
 * Teste si la chaine de caractères commence par la chaine \a str.
 *
 * \param str Chaine à rechercher.
 * \param cs  Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Booléen.
 ******************************/

bool CString::startsWith(const CString& str, bool cs) const
{
    if (str.m_data == m_data)
        return true;

    const std::size_t str_s = str.getSize();

    if (str_s == 0 || str_s > getSize())
    {
        return false;
    }

    if (cs)
    {
        // On compare le début des deux chaines
        for (std::size_t i = 0; i < str_s; ++i)
        {
            if (*(m_data->m_start + i) != *(str.m_data->m_start + i))
            {
                return false;
            }
        }

        return true;
    }
    else
    {
        // On compare le début des deux chaines
        for (std::size_t i = 0; i < str_s; ++i)
        {
            if ((m_data->m_start + i)->toLowerCase() != (str.m_data->m_start + i)->toLowerCase())
            {
                return false;
            }
        }

        return true;
    }
}


/**
 * Teste si la chaine de caractères commence par le caractère \a ch.
 *
 * \param ch Caractère à rechercher.
 * \param cs Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Booléen.
 ******************************/

bool CString::startsWith(const CChar& ch, bool cs) const
{
    // Chaine vide
    if (m_data->m_end == m_data->m_start)
    {
        return false;
    }

    if (cs)
    {
        return (*(m_data->m_start) == ch);
    }
    else
    {
        return (m_data->m_start->toLowerCase() == ch.toLowerCase());
    }
}


/**
 * Teste si la chaine de caractères se termine par la chaine \a str.
 *
 * \param str Chaine à rechercher.
 * \param cs  Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Booléen.
 ******************************/

bool CString::endsWith(const CString& str, bool cs) const
{
    if (str.m_data == m_data)
        return true;

    const std::size_t str_s = str.getSize();

    if (str_s == 0 || str_s > getSize())
        return false;

    if (cs)
    {
        // On compare la fin des deux chaines
        for (std::size_t i = 0; i < str_s; ++i)
        {
            if (*(m_data->m_end - i) != *(str.m_data->m_end - i))
            {
                return false;
            }
        }

        return true;
    }
    else
    {
        // On compare la fin des deux chaines
        for (std::size_t i = 0; i < str_s; ++i)
        {
            if ((m_data->m_end - i)->toLowerCase() != (str.m_data->m_end - i)->toLowerCase())
            {
                return false;
            }
        }

        return true;
    }
}


/**
 * Teste si la chaine de caractères se termine par le caractère \a ch.
 *
 * \param ch Caractère à rechercher.
 * \param cs Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Booléen.
 ******************************/

bool CString::endsWith(const CChar& ch, bool cs) const
{
    // Chaine vide
    if (m_data->m_end == m_data->m_start)
        return false;

    if (cs)
    {
        return (*(m_data->m_end - 1) == ch);
    }
    else
    {
        return ((m_data->m_end - 1)->toLowerCase() == ch.toLowerCase());
    }
}


/**
 * Teste si la chaine de caractères contient la chaine \a str.
 *
 * \param str Chaine à rechercher.
 * \param cs  Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Booléen.
 ******************************/

bool CString::contains(const CString& str, bool cs) const
{
    return (indexOf(str, cs) != -1);
}


/**
 * Teste si la chaine de caractères contient le caractère \a ch.
 *
 * \param ch Caractère à rechercher.
 * \param cs Booléen indiquant si la recherche doit tenir compte de la casse.
 * \return Booléen.
 ******************************/

bool CString::contains(const CChar& ch, bool cs) const
{
    if (cs)
    {
        for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
        {
            if (*str_ptr == ch)
            {
                return true;
            }
        }
    }
    else
    {
        const CChar c = ch.toLowerCase();

        for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
        {
            if (str_ptr->toLowerCase() == c)
            {
                return true;
            }
        }
    }

    return false;
}


CString& CString::replace(const CChar& from, const CChar& to)
{
    if (from == to)
    {
        return *this;
    }

    bool first = true;

    for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
    {
        if (*str_ptr == from)
        {
            if (first)
            {
                // Réallocation
                if (m_data->m_count > 1)
                {
                    const std::size_t len = getSize();

                    TData * data = new TData;
                    data->m_count     = 1;
                    data->m_str       = nullptr;
                    data->m_str_alloc = nullptr;

                    // Copie de la chaine
                    allocateArray(data, len);

                    std::copy(m_data->m_start, m_data->m_start + len, data->m_start);
                    *(data->m_end) = CChar();

                    str_ptr = data->m_start + (str_ptr - m_data->m_start);

                    // Suppression des anciennes données
                    if (--(m_data->m_count) == 0)
                    {
                        delete m_data;
                    }

                    m_data = data;
                }

                first = false;
            }

            *str_ptr = to;
        }
    }

    return *this;
}


CString CString::arg(const CString& a) const
{
    int indice = 100;
    int occ = 0;

    for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; ++str_ptr)
    {
        if (*str_ptr == '%')
        {
            ++str_ptr;

            if (str_ptr == m_data->m_end)
                break;

            int digitValue = str_ptr->getDigitValue();

            if (digitValue == -1)
                continue;

            ++str_ptr;

            if (str_ptr != m_data->m_end && str_ptr->getDigitValue() != -1)
            {
                digitValue = (10 * digitValue) + str_ptr->getDigitValue();
                ++str_ptr;
            }

            if (digitValue > indice)
                continue;

            if (digitValue < indice)
            {
                indice = digitValue;
                occ = 0;
            }

            ++occ;
        }
    }

    if (occ == 0)
        return *this;

    CString res;
    const std::size_t aSize = a.getSize();
    res.resize(getSize() + occ * (aSize - (indice < 10 ? 2 : 3)));

    CChar * resPtr = res.m_data->m_start;

    for (CChar * str_ptr = m_data->m_start; str_ptr < m_data->m_end; )
    {
        if (*str_ptr == '%')
        {
            CChar * start = str_ptr;

            ++str_ptr;

          //if (str_ptr == m_data->m_end)
          //    break;

            int digitValue = str_ptr->getDigitValue();

            ++str_ptr;

            if (digitValue == -1)
                continue;

            if (str_ptr != m_data->m_end && str_ptr->getDigitValue() != -1)
            {
                digitValue = (10 * digitValue) + str_ptr->getDigitValue();
                ++str_ptr;
            }

            // Indice différent
            if (digitValue != indice)
            {
                std::copy(start, str_ptr, resPtr);
                resPtr += (str_ptr - start);
                continue;
            }

            std::copy(a.m_data->m_start, a.m_data->m_end, resPtr);
            resPtr += aSize;
        }
        else
        {
            *resPtr++ = *str_ptr++;
        }
    }

    return res;
}


/**
 * Convertit la chaine en un double.
 *
 * \param ok Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \return Valeur convertie.
 ******************************/

double CString::toDouble(bool * ok) const
{
    double res;
    std::istringstream iss(toLatin1());

    if ((iss >> res))
    {
        if (ok != nullptr)
            *ok = true;
        return res;
    }

    if (ok != nullptr)
        *ok = false;
    return 0.0f;
}


/**
 * Convertit la chaine en un float.
 *
 * \param ok Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \return Valeur convertie.
 ******************************/

float CString::toFloat(bool * ok) const
{
    float res;
    std::istringstream iss(toLatin1());

    if ((iss >> res))
    {
        if (ok != nullptr)
            *ok = true;
        return res;
    }

    if (ok != nullptr)
        *ok = false;
    return 0.0f;
}


/**
 * Convertit la chaine en un entier sur 8 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

int8_t CString::toInt8(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    int64_t result = convertToInt64(base, &p_ok);

    if (result < std::numeric_limits<int8_t>::min() || result > std::numeric_limits<int8_t>::max())
    {
        if (ok != nullptr)
            *ok = false;
        return 0;
    }

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier non signé sur 8 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

uint8_t CString::toUnsignedInt8(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    uint64_t result = convertToUnsignedInt64(base, &p_ok);

    if (result > std::numeric_limits<uint8_t>::max())
    {
        if (ok != nullptr)
            *ok = false;
        return 0;
    }

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier sur 16 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

int16_t CString::toInt16(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    int64_t result = convertToInt64(base, &p_ok);

    if (result < std::numeric_limits<int16_t>::min() || result > std::numeric_limits<int16_t>::max())
    {
        if (ok != nullptr)
            *ok = false;
        return 0;
    }

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier non signé sur 16 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

uint16_t CString::toUnsignedInt16(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    uint64_t result = convertToUnsignedInt64(base, &p_ok);

    if (result > std::numeric_limits<uint16_t>::max())
    {
        if (ok != nullptr)
            *ok = false;
        return 0;
    }

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier sur 32 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

int32_t CString::toInt32(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    int64_t result = convertToInt64(base, &p_ok);

    if (result < std::numeric_limits<int32_t>::min() || result > std::numeric_limits<int32_t>::max())
    {
        if (ok != nullptr)
            *ok = false;
        return 0;
    }

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier non signé sur 32 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

uint32_t CString::toUnsignedInt32(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    uint64_t result = convertToUnsignedInt64(base, &p_ok);

    if (result > std::numeric_limits<uint32_t>::max())
    {
        if (ok != nullptr)
            *ok = false;
        return 0;
    }

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier sur 64 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

int64_t CString::toInt64(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    int64_t result = convertToInt64(base, &p_ok);

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Convertit la chaine en un entier non signé sur 64 bits.
 * Si la base est supérieure à 10, les caractères alphabétiques minuscules et majuscules sont utilisés.
 *
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \param base Base à utiliser, entre 2 et 36 (10 par défaut).
 * \return Valeur convertie.
 ******************************/

uint64_t CString::toUnsignedInt64(bool * ok, int base) const
{
    if (base < 2 || base > 36)
        base = 10;

    bool p_ok;
    uint64_t result = convertToUnsignedInt64(base, &p_ok);

    if (p_ok)
    {
        if (ok != nullptr)
            *ok = true;
        return result;
    }

    if (ok != nullptr)
        *ok = false;
    return 0;
}


/**
 * Retourne un pointeur sur un tableau de char constant avec les caractères encodés en Latin-1.
 * Le pointeur retourné est propre à chaque objet CString. Il est valable tant que l'objet existe,
 * et tant qu'un appel aux méthodes toLatin1, toLatin9, ou toUTF8 n'a pas eu lieu.
 *
 * \return Tableau de caractères.
 ******************************/

const char * CString::toLatin1() const
{
    // Longueur de la chaine
    const std::size_t s = getSize() + 1;

    if (m_data->m_str)
    {
        const std::size_t s_old = m_data->m_str_alloc - m_data->m_str;

        // On doit réallouer
        if (s_old < s)
        {
            delete[] m_data->m_str;

            m_data->m_str       = new char[s];
            m_data->m_str_alloc = m_data->m_str + s;
        }
    }
    else
    {
        m_data->m_str = new char[s];
    }

    char * ptr = m_data->m_str;

    // Copie des caractères
    for (CChar * str_ptr = m_data->m_start ; str_ptr <= m_data->m_end; ++str_ptr)
    {
        char ch = str_ptr->toLatin1();
        *ptr++ = (ch == '\0' && !str_ptr->isNull() ? '?' : ch);
    }

    return m_data->m_str;
}


/**
 * Retourne un pointeur sur un tableau de char constant avec les caractères encodés en Latin-9.
 * Le pointeur retourné est propre à chaque objet CString. Il est valable tant que l'objet existe,
 * et tant qu'un appel aux méthodes toLatin1, toLatin9, ou toUTF8 n'a pas eu lieu.
 *
 * \return Tableau de caractères.
 ******************************/

const char * CString::toLatin9() const
{
    // Longueur de la chaine
    const std::size_t s = getSize() + 1;

    if (m_data->m_str)
    {
        const std::size_t s_old = m_data->m_str_alloc - m_data->m_str;

        // On doit réallouer
        if (s_old < s)
        {
            delete[] m_data->m_str;

            m_data->m_str       = new char[s];
            m_data->m_str_alloc = m_data->m_str + s;
        }
    }
    else
    {
        m_data->m_str = new char[s];
    }

    char * ptr = m_data->m_str;

    // Copie des caractères
    for (CChar * str_ptr = m_data->m_start; str_ptr <= m_data->m_end; ++str_ptr)
    {
        char ch = str_ptr->toLatin9();
        *ptr++ = (ch == '\0' && !str_ptr->isNull() ? '?' : ch);
    }

    return m_data->m_str;
}


/**
 * Retourne un pointeur sur un tableau de char constant avec les caractères encodés en UTF-8.
 * Le pointeur retourné est propre à chaque objet CString. Il est valable tant que l'objet existe,
 * et tant qu'un appel aux méthodes toLatin1, toLatin9, ou toUTF8 n'a pas eu lieu.
 *
 * \todo Gérer les caractères codés sur 4 octets.
 *
 * \return Tableau de caractères.
 ******************************/

const char * CString::toUTF8() const
{
    // Nombre d'octets à allouer
    std::size_t s = 1;

    for (CChar * str_ptr = m_data->m_start ; str_ptr < m_data->m_end; ++str_ptr)
    {
        uint16_t u = str_ptr->unicode();

        if (u < 0x007F)
        {
            s += 1;
        }
        else if (u < 0x07FF)
        {
            s += 2;
        }
        else
        {
            s += 3;
        }
    }

    // Allocation de la mémoire pour la tableau de caractère
    if (m_data->m_str)
    {
        const std::size_t s_old = m_data->m_str_alloc - m_data->m_str;

        // On doit réallouer
        if (s_old < s)
        {
            delete[] m_data->m_str;
            m_data->m_str       = new char[s];
            m_data->m_str_alloc = m_data->m_str + s;
        }
    }
    else
    {
        m_data->m_str = new char[s];
    }

    char * ptr = m_data->m_str;

    // Copie des caractères
    for (CChar * str_ptr = m_data->m_start ; str_ptr < m_data->m_end; ++str_ptr)
    {
        uint16_t u = str_ptr->unicode();

        if (u < 0x007F)
        {
            *(ptr++) = u;
        }
        else if (u < 0x07FF)
        {
            *(ptr++) = (0xC0 | ((u & 0x07C0) >>  6));
            *(ptr++) = (0x80 | ((u & 0x003F) >>  0));
        }
        else
        {
            *(ptr++) = (0xE0 | ((u & 0xF000) >> 12));
            *(ptr++) = (0x80 | ((u & 0x0FC0) >>  6));
            *(ptr++) = (0x80 | ((u & 0x003F) >>  0));
        }
    }

    *ptr = '\0';

    return m_data->m_str;
}


/**
 * Opérateur de comparaison != avec un tableau de caractères.
 *
 * \param other Tableau de caractère à comparer.
 * \return Booléen indiquant si les deux chaines sont différentes.
 ******************************/

bool CString::operator!=(const char * other) const
{
    return operator!=(CString(other));
}


/**
 * Opérateur de comparaison <.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est inférieure à \a other.
 ******************************/

bool CString::operator<(const CString& other) const
{
    return (compare(other) < 0);
}


/**
 * Opérateur de comparaison < avec un tableau de caractères.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est inférieure à \a other.
 ******************************/

bool CString::operator<(const char * other) const
{
    return operator<(CString(other));
}


/**
 * Opérateur de comparaison <= avec un tableau de caractères.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est inférieure ou égale à \a other.
 ******************************/

bool CString::operator<=(const char * other) const
{
    return operator<=(CString(other));
}


/**
 * Opérateur de comparaison ==.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si les deux chaines sont identiques.
 ******************************/

bool CString::operator==(const CString& other) const
{
    // Remarque : les longueurs sont comparées deux fois
    return (getSize() == other.getSize() && compare(other) == 0);
}


/**
 * Opérateur de comparaison == avec un tableau de caractères.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est identique à \a other.
 ******************************/

bool CString::operator==(const char * other) const
{
    return operator==(CString(other));
}


/**
 * Opérateur de comparaison > avec un tableau de caractères.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est supérieure à \a other.
 ******************************/

bool CString::operator>(const char * other) const
{
    return operator>(CString(other));
}


/**
 * Opérateur de comparaison >= avec un tableau de caractères.
 * La comparaison se fait sur le code de chaque caractère.
 *
 * \param other Chaine à comparer.
 * \return Booléen indiquant si la chaine est supérieure ou égale à \a other.
 ******************************/

bool CString::operator>=(const char * other) const
{
    return operator>=(CString(other));
}


/**
 * Ajoute une chaine à la fin de celle-ci.
 *
 * \param other Chaine à ajouter.
 * \return Référence sur la chaine créée.
 ******************************/

CString& CString::operator+=(const CString& other)
{
    const std::size_t len1 = getSize();

    if (len1 == 0)
        return operator=(other);

    const std::size_t len2 = other.getSize();

    if (len2 == 0)
        return *this;

    const std::size_t len = len1 + len2;

    // Il y a assez de mémoire allouée
    if (len <= getCapacity())
    {
        if (m_data->m_count > 1)
        {
            TData * data = new TData;
            data->m_count     = 1;
            data->m_str       = nullptr;
            data->m_str_alloc = nullptr;

            // Copie de la chaine
            allocateArray(data, len);

            std::copy(m_data->m_start, m_data->m_start + len1, data->m_start);
            std::copy(other.m_data->m_start, other.m_data->m_end, data->m_start + len1);
            *(data->m_end) = CChar();

            // Suppression des anciennes données
            if (--(m_data->m_count) == 0)
            {
                delete m_data;
            }

            m_data = data;
        }
        else
        {
            std::copy(other.m_data->m_start, other.m_data->m_end, m_data->m_start + len1);
            m_data->m_end = m_data->m_start + len;
            *(m_data->m_end) = CChar();
        }
    }
    // Il faut obligatoirement réallouer
    else
    {
        TData * data = new TData;
        data->m_count     = 1;
        data->m_str       = nullptr;
        data->m_str_alloc = nullptr;

        // Copie de la chaine
        allocateArray(data, len);

        std::copy(m_data->m_start, m_data->m_end, data->m_start);
        std::copy(other.m_data->m_start, other.m_data->m_end, data->m_start + len1);
        *(data->m_end) = CChar();

        // Suppression des anciennes données
        if (--(m_data->m_count) == 0)
        {
            delete m_data;
        }

        m_data = data;
    }

    return *this;
}


/**
 * Ajoute un caractère à la fin de la chaine
 *
 * \param ch Caractère à ajouter.
 * \return Référence sur la chaine créée.
 ******************************/

CString& CString::operator+=(CChar ch)
{
    const std::size_t len = getSize();

    if (len == 0)
        return operator=(ch);

    // Il y a assez de mémoire allouée
    if (len + 1 <= getCapacity())
    {
        if (m_data->m_count > 1)
        {
            TData * data = new TData;
            data->m_count     = 1;
            data->m_str       = nullptr;
            data->m_str_alloc = nullptr;

            // Copie de la chaine
            allocateArray(data, len+1);

            std::copy(m_data->m_start, m_data->m_end, data->m_start);
            *(data->m_start + len) = ch;
            *(data->m_end) = CChar();

            // Suppression des anciennes données
            if (--(m_data->m_count) == 0)
            {
                delete m_data;
            }

            m_data = data;
        }
        else
        {
            *(m_data->m_start + len) = ch;
            ++(m_data->m_end);
            *(m_data->m_end) = CChar();
        }
    }
    // Il faut obligatoirement réallouer
    else
    {
        TData * data = new TData;
        data->m_count     = 1;
        data->m_str       = nullptr;
        data->m_str_alloc = nullptr;

        // Copie de la chaine
        allocateArray(data, len+1);

        std::copy(m_data->m_start, m_data->m_end, data->m_start);
        *(data->m_start + len) = ch;
        *(data->m_end) = CChar();

        // Suppression des anciennes données
        if (--(m_data->m_count) == 0)
        {
            delete m_data;
        }

        m_data = data;
    }

    return *this;
}


/**
 * Crée une chaine à partir d'un tableau de char encodés en Latin-1.
 * Équivaut à appeler le constructeur CString(const char * other).
 *
 * \param str Tableau de caractères.
 * \return Chaine de caractères.
 *
 * \sa CString::fromLatin9
 * \sa CString::fromUTF8
 ******************************/

CString CString::fromLatin1(const char * str, int len)
{
    return CString(fromLatin1Helper(str, len));
}


/**
 * Crée une chaine à partir d'un tableau de char encodés en Latin-9.
 *
 * \param str Tableau de caractères.
 * \return Chaine de caractères.
 *
 * \sa CString::fromLatin1
 * \sa CString::fromUTF8
 ******************************/

CString CString::fromLatin9(const char * str, int len)
{
    return CString(fromLatin9Helper(str, len));
}


/**
 * Crée une chaine à partir d'un tableau de char encodés en UTF-8.
 *
 * \todo Gérer les caractères qui occupent plus de 3 octets (donc 2 caractères UTF-16).
 *
 * \param str Tableau de caractères.
 * \return Chaine de caractères.
 *
 * \sa CString::fromLatin1
 * \sa CString::fromLatin9
 ******************************/

CString CString::fromUTF8(const char * str, int len)
{
    return CString(fromUTF8Helper(str, len));
}


/**
 * Convertit un nombre en chaine de caractères, en précisant le nombre de décimales.
 *
 * \param number   Nombre à convertir.
 * \param decimals Nombre de décimales.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromFloat(float number, unsigned int decimals)
{
    std::ostringstream oss;
    oss << std::setprecision(decimals) << std::fixed << number;
    return CString(oss.str().c_str());
}


/**
 * Convertit un nombre en chaine de caractères, en précisant le nombre de décimales.
 *
 * \param number   Nombre à convertir.
 * \param decimals Nombre de décimales.
 * \return Chaine de caractères.
 ******************************/

CString CString::fromFloat(double number, unsigned int decimals)
{
    std::ostringstream oss;
    oss << std::setprecision(decimals) << std::fixed << number;
    return CString(oss.str().c_str());
}


/**
 * Construit une nouvelle chaine avec un objet TData déjà initialisé.
 * Aucune vérification n'est faite sur l'objet TData et son contenu.
 *
 * Ce constructeur ne peut être appelé que dans des méthodes internes à la classe,
 * en particulier les méthodes qui retournent un nouvel objet CString.
 *
 * \param data Pointeur sur un objet TData.
 ******************************/

CString::CString(TData * data) :
	m_data{ data }
{
    assert(data != nullptr);
}


/**
 * Convertit un caractère en un chiffre pour une base donnée.
 * Pour les bases supérieures à 10, les lettres minuscules et majuscules sont utilisées.
 *
 * \param c    Caractère ASCII à convertir.
 * \param base Base à utiliser, entre 2 et 36.
 * \return Chiffre correspondant, ou -1 si le caractère ne correspond pas à la base.
 ******************************/

int convertCharToInt(char c, int base); // Prototype
int convertCharToInt(char c, int base)
{
    //T_ASSERT(base >= 2 && base <= 36);

    int v = -1;

         if (c >= '0' && c <= '9') v = c - '0';
    else if (c >= 'a' && c <= 'z') v = c - 'a' + 10;
    else if (c >= 'A' && c <= 'Z') v = c - 'A' + 10;

    return (v > base ? -1 : v);
}


/**
 * Convertit une chaine de caractères en entier de 64 bits.
 *
 * \param base Base à utiliser, entre 2 et 36.
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \return Résultat.
 ******************************/

int64_t CString::convertToInt64(int base, bool * ok) const
{
    //assert(base >= 2 && base <= 36);
    //assert(ok != nullptr);

    CChar * ptr_end = m_data->m_end - 1;

    // On ignore les caractères d'espacement finaux
    for ( ; ptr_end >= m_data->m_start; --ptr_end)
    {
        if (!ptr_end->isSpace())
        {
            break;
        }
    }

    int64_t result = 0;
    bool neg = false;
    bool begin_str = true;

    for (CChar * str_ptr = m_data->m_start; str_ptr <= ptr_end; ++str_ptr)
    {
        if (begin_str)
        {
            // Espacement
            if (str_ptr->isSpace())
                continue;

            begin_str = false;
            char c = str_ptr->toLatin1();

            // Signe moins
            if (c == '-')
            {
                neg = true;
                continue;
            }

            int val = convertCharToInt(c, base);

            // Le caractère est incorrect
            if (val < 0)
            {
                *ok = false;
                return 0;
            }

            result *= base;
            result += val;
        }
        else
        {
            int val = convertCharToInt(str_ptr->toLatin1(), base);

            // Le caractère est incorrect
            if (val < 0)
            {
                *ok = false;
                return 0;
            }

            result *= base;
            result += val;
        }
    }

    if (neg)
        result = -result;

    *ok = true;
    return result;
}


/**
 * Convertit une chaine de caractères en entier non signé de 64 bits.
 *
 * \param base Base à utiliser, entre 2 et 36.
 * \param ok   Pointeur sur un booléen qui contiendra le résultat de la conversion.
 * \return Résultat.
 ******************************/

uint64_t CString::convertToUnsignedInt64(int base, bool * ok) const
{
    //assert(base >= 2 && base <= 36);
    //assert(ok != nullptr);

    CChar * ptr_end = m_data->m_end - 1;

    // On ignore les caractères d'espacement finaux
    for ( ; ptr_end >= m_data->m_start; --ptr_end)
    {
        if (!ptr_end->isSpace())
        {
            break;
        }
    }

    uint64_t result = 0;
    bool begin_str = true;

    for (CChar * str_ptr = m_data->m_start; str_ptr <= ptr_end; ++str_ptr)
    {
        if (begin_str)
        {
            // Espacement
            if (str_ptr->isSpace())
                continue;

            begin_str = false;

            int val = convertCharToInt(str_ptr->toLatin1(), base);

            // Le caractère est incorrect
            if (val < 0)
            {
                *ok = false;
                return 0;
            }

            result *= base;
            result += val;
        }
        else
        {
            int val = convertCharToInt(str_ptr->toLatin1(), base);

            // Le caractère est incorrect
            if (val < 0)
            {
                *ok = false;
                return 0;
            }

            result *= base;
            result += val;
        }
    }

    *ok = true;
    return result;
}


/**
 * Effectue une copie en mémoire des données si nécessaire.
 ******************************/

void CString::copyData()
{
    if (m_data->m_count > 1)
    {
        // Longueur de la chaine
        const std::size_t s = getSize();

        TData * data;

        if (s == 0)
        {
            data = &sharedEmpty;
            ++(data->m_count);
        }
        else
        {
            data = new TData;
            data->m_count     = 1;
            data->m_str       = nullptr;
            data->m_str_alloc = nullptr;

            // Copie de la chaine
            allocateArray(data, s);

            std::copy(m_data->m_start, m_data->m_end, data->m_start);
            *(data->m_end) = CChar();
        }

        // Suppression des anciennes données
        // Ce test est effectué ici parce que les autres chaines qui utilisent ces
        // données pourraient décrémenter de leur côté le compteur de référence
        // pendant que la copie est effectuée.
        if (--(m_data->m_count) == 0)
        {
            delete m_data;
        }

        m_data = data;
    }
}


/**
 * Alloue la mémoire pour stocker un tableau de caractère.
 *
 * La taille de la zone mémoire réservée est déterminée pour minimiser le nombre de
 * réallocations. Elle peut contenir plus que s caractères, en fonction de la valeur de s :
 *
 * En-dessous de 20, on réserve de la place pour un multiple de 4 caractères (4, 8, 12, 16, 20).
 * Entre 21 et 1024, on réserve de la place pour un multiple de 32 caractères.
 * Entre 1025 et 4096, on réserve de la place pour un multiple de 256 caractères.
 * Au-dessus de 4097, on réserve de la place pour un multiple de 2048 caractères.
 *
 * L'idée est de toujours allouer un multiple de 8 octets (4 caractères) pour bénéficier de
 * l'alignement en mémoire, et d'augmenter progressivement la taille de la zone mémoire.
 * Pour les très grandes chaines (plus de 4096 caractères), on réserve des pages entières
 * (4096 octets soit 2048 caractères).
 *
 * \param s Nombre de caractères à réserver.
 */

void CString::allocateArray(TData * data, std::size_t s)
{
    std::size_t a = s + 1;

    if (a <= 20)
    {
        if ((a % 4) != 0)
            a += 4 - (a % 4);
    }
    else if (a <= 1024)
    {
        if ((a % 32) != 0)
            a += 32 - (a % 32);
    }
    else if (a <= 4096)
    {
        if ((a % 256) != 0)
            a += 256 - (a % 256);
    }
    else
    {
        if ((a % 2048) != 0)
            a += 2048 - (a % 2048);
    }

    data->m_start = new CChar[a];
    data->m_end   = data->m_start + s;
    data->m_alloc = data->m_start + a - 1;
}


/**
 * Convertit un tableau de caractères encodés en Latin-1.
 *
 * \param str Tableau de caractères.
 * \return Pointeur sur un objet TData.
 ******************************/

CString::TData * CString::fromLatin1Helper(const char * str, int len)
{
    const std::size_t s = (str != nullptr ? (len < 0 ? strlen(str) : len) : 0);

    if (s == 0)
    {
        ++(sharedEmpty.m_count);
        return &sharedEmpty;
    }

    TData * data = new TData;
    data->m_count     = 1;
    data->m_str       = nullptr;
    data->m_str_alloc = nullptr;

    allocateArray(data, s);

    CChar * ptr = data->m_start;

    if (s > 0)
    {
        // Copie de la chaine caractère par caractère
        while (*str)
        {
            *ptr++ = CChar::fromLatin1(*str++);
        }
    }

    *ptr = CChar();

    return data;
}


/**
 * Convertit un tableau de caractères encodés en Latin-9.
 *
 * \param str Tableau de caractères.
 * \return Pointeur sur un objet TData.
 ******************************/

CString::TData * CString::fromLatin9Helper(const char * str, int len)
{
    const std::size_t s = (str != nullptr ? (len < 0 ? strlen(str) : len) : 0);

    if (s == 0)
    {
        ++(sharedEmpty.m_count);
        return &sharedEmpty;
    }

    TData * data = new TData;
    data->m_count     = 1;
    data->m_str       = nullptr;
    data->m_str_alloc = nullptr;

    allocateArray(data, s);

    CChar * ptr = data->m_start;

    if (s > 0)
    {
        // Copie de la chaine caractère par caractère
        while (*str)
        {
            *ptr++ = CChar::fromLatin9(*str++);
        }
    }

    *ptr = CChar();

    return data;
}


/**
 * Convertit un tableau de caractères encodés en UTF8.
 *
 * \param str Tableau de caractères.
 * \return Pointeur sur un objet TData.
 ******************************/

CString::TData * CString::fromUTF8Helper(const char * str, int len)
{
    const std::size_t s = (str != nullptr ? (len < 0 ? strlen(str) : len) : 0);

    if (s == 0)
    {
        ++(sharedEmpty.m_count);
        return &sharedEmpty;
    }

    TData * data = new TData;
    data->m_count     = 1;
    data->m_str       = nullptr;
    data->m_str_alloc = nullptr;

    allocateArray(data, s);

    CChar * ptr = data->m_start;

    if (s > 0)
    {
        int code = 0;
        int current = 1;
        int type = 1; // 1, 2, 3 ou 4

        while (*str)
        {
            if (type == 1)
            {
                // Caractère ASCII
                if ((*str & 0x80) == 0x00)
                {
                    *(ptr++) = CChar(*str);
                }
                // Deux octets
                else if ((*str & 0xE0) == 0xC0)
                {
                    current = 2;
                    type = 2;

                    code = (*str & 0x1F);
                }
                // Trois octets
                else if ((*str & 0xF0) == 0xE0)
                {
                    current = 2;
                    type = 3;

                    code = (*str & 0x0F);
                }
                // Quatre octets
                else if ((*str & 0xF8) == 0xF0)
                {
                    current = 2;
                    type = 4;

                    code = (*str & 0x07);
                }
                // Caractère incorrect
                else
                {
                    //...
                }
            }
            else
            {
                if ((*str & 0xC0) == 0x80)
                {
                    code <<= 6;
                    code |= (*str & 0x3F);

                    if (type == current)
                    {
                        *(ptr++) = CChar(code);
                        type = current = 1;
                    }
                }
                // Caractère incorrect
                else
                {
                    current = 0;
                    type = 1;
                }

                ++current;
            }

            ++str;
        }

        data->m_end = ptr;
    }

    *ptr = CChar();

    return data;
}

} // Namespace TE
