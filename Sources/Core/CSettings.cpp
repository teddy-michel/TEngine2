/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <cstring>

#include "Core/CSettings.hpp"
#include "Core/ILogger.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur par défaut.
 ******************************/

CSettings::CSettings() :
	m_fileName{}
{ }


/**
 * Destructeur.
 ******************************/

CSettings::~CSettings()
{
    clear();
}


/**
 * Indique si un groupe existe.
 *
 * \param group Nom du groupe.
 * \return Booléen.
 ******************************/

bool CSettings::groupExists(const CString& group) const
{
    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        if ((*it)->name == group)
        {
            return true;
        }
    }

    return false;
}


/**
 * Indique si une clé existe dans un groupe.
 *
 * \param group Nom du groupe.
 * \param key   Nom de la clé.
 * \return Booléen.
 ******************************/

bool CSettings::keyExists(const CString& group, const CString& key) const
{
    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        if ((*it)->name == group)
        {
            // On parcourt la liste des clés du groupe
            for (std::map<CString, CString>::const_iterator it2 = (*it)->data.begin(); it2 != (*it)->data.end(); ++it2)
            {
                if (it2->first == key)
                {
                    return true;
                }
            }
        }
    }

    return false;
}


/**
 * Donne la valeur d'une clé.
 *
 * \param group  Nom du groupe.
 * \param key    Nom de la clé.
 * \param defval Valeur par défaut.
 * \return Valeur de la clé si elle existe, la valeur par défaut sinon.
 ******************************/

CString CSettings::getValue(const CString& group, const CString& key, const CString& defval) const
{
    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        if ((*it)->name == group)
        {
            // On parcourt la liste des clés du groupe
            for (std::map<CString, CString>::const_iterator it2 = (*it)->data.begin(); it2 != (*it)->data.end(); ++it2)
            {
                if (it2->first == key)
                {
                    return it2->second;
                }
            }
        }
    }

    return defval;
}


/**
 * Donne la valeur de type booléenne d'une clé.
 * Les valeurs "0" et "false" sont considérée comme fausse, toutes les
 * autres comme vraies.
 *
 * \param group  Nom du groupe.
 * \param key    Nom de la clé.
 * \param defval Valeur par défaut (true par défaut).
 * \return Valeur de la clé si elle existe, la valeur par défaut sinon.
 ******************************/

bool CSettings::getBooleanValue(const CString& group, const CString& key, const bool defval) const
{
    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        if ((*it)->name == group)
        {
            // On parcourt la liste des clés du groupe
            for (std::map<CString, CString>::const_iterator it2 = (*it)->data.begin(); it2 != (*it)->data.end(); ++it2)
            {
                if (it2->first == key)
                {
                    return !(it2->second == "false" || it2->second == "0");
                }
            }
        }
    }

    return defval;
}


/**
 * Charge les données depuis un fichier.
 *
 * \param fileName Adresse du fichier de configuration.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CSettings::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::binary);

    if (file)
    {
        gApplication->log(CString("Lecture du fichier %1").arg(fileName));
        m_fileName = fileName;
    }
    else
    {
		gApplication->log(CString("Impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    // Suppression des anciennes données
    for (std::list<TSettingGroup *>::iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        delete *it;
    }

    m_groups.clear();


    TSettingGroup * grp = nullptr;
    std::string line;

    // On parcourt le fichier ligne par ligne
    while (std::getline(file, line))
    {
        std::size_t taille = line.size() + 1;

        if (taille <= 2)
            continue;

        char * line_str = new char[taille];
        char * line_str_start = line_str;
        strcpy(line_str, line.c_str());

        char * lec_grp = nullptr;
        char * lec_key = nullptr;
        char * lec_val = nullptr;

        // On parcourt la ligne caractère par caractère
        while (*line_str)
        {
            // Lecture de la valeur
            if (lec_val)
            {
                // Fin de la ligne
                if (*(line_str + 1) == 0 || *(line_str + 1) == '\n' || *(line_str + 1) == '\r')
                {
                    *(line_str + 1) = 0;
                    grp->data[lec_key] = lec_val;
                    break; // Ligne suivante
                }
            }
            // Lecture du nom de la clé
            else if (lec_key)
            {
                // Fin du nom de la clé
                if (*line_str == '=')
                {
                    *line_str = 0;
                    grp->data[lec_key] = "";

                    do
                    {
                        ++line_str;
                    } while (*line_str == ' ' || *line_str == '\t');

                    lec_val = line_str;
                    continue;
                }
            }
            // Lecture du nom du groupe
            else if (lec_grp)
            {
                // Fin du groupe
                if (*line_str == ']')
                {
                    *line_str = 0;
                    grp = new TSettingGroup(lec_grp);
                    m_groups.push_back(grp);
                    break; // Ligne suivante
                }
                // Fin de la ligne
                else if (*(line_str + 1) == 0 || *(line_str + 1) == '\n' || *(line_str + 1) == '\r')
                {
                    grp = new TSettingGroup(lec_grp);
                    m_groups.push_back(grp);
                }
            }
            else
            {
                // Début d'un commentaire ou caractère invalide
                if (*line_str == ';' || *line_str == '=')
                {
                    break; // Ligne suivante
                }
                // Espace
                else if (*line_str == ' ' || *line_str == '\t' || *line_str == '\n' || *line_str == '\r')
                {
                    ++line_str;
                    continue; // Caractère suivant
                }
                // Début d'un groupe
                else if (*line_str == '[')
                {
                    do
                    {
                        ++line_str;
                    } while (*line_str == ' ' || *line_str == '\t');

                    lec_grp = line_str;
                }
                // Début d'une clé
                else
                {
                    if (grp == nullptr)
                    {
                        // On parcourt la liste des groupes
                        for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
                        {
                            if ((*it)->name.isEmpty())
                            {
                                grp = *it;
                            }
                        }

                        // Création du groupe par défaut
                        if (grp == nullptr)
                        {
                            grp = new TSettingGroup(CString());
                            m_groups.push_back(grp);
                        }
                    }

                    while (*line_str == ' ' || *line_str == '\t')
                    {
                        ++line_str;
                    }

                    lec_key = line_str;
                }
            }

            ++line_str; // Caractère suivant
        }

        delete[] line_str_start;
    }

    return true;
}


/**
 * Écrit les données dans un fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CSettings::saveToFile(const CString& fileName) const
{
    std::ofstream file(fileName.toCharArray(), std::ios::binary);

    if (!file)
    {
		gApplication->log(CString::fromUTF8("Impossible d'ouvrir le fichier %1 en écriture").arg(m_fileName), ILogger::Error);
        return false;
    }

    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        file << std::endl << "[" << (*it)->name << "]" << std::endl;

        // On parcourt la liste des clés du groupe
        for (std::map<CString, CString>::const_iterator it2 = (*it)->data.begin(); it2 != (*it)->data.end(); ++it2)
        {
            file << it2->first << "=" << it2->second << std::endl;
        }
    }

    file.close();
	gApplication->log(CString::fromUTF8("Sauvegarde des paramètres dans le fichier %1").arg(m_fileName));

    return true;
}


/**
 * Efface les données du fichier de configuration.
 ******************************/

void CSettings::clear()
{
    for (std::list<TSettingGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it)
    {
        delete (*it);
    }

    m_groups.clear();
}


/**
 * Ajoute un groupe à la liste s'il n'existe pas.
 *
 * \param group Nom du groupe.
 ******************************/

void CSettings::addGroup(const CString& group)
{
    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it)
    {
        if ((*it)->name == group)
        {
            return;
        }
    }

    // Le groupe n'existe pas
    TSettingGroup * grp = new TSettingGroup(group);
    m_groups.push_back(grp);
}


/**
 * Modifie la valeur d'une clé. Si le groupe ou la clé n'existe pas, ils sont crées.
 *
 * \param group Nom du groupe.
 * \param key   Nom de la clé.
 * \param value Valeur de la clé.
 ******************************/

void CSettings::setValue(const CString& group, const CString& key, const CString& value)
{
    // On parcourt la liste des groupes
    for (std::list<TSettingGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it)
    {
        if ((*it)->name == group)
        {
            (*it)->data[key] = value;
            return;
        }
    }

    // Le groupe n'existe pas
    TSettingGroup * grp = new TSettingGroup(group);
    grp->data[key] = value;
    m_groups.push_back(grp);
}

} // Namespace TE
