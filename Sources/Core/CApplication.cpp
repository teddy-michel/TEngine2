/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>

#include "Core/CApplication.hpp"
#include "Core/CLoggerFile.hpp"
#include "Core/CDateTime.hpp"


namespace TE
{

CApplication * gApplication = nullptr;


int getElapsedTime()
{
    static auto start = GetTickCount();
    return (GetTickCount() - start);
}


/**
 * Constructeur.
 ******************************/

CApplication::CApplication() :
    m_running   { true },
    m_logger    { nullptr },
    m_fps       { 0.0f },
    m_frameTime { 0.0f }
{
    assert(gApplication == nullptr); // Singleton simple
    gApplication = this;
	m_logger = std::make_unique<CLoggerFile>(CDateTime::getCurrentDateTime().toString("logs/log-%Y-%m-%d-%H-%i-%s.txt"));
    log("CApplication::CApplication", ILogger::Debug);
}


/**
 * Destructeur.
 ******************************/

CApplication::~CApplication()
{
    log("CApplication::~CApplication", ILogger::Debug);
    gApplication = nullptr;
}
    

unsigned int CApplication::getWidth() const
{
    return 0;
}


unsigned int CApplication::getHeight() const
{
    return 0;
}


/**
 * Termine l'application.
 ******************************/

void CApplication::close()
{
    log("CApplication::close", ILogger::Info);
    m_running = false;
}


/**
 * Initialise les donn�es.
 ******************************/

bool CApplication::init()
{
    log("CApplication::init - Chargement du moteur", ILogger::Info);

    // Lecture des param�tres
    m_settings.loadFromFile("../../../config.ini");

    // Initialisation de l'�tat des boutons de la souris
    m_buttons[MouseButtonLeft] = false;
    m_buttons[MouseButtonRight] = false;
    m_buttons[MouseButtonMiddle] = false;
    m_buttons[MouseWheelUp] = false;
    m_buttons[MouseWheelDown] = false;

    return true;
}


/**
 * Boucle principale de l'application.
 ******************************/

void CApplication::mainLoop()
{
    log("CApplication::mainLoop", ILogger::Info);

    auto time = getElapsedTime();
    auto timeLastUpdateFPS = time;
    unsigned int frameCountLastSecond = 0;

    log("Begin loop...", ILogger::Debug);
	while (m_running)
	{
        auto newTime = getElapsedTime();
        m_frameTime = static_cast<float>(newTime - time) / 1000.0f;
        time = newTime;

        if (newTime - timeLastUpdateFPS >= 1000)
        {
            m_fps = 1000.0f * frameCountLastSecond / (newTime - timeLastUpdateFPS);
            timeLastUpdateFPS = newTime;
            frameCountLastSecond = 0;
        }

        frame();

        ++frameCountLastSecond;
	}
}


/**
 * M�thode appell�e � chaque frame.
 ******************************/

void CApplication::frame()
{
	
}


/**
 * Lib�re les ressources de l'application.
 ******************************/

void CApplication::release()
{
	log("CApplication::release", ILogger::Debug);
	m_running = false;
}


/**
 * Log un message dans un fichier.
 ******************************/

void CApplication::log(const CString& message, ILogger::TLevel level)
{
    assert(m_logger != nullptr);
	m_logger->write(message, level);
}


/**
 * Gestion des �v�nements de la souris.
 *
 * \param event �v�nement.
 ******************************/

void CApplication::onEvent(const CMouseEvent& event)
{
	// Modification de l'�tat du bouton
	if (event.type == ButtonPressed)
	{
		m_buttons[event.button] = true;

		switch (event.button)
		{
			case MouseButtonLeft:
				m_keyStates[TKey::Mouse_Left] = true;
				break;
			case MouseButtonRight:
				m_keyStates[TKey::Mouse_Right] = true;
				break;
			case MouseButtonMiddle:
				m_keyStates[TKey::Mouse_Middle] = true;
				break;
		}
	}
	else if (event.type == ButtonReleased)
	{
		m_buttons[event.button] = false;

		switch (event.button)
		{
			case MouseButtonLeft:
				m_keyStates[TKey::Mouse_Left] = false;
				break;
			case MouseButtonRight:
				m_keyStates[TKey::Mouse_Right] = false;
				break;
			case MouseButtonMiddle:
				m_keyStates[TKey::Mouse_Middle] = false;
				break;
		}
	}
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event �v�nement.
 ******************************/

void CApplication::onEvent(const CTextEvent& event)
{
    T_UNUSED(event);
}


/**
 * Gestion des �v�nements du clavier.
 *
 * \param event �v�nement.
 ******************************/

void CApplication::onEvent(const CKeyboardEvent& event)
{
	// Modification de l'�tat de la touche
	m_keyStates[event.code] = (event.type == KeyPressed);

}


void CApplication::onEvent(const CResizeEvent& event)
{

}


/**
 * Donne l'�tat d'une touche du clavier.
 *
 * \param key Identifiant de la touche.
 * \return Bool�en.
 ******************************/

bool CApplication::getKeyState(TKey key) const
{
	std::map<TKey, bool>::const_iterator it = m_keyStates.find(key);
	return (it != m_keyStates.end() && it->second);
}


/**
 * Donne l'�tat d'un bouton de la souris.
 *
 * \param button Code du bouton de la souris.
 * \return Bool�en.
 ******************************/

bool CApplication::getButtonState(TMouseButton button) const
{
	std::map<TMouseButton, bool>::const_iterator it = m_buttons.find(button);
	return (it != m_buttons.end() && it->second);
}


/**
 * Donne le code correspondant � un nom de touche.
 *
 * \todo Trouver un moyen plus �l�gant de faire cela.
 *
 * \param keycode Nom de la touche (Key_??? ou Mouse_???).
 * \return Code de la touche (d�fini dans l'�num�ration TKey).
 ******************************/

TKey CApplication::getKeyCode(const CString& keycode)
{
	if (keycode.startsWith("Mouse_"))
	{
		CString str = keycode.subString(6);

		if (str == "Left")   return TKey::Mouse_Left;
		if (str == "Right")  return TKey::Mouse_Right;
		if (str == "Middle") return TKey::Mouse_Middle;

		return TKey::Key_Unknown;
	}

	// �a commence mal...
	if (!keycode.startsWith("Key_"))
	{
		return TKey::Key_Unknown;
	}

	CString str = keycode.subString(4);

	// Chiffres
	if (str == "0") return TKey::Key_0;
	if (str == "1") return TKey::Key_1;
	if (str == "2") return TKey::Key_2;
	if (str == "3") return TKey::Key_3;
	if (str == "4") return TKey::Key_4;
	if (str == "5") return TKey::Key_5;
	if (str == "6") return TKey::Key_6;
	if (str == "7") return TKey::Key_7;
	if (str == "8") return TKey::Key_8;
	if (str == "9") return TKey::Key_9;

	// Chiffres (pav� num�rique)
	if (str == "Num0") return TKey::Key_Num0;
	if (str == "Num1") return TKey::Key_Num1;
	if (str == "Num2") return TKey::Key_Num2;
	if (str == "Num3") return TKey::Key_Num3;
	if (str == "Num4") return TKey::Key_Num4;
	if (str == "Num5") return TKey::Key_Num5;
	if (str == "Num6") return TKey::Key_Num6;
	if (str == "Num7") return TKey::Key_Num7;
	if (str == "Num8") return TKey::Key_Num8;
	if (str == "Num9") return TKey::Key_Num9;

	// Lettres
	if (str == "A") return TKey::Key_A;
	if (str == "B") return TKey::Key_B;
	if (str == "C") return TKey::Key_C;
	if (str == "D") return TKey::Key_D;
	if (str == "E") return TKey::Key_E;
	if (str == "F") return TKey::Key_F;
	if (str == "G") return TKey::Key_G;
	if (str == "H") return TKey::Key_H;
	if (str == "I") return TKey::Key_I;
	if (str == "J") return TKey::Key_J;
	if (str == "K") return TKey::Key_K;
	if (str == "L") return TKey::Key_L;
	if (str == "M") return TKey::Key_M;
	if (str == "N") return TKey::Key_N;
	if (str == "O") return TKey::Key_O;
	if (str == "P") return TKey::Key_P;
	if (str == "Q") return TKey::Key_Q;
	if (str == "R") return TKey::Key_R;
	if (str == "S") return TKey::Key_S;
	if (str == "T") return TKey::Key_T;
	if (str == "U") return TKey::Key_U;
	if (str == "V") return TKey::Key_V;
	if (str == "W") return TKey::Key_W;
	if (str == "X") return TKey::Key_X;
	if (str == "Y") return TKey::Key_Y;
	if (str == "Z") return TKey::Key_Z;

	// Autres caract�res ASCII
	if (str == "Space") return TKey::Key_Space;
	if (str == "Exclam") return TKey::Key_Exclam;
	if (str == "Paragraph") return TKey::Key_Paragraph;
	if (str == "BracketRight") return TKey::Key_BracketRight;
	if (str == "ParenRight") return TKey::Key_ParenRight;
	if (str == "Degree") return TKey::Key_Degree;
	if (str == "Comma") return TKey::Key_Comma;
	if (str == "Question") return TKey::Key_Question;
	if (str == "Colon") return TKey::Key_Colon;
	if (str == "Slash") return TKey::Key_Slash;
	if (str == "Semicolon") return TKey::Key_Semicolon;
	if (str == "Less") return TKey::Key_Less;
	if (str == "Greater") return TKey::Key_Greater;
	if (str == "Equal") return TKey::Key_Equal;
	if (str == "Plus") return TKey::Key_Plus;
	if (str == "BraceRight") return TKey::Key_BraceRight;
	if (str == "Euro") return TKey::Key_Euro;
	if (str == "TwoSuperior") return TKey::Key_TwoSuperior;
	if (str == "At") return TKey::Key_At;
	if (str == "Ampersand") return TKey::Key_Ampersand;
	if (str == "Tilde") return TKey::Key_Tilde;
	if (str == "QuoteDbl") return TKey::Key_QuoteDbl;
	if (str == "Apostrophe") return TKey::Key_Apostrophe;
	if (str == "BraceLeft") return TKey::Key_BraceLeft;
	if (str == "ParenLeft") return TKey::Key_ParenLeft;
	if (str == "BracketLeft") return TKey::Key_BracketLeft;
	if (str == "Minus") return TKey::Key_Minus;
	if (str == "Bar") return TKey::Key_Bar;
	if (str == "Quote") return TKey::Key_Quote;
	if (str == "Underscore") return TKey::Key_Underscore;
	if (str == "Backslash") return TKey::Key_Backslash;
	if (str == "Dollar") return TKey::Key_Dollar;
	if (str == "Sterling") return TKey::Key_Sterling;
	if (str == "Circum") return TKey::Key_Circum;
	if (str == "Asterisk") return TKey::Key_Asterisk;
	if (str == "Mu") return TKey::Key_Mu;
	if (str == "Percent") return TKey::Key_Percent;
	if (str == "Add") return TKey::Key_Add;
	if (str == "Subtract") return TKey::Key_Subtract;
	if (str == "Divide") return TKey::Key_Divide;
	if (str == "Multiply") return TKey::Key_Multiply;

	// Touches sp�ciales
	if (str == "LAlt") return TKey::Key_LAlt;
	if (str == "RAlt") return TKey::Key_RAlt;
	if (str == "LControl") return TKey::Key_LControl;
	if (str == "RControl") return TKey::Key_RControl;
	if (str == "LShift") return TKey::Key_LShift;
	if (str == "RShift") return TKey::Key_RShift;
	if (str == "LMeta") return TKey::Key_LMeta;
	if (str == "RMeta") return TKey::Key_RMeta;
	if (str == "Menu") return TKey::Key_Menu;

	if (str == "Print") return TKey::Key_Print;
	if (str == "Pause") return TKey::Key_Pause;
	if (str == "Tab") return TKey::Key_Tab;
	if (str == "Backspace") return TKey::Key_Backspace;
	if (str == "Enter") return TKey::Key_Enter;
	if (str == "NumEnter") return TKey::Key_NumEnter;
	if (str == "Insert") return TKey::Key_Insert;
	if (str == "Delete") return TKey::Key_Delete;
	if (str == "Home") return TKey::Key_Home;
	if (str == "End") return TKey::Key_End;
	if (str == "PageUp") return TKey::Key_PageUp;
	if (str == "PageDown") return TKey::Key_PageDown;
	if (str == "Left") return TKey::Key_Left;
	if (str == "Up") return TKey::Key_Up;
	if (str == "Right") return TKey::Key_Right;
	if (str == "Down") return TKey::Key_Down;
	if (str == "CapsLock") return TKey::Key_CapsLock;
	if (str == "NumLock") return TKey::Key_NumLock;
	if (str == "ScrollLock") return TKey::Key_ScrollLock;
	if (str == "Function") return TKey::Key_Function;
	if (str == "Escape") return TKey::Key_Escape;

	// Fonctions
	if (str == "F1") return TKey::Key_F1;
	if (str == "F2") return TKey::Key_F2;
	if (str == "F3") return TKey::Key_F3;
	if (str == "F4") return TKey::Key_F4;
	if (str == "F5") return TKey::Key_F5;
	if (str == "F6") return TKey::Key_F6;
	if (str == "F7") return TKey::Key_F7;
	if (str == "F8") return TKey::Key_F8;
	if (str == "F9") return TKey::Key_F9;
	if (str == "F10") return TKey::Key_F10;
	if (str == "F11") return TKey::Key_F11;
	if (str == "F12") return TKey::Key_F12;
	if (str == "F13") return TKey::Key_F13;
	if (str == "F14") return TKey::Key_F14;
	if (str == "F15") return TKey::Key_F15;

	return TKey::Key_Unknown;
}


/**
 * Donne le code correspondant � un code de touche SFML.
 * \todo Impl�mentation.
 *
 * \param keycode Code de touche SFML.
 * \return Code de la touche (d�fini dans l'�num�ration TKey).
 ******************************/

TKey CApplication::getKeyCode(const sf::Keyboard::Key& keycode)
{
	switch (keycode)
	{
		default: return TKey::Key_Unknown;

		// Lettres
		case sf::Keyboard::A: return TKey::Key_A;
		case sf::Keyboard::B: return TKey::Key_B;
		case sf::Keyboard::C: return TKey::Key_C;
		case sf::Keyboard::D: return TKey::Key_D;
		case sf::Keyboard::E: return TKey::Key_E;
		case sf::Keyboard::F: return TKey::Key_F;
		case sf::Keyboard::G: return TKey::Key_G;
		case sf::Keyboard::H: return TKey::Key_H;
		case sf::Keyboard::I: return TKey::Key_I;
		case sf::Keyboard::J: return TKey::Key_J;
		case sf::Keyboard::K: return TKey::Key_K;
		case sf::Keyboard::L: return TKey::Key_L;
		case sf::Keyboard::M: return TKey::Key_M;
		case sf::Keyboard::N: return TKey::Key_N;
		case sf::Keyboard::O: return TKey::Key_O;
		case sf::Keyboard::P: return TKey::Key_P;
		case sf::Keyboard::Q: return TKey::Key_Q;
		case sf::Keyboard::R: return TKey::Key_R;
		case sf::Keyboard::S: return TKey::Key_S;
		case sf::Keyboard::T: return TKey::Key_T;
		case sf::Keyboard::U: return TKey::Key_U;
		case sf::Keyboard::V: return TKey::Key_V;
		case sf::Keyboard::W: return TKey::Key_W;
		case sf::Keyboard::X: return TKey::Key_X;
		case sf::Keyboard::Y: return TKey::Key_Y;
		case sf::Keyboard::Z: return TKey::Key_Z;

		// Chiffres
		case sf::Keyboard::Num0: return TKey::Key_0;
		case sf::Keyboard::Num1: return TKey::Key_1;
		case sf::Keyboard::Num2: return TKey::Key_2;
		case sf::Keyboard::Num3: return TKey::Key_3;
		case sf::Keyboard::Num4: return TKey::Key_4;
		case sf::Keyboard::Num5: return TKey::Key_5;
		case sf::Keyboard::Num6: return TKey::Key_6;
		case sf::Keyboard::Num7: return TKey::Key_7;
		case sf::Keyboard::Num8: return TKey::Key_8;
		case sf::Keyboard::Num9: return TKey::Key_9;

		// Touches sp�ciales
		case sf::Keyboard::Escape:   return TKey::Key_Escape;
		case sf::Keyboard::LControl: return TKey::Key_LControl;
		case sf::Keyboard::LShift:   return TKey::Key_LShift;
		case sf::Keyboard::LAlt:     return TKey::Key_LAlt;
		case sf::Keyboard::LSystem:  return TKey::Key_LMeta;
		case sf::Keyboard::RControl: return TKey::Key_RControl;
		case sf::Keyboard::RShift:   return TKey::Key_RShift;
		case sf::Keyboard::RAlt:     return TKey::Key_RAlt;
		case sf::Keyboard::RSystem:  return TKey::Key_RMeta;
		case sf::Keyboard::Menu:     return TKey::Key_Menu;

		case sf::Keyboard::LBracket:  return TKey::Key_BracketLeft;
		case sf::Keyboard::RBracket:  return TKey::Key_BracketRight;
		case sf::Keyboard::SemiColon: return TKey::Key_Semicolon;
		case sf::Keyboard::Comma:     return TKey::Key_Comma;
		case sf::Keyboard::Period:    return TKey::Key_Period;
		case sf::Keyboard::Quote:     return TKey::Key_Quote;
		case sf::Keyboard::Slash:     return TKey::Key_Slash;
		case sf::Keyboard::BackSlash: return TKey::Key_Backslash;
		case sf::Keyboard::Tilde:     return TKey::Key_Tilde;
		case sf::Keyboard::Equal:     return TKey::Key_Equal;
		case sf::Keyboard::Dash:      return TKey::Key_Minus;
		case sf::Keyboard::Space:     return TKey::Key_Space;
		case sf::Keyboard::Return:    return TKey::Key_Enter;
		case sf::Keyboard::BackSpace: return TKey::Key_Backspace;
		case sf::Keyboard::Tab:       return TKey::Key_Tab;
		case sf::Keyboard::PageUp:    return TKey::Key_PageUp;
		case sf::Keyboard::PageDown:  return TKey::Key_PageDown;
		case sf::Keyboard::End:       return TKey::Key_End;
		case sf::Keyboard::Home:      return TKey::Key_Home;
		case sf::Keyboard::Insert:    return TKey::Key_Insert;
		case sf::Keyboard::Delete:    return TKey::Key_Delete;
		case sf::Keyboard::Add:       return TKey::Key_Add;
		case sf::Keyboard::Subtract:  return TKey::Key_Subtract;
		case sf::Keyboard::Multiply:  return TKey::Key_Multiply;
		case sf::Keyboard::Divide:    return TKey::Key_Divide;
		case sf::Keyboard::Left:      return TKey::Key_Left;
		case sf::Keyboard::Right:     return TKey::Key_Right;
		case sf::Keyboard::Up:        return TKey::Key_Up;
		case sf::Keyboard::Down:      return TKey::Key_Down;
		case sf::Keyboard::Pause:     return TKey::Key_Pause;

		// Chiffres (pav� num�rique)
		case sf::Keyboard::Numpad0: return TKey::Key_Num0;
		case sf::Keyboard::Numpad1: return TKey::Key_Num1;
		case sf::Keyboard::Numpad2: return TKey::Key_Num2;
		case sf::Keyboard::Numpad3: return TKey::Key_Num3;
		case sf::Keyboard::Numpad4: return TKey::Key_Num4;
		case sf::Keyboard::Numpad5: return TKey::Key_Num5;
		case sf::Keyboard::Numpad6: return TKey::Key_Num6;
		case sf::Keyboard::Numpad7: return TKey::Key_Num7;
		case sf::Keyboard::Numpad8: return TKey::Key_Num8;
		case sf::Keyboard::Numpad9: return TKey::Key_Num9;

		// Fonctions
		case sf::Keyboard::F1:  return TKey::Key_F1;
		case sf::Keyboard::F2:  return TKey::Key_F2;
		case sf::Keyboard::F3:  return TKey::Key_F3;
		case sf::Keyboard::F4:  return TKey::Key_F4;
		case sf::Keyboard::F5:  return TKey::Key_F5;
		case sf::Keyboard::F6:  return TKey::Key_F6;
		case sf::Keyboard::F7:  return TKey::Key_F7;
		case sf::Keyboard::F8:  return TKey::Key_F8;
		case sf::Keyboard::F9:  return TKey::Key_F9;
		case sf::Keyboard::F10: return TKey::Key_F10;
		case sf::Keyboard::F11: return TKey::Key_F11;
		case sf::Keyboard::F12: return TKey::Key_F12;
		case sf::Keyboard::F13: return TKey::Key_F13;
		case sf::Keyboard::F14: return TKey::Key_F14;
		case sf::Keyboard::F15: return TKey::Key_F15;
	}

	return TKey::Key_Unknown;
}
    
} // Namespace TE
