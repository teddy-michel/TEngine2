/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Core/Events.hpp"


namespace TE
{

const char * GetKeyName(TKey key)
{
    switch (key)
    {
        default:
            return "<?>";

        case TKey::Key_0:
        case TKey::Key_Num0:
            return "0";
        case TKey::Key_1:
        case TKey::Key_Num1:
            return "1";
        case TKey::Key_2:
        case TKey::Key_Num2:
            return "2";
        case TKey::Key_3:
        case TKey::Key_Num3:
            return "3";
        case TKey::Key_4:
        case TKey::Key_Num4:
            return "4";
        case TKey::Key_5:
        case TKey::Key_Num5:
            return "5";
        case TKey::Key_6:
        case TKey::Key_Num6:
            return "6";
        case TKey::Key_7:
        case TKey::Key_Num7:
            return "7";
        case TKey::Key_8:
        case TKey::Key_Num8:
            return "8";
        case TKey::Key_9:
        case TKey::Key_Num9:
            return "9";

        case TKey::Key_A:
            return "A";
        case TKey::Key_B:
            return "B";
        case TKey::Key_C:
            return "C";
        case TKey::Key_D:
            return "D";
        case TKey::Key_E:
            return "E";
        case TKey::Key_F:
            return "F";
        case TKey::Key_G:
            return "G";
        case TKey::Key_H:
            return "H";
        case TKey::Key_I:
            return "I";
        case TKey::Key_J:
            return "J";
        case TKey::Key_K:
            return "K";
        case TKey::Key_L:
            return "L";
        case TKey::Key_M:
            return "M";
        case TKey::Key_N:
            return "N";
        case TKey::Key_O:
            return "O";
        case TKey::Key_P:
            return "P";
        case TKey::Key_Q:
            return "Q";
        case TKey::Key_R:
            return "R";
        case TKey::Key_S:
            return "S";
        case TKey::Key_T:
            return "T";
        case TKey::Key_U:
            return "U";
        case TKey::Key_V:
            return "V";
        case TKey::Key_W:
            return "W";
        case TKey::Key_X:
            return "X";
        case TKey::Key_Y:
            return "Y";
        case TKey::Key_Z:
            return "Z";

        // TODO: compléter la liste
    }
}

} // Namespace TE
