/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>

#include "Core/ILogger.hpp"
#include "Core/CDateTime.hpp"


namespace TE
{

/**
 * Constructeur par défaut.
 ******************************/

ILogger::ILogger()
{ }


/**
 * Destructeur.
 ******************************/

ILogger::~ILogger()
{ }


/**
 * Renvoie la date courante.
 *
 * \return Date courante sous forme de chaîne de caractères.
 ******************************/

CString ILogger::date()
{
    return CDateTime::getCurrentDate();
}


/**
 * Renvoie l'heure courante.
 *
 * \return Heure courante sous forme de chaîne de caractères.
 ******************************/

CString ILogger::time()
{
    return CDateTime::getCurrentTime();
}

} // Namespace TE
