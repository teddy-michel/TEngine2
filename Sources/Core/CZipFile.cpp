/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Core/CZipFile.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param fileName Nom du fichier.
 ******************************/

CZipFile::CZipFile(const CString& fileName) :
    m_zip { zip_open(fileName.toCharArray(), ZIP_RDONLY, nullptr) }
{

}


/**
 * Destructeur.
 ******************************/

CZipFile::~CZipFile()
{
    if (m_zip != nullptr)
    {
        zip_close(m_zip);
    }
}


bool CZipFile::isOpen() const noexcept
{
    return (m_zip != nullptr);
}


unsigned int CZipFile::getFilesCount() const noexcept
{
    assert(m_zip != nullptr);
    return static_cast<unsigned int>(zip_get_num_entries(m_zip, 0));
}


unsigned int CZipFile::getFileIndex(const CString& name) const noexcept
{
    assert(m_zip != nullptr);
    return static_cast<unsigned int>(zip_name_locate(m_zip, name.toCharArray(), ZIP_FL_ENC_GUESS));
}


const char * CZipFile::getFileName(unsigned int index) const noexcept
{
    assert(m_zip != nullptr);
    return zip_get_name(m_zip, index, ZIP_FL_ENC_GUESS);
}


bool CZipFile::getFileContent(const CString& name, std::vector<char>& buffer)
{
    assert(m_zip != nullptr);

    buffer.clear();

    zip_file_t * file = zip_fopen(m_zip, name.toCharArray(), 0);
    if (file == nullptr)
    {
        return false;
    }

    unsigned int offset = 0;
    while (1)
    {
        buffer.resize(offset + 4096);

        zip_int64_t sizeRead = zip_fread(file, &buffer[offset], 4096);
        if (sizeRead < 4096)
        {
            break;
        }

        offset += 4096;
    }

    zip_fclose(file);
    return true;
}

} // Namespace TE
