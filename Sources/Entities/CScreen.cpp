/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/matrix_transform.hpp>

#include "Entities/CScreen.hpp"
#include "Entities/CCamera.hpp"
#include "Entities/CMapEntity.hpp"
#include "Game/CMap.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CScreen::CScreen(IEntity * parent, const CString& name) :
    IGraphicEntity     { parent, name },
    m_frameBuffer      {},
    m_buffer           { std::make_unique<CBuffer<TVertex3D>>() },
    m_width            { 100.0f },
    m_height           { 100.0f },
    m_fboWidth         { 64 },
    m_fboHeight        { 64 },
    m_colorBufferFront { CTextureManager::NoTexture },
    m_colorBufferBack  { CTextureManager::NoTexture },
    m_fboIsValid       { false }
{
    m_buffer->setEntityId(getEntityId());
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CScreen::CScreen(CMap * map, const CString& name) :
    IGraphicEntity       { map, name },
    m_frameBuffer        {},
    m_buffer             { std::make_unique<CBuffer<TVertex3D>>() },
    m_width              { 100.0f },
    m_height             { 100.0f },
    m_fboWidth           { 64 },
    m_fboHeight          { 64 },
    m_colorBufferFront   { CTextureManager::NoTexture },
    m_colorBufferBack    { CTextureManager::NoTexture },
    m_colorBuffersLoaded { false },
    m_fboIsValid         { false }
{
    m_buffer->setEntityId(getEntityId());
}


void CScreen::renderEntity(CRenderParams * params)
{
    if (params->getType() == TRenderPass::ShadowMap)
    {
        return;
    }

    if (params->getType() == TRenderPass::Primary)
    {
        // Attachement des textures au FBO
        if (!m_colorBuffersLoaded && m_colorBufferFront != CTextureManager::NoTexture && m_colorBufferBack != CTextureManager::NoTexture)
        {
            m_frameBuffer.bind();

            if (gTextureManager->attachFramebufferTexture(0, m_colorBufferFront) &&
                gTextureManager->attachFramebufferTexture(1, m_colorBufferBack))
            {
                m_colorBuffersLoaded = true;
            }

            m_frameBuffer.unbind();
        }

        if (!m_fboIsValid)
        {
            m_fboIsValid = m_frameBuffer.check();

            if (!m_fboIsValid)
            {
                gApplication->log("FBO is not complete for CScreen.");
            }
        }

        // Rendu dans le FBO
        if (m_fboIsValid && m_colorBuffersLoaded && (params->getMode() == Normal || params->getMode() == Textured) && m_camera != nullptr)
        {
            m_frameBuffer.bind();

            gTextureManager->setTextureResident(m_colorBufferFront, false);
            gTextureManager->setTextureResident(m_colorBufferBack, false);

            // Affichage de la sc�ne avec la cam�ra
            CRenderParams screenParams{ params->getMap(), m_camera, m_camera->getViewMatrix(), glm::perspective(glm::radians(gRenderer->getFieldOfView()), static_cast<float>(m_fboWidth) / m_fboHeight, 1.0f, 10000.0f) };
            screenParams.setType(TRenderPass::Secondary);
            screenParams.setMode(params->getMode());
            screenParams.setViewport(0, 0, m_fboWidth, m_fboHeight);
            screenParams.setClearColor(0.0f, 0.0f, 1.0f, 1.0f); // Fond bleu pour le debug
            screenParams.render(getName());

            gTextureManager->setTextureResident(m_colorBufferBack, true);
            gTextureManager->setTextureResident(m_colorBufferFront, true);

            // Au cas o� le framebuffer a �t� modifi� par render()
            m_frameBuffer.bind();

            // Permutation des color buffers
            std::swap(m_colorBufferFront, m_colorBufferBack);
            gTextureManager->attachFramebufferTexture(0, m_colorBufferFront);
            gTextureManager->attachFramebufferTexture(1, m_colorBufferBack);

            // Default FBO
            m_frameBuffer.unbind();
        }
    }

    IGraphicEntity::renderEntity(params);

    if (m_buffer != nullptr)
    {
        TBufferTextureVector& textures = m_buffer->getTextures();
        textures[0].texture[0] = m_colorBufferBack;

        params->addBuffer(m_buffer.get(), getShader(), getWorldMatrix());
    }
}


void CScreen::setCamera(CCamera * camera)
{
    m_camera = camera;
}


/**
 * Modifie les dimensions de l'�cran.
 *
 * \param width Largeur de l'�cran.
 * \param height Hauteur de l'�cran.
 ******************************/

void CScreen::setSize(float width, float height)
{
    if (width > 0.0f && height > 0.0f && (m_width != width || m_height != height))
    {
        m_width = width;
        m_height = height;
        updateBuffer();
    }
}


/**
 * Modifie les dimensions du FBO.
 *
 * \param width Largeur du FBO.
 * \param height Hauteur du FBO.
 *
 * Les dimensions doivent �tre non nulles, et inf�rieures ou �gales � GL_MAX_RENDERBUFFER_SIZE.
 * La largeur doit �tre inf�rieure ou �gale � GL_MAX_FRAMEBUFFER_WIDTH.
 * La hauteur doit �tre inf�rieure ou �gale � GL_MAX_FRAMEBUFFER_HEIGHT.
 ******************************/

void CScreen::setFBOSize(unsigned int width, unsigned int height)
{
    if (width == 0 || height == 0)
    {
        gApplication->log(CString::fromUTF8("Les dimensions d'un FBO ne peuvent pas �tre nulles."));
        return;
    }

    m_fboWidth = width;
    m_fboHeight = height;
}


unsigned int CScreen::getFBOWidth() const
{
    return m_fboWidth;
}


unsigned int CScreen::getFBOHeight() const
{
    return m_fboHeight;
}


TTextureId CScreen::getTextureId() const
{
    return m_colorBufferFront;
}


void CScreen::initEntity()
{
    initBuffers();

    TBufferTextureVector& textures = m_buffer->getTextures();
    TBufferTexture texture;
    texture.nbr = 6;
    //texture.texture = m_colorBufferBack;
    textures.push_back(texture);

    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(6);
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);

    updateBuffer();
}


void CScreen::initBuffers()
{
    if (m_colorBufferFront != CTextureManager::NoTexture || m_colorBufferBack != CTextureManager::NoTexture)
    {
        gApplication->log(CString::fromUTF8("Les buffers de couleurs ont d�j� �t� charg�s pour ce FBO."));
        return;
    }

    if (m_fboWidth == 0 || m_fboHeight == 0)
    {
        gApplication->log("Les buffers de couleurs d'un FBO ne peuvent pas avoir une dimension nulle.");
        return;
    }

    // Initialisation des deux buffers avec un rectangle noir
    CImage image{ m_fboWidth, m_fboHeight };
    image.fill(CColor::Black);
    CString textureName = CString(":screen:%1:").arg(reinterpret_cast<uintptr_t>(this));
    m_colorBufferFront = gTextureManager->loadTexture(textureName + '1', image, CTextureManager::FilterNone, false);
    m_colorBufferBack = gTextureManager->loadTexture(textureName + '2', image, CTextureManager::FilterNone, false);
    m_colorBuffersLoaded = false;

    m_frameBuffer.createDepthBuffer(m_fboWidth, m_fboHeight, TDepthFormat::Depth24_Stencil8);
}


void CScreen::updateBuffer()
{
    std::vector<TVertex3D> vertices(4);

    vertices[0] = { {0.0f, -m_width, -m_height}, 0, {-1.0f, 0.0f, 0.0f}, {}, {{1.0f, 0.0f}} };
    vertices[1] = { {0.0f,  m_width, -m_height}, 0, {-1.0f, 0.0f, 0.0f}, {}, {{0.0f, 0.0f}} };
    vertices[2] = { {0.0f,  m_width,  m_height}, 0, {-1.0f, 0.0f, 0.0f}, {}, {{0.0f, 1.0f}} };
    vertices[3] = { {0.0f, -m_width,  m_height}, 0, {-1.0f, 0.0f, 0.0f}, {}, {{1.0f, 1.0f}} };

    m_buffer->setData(std::move(vertices));
    m_buffer->update();
}

} // Namespace TE
