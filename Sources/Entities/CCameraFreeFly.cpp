/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CCameraFreeFly.hpp"
#include "Core/Events.hpp"
#include "Core/utils.hpp"


namespace TE
{

constexpr float PlayerMouseMotionSpeed = 0.15f; ///< Vitesse de d�placement du curseur.
constexpr float PlayerVelocity = 10 * 200.0f;   ///< Vitesse de d�placement de la cam�ra (en cm/s).


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CCameraFreeFly::CCameraFreeFly(IEntity * parent, const CString& name) :
    CCamera        { parent, name },
    IInputReceiver {},
    m_theta        { 0.0f },
    m_phi          { 0.0f }
{
    updateCameraDirection(false);
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CCameraFreeFly::CCameraFreeFly(CMap * map, const CString& name) :
    CCamera        { map, name },
    IInputReceiver {},
    m_theta        { 0.0f },
    m_phi          { 0.0f }
{
    updateCameraDirection(false);
}


/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CCameraFreeFly::updateEntity(float frameTime)
{
    // D�placement vers l'avant
    if (isKeyPressed(sf::Keyboard::Z))
    {
        translate({ frameTime * PlayerVelocity, 0.0f, 0.0f });
    }

    // D�placement vers l'arri�re
    if (isKeyPressed(sf::Keyboard::S))
    {
        translate({ -frameTime * PlayerVelocity, 0.0f, 0.0f });
    }

    // D�placement vers la gauche
    if (isKeyPressed(sf::Keyboard::Q))
    {
        translate({ 0.0f, frameTime * PlayerVelocity, 0.0f });
    }

    // D�placement vers la droite
    if (isKeyPressed(sf::Keyboard::D))
    {
        translate({ 0.0f, -frameTime * PlayerVelocity, 0.0f });
    }

    // Rotation vers la gauche
    if (isKeyPressed(sf::Keyboard::Left))
    {
        m_theta += frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f;
        updateCameraDirection(true);
    }

    // Rotation vers la droite
    if (isKeyPressed(sf::Keyboard::Right))
    {
        m_theta -= frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f;
        updateCameraDirection(true);
    }

    // Rotation vers le haut
    if (isKeyPressed(sf::Keyboard::Up))
    {
        m_phi += frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f;
        updateCameraDirection(true);
    }

    // Rotation vers le bas
    if (isKeyPressed(sf::Keyboard::Down))
    {
        m_phi -= frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f;
        updateCameraDirection(true);
    }
}


void CCameraFreeFly::onEvent(const CMouseEvent& event)
{
    m_phi -= event.yrel * PlayerMouseMotionSpeed * Pi / 180.0f;
    m_theta -= event.xrel * PlayerMouseMotionSpeed * Pi / 180.0f;
    updateCameraDirection(true);
}


void CCameraFreeFly::updateCameraDirection(bool needUpdateWorldMatrix)
{
    // Check angles values
    if (m_phi > 89.0f * Pi / 180.0f)
    {
        m_phi = 89.0f * Pi / 180.0f;
    }
    else if (m_phi < -89.0f * Pi / 180.0f)
    {
        m_phi = -89.0f * Pi / 180.0f;
    }

    while (m_theta < -Pi)
    {
        m_theta += 2 * Pi;
    }

    while (m_theta > Pi)
    {
        m_theta -= 2 * Pi;
    }

    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, rotation, translation, skew, perspective);

    // Copy translation and scale and change rotation
    m_localMatrix = glm::mat4{ 1.0f };
    m_localMatrix = glm::scale(m_localMatrix, scale);
    m_localMatrix = glm::translate(m_localMatrix, translation);
    m_localMatrix = m_localMatrix * glm::mat4_cast(glm::angleAxis(m_theta, glm::vec3{ 0.0f, 0.0f, 1.0f }));
    m_localMatrix = m_localMatrix * glm::mat4_cast(glm::angleAxis(-m_phi, glm::vec3{ 0.0f, 1.0f, 0.0f })); // DEBUG: camera free fly

    if (needUpdateWorldMatrix)
    {
        updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
    }
}

} // Namespace TE
