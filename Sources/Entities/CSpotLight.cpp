/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CSpotLight.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CSpotLight::CSpotLight(IEntity * parent, const CString& name) :
    IDirectionalLight { parent, name },
    m_innerAngle      { 30.0f },
    m_outerAngle      { 45.0f },
    m_maxDistance     { 100000.0f },
    m_projection      { glm::perspective<float>(glm::radians(m_outerAngle), 1.0f, 1.0f, m_maxDistance) } // TODO: get near et far from Renderer?
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CSpotLight::CSpotLight(CMap * map, const CString& name) :
    IDirectionalLight { map, name },
    m_innerAngle      { 30.0f },
    m_outerAngle      { 45.0f },
    m_maxDistance     { 100000.0f },
    m_projection      { glm::perspective<float>(glm::radians(m_outerAngle), 1.0f, 1.0f, m_maxDistance) } // TODO: get near et far from Renderer?
{

}


/**
 * Modifie l'angle int�rieur du faisceau.
 *
 * \param angle Angle int�rieur du faisceau, en degr�s (entre 1 et 135 degr�s).
 ******************************/

void CSpotLight::setInnerAngle(float angle)
{
    if (angle >= 1.0f && angle <= 135.0f && angle != m_innerAngle)
    {
        m_innerAngle = angle;
    }
}


/**
 * Modifie l'angle ext�rieur du faisceau.
 *
 * \param angle Angle ext�rieur du faisceau, en degr�s (entre 1 et 135 degr�s).
 ******************************/

void CSpotLight::setOuterAngle(float angle)
{
    if (angle >= 1.0f && angle <= 135.0f && angle != m_outerAngle)
    {
        m_outerAngle = angle;
        m_projection = glm::perspective<float>(glm::radians(m_outerAngle), 1.0f, 1.0f, m_maxDistance); // TODO: get near et far from Renderer?
    }
}


void CSpotLight::setMaxDistance(float distance)
{
    if (distance > 1.0 && distance != m_maxDistance)
    {
        m_maxDistance = distance;
        m_projection = glm::perspective<float>(glm::radians(m_outerAngle), 1.0f, 1.0f, m_maxDistance); // TODO: get near et far from Renderer?
    }
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CSpotLight::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CSpotLight::loadFromJson()", ILogger::Debug);

    if (!IDirectionalLight::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    float innerAngle;
    T_PARSE_ENTITY_ATTR_NUMBER("inner_angle", innerAngle, hasAttr); // TODO: conversion ?
    if (hasAttr)
    {
        setInnerAngle(innerAngle);
    }

    float outerAngle;
    T_PARSE_ENTITY_ATTR_NUMBER("outer_angle", outerAngle, hasAttr); // TODO: conversion ?
    if (hasAttr)
    {
        setOuterAngle(outerAngle);
    }

    float maxDistance;
    T_PARSE_ENTITY_ATTR_NUMBER("max_distance", maxDistance, hasAttr);
    if (hasAttr)
    {
        setMaxDistance(maxDistance);
    }

    return true;
}


glm::mat4 CSpotLight::getViewMatrix(int face) const noexcept
{
    assert(face >= 0 && face <= 5);
    T_UNUSED(face);

    // TODO: mettre en cache la matrice de vue ?

    const glm::vec3 lightPos = getWorldPosition();

    const glm::quat rotation = getWorldOrientation();
    const glm::vec3 forward = glm::vec3{ 1.0f, 0.0f, 0.0f } * rotation;
    //const glm::vec3 left = glm::vec3{ 0.0f, 1.0f, 0.0f } * rotation;
    const glm::vec3 up = glm::vec3{ 0.0f, 0.0f, 1.0f } * rotation;

    return glm::lookAt(lightPos, lightPos + forward, up);
}

} // Namespace TE
