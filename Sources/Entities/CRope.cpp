/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cassert>

#include "Entities/CCylinder.hpp"
#include "Entities/CRope.hpp"
#include "Graphic/CShaderManager.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Core/CApplication.hpp"
#include "Physic/CDynamicActor.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CKinematicActor.hpp"
//#include "Physic/CShapeBox.hpp"
#include "Physic/CScene.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CRope::CRope(IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_length       { 1000.0f },
    m_radius       { 1.0f },
    m_segments     { 10 },
    m_mass         { 1.0f },
    m_attachment   { 0.0f },
    m_init         { false },
    m_boxes        {}
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CRope::CRope(CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_length       { 1000.0f },
    m_radius       { 1.0f },
    m_segments     { 10 },
    m_mass         { 1.0f },
    m_attachment   { 0.0f },
    m_init         { false },
    m_boxes        {}
{

}


/**
 * Modifie la longueur de la corde.
 *
 * \param length Longueur de la corde.
 ******************************/

void CRope::setLength(float length)
{
    if (length > 0.0f && length != m_length)
    {
        m_length = length;
        if (m_init)
        {
            updateBoxes();
        }
    }
}


/**
 * Modifie le rayon de la corde.
 *
 * \param radius Rayon de la corde.
 ******************************/

void CRope::setRadius(float radius)
{
    if (radius > 0.0f && radius != m_radius)
    {
        m_radius = radius;

        assert(m_segments > 0);
        for (auto& cylinder : m_boxes)
        {
            cylinder->setRadius(m_radius);
        }
    }
}


void CRope::setSegments(unsigned int segments)
{
    if (segments > 1 && segments != m_segments)
    {
        m_segments = segments;
        if (m_init)
        {
            updateBoxes();
        }
    }
}


/**
 * Modifie la masse de la corde.
 *
 * \param mass Masse de la corde en kilogrammes.
 ******************************/

void CRope::setMass(float mass)
{
    if (mass >= 0.0f && mass != m_mass)
    {
        m_mass = mass;

        assert(m_segments > 0);
        for (auto& cylinder : m_boxes)
        {
            cylinder->setMass(m_mass / m_segments);
        }
    }
}


void CRope::setAttachment(const glm::vec3& position)
{
    // TODO: move last box
    m_attachment = position;

    if (m_init)
    {
        updateBoxes();
    }
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/
/*
void CRope::renderEntity(const CRenderParams& params)
{
    IGraphicEntity::renderEntity(params);
}
*/

void CRope::initEntity()
{
    updateBoxes();
    m_init = true;
}


void CRope::updateBoxes()
{
    assert(m_segments > 0);

    for (auto& cylinder : m_boxes)
    {
        delete cylinder;
    }

    m_boxes.clear();

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        auto shader = gShaderManager->loadFromFiles("texturing_bt.vert", "texturing_bt.frag");
        //auto shader = std::make_shared<CShader>();
        //shader->loadFromFiles("../../../Resources/shaders/texturing_bt.vert", "../../../Resources/shaders/texturing_bt.frag");
        shader->setBindlessTexture(true);
        setShader(shader);
    }
    else
    {
        auto shader = gShaderManager->loadFromFiles("texturing.vert", "texturing.frag");
        //auto shader = std::make_shared<CShader>();
        //shader->loadFromFiles("../../../Resources/shaders/texturing.vert", "../../../Resources/shaders/texturing.frag");
        setShader(shader);
    }

    for (unsigned int i = 0; i < m_segments; ++i)
    {
        auto box = new CCylinder { this, getName() + CString("_segment_%1").arg(i) };
        m_boxes.push_back(box);
        box->setFixParent(true);
        box->setMass(m_mass / m_segments);
        box->setDynamic(true);
        box->setShader(getShader());
        box->setTexture(gTextureManager->loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap));
        box->setLength(m_length / m_segments);
        box->setRadius(m_radius);
        box->translate({ i * m_length / m_segments, 0.0f, 0.0f });
        box->rotate(glm::angleAxis(glm::radians(90.0f), glm::vec3{ 0.0f, 1.0f, 0.0f }));
        // TODO: position
    }

    // TODO: create joint
}

} // Namespace TE
