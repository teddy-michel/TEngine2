/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Entities/ILight.hpp"
#include "Game/CMap.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

ILight::ILight(IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_color        { CColor::White },
    m_intensity    { 0.5f },
    m_enabled      { true }
{
    //getMap()->addLight(this); // we can't do that because addLight call a virtual method of ILight
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

ILight::ILight(CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_color        { CColor::White },
    m_intensity    { 0.5f },
    m_enabled      { true }
{
    //getMap()->addLight(this); // we can't do that because addLight call a virtual method of ILight
}


/**
 * Destructeur
 ******************************/

ILight::~ILight()
{
    getMap()->removeLight(this);
}


/**
 * Modifie la couleur de la lumi�re.
 *
 * \param color Couleur de la lumi�re.
 ******************************/

void ILight::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Modifie l'intensit� de la lumi�re.
 *
 * \param intensity Intensit� de la lumi�re (entre 0 et 1).
 ******************************/

void ILight::setIntensity(float intensity)
{
    if (intensity <= 0.0f)
    {
        m_intensity = 0.0f;
    }
    else if (intensity >= 1.0f)
    {
        m_intensity = 1.0f;
    }
    else
    {
        m_intensity = intensity;
    }
}


/**
 * Active ou d�sactive la lumi�re.
 *
 * \param enabled Bool�en.
 ******************************/

void ILight::setEnabled(bool enabled)
{
    m_enabled = enabled;
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool ILight::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("ILight::loadFromJson()", ILogger::Debug);

    if (!IGraphicEntity::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    bool staticLight = false;
    T_PARSE_ENTITY_ATTR_BOOL("static", staticLight, hasAttr);

    if (staticLight)
    {
        // Static lights are only present in the editor and must be removed by the map compiler
        return false;
    }

    T_PARSE_ENTITY_ATTR_BOOL("enabled", m_enabled, hasAttr);

    float intensity;
    T_PARSE_ENTITY_ATTR_NUMBER("intensity", intensity, hasAttr);
    if (hasAttr)
    {
        setIntensity(intensity);
    }

    if (json.contains("color"))
    {
        const auto attribute = json["color"];
        if (!attribute.is_object())
        {
            gApplication->log("Entity attribute \"color\" should be an object.", ILogger::Error);
            return false;
        }

        if (!attribute.contains("r") || !attribute.contains("g") || !attribute.contains("b"))
        {
            gApplication->log("Entity attribute \"color\" must contains keys \"r\", \"g\", and \"b\".", ILogger::Error);
            return false;
        }

        const auto valueR = attribute["r"];
        if (!valueR.is_number_unsigned() || valueR > 255)
        {
            gApplication->log("Entity attribute \"color.r\" must be an unsigned integer between 0 and 255.", ILogger::Error);
            return false;
        }

        const auto valueG = attribute["g"];
        if (!valueG.is_number_unsigned() || valueG > 255)
        {
            gApplication->log("Entity attribute \"color.g\" must be an unsigned integer between 0 and 255.", ILogger::Error);
            return false;
        }

        const auto valueB = attribute["b"];
        if (!valueB.is_number_unsigned() || valueB > 255)
        {
            gApplication->log("Entity attribute \"color.b\" must be an unsigned integer between 0 and 255.", ILogger::Error);
            return false;
        }

        m_color.setRed(valueR);
        m_color.setGreen(valueG);
        m_color.setBlue(valueB);
    }

    return true;
}

} // Namespace TE
