/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CVehicle.hpp"
#include "Physic/CScene.hpp"
#include "Physic/CVehicleController.hpp"


namespace TE
{

/**
 * Construit une entit� CVehicle.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CVehicle::CVehicle(IEntity * parent, const CString& name) :
    IEntity        { parent, name },
    IInputReceiver {},
    m_controller   { nullptr },
    m_player       { nullptr }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CVehicle::CVehicle(CMap * map, const CString& name) :
    IEntity        { map, name },
    IInputReceiver {},
    m_controller   { nullptr },
    m_player       { nullptr }
{

}


/**
 * Destructeur.
 ******************************/

CVehicle::~CVehicle()
{
    if (m_controller != nullptr)
    {
        CScene * scene = getScene();
        if (scene != nullptr)
        {
            scene->removeVehicleController(m_controller);
        }
    }
}


bool CVehicle::setPlayer(CPlayer * player)
{
    if (player == nullptr)
    {
        return false;
    }

    // Le joueur est d�j� conducteur
    if (player == m_player)
    {
        return true;
    }

    // Il y a d�j� un conducteur
    if (m_player != nullptr)
    {
        return false;
    }

    m_player = player;

    return true;
}


/**
 * Calcule la vitesse du v�hicule.
 *
 * \return Vitesse du v�hicule en m/s.
 ******************************/

float CVehicle::getSpeed() const
{
    assert(m_controller != nullptr);
    return m_controller->getSpeed();
}


/**
 * Calcule la vitesse de rotation du moteur.
 *
 * \return Vitesse de rotation du moteur en tours/min.
 ******************************/

float CVehicle::getEngineSpeed() const
{
    assert(m_controller != nullptr);
    return m_controller->getEngineSpeed();
}


/**
 * Retourne le rapport de vitesse actuel.
 *
 * \return Rapport de vitesse. -1 pour la marche arri�re, 0 pour le neutre, et entre 1 et 5 pour les vitesses.
 ******************************/

int CVehicle::getCurrentGear() const
{
    assert(m_controller != nullptr);
    return m_controller->getCurrentGear();
}



/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CVehicle::updateEntity(float frameTime)
{
    assert(m_controller != nullptr);

    //if (m_player != nullptr)
    {
        m_controller->setDigitalAccel(isActionActive(TAction::ActionVehicleAccel));
        m_controller->setDigitalBrake(isActionActive(TAction::ActionVehicleBrake));
        m_controller->setDigitalSteerLeft(isActionActive(TAction::ActionVehicleTurnLeft));
        m_controller->setDigitalSteerRight(isActionActive(TAction::ActionVehicleTurnRight));
        m_controller->setDigitalHandBrake(isActionActive(TAction::ActionVehicleHandBrake));
        m_controller->setGearUp(isActionActive(TAction::ActionVehicleGearUp));
        m_controller->setGearDown(isActionActive(TAction::ActionVehicleGearDown));

        m_controller->updateInputs(frameTime);
    }
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CVehicle::renderEntity(CRenderParams * params)
{
    IEntity::renderEntity(params);
/*
    if (m_buffer != nullptr)
    {
        if (m_buffer->getDisplayMode() != params.mode)
        {
            m_buffer->setDisplayMode(params.mode);
            updateSize(); // TODO: pourquoi cet appel ???
            //m_buffer->update();
        }
        m_buffer->draw();
    }
*/
}


/**
 * Initialise le contr�leur du v�hicule.
 *
 * \todo Faire �a dans initEntity ?
 * \todo Pr�ciser un mod�le ? ou des IShape ?
 ******************************/

void CVehicle::initController()
{
    CScene * scene = getScene();
    assert(scene != nullptr);
    assert(m_controller == nullptr);

    // TODO: charger un mod�le fixe pour les tests
    // TODO: calculer les dimensions de la boite AABB
    // TODO: cr�er les buffers graphiques

    CVehicleData data{};
    data.mass = 1000.0f;
    data.shape = nullptr; // TODO
    data.useAutoGearFlag = true;

    m_controller = scene->createVehicleController(this, data);
    m_controller->setTransform(getWorldMatrix());
}


/**
 * Lib�re le contr�leur du v�hicule.
 *
 * \todo Supprimer cette m�thode ?
 ******************************/

void CVehicle::releaseController()
{
    // Suppression de l'ancien contr�leur
    if (m_controller != nullptr)
    {
        CScene * scene = getScene();
        assert(scene != nullptr);
        scene->removeVehicleController(m_controller);
        m_controller = nullptr;
    }
}


void CVehicle::notifyWorldMatrixChange()
{
    if (m_controller != nullptr)
    {
        glm::vec3 scale;
        glm::quat rotation;
        glm::vec3 position;
        glm::quat currentRotation;
        glm::vec3 currentPosition;
        glm::vec3 skew;
        glm::vec4 perspective;

        glm::decompose(getWorldMatrix(), scale, rotation, position, skew, perspective);

        auto controllerTransform = m_controller->getTransform();
        glm::decompose(controllerTransform, scale, currentRotation, currentPosition, skew, perspective);

        if (std::abs(position.x - currentPosition.x) > 0.005f ||
            std::abs(position.y - currentPosition.y) > 0.005f ||
            std::abs(position.z - currentPosition.z) > 0.005f ||
            std::abs(rotation.w - currentRotation.w) > 0.005f ||
            std::abs(rotation.x - currentRotation.x) > 0.005f ||
            std::abs(rotation.y - currentRotation.y) > 0.005f ||
            std::abs(rotation.z - currentRotation.z) > 0.005f)
        {
            m_controller->setTransform(getWorldMatrix());
        }
    }
}

} // Namespace TE
