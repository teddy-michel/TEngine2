/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CCamera.hpp"
//#include "Entities/CBox.hpp" // DEBUG


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit้ parent (non nul).
 * \param name Nom de l'entit้.
 ******************************/

CCamera::CCamera(IEntity * parent, const CString& name) :
    IEntity { parent, name }
{
    // DEBUG: affichage d'une boite
    //auto m_mesh = new CBox{ "camera_box", this };
    //m_mesh->setSolid(false);
    //m_mesh->setSize({ 10.0f, 10.0f, 10.0f });
    //m_mesh->setTexture(CTextureManager::instance().loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap));
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit้.
 ******************************/

CCamera::CCamera(CMap * map, const CString& name) :
    IEntity { map, name }
{
    // DEBUG: affichage d'une boite
    //auto m_mesh = new CBox{ "camera_box", this };
    //m_mesh->setSolid(false);
    //m_mesh->setSize({ 10.0f, 10.0f, 10.0f });
    //m_mesh->setTexture(CTextureManager::instance().loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap));
}


glm::mat4 CCamera::getViewMatrix() const
{
    const glm::vec3 cameraPos = getWorldPosition();

    const glm::quat rotation = getWorldOrientation();
    const glm::vec3 forward = glm::vec3{ 1.0f, 0.0f, 0.0f } * rotation;
    //const glm::vec3 left = glm::vec3{ 0.0f, 1.0f, 0.0f } * rotation;
    const glm::vec3 up = glm::vec3{ 0.0f, 0.0f, 1.0f } * rotation;

    return glm::lookAt(cameraPos, cameraPos + forward, up);
}

} // Namespace TE
