/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CWeaponSphere.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CSphere.hpp"
#include "Entities/CMapEntity.hpp"
#include "Entities/CCamera.hpp"
#include "Core/CApplication.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CShaderManager.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CWeaponSphere::CWeaponSphere(IEntity * parent, const CString& name) :
	IWeapon { parent, name }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CWeaponSphere::CWeaponSphere(CMap * map, const CString& name) :
	IWeapon { map, name }
{

}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CWeaponSphere::renderEntity(CRenderParams * params)
{
	if (m_player != nullptr && m_player->getCamera() == params->getCamera())
	{
		// TODO: affichage du mod�le � la premi�re personne
	}
	else
	{
		// TODO: affichage du mod�le � la troisi�me personne
	}
}


void CWeaponSphere::primaryFire()
{
    gApplication->log("Throwing sphere...", ILogger::Debug);

    CSphere * sphere = new CSphere{ getMap() };

    IEntity * root = getRoot();
    if (root != nullptr)
    {
        sphere->setParent(root);
    }

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        auto shader = gShaderManager->loadFromFiles("texturing_bt.vert", "texturing_bt.frag");
        //auto shader = std::make_shared<CShader>();
        //shader->loadFromFiles("../../../Resources/shaders/texturing_bt.vert", "../../../Resources/shaders/texturing_bt.frag");
        shader->setBindlessTexture(true);
        sphere->setShader(shader);
    }
    else
    {
        auto shader = gShaderManager->loadFromFiles("texturing.vert", "texturing.frag");
        //auto shader = std::make_shared<CShader>();
        //shader->loadFromFiles("../../../Resources/shaders/texturing.vert", "../../../Resources/shaders/texturing.frag");
        sphere->setShader(shader);
    }

    sphere->setTexture(gTextureManager->loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap));
    sphere->translate(getWorldPosition());
    //sphere->setOrientation(getWorldOrientation());
    sphere->setRadius(2.0f);
    sphere->setMass(10.0f);
    sphere->setDynamic(true);
    sphere->setSolid(true);

    sphere->initEntity();

    auto orientation = getWorldOrientation();
    auto direction = glm::vec3{ 1.0f, 0.0f, 0.0f } * orientation;
    sphere->setLinearVelocity(1000.0f * direction);
}

} // Namespace TE
