/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CPointLight.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CPointLight::CPointLight(IEntity * parent, const CString& name) :
    ILight        { parent, name },
    m_maxDistance { 100000.0f },
    m_projection  { glm::perspective<float>(glm::radians(91.0f), 1.0f, 1.0f, m_maxDistance) } // TODO: get near et far from Renderer?
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CPointLight::CPointLight(CMap * map, const CString& name) :
    ILight        { map, name },
    m_maxDistance { 100000.0f },
    m_projection  { glm::perspective<float>(glm::radians(91.0f), 1.0f, 1.0f, m_maxDistance) } // TODO: get near et far from Renderer?
{

}


void CPointLight::setMaxDistance(float maxDistance)
{
    if (maxDistance > 0.1f && maxDistance != m_maxDistance)
    {
        m_maxDistance = maxDistance;
        m_projection = glm::perspective<float>(glm::radians(91.0f), 1.0f, 1.0f, m_maxDistance); // TODO: get near et far from Renderer?
    }
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CPointLight::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CPointLight::loadFromJson()", ILogger::Debug);

    if (!ILight::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    float maxDistance;
    T_PARSE_ENTITY_ATTR_NUMBER("max_distance", maxDistance, hasAttr);
    if (hasAttr)
    {
        setMaxDistance(maxDistance);
    }

    return true;
}


/**
 * Retourne la matrice de vue pour une direction.
 *
 * \param face Direction (entre 0 et 5).
 * \return Matrice de vue.
 ******************************/

glm::mat4 CPointLight::getViewMatrix(int face) const noexcept
{
    assert(face >= 0 && face <= 5);

    const glm::vec3 lightPos = getWorldPosition();

    switch (face)
    {
        case 0: // X_Neg
            return glm::lookAt(lightPos, lightPos + glm::vec3{ -1.0f,  0.0f,  0.0f }, glm::vec3{ 0.0f, 0.0f, 1.0f });
        case 1: // X_Pos
            return glm::lookAt(lightPos, lightPos + glm::vec3{  1.0f,  0.0f,  0.0f }, glm::vec3{ 0.0f, 0.0f, 1.0f });
        case 2: // Y_Neg
            return glm::lookAt(lightPos, lightPos + glm::vec3{  0.0f, -1.0f,  0.0f }, glm::vec3{ 0.0f, 0.0f, 1.0f });
        case 3: // Y_Pos
            return glm::lookAt(lightPos, lightPos + glm::vec3{  0.0f,  1.0f,  0.0f }, glm::vec3{ 0.0f, 0.0f, 1.0f });
        case 4: // Z_Neg
            return glm::lookAt(lightPos, lightPos + glm::vec3{  0.0f,  0.0f, -1.0f }, glm::vec3{ 1.0f, 0.0f, 0.0f });
        case 5: // Z_Pos
            return glm::lookAt(lightPos, lightPos + glm::vec3{  0.0f,  0.0f,  1.0f }, glm::vec3{ 1.0f, 0.0f, 0.0f });
    }

    return glm::mat4{}; // code jamais atteint
}

} // Namespace TE
