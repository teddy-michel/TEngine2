/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_interpolation.hpp>

#include "Entities/CTrack.hpp"
#include "Entities/CTrackPoint.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CTrack::CTrack(IEntity * parent, const CString& name) :
    IEntity     { parent, name },
    m_running   { false },
    m_loop      { false },
    m_lastPoint { -1 },
    m_delta     { 0.0f },
    m_speed     { 100.0f }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CTrack::CTrack(CMap * map, const CString& name) :
    IEntity     { map, name },
    m_running   { false },
    m_loop      { false },
    m_lastPoint { -1 },
    m_delta     { 0.0f },
    m_speed     { 100.0f }
{

}


std::vector<CTrackPoint *> CTrack::getPoints() const noexcept
{
    return m_points;
}


void CTrack::addPoint(CTrackPoint * point, int position)
{
	if (point == nullptr)
	{
		return;
	}

	if (position < 0 || position >= static_cast<int>(m_points.size()))
	{
		m_points.push_back(point);
	}
	else
	{
		auto it = m_points.begin();
		it += position;
		m_points.insert(it, point);
	}
}


void CTrack::start()
{
	if (m_points.size() > 1)
	{
		if (m_loop || m_lastPoint < m_points.size() - 1)
		{
			m_running = true;
		}
	}
}


void CTrack::stop()
{
    m_running = false;
}


void CTrack::reset()
{
	if (m_points.size() != 0)
	{
		m_lastPoint = 0;
	}
	else
	{
		m_lastPoint = -1;
		m_running = false;
	}

	m_delta = 0.0f;
}


void CTrack::setSpeed(float speed)
{
    if (speed > 0.0f)
    {
        m_speed = speed;
    }
}


void CTrack::setLoop(bool loop)
{
    m_loop = loop;
}


/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CTrack::updateEntity(float frameTime)
{
	if (m_points.size() == 0)
	{
		return;
	}

	// Premi�re mise-�-jour
	if (m_lastPoint == -1)
	{
		if (!m_running)
		{
			setWorldMatrix(m_points[0]->getWorldMatrix());
		}

		m_lastPoint = 0;
	}

	if (m_running && frameTime > 0.0f)
	{
		assert(m_points.size() > 1);

		// Distance � parcourir
		float distance = m_speed * frameTime;
		int nextPoint = -1;

		while (true)
		{
			nextPoint = (m_lastPoint == m_points.size() - 1 ? (m_loop ? 0 : -1) : m_lastPoint + 1);

			if (nextPoint == -1) // Fin du chemin
			{
				m_running = false;
				break;
			}

			float distanceBetweenPoints = glm::distance(m_points[m_lastPoint]->getWorldPosition(), m_points[nextPoint]->getWorldPosition());

			// Les deux points sont au m�me endroit
			if (distanceBetweenPoints == 0.0f)
			{
				m_lastPoint = nextPoint;
				m_delta = 0.0f;
				continue;
			}

			float newDelta = m_delta + (distance / distanceBetweenPoints);

			if (newDelta < 1.0f)
			{
				m_delta = newDelta;
				break;
			}
			else
			{
				distance -= (1.0f - m_delta) * distanceBetweenPoints;
				m_lastPoint = nextPoint;
				m_delta = 0.0f;
			}
		}

		if (m_delta == 0.0f || nextPoint == -1)
		{
			setWorldMatrix(m_points[m_lastPoint]->getWorldMatrix());
		}
		else
		{
			glm::mat4 newWorldMatrix = m_points[m_lastPoint]->getWorldMatrix();
			glm::mat4 nextWorldMatrix = m_points[nextPoint]->getWorldMatrix();

			// Interpollation de la position et conservation de l'orientation du dernier point
			newWorldMatrix[3][0] += m_delta * (nextWorldMatrix[3][0] - newWorldMatrix[3][0]);
			newWorldMatrix[3][1] += m_delta * (nextWorldMatrix[3][1] - newWorldMatrix[3][1]);
			newWorldMatrix[3][2] += m_delta * (nextWorldMatrix[3][2] - newWorldMatrix[3][2]);
			setWorldMatrix(newWorldMatrix);

			// Interpollation des matrices globales (y compris l'orientation)
			//setWorldMatrix(glm::interpolate(newWorldMatrix, nextWorldMatrix, m_delta));
		}
	}
}

} // Namespace TE
