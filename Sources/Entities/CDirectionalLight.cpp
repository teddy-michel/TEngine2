/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CDirectionalLight.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CDirectionalLight::CDirectionalLight(IEntity * parent, const CString& name) :
    IDirectionalLight { parent, name },
    m_projection      { glm::ortho<float>(-1000.0f, 1000.0f, -1000.0f, 1000.0f, -1000.0f, 1000.0f) } // TODO: param�triser
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CDirectionalLight::CDirectionalLight(CMap * map, const CString& name) :
    IDirectionalLight { map, name },
    m_projection      { glm::ortho<float>(-1000.0f, 1000.0f, -1000.0f, 1000.0f, -1000.0f, 1000.0f) } // TODO: param�triser
{

}


glm::mat4 CDirectionalLight::getViewMatrix(int face) const noexcept
{
    assert(face >= 0 && face <= 5);
    T_UNUSED(face);

    // TODO: mettre en cache la matrice de vue ?

    const glm::quat rotation = getWorldOrientation();

    // Par d�faut la lumi�re arrive du haut
    const glm::vec3 forward = glm::vec3{ 0.0f, 0.0f, 1.0f } * rotation;
    //const glm::vec3 left = glm::vec3{ 0.0f, 1.0f, 0.0f } * rotation;
    const glm::vec3 up = glm::vec3{ 1.0f, 0.0f, 0.0f } * rotation;

    return glm::lookAt(forward, glm::vec3{ 0.0f, 0.0f, 0.0f }, up);
}

} // Namespace TE
