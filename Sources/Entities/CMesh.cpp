/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/type_ptr.hpp>

#include "Entities/CMesh.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Physic/CShapeTriMesh.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CScene.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Construit une entit� CMesh.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CMesh::CMesh(IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_buffer       { nullptr },
    m_actor        { nullptr }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CMesh::CMesh(CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_buffer       { nullptr },
    m_actor        { nullptr }
{

}


/**
 * Destructeur.
 ******************************/

CMesh::~CMesh()
{
    if (m_actor != nullptr)
    {
        auto scene = getScene();
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
    }
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CMesh::renderEntity(CRenderParams * params)
{
    IGraphicEntity::renderEntity(params);

    if (m_buffer != nullptr)
    {
        params->addBuffer(m_buffer.get(), getShader(), getWorldMatrix());
    }
}


std::shared_ptr<CBuffer<TVertex3D>> CMesh::getBuffer() const
{
    return m_buffer;
}


void CMesh::setBuffer(const std::shared_ptr<CBuffer<TVertex3D>>& buffer)
{
    m_buffer = buffer;
    m_buffer->setEntityId(getEntityId()); // TODO: g�rer les buffers partag�s
}


void CMesh::setTriMesh(const std::shared_ptr<CShapeTriMesh>& trimesh)
{
    assert(trimesh != nullptr);

    m_actor = std::make_shared<CStaticActor>(this, getWorldMatrix(), trimesh, getName().toCharArray());

    CScene * scene = getScene();
    if (scene != nullptr)
    {
        scene->addActor(m_actor);
    }
}


void CMesh::notifyWorldMatrixChange()
{
    if (m_actor != nullptr)
    {
        gApplication->log(CString("Moving static actor %1.").arg(getName()), ILogger::Warning);
        m_actor->setTransform(getWorldMatrix());
    }
}

} // Namespace TE
