/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CPlayer.hpp"
#include "Entities/CCamera.hpp"
#include "Entities/IWeapon.hpp"
#include "Entities/CMapEntity.hpp"
#include "Core/Events.hpp"
#include "Game/CGameApplication.hpp"
#include "Physic/CCharacterController.hpp"
#include "Physic/CRaycastResult.hpp"
#include "Physic/CScene.hpp"
#include "Physic/IActor.hpp"
#include "Core/utils.hpp"


namespace TE
{

constexpr float PlayerMouseMotionSpeed = 0.15f; ///< Vitesse de d�placement du curseur.
constexpr float PlayerVelocity = 200.0f;        ///< Vitesse de d�placement de la cam�ra (en cm/s).


/**
 * Construit une entit� CPlayer.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CPlayer::CPlayer(IEntity * parent, const CString& name) :
    ICharacter        { parent, name },
    IInputReceiver    {},
    m_onGround        { true },
    m_fallingDuration { 0.0f },
    m_controller      { nullptr },
    m_currentWeapon   { nullptr },
    m_vehicle         { nullptr },
    m_weapons         {},
    m_interaction     {}
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CPlayer::CPlayer(CMap * map, const CString& name) :
    ICharacter        { map, name },
    IInputReceiver    {},
    m_onGround        { true },
    m_fallingDuration { 0.0f },
    m_controller      { nullptr },
    m_currentWeapon   { nullptr },
    m_vehicle         { nullptr },
    m_weapons         {},
    m_interaction     {}
{

}


/**
 * Destructeur.
 ******************************/

CPlayer::~CPlayer()
{
    if (m_controller != nullptr)
    {
        CScene * scene = getScene();
        if (scene != nullptr)
        {
            scene->removeCharacterController(m_controller);
        }
    }
}


/**
 * Modifie la hauteur du joueur.
 *
 * \param height Nouvelle hauteur.
 ******************************/

void CPlayer::setHeight(float height)
{
    ICharacter::setHeight(height);
    if (m_controller != nullptr)
    {
        m_controller->resize(getHeight());
    }
}


/**
 * Modifie le rayon du volume englobant du joueur.
 *
 * \param radius Rayon du volume englobant du joueur.
 ******************************/

void CPlayer::setRadius(float radius)
{
    ICharacter::setRadius(radius);

    if (m_controller != nullptr)
    {
        m_controller->setRadius(getRadius());
    }
}


/**
 * Enl�ve l'arme courante du joueur.
 ******************************/

void CPlayer::dropCurrentWeapon()
{
    removeWeapon(m_currentWeapon);
}


/**
 * Ajoute une arme � un joueur.
 * Si le joueur ne poss�de aucune arme, la nouvelle arme est automatiquement s�lectionn�e.
 *
 * \param weapon Pointeur sur l'arme � ajouter.
 ******************************/

void CPlayer::addWeapon(IWeapon * weapon)
{
    assert(weapon != nullptr);

    // Le joueur poss�de d�j� cette arme
    if (std::find(m_weapons.cbegin(), m_weapons.cend(), weapon) != m_weapons.cend())
    {
        return;
    }

    if (weapon->m_player != nullptr)
    {
        weapon->m_player->removeWeapon(weapon);
    }

    weapon->m_player = this;
    m_weapons.push_back(weapon);

    addChild(weapon);
    weapon->setFixParent(true);

    weapon->setPosition(glm::vec3{ 40.0f, 0.0f, 0.0f });

    if (m_currentWeapon == nullptr)
    {
        m_currentWeapon = weapon;
    }
}


/**
 * Enl�ve une arme � un joueur.
 *
 * \param weapon Pointeur sur l'arme � retirer.
 ******************************/

void CPlayer::removeWeapon(IWeapon * weapon)
{
    assert(weapon != nullptr);

    // Le joueur ne poss�de pas cette arme
    if (std::find(m_weapons.cbegin(), m_weapons.cend(), weapon) == m_weapons.cend())
    {
        return;
    }

    weapon->setFireActive(false);

    if (m_currentWeapon == weapon)
    {
        m_currentWeapon = nullptr;
    }

    m_weapons.remove(weapon);
    weapon->m_player = nullptr;

    weapon->setFixParent(false);
    weapon->setParent(getParent()); // TODO: rattacher � la racine de l'arbre ?
}


/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CPlayer::updateEntity(float frameTime)
{
    if (m_vehicle == nullptr)
    {
        glm::vec3 wantedTranslation{ 0.0f, 0.0f, -9.81f * 100 }; // TODO: get gravity from scene
        // TODO: mieux g�rer la gravit�. Utiliser un bool�en pour savoir si le joueur est sur le sol.

        if (m_onGround)
        {
            bool forward = isActionActive(TAction::ActionForward);
            bool backward = isActionActive(TAction::ActionBackward);
            bool left = isActionActive(TAction::ActionStrafeLeft);
            bool right = isActionActive(TAction::ActionStrafeRight);

            // Mouvements qui s'annulent
            if (forward && backward)
            {
                forward = backward = false;
            }

            if (left && right)
            {
                left = right = false;
            }

            // Calcul du d�placement d�sir�
            if (forward || backward || left || right)
            {
                const glm::quat rotation = getWorldOrientation();
                const glm::vec3 forwardDir = glm::vec3{ 1.0f, 0.0f, 0.0f } *rotation;
                const glm::vec3 leftDir = glm::vec3{ 0.0f, 1.0f, 0.0f } *rotation;
                //const glm::vec3 upDir = glm::vec3{ 0.0f, 0.0f, 1.0f } * rotation;

                // D�placement vers l'avant
                if (forward)
                {
                    wantedTranslation += forwardDir * PlayerVelocity;
                }

                // D�placement vers l'arri�re
                if (backward)
                {
                    wantedTranslation -= forwardDir * PlayerVelocity;
                }

                // D�placement vers la gauche
                if (left)
                {
                    wantedTranslation += leftDir * PlayerVelocity;
                }

                // D�placement vers la droite
                if (right)
                {
                    wantedTranslation -= leftDir * PlayerVelocity;
                }
            }
        }

        // D�placement du joueur
        movePlayer(wantedTranslation, frameTime);

        // Rotation vers la gauche
        if (isActionActive(TAction::ActionTurnLeft))
        {
            changeViewAngles(frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f, 0.0f);
        }

        // Rotation vers la droite
        if (isActionActive(TAction::ActionTurnRight))
        {
            changeViewAngles(-frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f, 0.0f);
        }

        // Rotation vers le haut
        if (isActionActive(TAction::ActionTurnUp))
        {
            changeViewAngles(0.0f, frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f);
        }

        // Rotation vers le bas
        if (isActionActive(TAction::ActionTurnDown))
        {
            changeViewAngles(0.0f, -frameTime * 1000.0f * PlayerMouseMotionSpeed * Pi / 180.0f);
        }

        checkInteractions();
    }

    // Tir
    if (m_currentWeapon != nullptr)
    {
        m_currentWeapon->setFireActive(isActionActive(TAction::ActionFire1));
    }
}


void CPlayer::onEvent(const CMouseEvent& event)
{
    if (event.type == ButtonPressed)
    {
        if (m_currentWeapon != nullptr)
        {
            // TODO: trouver un meilleur moyen de g�rer les actions
            TKey keyFire1 = gGameApplication->getKeyForAction(TAction::ActionFire1);
            if ((event.button == MouseButtonLeft && keyFire1 == TKey::Mouse_Left) || 
                (event.button == MouseButtonRight && keyFire1 == TKey::Mouse_Right))
            {
                m_currentWeapon->primaryFire();
            }

            TKey keyFire2 = gGameApplication->getKeyForAction(TAction::ActionFire2);
            if ((event.button == MouseButtonLeft && keyFire2 == TKey::Mouse_Left) ||
                (event.button == MouseButtonRight && keyFire2 == TKey::Mouse_Right))
            {
                m_currentWeapon->secondaryFire();
            }
        }
    }
    else if (event.type == MoveMouse)
    {
        // TODO: disable when in a vehicle?
        changeViewAngles(-event.xrel * PlayerMouseMotionSpeed * Pi / 180.0f, -event.yrel * PlayerMouseMotionSpeed * Pi / 180.0f);
        //m_phi -= event.yrel * PlayerMouseMotionSpeed * Pi / 180.0f;
        //m_theta -= event.xrel * PlayerMouseMotionSpeed * Pi / 180.0f;
        //updateCameraDirection(true);
    }
}


void CPlayer::onEvent(const CKeyboardEvent& event)
{
    // Tir
    if (m_currentWeapon != nullptr)
    {
        if (event.code == gGameApplication->getKeyForAction(TAction::ActionFire1))
        {
            m_currentWeapon->primaryFire();
        }

        if (event.code == gGameApplication->getKeyForAction(TAction::ActionFire2))
        {
            m_currentWeapon->secondaryFire();
        }
    }

    // Interraction
    if (m_interaction.entity != nullptr)
    {
        if (event.code == m_interaction.key)
        {
            m_interaction.entity->interact(this);
        }
    }
}


void CPlayer::onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage)
{
    gApplication->log(CString::fromUTF8("Player touch� (damage = %1) !").arg(damage));
    //setSize(m_size + 1.0f); // DEBUG
}

/*
void CPlayer::sendToMap(CMapEntity * map)
{
    // Suppression de l'ancien contr�lleur
    if (m_controller != nullptr)
    {
        CScene * scene = getScene();
        assert(scene != nullptr);
        scene->removeCharacterController(m_controller);
        m_controller = nullptr;
    }

    if (map != nullptr)
    {
        map->addPlayer(this);

        CScene * scene = getScene();
        assert(scene != nullptr);
        m_controller = scene->createCharacterController(this, getHeight(), getRadius());
        m_controller->setPosition(getWorldPosition());
    }
}
*/


void CPlayer::initController()
{
    CScene * scene = getScene();
    assert(scene != nullptr);
    assert(m_controller == nullptr);
    m_controller = scene->createCharacterController(this, getHeight(), getRadius());
    m_controller->setPosition(getWorldPosition());
}


void CPlayer::releaseController()
{
    // Suppression de l'ancien contr�leur
    if (m_controller != nullptr)
    {
        CScene * scene = getScene();
        assert(scene != nullptr);
        scene->removeCharacterController(m_controller);
        m_controller = nullptr;
    }
}


void CPlayer::movePlayer(const glm::vec3& translation, float frameTime)
{
    if (m_controller != nullptr)
    {
        bool onGround = m_controller->move(translation * frameTime, frameTime);
        if (onGround)
        {
            if (!m_onGround)
            {
                // TODO: mieux calculer les d�g�ts de chute (utiliser la diff�rence de hauteur ? la vitesse d'impact ?)
                if (m_fallingDuration > 0.5f)
                {
                    float damage = m_fallingDuration * m_fallingDuration * 80.0f;
                    gApplication->log(CString("Fall damage = %1").arg(damage));
                    addLife(-damage);
                }

                m_fallingDuration = 0.0;
                m_onGround = true;
            }
        }
        else
        {
            m_onGround = false;
            m_fallingDuration += frameTime;
        }

        glm::vec3 newControllerPosition = m_controller->getPosition();

        // Copy rotation and scale of world matrix and change position
        glm::vec3 scale;
        glm::quat rotation;
        glm::vec3 position;
        glm::vec3 skew;
        glm::vec4 perspective;
        glm::decompose(getWorldMatrix(), scale, rotation, position, skew, perspective);

        glm::mat4 worldMatrix{ 1.0f };
        worldMatrix = glm::translate(worldMatrix, glm::vec3{ newControllerPosition.x, newControllerPosition.y, newControllerPosition.z });
        worldMatrix = worldMatrix * glm::mat4_cast(rotation);
        worldMatrix = glm::scale(worldMatrix, scale);
        updateWorldMatrixByPhysic(worldMatrix);
    }
}


void CPlayer::notifyWorldMatrixChange()
{
    ICharacter::notifyWorldMatrixChange();

    if (m_controller != nullptr)
    {
        const auto newWorldPosition = getWorldPosition();
        const auto controllerPosition = m_controller->getPosition();

        if (std::abs(controllerPosition.x - newWorldPosition.x) > 0.005f ||
            std::abs(controllerPosition.y - newWorldPosition.y) > 0.005f ||
            std::abs(controllerPosition.z - newWorldPosition.z) > 0.005f)
        {
            m_controller->setPosition(newWorldPosition);
        }
    }

    //checkInteractions(); // D�j� appell� dans la m�thode update
}


void CPlayer::updateViewDirection(const glm::quat& direction)
{
    ICharacter::updateViewDirection(direction);
    if (m_currentWeapon != nullptr)
    {
        m_currentWeapon->setOrientation(direction);
    }
}


void CPlayer::checkInteractions()
{
    m_interaction.entity = nullptr;

    // Lancement d'un rayon dans la direction d'observation du joueur
    const auto position = m_camera->getWorldPosition();
    const glm::quat orientation = m_camera->getWorldOrientation();
    const glm::vec3 direction = glm::vec3{ 1.0f, 0.0f, 0.0f } *orientation;

    CScene * scene = getScene();
    CRaycastResult result;
    // TODO: ajouter une distance maximale pour v�rifier les interractions
    if (scene != nullptr && scene->raycast(position, direction, result))
    {
        if (result.actor != nullptr)
        {
            IEntity * entity = static_cast<IEntity *>(result.actor->getEntity());

            if (entity != nullptr)
            {
                entity->checkInteraction(this, result.distance);
            }
        }
    }
}


void CPlayer::setInteraction(IEntity * entity, TKey key, const CString& message)
{
    assert(entity != nullptr);
    m_interaction.entity = entity;
    m_interaction.key = key;
    m_interaction.message = message;
}


bool CPlayer::getInteraction(TInteraction& interaction) const noexcept
{
    if (m_interaction.entity == nullptr)
    {
        return false;
    }
    else
    {
        interaction = m_interaction;
        return true;
    }
}

} // Namespace TE
