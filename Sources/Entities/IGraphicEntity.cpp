/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Entities/IGraphicEntity.hpp"
#include "Graphic/CShaderManager.hpp"
#include "Graphic/CRenderParams.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

IGraphicEntity::IGraphicEntity(IEntity * parent, const CString& name) :
    IEntity    { parent, name },
    m_shader   { nullptr },
    m_hasMoved { true }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

IGraphicEntity::IGraphicEntity(CMap * map, const CString& name) :
    IEntity    { map, name },
    m_shader   { nullptr },
    m_hasMoved { true }
{

}


/**
 * Destructeur.
 ******************************/

IGraphicEntity::~IGraphicEntity() = default;


/**
 * Modifie le shader utilis� par l'entit�.
 *
 * \param shader Shader.
 ******************************/

void IGraphicEntity::setShader(const std::shared_ptr<CShader>& shader)
{
    m_shader = shader;
}


void IGraphicEntity::notifyWorldMatrixChange()
{
    IEntity::notifyWorldMatrixChange();
    m_hasMoved = true;
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool IGraphicEntity::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("IGraphicEntity::loadFromJson()", ILogger::Debug);

    if (!IEntity::loadFromJson(json))
    {
        return false;
    }

    if (json.contains("shader"))
    {
        const auto attribute = json["shader"];
        if (!attribute.is_object())
        {
            gApplication->log("Entity attribute \"shader\" should be an object.", ILogger::Error);
            return false;
        }

        CString vertFile;
        CString fragFile;

        if (attribute.contains("vertex"))
        {
            auto attrVertex = attribute["vertex"];
            if (!attrVertex.is_string())
            {
                gApplication->log("Entity attribute \"shader.vertex\" should be a string.", ILogger::Error);
                return false;
            }

            vertFile = attrVertex.get<std::string>().c_str();
        }

        if (attribute.contains("fragment"))
        {
            auto attrFrag = attribute["fragment"];
            if (!attrFrag.is_string())
            {
                gApplication->log("Entity attribute \"fragment.vertex\" should be a string.", ILogger::Error);
                return false;
            }

            fragFile = attrFrag.get<std::string>().c_str();
        }

        if (!vertFile.isEmpty() && !fragFile.isEmpty())
        {
            m_shader = gShaderManager->loadFromFiles(vertFile, fragFile);
            //m_shader = std::make_shared<CShader>();
            //m_shader->loadFromFiles("../../../Resources/shaders/" + vertFile, "../../../Resources/shaders/" + fragFile);
        }
        else if (!vertFile.isEmpty())
        {
            m_shader = gShaderManager->loadVertexShaderFromFile(vertFile);
            //m_shader = std::make_shared<CShader>();
            //m_shader->loadFromFiles("../../../Resources/shaders/" + vertFile, "");
        }
        else if (!fragFile.isEmpty())
        {
            m_shader = gShaderManager->loadFragmentShaderFromFile(fragFile);
            //m_shader = std::make_shared<CShader>();
            //m_shader->loadFromFiles("", "../../../Resources/shaders/" + fragFile);
        }
        else
        {
            gApplication->log("Entity attribute \"shader\" must contains keys \"vertex\" or \"fragment\".", ILogger::Error);
            return false;
        }

        if (attribute.contains("bindless_texture"))
        {
            auto attr = attribute["bindless_texture"];
            if (!attr.is_boolean())
            {
                gApplication->log("Entity attribute \"fragment.bindless_texture\" should be a boolean.", ILogger::Error);
                return false;
            }

            bool bindlessTexture = attr;
            if (bindlessTexture && gRenderer->support(TFunctionnality::BindlessTexture) && m_shader != nullptr)
            {
                m_shader->setBindlessTexture(true);
            }
        }
    }

    return true;
}

} // Namespace TE
