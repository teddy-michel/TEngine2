/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Entities/CModelEntity.hpp"
#include "Game/IModelData.hpp"
#include "Graphic/CRenderParams.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param modelData Pointeur sur les données du modèle.
 * \param parent Pointeur sur l'entité parent (non nul).
 * \param name Nom de l'entité.
 ******************************/

CModelEntity::CModelEntity(IModelData * modelData, IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_model        { std::make_unique<CModel>(modelData) }
{

}


/**
 * Constructeur.
 *
 * \param modelData Pointeur sur les données du modèle.
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entité.
 ******************************/

CModelEntity::CModelEntity(IModelData * modelData, CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_model        { std::make_unique<CModel>(modelData) }
{

}


/**
 * Donne la masse du modèle.
 *
 * \return Masse du modèle en kilogrammes.
 *
 * \sa CModelEntity::setMass
 ******************************/

float CModelEntity::getMass() const
{
    return m_model->getMass();
}


/**
 * Indique si l'objet est mobile.
 *
 * \return Booléen indiquant si l'objet est mobile.
 *
 * \sa CModel::setMovable
 ******************************/

bool CModelEntity::isMovable() const
{
    return m_model->isMovable();
}


/**
 * Donne le numéro de la séquence actuellement utilisée.
 *
 * \return Numéro de la séquence.
 *
 * \sa CModelEntity::setSequence
 ******************************/

unsigned int CModelEntity::getSequence() const
{
    return m_model->getSequence();
}


/**
 * Donne le mode de lecture des séquences.
 *
 * \return Mode de lecture des séquences.
 *
 * \sa CModelEntity::setSeqMode
 ******************************/

CModel::TModelSeqMode CModelEntity::getSeqMode() const
{
    return m_model->getSeqMode();
}


/**
 * Donne le numéro du skin utilisé.
 *
 * \return Numéro du skin.
 *
 * \sa CModelEntity::setSkin
 ******************************/

unsigned int CModelEntity::getSkin() const
{
    return m_model->getSkin();
}


/**
 * Donne le groupe utilisé par le modèle.
 *
 * \return Numéro du groupe.
 *
 * \sa CModelEntity::setGroup
 ******************************/

unsigned int CModelEntity::getGroup() const
{
    return m_model->getGroup();
}


/**
 * Donne la valeur d'un contrôleur.
 *
 * \param controller Numéro du contrôleur.
 * \return Valeur du contrôleur.
 *
 * \sa CModelEntity::setControllerValue
 ******************************/

float CModelEntity::getControllerValue(unsigned int controller) const
{
    return m_model->getControllerValue(controller);
}


/**
 * Donne la valeur d'un blender.
 *
 * \param blender Numéro du blender.
 * \return Valeur du blender.
 *
 * \sa CModelEntity::setBlendingValue
 ******************************/

float CModelEntity::getBlendingValue(unsigned int blender) const
{
    return m_model->getBlendingValue(blender);
}


/**
 * Modifie la masse du modèle.
 *
 * \param mass Masse du modèle en kilogrammes.
 *
 * \sa CModelEntity::getMass
 ******************************/

void CModelEntity::setMass(float mass)
{
    m_model->setMass(mass);
}


/**
 * Rend le modèle mobile ou immobile.
 *
 * \param movable Booléen indiquant si l'objet est mobile.
 *
 * \sa CModelEntity::isMovable
 ******************************/

void CModelEntity::setMovable(bool movable)
{
    m_model->setMovable(movable);
}


/**
 * Modifie le numéro de la séquence à utiliser.
 * Si le numéro est trop grand, la première séquence est utilisée.
 *
 * \param sequence Numéro de la séquence à utiliser.
 *
 * \sa CModelEntity::getSequence
 ******************************/

void CModelEntity::setSequence(unsigned int sequence)
{
    m_model->setSequence(sequence);
}


/**
 * Modifie le numéro de la séquence à utiliser.
 *
 * \param name Nom de la séquence à utiliser.
 *
 * \sa CModelEntity::getSequence
 ******************************/

void CModelEntity::setSequenceByName(const CString& name)
{
    m_model->setSequenceByName(name);
}


/**
 * Modifie le mode de lecture des séquences.
 *
 * \param mode Mode de lecture des séquences.
 *
 * \sa CModelEntity::getSeqMode
 ******************************/

void CModelEntity::setSeqMode(CModel::TModelSeqMode mode)
{
    m_model->setSeqMode(mode);
}


/**
 * Modifie le numéro du skin à utiliser.
 *
 * \param skin Numéro du skin à utiliser.
 *
 * \sa CModelEntity::getSkin
 ******************************/

void CModelEntity::setSkin(unsigned int skin)
{
    m_model->setSkin(skin);
}


/**
 * Modifie le numéro du groupe à utiliser.
 *
 * \param group Numéro du groupe à utiliser.
 *
 * \sa CModelEntity::getGroup
 ******************************/

void CModelEntity::setGroup(unsigned int group)
{
    m_model->setGroup(group);
}


/**
 * Modifie la valeur d'un controlleur.
 *
 * \param controller Numéro du controlleur (entre 0 et 7).
 * \param value      Valeur du controlleur.
 *
 * \sa CModelEntity::getControllerValue
 ******************************/

void CModelEntity::setControllerValue(unsigned int controller, float value)
{
    m_model->setControllerValue(controller, value);
}


/**
 * Modifie la valeur du blending.
 *
 * \param blender Numéro du blender.
 * \param value   Valeur du blending.
 *
 * \sa CModelEntity::getBlendingValue
 ******************************/

void CModelEntity::setBlendingValue(unsigned int blender, float value)
{
    m_model->setBlendingValue(blender, value);
}


/**
 * Affiche le modèle.
 *
 * \params params Paramètres de rendu.
 ******************************/

void CModelEntity::renderEntity(CRenderParams * params)
{
    IGraphicEntity::renderEntity(params);
    m_model->render(params, getShader(), getWorldMatrix());
}


/**
 * Met à jour les données du modèle.
 *
 * \param frameTime Durée de la frame en secondes.
 ******************************/

void CModelEntity::updateEntity(float frameTime)
{
    m_model->update(frameTime);
}

} // Namespace TE
