/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/type_ptr.hpp>
#include <cassert>

#include "Entities/CTrigger.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CShapeBox.hpp"
#include "Physic/CScene.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CTrigger::CTrigger(IEntity * parent, const CString& name) :
    IEntity { parent, name },
    m_size  { 100.0f, 100.0f, 100.0f },
    m_actor { nullptr }
{
    updateShape();
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CTrigger::CTrigger(CMap * map, const CString& name) :
    IEntity { map, name },
    m_size  { 100.0f, 100.0f, 100.0f },
    m_actor { nullptr }
{
    updateShape();
}


/**
 * Destructeur.
 ******************************/

CTrigger::~CTrigger()
{
    if (m_actor != nullptr)
    {
        auto scene = getScene();
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
    }
}


/**
 * Retourne les dimensions du trigger.
 *
 * \return Dimensions du trigger.
 ******************************/

glm::vec3 CTrigger::getSize() const
{
    return m_size;
}


/**
 * Modifie les dimensions du trigger.
 *
 * \param size Dimensions du trigger.
 ******************************/

void CTrigger::setSize(const glm::vec3& size)
{
    if (size.x > 0.1f && size.y > 0.1f && size.z > 0.1f && size != m_size)
    {
        m_size = size;
        updateShape();
    }
}


/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CTrigger::updateEntity(float frameTime)
{
    // Mise-�-jour de la liste des entit�s
    for (auto& it : m_entities)
    {
        it.time += frameTime;
    }
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CTrigger::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CTrigger::loadFromJson()", ILogger::Debug);

    if (!IEntity::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    glm::vec3 size;
    T_PARSE_ENTITY_ATTR_VEC("size", size, hasAttr);
    if (hasAttr)
    {
        setSize(size);
    }

    return true;
}


void CTrigger::updateShape()
{
    auto scene = getScene();
    assert(scene != nullptr);

    // Suppression de l'ancien acteur
    if (m_actor != nullptr)
    {
        scene->removeActor(m_actor);
        m_actor = nullptr;
    }

    auto shape = std::make_shared<CShapeBox>( glm::vec3{ m_size.x, m_size.y, m_size.z } );
    shape->setTrigger(true);
    m_actor = std::make_shared<CStaticActor>(this, getWorldMatrix(), shape, getName().toCharArray());
    scene->addActor(m_actor);
}


void CTrigger::notifyWorldMatrixChange()
{
/*
    if (m_shape != nullptr)
    {
        m_shape->setGlobalPose(physx::PxTransform(physx::PxMat44(glm::value_ptr(m_worldMatrix))));
    }
*/
    if (m_actor != nullptr)
    {
        m_actor->setTransform(getWorldMatrix());
    }
}


void CTrigger::onTrigger(IEntity * entity, bool enter)
{
    assert(entity != nullptr);

    auto it = m_entities.begin();
    while (it != m_entities.end())
    {
        if (it->entity == entity)
        {
            break;
        }
        ++it;
    }

    if (enter)
    {
        if (it != m_entities.end())
        {
            gApplication->log(CString("Entity '%1' was already in trigger '%2'.").arg(entity->getName()).arg(getName()), ILogger::Warning);
            it->time = 0.0f; // reset time
        }
        else
        {
            m_entities.push_back(TTriggerEntity{ entity });
        }
    }
    else
    {
        if (it == m_entities.end())
        {
            gApplication->log(CString("Entity '%1' was not in trigger '%2'.").arg(entity->getName()).arg(getName()), ILogger::Warning);
        }
        else
        {
            m_entities.erase(it);
        }
    }
}

} // Namespace TE
