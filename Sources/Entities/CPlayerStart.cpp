/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Entities/CPlayerStart.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CPlayerStart::CPlayerStart(IEntity * parent, const CString& name) :
    IEntity { parent, name }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CPlayerStart::CPlayerStart(CMap * map, const CString& name) :
    IEntity { map, name }
{

}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CPlayerStart::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CPlayerStart::loadFromJson()", ILogger::Debug);

    if (!IEntity::loadFromJson(json))
    {
        return false;
    }

    // Nothing to do

    return true;
}

} // Namespace TE
