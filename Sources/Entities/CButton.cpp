/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Entities/CButton.hpp"
#include "Entities/CPlayer.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit้ parent (non nul).
 * \param name   Nom de l'entit้.
 ******************************/

CButton::CButton(IEntity * parent, const CString& name) :
    CBox { parent, name }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit้.
 ******************************/

CButton::CButton(CMap * map, const CString& name) :
    CBox { map, name }
{

}


bool CButton::checkInteraction(CPlayer * player, float distance)
{
    assert(player != nullptr);

    if (distance <= 200.0f)
    {
        player->setInteraction(this, TKey::Key_E, "Push the button");
        return true;
    }

    return false;
}


bool CButton::interact(CPlayer * player)
{
    assert(player != nullptr);
    gApplication->log("Player interact with the button!", ILogger::Debug);
    push();
    return true;
}


void CButton::push()
{
    gApplication->log("Button pushed", ILogger::Debug);
    // TODO: call slot m_signal_onPush;
}

} // Namespace TE
