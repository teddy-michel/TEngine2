/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cassert>

#include "Entities/CBox.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CRenderParams.hpp"

#include "Graphic/CRenderer.hpp"
#include "Graphic/CMegaBuffer.hpp"
#include "Graphic/CSubBuffer.hpp"
#include "Graphic/CBuffer.hpp"

#include "Core/CApplication.hpp"
#include "Physic/CDynamicActor.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CKinematicActor.hpp"
#include "Physic/CShapeBox.hpp"
#include "Physic/CScene.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CBox::CBox(IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_init         { false },
    m_size         { 100.0f, 100.0f, 100.0f },
    m_textureScale { 1.0f, 1.0f },
    m_buffer       { nullptr },
    m_actor        { nullptr },
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        m_textures[unit] = CTextureManager::NoTexture;
    }
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CBox::CBox(CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_init         { false },
    m_size         { 100.0f, 100.0f, 100.0f },
    m_textureScale { 1.0f, 1.0f },
    m_buffer       { nullptr },
    m_actor        { nullptr },
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        m_textures[unit] = CTextureManager::NoTexture;
    }
}


/**
 * Destructeur.
 ******************************/

CBox::~CBox()
{
    if (m_actor != nullptr)
    {
        auto scene = getScene();
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
    }
}


/**
 * Modifie les dimensions de la boite.
 *
 * \param size Dimensions de la boite.
 ******************************/

void CBox::setSize(const glm::vec3& size)
{
    if (size.x > 0.1f && size.y > 0.1f && size.z > 0.1f && size != m_size)
    {
        m_size = size;

        if (m_init)
        {
            m_hasMoved = true;
            updateBuffer();
            updateShape();
        }
    }
}


void CBox::setTextureScale(const glm::vec2& scale)
{
    if (scale.x > 0.0f && scale.y > 0.0f && scale != m_textureScale)
    {
        m_textureScale = scale;

        if (m_init)
        {
            updateBuffer();
        }
    }
}


/**
 * D�finit une texture � utiliser pour l'affichage.
 *
 * \param textureId Identifiant de la texture.
 * \param unit      Unit� de texture.
 ******************************/

void CBox::setTexture(TTextureId textureId, unsigned int unit)
{
    assert(unit < NumUnit);

    if (textureId != m_textures[unit])
    {
        m_textures[unit] = textureId;

        // Update buffer
        if (m_init)
        {
            assert(m_buffer != nullptr);
            TBufferTextureVector& textures = m_buffer->getTextures();
            assert(textures.size() == 1);
            textures[0].texture[unit] = m_textures[unit];
        }
    }
}


/**
 * D�finit si la boite est solide ou non.
 *
 * \param solid Bool�en.
 ******************************/

void CBox::setSolid(bool solid)
{
    if (solid != m_solid)
    {
        m_solid = solid;

        if (m_init)
        {
            updateShape();
        }
    }
}


/**
 * D�finit si la boite est dynamique ou non.
 *
 * \param solid Bool�en.
 ******************************/

void CBox::setDynamic(bool dynamic)
{
    if (dynamic != m_dynamic)
    {
        m_dynamic = dynamic;

        if (m_init)
        {
            updateShape();
        }
    }
}


/**
 * Modifie la masse de la boite.
 *
 * \param mass Masse de la boite en kilogrammes.
 ******************************/

void CBox::setMass(float mass)
{
    if (mass >= 0.0f && mass != m_mass)
    {
        m_mass = mass;

        if (m_init)
        {
            updateShape();
        }
    }
}


/**
 * Initialise l'entit�.
 ******************************/

void CBox::initEntity()
{
    assert(m_init == false);

    m_buffer = std::make_shared<CBuffer<TVertex3D>>();
    //auto megaBuffer = gRenderer->getMegaBuffer();
    //m_buffer = std::make_shared<CSubBuffer<TVertex3D>>(megaBuffer.get());
    m_buffer->setEntityId(getEntityId());

    TBufferTextureVector& textures = m_buffer->getTextures();
    TBufferTexture texture;
    texture.nbr = 6 * 6;
    textures.push_back(texture);

    // TODO: ne pas utiliser d'indices
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(6 * 6);

    for (unsigned int i = 0; i < 6; ++i)
    {
        indices.push_back(4 * i + 0);
        indices.push_back(4 * i + 1);
        indices.push_back(4 * i + 2);
        indices.push_back(4 * i + 0);
        indices.push_back(4 * i + 2);
        indices.push_back(4 * i + 3);
    }

    m_init = true;

    updateBuffer();
    updateShape();
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CBox::renderEntity(CRenderParams * params)
{
    IGraphicEntity::renderEntity(params);

    assert(m_buffer != nullptr);
    params->addBuffer(m_buffer.get(), getShader(), getWorldMatrix());
}


// DEBUG
void CBox::onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage)
{
    gApplication->log(CString::fromUTF8("Box touch�e (damage = %1) !").arg(damage));
    //setSize(m_size + 1.0f); // DEBUG
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CBox::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CBox::loadFromJson()", ILogger::Debug);

    if (!IGraphicEntity::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    T_PARSE_ENTITY_ATTR_BOOL("solid", m_solid, hasAttr);
    T_PARSE_ENTITY_ATTR_BOOL("dynamic", m_dynamic, hasAttr);

    float mass;
    T_PARSE_ENTITY_ATTR_NUMBER("mass", mass, hasAttr);
    if (hasAttr)
    {
        setMass(mass);
    }

    glm::vec3 size;
    T_PARSE_ENTITY_ATTR_VEC("size", size, hasAttr);
    if (hasAttr)
    {
        setSize(size);
    }

    glm::vec2 textureScale;
    T_PARSE_ENTITY_ATTR_VEC("texture_scale", textureScale, hasAttr);
    if (hasAttr)
    {
        setTextureScale(textureScale);
    }

    CString texture;
    T_PARSE_ENTITY_ATTR_STRING("texture", texture, hasAttr);
    if (hasAttr)
    {
        const auto textureId = gTextureManager->loadTexture(texture, CTextureManager::FilterBilinearMipmap);
        setTexture(textureId, 0);
        // TODO: multitexturing
    }

    return true;
}


/**
 * Met-�-jour le buffer graphique.
 ******************************/

void CBox::updateBuffer()
{
    assert(m_init);
    assert(m_buffer != nullptr);

    const float x = m_size.x / 2;
    const float y = m_size.y / 2;
    const float z = m_size.z / 2;

    std::vector<TVertex3D> vertices(6 * 4);

    vertices[4 * 0 + 0] = { {-x, -y, -z}, 0, { 0.0f,  0.0f, -1.0f}, {}, {{ 0.0f, 0.0f                         }} };
    vertices[4 * 0 + 1] = { { x, -y, -z}, 0, { 0.0f,  0.0f, -1.0f}, {}, {{ m_textureScale.x, 0.0f             }} };
    vertices[4 * 0 + 2] = { { x,  y, -z}, 0, { 0.0f,  0.0f, -1.0f}, {}, {{ m_textureScale.x, m_textureScale.y }} };
    vertices[4 * 0 + 3] = { {-x,  y, -z}, 0, { 0.0f,  0.0f, -1.0f}, {}, {{ 0.0f, m_textureScale.y             }} };

    vertices[4 * 1 + 0] = { { x,  y,  z}, 0, { 0.0f,  0.0f,  1.0f}, {}, {{ m_textureScale.x, m_textureScale.y }} };
    vertices[4 * 1 + 1] = { { x, -y,  z}, 0, { 0.0f,  0.0f,  1.0f}, {}, {{ m_textureScale.x, 0.0f             }} };
    vertices[4 * 1 + 2] = { {-x, -y,  z}, 0, { 0.0f,  0.0f,  1.0f}, {}, {{ 0.0f, 0.0f                         }} };
    vertices[4 * 1 + 3] = { {-x,  y,  z}, 0, { 0.0f,  0.0f,  1.0f}, {}, {{ 0.0f, m_textureScale.y             }} };

    vertices[4 * 2 + 0] = { {-x, -y, -z}, 0, {-1.0f,  0.0f,  0.0f}, {}, {{ 0.0f, m_textureScale.y             }} };
    vertices[4 * 2 + 1] = { {-x,  y, -z}, 0, {-1.0f,  0.0f,  0.0f}, {}, {{ m_textureScale.x, m_textureScale.y }} };
    vertices[4 * 2 + 2] = { {-x,  y,  z}, 0, {-1.0f,  0.0f,  0.0f}, {}, {{ m_textureScale.x, 0.0f             }} };
    vertices[4 * 2 + 3] = { {-x, -y,  z}, 0, {-1.0f,  0.0f,  0.0f}, {}, {{ 0.0f, 0.0f                         }} };

    vertices[4 * 3 + 0] = { { x,  y,  z}, 0, { 1.0f,  0.0f,  0.0f}, {}, {{ m_textureScale.x, 0.0f             }} };
    vertices[4 * 3 + 1] = { { x,  y, -z}, 0, { 1.0f,  0.0f,  0.0f}, {}, {{ m_textureScale.x, m_textureScale.y }} };
    vertices[4 * 3 + 2] = { { x, -y, -z}, 0, { 1.0f,  0.0f,  0.0f}, {}, {{ 0.0f, m_textureScale.y             }} };
    vertices[4 * 3 + 3] = { { x, -y,  z}, 0, { 1.0f,  0.0f,  0.0f}, {}, {{ 0.0f, 0.0f                         }} };

    vertices[4 * 4 + 0] = { { x, -y,  z}, 0, { 0.0f, -1.0f,  0.0f}, {}, {{ m_textureScale.x, 0.0f             }} };
    vertices[4 * 4 + 1] = { { x, -y, -z}, 0, { 0.0f, -1.0f,  0.0f}, {}, {{ m_textureScale.x, m_textureScale.y }} };
    vertices[4 * 4 + 2] = { {-x, -y, -z}, 0, { 0.0f, -1.0f,  0.0f}, {}, {{ 0.0f, m_textureScale.y             }} };
    vertices[4 * 4 + 3] = { {-x, -y,  z}, 0, { 0.0f, -1.0f,  0.0f}, {}, {{ 0.0f, 0.0f                         }} };

    vertices[4 * 5 + 0] = { {-x,  y, -z}, 0, { 0.0f,  1.0f,  0.0f}, {}, {{ 0.0f, m_textureScale.y             }} };
    vertices[4 * 5 + 1] = { { x,  y, -z}, 0, { 0.0f,  1.0f,  0.0f}, {}, {{ m_textureScale.x, m_textureScale.y }} };
    vertices[4 * 5 + 2] = { { x,  y,  z}, 0, { 0.0f,  1.0f,  0.0f}, {}, {{ m_textureScale.x, 0.0f             }} };
    vertices[4 * 5 + 3] = { {-x,  y,  z}, 0, { 0.0f,  1.0f,  0.0f}, {}, {{ 0.0f, 0.0f                         }} };

    // Textures
    TBufferTextureVector& textures = m_buffer->getTextures();
    assert(textures.size() == 1);
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        textures[0].texture[unit] = m_textures[unit];
    }

    m_buffer->setData(std::move(vertices));
    m_buffer->update();
}


/**
 * Met-�-jour le volume physique de la boite.
 ******************************/

void CBox::updateShape()
{
    assert(m_init);

    auto scene = getScene();

    // Suppression de l'ancien volume physique
    if (m_actor != nullptr)
    {
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
        m_actor = nullptr;
    }

    // Cr�ation du volume physique
    if (m_solid)
    {
        std::shared_ptr<CShapeBox> shape = std::make_shared<CShapeBox>(glm::vec3{ m_size.x, m_size.y, m_size.z });

        if (m_dynamic)
        {
            m_actor = std::make_shared<CDynamicActor>(this, getWorldMatrix(), shape, m_mass, getName().toCharArray());
        }
        else
        {
            m_actor = std::make_shared<CKinematicActor>(this, getWorldMatrix(), shape, getName().toCharArray());
        }

        if (m_actor != nullptr)
        {
            if (scene != nullptr)
            {
                scene->addActor(m_actor);
            }
        }
    }
}


void CBox::notifyWorldMatrixChange()
{
    IGraphicEntity::notifyWorldMatrixChange();

    if (m_actor != nullptr)
    {
        m_actor->setTransform(getWorldMatrix());
        // TODO: use setKinematicTarget()
    }
}

} // Namespace TE
