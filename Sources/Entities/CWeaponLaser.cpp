/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/CWeaponLaser.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CCamera.hpp"
#include "Physic/CScene.hpp"
#include "Physic/CRaycastResult.hpp"
#include "Physic/IActor.hpp"
#include "Core/CApplication.hpp"
#include "Graphic/CRenderParams.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CWeaponLaser::CWeaponLaser(IEntity * parent, const CString& name) :
	IWeapon     { parent, name },
	m_direction { 0.0f, 0.0f, 0.0f }
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CWeaponLaser::CWeaponLaser(CMap * map, const CString& name) :
	IWeapon     { map, name },
	m_direction { 0.0f, 0.0f, 0.0f }
{

}


/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CWeaponLaser::updateEntity(float frameTime)
{
    if (isFireActive())
    {
	    gApplication->log("L'arme est en train de tirer.");
	    assert(m_player != nullptr);

	    // Lancement d'un rayon dans la direction d'observation du joueur
	    auto camera = m_player->getCamera();
	    assert(camera != nullptr);
	    auto position = camera->getWorldPosition();
        const glm::quat orientation = camera->getWorldOrientation();
        const glm::vec3 direction = glm::vec3{ 1.0f, 0.0f, 0.0f } * orientation;

        CScene * scene = getScene();
        CRaycastResult result;
        if (scene != nullptr && scene->raycast(position, direction, result))
        {
            gApplication->log(CString::fromUTF8("Intersection found with actor 0x%1 (distance = %2, position = %3 x %4 x %5).").arg(CString::fromPointer(result.actor)).arg(result.distance).arg(result.position.x).arg(result.position.y).arg(result.position.z));

            if (result.actor != nullptr)
		    {
			    IEntity * entity = static_cast<IEntity *>(result.actor->getEntity());

			    if (entity != nullptr)
			    {
				    // Calcul des d�g�ts (entre 0 et 20, jusqu'� 100m de distance)
				    constexpr float max_distance = 10000.0f;
				    float damage = 0.0f;
				    if (result.distance < max_distance)
				    {
					    damage = 20.0f * (max_distance - result.distance) / max_distance;
				    }

				    entity->onFireTouch(
                        this,
                        m_player,
                        result.position,
                        result.normal,
                        result.distance,
                        damage
				    );
			    }
		    }

		    m_direction = result.position;
        }
		else
		{
			// Aucun impact, on prend la direction de la cam�ra � 1000m
			m_direction = position + direction * 100000.0f;
		}
	}
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CWeaponLaser::renderEntity(CRenderParams * params)
{
	if (isFireActive())
	{
		// TODO: afficher une ligne color�e de getPosition() � m_direction
	}

	if (m_player != nullptr && m_player->getCamera() == params->getCamera())
	{
		// TODO: affichage du mod�le � la premi�re personne
	}
	else
	{
		// TODO: affichage du mod�le � la troisi�me personne
	}
}

} // Namespace TE
