/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <cmath>

#include "Entities/CCameraVehicle.hpp"
#include "Physic/CScene.hpp"
#include "Physic/CRaycastResult.hpp"
#include "Core/Events.hpp"
#include "Core/utils.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CCameraVehicle::CCameraVehicle(IEntity * parent, const CString& name) :
    CCamera               { parent, name },
    mLastCarPos           { 0.0f },
    mLastCarVelocity      { 0.0f },
    mCameraInit           { false },
    mRotateInputY         { 0.0f },
    mRotateInputZ         { 0.0f },
    mMaxCameraRotateSpeed { 5.0f },  // <= param
    mCameraRotateAngleY   { 0.0f },
    mCameraRotateAngleZ   { 0.13f }, // <= param
    mCamDist              { 5.0f }   // <= param
{

}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CCameraVehicle::CCameraVehicle(CMap * map, const CString& name) :
    CCamera               { map, name },
    mLastCarPos           { 0.0f },
    mLastCarVelocity      { 0.0f },
    mCameraInit           { false },
    mRotateInputY         { 0.0f },
    mRotateInputZ         { 0.0f },
    mMaxCameraRotateSpeed { 5.0f },  // <= param
    mCameraRotateAngleY   { 0.0f },
    mCameraRotateAngleZ   { 0.13f }, // <= param
    mCamDist              { 5.0f }   // <= param
{

}


static void dampVec3(const glm::vec3& oldPosition, glm::vec3& newPosition, float timestep)
{
    float t = 0.7f * timestep * 8.0f;
    t = std::min(t, 1.0f);
    newPosition = oldPosition * (1.0f - t) + newPosition * t;
}


/**
 * Met-�-jour l'entit�.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CCameraVehicle::updateEntity(float frameTime)
{
    if (m_parent == nullptr)
    {
        return;
    }

    //const PxRigidDynamic * actor = focusVehicle.getRigidDynamicActor();

    //PxSceneReadLock scopedLock(scene);

    glm::vec3 actorPosition = m_parent->getWorldPosition();
    glm::quat actorOrientation = m_parent->getWorldOrientation();

    float camDist = 5.0f;  // <= param
    float cameraYRotExtra = 0.0f;

    const glm::vec3 velocity = mLastCarPos - actorPosition;

    if (mCameraInit)
    {
        const glm::vec3 carDirection = glm::rotate(actorOrientation, glm::vec3{ 0.0f, 0.0f, 1.0f });
        const glm::vec3 carSideDirection = glm::rotate(actorOrientation, glm::vec3{ 0.0f, 1.0f, 0.0f });

        //Acceleration (note that is not scaled by time).
        const glm::vec3 acclVec = mLastCarVelocity - velocity;

        //Higher forward accelerations allow the car to speed away from the camera.
        const float velZ = std::abs(glm::dot(carDirection, velocity));
        camDist = std::min(std::max(camDist + velZ * 100.0f, 10.0f), 15.0f); // <= param ?

        //Higher sideways accelerations allow the car's rotation to speed away from the camera's rotation.
        const float acclX = glm::dot(carSideDirection, acclVec);
        cameraYRotExtra = -acclX * 10.0f;

        //At very small sideways speeds the camera greatly amplifies any numeric error in the body and leads to a slight jitter.
        //Scale cameraYRotExtra by a value in range (0,1) for side speeds in range (0.1,1.0) and by zero for side speeds less than 0.1.
        const float velX = std::abs(glm::dot(carSideDirection, velocity));
        if (velX <= 0.1f * frameTime)
        {
            cameraYRotExtra = 0.0f;
        }
        else
        {
            cameraYRotExtra *= ((velX - 0.1f * frameTime) / (0.9f * frameTime));
        }
    }

    mCameraRotateAngleY += mRotateInputY * mMaxCameraRotateSpeed * frameTime;
    // why 10 * pi ???
    mCameraRotateAngleY = (mCameraRotateAngleY - 10 * Pi >= 0.0f ? mCameraRotateAngleY - 10 * Pi : (-mCameraRotateAngleY - 10 * Pi >= 0.0f ? mCameraRotateAngleY + 10 * Pi : mCameraRotateAngleY));
    mCameraRotateAngleZ += mRotateInputZ * mMaxCameraRotateSpeed * frameTime;
    mCameraRotateAngleZ = std::min(Pi * 0.45f, std::max(-Pi * 0.05f, mCameraRotateAngleZ));  // <= param 0.05

    glm::vec3 cameraDir = glm::vec3(0.0f, 0.0f, 1.0f) * std::cos(mCameraRotateAngleY + cameraYRotExtra) + glm::vec3(1.0f, 0.0f, 0.0f) * std::sin(mCameraRotateAngleY + cameraYRotExtra);
    cameraDir = cameraDir * std::cos(mCameraRotateAngleZ) - glm::vec3(0.0f, 1.0f, 0.0f) * std::sin(mCameraRotateAngleZ);

    const glm::vec3 direction = glm::rotate(actorOrientation, cameraDir);
    glm::vec3 target = actorPosition;
    target.y += 0.5f;  // <= param ?

    CScene * scene = getScene();
    CRaycastResult result;
    if (scene != nullptr && scene->raycast(target, -direction, result))
    {
        camDist = result.distance - 0.25f;  // <= param ?
    }

    camDist = std::max(4.0f, std::min(camDist, 50.0f));  // <= params
    mCamDist += std::max(camDist - mCamDist, 0.0f) * 0.05f + std::min(0.0f, camDist - mCamDist);  // <= param .05
    glm::vec3 position = target - direction * mCamDist;

    if (mCameraInit)
    {
        dampVec3(getLocalPosition(), position, frameTime);
        //dampVec3(m_direction, target, frameTime); // TODO
    }

    setPosition(position);
    //setDirection(target); // TODO
    mCameraInit = true;

    mLastCarVelocity = velocity;
    mLastCarPos = actorPosition;
}

} // Namespace TE
