/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cassert>

#include "Entities/CSphere.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Core/CApplication.hpp"
#include "Core/utils.hpp"
#include "Physic/CDynamicActor.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CKinematicActor.hpp"
#include "Physic/CShapeSphere.hpp"
#include "Physic/CScene.hpp"

#include "Entities/CPlayer.hpp" // DEBUG


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CSphere::CSphere(IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_init         { false },
    m_radius       { 50.0f },
    m_slices       { 16 },
    m_rings        { 16 },
    m_buffer       { nullptr },
    m_actor        { nullptr },
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        m_textures[unit] = CTextureManager::NoTexture;
    }

    updateShape();
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CSphere::CSphere(CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_init         { false },
    m_radius       { 50.0f },
    m_slices       { 16 },
    m_rings        { 16 },
    m_buffer       { nullptr },
    m_actor        { nullptr },
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        m_textures[unit] = CTextureManager::NoTexture;
    }

    updateShape();
}


/**
 * Destructeur.
 ******************************/

CSphere::~CSphere()
{
    if (m_actor != nullptr)
    {
        auto scene = getScene();
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
    }
}


/**
 * Modifie les dimensions de la sph�re.
 *
 * \param size Dimensions de la sph�re.
 ******************************/

void CSphere::setRadius(float radius)
{
    if (radius > 0.0f && radius != m_radius)
    {
        m_radius = radius;
        m_hasMoved = true;
        updateBuffer();
    }
}


/**
 * D�finit la texture � utiliser pour l'affichage.
 *
 * \param textureId Identifiant de la texture.
 * \param unit      Unit� de texture.
 ******************************/

void CSphere::setTexture(TTextureId textureId, unsigned int unit)
{
    assert(unit < NumUnit);
    m_textures[unit] = textureId;

    // Update buffer
    if (m_init && m_buffer != nullptr)
    {
        //const unsigned int faces = (m_slices - 1) * (m_rings - 1); // \todo: remove this line
        TBufferTextureVector& textures = m_buffer->getTextures();
        textures[0].texture[unit] = m_textures[unit];
    }
}


/**
 * D�finit si la sph�re est solide ou non.
 *
 * \param solid Bool�en.
 ******************************/

void CSphere::setSolid(bool solid)
{
    if (solid != m_solid)
    {
        m_solid = solid;
        updateShape();
    }
}


/**
 * D�finit si la sph�re est dynamique ou non.
 *
 * \param solid Bool�en.
 ******************************/

void CSphere::setDynamic(bool dynamic)
{
    if (dynamic != m_dynamic)
    {
        m_dynamic = dynamic;
        updateShape();
    }
}


/**
 * Modifie la masse de la sph�re.
 *
 * \param mass Masse de la sph�re en kilogrammes.
 ******************************/

void CSphere::setMass(float mass)
{
    if (mass >= 0.0f && mass != m_mass)
    {
        m_mass = mass;
        updateShape();
    }
}


/**
 * Modifie la vitesse lin�aire de l'objet.
 *
 * \param velocity Vitesse lin�aire.
 ******************************/

void CSphere::setLinearVelocity(const glm::vec3& velocity)
{
    if (m_actor != nullptr)
    {
        m_actor->setLinearVelocity(velocity);
    }
}


/**
 * Initialise l'entit�.
 ******************************/

void CSphere::initEntity()
{
    m_buffer = std::make_unique<CBuffer<TVertex3D>>();
    m_buffer->setEntityId(getEntityId());

    const unsigned int faces = (m_slices - 1) * (m_rings - 1);

    TBufferTextureVector& textures = m_buffer->getTextures();
    TBufferTexture texture;
    texture.nbr = 6 * faces;
    textures.push_back(texture);

    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.resize(6 * faces);
    
    unsigned int idx = 0;

    for (unsigned int r = 0; r < m_rings - 1; ++r)
    {
        unsigned int ringStart = r * m_slices;
        unsigned int nextRingStart = (r + 1) * m_slices;

        for (unsigned int s = 0; s < m_slices - 1; ++s)
        {
            unsigned int nextslice = s + 1;

            // The quad
            indices[idx+0] = ringStart + s;
            indices[idx+1] = nextRingStart + s;
            indices[idx+2] = nextRingStart + nextslice;
            //gApplication->log(CString("CSphere::initEntity - add indices %1, %2, %3").arg(indices[idx+0]).arg(indices[idx+1]).arg(indices[idx+2]), ILogger::Debug);

            indices[idx+3] = ringStart + s;
            indices[idx+4] = nextRingStart + nextslice;
            indices[idx+5] = ringStart + nextslice;
            //gApplication->log(CString("CSphere::initEntity - add indices %1, %2, %3").arg(indices[idx+3]).arg(indices[idx+4]).arg(indices[idx+5]), ILogger::Debug);

            idx += 6;
        }
    }

    m_init = true;

    updateBuffer();
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CSphere::renderEntity(CRenderParams* params)
{
    IGraphicEntity::renderEntity(params);

    if (m_buffer != nullptr)
    {
        params->addBuffer(m_buffer.get(), getShader(), getWorldMatrix());
    }
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CSphere::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CSphere::loadFromJson()", ILogger::Debug);

    if (!IGraphicEntity::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    T_PARSE_ENTITY_ATTR_BOOL("solid", m_solid, hasAttr);
    T_PARSE_ENTITY_ATTR_BOOL("dynamic", m_dynamic, hasAttr);
    T_PARSE_ENTITY_ATTR_NUMBER("mass", m_mass, hasAttr);
    T_PARSE_ENTITY_ATTR_NUMBER("radius", m_radius, hasAttr);

    CString texture;
    T_PARSE_ENTITY_ATTR_STRING("texture", texture, hasAttr);
    if (hasAttr)
    {
        m_textures[0] = gTextureManager->loadTexture(texture, CTextureManager::FilterBilinearMipmap);
        // TODO: multitexturing
    }

    updateShape();

    return true;
}

/*
// DEBUG
void CSphere::onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage)
{
    gApplication->log(CString::fromUTF8("CSphere::onFireTouch - damage = %1").arg(damage));
    setRadius(m_radius + 1.0f); // DEBUG
}


// DEBUG
bool CSphere::checkInteraction(CPlayer * player, float distance)
{
    assert(player != nullptr);

    if (distance <= 200.0f)
    {
        player->setInteraction(this, TKey::Key_E, "Test interaction!");
        return true;
    }

    return false;
}


// DEBUG
bool CSphere::interact(CPlayer * player)
{
    assert(player != nullptr);
    gApplication->log("Player interact with sphere!");
    return false;
}
*/

/**
 * Met-�-jour le buffer graphique.
 ******************************/

void CSphere::updateBuffer()
{
    if (m_init && m_buffer != nullptr)
    {
        std::vector<TVertex3D> vertices(m_slices * m_rings);

        const float R = 1.0f / static_cast<float>(m_rings - 1);
        const float S = 1.0f / static_cast<float>(m_slices - 1);
        unsigned int index = 0;

        for (unsigned int r = 0; r < m_rings; ++r)
        {
            for (unsigned int s = 0; s < m_slices; ++s)
            {
                const float tmp1 = R * r;
                const float tmp2 = S * s;

                const float x = std::cos(2 * Pi * s * S) * std::sin(Pi * r * R);
                const float y = std::sin(2 * Pi * s * S) * std::sin(Pi * r * R);
                const float z = std::sin(Pi * r * R - Pi / 2);
/*
                float tmp3 = pi * tmp1;
                float tmp4 = sin(tmp3);
                float tmp5 = pi * 2.0f * tmp2;

                float x = cos(tmp5) * tmp4;
                float y = sin(tmp5) * tmp4;
                float z = sin(-pi * 0.5f + (tmp3));
*/
                //gApplication->log(CString("CSphere::updateSize - add vertex %1 x %2 x %3").arg(x).arg(y).arg(z), ILogger::Debug);

                vertices[index].position = { x * m_radius, y * m_radius, z * m_radius };
                vertices[index].normal = { x, y, z };
                vertices[index].coords[0] = { tmp2, tmp1 };

                ++index;
            }
        }
        
        // Textures
        TBufferTextureVector& textures = m_buffer->getTextures();
        for (unsigned int unit = 0; unit < NumUnit; ++unit)
        {
            textures[0].texture[unit] = m_textures[unit];
        }

        m_buffer->setData(std::move(vertices));
        m_buffer->update();
    }

    updateShape();
}


/**
 * Met-�-jour le volume physique de la sph�re.
 ******************************/

void CSphere::updateShape()
{
    auto scene = getScene();

    // Suppression de l'ancien volume physique
    if (m_actor != nullptr)
    {
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
        m_actor = nullptr;
    }

    // Cr�ation du volume physique
    if (m_solid)
    {
        std::shared_ptr<CShapeSphere> shape = std::make_shared<CShapeSphere>(m_radius);

        if (m_dynamic)
        {
            m_actor = std::make_shared<CDynamicActor>(this, getWorldMatrix(), shape, m_mass, getName().toCharArray());
        }
        else
        {
            m_actor = std::make_shared<CKinematicActor>(this, getWorldMatrix(), shape, getName().toCharArray());
        }

        if (m_actor != nullptr)
        {
            if (scene != nullptr)
            {
                scene->addActor(m_actor);
            }
        }
    }
}


void CSphere::notifyWorldMatrixChange()
{
    IGraphicEntity::notifyWorldMatrixChange();

    if (m_actor != nullptr)
    {
        m_actor->setTransform(getWorldMatrix());
        // TODO: use setKinematicTarget()
    }
}

} // Namespace TE
