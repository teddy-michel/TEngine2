/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/IEntity.hpp"
#include "Entities/CCamera.hpp"
#include "Entities/CMapEntity.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name Nom de l'entit�.
 ******************************/

IEntity::IEntity(IEntity * parent, const CString& name) :
    m_localMatrix { 1.0f },
    m_parent      { parent },
    m_children    {},
    m_map         { m_parent->m_map },
    m_name        { name },
    m_id          { gEntityManager->addEntity(this) }, // TODO: call m_map->addEntity() to store entities in CMap
    m_fixParent   { false }
{
    assert(m_parent != nullptr);
    assert(m_map != nullptr);

    m_parent->m_children.push_back(this);

    gEntityManager->setWorldMatrix(m_id, m_parent->getWorldMatrix());

    gApplication->log(CString("Create entity '%1' (0x%2).").arg(m_name).arg(CString::fromPointer(this)), ILogger::Debug);
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

IEntity::IEntity(CMap * map, const CString& name) :
    m_localMatrix { 1.0f },
    m_parent      { nullptr },
    m_children    {},
    m_map         { map },
    m_name        { name },
    m_id          { gEntityManager->addEntity(this) }, // TODO: call m_map->addEntity() to store entities in CMap
    m_fixParent   { false }
{
    assert(m_map != nullptr);

    gApplication->log(CString("Create entity '%1' (0x%2).").arg(m_name).arg(CString::fromPointer(this)), ILogger::Debug);
}


/**
 * Destructeur.
 * Supprime les entit�s enfants r�cursivement.
 ******************************/

IEntity::~IEntity()
{
    gApplication->log(CString("Delete entity '%1' (0x%2).").arg(m_name).arg(CString::fromPointer(this)), ILogger::Debug);

    // TODO: use shared_ptr
    while (m_children.size() > 0)
    {
        delete m_children.front();
    }

    if (m_parent != nullptr)
    {
        m_parent->m_children.remove(this);
    }

    gEntityManager->removeEntity(this);
}


/**
 * Retourne la sc�ne contenant l'entit�.
 *
 * \return Sc�ne.
 ******************************/

CScene * IEntity::getScene() const
{
    assert(m_map != nullptr);
    return m_map->getScene();
}


/**
 * Retourne l'entit� racine du graphe de sc�ne (le plus lointain anc�tre).
 * Si l'entit� n'a pas de parent, elle est son propre anc�tre.
 *
 * \return Pointeur sur l'entit� racine (non nul).
 ******************************/

IEntity * IEntity::getRoot()
{
    if (m_parent != nullptr)
    {
        return m_parent->getRoot();
    }

    return this;
}


/**
 * Ajoute un enfant � une entit�.
 *
 * \param entity Pointeur sur l'entit� � ajouter comme enfant (non nul).
 ******************************/

bool IEntity::addChild(IEntity * entity)
{
    assert(entity != nullptr);
    assert(entity->m_map == m_map);

    if (this == entity)
    {
        return false;
    }

    return entity->setParent(this);
}


/**
 * Modifie l'entit� parent.
 *
 * \param entity Pointeur sur l'entit� parent.
 * \return True si l'entit� a pu �tre d�finie comme parent, false sinon.
 ******************************/

bool IEntity::setParent(IEntity * entity)
{
    if (m_fixParent || this == entity || m_parent == entity)
    {
        return false;
    }

    if (isAncestorOf(entity))
    {
        return false;
    }

    if (m_parent != nullptr)
    {
        m_parent->m_children.remove(entity);
    }

    m_parent = entity;

    if (entity != nullptr)
    {
        assert(entity->m_map == m_map);
        entity->m_children.push_back(this);
        updateWorldMatrix(entity->getWorldMatrix());
    }

    return true;
}


/**
 * Indique si l'entit� en param�tre est mon anc�tre.
 *
 * \param entity Entit� � tester.
 * \return True ou false.
 ******************************/

bool IEntity::isAncestorOf(IEntity * entity) const
{
    if (entity == nullptr)
    {
        return false;
    }

    if (entity == this || m_parent == entity)
    {
        return true;
    }

    if (m_parent != nullptr && m_parent->isAncestorOf(entity))
    {
        return true;
    }

    return false;
}


void IEntity::setFixParent(bool fix)
{
    m_fixParent = fix;
}


glm::vec3 IEntity::getWorldPosition() const
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(getWorldMatrix(), scale, rotation, translation, skew, perspective);
    return translation;
}


glm::vec3 IEntity::getLocalPosition() const
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, rotation, translation, skew, perspective);
    return translation;
}


glm::quat IEntity::getWorldOrientation() const
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(getWorldMatrix(), scale, rotation, translation, skew, perspective);
    return glm::conjugate(rotation);
}


glm::quat IEntity::getLocalOrientation() const
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, rotation, translation, skew, perspective);
    return glm::conjugate(rotation);
}


glm::vec3 IEntity::getWorldScale() const
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(getWorldMatrix(), scale, rotation, translation, skew, perspective);
    return scale;
}


glm::vec3 IEntity::getLocalScale() const
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, rotation, translation, skew, perspective);
    return scale;
}


/**
 * D�finit la matrice de transformation globale et calcule la matrice locale en cons�quence.
 *
 * \param worldMatrix Nouvelle matrice globale.
 ******************************/

void IEntity::setWorldMatrix(const glm::mat4& worldMatrix)
{
    gEntityManager->setWorldMatrix(m_id, worldMatrix);
    notifyWorldMatrixChange();

    if (m_parent != nullptr)
    {
        m_localMatrix = glm::inverse(m_parent->getWorldMatrix()) * worldMatrix;
    }
    else
    {
        m_localMatrix = worldMatrix;
    }

    notifyLocalMatrixChange();

    for (auto& child : m_children)
    {
        child->updateWorldMatrix(worldMatrix, true);
    }
}


/**
 * D�finit la matrice de transformation locale.
 *
 * \param localMatrix Nouvelle matrice locale.
 ******************************/

void IEntity::setLocalMatrix(const glm::mat4& localMatrix)
{
    m_localMatrix = localMatrix;
    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


/**
 * Modifie la position locale de l'entit�.
 *
 * \param position Nouvelle position locale.
 ******************************/

void IEntity::setPosition(const glm::vec3& position)
{
    glm::vec3 scale;
    glm::quat orientation;
    glm::vec3 old_position;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, orientation, old_position, skew, perspective);

    // Copy rotation and scale and change position
    m_localMatrix = glm::mat4{ 1.0f };
    m_localMatrix[0][0] = scale.x;
    m_localMatrix[1][1] = scale.y;
    m_localMatrix[2][2] = scale.z;
    m_localMatrix[3][0] = position.x;
    m_localMatrix[3][1] = position.y;
    m_localMatrix[3][2] = position.z;
    //m_localMatrix = glm::scale(m_localMatrix, scale);
    //m_localMatrix = glm::translate(m_localMatrix, position);
    m_localMatrix = m_localMatrix * glm::mat4_cast(orientation);

    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


void IEntity::translate(const glm::vec3& translation)
{
    m_localMatrix = glm::translate(m_localMatrix, translation);
    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


void IEntity::setOrientation(const glm::quat& orientation)
{
    glm::vec3 scale;
    glm::quat old_orientation;
    glm::vec3 position;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, old_orientation, position, skew, perspective);

    // Copy translation and scale and change rotation
    m_localMatrix = glm::mat4{ 1.0f };
    m_localMatrix[0][0] = scale.x;
    m_localMatrix[1][1] = scale.y;
    m_localMatrix[2][2] = scale.z;
    m_localMatrix[3][0] = position.x;
    m_localMatrix[3][1] = position.y;
    m_localMatrix[3][2] = position.z;
    //m_localMatrix = glm::scale(m_localMatrix, scale);
    //m_localMatrix = glm::translate(m_localMatrix, position);
    m_localMatrix = m_localMatrix * glm::mat4_cast(orientation);

    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


/**
 * Applique une rotation � l'entit�.
 *
 * \param rotation Quaternion repr�sentant la rotation � appliquer.
 ******************************/

void IEntity::rotate(const glm::quat& rotation)
{
    m_localMatrix = m_localMatrix * glm::mat4_cast(rotation);
    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


/**
 * Modifie la valeur du facteur d'agrandissement de l'entit�.
 *
 * \param scale Facteur d'agrandissement.
 ******************************/

void IEntity::setScale(const glm::vec3& scale)
{
    glm::vec3 old_scale;
    glm::quat orientation;
    glm::vec3 position;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, old_scale, orientation, position, skew, perspective);

    // Copy rotation and position and change scale
    m_localMatrix = glm::mat4{ 1.0f };
    m_localMatrix[0][0] = scale.x;
    m_localMatrix[1][1] = scale.y;
    m_localMatrix[2][2] = scale.z;
    m_localMatrix[3][0] = position.x;
    m_localMatrix[3][1] = position.y;
    m_localMatrix[3][2] = position.z;
    //m_localMatrix = glm::scale(m_localMatrix, scale);
    //m_localMatrix = glm::translate(m_localMatrix, position);
    m_localMatrix = m_localMatrix * glm::mat4_cast(orientation);

    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


/**
 * Change le facteur d'agrandissement de l'entit�.
 *
 * \param scale Facteur d'agrandissement relatif.
 ******************************/

void IEntity::scale(const glm::vec3& scale)
{
    m_localMatrix = glm::scale(m_localMatrix, scale);
    notifyLocalMatrixChange();
    updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
}


/**
 * Dessine l'entit� et ses enfants.
 * Cette m�thode peut �tre appell�e plusieurs fois par frame. Il ne doit donc pas y avoir de
 * mise-�-jour des param�tres de l'entit� ici (utiliser la m�thode updateEntity pour cela).
 *
 * \params params Param�tres de rendu.
 ******************************/

void IEntity::render(CRenderParams * params)
{
    renderEntity(params);
    renderChildren(params);
}


/**
 * Dessine l'entit�.
 * Cette m�thode peut �tre appell�e plusieurs fois par frame. Il ne doit donc pas y avoir de
 * mise-�-jour des param�tres de l'entit� ici (utiliser la m�thode updateEntity pour cela).
 *
 * \params params Param�tres de rendu.
 ******************************/

void IEntity::renderEntity(CRenderParams * params)
{
    T_UNUSED(params);
}


/**
 * Dessine les enfants de l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void IEntity::renderChildren(CRenderParams * params)
{
    for (auto& child : m_children)
    {
        child->render(params);
    }
}


// TODO: rename to init()
void IEntity::initEntities()
{
    initEntity();
    for (auto& child : m_children)
    {
        child->initEntities();
    }
}


/**
 * Initialise l'entit�.
 * Cette m�thode est appell�e une fois que la map a �t� charg�e et avant le premier affichage.
 ******************************/

void IEntity::initEntity()
{

}


/**
 * Met-�-jour les entit�s r�cursivement.
 * M�thode appell�e exactement une fois � chaque frame dans la boucle principale de l'application.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void IEntity::update(float frameTime)
{
    updateEntity(frameTime);
    for (auto& child : m_children)
    {
        child->update(frameTime);
    }
}


/**
 * Met-�-jour l'entit�.
 * M�thode appell�e exactement une fois � chaque frame.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void IEntity::updateEntity(float frameTime)
{
    T_UNUSED(frameTime);
}


void IEntity::onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage)
{
    T_UNUSED(weapon);
    T_UNUSED(player);
    T_UNUSED(position);
    T_UNUSED(normale);
    T_UNUSED(damage);
}


/**
 * Cherche s'il y a une interraction entre le joueur et cette entit�.
 *
 * \param player Pointeur sur le joueur (non nul).
 * \param distance Distance entre le joueur et le point d'interraction.
 * \param interaction Structure contenant les param�tres de l'interraction.
 * \return True en cas d'interraction possible, false sinon.
 ******************************/

bool IEntity::checkInteraction(CPlayer * player, float distance)
{
    assert(player != nullptr);
    return false;
}


/**
 * M�thode appell�e quand un joueur veut interragir avec l'entit�.
 *
 * \param player Joueur qui cherche � interragir (non nul).
 * \return True si l'interraction est possible, false sinon.
 ******************************/

bool IEntity::interact(CPlayer * player)
{
    assert(player != nullptr);
    return false;
}


void IEntity::updateWorldMatrix(const glm::mat4& parentWorldMatrix, bool notify)
{
    const glm::mat4 worldMatrix = parentWorldMatrix * m_localMatrix;
    gEntityManager->setWorldMatrix(m_id, worldMatrix);

    if (notify)
    {
        notifyWorldMatrixChange();
    }

    for (auto& child : m_children)
    {
        child->updateWorldMatrix(worldMatrix, notify);
    }
}


void IEntity::updateWorldMatrixByPhysic(const glm::mat4& worldMatrix)
{
    gEntityManager->setWorldMatrix(m_id, worldMatrix);

    if (m_parent != nullptr)
    {
        m_localMatrix = glm::inverse(m_parent->getWorldMatrix()) * worldMatrix;
    }
    else
    {
        m_localMatrix = worldMatrix;
    }

    for (auto& child : m_children)
    {
        child->updateWorldMatrix(worldMatrix, false);
    }
}


void IEntity::notifyLocalMatrixChange()
{

}


void IEntity::notifyWorldMatrixChange()
{

}


bool IEntity::parseJsonPoint(const nlohmann::json& json, glm::vec3& point)
{
    assert(json.is_object());

    if (!json.contains("x") || !json.contains("y") || !json.contains("z"))
    {
        return false;
    }

    const auto x = json["x"];
    if (!x.is_number())
    {
        return false;
    }

    const auto y = json["y"];
    if (!y.is_number())
    {
        return false;
    }

    const auto z = json["z"];
    if (!z.is_number())
    {
        return false;
    }

    point.x = x;
    point.y = y;
    point.z = z;

    return true;
}


bool IEntity::parseJsonPoint(const nlohmann::json& json, glm::vec2& point)
{
    assert(json.is_object());

    if (!json.contains("x") || !json.contains("y"))
    {
        return false;
    }

    const auto x = json["x"];
    if (!x.is_number())
    {
        return false;
    }

    const auto y = json["y"];
    if (!y.is_number())
    {
        return false;
    }

    point.x = x;
    point.y = y;

    return true;
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool IEntity::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());
    assert(json.contains("type"));

    gApplication->log("IEntity::loadFromJson()", ILogger::Debug);

    if (json.contains("name"))
    {
        const auto attribute = json["name"];
        if (!attribute.is_string())
        {
            gApplication->log("Entity attribute \"type\" should be a string.", ILogger::Error);
            return false;
        }

        m_name = attribute.get<std::string>().c_str();
        // TODO: update name in CMap list of entities
        //m_map->renameEntity(this);
    }

    bool hasAttr;
    bool localMatrixChanged = false;

    glm::vec3 position;
    T_PARSE_ENTITY_ATTR_VEC("position", position, hasAttr);
    if (hasAttr)
    {
        //setPosition(position);
        m_localMatrix = glm::translate(m_localMatrix, position);
        localMatrixChanged = true;
    }

    glm::vec3 orientation;
    T_PARSE_ENTITY_ATTR_VEC("orientation", orientation, hasAttr);
    if (hasAttr)
    {
        //setOrientation(orientation);
        m_localMatrix *= glm::mat4_cast(glm::angleAxis(glm::radians(orientation.x), glm::vec3{ 1.0f, 0.0f, 0.0f }));
        m_localMatrix *= glm::mat4_cast(glm::angleAxis(glm::radians(orientation.y), glm::vec3{ 0.0f, 1.0f, 0.0f }));
        m_localMatrix *= glm::mat4_cast(glm::angleAxis(glm::radians(orientation.z), glm::vec3{ 0.0f, 0.0f, 1.0f }));
        localMatrixChanged = true;
    }

    if (localMatrixChanged)
    {
        notifyLocalMatrixChange();
        updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
    }

    return true;
}

} // Namespace TE
