/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Entities/CMapEntity.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CPlayerStart.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Construit une entit� CMapEntity.
 *
 * \param map  Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CMapEntity::CMapEntity(CMap * map, const CString& name) :
    IEntity   { map, name }
{
    setFixParent(true);
}


/**
 * Ajoute un joueur � la map.
 * Le joueur est ajout� � la liste des enfants de la map.
 *
 * Il est positionn� sur la premi�re entit� CPlayerStart de la map.
 * S'il n'y a pas d'entit�s CPlayerStart parmi les enfants, le joueur est positionn� � l'origine de la map.
 *
 * Cette m�thode peut �tre red�finie dans les classes d�riv�es (par exemple pour g�rer les joueurs de plusieurs �quipes).
 *
 * \param player Pointeur sur l'entit� CPlayer � ajouter � la map (non nul).
 ******************************/

void CMapEntity::addPlayer(CPlayer * player)
{
    assert(player != nullptr);

    // Recherche de la premi�re entit� CPlayerStart parmi les enfants
    for (const auto& child : m_children)
    {
        CPlayerStart * start = dynamic_cast<CPlayerStart *>(child);
        if (start != nullptr)
        {
            // Copie de la matrice locale de transformation vers le joueur
            player->setLocalMatrix(start->getLocalMatrix());
            break;
        }
    }

    addChild(player);
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CMapEntity::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CMapEntity::loadFromJson()", ILogger::Debug);

    if (!IEntity::loadFromJson(json))
    {
        return false;
    }

    // Nothing to do

    return true;
}

} // Namespace TE
