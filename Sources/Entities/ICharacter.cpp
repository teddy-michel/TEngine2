/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Entities/ICharacter.hpp"
#include "Entities/CCamera.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CShaderManager.hpp"
#include "Core/CApplication.hpp"
#include "Core/utils.hpp"
#include "Entities/CBox.hpp" // DEBUG


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

ICharacter::ICharacter(IEntity * parent, const CString& name) :
    IEntity    { parent, name },
    m_theta    { 0.0f },
    m_phi      { 0.0f },
    m_camera   { nullptr },
    m_life     { 100.0f },
    m_height   { 180.0f },
    m_radius   { 30.0f },
    m_meshBody { nullptr },
    m_meshHead { nullptr }
{
    init();
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

ICharacter::ICharacter(CMap * map, const CString& name) :
    IEntity    { map, name },
    m_theta    { 0.0f },
    m_phi      { 0.0f },
    m_camera   { nullptr },
    m_life     { 100.0f },
    m_height   { 180.0f },
    m_radius   { 30.0f },
    m_meshBody { nullptr },
    m_meshHead { nullptr }
{
    init();
}


/**
 * Destructeur.
 ******************************/

ICharacter::~ICharacter()
{
    //m_camera.reset();
    //m_meshBody.reset();
    //m_meshHead.reset();
}


/**
 * Retourne la cam�ra associ�e au personnage.
 *
 * \return Pointeur sur la cam�ra associ�e au personnage.
 ******************************/

CCamera * ICharacter::getCamera() const noexcept
{
    return m_camera;
}


/**
 * Modifie le niveau de vie du personnage.
 *
 * \param life Niveau de vie souhait�.
 * \return Nouveau niveau de vie du personnage.
 ******************************/

float ICharacter::setLife(float life) noexcept
{
    m_life = life;
    return m_life;
}


/**
 * Augmente le niveau de vie du personnage.
 *
 * \param life Valeur � ajouter au niveau de vie.
 * \return Nouveau niveau de vie du personnage.
 ******************************/

float ICharacter::addLife(float life)
{
    m_life += life;
    return m_life;
}


/**
 * Modifie la hauteur du personnage.
 *
 * \param height Nouvelle hauteur.
 ******************************/

void ICharacter::setHeight(float height)
{
    if (height < 0.0f)
    {
        gApplication->log("Player height can't be negative.", ILogger::Warning);
        m_height = 0.0f;
    }
    else
    {
        m_height = height;
    }

    m_camera->setPosition({ 0.2f, 0.0f, m_height / 2 - 0.1f }); // DEBUG: d�placement en X
}


/**
 * Retourne la hauteur du personnage.
 *
 * \return Hauteur du personnage.
 ******************************/

float ICharacter::getHeight() const noexcept
{
    return m_height;
}


/**
 * Modifie le rayon du volume englobant du personnage.
 *
 * \param radius Rayon du volume englobant du personnage.
 ******************************/

void ICharacter::setRadius(float radius)
{
    if (radius < 0.0f)
    {
        gApplication->log("Character radius can't be negative.", ILogger::Warning);
        m_radius = 0.0f;
    }
    else
    {
        m_radius = radius;
    }
}


/**
 * Retourne le rayon du volume englobant du personnage.
 *
 * \return Rayon du volume englobant du personnage.
 ******************************/

float ICharacter::getRadius() const noexcept
{
    return m_radius;
}


void ICharacter::renderChildren(CRenderParams * params)
{
    if (params->getCamera() == m_camera)
    {
        for (auto& child : m_children)
        {
            if (child != m_meshBody && child != m_meshHead)
            {
                child->render(params);
            }
        }
    }
    else
    {
        IEntity::renderChildren(params);
    }
}


void ICharacter::onFireTouch(IWeapon * weapon, CPlayer * player, const glm::vec3& position, const glm::vec3& normale, float distance, float damage)
{
    gApplication->log(CString::fromUTF8("ICharacter::onFireTouch - damage = %1").arg(damage));
    addLife(-damage);
}


void ICharacter::init()
{
    float headSize = m_radius; // taille de la t�te du bonhomme et de son �paisseur
    float headSpace = 10.0f; // espace entre la t�te et le corps

    std::shared_ptr<CShader> shader;

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        shader = gShaderManager->loadFromFiles("texturing_bt.vert", "texturing_bt.frag");
        //auto shader = std::make_shared<CShader>();
        //shader->loadFromFiles("../../../Resources/shaders/texturing_bt.vert", "../../../Resources/shaders/texturing_bt.frag");

        if (shader != nullptr)
        {
            shader->setBindlessTexture(true);
        }
    }
    else
    {
        shader = gShaderManager->loadFromFiles("texturing.vert", "texturing.frag");
        //auto shader = std::make_shared<CShader>();
        //shader->loadFromFiles("../../../Resources/shaders/texturing.vert", "../../../Resources/shaders/texturing.frag");
    }

    // DEBUG: affichage d'une boite
    m_meshBody = new CBox { this, "__player_body" };
    m_meshBody->setShader(shader);
    m_meshBody->setSolid(false);
    m_meshBody->setSize({ headSize, m_radius * 2, (m_height - headSize - headSpace) });
    m_meshBody->setTexture(gTextureManager->loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap));
    m_meshBody->translate({ 0.0f, 0.0f, -(headSize + headSpace) / 2 });

    m_meshHead = new CBox { this, "__player_head" };
    m_meshHead->setShader(shader);
    m_meshHead->setSolid(false);
    m_meshHead->setSize({ headSize, headSize, headSize });
    m_meshHead->setTexture(gTextureManager->loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap));
    m_meshHead->translate({ 0.0f, 0.0f, (m_height - headSize) / 2 });

    m_camera = new CCamera { this, "__player_camera" };
    m_camera->setFixParent(true);
    //m_camera->translate({ 0.0f, 0.0f, m_height / 2 - 0.1f });
    updateCameraDirection(false);

    //m_camera->translate({ 0.0f, 0.0f, m_height / 2 - 10.0f }); // TODO: param�triser
    m_camera->translate({ headSize / 2, 0.0f, (m_height - headSize) / 2 }); // DEBUG: d�placement en X
}


void ICharacter::notifyLocalMatrixChange()
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, rotation, translation, skew, perspective);

    auto angles = glm::eulerAngles(rotation);
    //m_phi = -angles.y; // DEBUG: camera free fly
    m_theta = angles.z;

    updateCameraDirection(false);
}


void ICharacter::updateCameraDirection(bool needUpdateWorldMatrix)
{
    // Check angles values
    if (m_phi > 89.0f * Pi / 180.0f)
    {
        m_phi = 89.0f * Pi / 180.0f;
    }
    else if (m_phi < -89.0f * Pi / 180.0f)
    {
        m_phi = -89.0f * Pi / 180.0f;
    }

    while (m_theta < -Pi)
    {
        m_theta += 2 * Pi;
    }

    while (m_theta > Pi)
    {
        m_theta -= 2 * Pi;
    }

    // Update camera direction
    const glm::quat orientation = glm::angleAxis(-m_phi, glm::vec3{ 0.0f, 1.0f, 0.0f });
    updateViewDirection(orientation);

    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_localMatrix, scale, rotation, translation, skew, perspective);

    // Copy translation and scale and change rotation
    m_localMatrix = glm::mat4{ 1.0f };
    m_localMatrix[0][0] = scale.x;
    m_localMatrix[1][1] = scale.y;
    m_localMatrix[2][2] = scale.z;
    m_localMatrix[3][0] = translation.x;
    m_localMatrix[3][1] = translation.y;
    m_localMatrix[3][2] = translation.z;
    //m_localMatrix = glm::mat4{ 1.0f };
    //m_localMatrix = glm::scale(m_localMatrix, scale);
    //m_localMatrix = glm::translate(m_localMatrix, translation);
    m_localMatrix = m_localMatrix * glm::mat4_cast(glm::angleAxis(m_theta, glm::vec3{ 0.0f, 0.0f, 1.0f }));
    //m_localMatrix = m_localMatrix * glm::mat4_cast(glm::angleAxis(-m_phi, glm::vec3{ 0.0f, 1.0f, 0.0f })); // DEBUG: camera free fly

    if (needUpdateWorldMatrix)
    {
        updateWorldMatrix(m_parent != nullptr ? m_parent->getWorldMatrix() : glm::mat4{ 1.0f });
    }
}


void ICharacter::changeViewAngles(float theta, float phi)
{
    m_theta += theta;
    m_phi += phi;
    updateCameraDirection(true);
}


void ICharacter::updateViewDirection(const glm::quat& direction)
{
    m_camera->setOrientation(direction);

    // DEBUG: changement de l'orientation de la t�te
    if (m_meshHead != nullptr)
    {
        m_meshHead->setOrientation(direction);
    }
}

} // Namespace TE
