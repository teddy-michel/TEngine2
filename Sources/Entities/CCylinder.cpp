/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cassert>

#include "Entities/CCylinder.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Core/CApplication.hpp"
#include "Core/utils.hpp"
#include "Physic/CDynamicActor.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CKinematicActor.hpp"
#include "Physic/CShapeCylinder.hpp"
#include "Physic/CScene.hpp"


namespace TE
{

constexpr int NumFacesInCylinder = 16;


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CCylinder::CCylinder(IEntity * parent, const CString& name) :
    IGraphicEntity { parent, name },
    m_init         { false },
    m_radius       { 50.0f },
	m_length       { 100.0f },
    m_buffer       { nullptr },
    m_actor        { nullptr },
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        m_textures[unit] = CTextureManager::NoTexture;
    }
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CCylinder::CCylinder(CMap * map, const CString& name) :
    IGraphicEntity { map, name },
    m_init         { false },
    m_radius       { 50.0f },
	m_length       { 100.0f },
    m_buffer       { nullptr },
    m_actor        { nullptr },
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        m_textures[unit] = CTextureManager::NoTexture;
    }
}


/**
 * Destructeur.
 ******************************/

CCylinder::~CCylinder()
{
    if (m_actor != nullptr)
    {
        auto scene = getScene();
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
    }
}


/**
 * Modifie le rayon du cylindre.
 *
 * \param radius Rayon du cylindre.
 ******************************/

void CCylinder::setRadius(float radius)
{
    if (radius > 0.1f && radius != m_radius)
    {
        m_radius = radius;

        if (m_init)
        {
            m_hasMoved = true;
            updateBuffer();
            updateShape();
        }
    }
}


/**
 * Modifie la longueur du cylindre.
 *
 * \param length Longueur du cylindre.
 ******************************/

void CCylinder::setLength(float length)
{
	if (length > 0.1f && length != m_length)
	{
		m_length = length;

        if (m_init)
        {
            m_hasMoved = true;
            updateBuffer();
            updateShape();
        }
	}
}


/**
 * D�finit une texture � utiliser pour l'affichage.
 *
 * \param textureId Identifiant de la texture.
 * \param unit      Unit� de texture.
 ******************************/

void CCylinder::setTexture(TTextureId textureId, unsigned int unit)
{
    assert(unit < NumUnit);

    if (textureId != m_textures[unit])
    {
        m_textures[unit] = textureId;

        // Update buffer
        if (m_init)
        {
            assert(m_buffer != nullptr);
            TBufferTextureVector& textures = m_buffer->getTextures();
            assert(textures.size() == 1);
            textures[0].texture[unit] = m_textures[unit];
        }
    }
}


/**
 * D�finit si le cylindre est solide ou non.
 *
 * \param solid Bool�en.
 ******************************/

void CCylinder::setSolid(bool solid)
{
    if (solid != m_solid)
    {
        m_solid = solid;

        if (m_init)
        {
            updateShape();
        }
    }
}


/**
 * D�finit si le cylindre est dynamique ou non.
 *
 * \param solid Bool�en.
 ******************************/

void CCylinder::setDynamic(bool dynamic)
{
    if (dynamic != m_dynamic)
    {
        m_dynamic = dynamic;

        if (m_init)
        {
            updateShape();
        }
    }
}


/**
 * Modifie la masse du cylindre.
 *
 * \param mass Masse du cylindre en kilogrammes.
 ******************************/

void CCylinder::setMass(float mass)
{
    if (mass >= 0.0f && mass != m_mass)
    {
        m_mass = mass;

        if (m_init)
        {
            updateShape();
        }
    }
}


/**
 * Initialise l'entit�.
 ******************************/

void CCylinder::initEntity()
{
    assert(m_init == false);

    m_buffer = std::make_unique<CBuffer<TVertex3D>>();
    m_buffer->setEntityId(getEntityId());

	TBufferTextureVector& textures = m_buffer->getTextures();
	TBufferTexture texture;
	texture.nbr = 12 * NumFacesInCylinder; // 4 triangles pour chaque face
	textures.push_back(texture);

	std::vector<unsigned int>& indices = m_buffer->getIndices();
	indices.reserve(12 * NumFacesInCylinder);

	for (int face = 0; face < 10 * NumFacesInCylinder; face += 10) // 10 sommets pour chaque face
	{
		indices.push_back(face + 0);
		indices.push_back(face + 1);
		indices.push_back(face + 2);

		indices.push_back(face + 0);
		indices.push_back(face + 2);
		indices.push_back(face + 3);

        indices.push_back(face + 4);
        indices.push_back(face + 5);
        indices.push_back(face + 6);

        indices.push_back(face + 7);
        indices.push_back(face + 8);
        indices.push_back(face + 9);
	}

    m_init = true;

    updateBuffer();
    updateShape();
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CCylinder::renderEntity(CRenderParams * params)
{
    IGraphicEntity::renderEntity(params);

    assert(m_buffer != nullptr);
    params->addBuffer(m_buffer.get(), getShader(), getWorldMatrix());
}


/**
 * Initialise l'entit� � partir d'un objet JSON.
 *
 * \param json Objet JSON.
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CCylinder::loadFromJson(const nlohmann::json& json)
{
    assert(json.is_object());

    gApplication->log("CCylinder::loadFromJson()", ILogger::Debug);

    if (!IGraphicEntity::loadFromJson(json))
    {
        return false;
    }

    bool hasAttr;

    T_PARSE_ENTITY_ATTR_BOOL("solid", m_solid, hasAttr);
    T_PARSE_ENTITY_ATTR_BOOL("dynamic", m_dynamic, hasAttr);

    float mass;
    T_PARSE_ENTITY_ATTR_NUMBER("mass", mass, hasAttr);
    if (hasAttr)
    {
        setMass(mass);
    }

    float radius;
    T_PARSE_ENTITY_ATTR_NUMBER("radius", radius, hasAttr);
    if (hasAttr)
    {
        setRadius(radius);
    }

    float length;
    T_PARSE_ENTITY_ATTR_NUMBER("length", length, hasAttr);
    if (hasAttr)
    {
        setLength(length);
    }

    CString texture;
    T_PARSE_ENTITY_ATTR_STRING("texture", texture, hasAttr);
    if (hasAttr)
    {
        const auto textureId = gTextureManager->loadTexture(texture, CTextureManager::FilterBilinearMipmap);
        setTexture(textureId, 0);
        // TODO: multitexturing
    }

    return true;
}


/**
 * Fonction appell�e lorsque la taille du cylindre change.
 * Met-�-jour le buffer graphique.
 ******************************/

void CCylinder::updateBuffer()
{
    assert(m_init);
    assert(m_buffer != nullptr);

    std::vector<TVertex3D> vertices(10 * NumFacesInCylinder); // 10 sommets pour chaque face

    constexpr float invFaces = 1.0f / static_cast<float>(NumFacesInCylinder);
    constexpr float dtheta = 2 * Pi / static_cast<float>(NumFacesInCylinder);

	for (int face = 0; face < NumFacesInCylinder; ++face)
	{
        const float theta1 = dtheta * face;
        const float cosTheta1 = std::cos(theta1);
        const float sinTheta1 = std::sin(theta1);

        const float theta2 = dtheta * (face + 1);
        const float cosTheta2 = std::cos(theta2);
        const float sinTheta2 = std::sin(theta2);

        // face
        vertices[10 * face + 0].position.x = m_radius * cosTheta1; // position
        vertices[10 * face + 0].position.y = m_radius * sinTheta1;
        vertices[10 * face + 0].position.z = -0.5f * m_length;
        vertices[10 * face + 0].normal.x = cosTheta1; // normale
        vertices[10 * face + 0].normal.y = sinTheta1;
        vertices[10 * face + 0].normal.z = 0.0f;
        vertices[10 * face + 0].coords[0].x = invFaces * face; // textcoords
        vertices[10 * face + 0].coords[0].y = 0.0f;

        vertices[10 * face + 1].position.x = m_radius * cosTheta1; // position
        vertices[10 * face + 1].position.y = m_radius * sinTheta1;
        vertices[10 * face + 1].position.z = 0.5f * m_length;
        vertices[10 * face + 1].normal.x = cosTheta1; // normale
        vertices[10 * face + 1].normal.y = sinTheta1;
        vertices[10 * face + 1].normal.z = 0.0f;
        vertices[10 * face + 1].coords[0].x = invFaces * face; // textcoords
        vertices[10 * face + 1].coords[0].y = 1.0f;

        vertices[10 * face + 2].position.x = m_radius * cosTheta2; // position
        vertices[10 * face + 2].position.y = m_radius * sinTheta2;
        vertices[10 * face + 2].position.z = 0.5f * m_length;
        vertices[10 * face + 2].normal.x = cosTheta2; // normale
        vertices[10 * face + 2].normal.y = sinTheta2;
        vertices[10 * face + 2].normal.z = 0.0f;
        vertices[10 * face + 2].coords[0].x = invFaces * (face + 1); // textcoords
        vertices[10 * face + 2].coords[0].y = 1.0f;

        vertices[10 * face + 3].position.x = m_radius * cosTheta2; // position
        vertices[10 * face + 3].position.y = m_radius * sinTheta2;
        vertices[10 * face + 3].position.z = -0.5f * m_length;
        vertices[10 * face + 3].normal.x = cosTheta2; // normale
        vertices[10 * face + 3].normal.y = sinTheta2;
        vertices[10 * face + 3].normal.z = 0.0f;
        vertices[10 * face + 3].coords[0].x = invFaces * (face + 1); // textcoords
        vertices[10 * face + 3].coords[0].y = 0.0f;

        // bottom
        vertices[10 * face + 4].position.x = m_radius * cosTheta1; // position
        vertices[10 * face + 4].position.y = m_radius * sinTheta1;
        vertices[10 * face + 4].position.z = -0.5f * m_length;
        vertices[10 * face + 4].normal.x = 0.0f; // normale
        vertices[10 * face + 4].normal.y = 0.0f;
        vertices[10 * face + 4].normal.z = -1.0f;
        vertices[10 * face + 4].coords[0].x = 0.0f; // textcoords
        vertices[10 * face + 4].coords[0].y = 0.0f;

        vertices[10 * face + 5].position.x = m_radius * cosTheta2; // position
        vertices[10 * face + 5].position.y = m_radius * sinTheta2;
        vertices[10 * face + 5].position.z = -0.5f * m_length;
        vertices[10 * face + 5].normal.x = 0.0f; // normale
        vertices[10 * face + 5].normal.y = 0.0f;
        vertices[10 * face + 5].normal.z = -1.0f;
        vertices[10 * face + 5].coords[0].x = 0.0f; // textcoords
        vertices[10 * face + 5].coords[0].y = 0.0f;

        vertices[10 * face + 6].position.x = 0.0f; // position
        vertices[10 * face + 6].position.y = 0.0f;
        vertices[10 * face + 6].position.z = -0.5f * m_length;
        vertices[10 * face + 6].normal.x = 0.0f; // normale
        vertices[10 * face + 6].normal.y = 0.0f;
        vertices[10 * face + 6].normal.z = -1.0f;
        vertices[10 * face + 6].coords[0].x = 0.0f; // textcoords
        vertices[10 * face + 6].coords[0].y = 0.0f;

        // top
        vertices[10 * face + 7].position.x = m_radius * cosTheta1; // position
        vertices[10 * face + 7].position.y = m_radius * sinTheta1;
        vertices[10 * face + 7].position.z = 0.5f * m_length;
        vertices[10 * face + 7].normal.x = 0.0f; // normale
        vertices[10 * face + 7].normal.y = 0.0f;
        vertices[10 * face + 7].normal.z = 1.0f;
        vertices[10 * face + 7].coords[0].x = 0.0f; // textcoords
        vertices[10 * face + 7].coords[0].y = 0.0f;

        vertices[10 * face + 8].position.x = 0.0f; // position
        vertices[10 * face + 8].position.y = 0.0f;
        vertices[10 * face + 8].position.z = 0.5f * m_length;
        vertices[10 * face + 8].normal.x = 0.0f; // normale
        vertices[10 * face + 8].normal.y = 0.0f;
        vertices[10 * face + 8].normal.z = 1.0f;
        vertices[10 * face + 8].coords[0].x = 0.0f; // textcoords
        vertices[10 * face + 8].coords[0].y = 0.0f;

        vertices[10 * face + 9].position.x = m_radius * cosTheta2; // position
        vertices[10 * face + 9].position.y = m_radius * sinTheta2;
        vertices[10 * face + 9].position.z = 0.5f * m_length;
        vertices[10 * face + 9].normal.x = 0.0f; // normale
        vertices[10 * face + 9].normal.y = 0.0f;
        vertices[10 * face + 9].normal.z = 1.0f;
        vertices[10 * face + 9].coords[0].x = 0.0f; // textcoords
        vertices[10 * face + 9].coords[0].y = 0.0f;
	}

    // Textures
    TBufferTextureVector& textures = m_buffer->getTextures();
    for (unsigned int unit = 0; unit < NumUnit; ++unit)
    {
        textures[0].texture[unit] = m_textures[unit];
    }

    m_buffer->setData(std::move(vertices));
    m_buffer->update();
}


void CCylinder::updateShape()
{
    assert(m_init);

    auto scene = getScene();

    // Suppression de l'ancien volume physique
    if (m_actor != nullptr)
    {
        if (scene != nullptr)
        {
            scene->removeActor(m_actor);
        }
        m_actor = nullptr;
    }

    // Cr�ation du volume physique
    if (m_solid)
    {
        std::shared_ptr<CShapeCylinder> shape = std::make_shared<CShapeCylinder>(m_radius, m_length);

        if (m_dynamic)
        {
            m_actor = std::make_shared<CDynamicActor>(this, getWorldMatrix(), shape, m_mass, getName().toCharArray());
        }
        else
        {
            m_actor = std::make_shared<CKinematicActor>(this, getWorldMatrix(), shape, getName().toCharArray());
        }

        if (m_actor != nullptr)
        {
            if (scene != nullptr)
            {
                scene->addActor(m_actor);
            }
        }
    }
}


void CCylinder::notifyWorldMatrixChange()
{
    IGraphicEntity::notifyWorldMatrixChange();

    if (m_actor != nullptr)
    {
        m_actor->setTransform(getWorldMatrix());
        // TODO: use setKinematicTarget()
    }
}

} // Namespace TE
