/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/type_ptr.hpp>
#include <limits>
#include <cassert>

#include "Entities/CHeightField.hpp"
#include "Physic/CShapeHeightField.hpp"
#include "Physic/CStaticActor.hpp"
#include "Physic/CScene.hpp"
#include "Core/CApplication.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/VertexFormat.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entit� parent (non nul).
 * \param name   Nom de l'entit�.
 ******************************/

CHeightField::CHeightField(IEntity * parent, const CString& name) :
    IGraphicEntity  { parent, name },
    m_subdivisionsX { 1 },
    m_subdivisionsY { 1 },
    m_size          { 100.0f, 100.0f },
    m_heightScale   { 1.0f },
    m_actor         { nullptr },
    m_buffer        { std::make_unique<CBuffer<TVertex3D>>() }
{
    m_buffer->setEntityId(getEntityId());
}


/**
 * Constructeur.
 *
 * \param map Pointeur sur la map (non nul).
 * \param name Nom de l'entit�.
 ******************************/

CHeightField::CHeightField(CMap * map, const CString& name) :
    IGraphicEntity  { map, name },
    m_subdivisionsX { 1 },
    m_subdivisionsY { 1 },
    m_size          { 100.0f, 100.0f },
    m_heightScale   { 1.0f },
    m_actor         { nullptr },
    m_buffer        { std::make_unique<CBuffer<TVertex3D>>() }
{
    m_buffer->setEntityId(getEntityId());
}


void CHeightField::setSubdivisions(unsigned int x, unsigned int y)
{
    m_subdivisionsX = (x == 0 ? 1 : x);
    m_subdivisionsY = (y == 0 ? 1 : y);
}


void CHeightField::setSize(const glm::vec2& size)
{
    m_size = size;
}


void CHeightField::setHeightScale(float heightScale)
{
    m_heightScale = (heightScale < 0.1f ? 0.1f : heightScale);
}


//void CHeightField::setPoints(const std::vector<float>& points)
void CHeightField::setPoints(const std::vector<int16_t>& points)
{
    if (points.size() == (m_subdivisionsX + 1) * (m_subdivisionsY + 1))
    {
        m_points = points;
    }
}


void CHeightField::setTexture(TTextureId textureId, unsigned int unit)
{
    assert(unit < NumUTUsed);

    TBufferTextureVector& textinfos = m_buffer->getTextures();
    textinfos.resize(1);
    textinfos[0].texture[unit] = textureId;
}


/**
 * Dessine l'entit�.
 *
 * \params params Param�tres de rendu.
 ******************************/

void CHeightField::renderEntity(CRenderParams * params)
{
    IGraphicEntity::renderEntity(params);

    if (m_buffer != nullptr)
    {
        params->addBuffer(m_buffer.get(), getShader(), getWorldMatrix());
    }
}


/**
 * Initialise l'entit�.
 ******************************/

void CHeightField::initEntity()
{
    // Le buffer contient toujours une texture
    TBufferTextureVector textinfosCopy = m_buffer->getTextures();

    TBufferTextureVector& textinfos = m_buffer->getTextures();
    textinfosCopy[0].nbr = m_subdivisionsX * m_subdivisionsY * 6;
    textinfos.push_back(textinfosCopy[0]);

    if (m_points.size() != (m_subdivisionsX + 1) * (m_subdivisionsY + 1))
    {
        return;
    }

    const float xScale = m_size.x / m_subdivisionsX;
    const float yScale = m_size.y / m_subdivisionsY;

    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(m_subdivisionsX * m_subdivisionsY * 6);

    std::vector<TVertex3D> vertices(4 * m_subdivisionsX * m_subdivisionsY);

    unsigned int index = 0;

    for (unsigned int y = 0; y < m_subdivisionsY; ++y)
    {
        for (unsigned int x = 0; x < m_subdivisionsX; ++x)
        {
            vertices[index + 0].position.x = -m_size.x / 2 + x * xScale;
            vertices[index + 0].position.y = m_size.y / 2 - (y + 1) * yScale;
            vertices[index + 0].position.z = m_heightScale * m_points[(y + 1) * (m_subdivisionsX + 1) + x];
            vertices[index + 0].coords[0].x = 1.0f;
            vertices[index + 0].coords[0].y = 0.0f;
            vertices[index + 0].coords[1].x = 1.0f;
            vertices[index + 0].coords[1].y = 0.0f;

            vertices[index + 1].position.x = -m_size.x / 2 + x * xScale;
            vertices[index + 1].position.y = m_size.y / 2 - y * yScale;
            vertices[index + 1].position.z = m_heightScale * m_points[y * (m_subdivisionsX + 1) + x];
            vertices[index + 1].coords[0].x = 0.0f;
            vertices[index + 1].coords[0].y = 0.0f;
            vertices[index + 1].coords[1].x = 0.0f;
            vertices[index + 1].coords[1].y = 0.0f;

            vertices[index + 2].position.x = -m_size.x / 2 + (x + 1) * xScale;
            vertices[index + 2].position.y = m_size.y / 2 - y * yScale;
            vertices[index + 2].position.z = m_heightScale * m_points[y * (m_subdivisionsX + 1) + (x + 1)];
            vertices[index + 2].coords[0].x = 0.0f;
            vertices[index + 2].coords[0].y = 1.0f;
            vertices[index + 2].coords[1].x = 0.0f;
            vertices[index + 2].coords[1].y = 1.0f;

            vertices[index + 3].position.x = -m_size.x / 2 + (x + 1) * xScale;
            vertices[index + 3].position.y = m_size.y / 2 - (y + 1) * yScale;
            vertices[index + 3].position.z = m_heightScale * m_points[(y + 1) * (m_subdivisionsX + 1) + (x + 1)];
            vertices[index + 3].coords[0].x = 1.0f;
            vertices[index + 3].coords[0].y = 1.0f;
            vertices[index + 3].coords[1].x = 1.0f;
            vertices[index + 3].coords[1].y = 1.0f;

            // Calcul des normales approximatives
            vertices[index + 0].normal = glm::normalize(glm::cross(vertices[index + 0].position - vertices[index + 1].position, vertices[index + 2].position - vertices[index + 1].position));
            vertices[index + 3].normal = glm::normalize(glm::cross(vertices[index + 2].position - vertices[index + 3].position, vertices[index + 0].position - vertices[index + 3].position));
            vertices[index + 1].normal = glm::normalize(vertices[index + 0].normal + vertices[index + 3].normal);
            vertices[index + 2].normal = vertices[index + 1].normal;

            indices.push_back(index + 0);
            indices.push_back(index + 1);
            indices.push_back(index + 2);
            indices.push_back(index + 0);
            indices.push_back(index + 2);
            indices.push_back(index + 3);

            index += 4;
        }
    }

    m_buffer->setData(std::move(vertices));
    m_buffer->update();

    std::shared_ptr<CShapeHeightField> shape = std::make_shared<CShapeHeightField>(m_points, m_subdivisionsX, m_subdivisionsY, m_heightScale, xScale, yScale);

    // Inversion des axes Y et Z, et translation (PhysX consid�re que la position d'un heightfield est dans un coin et pas au centre)
    // TODO: move to PhysX impl
    auto worldMatrix = glm::translate(glm::rotate(getWorldMatrix(), glm::radians(90.0f), glm::vec3{ 1.0f, 0.0f, 0.0f }), glm::vec3{ -m_size.x / 2, 0.0f, -m_size.y / 2 });

    m_actor = std::make_shared<CStaticActor>(this, worldMatrix, shape, getName().toCharArray());

    CScene * scene = getScene();
    if (scene != nullptr)
    {
        scene->addActor(m_actor);
    }
}


void CHeightField::notifyWorldMatrixChange()
{
    if (m_actor != nullptr)
    {
        gApplication->log(CString("Moving static actor %1.").arg(getName()), ILogger::Warning);

        // Inversion des axes Y et Z, et translation (PhysX consid�re que la position d'un heightfield est dans un coin et pas au centre)
        // TODO: move to PhysX impl
        auto worldMatrix = glm::translate(glm::rotate(getWorldMatrix(), glm::radians(90.0f), glm::vec3{ 1.0f, 0.0f, 0.0f }), glm::vec3{ -m_size.x / 2, 0.0f, -m_size.y / 2 });
        m_actor->setTransform(worldMatrix);
    }
}

} // Namespace TE
