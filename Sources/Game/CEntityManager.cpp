/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>

#include "Game/CEntityManager.hpp"
#include "Game/CMap.hpp"
#include "Core/CApplication.hpp"

#include "Entities/CBox.hpp"
#include "Entities/CCylinder.hpp"
#include "Entities/CMapEntity.hpp"
#include "Entities/CPlayerStart.hpp"
#include "Entities/CPointLight.hpp"
#include "Entities/CSphere.hpp"
#include "Entities/CSpotLight.hpp"
#include "Entities/CTrigger.hpp"


namespace TE
{

CEntityManager * gEntityManager = nullptr;


/**
 * Constructeur.
 ******************************/

CEntityManager::CEntityManager() :
    m_entities           {},
    m_models             {}
{
    assert(gEntityManager == nullptr); // Singleton simple
    gEntityManager = this;

    m_entities.reserve(256);
    m_models.reserve(256);

    // L'identifiant 0 pour les entit�s est r�serv�
    m_entities.push_back(reinterpret_cast<IEntity *>(-1));
    m_models.push_back(glm::mat4{ 1.0f });
}


/**
 * Destructeur.
 ******************************/

CEntityManager::~CEntityManager()
{
    gApplication->log("CEntityManager::~CEntityManager", ILogger::Debug);
    gEntityManager = nullptr;
}


/**
 * Charge les entit�s depuis un fichier.
 *
 * \param fileName Nom du fichier JSON contenant les entit�s.
 * \param map      Pointeur sur la map (non nul).
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CEntityManager::loadFromFile(const CString& fileName, CMap * map)
{
    assert(map != nullptr);

    gApplication->log(CString("Load entities from file \"%1\"...").arg(fileName), ILogger::Debug);

    std::ifstream file{ fileName.toCharArray(), std::fstream::in | std::fstream::binary };
    if (!file)
    {
        return false;
    }

    auto map_root = nlohmann::json::parse(file, nullptr, false);

    if (map_root.is_discarded())
    {
        gApplication->log("Can't load entities (invalid JSON)", ILogger::Error);
        return false;
    }

    if (!loadEntityFromJson(map_root, nullptr, map))
    {
        gApplication->log("Can't load entities", ILogger::Error);
        return false;
    }

    return true;
}


/**
 * Charge les entit�s depuis un buffer.
 *
 * \param buffer Buffer contenant du JSON (non nul).
 * \param map    Pointeur sur la map (non nul).
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CEntityManager::loadFromBuffer(const char * buffer, CMap * map)
{
    assert(buffer != nullptr);
    assert(map != nullptr);

    auto map_root = nlohmann::json::parse(buffer, nullptr, false);

    if (map_root.is_discarded())
    {
        gApplication->log("Can't load entities (invalid JSON)", ILogger::Error);
        return false;
    }

    if (!loadEntityFromJson(map_root, nullptr, map))
    {
        gApplication->log("Can't load entities", ILogger::Error);
        return false;
    }

    return true;
}


/**
 * Charge une entit� et ses descendants depuis un objet JSON.
 *
 * \param json   Objet JSON correspondant � l'entit� � charger.
 * \param parent Pointeur sur l'entit� parent.
 * \param map    Pointeur sur la map (non nul).
 * \return True si le chargement s'est bien pass�, false en cas d'erreur.
 ******************************/

bool CEntityManager::loadEntityFromJson(const nlohmann::json& json, IEntity * parent, CMap * map)
{
    assert(map != nullptr);

    if (!json.is_object())
    {
        gApplication->log("Entity JSON must be an object", ILogger::Error);
        return false;
    }

    if (!json.contains("type"))
    {
        gApplication->log("Entity JSON must contains the key \"type\"", ILogger::Error);
        return false;
    }

    const auto t = json["type"];
    if (!t.is_string())
    {
        gApplication->log("Entity JSON attribute \"type\" must be a string", ILogger::Error);
        return false;
    }

    IEntity * entity = nullptr;

    if (parent == nullptr)
    {
        if (t != "map")
        {
            gApplication->log("Root entity must have the type \"map\"", ILogger::Error);
            return false;
        }

        entity = map->getMapEntity();
    }
    else
    {
        if (t == "box")
        {
            entity = new CBox{ parent };
        }
        else if (t == "cylinder")
        {
            entity = new CCylinder{ parent };
        }
        else if (t == "player_start")
        {
            entity = new CPlayerStart{ parent };
        }
        else if (t == "point_light")
        {
            auto pointLight = new CPointLight{ parent };
            entity = pointLight;
            map->addLight(pointLight);
        }
        else if (t == "sphere")
        {
            entity = new CSphere{ parent };
        }
        else if (t == "spot_light")
        {
            auto spotLight = new CSpotLight{ parent };
            entity = spotLight;
            map->addLight(spotLight);
        }
        else if (t == "trigger")
        {
            entity = new CTrigger{ parent };
        }
        // TODO: load other entities types
        else if (t == "map")
        {
            gApplication->log("Entity with type \"map\" is reserved for root entity", ILogger::Error);
            return false;
        }
        else if (t == "player" || t == "light" || t == "entity" || t == "graphic_entity" || t == "weapon" || t == "character")
        {
            gApplication->log(CString("Entity with type \"%1\" is forbidden").arg(t.get<std::string>().c_str()), ILogger::Error);
            return false;
        }
        else
        {
            gApplication->log(CString("Entity JSON unknown \"type\" value (\"%1\")").arg(t.get<std::string>().c_str()), ILogger::Warning);
            return true;
        }
    }

    assert(entity != nullptr);

    bool res = true;

    if (!entity->loadFromJson(json))
    {
        gApplication->log("Can't load entity from JSON", ILogger::Error);
        res = false;
    }

    if (res && json.contains("children"))
    {
        const auto children = json["children"];
        if (!children.is_array())
        {
            gApplication->log("Entity JSON attribute \"type\" must be an array", ILogger::Error);
            res = false;
        }

        // Cr�ation des entit�s enfants
        for (const auto& child : children)
        {
            if (!loadEntityFromJson(child, entity, map))
            {
                gApplication->log("Can't load child entity", ILogger::Error);
            }
        }
    }

    if (!res)
    {
        delete entity;
        return false;
    }

    return true;
}


/**
 * Ajoute une entit� au gestionnaire.
 * Si l'entit� est d�j� r�f�renc�e dans le gestionnaire, l'identifiant existant est retourn�.
 *
 * \param entity Pointeur sur l'entit� � ajouter (non nul).
 * \return Identifiant de l'entit�.
 ******************************/

TEntityId CEntityManager::addEntity(IEntity * entity)
{
    assert(entity != nullptr);

    TEntityId id = 0;
    TEntityId freeSpot = 0xFFFFFFFF;

    while (id < m_entities.size())
    {
        if (m_entities[id] == entity)
        {
            // L'entit� est d�j� dans la liste
            gApplication->log("CEntityManager::addEntity(): entity already in list", ILogger::Warning);
            return id;
        }

        if (freeSpot != 0xFFFFFFFF && m_entities[id] == nullptr)
        {
            freeSpot = id;
        }

        ++id;
    }

    if (freeSpot == 0xFFFFFFFF)
    {
        m_entities.push_back(entity);
        m_models.push_back(glm::mat4{ 1.0f });
    }
    else
    {
        id = freeSpot;
        m_entities[id] = entity;
        m_models[id] = glm::mat4{ 1.0f };
    }

    return id;
}


/**
 * Enl�ve une entit� du gestionnaire.
 *
 * \param entity Pointeur sur l'entit� � enlever (non nul).
 ******************************/

void CEntityManager::removeEntity(IEntity * entity)
{
    assert(entity != nullptr);

    TEntityId id = 0;

    while (id < m_entities.size())
    {
        if (m_entities[id] == entity)
        {
            m_entities[id] = nullptr;
            return;
        }

        ++id;
    }

    gApplication->log("CEntityManager::removeEntity(): unknown entity", ILogger::Warning);
}


/**
 * R�cup�re la matrice globale d'une entit�.
 *
 * \param id     Identifiant de l'entit�.
 * \param matrix Contiendra la matrice globale.
 ******************************/

void CEntityManager::getWorldMatrix(TEntityId id, glm::mat4& matrix) const
{
    assert(id < m_entities.size());
    matrix = m_models[id];
}


/**
 * Modifie la matrice globale d'une entit�.
 *
 * \param id     Identifiant de l'entit�.
 * \param matrix Matrice globale.
 ******************************/

void CEntityManager::setWorldMatrix(TEntityId id, const glm::mat4& matrix)
{
    assert(id < m_entities.size());
    m_models[id] = matrix;
}

} // Namespace TE
