/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/matrix_transform.hpp>

#include "Game/CGameApplication.hpp"
#include "Entities/CMapEntity.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CCamera.hpp"
#include "Entities/CCameraFreeFly.hpp"
#include "Entities/CScreen.hpp" // DEBUG
#include "Entities/CBox.hpp" // DEBUG
#include "Game/CMapLoader.hpp"
//#include "Game/CMapLoaderBSPGoldSrc.hpp" // DEBUG
//#include "Game/CActivityMenu.hpp"
//#include "Game/CActivityGame.hpp"
#include "Game/IActivity.hpp"
#include "Game/CMap.hpp"
#include "Game/CEntityManager.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp" // DEBUG
#include "Graphic/CRenderParams.hpp"
#include "Core/CLoggerFile.hpp"
#include "Core/CDateTime.hpp"


namespace TE
{

CGameApplication * gGameApplication = nullptr;


/**
 * Constructeur par d�faut.
 ******************************/

CGameApplication::CGameApplication() :
    CApplication    {},
    m_title         {},
    m_window        {},
    //m_map           { nullptr },
    //m_player        { nullptr },
    //m_camera        { nullptr },
    m_receiver      { nullptr },
    m_activity      { nullptr },
    m_activities    {},
    m_fullscreen    { false },
    m_gameActive    { false },
    m_cursorPos     { 0, 0 },
    m_cursorVisible { true },
    m_keyActions    {}
{
    assert(gGameApplication == nullptr); // Singleton simple
    gGameApplication = this;
    log("CGameApplication::CGameApplication");
}


/**
 * Destructeur.
 ******************************/

CGameApplication::~CGameApplication()
{
    log("CGameApplication::~CGameApplication");

    for (auto& activity : m_activities)
    {
        delete activity;
    }

    gGameApplication = nullptr;
}


/**
 * Modifie le titre de l'application.
 *
 * \param title Titre de l'application.
 ******************************/

void CGameApplication::setTitle(const CString& title)
{
    m_title = title;
    m_window.setTitle(m_title.toCharArray());
}

/*
// TODO: move
void CGameApplication::setMap(CMap * map)
{
    if (m_map != nullptr)
    {
        delete m_map;
        m_map = nullptr;
    }

    m_map = map;
}


CMap * CGameApplication::getMap() const
{
    return m_map;
}


void CGameApplication::setCamera(CCamera * camera)
{
    m_camera = camera;
}
*/

void CGameApplication::setReceiver(IInputReceiver * receiver)
{
    m_receiver = receiver;
}


/**
 * Donne l'action associ�e � une touche.
 *
 * \param key Touche recherch�e.
 * \return Action associ�e.
 ******************************/

TAction CGameApplication::getActionForKey(TKey key) const
{
    for (std::map<TAction, TKey>::const_iterator it = m_keyActions.begin(); it != m_keyActions.end(); ++it)
    {
        if (it->second == key)
        {
            return it->first;
        }
    }

    return TAction::ActionNone;
}


/**
 * Donne la touche associ�e � une action.
 *
 * \param action Action recherch�e.
 * \return Touche associ�e.
 ******************************/

TKey CGameApplication::getKeyForAction(TAction action) const
{
    std::map<TAction, TKey>::const_iterator it = m_keyActions.find(action);

    if (it != m_keyActions.end())
    {
        return it->second;
    }

    return TKey::Key_Unknown;
}


/**
 * Indique si la touche associ�e � une action est actuellement enfonc�e.
 *
 * \param action Action recherch�e.
 * \return Bool�en.
 ******************************/

bool CGameApplication::isActionActive(TAction action) const
{
    if (action == TAction::ActionNone)
    {
        return false;
    }

    std::map<TAction, TKey>::const_iterator it = m_keyActions.find(action);

    if (it != m_keyActions.end() && it->second != TKey::Key_Unknown)
    {
        return getKeyState(it->second);
    }

    return false;
}


/**
 * Change la touche associ�e � une action.
 *
 * \param action Action � modifier.
 * \param key    Touche � utiliser.
 ******************************/

void CGameApplication::setKeyForAction(TAction action, TKey key)
{
    // On cherche si une autre action utilise cette touche
    for (std::map<TAction, TKey>::iterator it = m_keyActions.begin(); it != m_keyActions.end(); ++it)
    {
        if (it->second == key)
        {
            it->second = TKey::Key_Unknown;
        }
    }

    m_keyActions[action] = key;
}


/**
 * Donne la largeur de la fen�tre.
 *
 * \return Largeur de la fen�tre en pixels.
 *
 * \sa CApplication::getHeight
 ******************************/

unsigned int CGameApplication::getWidth() const
{
    sf::Vector2u windowSize = m_window.getSize();
    return windowSize.x;
}


/**
 * Donne la hauteur de la fen�tre.
 *
 * \return Hauteur de la fen�tre en pixels.
 *
 * \sa CApplication::getWidth
 ******************************/

unsigned int CGameApplication::getHeight() const
{
    sf::Vector2u windowSize = m_window.getSize();
    return windowSize.y;
}


/**
 * Donne la position du curseur de la souris
 *
 * \return Position du curseur.
 ******************************/

sf::Vector2i CGameApplication::getCursorPosition() const
{
    return sf::Mouse::getPosition(m_window);
}


/**
 * Indique si le curseur est affich�.
 *
 * \return Bool�en.
 ******************************/

bool CGameApplication::isCursorVisible() const
{
    return m_cursorVisible;
}


/**
 * Affiche ou masque le curseur de la souris.
 *
 * \param show Bool�en.
 ******************************/

void CGameApplication::showMouseCursor(bool show)
{
    m_window.setMouseCursorVisible(show);
    m_cursorVisible = show;
    m_cursorPos.x = getWidth() / 2;
    m_cursorPos.y = getHeight() / 2;

    sf::Mouse::setPosition(sf::Vector2i(m_cursorPos.x, m_cursorPos.y), m_window);
}


/**
 * Initialise les donn�es.
 ******************************/

bool CGameApplication::init()
{
    log("CGameApplication::init");
    if (!CApplication::init())
    {
        return false;
    }

    // TODO (use class member variables ?)
    CRenderer * renderer = new CRenderer{};
    CPhysicEngine * physicEngine = new CPhysicEngine{};
    //CSoundEngine * soundEngine = new CSoundEngine{};

    if (!renderer->init())
    {
        return false;
    }

    new CEntityManager{};

    physicEngine->init();
    //soundEngine->init();

    // Touches par d�faut
    m_keyActions[TAction::ActionForward]          = getKeyCode(m_settings.getValue("Keys", "forward",     "Key_Z"));
    m_keyActions[TAction::ActionBackward]         = getKeyCode(m_settings.getValue("Keys", "backward",    "Key_S"));
    m_keyActions[TAction::ActionStrafeLeft]       = getKeyCode(m_settings.getValue("Keys", "strafeleft",  "Key_Q"));
    m_keyActions[TAction::ActionStrafeRight]      = getKeyCode(m_settings.getValue("Keys", "straferight", "Key_D"));
    m_keyActions[TAction::ActionTurnLeft]         = getKeyCode(m_settings.getValue("Keys", "turnleft",    "Key_Left"));
    m_keyActions[TAction::ActionTurnRight]        = getKeyCode(m_settings.getValue("Keys", "turnright",   "Key_Right"));
    m_keyActions[TAction::ActionTurnUp]           = getKeyCode(m_settings.getValue("Keys", "turnup",      "Key_Up"));
    m_keyActions[TAction::ActionTurnDown]         = getKeyCode(m_settings.getValue("Keys", "turndown",    "Key_Down"));
    m_keyActions[TAction::ActionJump]             = getKeyCode(m_settings.getValue("Keys", "jump",        "Key_Space"));
    m_keyActions[TAction::ActionCrunch]           = getKeyCode(m_settings.getValue("Keys", "crunch",      "Key_LShift"));
    m_keyActions[TAction::ActionZoom]             = getKeyCode(m_settings.getValue("Keys", "zoom",        "Key_G"));
    m_keyActions[TAction::ActionFire1]            = getKeyCode(m_settings.getValue("Keys", "fire1",       "Mouse_Left"));
    m_keyActions[TAction::ActionFire2]            = getKeyCode(m_settings.getValue("Keys", "fire2",       "Mouse_Right"));
    m_keyActions[TAction::ActionVehicleAccel]     = getKeyCode(m_settings.getValue("Keys", "forward",     "Key_Z"));
    m_keyActions[TAction::ActionVehicleBrake]     = getKeyCode(m_settings.getValue("Keys", "backward",    "Key_S"));
    m_keyActions[TAction::ActionVehicleTurnLeft]  = getKeyCode(m_settings.getValue("Keys", "strafeleft",  "Key_Q"));
    m_keyActions[TAction::ActionVehicleTurnRight] = getKeyCode(m_settings.getValue("Keys", "straferight", "Key_D"));
    m_keyActions[TAction::ActionVehicleHandBrake] = getKeyCode(m_settings.getValue("Keys", "handbrake",   "Key_Space"));
    m_keyActions[TAction::ActionVehicleGearUp]    = getKeyCode(m_settings.getValue("Keys", "gear_up",     "Key_PageUp"));
    m_keyActions[TAction::ActionVehicleGearDown]  = getKeyCode(m_settings.getValue("Keys", "gear_down",   "Key_PageDown"));

    return true;
}


/**
 * Initialise la fen�tre.
 *
 * \param title Titre de l'application.
 ******************************/

void CGameApplication::initWindow(const CString& title)
{
    log("CGameApplication::initWindow");

    m_title = title;
    m_fullscreen = m_settings.getBooleanValue("Window", "fullscreen", false);

    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.majorVersion = 3;
    settings.minorVersion = 2;

    sf::VideoMode mode;
    mode.width = m_settings.getValue("Window", "width", "800").toUnsignedInt32();
    mode.height = m_settings.getValue("Window", "height", "600").toUnsignedInt32();
    mode.bitsPerPixel = 32;

    // Cr�ation de la nouvelle fen�tre
    if (m_fullscreen)
    {
        m_window.create(mode, m_title.toCharArray(), sf::Style::Fullscreen, settings);
    }
    else
    {
        m_window.create(mode, m_title.toCharArray(), sf::Style::Titlebar | sf::Style::Close, settings);
    }

    showMouseCursor(false);
    //m_window.setFramerateLimit(60);
    m_window.setVerticalSyncEnabled(true);

    //gRenderer->init();
}


/**
 * M�thode appell�e � chaque frame.
 ******************************/

void CGameApplication::frame()
{
    processEvents();

    //CRenderer::instance().doPendingTasks();
    gTextureManager->doPendingTasks();

    m_activity->frame(m_frameTime);

    // Swap buffers
    m_window.display();
}


void CGameApplication::processEvents()
{
    sf::Event windowEvent;
    while (m_window.pollEvent(windowEvent))
    {
        switch (windowEvent.type)
        {
            // Bouton Quitter (croix ou Alt-F4)
            case sf::Event::Closed:
                close();
                break;

            // Changement de taille de la fen�tre
            case sf::Event::Resized:
            {
                CResizeEvent ev;
                ev.width = windowEvent.size.width;
                ev.height = windowEvent.size.height;
                onEvent(ev);
                break;
            }

            // Texte provenant du clavier
            case sf::Event::TextEntered:
            {
                CTextEvent ev;
                ev.type = EventText;
                ev.unicode = windowEvent.text.unicode;
                onEvent(ev);
                break;
            }

            // Enfoncement d'une touche du clavier
            case sf::Event::KeyPressed:
            {
                switch (windowEvent.key.code)
                {
                case sf::Keyboard::Escape:
                    if (m_gameActive)
                    {
                        showMouseCursor();
                        m_gameActive = false;
                        m_receiver->setEventsEnable(m_gameActive);
                    }
                    else
                    {
                        showMouseCursor(false);
                        m_gameActive = true;
                        m_receiver->setEventsEnable(m_gameActive);
                    }
                    break;

                    // TODO: move to onEvent(keyboard)
                case sf::Keyboard::F4:
                    close(); // DEBUG
                    break;
                case sf::Keyboard::U:
                    log("Set display mode to Normal.");
                    gRenderer->setMode(Normal);
                    //mode = Normal;
                    break;
                case sf::Keyboard::I:
                    log("Set display mode to Textured.");
                    gRenderer->setMode(Textured);
                    //mode = Textured;
                    break;
                case sf::Keyboard::O:
                    log("Set display mode to Lightmaps.");
                    gRenderer->setMode(Lightmaps);
                    //mode = Lightmaps;
                    break;
                case sf::Keyboard::P:
                    log("Set display mode to Wireframe.");
                    gRenderer->setMode(Wireframe);
                    //mode = Wireframe;
                    break;
                }

                CKeyboardEvent ev;

                ev.type = KeyPressed;
                ev.code = getKeyCode(windowEvent.key.code);
                ev.modif = NoModifier;

                if (windowEvent.key.alt) ev.modif |= AltModifier;
                if (windowEvent.key.control) ev.modif |= ControlModifier;
                if (windowEvent.key.shift) ev.modif |= ShiftModifier;

                onEvent(ev);
                break;
            }

            // Relachement d'une touche du clavier
            case sf::Event::KeyReleased:
            {
                CKeyboardEvent ev;

                ev.type = KeyReleased;
                ev.code = getKeyCode(windowEvent.key.code);
                ev.modif = NoModifier;

                if (windowEvent.key.alt) ev.modif |= AltModifier;
                if (windowEvent.key.control) ev.modif |= ControlModifier;
                if (windowEvent.key.shift) ev.modif |= ShiftModifier;

                onEvent(ev);
                break;
            }

            case sf::Event::MouseButtonPressed:
            {
                CMouseEvent ev;

                ev.type = ButtonPressed;
                ev.x = windowEvent.mouseButton.x;
                ev.y = windowEvent.mouseButton.y;
                ev.xrel = windowEvent.mouseButton.x - m_cursorPos.x;
                ev.yrel = windowEvent.mouseButton.y - m_cursorPos.y;

                switch (windowEvent.mouseButton.button)
                {
                default:
                    ev.button = MouseNoButton;
                    break;

                case sf::Mouse::Left:
                    ev.button = MouseButtonLeft;
                    break;

                case sf::Mouse::Right:
                    ev.button = MouseButtonRight;
                    break;

                case sf::Mouse::Middle:
                    ev.button = MouseButtonMiddle;
                    break;

                case sf::Mouse::XButton1:
                    ev.button = MouseButtonX1;
                    break;

                case sf::Mouse::XButton2:
                    ev.button = MouseButtonX2;
                    break;
                }

                if (m_cursorVisible)
                {
                    m_cursorPos = sf::Vector2i{ ev.x, ev.y };
                }

                // On place le curseur au centre de l'�cran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), m_window);
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseButtonReleased:
            {
                CMouseEvent ev;

                ev.type = ButtonReleased;
                ev.x = windowEvent.mouseButton.x;
                ev.y = windowEvent.mouseButton.y;
                ev.xrel = windowEvent.mouseButton.x - m_cursorPos.x;
                ev.yrel = windowEvent.mouseButton.y - m_cursorPos.y;

                switch (windowEvent.mouseButton.button)
                {
                default:
                    ev.button = MouseNoButton;
                    break;

                case sf::Mouse::Left:
                    ev.button = MouseButtonLeft;
                    break;

                case sf::Mouse::Right:
                    ev.button = MouseButtonRight;
                    break;

                case sf::Mouse::Middle:
                    ev.button = MouseButtonMiddle;
                    break;

                case sf::Mouse::XButton1:
                    ev.button = MouseButtonX1;
                    break;

                case sf::Mouse::XButton2:
                    ev.button = MouseButtonX2;
                    break;
                }

                if (m_cursorVisible)
                {
                    m_cursorPos = sf::Vector2i{ ev.x, ev.y };
                }

                // On place le curseur au centre de l'�cran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), m_window);
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseWheelMoved:
            {
                CMouseEvent ev;

                ev.type = MouseWheel;
                ev.x = windowEvent.mouseWheel.x;
                ev.y = windowEvent.mouseWheel.y;
                ev.xrel = windowEvent.mouseWheel.x - m_cursorPos.x;
                ev.yrel = windowEvent.mouseWheel.y - m_cursorPos.y;
                ev.button = (windowEvent.mouseWheel.delta > 0 ? MouseWheelUp : MouseWheelDown);

                if (m_cursorVisible)
                {
                    m_cursorPos = sf::Vector2i{ ev.x, ev.y };
                }

                // On place le curseur au centre de l'�cran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), m_window);
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseMoved:
            {
                CMouseEvent ev;

                ev.type = MoveMouse;
                ev.x = windowEvent.mouseMove.x;
                ev.y = windowEvent.mouseMove.y;
                ev.xrel = windowEvent.mouseMove.x - m_cursorPos.x;
                ev.yrel = windowEvent.mouseMove.y - m_cursorPos.y;
                ev.button = MouseNoButton;

                if (m_cursorVisible)
                {
                    m_cursorPos = sf::Vector2i{ ev.x, ev.y };
                }

                // On place le curseur au centre de l'�cran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), m_window);
                }

                onEvent(ev);
                break;
            }
        }
    }
}


/**
 * Lib�re les ressources de l'application.
 ******************************/

void CGameApplication::release()
{
    log("CGameApplication::releaseEngine");
    CApplication::release();
    /*
    // TODO: move to CActivityGame
    if (m_map != nullptr)
    {
        delete m_map;
        m_map = nullptr;
    }
    */
    gPhysicEngine->close();
    delete gPhysicEngine;

    m_window.close();
    delete gRenderer;
}


/**
 * Gestion des �v�nements de la souris.
 *
 * \param event �v�nement.
 ******************************/

void CGameApplication::onEvent(const CMouseEvent& event)
{
    CApplication::onEvent(event);

    // On envoie l'�v�nement au r�cepteur d'�v�nements
    if (m_receiver && m_gameActive)
    {
        m_receiver->onEvent(event);
    }

    m_activity->onEvent(event);
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event �v�nement.
 ******************************/

void CGameApplication::onEvent(const CTextEvent& event)
{
    CApplication::onEvent(event);

    // On envoie l'�v�nement au r�cepteur d'�v�nements
    if (m_receiver && m_gameActive)
    {
        //m_receiver->onEvent(event);
    }

    m_activity->onEvent(event);
}


/**
 * Gestion des �v�nements du clavier.
 *
 * \param event �v�nement.
 ******************************/

void CGameApplication::onEvent(const CKeyboardEvent& event)
{
    CApplication::onEvent(event);

    // Modification de l'�tat de la touche
    //m_keyStates[event.code] = (event.type == KeyPressed);

    // On envoie l'�v�nement au r�cepteur d'�v�nements
    if (m_receiver && m_gameActive)
    {
        m_receiver->onEvent(event);
    }

    m_activity->onEvent(event);
}


void CGameApplication::onEvent(const CResizeEvent& event)
{
    CApplication::onEvent(event);

    // On envoie l'�v�nement au r�cepteur d'�v�nements
    if (m_receiver && m_gameActive)
    {
        //m_receiver->onEvent(event);
    }

    m_activity->onEvent(event);
}


void CGameApplication::setActivity(IActivity * activity)
{
    assert(activity != nullptr);
    m_activity = activity;
}


void CGameApplication::addActivity(IActivity * activity)
{
    assert(activity != nullptr);

    auto it = std::find(m_activities.begin(), m_activities.end(), activity);
    if (it == m_activities.end())
    {
        m_activities.push_back(activity);
    }
}


void CGameApplication::removeActivity(IActivity * activity)
{
    assert(activity != nullptr);

    if (m_activity == activity)
    {
        log("CGameApplication::removeActivity - You should'nt remove current activity.", ILogger::Error);
        m_activity = nullptr;
    }

    auto it = std::find(m_activities.begin(), m_activities.end(), activity);
    if (it != m_activities.end())
    {
        m_activities.erase(it);
    }
}

} // Namespace TE
