/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Game/CModel.hpp"
#include "Game/IModelData.hpp"
#include "Graphic/CRenderParams.hpp"


namespace TE
{

/**
 * Constructeur. Le modèle est ajouté au moteur physique.
 * Un buffer graphique est crée.
 *
 * \param modelData Pointeur sur les données du modèle.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CModel::CModel(IModelData * modelData) :
    m_mass    { 0.0f },
    m_movable { false },
    m_buffer  { std::make_unique<CBuffer<TVertex3D>>() },
    m_data    { modelData }
{
    assert(m_buffer != nullptr);

    setSequence(0);
}


/**
 * Donne la position du modèle.
 *
 * \return Position du modèle.
 *
 * \sa CModel::setPosition
 ******************************/
/*
TVector3F CModel::getPosition() const
{
    if (m_body)
    {
        const btVector3& vect = m_body->getCenterOfMassPosition();
        return TVector3F(vect.x(), vect.y(), vect.z());
    }

    return Origin3F;
}
*/

/**
 * Donne l'orientation du modèle.
 *
 * \return Orientation du modèle.
 *
 * \sa CModel::setOrientation
 ******************************/
/*
glm::quat CModel::getRotation() const
{
    btQuaternion quat = m_body->getOrientation();
    return CQuaternion(quat.x(), quat.y(), quat.z(), quat.w());
}
*/

/**
 * Donne la vitesse linéaire de l'objet.
 *
 * \return Vitesse linéaire de l'objet.
 *
 * \sa CModel::setLinearVelocity
 ******************************/
/*
glm::vec3 CModel::getLinearVelocity() const
{
    const btVector3& vect = m_body->getLinearVelocity();
    return TVector3F(vect.x(), vect.y(), vect.z());
}
*/

/**
 * Donne la vitesse angulaire de l'objet.
 *
 * \return Vitesse angulaire du modèle.
 *
 * \sa CModel::setAngularVelocity
 ******************************/
/*
TVector3F CModel::getAngularVelocity() const
{
    const btVector3& vect = m_body->getAngularVelocity();
    return TVector3F(vect.x(), vect.y(), vect.z());
}
*/

/**
 * Donne la masse du modèle.
 *
 * \return Masse du modèle en kilogrammes.
 *
 * \sa CModel::setMass
 ******************************/

float CModel::getMass() const
{
    return m_mass;
    //return (1.0f / m_body->getInvMass());
}


/**
 * Donne l'inverse la matrice d'inertie du modèle.
 *
 * \return Inverse de la matrice d'inertie.
 *
 * \sa CModel::setLocalInertia
 ******************************/
/*
TMatrix3F CModel::getInvInertia() const
{
    const btMatrix3x3& matrix = m_body->getInvInertiaTensorWorld();

    return TMatrix3F(matrix[0].x(), matrix[0].y(), matrix[0].z(),
                     matrix[1].x(), matrix[1].y(), matrix[1].z(),
                     matrix[2].x(), matrix[2].y(), matrix[2].z());
}
*/

/**
 * Donne la boite AABB englobant le modèle.
 *
 * \return Bounding box non orientée englobant le modèle.
 *
 * \sa CModel::setBoundingBox
 ******************************/
/*
CBoundingBox CModel::getBoundingBox() const
{
    btVector3 min, max;
    m_body->getAabb(min, max);
    return CBoundingBox(TVector3F(min.x(), min.y(), min.z()), TVector3F(max.x(), max.y(), max.z()));
}
*/

/**
 * Donne le numéro de la séquence actuellement utilisée.
 *
 * \return Numéro de la séquence.
 *
 * \sa CModel::setSequence
 ******************************/

unsigned int CModel::getSequence() const
{
    return m_params.sequence;
}


/**
 * Donne le mode de lecture des séquences.
 *
 * \return Mode de lecture des séquences.
 *
 * \sa CModel::setSeqMode
 ******************************/

CModel::TModelSeqMode CModel::getSeqMode() const
{
    return m_params.mode;
}


/**
 * Donne le numéro du skin utilisé.
 *
 * \return Numéro du skin.
 *
 * \sa CModel::setSkin
 ******************************/

unsigned int CModel::getSkin() const
{
    return m_params.skin;
}


/**
 * Donne le groupe utilisé par le modèle.
 *
 * \return Numéro du groupe.
 *
 * \sa CModel::setGroup
 ******************************/

unsigned int CModel::getGroup() const
{
    return m_params.group;
}


/**
 * Donne la valeur d'un contrôleur.
 *
 * \param controller Numéro du contrôleur.
 * \return Valeur du contrôleur.
 *
 * \sa CModel::setControllerValue
 ******************************/

float CModel::getControllerValue(unsigned int controller) const
{
    assert(controller < TModelParams::NumControllers);
    return m_params.controllers[controller];
}


/**
 * Donne la valeur d'un blender.
 *
 * \param blender Numéro du blender.
 * \return Valeur du blender.
 *
 * \sa CModel::setBlendingValue
 ******************************/

float CModel::getBlendingValue(unsigned int blender) const
{
    assert(blender < 2);
    return m_params.blending[blender];
}


/**
 * Modifie la position du modèle.
 *
 * \param position Position du modèle.
 *
 * \sa CModel::getPosition
 ******************************/
/*
void CModel::setPosition(const TVector3F& position)
{
    TVector3F positionWithOffset = position - m_offset;

    btTransform transform;
    m_motionState->getWorldTransform(transform);
    transform.setOrigin(btVector3(positionWithOffset.X, positionWithOffset.Y, positionWithOffset.Z));
    m_motionState->setWorldTransform(transform);
    m_body->setWorldTransform(transform);
}
*/

/**
 * Translate le modèle.
 *
 * \param v Vecteur de translation.
 *
 * \sa CModel::setPosition
 ******************************/
/*
void CModel::translate(const glm::vec3& v)
{
    m_body->translate(btVector3(v.X, v.Y, v.Z));
}
*/

/**
 * Modifie l'orientation de l'objet.
 *
 * \param orientation Orientation du modèle.
 *
 * \sa CModel::getRotation
 ******************************/
/*
void CModel::setRotation(const glm::quat& orientation)
{
    btTransform transform;
    m_motionState->getWorldTransform(transform);
    transform.setRotation(btQuaternion(orientation.B, orientation.C, orientation.D, orientation.A));
    m_motionState->setWorldTransform(transform);
}
*/

/**
 * Modifie la vitesse linéaire de l'objet.
 *
 * \param velocity Vitesse du modèle.
 *
 * \sa CModel::getLinearVelocity
 ******************************/
/*
void CModel::setLinearVelocity(const glm::vec3& velocity)
{
    Game::physicEngine->removeRigidBody(m_body);
    m_body->setLinearVelocity(btVector3(velocity.X, velocity.Y, velocity.Z));
    Game::physicEngine->addRigidBody(m_body);
}
*/

/**
 * Modifie la vitesse angulaire de l'objet.
 *
 * \param velocity Vitesse angulaire du modèle.
 *
 * \sa CModel::getAngularVelocity
 ******************************/
/*
void CModel::setAngularVelocity(const TVector3F& velocity)
{
    Game::physicEngine->removeRigidBody(m_body);
    m_body->setAngularVelocity(btVector3(velocity.X, velocity.Y, velocity.Z));
    Game::physicEngine->addRigidBody(m_body);
}
*/

/**
 * Modifie la masse du modèle.
 *
 * \param mass Masse du modèle en kilogrammes.
 *
 * \sa CModel::getMass
 ******************************/

void CModel::setMass(float mass)
{
    //btVector3 local_inertia(0.0f, 0.0f, 0.0f);

    if (mass > 0.0f)
    {
        //m_shape->calculateLocalInertia(mass, local_inertia);
        m_mass = mass;
    }
    else
    {
        m_mass = 0.0f;
    }

    //Game::physicEngine->removeRigidBody(m_body);
    //m_body->setMassProps(m_mass, local_inertia);
    //Game::physicEngine->addRigidBody(m_body);
}


/**
 * Modifie la matrice d'inertie du modèle.
 *
 * \param inertia Inverse de la matrice d'inertie du modèle.
 *
 * \sa CModel::getInvInertia
 ******************************/
/*
void CModel::setLocalInertia(const TVector3F& inertia)
{
    btVector3 local_inertia(inertia.X, inertia.Y, inertia.Z);

    if (m_mass > 0.0f)
    {
        m_shape->calculateLocalInertia(m_mass, local_inertia);
    }

    Game::physicEngine->removeRigidBody(m_body);
    m_body->setMassProps(m_mass, local_inertia);
    Game::physicEngine->addRigidBody(m_body);
}
*/

/**
 * Rend le modèle mobile ou immobile.
 *
 * \param movable Booléen indiquant si l'objet est mobile.
 *
 * \sa CModel::isMovable
 ******************************/

void CModel::setMovable(bool movable)
{
    m_movable = movable;
/*
    btVector3 local_inertia(0.0f, 0.0f, 0.0f);

    if (movable)
    {
        if (m_mass > 0.0f)
        {
            m_shape->calculateLocalInertia(m_mass, local_inertia);
        }

        Game::physicEngine->removeRigidBody(m_body);
        m_body->setMassProps(m_mass, local_inertia);
        Game::physicEngine->addRigidBody(m_body);
    }
    else
    {
        Game::physicEngine->removeRigidBody(m_body);
        m_body->setMassProps(0.0f, local_inertia);
        Game::physicEngine->addRigidBody(m_body);
    }
*/
}


/**
 * Modifie le numéro de la séquence à utiliser.
 * Si le numéro est trop grand, la première séquence est utilisée.
 *
 * \param sequence Numéro de la séquence à utiliser.
 *
 * \sa CModel::getSequence
 ******************************/

void CModel::setSequence(unsigned int sequence)
{
    if (m_data && sequence >= m_data->getNumSequences())
    {
        sequence = 0;
    }

    m_params.sequence = sequence;
    m_params.frame = 0;
}


/**
 * Modifie le numéro de la séquence à utiliser.
 *
 * \param name Nom de la séquence à utiliser.
 *
 * \sa CModel::getSequence
 ******************************/

void CModel::setSequenceByName(const CString& name)
{
    assert(m_data != nullptr);

    m_params.sequence = m_data->getSequenceNumber(name);
    m_params.frame = 0;
}


/**
 * Modifie le mode de lecture des séquences.
 *
 * \param mode Mode de lecture des séquences.
 *
 * \sa CModel::getSeqMode
 ******************************/

void CModel::setSeqMode(TModelSeqMode mode)
{
    m_params.mode = mode;
}


/**
 * Modifie le numéro du skin à utiliser.
 *
 * \param skin Numéro du skin à utiliser.
 *
 * \sa CModel::getSkin
 ******************************/

void CModel::setSkin(unsigned int skin)
{
    assert(m_data != nullptr);
    m_params.skin = (skin < m_data->getNumSkins() ? skin : 0);
}


/**
 * Modifie le numéro du groupe à utiliser.
 *
 * \param group Numéro du groupe à utiliser.
 *
 * \sa CModel::getGroup
 ******************************/

void CModel::setGroup(unsigned int group)
{
    assert(m_data != nullptr);
    m_params.group = (group < m_data->getNumGroups() ? group : 0);
}


/**
 * Modifie la valeur d'un controlleur.
 *
 * \param controller Numéro du controlleur (entre 0 et 7).
 * \param value      Valeur du controlleur.
 *
 * \sa CModel::getControllerValue
 ******************************/

void CModel::setControllerValue(unsigned int controller, float value)
{
    assert(controller < TModelParams::NumControllers);
    m_params.controllers[controller] = value;
}


/**
 * Modifie la valeur du blending.
 *
 * \param blender Numéro du blender.
 * \param value   Valeur du blending.
 *
 * \sa CModel::getBlendingValue
 ******************************/

void CModel::setBlendingValue(unsigned int blender, float value)
{
    assert(blender < 2);
    m_params.blending[blender] = value;
}

/*
void CModel::setOffset(const glm::vec3& offset)
{
    m_offset = offset;
    translate(m_offset - offset);
}
*/

/**
 * Ajoute une force au modèle.
 *
 * \param force Force à ajouter.
 ******************************/
/*
void CModel::addForce(const TVector3F& force)
{
    m_body->applyCentralImpulse(btVector3(force.X, force.Y, force.Z));
}
*/

/**
 * Ajoute un couple au modèle.
 *
 * \param torque Couple à ajouter.
 ******************************/
/*
void CModel::addTorque(const TVector3F& torque)
{
    m_body->applyTorqueImpulse(btVector3(torque.X, torque.Y, torque.Z));
}
*/

/**
 * Affiche le modèle.
 *
 * \params params Paramètres de rendu.
 ******************************/

void CModel::render(CRenderParams * params, const std::shared_ptr<CShader>& shader, const glm::mat4& world)
{
    params->addBuffer(m_buffer.get(), shader, world);
}


/**
 * Met à jour les données du modèle.
 *
 * \param frameTime Durée de la frame en secondes.
 ******************************/

void CModel::update(float frameTime)
{
    //assert(m_data != nullptr);

    if (m_data)
    {
        // Mise-à-jour de la boite englobante
        //setBoundingBox(m_data->getBoundingBox(m_params.sequence));

        m_data->updateParams(m_params, frameTime);

        //translate(m_data->getLinearMovement(m_params.sequence));

        if (m_buffer)
        {
            m_data->updateBuffer(m_buffer.get(), m_params);
        }
    }
}

} // Namespace TE
