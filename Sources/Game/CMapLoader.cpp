/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include "Game/CMapLoader.hpp"
#include "Game/CMap.hpp"
#include "Core/CZipFile.hpp"
#include "Entities/CMapEntity.hpp"
#include "Graphic/CShader.hpp"

#include "Entities/CBox.hpp" // DEBUG
#include "Entities/CSphere.hpp" // DEBUG
#include "Entities/CCylinder.hpp" // DEBUG
#include "Entities/CCamera.hpp"  // DEBUG
#include "Entities/CScreen.hpp"  // DEBUG
#include "Entities/CPlayerStart.hpp"  // DEBUG
#include "Entities/CPlayer.hpp"  // DEBUG
#include "Entities/CTrack.hpp"  // DEBUG
#include "Entities/CTrackPoint.hpp"  // DEBUG
#include "Entities/CTrigger.hpp"  // DEBUG
#include "Entities/CRope.hpp"  // DEBUG
#include "Entities/CHeightField.hpp"  // DEBUG
#include "Entities/CModelEntity.hpp"  // DEBUG
#include "Entities/CSpotLight.hpp" // DEBUG
#include "Entities/CDirectionalLight.hpp" // DEBUG
#include "Entities/CPointLight.hpp" // DEBUG


namespace TE
{

CMap * CMapLoader::loadFromFile(const CString& filename)
{
    gApplication->log(CString("Load map \"%1\"").arg(filename), ILogger::Debug);

    CZipFile zip(filename);
    if (!zip.isOpen())
    {
        gApplication->log(CString("Can't load ZIP file"), ILogger::Error);
        return nullptr;
    }

    std::vector<char> entities;
    if (!zip.getFileContent("entities.json", entities))
    {
        gApplication->log(CString("Can't read entities.json from ZIP file"), ILogger::Error);
        return nullptr;
    }

    CMap * map = new CMap{};

    if (!gEntityManager->loadFromBuffer(&entities[0], map))
    {
        gApplication->log("Can't load entities.json", ILogger::Error);
        delete map;
        return nullptr;
    }

    gApplication->log("Entities loaded", ILogger::Debug);

#ifdef T_DEBUG_MAP_LOADER

    auto mapEntity = map->getMapEntity();

    // DEBUG: test
    auto playerStart = new CPlayerStart { mapEntity, "player_start" };
    playerStart->translate({ 0.0f, 0.0f, 100.0f });
    //playerStart->rotate(glm::angleAxis(glm::radians(45.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));

    auto shader = std::make_shared<CShader>();
    //shader->loadFromFiles("../../../Resources/shaders/texturing.vert", "../../../Resources/shaders/texturing.frag");
    shader->loadFromFiles("../../../Resources/shaders/texturing_shadow.vert", "../../../Resources/shaders/texturing_shadow.frag");
    auto texture1 = gTextureManager->loadTexture("sol.bmp", CTextureManager::FilterBilinearMipmap);
    auto texture2 = gTextureManager->loadTexture("sample.png", CTextureManager::FilterBilinearMipmap);
    auto texture3 = gTextureManager->loadTexture("sample2.png", CTextureManager::FilterBilinearMipmap);
    auto texture4 = gTextureManager->loadTexture("test2.bmp", CTextureManager::FilterBilinearMipmap);
    auto texture5 = gTextureManager->loadTexture("textures/FIFTIES_FLR03.bmp", CTextureManager::FilterBilinearMipmap);

    {
        auto mesh = new CBox{ mapEntity, "box_big_floor" };
        mesh->setShader(shader);
        mesh->setTexture(texture5);
        mesh->setSize({ 10000.0f, 10000.0f, 10.0f });
        mesh->translate({ 0.0f, 0.0f, -1000.0f });
        mesh->setTextureScale({ 10.0f, 10.0f });
    }

    {
        auto mesh = new CBox{ mapEntity, "box_big_ceil" };
        mesh->setShader(shader);
        mesh->setTexture(texture5);
        mesh->setSize({ 10000.0f, 10000.0f, 10.0f });
        mesh->translate({ 0.0f, 0.0f, 3000.0f });
        mesh->setTextureScale({ 10.0f, 10.0f });
    }

    {
        auto mesh = new CBox{ mapEntity, "box_big_wall_x_pos" };
        mesh->setShader(shader);
        mesh->setTexture(texture5);
        mesh->setSize({ 10.0f, 10000.0f, 4000.0f });
        mesh->translate({ 5000.0f, 0.0f, 1000.0f });
        mesh->setTextureScale({ 10.0f, 4.0f });
    }

    {
        auto mesh = new CBox{ mapEntity, "box_big_wall_x_neg" };
        mesh->setShader(shader);
        mesh->setTexture(texture5);
        mesh->setSize({ 10.0f, 10000.0f, 4000.0f });
        mesh->translate({ -5000.0f, 0.0f, 1000.0f });
        mesh->setTextureScale({ 10.0f, 4.0f });
    }

    {
        auto mesh = new CBox{ mapEntity, "box_big_wall_y_pos" };
        mesh->setShader(shader);
        mesh->setTexture(texture5);
        mesh->setSize({ 10000.0f, 10.0f, 4000.0f });
        mesh->translate({ 0.0f, 5000.0f, 1000.0f });
        mesh->setTextureScale({ 10.0f, 4.0f });
    }

    {
        auto mesh = new CBox{ mapEntity, "box_big_wall_y_neg" };
        mesh->setShader(shader);
        mesh->setTexture(texture5);
        mesh->setSize({ 10000.0f, 10.0f, 4000.0f });
        mesh->translate({ 0.0f, -5000.0f, 1000.0f });
        mesh->setTextureScale({ 10.0f, 4.0f });
    }

    auto mesh1 = new CBox{ mapEntity, "box_floor" };
    mesh1->setShader(shader);
    mesh1->setSize({ 2000.0f, 1000.0f, 210.0f });
    mesh1->translate({ 0.0f, 0.0f, -105.0f });
    mesh1->setTexture(texture1);

    auto mesh1b = new CBox{ mapEntity, "box_floor2" };
    mesh1b->setShader(shader);
    mesh1b->setSize({ 2000.0f, 500.0f, 10.0f });
    mesh1b->translate({ 0.0f, -750.0f, -205.0f });
    mesh1b->setTexture(texture1);

    // Escalier
    float height = 25.0f;
    for (auto i = 0; i < 10; ++i)
    {
        auto mesh1c = new CBox{ mapEntity, CString("box_floor3_%1").arg(i) };
        mesh1c->setShader(shader);
        mesh1c->setSize({ 200.0f, 200.0f, height });
        mesh1c->translate({ -200.0f - 50.0f * i, -600.0f, -200.0f + height / 2 + height * i });
        mesh1c->setTexture(texture1);
    }

    // Cam�ra
    auto camera = new CCamera { mapEntity, "camera" };
    //camera->translate({ -1000.0f, -1000.0f, 300.0f });
    camera->translate({ 500.0f, -200.0f, 200.0f });
    //camera->rotate(glm::angleAxis(glm::radians(45.0f) , glm::vec3{ 0.0f, 0.0f, 1.0f } ));
    camera->rotate(glm::angleAxis(glm::radians(30.0f) , glm::vec3{ 0.0f, 1.0f, 0.0f } ));

    // �cran
    auto screen = new CScreen { mapEntity, "screen1" };
    screen->setShader(shader);
    screen->translate({ 850.0f, -200.0f, 180.0f, });
    screen->setCamera(camera);
    screen->setSize(150.0f, 100.0f);
    screen->setFBOSize(512, 512);
    //screen->initBuffers();

    auto mesh2 = new CBox{ mapEntity, "box1" };
    mesh2->setShader(shader);
    mesh2->setSize({ 100.0f, 100.0f, 100.0f });
    //mesh2->setMass(100.0f);
    mesh2->setDynamic(true);
    mesh2->translate({ 500.0f, 400.0f, 100+60.0f });
    mesh2->setTexture(texture2);

    auto mesh3 = new CBox { mapEntity, "box2" };
    mesh3->setShader(shader);
    mesh3->setSize({ 200.0f, 800.0f, 170.0f });
    mesh3->translate({ -800.0f, 0.0f, 85.0f });
    mesh3->setTexture(texture3);

    auto mesh4 = new CBox { mapEntity, "box3" };
    mesh4->setShader(shader);
    mesh4->setSize({ 400.0f, 160.0f, 160.0f });
    //mesh4->setMass(500.0f);
    mesh4->setDynamic(true);
    mesh4->translate({ 300.0f, -300.0f, 100 + 160.0f });
    mesh4->rotate(glm::angleAxis(glm::radians(30.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    mesh4->setTexture(texture4);

    auto sphere = new CSphere { mapEntity, "sphere" };
    sphere->setShader(shader);
    sphere->setRadius(50.0f);
    sphere->setMass(100.0f);
    sphere->setDynamic(true);
    sphere->translate({ 600.0f, 0.0f, 200.0f });
    sphere->setTexture(texture4);
        
    auto cylinder = new CCylinder { mapEntity, "cylinder" };
    cylinder->setShader(shader);
    cylinder->setRadius(20.0f);
    cylinder->setLength(200.0f);
    cylinder->setMass(30.0f);
    cylinder->setDynamic(true);
    cylinder->translate({ 690.0f, 250.0f, 210.0f });
    cylinder->setTexture(texture4);

    auto rope = new CRope { mapEntity, "rope" };
    rope->setRadius(10.0f);
    rope->translate({ 460.0f, 0.0f, 100.0f });

    // NPC
    auto npc = new CPlayer { mapEntity, "npc" };
    npc->translate({ -200.0f, 200.0f, 100.0f });

    // Track
    auto trackPoint1 = new CTrackPoint { mapEntity, "track_point_1" };
    trackPoint1->translate({ -500.0f, -500.0f, 300.0f });
    auto trackPoint2 = new CTrackPoint { mapEntity, "track_point_2" };
    trackPoint2->translate({ -500.0f, 500.0f, 500.0f });
    trackPoint2->rotate(glm::angleAxis(glm::radians(-90.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    auto trackPoint3 = new CTrackPoint { mapEntity, "track_point_3" };
    trackPoint3->translate({ 500.0f, 500.0f, 150.0f });
    trackPoint3->rotate(glm::angleAxis(glm::radians(-180.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    auto trackPoint4 = new CTrackPoint { mapEntity, "track_point_4" };
    trackPoint4->translate({ 500.0f, -500.0f, 150.0f });
    trackPoint4->rotate(glm::angleAxis(glm::radians(90.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));

    auto track = new CTrack { mapEntity, "track" };
    track->setLoop(true);
    track->setSpeed(200.0f);
    track->addPoint(trackPoint1);
    track->addPoint(trackPoint2);
    track->addPoint(trackPoint3);
    track->addPoint(trackPoint4);
    track->start();

    auto box_track = new CBox{ track, "box_track" };
    box_track->setShader(shader);
    box_track->setSolid(false);
    box_track->setSize({ 50.0f, 50.0f, 50.0f });
    box_track->setTexture(texture2);

    // Trigger
    CTrigger * trigger = new CTrigger{ mapEntity, "trigger" };
    trigger->setSize({ 100.0f, 500.0f, 200.0f });
    trigger->translate({ 800.0f, -750.0f, -100.0f });

    auto heightfield = new CHeightField{ mapEntity, "heightfield" };
    heightfield->setShader(shader);
    heightfield->setSize({ 1800.0f, 1000.0f });
    heightfield->translate({ 1900.0f, 0.0f, 0.0f });
    heightfield->setSubdivisions(4, 3);
    //heightfield->setPoints({ 100.0f, 80.0f, 53.0f, 12.0f, 0.0f, 80.0f, 20.0f, 30.0f, 10.0f, 0.0f, 0.0f, 0.0f, 10.0f, -100.0f, -150.0f, -200.0f, -80.0f, 0.0f, 0.0f, 70.0f });
    heightfield->setPoints({ 0, 150, 120, 20, 0, 0, 0, 0, 0, 0, 0, 0, -200, 0, 0, 0, 0, 0, 0, 0 });
    heightfield->setTexture(texture1);
    //heightfield->init();

    // lumi�res dynamiques
    auto shaderLight = std::make_shared<CShader>();
    shaderLight->loadFromFiles("../../../Resources/shaders/shadowmap.vert", "../../../Resources/shaders/shadowmap.frag");

    auto light1 = new CSpotLight{ mapEntity, "spot_light1" };
    light1->setShader(shaderLight);
    light1->setColor(CColor::Orange);
    light1->setAngle(50.0f);
    light1->translate(glm::vec3{ 700.0f, 0.0f, 300.0f });
    light1->rotate(glm::angleAxis(glm::radians(-25.0f), glm::vec3{ 0.0f, 1.0f, 0.0f }));
    light1->rotate(glm::angleAxis(glm::radians(180.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    map->addLight(light1);

    auto light2 = new CSpotLight{ mapEntity, "spot_light2" };
    light2->setShader(shaderLight);
    light2->setColor(CColor::Blue);
    light2->setAngle(20.0f);
    light2->translate(glm::vec3{ 700.0f, 0.0f, 300.0f });
    light2->rotate(glm::angleAxis(glm::radians(-25.0f), glm::vec3{ 0.0f, 1.0f, 0.0f }));
    light2->rotate(glm::angleAxis(glm::radians(180.0f), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    map->addLight(light2);
/*
    auto light3 = new CDirectionalLight{ mapEntity, "dir_light" };
    light3->setShader(shaderLight);
    light3->setColor(CColor::Red);
    light3->setIntensity(0.2f);
    light3->rotate(glm::angleAxis(glm::radians(25.0f), glm::vec3{ 0.0f, 1.0f, 0.0f }));
    map->addLight(light3);

    auto light4 = new CPointLight{ mapEntity, "point_light" };
    light4->setShader(shaderLight);
    light4->setColor(CColor::Green);
    light4->setIntensity(0.2f);
    //light4->translate(glm::vec3{ -200.0f, -400.0f, 155.0f });
    light4->translate(glm::vec3{ 4900.0f, 4900.0f, 2900.0f });
    map->addLight(light4);
*/
/*
    // Pleins d'objets...
    const glm::vec3 boxSize{ 50.0f, 50.0f, 50.0f };
    const float cylinderRadius = 20.0f;
    const float cylinderHeight = 100.0f;
    for (int range = 0; range < 8; ++range)
    {
        for (int row = 0; row < 5; ++row)
        {
            for (int col = 0; col < 10; ++col)
            {
                auto box = new CBox{ mapEntity, "box" };
                box->setShader(shader);
                box->setTexture(texture4);
                box->setSize(boxSize);
                box->setMass(100.0f);
                //box->setDynamic(true);
                box->translate({ -200.0f + (boxSize.x + 10.0f) * col, 800.0f + range * (boxSize.y + 10.0f), -600.0f + boxSize.z / 2 + row * (cylinderHeight + boxSize.z + 1.0f) });

                auto cylinder = new CCylinder{ mapEntity, "cylinder" };
                cylinder->setShader(shader);
                cylinder->setTexture(texture4);
                cylinder->setRadius(cylinderRadius);
                cylinder->setLength(cylinderHeight);
                cylinder->setMass(30.0f);
                //cylinder->setDynamic(true);
                cylinder->translate({ -200.0f + (boxSize.x + 10.0f) * col, 800.0f + range * (boxSize.y + 10.0f), -600.0f + cylinderHeight / 2 + row * (cylinderHeight + boxSize.z + 1.0f) + boxSize.z });
            }
        }
    }
*/

#endif

    return map;
}

} // Namespace TE
