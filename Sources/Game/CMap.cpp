/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/matrix_transform.hpp>

#include "Game/CMap.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CMapEntity.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Core/CApplication.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CShader.hpp" // DEBUG: shader normals


namespace TE
{

/**
 * Constructeur.
 ******************************/

CMap::CMap() :
    m_mapEntity { nullptr },
    m_scene     { gPhysicEngine->createScene() },
    m_shadowMap { CShadowMap::ShadowMap_4_1024 }
{
    m_mapEntity = new CMapEntity { this, "map" };
}


/**
 * Destructeur.
 ******************************/

CMap::~CMap()
{
    gApplication->log("CMap::~CMap()", ILogger::Debug);
    delete m_mapEntity;
    gPhysicEngine->removeScene(m_scene);
}


/**
 * Ajoute un joueur � la map.
 *
 * Le joueur est ajout� � la hi�rarchie des entit�s de la map.
 * Avant d'appeler cette m�thode, il faut retirer le joueur de la pr�c�dente map avec la m�thode removePlayer.
 *
 * \param player Pointeur sur l'entit� CPlayer � ajouter � la map (ne doit pas �tre nul).
 ******************************/

void CMap::addPlayer(CPlayer * player)
{
    assert(player != nullptr);
    m_mapEntity->addPlayer(player);
    player->initController();
}


/**
 * Retire un joueur de la map.
 *
 * \param player Pointeur sur l'entit� CPlayer � retirer de la map (ne doit pas �tre nul).
 ******************************/

void CMap::removePlayer(CPlayer * player)
{
    assert(player != nullptr);
    player->releaseController();
    player->setParent(nullptr);
}


/**
 * Ajoute une lumi�re � la map.
 *
 * \param light Pointeur sur l'entit� ILight � ajouter � la map (ne doit pas �tre nul).
 ******************************/

void CMap::addLight(ILight * light)
{
    m_shadowMap.addLight(light);
}


/**
 * Retire une lumi�re de la map.
 *
 * \param light Pointeur sur l'entit� ILight � retirer de la map (ne doit pas �tre nul).
 ******************************/

void CMap::removeLight(ILight * light)
{
    m_shadowMap.removeLight(light);
}


void CMap::update(float frameTime)
{
/* // TODO
    // R�initialise l'�tat des entit�s graphiques
    for (auto& entity : m_graphicEntities)
    {
        entity->resetState();
    }
*/
    // Mise-�-jour du graphe de sc�ne
    m_mapEntity->update(frameTime);
}


void CMap::renderMap(CCamera * camera)
{
    assert(camera != nullptr);

    // Calcul des shadow maps
    m_shadowMap.update(this);


    // Rendu primaire
    // TODO: utiliser des param�tres pour near et far
    CRenderParams params{ this, camera, camera->getViewMatrix(), glm::perspective(glm::radians(gRenderer->getFieldOfView()), static_cast<float>(gApplication->getWidth()) / static_cast<float>(gApplication->getHeight()), 1.0f, 100 * 1000.0f) };
    params.setMode(gRenderer->getMode());
    params.setShadowMap(&m_shadowMap);

    // DEBUG: display normals
    //auto shader = std::make_shared<CShader>();
    //shader->loadFromFiles("../../../Resources/shaders/normals.vert", "../../../Resources/shaders/normals.frag");
    //params.setShader(shader);

    params.render();
}

} // Namespace TE
