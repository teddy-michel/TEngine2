/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Game/IInputReceiver.hpp"
#include "Core/Events.hpp"
#include "Game/CGameApplication.hpp"


namespace TE
{

/**
 * Constructeur par défaut.
 ******************************/

IInputReceiver::IInputReceiver() :
    m_eventsEnable{ false }
{ }


/**
 * Destructeur.
 ******************************/

IInputReceiver::~IInputReceiver()
{ }


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void IInputReceiver::onEvent(const CMouseEvent& event)
{
    T_UNUSED(event);
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/
/*
void IInputReceiver::onEvent(const CTextEvent& event)
{
    T_UNUSED(event);
}
*/

/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void IInputReceiver::onEvent(const CKeyboardEvent& event)
{
    T_UNUSED(event);
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/
/*
void IInputReceiver::onEvent(const CResizeEvent& event)
{
    T_UNUSED(event);
}
*/


bool IInputReceiver::isActionActive(TAction action) const
{
    if (!m_eventsEnable)
    {
        return false;
    }

    return gGameApplication->isActionActive(action);
}


bool IInputReceiver::isKeyPressed(sf::Keyboard::Key key) const
{
    return (m_eventsEnable && sf::Keyboard::isKeyPressed(key));
}

} // Namespace TE
