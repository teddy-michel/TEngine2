/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>

//#include "Graphic/CRenderer.hpp"
#include "Graphic/CCursor.hpp"
//#include "Core/Utils.hpp"


namespace TE
{

/**
 * Constructeur par défaut.
 *
 * \param pixels Tableau des pixels du curseur.
 ******************************/

CCursor::CCursor(const char pixels[CursorSize*CursorSize]) :
    m_px      (0),
    m_py      (0),
    m_texture (0)
{
    if (pixels == nullptr)
    {
        for (std::size_t i = 0; i < CursorSize * CursorSize; ++i)
        {
            m_pixels[i] = ' ';
        }
    }
    else
    {
        std::copy(pixels, pixels + CursorSize * CursorSize * sizeof(char), m_pixels);
    }
}


/**
 * Constructeur.
 *
 * \param pixels Tableau des pixels du curseur.
 * \param px     Coordonnée horizontale du pointeur.
 * \param py     Coordonnée verticale du pointeur.
 ******************************/

CCursor::CCursor(const char pixels[CursorSize*CursorSize], const unsigned int px, const unsigned int py) :
    m_px      (px),
    m_py      (py),
    m_texture (0)
{
    if (pixels == nullptr)
    {
        for (std::size_t i = 0; i < CursorSize * CursorSize; ++i)
        {
            m_pixels[i] = ' ';
        }
    }
    else
    {
        std::copy(pixels , pixels + CursorSize * CursorSize * sizeof(char), m_pixels);
    }
}


/**
 * Destructeur. Supprime la texture en mémoire graphique.
 ******************************/

CCursor::~CCursor()
{
    gTextureManager->deleteTexture(m_texture);
}


/**
 * Permet de récupérer le tableau des pixels composant le curseur.
 *
 * \return Tableau de pixels en lecture seul.
 ******************************/

char const * CCursor::getPixels() const
{
    return m_pixels;
}


/**
 * Donne la coordonnée horizontale du pointeur.
 *
 * \return Coordonnée horizontale du pointeur.
 ******************************/

unsigned int CCursor::getPointerX() const
{
    return m_px;
}


/**
 * Donne la coordonnée verticale du pointeur.
 *
 * \return Coordonnée verticale du pointeur.
 ******************************/

unsigned int CCursor::getPointerY() const
{
    return m_py;
}


/**
 * Donne l'identifiant de la texture correspondant au curseur.
 * La texture est crée lors du premier appel à cette méthode.
 *
 * \todo Tester.
 *
 * \return Identifiant de la texture du curseur.
 ******************************/

TTextureId CCursor::getTextureId()
{
    if (m_texture == 0)
        loadTexture();
    return m_texture;
}


/**
 * Modifie la coordonnée horizontale du pointeur.
 *
 * \param px Coordonnée horizontale du pointeur.
 ******************************/

void CCursor::setPointerX(unsigned int px)
{
    m_px = px;
}


/**
 * Modifie la coordonnée verticale du pointeur.
 *
 * \param py Coordonnée verticale du pointeur.
 ******************************/

void CCursor::setPointerY(unsigned int py)
{
    m_py = py;
}


/**
 * Charge le curseur en mémoire graphique.
 ******************************/

void CCursor::loadTexture()
{
    static unsigned int num_cursor = 0;

    if (m_texture == 0)
    {
        CColor * data = new CColor[CursorSize * CursorSize];

        for (std::size_t i = 0; i < CursorSize * CursorSize; ++i)
        {
            switch (m_pixels[i])
            {
                default:
                case PixelTransparent:
                    data[i] = CColor(0xFF, 0xFF, 0xFF, 0x00);
                    break;

                case PixelBlack:
                    data[i] = CColor(0x00, 0x00, 0x00, 0xFF);
                    break;

                case PixelInverted:
                    data[i] = CColor(0x00, 0x00, 0x00, 0x00);
                    break;

                case PixelWhite:
                    data[i] = CColor(0xFF, 0xFF, 0xFF, 0xFF);
                    break;
            }
        }

        CImage image = CImage(CursorSize, CursorSize, data);
        delete[] data;

        m_texture = gTextureManager->loadTexture(":cursor:" + CString::fromNumber(num_cursor++), image, CTextureManager::FilterNone);
    }
}

} // Namespace TE
