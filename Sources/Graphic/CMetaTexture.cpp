/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "Graphic/CMetaTexture.hpp"
//#include "Core/Maths/MathsUtils.hpp"
#include "Core/utils.hpp"
//#include "Graphic/CRenderer.hpp"


namespace TE
{

// Nombre de méta-textures
unsigned int CMetaTexture::NumMetaTextures = 0;


/**
 * Constructeur par défaut.
 ******************************/

CMetaTexture::CMetaTexture() :
    m_metaId      { NumMetaTextures++ },
    m_texture     { 0 },
    m_static      { false },
    m_width       { 512 },
    m_height      { 512 },
    m_imagesCount { 0 },
    m_root        { nullptr },
    m_pixels      { nullptr }
{
    //createTexture();
}


/**
 * Constructeur permettant de préciser les dimensions de la texture.
 *
 * \param width  Largeur de la texture en pixels.
 * \param height Hauteur de la texture en pixels.
 ******************************/

CMetaTexture::CMetaTexture(unsigned int width, unsigned int height) :
    m_metaId      { NumMetaTextures++ },
    m_texture     { 0 },
    m_static      { false },
    m_width       { smallestPowerOfTwoGreaterOrEqual(width) },
    m_height      { smallestPowerOfTwoGreaterOrEqual(height) },
    m_imagesCount { 0 },
    m_root        { nullptr },
    m_pixels      { nullptr }
{
    //createTexture();
}


/**
 * Constructeur par déplacement.
 ******************************/

CMetaTexture::CMetaTexture(CMetaTexture&& other) :
    m_metaId      { std::move(other.m_metaId) },
    m_texture     { std::move(other.m_texture) },
    m_static      { std::move(other.m_static) },
    m_width       { std::move(other.m_width) },
    m_height      { std::move(other.m_height) },
    m_imagesCount { std::move(other.m_imagesCount) },
    m_root        { std::move(other.m_root) },
    m_pixels      { std::move(other.m_pixels) }
{

}


CMetaTexture& CMetaTexture::operator=(CMetaTexture&& other)
{
    if (this != &other)
    {
        if (m_root != nullptr)
        {
            delete m_root;
        }

        if (m_pixels != nullptr)
        {
            delete[] m_pixels;
        }

        m_metaId = std::move(other.m_metaId);
        m_texture = std::move(other.m_texture);
        m_static = std::move(other.m_static);
        m_width = std::move(other.m_width);
        m_height = std::move(other.m_height);
        m_imagesCount = std::move(other.m_imagesCount);
        m_root = std::move(other.m_root);
        m_pixels = std::move(other.m_pixels);

        other.m_metaId = static_cast<unsigned int>(-1);
        other.m_texture = CTextureManager::NoTexture;
        other.m_static = false;
        other.m_width = 512;
        other.m_height = 512;
        other.m_imagesCount = 0;
        other.m_root = nullptr;
        other.m_pixels = nullptr;
    }

    return *this;
}


/**
 * Destructeur.
 ******************************/

CMetaTexture::~CMetaTexture()
{
    if (m_root != nullptr)
    {
        delete m_root;
    }

    if (m_pixels != nullptr)
    {
        delete[] m_pixels;
    }
}


void CMetaTexture::createTexture()
{
    // Création du nœud racine
    m_root = new TTextureNode();
    m_root->width = m_width;
    m_root->height = m_height;

    m_pixels = new unsigned char[4 * m_width * m_height];
    assert(m_pixels != nullptr);

    for (unsigned int i = 0; i < 4 * m_width * m_height; ++i)
    {
        m_pixels[i] = 0xFF;
    }

    // Création de la texture
    CImage img(m_width, m_height, reinterpret_cast<CColor *>(m_pixels));
    m_texture = gTextureManager->loadTexture(CString(":metatext:%1").arg(m_metaId), img, CTextureManager::FilterBilinear, false);
}


/**
 * Donne l'identifiant de la texture.
 *
 * \return Identifiant de la texture.
 ******************************/

TTextureId CMetaTexture::getTextureId() const
{
    return m_texture;
}


/**
 * Indique si la texture est statique (elle a été chargée depuis un fichier), ou si elle est
 * dynamique (on peut ajouter ou enlever des images).
 *
 * \return Booléen.
 ******************************/

bool CMetaTexture::isStatic() const
{
    return m_static;
}


/**
 * Donne la largeur totale de la texture.
 *
 * \return Largeur de la texture en pixels.
 *
 * \sa CMetaTexture::getHeight
 ******************************/

unsigned int CMetaTexture::getWidth() const
{
    return m_width;
}


/**
 * Donne la hauteur totale de la texture.
 *
 * \return Hauteur de la texture en pixels.
 *
 * \sa CMetaTexture::getWidth
 ******************************/

unsigned int CMetaTexture::getHeight() const
{
    return m_height;
}


/**
 * Donne l'abcisse d'une image.
 *
 * \return Abcisse de l'image.
 *
 * \sa CMetaTexture::getY
 ******************************/

unsigned int CMetaTexture::getX(unsigned int num) const
{
    return (m_root ? m_root->getX(num) : 0);
}


/**
 * Donne l'ordonnée d'une image.
 *
 * \return Ordonnée de l'image.
 *
 * \sa CMetaTexture::getX
 ******************************/

unsigned int CMetaTexture::getY(unsigned int num) const
{
    return (m_root ? m_root->getY(num) : 0);
}


/**
 * Donne le nombre d'images de la texture.
 *
 * \return Nombre d'images.
 ******************************/

unsigned int CMetaTexture::getNumImages() const
{
    return m_imagesCount;
}


/**
 * Ajoute une image à la texture.
 *
 * \param img Image à ajouter.
 * \return Numéro de l'image.
 *
 * \sa CMetaTexture::deleteImage
 * \sa CMetaTexture::update
 ******************************/

unsigned int CMetaTexture::addImage(const CImage& img)
{
    if (m_static)
    {
        return InvalidImageNumber;
    }

    if (m_root == nullptr)
    {
        createTexture();
    }

    assert(m_root != nullptr);
    assert(m_pixels != nullptr);

    // Insertion de l'image dans l'arbre binaire
    if (m_root->insertImage(img, m_imagesCount))
    {
        const unsigned char * tableau = reinterpret_cast<const unsigned char *>(img.getPixels());

        unsigned int w = img.getWidth();
        unsigned int h = img.getHeight();
        unsigned int x = m_root->getX(m_imagesCount);
        unsigned int y = m_root->getY(m_imagesCount);

        // Copie des pixels de l'image dans le tableau de pixels de la texture
        for (unsigned int i = 0; i < h; ++i)
        {
            for (unsigned int j = 0; j < w; ++j)
            {
                m_pixels[4 * (m_width * (y + i) + x + j) + 0] = tableau[4 * (i * w + j) + 0];
                m_pixels[4 * (m_width * (y + i) + x + j) + 1] = tableau[4 * (i * w + j) + 1];
                m_pixels[4 * (m_width * (y + i) + x + j) + 2] = tableau[4 * (i * w + j) + 2];
                m_pixels[4 * (m_width * (y + i) + x + j) + 3] = tableau[4 * (i * w + j) + 3];
            }
        }

        return (m_imagesCount++);
    }

    return InvalidImageNumber;
}


/**
 * Enlève une image de la texture.
 *
 * \param num Numéro de l'image à supprimer.
 *
 * \sa CMetaTexture::addImage
 ******************************/

void CMetaTexture::deleteImage(unsigned int num)
{
    if (!m_static && m_root && num != InvalidImageNumber)
    {
        m_root->deleteImage(num);
    }
}


/**
 * Met à jour la texture.
 *
 * \sa CMetaTexture::addImage
 ******************************/

void CMetaTexture::update()
{
    if (m_static)
    {
        return;
    }

    if (m_root == nullptr)
    {
        createTexture();
    }

    assert(m_root != nullptr);
    assert(m_pixels != nullptr);

    gTextureManager->deleteTexture(m_texture);

    CImage img(m_width, m_height, reinterpret_cast<CColor *>(m_pixels));

#ifdef T_DEBUG
    //img.saveToFile("lightmaps.tga");
#endif

    // Normalement l'identifiant ne change pas
    m_texture = gTextureManager->loadTexture(CString(":metatext:%1").arg(m_metaId), img, CTextureManager::FilterBilinear, false);
}


/**
 * Constructeur par défaut.
 ******************************/

CMetaTexture::TTextureNode::TTextureNode() :
    x         (0),
    y         (0),
    width     (0),
    height    (0),
    num_image (InvalidImageNumber)
{
    child[0] = child[1] = nullptr;
}


/**
 * Destructeur.
 ******************************/

CMetaTexture::TTextureNode::~TTextureNode()
{
    delete child[0];
    delete child[1];
}


/**
 * Insert une image dans l'arbre binaire.
 *
 * \todo Retourner un booléen ?
 *
 * \param img Image à insérer.
 * \param num Numéro de l'image.
 * \return Pointeur sur le nœud dans lequel l'image a été insérée.
 ******************************/

CMetaTexture::TTextureNode * CMetaTexture::TTextureNode::insertImage(const CImage& img, unsigned int num)
{
    // nœud
    if (child[0] && child[1])
    {
        // Try inserting into first child
        TTextureNode * new_node = child[0]->insertImage(img, num);
        if (new_node)
            return new_node;

        // No room, insert into second
        return child[1]->insertImage(img, num);
    }
    // Feuille
    else
    {
        // If there's already a lightmap here, return
        if (num_image != InvalidImageNumber)
        {
            return nullptr;
        }

        // On ajoute un pixel de séparation entre chaque image (facilite le debug)
        unsigned int tmp_w = img.getWidth() /*+ 1*/;
        unsigned int tmp_h = img.getHeight() /*+ 1*/;

        // If we're too small, return
        if (tmp_w > width || tmp_h > height)
        {
            return nullptr;
        }

        // If we're just right, accept
        if (tmp_w == width && tmp_h == height)
        {
            num_image = num;
            return this;
        }

        // Otherwise, gotta split this node and create some kids
        child[0] = new TTextureNode;
        child[1] = new TTextureNode;

        // On recopie les dimensions du rectangle
        child[0]->x = child[1]->x = x;
        child[0]->y = child[1]->y = y;
        child[0]->width = child[1]->width = width;
        child[0]->height = child[1]->height = height;

        // Decide which way to split
        if (width - tmp_w > height - tmp_h)
        {
            child[0]->width = tmp_w;
            child[1]->width -= tmp_w;
            child[1]->x += tmp_w;
        }
        else
        {
            child[0]->height = tmp_h;
            child[1]->height -= tmp_h;
            child[1]->y += tmp_h;
        }

        // Insert into first child we created
        return child[0]->insertImage(img, num);
    }
}


/**
 * Donne l'abscisse d'une image dans la texture.
 *
 * \param num Numéro de l'image.
 * \return Abscisse de l'image.
 *
 * \sa CMetaTexture::TTextureNode::getY
 ******************************/

unsigned int CMetaTexture::TTextureNode::getX(unsigned int num)
{
    if (num_image == num)
        return x;

    unsigned int tmp = InvalidImageNumber;

    if (child[0])
    {
        tmp = child[0]->getX(num);

        if (tmp != InvalidImageNumber)
        {
            return tmp;
        }
    }

    if (child[1])
    {
        tmp = child[1]->getX(num);
    }

    return tmp; // -1
}


/**
 * Donne l'ordonnée d'une image dans la texture.
 *
 * \param num Numéro de l'image.
 * \return Ordonnée de l'image.
 *
 * \sa CMetaTexture::TTextureNode::getX
 ******************************/

unsigned int CMetaTexture::TTextureNode::getY(unsigned int num)
{
    if (num_image == num)
        return y;

    unsigned int tmp = InvalidImageNumber;

    if (child[0])
    {
        tmp = child[0]->getY(num);

        if (tmp != InvalidImageNumber)
        {
            return tmp;
        }
    }

    if (child[1])
    {
        tmp = child[1]->getY(num);
    }

    return tmp; // -1
}


/**
 * Supprime une image dans la texture.
 *
 * \param num Numéro de l'image.
 ******************************/

void CMetaTexture::TTextureNode::deleteImage(unsigned int num)
{
    if (child[0])
    {
        child[0]->deleteImage(num);
    }

    if (child[1])
    {
        child[1]->deleteImage(num);
    }

    if (num_image == num)
    {
        num_image = InvalidImageNumber;
    }
    else if (num_image > num)
    {
        --num_image;
    }
}

} // Namespace TE
