/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/opengl.h"
#include "Core/CString.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * V�rifie les erreurs OpenGL apr�s un appel de fonction et log si n�cessaire.
 *
 * \param function Nom de la fonction OpenGL appell�e.
 * \param filename Nom du fichier (__FILE__).
 * \param line Num�ro de ligne dans le fichier (__LINE__).
 *
 * \todo Surcharger toutes les API OpenGL pour int�grer cette gestion des erreurs en Debug.
 ******************************/

void checkOpenGLError(const char * function, const char * filename, int line)
{
    GLenum error = glGetError();
    switch (error)
    {
        case GL_NO_ERROR:
            // D�commenter la ligne suivante pour logguer tous les appels � OpenGL (tr�s verbeux !)
            //CGame::instance().log(CString("GL %1 (fichier %2, ligne %3)").arg(function).arg(filename).arg(line));
            return;
        case GL_INVALID_VALUE:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_INVALID_VALUE").arg(function).arg(filename).arg(line));
            break;
        case GL_INVALID_ENUM:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_INVALID_ENUM").arg(function).arg(filename).arg(line));
            break;
        case GL_INVALID_OPERATION:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_INVALID_OPERATION").arg(function).arg(filename).arg(line));
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_INVALID_FRAMEBUFFER_OPERATION").arg(function).arg(filename).arg(line));
            break;
        case GL_OUT_OF_MEMORY:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_OUT_OF_MEMORY").arg(function).arg(filename).arg(line));
            break;
        case GL_STACK_UNDERFLOW:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_STACK_UNDERFLOW").arg(function).arg(filename).arg(line));
            break;
        case GL_STACK_OVERFLOW:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg("GL_STACK_OVERFLOW").arg(function).arg(filename).arg(line));
            break;
        default:
            gApplication->log(CString::fromUTF8("Erreur OpenGL (%1) dans %2 (fichier %3, ligne %4).").arg(error).arg(function).arg(filename).arg(line));
            break;
    }
}


void GLAPIENTRY OpenGLMessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam)
{
    if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
    {
        return;
    }

    CString type_str;
    CString severity_str;

    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR:
            type_str = "ERROR";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            type_str = "DEPRECATED_BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            type_str = "UNDEFINED_BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            type_str = "PORTABILITY";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            type_str = "PERFORMANCE";
            break;
        case GL_DEBUG_TYPE_OTHER:
            type_str = "OTHER";
            break;
        case GL_DEBUG_TYPE_MARKER:
            type_str = "MARKER";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            type_str = "PUSH_GROUP";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            type_str = "POP_GROUP";
            break;
    }

    switch (severity)
    {
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            severity_str = "NOTIFICATION";
            break;
        case GL_DEBUG_SEVERITY_HIGH:
            severity_str = "HIGH";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            severity_str = "MEDIUM";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            severity_str = "LOW";
            break;
    }

    if (length < 0)
    {
        gApplication->log(CString("OpenGL callback [") + severity_str + "] [" + type_str + "] : " + message);
    }
    else
    {
        gApplication->log(CString("OpenGL callback [") + severity_str + "] [" + type_str + "] : " + CString(message, length));
    }
}

} // Namespace TE
