/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CBuffer2D.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

CBuffer2D::CBuffer2D() :
    m_data     {},
    m_textures {},
    m_indices  {},
    m_buffer   {std::make_shared<CBuffer<TVertex2D>>()}
{

}


/**
 * R�serve de la place pour de nouveaux sommets.
 *
 * \param count Nombre de sommets suppl�mentaires qui seront ajout�s � la liste.
 ******************************/

void CBuffer2D::reserveSize(unsigned int count)
{
    m_data.reserve(m_data.size() + count);
    m_textures.reserve(m_textures.size() + count);
    m_indices.reserve(m_indices.size() + count);
}


/**
 * Ajoute un sommet � la liste.
 *
 * \param vertex Sommet � ajouter.
 ******************************/

void CBuffer2D::addVertex(const TVertex2D& vertex, TTextureId texture)
{
    m_indices.push_back(m_data.size());
    m_data.push_back(vertex);
    m_textures.push_back(texture);
}


void CBuffer2D::addRectangle(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color)
{
    unsigned int index = m_data.size();
    m_data.reserve(index + 4);
    m_textures.reserve(m_textures.size() + 6);
    m_indices.reserve(m_indices.size() + 6);

    m_data.push_back(TVertex2D{ position, glm::vec2{}, color });
    m_data.push_back(TVertex2D{ glm::vec2{position.x + size.x, position.y}, glm::vec2{}, color });
    m_data.push_back(TVertex2D{ glm::vec2{position.x + size.x, position.y + size.y}, glm::vec2{}, color });
    m_data.push_back(TVertex2D{ glm::vec2{position.x, position.y + size.y}, glm::vec2{}, color });

    for (int i = 0; i < 6; ++i)
    {
        m_textures.push_back(CTextureManager::NoTexture);
    }

    m_indices.push_back(index + 0);
    m_indices.push_back(index + 1);
    m_indices.push_back(index + 2);
    m_indices.push_back(index + 0);
    m_indices.push_back(index + 2);
    m_indices.push_back(index + 3);
}


void CBuffer2D::addRectangle(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color, TTextureId texture, const glm::vec2& coordsMin, const glm::vec2& coordsMax)
{
    unsigned int index = m_data.size();
    m_data.reserve(index + 4);
    m_textures.reserve(m_textures.size() + 6);
    m_indices.reserve(m_indices.size() + 6);

    m_data.push_back(TVertex2D{ position, coordsMin, color });
    m_data.push_back(TVertex2D{ glm::vec2{position.x + size.x, position.y}, glm::vec2{ coordsMax.x, coordsMin.y }, color });
    m_data.push_back(TVertex2D{ glm::vec2{position.x + size.x, position.y + size.y}, coordsMax, color });
    m_data.push_back(TVertex2D{ glm::vec2{position.x, position.y + size.y}, glm::vec2{ coordsMin.x, coordsMax.y }, color });

    for (int i = 0; i < 6; ++i)
    {
        m_textures.push_back(texture);
    }

    m_indices.push_back(index + 0);
    m_indices.push_back(index + 1);
    m_indices.push_back(index + 2);
    m_indices.push_back(index + 0);
    m_indices.push_back(index + 2);
    m_indices.push_back(index + 3);
}


/**
 * Supprime toutes les donn�es du buffer.
 ******************************/

void CBuffer2D::clear()
{
    m_data.clear();
    m_textures.clear();
    m_indices.clear();
}


unsigned int CBuffer2D::draw(TDisplayMode displayMode, const std::shared_ptr<CShader>& shader) const
{
    return m_buffer->draw(displayMode, shader);
}


/**
 * Met-�-jour le buffer graphique.
 ******************************/

void CBuffer2D::update()
{
    TBufferTextureVector textures(m_textures.size());
    for (const auto& text_id : m_textures)
    {
        textures.push_back(TBufferTexture(1, text_id));
    }

    m_buffer->setIndices(m_indices);
    m_buffer->setTextures(textures);

    m_buffer->setData(m_data);
    m_buffer->update();
}

} // Namespace TE
