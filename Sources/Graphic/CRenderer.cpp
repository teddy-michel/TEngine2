/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CRenderer.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CShaderManager.hpp"
#include "Graphic/CMegaBuffer.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

CRenderer * gRenderer = nullptr;


/**
 * Constructeur par d�faut.
 ******************************/

CRenderer::CRenderer() :
    m_mode                   { Normal },
    m_currentShader          { 0 },
    m_vertexData             {},
    m_vertexDataRefCount     {},
    m_linksSSBO              { 0 },
    m_modelsSSBO             { 0 },
    m_megaBuffer             { nullptr },
    m_supportBindlessTexture { false }
{
    assert(gRenderer == nullptr);
    gRenderer = this;
}


/**
 * Destructeur.
 ******************************/

CRenderer::~CRenderer()
{
    gApplication->log("CRenderer::~CRenderer", ILogger::Debug);
    gRenderer = nullptr;

    glDeleteBuffers(1, &m_modelsSSBO);
    checkOpenGLError("glDeleteBuffers", __FILE__, __LINE__);
    glDeleteBuffers(1, &m_linksSSBO);
    checkOpenGLError("glDeleteBuffers", __FILE__, __LINE__);
}


/**
 * Initialise le moteur de rendu.
 *
 * \return True si l'initialisation s'est bien pass�e, false en cas d'erreur.
 ******************************/

bool CRenderer::init()
{
    gApplication->log("CRenderer::init", ILogger::Debug);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        gApplication->log("Erreur lors de l'initialisation de Glew.", ILogger::Error);
        return false;
    }

    // Check OpenGL extensions
    m_supportBindlessTexture = (glewGetExtension("GL_ARB_bindless_texture") == GL_TRUE);

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(OpenGLMessageCallback, 0);

    // Initialize OpenGL
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Couleurs
    glEnable(GL_COLOR_MATERIAL);

    // Textures
    glEnable(GL_TEXTURE_2D);
    glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Transparence
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.0f);

    // Autres param�tres
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_TEXTURE_COMPRESSION_HINT, GL_FASTEST);

    glCullFace(GL_FRONT);
    checkOpenGLError("glCullFace", __FILE__, __LINE__);

    // Initialise les gestionnaires de textures et de polices
    new CShaderManager{};
    new CTextureManager{};
    new CFontManager{};

    gShaderManager->setPath("../../../Resources/shaders/");

    m_megaBuffer = std::make_shared<CMegaBuffer<TVertex3D>>(0x1000000);

    // Cr�ation des SSBO
    glGenBuffers(1, &m_linksSSBO);
    checkOpenGLError("glGenBuffers", __FILE__, __LINE__);
    glGenBuffers(1, &m_modelsSSBO);
    checkOpenGLError("glGenBuffers", __FILE__, __LINE__);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_linksSSBO);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    // Pour le DEBUG (n'a pas d'effet)
    glObjectLabel(GL_BUFFER, m_linksSSBO, -1, CString("Shader Storage Buffer #%1 (links)").arg(m_linksSSBO).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_modelsSSBO);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    // Pour le DEBUG (n'a pas d'effet)
    glObjectLabel(GL_BUFFER, m_modelsSSBO, -1, CString("Shader Storage Buffer #%1 (models)").arg(m_modelsSSBO).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    m_vertexData.reserve(1024);
    m_vertexDataRefCount.reserve(1024);

    m_vertexData.push_back(TVertexData{});
    m_vertexDataRefCount.push_back(0xFFFFFFFF);

    return true;
}


/**
 * M�thode appell�e au d�but d'une frame.
 ******************************/

void CRenderer::startFrame()
{
    sendVertexData();
    sendModelData();

    // Clear the screen
    // TODO: supprimer car d�j� fait dans CRenderParams::render()
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    checkOpenGLError("glClearColor", __FILE__, __LINE__);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //checkOpenGLError("glClear", __FILE__, __LINE__);
}


/**
 * M�thode appell�e � la fin d'une frame.
 * Doit �tre appell�e apr�s startFrame.
 ******************************/

void CRenderer::endFrame()
{

}


/**
 * M�thode appell�e au d�but du rendu de l'interface graphique.
 * Doit �tre appell�e apr�s startFrame.
 ******************************/

void CRenderer::startGUI()
{
    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "2D rendering");
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    glDisable(GL_DEPTH_TEST);
    checkOpenGLError("glDisable", __FILE__, __LINE__);
}


/**
 * M�thode appell�e � la fin du rendu de l'interface graphique.
 * Doit �tre appell�e apr�s startGUI et avant endFrame.
 ******************************/

void CRenderer::endGUI()
{
    glEnable(GL_DEPTH_TEST);
    checkOpenGLError("glEnable", __FILE__, __LINE__);

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);
}


/**
 * Indique si une fonctionnalit� est support�e par le moteur de rendu.
 *
 * \param functionality Identifiant de la fonctionnalit� � tester.
 * \return Bool�en.
 ******************************/

bool CRenderer::support(TFunctionnality functionality) const
{
    switch (functionality)
    {
        case TFunctionnality::BindlessTexture:
            return m_supportBindlessTexture;
    }

    return false;
}


void CRenderer::bindShader(GLuint shader)
{
    if (shader != m_currentShader)
    {
        glUseProgram(shader);
        checkOpenGLError("glUseProgram", __FILE__, __LINE__);
        m_currentShader = shader;
    }
}


TVertexDataRef CRenderer::getVertexDataRef(const TVertexData& data)
{
    assert(m_vertexData.size() == m_vertexDataRefCount.size());

    TVertexDataRef ref = 0;
    TVertexDataRef refEmpty = 0xFFFFFFFF;
    bool found = false;

    while (ref < m_vertexData.size())
    {
        if (m_vertexData[ref] == data)
        {
            found = true;
            break;
        }

        if (refEmpty != 0xFFFFFFFF && m_vertexDataRefCount[ref] == 0)
        {
            refEmpty = ref;
        }

        ++ref;
    }

    if (!found && refEmpty != 0xFFFFFFFF)
    {
        ref = refEmpty;
        found = true;
    }

    if (found)
    {
        if (ref > 0) // L'�l�ment 0 n'utilise pas le compteur de r�f�rence
        {
            ++m_vertexDataRefCount[ref];
        }
    }
    else
    {
        m_vertexData.push_back(data);
        m_vertexDataRefCount.push_back(1);
        ref = static_cast<TVertexDataRef>(m_vertexData.size() - 1);
    }

    return ref;
}


void CRenderer::unrefVertexData(const std::vector<TVertexDataRef>& refs)
{
    assert(m_vertexData.size() == m_vertexDataRefCount.size());

    for (auto& ref : refs)
    {
        assert(ref < m_vertexData.size());
        if (ref > 0 && m_vertexDataRefCount[ref] > 0)
        {
            --m_vertexDataRefCount[ref];
        }
    }
}


/**
 * M�thode appell�e au d�but de la frame.
 ******************************/

void CRenderer::sendVertexData()
{
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_linksSSBO);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    // Pour le DEBUG (l'appel dans le constructeur n'a pas d'effet)
    glObjectLabel(GL_BUFFER, m_linksSSBO, -1, CString("Shader Storage Buffer #%1 (links)").arg(m_linksSSBO).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glBufferData(GL_SHADER_STORAGE_BUFFER, m_vertexData.size() * sizeof(TVertexData), &m_vertexData[0], GL_DYNAMIC_DRAW);
    checkOpenGLError("glBufferData", __FILE__, __LINE__);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
}


/**
 * M�thode appell�e au d�but de la frame.
 ******************************/

void CRenderer::sendModelData()
{
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_modelsSSBO);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    // Pour le DEBUG (l'appel dans le constructeur n'a pas d'effet)
    glObjectLabel(GL_BUFFER, m_modelsSSBO, -1, CString("Shader Storage Buffer #%1 (models)").arg(m_modelsSSBO).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    const auto models = gEntityManager->getModelMatrices();

    glBufferData(GL_SHADER_STORAGE_BUFFER, models.size() * sizeof(glm::mat4), &models[0], GL_DYNAMIC_DRAW);
    checkOpenGLError("glBufferData", __FILE__, __LINE__);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
}

} // Namespace TE
