/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CFrameBuffer.hpp"
#include "Core/CString.hpp"
#include "Core/CApplication.hpp"

#include <algorithm>


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

CFrameBuffer::CFrameBuffer() :
    m_fbo          { 0 },
    m_depthBuffer  { 0 },
    m_colorBuffers { 0 }
{
    // Cr�ation du frame buffer object
    glGenFramebuffers(1, &m_fbo);
    checkOpenGLError("glGenFramebuffers", __FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo); // TODO: a-t-on besoin de binder le buffer pour le nommer ?
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    // Pour le DEBUG
    glObjectLabel(GL_FRAMEBUFFER, m_fbo, -1, CString("Framebuffer #%1").arg(m_fbo).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);
}


/**
 * Destructeur.
 ******************************/

CFrameBuffer::~CFrameBuffer()
{
    glDeleteFramebuffers(1, &m_fbo);
    checkOpenGLError("glDeleteFramebuffers", __FILE__, __LINE__);

    if (m_depthBuffer != 0)
    {
        glDeleteRenderbuffers(1, &m_depthBuffer);
        checkOpenGLError("glDeleteRenderbuffers", __FILE__, __LINE__);
    }

    for (int i = 0; i < MaxColorAttachment; ++i)
    {
        glDeleteRenderbuffers(1, &m_colorBuffers[i]);
        checkOpenGLError("glDeleteRenderbuffers", __FILE__, __LINE__);
    }
}


void CFrameBuffer::bind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);
}


void CFrameBuffer::unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);
}


bool CFrameBuffer::check() const
{
    if (m_fbo == 0)
    {
        return false;
    }

    bool fboIsValid = false;

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    switch (glCheckFramebufferStatus(GL_FRAMEBUFFER))
    {
        case GL_FRAMEBUFFER_COMPLETE:
            fboIsValid = true;
            break;

        case GL_FRAMEBUFFER_UNDEFINED:
            gApplication->log("FBO is undefined.", ILogger::Warning);
            break;

        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            gApplication->log("FBO attachment points are incomplete.", ILogger::Warning);
            break;

        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            gApplication->log("FBO does not have at least one image attached to it.", ILogger::Warning);
            break;

        case GL_FRAMEBUFFER_UNSUPPORTED:
            gApplication->log("FBO combination of internal formats of the attached images violates an implementation-dependent set of restrictions.", ILogger::Warning);
            break;

        default:
            gApplication->log("FBO error.", ILogger::Warning);
            break;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    return fboIsValid;
}


bool CFrameBuffer::createColorBuffer(int width, int height, TColorFormat format, unsigned int attachment)
{
    if (attachment >= MaxColorAttachment)
    {
        gApplication->log("Max color attachments", ILogger::Error);
        return false;
    }

    GLint maxAttachment = 0;
    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxAttachment);
    assert(maxAttachment >= 0);

    if (attachment >= static_cast<unsigned int>(maxAttachment))
    {
        gApplication->log("Max color attachments", ILogger::Error);
        return false;
    }

    if (!checkBufferSize(width, height))
    {
        return false;
    }

    if (m_colorBuffers[attachment] != 0)
    {
        glDeleteRenderbuffers(1, &m_colorBuffers[attachment]);
        m_colorBuffers[attachment] = 0;
        checkOpenGLError("glDeleteRenderbuffers", __FILE__, __LINE__);
    }

    GLenum formatGL = 0;

    switch (format)
    {
        case TColorFormat::Red:
            formatGL = GL_R8;
            break;
        case TColorFormat::RG:
            formatGL = GL_RG8;
            break;
        case TColorFormat::RGB:
            formatGL = GL_RGB8;
            break;
        case TColorFormat::RGBA:
            formatGL = GL_RGBA8;
            break;
    }

    glGenRenderbuffers(1, &m_colorBuffers[attachment]);
    checkOpenGLError("glGenRenderbuffers", __FILE__, __LINE__);
    glBindRenderbuffer(GL_RENDERBUFFER, m_colorBuffers[attachment]);
    checkOpenGLError("glBindRenderbuffer", __FILE__, __LINE__);
    glRenderbufferStorage(GL_RENDERBUFFER, formatGL, width, height);
    checkOpenGLError("glRenderbufferStorage", __FILE__, __LINE__);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    checkOpenGLError("glBindRenderbuffer", __FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachment, GL_RENDERBUFFER, m_colorBuffers[attachment]);
    checkOpenGLError("glFramebufferRenderbuffer", __FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    return true;
}


bool CFrameBuffer::removeColorBuffer(unsigned int attachment)
{
    if (attachment >= MaxColorAttachment)
    {
        gApplication->log("Max color attachments", ILogger::Error);
        return false;
    }

    GLint maxAttachment = 0;
    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxAttachment);
    assert(maxAttachment >= 0);

    if (attachment >= static_cast<unsigned int>(maxAttachment))
    {
        gApplication->log("Max color attachments", ILogger::Error);
        return false;
    }

    if (m_colorBuffers[attachment] == 0)
    {
        return false;
    }

    glDeleteRenderbuffers(1, &m_colorBuffers[attachment]);
    m_colorBuffers[attachment] = 0;
    checkOpenGLError("glDeleteRenderbuffers", __FILE__, __LINE__);

    return true;
}


bool CFrameBuffer::createDepthBuffer(int width, int height, TDepthFormat format)
{
    if (!checkBufferSize(width, height))
    {
        return false;
    }

    if (m_depthBuffer != 0)
    {
        glDeleteRenderbuffers(1, &m_depthBuffer);
        m_depthBuffer = 0;
        checkOpenGLError("glDeleteRenderbuffers", __FILE__, __LINE__);
    }

    GLenum formatGL = 0;
    GLenum attachment = 0;

    switch (format)
    {
        case TDepthFormat::Depth16:
            formatGL = GL_DEPTH_COMPONENT16;
            attachment = GL_DEPTH_ATTACHMENT;
            break;
        case TDepthFormat::Depth24:
            formatGL = GL_DEPTH_COMPONENT24;
            attachment = GL_DEPTH_ATTACHMENT;
            break;
        case TDepthFormat::Depth32:
            formatGL = GL_DEPTH_COMPONENT32F;
            attachment = GL_DEPTH_ATTACHMENT;
            break;
        case TDepthFormat::Depth24_Stencil8:
            formatGL = GL_DEPTH24_STENCIL8;
            attachment = GL_DEPTH_STENCIL_ATTACHMENT;
            break;
        case TDepthFormat::Stencil8:
            formatGL = GL_STENCIL_INDEX8;
            attachment = GL_STENCIL_ATTACHMENT;
            break;
    }

    glGenRenderbuffers(1, &m_depthBuffer);
    checkOpenGLError("glGenRenderbuffers", __FILE__, __LINE__);
    glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);
    checkOpenGLError("glBindRenderbuffer", __FILE__, __LINE__);
    glRenderbufferStorage(GL_RENDERBUFFER, formatGL, width, height);
    checkOpenGLError("glRenderbufferStorage", __FILE__, __LINE__);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    checkOpenGLError("glBindRenderbuffer", __FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, m_depthBuffer);
    checkOpenGLError("glFramebufferRenderbuffer", __FILE__, __LINE__);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    return true;
}


bool CFrameBuffer::attachTexture(TTextureId texture, unsigned int attachment)
{
    if (attachment >= MaxColorAttachment)
    {
        gApplication->log("Max color attachments", ILogger::Error);
        return false;
    }

    GLint maxAttachment = 0;
    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxAttachment);
    assert(maxAttachment >= 0);

    if (attachment >= static_cast<unsigned int>(maxAttachment))
    {
        gApplication->log("Max color attachments", ILogger::Error);
        return false;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    bool res = gTextureManager->attachFramebufferTexture(attachment, texture);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkOpenGLError("glBindFramebuffer", __FILE__, __LINE__);

    return res;
}


void CFrameBuffer::setNoColorBufferFlag(bool flag)
{
    if (flag)
    {
        glDrawBuffer(GL_NONE); // No color buffer is drawn to
    }
    else
    {
        glDrawBuffer(GL_BACK);
    }

    checkOpenGLError("glDrawBuffer", __FILE__, __LINE__);
}


bool CFrameBuffer::checkBufferSize(int width, int height) const
{
    if (width == 0 || height == 0)
    {
        gApplication->log(CString::fromUTF8("Les dimensions d'un FBO ne peuvent pas �tre nulles."), ILogger::Error);
        return false;
    }

    GLint maxSize = 0;
    glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &maxSize);

    GLint maxWidth = 0;
    glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &maxWidth);
    maxWidth = std::min(maxWidth, maxSize);

    if (width > maxWidth)
    {
        gApplication->log(CString::fromUTF8("La largeur d'un FBO ne peut pas d�passer %1 pixels.").arg(maxWidth), ILogger::Error);
        return false;
    }

    GLint maxHeight = 0;
    glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &maxHeight);
    maxHeight = std::min(maxHeight, maxSize);

    if (height > maxHeight)
    {
        gApplication->log(CString::fromUTF8("La hauteur d'un FBO ne peut pas d�passer %1 pixels.").arg(maxHeight), ILogger::Error);
        return false;
    }

    return true;
}

} // Namespace TE
