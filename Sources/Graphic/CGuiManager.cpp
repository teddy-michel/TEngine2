/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <glm/gtc/matrix_transform.hpp>

#include "Core/CApplication.hpp"
#include "Graphic/CGuiManager.hpp"
#include "Graphic/CShaderManager.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/IGuiObject.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

CGuiManager::CGuiManager() :
    m_buffer  {},
    m_shader  { nullptr },
    m_objects {}
{
    // Chargement du shader
    m_shader = gShaderManager->loadFromFiles("shader2d.vert", "shader2d.frag");
    //m_shader = std::make_shared<CShader>();
    //m_shader->loadFromFiles("../../../Resources/shaders/shader2d.vert", "../../../Resources/shaders/shader2d.frag");
    m_shader->setUniformValue("model", glm::mat4{ 1.0f });
}


void CGuiManager::addObject(const std::shared_ptr<IGuiObject>& object)
{
    assert(object != nullptr);
    auto it = std::find(m_objects.begin(), m_objects.end(), object);
    if (it == m_objects.end())
    {
        m_objects.push_back(object);
    }
}


void CGuiManager::removeObject(const std::shared_ptr<IGuiObject>& object)
{
    assert(object != nullptr);
    m_objects.remove(object);
}


void CGuiManager::update(float frameTime)
{
    // Mise-�-jour des objets graphiques
    for (const auto& object : m_objects)
    {
        object->update(frameTime);
    }
}

void CGuiManager::draw()
{
    gRenderer->startGUI();
    glm::mat4 projection = glm::ortho(0.0f, static_cast<float>(gApplication->getWidth()), static_cast<float>(gApplication->getHeight()), 0.0f, -1.0f, 1.0f);
    m_shader->setUniformValue("proj", projection);

    m_buffer.clear();

    // Mise-�-jour du buffer avec chaque objet
    for (const auto& object : m_objects)
    {
        object->draw(&m_buffer);
    }

    m_buffer.update();

    // Affichage des buffers
    m_buffer.draw(TDisplayMode::Normal, m_shader);

    gRenderer->endGUI();
}

} // Namespace TE
