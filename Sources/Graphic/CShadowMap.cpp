/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CShadowMap.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CShader.hpp"
#include "Core/CApplication.hpp"
#include "Entities/ILight.hpp"
#include "Entities/CPointLight.hpp"
#include "Entities/CSpotLight.hpp"


namespace TE
{

/**
 * Constructeur.
 ******************************/

CShadowMap::CShadowMap(TShadowMapSize size) :
    m_size         { size },
    m_lights       {},
    m_lightsData   {},
    m_frameBuffer  {},
    m_depthTexture { 0 },
    m_ubo          { 0 }
{
    initFrameBuffer();

    // Cr�ation du uniform buffer
    glGenBuffers(1, &m_ubo);
    checkOpenGLError("glGenBuffers", __FILE__, __LINE__);
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    glBufferData(GL_UNIFORM_BUFFER, m_lightsData.size() * sizeof(TLight), nullptr, GL_DYNAMIC_COPY);
    checkOpenGLError("glBufferData", __FILE__, __LINE__);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
}


/**
 * Destructeur.
 ******************************/

CShadowMap::~CShadowMap()
{
    if (m_depthTexture != 0)
    {
        glDeleteTextures(1, &m_depthTexture);
        checkOpenGLError("glDeleteTextures", __FILE__, __LINE__);
    }

    glDeleteBuffers(1, &m_ubo);
    checkOpenGLError("glDeleteBuffers", __FILE__, __LINE__);
}


/**
 * Met-�-jour les shadow maps.
 *
 * \todo V�rifier les shadow maps qui ont besoin d'�tre actualis�es.
 ******************************/

void CShadowMap::update(CMap * map)
{
    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Shadow maps update");
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    unsigned int shadowSize = 0;
    unsigned int lightsCount = 0;

    switch (m_size)
    {
        case ShadowMap_1_512:
            lightsCount = 1;
            shadowSize = 512;
            break;
        case ShadowMap_1_1024:
            lightsCount = 1;
            shadowSize = 1024;
            break;
        case ShadowMap_2_512:
            lightsCount = 2;
            shadowSize = 512;
            break;
        case ShadowMap_2_1024:
            lightsCount = 2;
            shadowSize = 1024;
            break;
        case ShadowMap_4_512:
            lightsCount = 4;
            shadowSize = 512;
            break;
        case ShadowMap_4_1024:
            lightsCount = 4;
            shadowSize = 1024;
            break;
        case ShadowMap_8_512:
            lightsCount = 8 * 8;
            shadowSize = 512;
            break;
        case ShadowMap_8_1024:
            lightsCount = 8;
            shadowSize = 1024;
            break;
        case ShadowMap_16_512:
            lightsCount = 16;
            shadowSize = 512;
            break;
    }

    const unsigned int textureSize = lightsCount * shadowSize;

    m_frameBuffer.bind();

    for (unsigned int row = 0; row < lightsCount; ++row)
    {
        for (unsigned int col = 0; col < lightsCount; ++col)
        {
            ILight * light = m_lights[row * lightsCount + col];
            if (light == nullptr)
            {
                continue;
            }

            glm::mat4 view = light->getViewMatrix();

            const uint32_t light_type = ConvertFloatToType(m_lightsData[row * lightsCount + col].color.a) & ~LightEnabled;

            if (light->isEnabled())
            {
                m_lightsData[row * lightsCount + col].color.a = ConvertTypeToFloat(light_type | LightEnabled);

                if (light_type > Point_X_Neg && light_type <= Point_Z_Pos)
                {
                    // Remarque : si light_type == Point_X_Neg on n'a pas besoin d'appeller light->getViewMatrix(0)
                    //  car on l'a d�j� fait plus haut avec light->getViewMatrix()
                    //  d'o� le test "light_type > Point_X_Neg" et non "light_type >= Point_X_Neg"
                    view = light->getViewMatrix(light_type - Point_X_Neg);
                    CPointLight * pointLight = static_cast<CPointLight *>(light);
                    m_lightsData[row * lightsCount + col].attenuation.x = pointLight->getMaxDistance();
                    //m_lightsData[row * lightsCount + col].attenuation.y = pointLight->getAttenuationLinear();
                    //m_lightsData[row * lightsCount + col].attenuation.z = pointLight->getAttenuationQuadratic();
                }

                if (light_type == Spot)
                {
                    CSpotLight * spotLight = static_cast<CSpotLight *>(light);
                    m_lightsData[row * lightsCount + col].attenuation.x = spotLight->getMaxDistance();
                    //m_lightsData[row * lightsCount + col].attenuation.y = spotLight->getAttenuationLinear();
                    //m_lightsData[row * lightsCount + col].attenuation.z = spotLight->getAttenuationQuadratic();
                    m_lightsData[row * lightsCount + col].attenuation.w = spotLight->getAnglesRatio();
                }

                const auto position = light->getWorldPosition();
                const auto proj = light->getProjectionMatrix();

                // Couleur de la lumi�re
                light->getColor().toFloatRGB(&m_lightsData[row * lightsCount + col].color[0]);
                m_lightsData[row * lightsCount + col].position = glm::vec4(position, light->getIntensity());
                m_lightsData[row * lightsCount + col].matrix = proj * view;


                // On limite la zone de dessin
                glScissor(col * shadowSize, row * shadowSize, shadowSize, shadowSize);
                checkOpenGLError("glScissor", __FILE__, __LINE__);
                glEnable(GL_SCISSOR_TEST);
                checkOpenGLError("glEnable", __FILE__, __LINE__);

                // Rendu de la sc�ne du point de vue de la lumi�re
                CRenderParams params{ map, position, view, proj };
                params.setType(TRenderPass::ShadowMap);
                params.setShader(light->getShader());
                params.setMode(Colored);
                params.setViewport(col * shadowSize, row * shadowSize, shadowSize, shadowSize);
                params.render(light->getName());

                glDisable(GL_SCISSOR_TEST);
                checkOpenGLError("glDisable", __FILE__, __LINE__);
            }
            else
            {
                m_lightsData[row * lightsCount + col].color.a = ConvertTypeToFloat(light_type | LightDisabled);
            }
        }
    }

    m_frameBuffer.unbind();

    // Mise-�-jour des donn�es du buffer pour les lumi�res
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    GLvoid * ptr = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
    checkOpenGLError("glMapBuffer", __FILE__, __LINE__);

    if (ptr != nullptr)
    {
        memcpy(ptr, &m_lightsData[0], lightsCount * lightsCount * sizeof(TLight));
    }

    glUnmapBuffer(GL_UNIFORM_BUFFER);
    checkOpenGLError("glUnmapBuffer", __FILE__, __LINE__);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);
}


bool CShadowMap::addLight(ILight * light)
{
    assert(light != nullptr);

    int freeSpots[6] = { -1, -1, -1, -1, -1, -1 };
    int * currentFreeSpot = &freeSpots[0];
    for (std::size_t i = 0; i < m_lights.size(); ++i)
    {
        // La lumi�re est d�j� dans la liste
        if (m_lights[i] == light)
        {
            return true;
        }

        if (m_lights[i] == nullptr)
        {
            if (currentFreeSpot != nullptr && *currentFreeSpot == -1)
            {
                *currentFreeSpot = i;
                if (currentFreeSpot == &freeSpots[5])
                {
                    currentFreeSpot = nullptr;
                }
                else
                {
                    ++currentFreeSpot;
                }
            }
        }
    }

    if (freeSpots[0] >= 0)
    {
        uint32_t light_type = (light->isEnabled() ? LightEnabled : LightDisabled);

        switch (light->getLightType())
        {
            case TLightType::Spot:
            {
                CSpotLight * spotLight = static_cast<CSpotLight *>(light);
                m_lights[freeSpots[0]] = light;
                light_type |= Spot;
                m_lightsData[freeSpots[0]].color.a = ConvertTypeToFloat(light_type);
                m_lightsData[freeSpots[0]].attenuation.x = spotLight->getMaxDistance();
                //m_lightsData[freeSpots[0]].attenuation.y = spotLight->getAttenuationLinear();
                //m_lightsData[freeSpots[0]].attenuation.z = spotLight->getAttenuationQuadratic();
                m_lightsData[freeSpots[0]].attenuation.w = spotLight->getAnglesRatio();
                return true;
            }

            case TLightType::Directional:
                m_lights[freeSpots[0]] = light;
                light_type |= Directional;
                m_lightsData[freeSpots[0]].color.a = ConvertTypeToFloat(light_type);
                return true;

            case TLightType::Point:
                // currentFreeSpot is null if we have found 6 free spots
                if (currentFreeSpot == nullptr)
                {
                    CPointLight * pointLight = static_cast<CPointLight *>(light);
                    for (int i = 0; i < 6; ++i)
                    {
                        m_lights[freeSpots[i]] = light;
                        m_lightsData[freeSpots[i]].color.a = ConvertTypeToFloat(light_type | (Point_X_Neg + i));
                        m_lightsData[freeSpots[i]].attenuation.x = pointLight->getMaxDistance();
                        //m_lightsData[freeSpots[i]].attenuation.y = pointLight->getAttenuationLinear();
                        //m_lightsData[freeSpots[i]].attenuation.z = pointLight->getAttenuationQuadratic();
                    }
                }
                return true;
        }
    }

    // Aucun emplacement disponible
    gApplication->log("Too many lights", ILogger::Error);
    return false;
}


float CShadowMap::ConvertTypeToFloat(uint32_t type)
{
    // Because we can't do "reinterpret_cast<float>(uint32_t)"...
    union {
        uint32_t i;
        float f;
    } t;

    t.i = type;
    return t.f;
}


uint32_t CShadowMap::ConvertFloatToType(float value)
{
    // Because we can't do "reinterpret_cast<uint32_t>(float)"...
    union {
        uint32_t i;
        float f;
    } t;

    t.f = value;
    return t.i;
}


bool CShadowMap::removeLight(ILight * light)
{
    assert(light != nullptr);

    for (std::size_t i = 0; i < m_lights.size(); ++i)
    {
        if (m_lights[i] == light)
        {
            m_lights[i] = nullptr;
            m_lightsData[i].color.a = ConvertTypeToFloat(LightDisabled);
            //return true; // Because CPointLight uses 6 slots, not necessary adjacents
        }
    }

    return false;
}


void CShadowMap::updateShader(const std::shared_ptr<CShader>& shader)
{
    // TODO: utiliser le CTextureManager
    glActiveTexture(GL_TEXTURE4);
    checkOpenGLError("glActiveTexture", __FILE__, __LINE__);
    glBindTexture(GL_TEXTURE_2D, m_depthTexture);
    checkOpenGLError("glBindTexture", __FILE__, __LINE__);

    shader->setUniformValue("shadowmap", 4); // 4 = GL_TEXTURE4 (TODO: move to CShader loading?)
    shader->bindBuffer("LightArray", m_ubo, TShaderBinding::LightArray);
}


/**
 * Initialise le frame buffer et la texture de profondeur.
 ******************************/

void CShadowMap::initFrameBuffer()
{
    gApplication->log("Initializing frame buffer for shadow map...", ILogger::Debug);

    unsigned int textureSize = 0;
    unsigned int lightsCount = 0;

    switch (m_size)
    {
        case ShadowMap_1_512:
            lightsCount = 1;
            textureSize = 1 * 512;
            break;
        case ShadowMap_1_1024:
            lightsCount = 1;
            textureSize = 1 * 1024;
            break;
        case ShadowMap_2_512:
            lightsCount = 2 * 2;
            textureSize = 2 * 512;
            break;
        case ShadowMap_2_1024:
            lightsCount = 2 * 2;
            textureSize = 2 * 1024;
            break;
        case ShadowMap_4_512:
            lightsCount = 4 * 4;
            textureSize = 4 * 512;
            break;
        case ShadowMap_4_1024:
            lightsCount = 4 * 4;
            textureSize = 4 * 1024;
            break;
        case ShadowMap_8_512:
            lightsCount = 8 * 8;
            textureSize = 8 * 512;
            break;
        case ShadowMap_8_1024:
            lightsCount = 8 * 8;
            textureSize = 8 * 1024;
            break;
        case ShadowMap_16_512:
            lightsCount = 16 * 16;
            textureSize = 16 * 512;
            break;
    }

    m_lights.resize(lightsCount, nullptr);
    m_lightsData.resize(lightsCount);

    if (!m_frameBuffer.checkBufferSize(textureSize, textureSize))
    {
        gApplication->log("Can't create shadow map.", ILogger::Error);
        return;
    }

    m_frameBuffer.bind();

    // TODO: utiliser le CTextureManager pour g�n�rer une texture et la param�trer
    glGenTextures(1, &m_depthTexture);
    checkOpenGLError("glGenTextures", __FILE__, __LINE__);
    glBindTexture(GL_TEXTURE_2D, m_depthTexture);
    checkOpenGLError("glBindTexture", __FILE__, __LINE__);

    // Pour le DEBUG
    glObjectLabel(GL_TEXTURE, m_depthTexture, -1, CString("Shadow map depth texture").toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, textureSize, textureSize, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    checkOpenGLError("glTexImage2D", __FILE__, __LINE__);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);

    //glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depthTexture, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthTexture, 0);
    checkOpenGLError("glFramebufferTexture", __FILE__, __LINE__);

    m_frameBuffer.setNoColorBufferFlag(true);

    m_frameBuffer.check(); // TODO: move to update()
    m_frameBuffer.unbind();
}

} // Namespace TE
