/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstdlib>

#include "Graphic/IBufferBase.hpp"
#include "Graphic/CShader.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

IBufferBase::IBufferBase() :
    m_transparent { false },
    m_primitive   { PrimTriangle },
    m_indices     {},
    m_textures    {},
    m_dataRefs    {},
    m_entityId    { 0 }
{

}


/**
 * D�finit si le buffer contient des surfaces transparentes ou non.
 *
 * \param transparent True si le buffer des surfaces transparentes, false sinon.
 ******************************/

void IBufferBase::setTransparent(bool transparent)
{
    m_transparent = transparent;
}


/**
 * Donne le type de primitive � afficher.
 *
 * \return Type de primitive � afficher.
 *
 * \sa IBufferBase::setPrimitiveType
 ******************************/

TPrimitiveType IBufferBase::getPrimitiveType() const
{
    return m_primitive;
}


/**
 * Modifie les primitive.
 *
 * \param primitive Type de primitive � afficher.
 *
 * \sa IBufferBase::getPrimitiveType
 ******************************/

void IBufferBase::setPrimitiveType(TPrimitiveType primitive)
{
    m_primitive = primitive;
}


/**
 * D�finit le tableau des indices.
 *
 * \param indices Tableau des indices.
 ******************************/

void IBufferBase::setIndices(const std::vector<unsigned int>& indices)
{
    m_indices = indices;
}


/**
 * Retourne le tableau des indices.
 *
 * \return R�f�rence sur le tableau des indices.
 ******************************/

std::vector<unsigned int>& IBufferBase::getIndices()
{
    return m_indices;
}


/**
 * Retourne le tableau des indices.
 *
 * \return R�f�rence constante sur le tableau des indices.
 ******************************/

const std::vector<unsigned int>& IBufferBase::getIndices() const
{
    return m_indices;
}


/**
 * D�finit le tableau des textures.
 *
 * \param textures Tableau des textures.
 ******************************/

void IBufferBase::setTextures(const TBufferTextureVector& textures)
{
    m_textures = textures;
}


/**
 * Retourne le tableau des textures.
 *
 * \return R�f�rence sur le tableau des textures.
 ******************************/

TBufferTextureVector& IBufferBase::getTextures()
{
    return m_textures;
}


/**
 * Retourne le tableau des textures.
 *
 * \return R�f�rence constante sur le tableau des textures.
 ******************************/

const TBufferTextureVector& IBufferBase::getTextures() const
{
    return m_textures;
}


/**
 * D�finit l'identifiant de l'entit� associ�e au buffer.
 *
 * \param id Identifiant de l'entit�.
 ******************************/

void IBufferBase::setEntityId(TEntityId id)
{
    m_entityId = id;
}


/**
 * Supprime toutes les donn�es.
 ******************************/

void IBufferBase::clear()
{
    m_indices.clear();
    m_textures.clear();
}

} // Namespace TE
