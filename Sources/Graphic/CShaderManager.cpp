/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>

#include "Graphic/CShaderManager.hpp"
#include "Graphic/CShader.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

CShaderManager * gShaderManager = nullptr;


/**
 * Constructeur.
 ******************************/

CShaderManager::CShaderManager() :
    m_path    {},
    m_files   {},
    m_shaders {}
{
    assert(gShaderManager == nullptr);
    gShaderManager = this;
}


/**
 * Destructeur.
 ******************************/

CShaderManager::~CShaderManager()
{
    gShaderManager = nullptr;

    // Lib�re tous les shaders
    for (auto& [key, value] : m_files)
    {
        glDeleteShader(value);
        checkOpenGLError("glDeleteShader", __FILE__, __LINE__);
    }
}


void CShaderManager::setPath(const CString& path)
{
    m_path = path;
}


/**
 * Cr�e un shader � partir de deux fichiers.
 *
 * \param vertFile Nom du fichier contenant le vertex shader.
 * \param fragFile Nom du fichier contenant le fragment shader.
 * \return Pointeur sur le shader cr�e, ou nullptr en cas d'erreur.
 ******************************/

std::shared_ptr<CShader> CShaderManager::loadFromFiles(const CString& vertFile, const CString& fragFile)
{
    gApplication->log(CString("Load shader from files \"%1\" and \"%2\".").arg(vertFile).arg(fragFile));

    GLuint vertexShader = loadShaderFromFile(vertFile, VertexShader);
    if (vertexShader == 0)
    {
        return nullptr;
    }

    GLuint fragmentShader = loadShaderFromFile(fragFile, FragmentShader);
    if (fragmentShader == 0)
    {
        return nullptr;
    }

    const auto p = std::make_pair(vertexShader, fragmentShader);

    auto shaderIt = m_shaders.find(p);
    if (shaderIt == m_shaders.end())
    {
        CShader * shader = new CShader{ vertexShader, fragmentShader };
        auto shaderPtr = std::shared_ptr<CShader>(shader);
        //auto shaderPtr = std::make_shared<CShader>(vertexShader, fragmentShader);  // Constructor is private!

        m_shaders[p] = shaderPtr;
        return shaderPtr;
    }
    else
    {
        gApplication->log("Shader already loaded.");
        return shaderIt->second;
    }
}


std::shared_ptr<CShader> CShaderManager::loadVertexShaderFromFile(const CString& fileName)
{
    gApplication->log(CString("Load vertex shader from file \"%1\".").arg(fileName));

    GLuint vertexShader = loadShaderFromFile(fileName, VertexShader);
    if (vertexShader == 0)
    {
        return nullptr;
    }

    const auto p = std::make_pair(vertexShader, 0);

    auto shaderIt = m_shaders.find(p);
    if (shaderIt == m_shaders.end())
    {
        CShader * shader = new CShader{ vertexShader, 0 };
        auto shaderPtr = std::shared_ptr<CShader>(shader);
        //auto shaderPtr = std::make_shared<CShader>(vertexShader, 0);  // Constructor is private!

        m_shaders[p] = shaderPtr;
        return shaderPtr;
    }
    else
    {
        gApplication->log("Shader already loaded.");
        return shaderIt->second;
    }
}


std::shared_ptr<CShader> CShaderManager::loadFragmentShaderFromFile(const CString& fileName)
{
    gApplication->log(CString("Load fragment shader from file \"%1\".").arg(fileName));

    GLuint fragmentShader = loadShaderFromFile(fileName, FragmentShader);
    if (fragmentShader == 0)
    {
        return nullptr;
    }

    const auto p = std::make_pair(0, fragmentShader);

    auto shaderIt = m_shaders.find(p);
    if (shaderIt == m_shaders.end())
    {
        CShader * shader = new CShader{ 0, fragmentShader };
        auto shaderPtr = std::shared_ptr<CShader>(shader);
        //auto shaderPtr = std::make_shared<CShader>(0, fragmentShader);  // Constructor is private!

        m_shaders[p] = shaderPtr;
        return shaderPtr;
    }
    else
    {
        gApplication->log("Shader already loaded.");
        return shaderIt->second;
    }
}


/**
 * Charge un shader depuis un fichier.
 *
 * \param fileName Nom du fichier contenant le shader.
 * \return True si le chargement s'est bien pass�, false sinon.
 ******************************/

GLuint CShaderManager::loadShaderFromFile(const CString& fileName, TShaderType type)
{
    GLuint shader = 0;

    auto it = m_files.find(fileName);
    if (it == m_files.end())
    {
        // Read shader file
        std::ifstream file{ (m_path + fileName).toCharArray(), std::fstream::in | std::fstream::binary };
        if (!file)
        {
            gApplication->log(CString("Can't open file \"%1\".").arg(fileName), ILogger::Error);
            return 0;
        }

        file.seekg(0, std::ios::end);
        std::streampos length = file.tellg();
        file.seekg(0, std::ios::beg);
        std::streambuf * raw_buffer = file.rdbuf();
        std::vector<char> src(static_cast<std::size_t>(length) + 1);
        raw_buffer->sgetn(&src[0], length);
        src[length] = 0;

        shader = loadShaderFromSource(&src[0], type);
        m_files[fileName] = shader;

        // Pour le DEBUG
        glObjectLabel(GL_SHADER, shader, -1, CString("Shader #%1 (%2)").arg(shader).arg(fileName).toCharArray());
        checkOpenGLError("glObjectLabel", __FILE__, __LINE__);
    }
    else
    {
        shader = it->second;
    }

    return shader;
}


GLuint CShaderManager::loadShaderFromSource(const char * src, TShaderType type)
{
    assert(src != nullptr);

    GLuint shader = 0;

    // Create and compile the shader
    switch (type)
    {
        case VertexShader:
            shader = glCreateShader(GL_VERTEX_SHADER);
            break;
        case FragmentShader:
            shader = glCreateShader(GL_FRAGMENT_SHADER);
            break;
        case GeometryShader:
            shader = glCreateShader(GL_GEOMETRY_SHADER);
            break;
        case ComputeShader:
            shader = glCreateShader(GL_COMPUTE_SHADER);
            break;
    }

    checkOpenGLError("glCreateShader", __FILE__, __LINE__);

    if (shader == 0)
    {
        gApplication->log("Can't create shader.", ILogger::Error);
        return 0;
    }

    glShaderSource(shader, 1, &src, NULL);
    checkOpenGLError("glShaderSource", __FILE__, __LINE__);
    glCompileShader(shader);
    checkOpenGLError("glCompileShader", __FILE__, __LINE__);

    bool success = false;
    CString error = getShaderInfoLog(shader, success);

    if (!success)
    {
        gApplication->log("Error when compiling shader.", ILogger::Error);
        gApplication->log(error, ILogger::Error);
        return 0;
    }

    return shader;
}


CString CShaderManager::getShaderInfoLog(GLuint shader, bool& success) const
{
    GLint status = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    checkOpenGLError("glGetShaderiv", __FILE__, __LINE__);
    success = (status == GL_TRUE);

    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    checkOpenGLError("glGetShaderiv", __FILE__, __LINE__);

    if (length == 0)
    {
        return CString();
    }

    char * error_str = new char[length];
    glGetShaderInfoLog(shader, length, &length, error_str);
    checkOpenGLError("glGetShaderInfoLog", __FILE__, __LINE__);
    error_str[length - 1] = 0; // Au cas o�...
    CString error(error_str);
    delete[] error_str;
    return error;
}

} // Namespace TE
