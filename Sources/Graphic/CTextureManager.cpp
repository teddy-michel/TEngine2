/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "Graphic/CTextureManager.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CShader.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

CTextureManager * gTextureManager = nullptr;


/**
 * Constructeur par défaut.
 ******************************/

CTextureManager::CTextureManager() :
    m_mutex    {},
    m_textures {},
    m_ubo      { 0 }
{
    assert(gTextureManager == nullptr);
    gTextureManager = this;

    init();
}


/**
 * Destructeur.
 ******************************/

CTextureManager::~CTextureManager()
{
    unloadTextures();

    if (m_ubo != 0)
    {
        glDeleteBuffers(1, &m_ubo);
        checkOpenGLError("glDeleteBuffers", __FILE__, __LINE__);
    }

    gTextureManager = nullptr;
}


/**
 * Retourne l'identifiant de la texture à partir de son nom.
 *
 * \param name Nom de la texture.
 * \return Identifiant de la texture si elle a été chargée, sinon 0.
 ******************************/

TTextureId CTextureManager::getTextureId(const CString& name) const
{
    sf::Lock lock(m_mutex);

    for (TTextureInfoVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        if (it->name == name)
        {
            return std::distance(m_textures.begin(), it);
        }
    }

    return 0;
}


/**
 * Retourne le nom de la texture associée à un identifiant.
 *
 * \param id Identifiant de la texture.
 * \return Nom de la texture ou chaine vide si la texture n'existe pas.
 ******************************/

CString CTextureManager::getTextureName(TTextureId id) const
{
    sf::Lock lock(m_mutex);

    if (id < m_textures.size())
    {
        return m_textures[id].name;
    }

    // La texture n'a pas été trouvée
    return CString();
}


/**
 * Donne la taille totale occupée par les textures.
 * Cela concerne uniquement la mémoire vive, et pas la mémoire vidéo.
 *
 * \return Taille totale utilisée pour les textures.
 ******************************/

unsigned int CTextureManager::getMemorySize() const
{
    unsigned int s = 0;

    for (TTextureInfoVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        s += it->width * it->height * 4;
        s += sizeof(TTextureInfo);
    }

    return s;
}


unsigned int CTextureManager::getNumElements() const
{
    return m_textures.size();
}


/**
 * Initialise le gestionnaire de texture et crée la texture par défaut (échiquier).
 * Cette méthode doit être appelée dans le thread principal.
 ******************************/

void CTextureManager::init()
{
    sf::Lock lock(m_mutex);

    gApplication->log("Initialisation du gestionnaire de textures.");
    m_textures.reserve(100);

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        // Default handles for UBO
        GLuint64 handles[MaxTextures] = { 0 };

        // Création du uniform buffer
        glGenBuffers(1, &m_ubo);
        checkOpenGLError("glGenBuffers", __FILE__, __LINE__);
        glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

        glBufferData(GL_UNIFORM_BUFFER, sizeof(handles), &handles, GL_DYNAMIC_COPY);
        checkOpenGLError("glBufferData", __FILE__, __LINE__);

        // Pour le DEBUG (n'a pas d'effet)
        glObjectLabel(GL_BUFFER, m_ubo, -1, CString("Uniform Buffer #%1 (textures)").arg(m_ubo).toCharArray());
        checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    }

    //float maxanis = 0;
    //glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxanis);
    //CApplication::getApp()->log(CString("Filtrage anisotropique maximal : %1").arg(maxanis));

    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        m_texture[i] = NoTexture;
    }

    // Création et initialisation de la texture par défaut
    GLuint idgl1;
    glGenTextures(1, &idgl1);
    checkOpenGLError("glGenTextures", __FILE__, __LINE__);

    glBindTexture(GL_TEXTURE_2D, idgl1);
    checkOpenGLError("glBindTexture", __FILE__, __LINE__);
    m_texture[0] = 1;

    // Pour le DEBUG
    glObjectLabel(GL_TEXTURE, idgl1, -1, CString("Texture #%1 (Default texture)").arg(idgl1).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);

    GLubyte * checker = new GLubyte[4 * 64 * 64];

    // Construction de l'échiquier
    for (unsigned int i = 0; i < 64; ++i)
    {
        for (unsigned int j = 0; j < 64; ++j)
        {
            int c = (!(i & 8) ^ !(j & 8)) * 0xFF;

//#ifdef T_BIG_ENDIAN
            checker[(i * 256) + (j * 4) + 0] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 1] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 2] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 3] = static_cast<unsigned char>(0xFF);
/*
#else
            checker[(i * 256) + (j * 4) + 0] = static_cast<unsigned char>(0xFF);
            checker[(i * 256) + (j * 4) + 1] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 2] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 3] = static_cast<unsigned char>(c);
#endif
*/
        }
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, checker);
    checkOpenGLError("glTexImage2D", __FILE__, __LINE__);
    delete[] checker;

    // Ajout à la base de données
    TTextureInfo texture_info0{ 0, CString(), FilterNone };
    m_textures.push_back(texture_info0);

    TTextureInfo texture_info1{ idgl1, "default",  FilterNone };
    texture_info1.width  = 64;
    texture_info1.height = 64;
    m_textures.push_back(texture_info1);

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        GLuint64 handle = glGetTextureHandleARB(idgl1);
        glMakeTextureHandleResidentARB(handle);
        updateUBO(1, handle);
    }

    gApplication->log(CString::fromUTF8("Création de la texture par défaut (1)."));
}


/**
 * Charge une texture à partir d'un fichier image.
 *
 * Vérifie si la texture n'est pas déjà stockée en mémoire. Si c'est
 * le cas, la fonction retourne l'identifiant de la texture déjà stockée.
 * Sinon, elle retourne l'identifiant de la nouvelle texture.
 *
 * Cette fonction peut être appelée dans n'importe quel thread, le chargement
 * effectif de la texture dans la carte graphique se faisant dans le thread
 * principal de l'application.
 *
 * \param fileName Adresse du fichier contenant la texture.
 * \param filter   Filtrage à utiliser.
 * \param repeat   Indique si la texture doit se répéter sur les bords (true par défaut).
 * \return Identifiant de la texture.
 *
 * \todo Utiliser la liste des répertoires.
 ******************************/

TTextureId CTextureManager::loadTexture(const CString& fileName, TFilter filter, bool repeat)
{
    sf::Lock lock(m_mutex);

    TTextureId identifiant = DefaultTexture;
    bool exist = false;

    // On cherche si la texture existe déjà
    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        // La texture existe
        if (it->name == fileName)
        {
            identifiant = std::distance(m_textures.begin(), it);

            // La texture n'est pas chargée
            if (it->id == 0)
            {
                exist = true;
                break;
            }

            return identifiant;
        }
    }

    bool loaded = false;

    // Recherche de l'image dans chaque répertoire du gestionnaire
    //for (const auto& it : m_paths)
    {
        CString it = "../../../Resources/";
        CImage image;

        // On tente de charger la texture
        if (image.loadFromFile(it + fileName))
        {
            // Création d'une nouvelle texture et ajout à la liste
            if (exist)
            {
                m_textures[identifiant].filter = filter;
                m_textures[identifiant].repeat = repeat;
                m_textures[identifiant].action = TextureActionLoad;
                m_textures[identifiant].image  = image;
                m_textures[identifiant].width  = image.getWidth();
                m_textures[identifiant].height = image.getHeight();
            }
            else
            {
                identifiant = m_textures.size();

                TTextureInfo texture_info;

                texture_info.id     = 0;
                texture_info.name   = fileName;
                texture_info.filter = filter;
                texture_info.repeat = repeat;
                texture_info.action = TextureActionLoad;
                texture_info.image  = image;
                texture_info.width  = image.getWidth();
                texture_info.height = image.getHeight();

                m_textures.push_back(texture_info);
            }

            loaded = true;
            //break;
        }
    }

    // Impossible de charger la texture, texture par défault utilisée
    if (!loaded)
    {
        gApplication->log(CString::fromUTF8("Impossible de créer la texture \"%1\".").arg(fileName));
    }

    return identifiant;
}


/**
 * Crée une texture depuis une image déjà chargée.
 *
 * Vérifie si la texture n'est pas déjà stockée en mémoire. Si c'est le cas, la
 * fonction retourne l'id de la texture déjà stockée. Sinon, elle retourne
 * l'identifiant de la nouvelle texture.
 *
 * Cette fonction peut être appelée dans n'importe quel thread, le chargement
 * effectif de la texture dans la carte graphique se faisant dans le thread
 * principal de l'application.
 *
 * \param name   Nom de la texture.
 * \param image  Image à utiliser pour créer la texture.
 * \param filter Filtrage à utiliser.
 * \param repeat Indique si la texture doit se répéter sur les bords (true par défaut).
 * \return Identifiant de la texture.
 ******************************/

TTextureId CTextureManager::loadTexture(const CString& name, const CImage& image, TFilter filter, bool repeat)
{
    sf::Lock lock(m_mutex);

    TTextureId identifiant = DefaultTexture;
    bool exist = false;

    // On cherche si la texture existe déjà
    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        // La texture existe
        if (it->name == name)
        {
            identifiant = std::distance(m_textures.begin(), it);

            // La texture n'est pas chargée
            if (it->id == 0)
            {
                exist = true;
                break;
            }

            return identifiant;
        }
    }

    if (identifiant < DefaultTexture)
        return identifiant;

    // Création d'une nouvelle texture et ajout à la liste
    if (exist)
    {
        m_textures[identifiant].filter = filter;
        m_textures[identifiant].repeat = repeat;
        m_textures[identifiant].action = TextureActionLoad;
    }
    else
    {
        identifiant = m_textures.size();

        TTextureInfo texture_info;

        texture_info.id     = 0;
        texture_info.name   = name;
        texture_info.filter = filter;
        texture_info.repeat = repeat;
        texture_info.action = TextureActionLoad;

        m_textures.push_back(texture_info);
    }

    m_textures[identifiant].image  = image;
    m_textures[identifiant].width  = image.getWidth();
    m_textures[identifiant].height = image.getHeight();

    return identifiant;
}


/**
 * Attache une texture au framebuffer courant.
 *
 * \param attachment Numéro d'attachement (entre 0 et 15).
 * \param texture    Texture à utiliser.
 * \return True si la texture est chargée et attachée au framebuffer, false sinon.
 ******************************/

bool CTextureManager::attachFramebufferTexture(unsigned int attachment, TTextureId texture)
{
    assert(attachment < 16); // TODO: use constante

    if (texture >= m_textures.size())
    {
        return false;
    }

    if (m_textures[texture].id != 0)
    {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachment, GL_TEXTURE_2D, m_textures[texture].id, 0);
        checkOpenGLError("glFramebufferTexture2D", __FILE__, __LINE__);
        return true;
    }

    return false;
}


/**
 * Met-à-jour une texture à partir d'une image déjà chargée.
 *
 * Cette fonction peut être appelée dans n'importe quel thread, le chargement
 * effectif de la texture dans la carte graphique se faisant dans le thread
 * principal de l'application.
 *
 * \param id    Identifiant de la texture.
 * \param image Image à utiliser pour créer la texture.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CTextureManager::reloadTexture(TTextureId texture, const CImage& image)
{
    sf::Lock lock(m_mutex);

    if (texture <= DefaultTexture || texture >= m_textures.size())
    {
        return false;
    }

    m_textures[texture].action = TextureActionLoad;
    m_textures[texture].image  = image;
    m_textures[texture].width  = image.getWidth();
    m_textures[texture].height = image.getHeight();

    return true;
}


/**
 * Supprime toutes les textures.
 ******************************/

void CTextureManager::unloadTextures()
{
    sf::Lock lock(m_mutex);

    gApplication->log(CString::fromUTF8("Suppression des textures : %1 textures supprimées").arg(m_textures.size()));

    // Suppression des textures OpenGL
    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        it->action = TextureActionDelete;
        //glDeleteTextures(1, &(it->id));
        //it->id = 0;
    }

    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        m_texture[i] = NoTexture;
    }
}


/**
 * Supprime une texture à partir de son nom.
 *
 * \param name Nom de la texture.
 ******************************/

void CTextureManager::deleteTexture(const CString& name)
{
    sf::Lock lock(m_mutex);

    TTextureId id = NoTexture;

    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it, ++id)
    {
        if (it->name == name && id > DefaultTexture)
        {
            it->action = TextureActionDelete;
        }
    }
}


/**
 * Supprime une texture à partir de son identifiant.
 *
 * \param id Identifiant de la texture.
 ******************************/

void CTextureManager::deleteTexture(TTextureId texture)
{
    if (texture > DefaultTexture)
    {
        sf::Lock lock(m_mutex);

        TTextureInfoVector::iterator it = m_textures.begin();
        std::advance(it, texture);

        if (it != m_textures.end())
        {
            it->action = TextureActionDelete;
        }
    }
}


/**
 * Change la texture active sur une unité.
 * Cette méthode doit être appelée dans le thread principal.
 *
 * \param texture Identifiant de la texture.
 * \param unit    Unité de texture à utiliser (0 par défaut).
 ******************************/

void CTextureManager::bindTexture(TTextureId texture, unsigned int unit) const
{
    assert(unit < NumUnit);

    sf::Lock lock(m_mutex);

    if (m_texture[unit] != texture)
    {
        glActiveTexture(GL_TEXTURE0 + unit);

        if (texture < m_textures.size() && m_textures[texture].id != 0)
        {
            glBindTexture(GL_TEXTURE_2D, m_textures[texture].id);
#ifdef T_TEXTURES_KEEP_TIME
            m_textures[texture].time = getElapsedTime();
#endif
            m_texture[unit] = texture;
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, m_textures[DefaultTexture].id);
#ifdef T_TEXTURES_KEEP_TIME
            m_textures[DefaultTexture].time = getElapsedTime();
#endif
            m_texture[unit] = DefaultTexture;
        }
    }
}


/**
 * Inscrit la liste des textures dans le log.
 ******************************/

void CTextureManager::logTexturesList() const
{
    sf::Lock lock(m_mutex);

    //gApplication->log("\nListe des textures :");

    // On parcourt le tableau des textures
    for (unsigned int i = 0; i < m_textures.size(); ++i)
    {
        //gApplication->log(CString(" - %1 : %2 (%3)").arg(i).arg(m_textures[i].name).arg(m_textures[i].id));
    }

    //gApplication->log("\n");
}


/**
 * Effectue les tâches en attente (chargement ou déchargement d'une texture).
 * Cette méthode doit être appelée régulièrement dans le thread principal.
 *
 * Le nombre de tâche est limité à 10 par appel.
 ******************************/

void CTextureManager::doPendingTasks()
{
    unsigned int actions = 0;

    sf::Lock lock(m_mutex);

    // Reset des textures pour forcer un rebinding à chaque frame (au cas où...)
/*
    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        m_texture[i] = NoTexture;
    }
*/
    for (TTextureId id = 0; id < m_textures.size(); ++id)
    {
        switch (m_textures[id].action)
        {
            default:
                continue;

            case TextureActionLoad:
                PrivLoadTexture(id, m_textures[id]);
                ++actions;
                break;

            case TextureActionDelete:
                PrivDeleteTexture(id, m_textures[id]);
                ++actions;
                break;
        }

        if (actions > 10)
        {
            break;
        }
    }
}


CTextureManager::TTextureInfo CTextureManager::getTextureInfos(TTextureId texture) const
{
    sf::Lock lock(m_mutex);

    if (texture < m_textures.size())
    {
        return m_textures[texture];
    }
    else
    {
        return TTextureInfo{};
    }
}


/**
 * Charge une texture dans la carte graphique.
 * Cette méthode doit être appelée dans le thread principal.
 *
 * \todo Supprimer les données de l'image une fois celle-ci chargée en mémoire.
 * \todo Pouvoir créer une texture de profondeur (voir CSpotLight.cpp).
 *
 * \param id      Identifiant de la texture.
 * \param texture Structure contenant les informations de la texture.
 ******************************/

void CTextureManager::PrivLoadTexture(TTextureId id, TTextureInfo& texture)
{
    assert(id < m_textures.size());

    GLenum format = GL_RGBA;

    GLuint idgl = texture.id;
    int img_w = texture.image.getWidth();  // TODO: utiliser texture.width ???
    int img_h = texture.image.getHeight(); // TODO: utiliser texture.height ???

    // La texture a été supprimée de la carte graphique, on recrée un identifiant OpenGL
    if (texture.id == 0)
    {
        glGenTextures(1, &idgl);
        checkOpenGLError("glGenTextures", __FILE__, __LINE__);
    }

    // Initialisation de la texture OpenGL
    glActiveTexture(GL_TEXTURE0); // TODO: tester cet ajout
    checkOpenGLError("glActiveTexture", __FILE__, __LINE__);
    glBindTexture(GL_TEXTURE_2D, idgl);
    checkOpenGLError("glBindTexture", __FILE__, __LINE__);
    m_texture[0] = id;

    // Pour le DEBUG
    glObjectLabel(GL_TEXTURE, idgl, -1, CString("Texture #%1 (%2)").arg(idgl).arg(texture.name).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    // Répétition de la texture sur les bords
    if (texture.repeat)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
    }

    // Filtrage
    switch (texture.filter)
    {
        case FilterNone:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            break;

        case FilterBilinear:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            break;

        case FilterBilinearMipmap:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            break;

        case FilterTrilinear:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            break;
    }

    // Filtrage anisotrope
    //float anis = Game::renderer->getAnisotropic();
    float anis = 1.0f; // TODO: ajouter des entrées FilterAnisotropicXX (XX=2, 4, 8, 16 ?)
    if (anis > 1.0f)
    {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anis);
        checkOpenGLError("glTexParameterf", __FILE__, __LINE__);
    }

    if (img_w > 0 && img_h > 0)
    {
        // Mip-mapping
        if (texture.filter == FilterBilinearMipmap || texture.filter == FilterTrilinear)
        {
#if defined(GL_ARB_framebuffer_object) // OpenGL > 3.0 : glGenerateMipmap
            //#   if defined(GL_ARB_texture_storage) //glTexStorage2D
            //        glTexStorage2D(GL_TEXTURE_2D, 2, GL_RGBA8UI, img_w, img_h);
            //        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, img_w, img_h, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
            //#   else
            glTexImage2D(GL_TEXTURE_2D, 0, format, img_w, img_h, 0, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
            checkOpenGLError("glTexImage2D", __FILE__, __LINE__);
            //#   endif
            glGenerateMipmap(GL_TEXTURE_2D);
            checkOpenGLError("glGenerateMipmap", __FILE__, __LINE__);
#elif defined(GL_VERSION_1_4) // OpenGL > 1.4 : GL_GENERATE_MIPMAP
            glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            glTexImage2D(GL_TEXTURE_2D, 0, format, img_w, img_h, 0, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
            checkOpenGLError("glTexImage2D", __FILE__, __LINE__);
#else
            // On utilise gluBuild2DMipmaps si on n'a vraiment pas le choix...
            if (int error = gluBuild2DMipmaps(GL_TEXTURE_2D, format, img_w, img_h, format, GL_UNSIGNED_BYTE, texture.image.getPixels()))
            {
                //CApplication::getApp()->log(CString::fromUTF8("Erreur lors de la création des niveaux de détail de la texture : %1").arg(gluErrorString(error)), ILogger::Error);
            }
#endif
        }
        else
        {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
            checkOpenGLError("glTexParameteri", __FILE__, __LINE__);
            glTexImage2D(GL_TEXTURE_2D, 0, format, img_w, img_h, 0, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
            checkOpenGLError("glTexImage2D", __FILE__, __LINE__);
        }
    }

    if (texture.id == 0)
    {
        gApplication->log(CString::fromUTF8("Création de la texture \"%1\" (%2) : %3 x %4").arg(texture.name).arg(id).arg(img_w).arg(img_h));
    }

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        GLuint64 handle = glGetTextureHandleARB(idgl);
        checkOpenGLError("glGetTextureHandleARB", __FILE__, __LINE__);
        glMakeTextureHandleResidentARB(handle);
        checkOpenGLError("glMakeTextureHandleResidentARB", __FILE__, __LINE__);
        updateUBO(id, handle);
    }

    texture.id     = idgl;
    texture.action = TextureActionNone;
  //texture.image  = CImage();
  //texture.width  = img_w;
  //texture.height = img_h;
}



/**
 * Active ou désactive la persistance de handle pour une texture.
 * Utilisé pour le bindless texture quand une texture doit être modifiée.
 *
 * \param id Identifiant de la texture.
 ******************************/

void CTextureManager::setTextureResident(TTextureId id, bool resident)
{
    assert(id < m_textures.size());
    assert(id != NoTexture);
    assert(id != DefaultTexture);

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        GLuint64 handle = glGetTextureHandleARB(m_textures[id].id);
        checkOpenGLError("glGetTextureHandleARB", __FILE__, __LINE__);

        if (resident)
        {
            glMakeTextureHandleResidentARB(handle);
            checkOpenGLError("glMakeTextureHandleResidentARB", __FILE__, __LINE__);
        }
        else
        {
            glMakeTextureHandleNonResidentARB(handle);
            checkOpenGLError("glMakeTextureHandleNonResidentARB", __FILE__, __LINE__);
        }

        updateUBO(id, handle);
    }
}


/**
 * Efface les données d'une texture dans la carte graphique.
 * Cette méthode doit être appelée dans le thread principal.
 *
 * \param id      Identifiant de la texture.
 * \param texture Structure contenant les informations de la texture.
 ******************************/

void CTextureManager::PrivDeleteTexture(TTextureId id, TTextureInfo& texture)
{
    assert(id < m_textures.size());
    assert(id != NoTexture);
    assert(id != DefaultTexture);

    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        if (m_texture[i] == texture.id)
        {
            glActiveTexture(GL_TEXTURE0 + i); // TODO: tester cet ajout
            checkOpenGLError("glActiveTexture", __FILE__, __LINE__);
            glBindTexture(GL_TEXTURE_2D, m_textures[NoTexture].id);
            checkOpenGLError("glBindTexture", __FILE__, __LINE__);
            m_texture[i] = NoTexture;
        }
    }

    glDeleteTextures(1, &(texture.id));
    checkOpenGLError("glDeleteTextures", __FILE__, __LINE__);

    texture.id     = 0;
    texture.action = TextureActionNone;
  //texture.width  = 0;
  //texture.height = 0;

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        updateUBO(id, 0);
    }

    gApplication->log(CString("Suppression de la texture \"%1\" (%2).").arg(texture.name).arg(id));
}


void CTextureManager::updateUBO(TTextureId id, GLuint64 handle)
{
    assert(id < m_textures.size());
    assert(id < MaxTextures);
    assert(gRenderer->support(TFunctionnality::BindlessTexture));

    //gApplication->log(CString("CTextureManager::updateUBO(%1, %2)").arg(id).arg(handle), ILogger::Debug);

    // Mise-à-jour des données du buffer pour les lumières
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
    GLuint64 * ptr = static_cast<GLuint64 *>(glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY));
    checkOpenGLError("glMapBuffer", __FILE__, __LINE__);

    if (ptr != nullptr)
    {
        ptr[id] = handle;
    }

    glUnmapBuffer(GL_UNIFORM_BUFFER);
    checkOpenGLError("glUnmapBuffer", __FILE__, __LINE__);

    // Pour le DEBUG (n'a pas d'effet)
    glObjectLabel(GL_BUFFER, m_ubo, -1, CString("Uniform Buffer #%1 (textures)").arg(m_ubo).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
}

} // Namespace TE
