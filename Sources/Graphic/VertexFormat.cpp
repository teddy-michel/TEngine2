/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/VertexFormat.hpp"
#include "Graphic/CShader.hpp"

namespace TE
{

template<>
void CBuffer<TVertex3D>::updateAttributes(const std::shared_ptr<CShader>& shader) const
{
    assert(shader != nullptr);

    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    GLint attrib = shader->getAttributeLocation("position");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 3, GL_FLOAT, GL_FALSE, sizeof(TVertex3D), T_BUFFER_OFFSET(offsetof(TVertex3D, position)));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("id");
    if (attrib >= 0)
    {
        glVertexAttribIPointer(attrib, 1, GL_UNSIGNED_INT, sizeof(TVertex3D), T_BUFFER_OFFSET(offsetof(TVertex3D, id)));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("normal");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 3, GL_FLOAT, GL_FALSE, sizeof(TVertex3D), T_BUFFER_OFFSET(offsetof(TVertex3D, normal)));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("color");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex3D), T_BUFFER_OFFSET(offsetof(TVertex3D, color)));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("textcoord1");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 2, GL_FLOAT, GL_FALSE, sizeof(TVertex3D), T_BUFFER_OFFSET(offsetof(TVertex3D, coords[0])));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("textcoord2");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 2, GL_FLOAT, GL_FALSE, sizeof(TVertex3D), T_BUFFER_OFFSET(offsetof(TVertex3D, coords[1])));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
}

template<>
void CBuffer<TVertex2D>::updateAttributes(const std::shared_ptr<CShader>& shader) const
{
    assert(shader != nullptr);

    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);

    GLint attrib = shader->getAttributeLocation("position");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 2, GL_FLOAT, GL_FALSE, sizeof(TVertex2D), T_BUFFER_OFFSET(0));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("textcoord1");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 2, GL_FLOAT, GL_FALSE, sizeof(TVertex2D), T_BUFFER_OFFSET(2 * sizeof(float)));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    attrib = shader->getAttributeLocation("color");
    if (attrib >= 0)
    {
        glVertexAttribPointer(attrib, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex2D), T_BUFFER_OFFSET(4 * sizeof(float)));
        checkOpenGLError("glVertexAttribPointer", __FILE__, __LINE__);
        glEnableVertexAttribArray(attrib);
        checkOpenGLError("glEnableVertexAttribArray", __FILE__, __LINE__);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    checkOpenGLError("glBindBuffer", __FILE__, __LINE__);
}

} // Namespace TE
