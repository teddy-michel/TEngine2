/*
Copyright (C) 2020-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>

#include <chrono>

#include "Graphic/CRenderParams.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CMegaBuffer.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Game/CMap.hpp"
#include "Entities/CMapEntity.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur avec une position et des matrices de vue et projection.
 *
 * \param map        Pointeur sur la map � afficher.
 * \param position   Position de la source d'affichage.
 * \param view       Matrice de vue.
 * \param projection Matrice de projection.
 ******************************/

CRenderParams::CRenderParams(CMap * map, const glm::vec3& position, const glm::mat4& view, const glm::mat4& projection) :
    m_map        { map },
    m_camera     { nullptr },
    m_position   { position },
    m_view       { view },
    m_projection { projection },
    m_type       { TRenderPass::Primary },
    m_mode       { Normal },
    m_shader     { nullptr },
    m_shadowMap  { nullptr },
    m_buffers    {},
    m_shaders    {},
    m_viewport   { 0, 0, 0, 0 },
    m_clearColor { 0.0f, 0.0f, 0.0f, 1.0f }
{
    assert(m_map != nullptr);
}


/**
 * Constructeur.
 *
 * \param map    Pointeur sur la map � afficher.
 * \param camera Pointeur sur la cam�ra utilis�e pour le rendu.
 ******************************/

CRenderParams::CRenderParams(CMap * map, CCamera * camera) :
    m_map        { map },
    m_camera     { camera },
    m_position   {},
    m_view       {},
    m_projection {},
    m_type       { TRenderPass::Primary },
    m_mode       { Normal },
    m_shader     { nullptr },
    m_shadowMap  { nullptr },
    m_buffers    {},
    m_shaders    {},
    m_viewport   { 0, 0, 0, 0 },
    m_clearColor { 0.0f, 0.0f, 0.0f, 1.0f }
{
    assert(m_map != nullptr);
    assert(m_camera != nullptr);
    m_view = m_camera->getViewMatrix();
    m_position = m_camera->getWorldPosition();
}


/**
 * Constructeur avec une cam�ra et des matrices de vue et projection.
 *
 * \param map        Pointeur sur la map � afficher.
 * \param camera     Pointeur sur la cam�ra utilis�e pour le rendu.
 * \param view       Matrice de vue.
 * \param projection Matrice de projection.
 ******************************/

CRenderParams::CRenderParams(CMap * map, CCamera * camera, const glm::mat4& view, const glm::mat4& projection) :
    m_map        { map },
    m_camera     { camera },
    m_position   {},
    m_view       { view },
    m_projection { projection },
    m_type       { TRenderPass::Primary },
    m_mode       { Normal },
    m_shader     { nullptr },
    m_shadowMap  { nullptr },
    m_buffers    {},
    m_shaders    {},
    m_viewport   { 0, 0, 0, 0 },
    m_clearColor { 0.0f, 0.0f, 0.0f, 1.0f }
{
    assert(m_map != nullptr);
    assert(m_camera != nullptr);
    m_position = m_camera->getWorldPosition();
}


/**
 * Modifie le type de passe de rendu.
 *
 * \param type Type de passe de rendu.
 ******************************/

void CRenderParams::setType(TRenderPass type)
{
    m_type = type;
}


/**
 * Modifie le mode d'affichage.
 *
 * \param mode Mode d'affichage.
 ******************************/

void CRenderParams::setMode(TDisplayMode mode)
{
    m_mode = mode;
}


void CRenderParams::setShader(const std::shared_ptr<CShader>& shader)
{
    m_shader = shader;
}


void CRenderParams::setShadowMap(CShadowMap * shadowMap)
{
    m_shadowMap = shadowMap;
}


void CRenderParams::setViewport(int x, int y, int width, int height)
{
    m_viewport = glm::ivec4{ x, y, width, height };
}


void CRenderParams::setClearColor(float red, float green, float blue, float alpha)
{
    m_clearColor = glm::vec4{ red, green, blue, alpha };
}


void CRenderParams::addBuffer(IBufferBase * buffer, const std::shared_ptr<CShader>& shader, const glm::mat4& world)
{
    assert(buffer != nullptr);

    if (shader == nullptr && m_shader == nullptr)
    {
        gApplication->log("Can't render a buffer without a shader", ILogger::Warning);
        return;
    }

    // Calcul de la distance entre le buffer et la source de rendu
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(world, scale, rotation, translation, skew, perspective);
    float distance = glm::distance(translation, m_position);

    m_buffers.push_back(TRenderBuffer{ buffer, shader, world, distance });

    const auto it = std::find(m_shaders.begin(), m_shaders.end(), shader);
    if (it == m_shaders.end())
    {
        m_shaders.push_back(shader);
    }
}


/**
 * Fonction de rendu.
 *
 * \param name Nom de la passe de rendu (facultatif, uniquement pour le debug).
 ******************************/

void CRenderParams::render(const CString& name)
{
    const auto startTime = std::chrono::high_resolution_clock::now();

    // Remplissage de la liste des buffers � afficher et affichages secondaires
    m_buffers.clear();
    m_shaders.clear();
    m_map->getMapEntity()->render(this);

    // Tri des buffers par �loignement avec la cam�ra
    //std::sort(m_buffers.begin(), m_buffers.end(), [](const TRenderBuffer& a, const TRenderBuffer& b) { return a.distance < b.distance; });
    std::sort(m_buffers.begin(), m_buffers.end());

    CString passName;

    switch (m_type)
    {
        case TRenderPass::Primary:
            passName = "Primary rendering";
            break;
        case TRenderPass::Secondary:
            passName = "Secondary rendering";
            break;
        case TRenderPass::ShadowMap:
            passName = "Shadow map rendering";
            break;
        default:
            passName = "Unknown rendering";
            break;
    }

    if (!name.isEmpty())
    {
        passName = CString("%1 (%2)").arg(passName).arg(name);
    }

    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, passName.toCharArray());
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    // Clear the screen
    glClearColor(m_clearColor.r, m_clearColor.g, m_clearColor.b, m_clearColor.a);
    checkOpenGLError("glClearColor", __FILE__, __LINE__);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // TODO: clear GL_STENCIL_BUFFER_BIT ?
    checkOpenGLError("glClear", __FILE__, __LINE__);

    // Set viewport
    if (m_viewport.z != 0 && m_viewport.w != 0)
    {
        glViewport(m_viewport.x, m_viewport.y, m_viewport.z, m_viewport.w);
        checkOpenGLError("glViewport", __FILE__, __LINE__);
    }

    if (m_shader != nullptr)
    {
        m_shader->bind();
        m_shader->setViewMatrix(m_view);
        m_shader->setProjectionMatrix(m_projection);
    }
    else
    {
        for (auto& shader : m_shaders)
        {
            shader->bind();
            shader->setViewMatrix(m_view);
            shader->setProjectionMatrix(m_projection);
        }
    }

    if (m_type == TRenderPass::ShadowMap)
    {
        glCullFace(GL_BACK);
    }

    renderOpaqueBuffers();
    renderTransparentBuffers();

    if (m_type == TRenderPass::ShadowMap)
    {
        glCullFace(GL_FRONT);
    }

    // Reset viewport
    if (m_viewport.z != 0 && m_viewport.w != 0)
    {
        glViewport(0, 0, gApplication->getWidth(), gApplication->getHeight());
        checkOpenGLError("glViewport", __FILE__, __LINE__);
    }

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);

    const auto endTime = std::chrono::high_resolution_clock::now();
    const std::chrono::microseconds duration = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime);
    //gApplication->log(CString("Frame render in %1 �s").arg(duration.count()), ILogger::Debug);
}


/**
 * Affiche les buffers opaques.
 ******************************/

void CRenderParams::renderOpaqueBuffers()
{
    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Opaque buffers");
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    std::shared_ptr<CShader> lastShader = m_shader;
    bool megaBufferRendered = false;
    std::shared_ptr<CMegaBuffer<TVertex3D>> megaBuffer = gRenderer->getMegaBuffer();

    // Affichage des buffers (front to back)
    for (auto& it = m_buffers.begin(); it != m_buffers.end(); ++it)
    {
        if (it->buffer->isTransparent())
        {
            continue;
        }

        if (m_shader == nullptr && it->shader != lastShader)
        {
            lastShader = it->shader;
            megaBufferRendered = false;
        }

        if (it->buffer->isSubBuffer())
        {
            if (!megaBufferRendered)
            {
                // Liste des sous buffers utilisant le m�me shader
                std::vector<const IBufferBase *> subBuffers;

                auto itCopy = it;
                while (itCopy != m_buffers.end())
                {
                    if (m_shader == nullptr && itCopy->shader != lastShader)
                    {
                        break;
                    }

                    subBuffers.push_back(itCopy->buffer);
                    ++itCopy;
                }

                renderMegaBuffer(megaBuffer, subBuffers, lastShader);
                megaBufferRendered = true;
            }
        }
        else
        {
            renderBuffer(*it);
        }
    }

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);
}


/**
 * Affiche les buffers transparents.
 ******************************/

void CRenderParams::renderTransparentBuffers()
{
    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Transparent buffers");
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    // Affichage des buffers (back to front)
    for (auto& it = m_buffers.rbegin(); it != m_buffers.rend(); ++it)
    {
        if (it->buffer->isTransparent())
        {
            renderBuffer(*it);
        }
    }

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);
}


/**
 * Affiche un buffer.
 *
 * \param buffer Buffer � afficher.
 ******************************/

void CRenderParams::renderBuffer(const TRenderBuffer& buffer)
{
    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Render buffer");
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    if (m_shader != nullptr)
    {
        m_shader->setModelMatrix(buffer.world);
        buffer.buffer->draw(m_mode, m_shader);
    }
    else
    {
        buffer.shader->bind();

        if (m_type != TRenderPass::ShadowMap && m_shadowMap != nullptr)
        {
            m_shadowMap->updateShader(buffer.shader);
        }

        buffer.shader->setModelMatrix(buffer.world);
        buffer.buffer->draw(m_mode, buffer.shader);
    }

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);
}


/**
 * Affiche un megabuffer.
 *
 * \param megaBuffer Buffer � afficher.
 * \param subBuffers Liste des subbuffers.
 * \param shader     Shader.
 ******************************/

void CRenderParams::renderMegaBuffer(const std::shared_ptr<CMegaBuffer<TVertex3D>>& megaBuffer, const std::vector<const IBufferBase *>& subBuffers, const std::shared_ptr<CShader>& shader)
{
    assert(megaBuffer != nullptr);
    assert(shader != nullptr);

    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Render mega buffer");
    checkOpenGLError("glPushDebugGroup", __FILE__, __LINE__);

    if (m_shader != nullptr)
    {
        megaBuffer->draw(subBuffers, m_mode, m_shader);
    }
    else
    {
        shader->bind();

        if (m_type != TRenderPass::ShadowMap && m_shadowMap != nullptr)
        {
            m_shadowMap->updateShader(shader);
        }

        megaBuffer->draw(subBuffers, m_mode, shader);
    }

    glPopDebugGroup();
    checkOpenGLError("glPopDebugGroup", __FILE__, __LINE__);
}

} // Namespace TE
