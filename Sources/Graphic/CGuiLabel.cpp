/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "Graphic/CGuiLabel.hpp"
#include "Graphic/CBuffer2D.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

CGuiLabel::CGuiLabel(const CString& text) :
    IGuiObject        {},
    m_textParams      {},
    m_backgroundColor { 255, 255, 255, 0 }
{
    m_textParams.text = text;
}


/**
 * Destructeur.
 ******************************/

CGuiLabel::~CGuiLabel() = default;


void CGuiLabel::update(float frameTime)
{
    // TODO: centrage
    m_textParams.rec.x = m_posX;
    m_textParams.rec.y = m_posY;
}


void CGuiLabel::draw(CBuffer2D * buffer) const
{
    assert(buffer != nullptr);

    // TODO: check position and size

    // Fond
    if (m_backgroundColor.getAlpha() != 0)
    {
        glm::vec4 backgroundColor;
        m_backgroundColor.toFloat(&backgroundColor[0]);
        buffer->addRectangle(glm::vec2{ m_posX, m_posY }, glm::vec2{ m_textParams.rec.z, m_textParams.rec.w }, backgroundColor);
    }

    // Texte
    gFontManager->drawText(buffer, m_textParams);
}


void CGuiLabel::setText(const CString& text)
{
    m_textParams.text = text;
}


void CGuiLabel::setColor(const CColor& color)
{
    m_textParams.color = color;
}


void CGuiLabel::setFontSize(int size)
{
    assert(size > 0);

    if (size < CFontManager::minFontSize || size > CFontManager::maxFontSize)
    {
        return;
    }

    m_textParams.size = size;
}


void CGuiLabel::setFont(TFontId font)
{
    m_textParams.font = font;
}


void CGuiLabel::setSize(int width, int height)
{
    assert(width >= 0);
    assert(height >= 0);
    m_textParams.rec.z = width;
    m_textParams.rec.w = height;
}


void CGuiLabel::setWidth(int width)
{
    assert(width >= 0);
    m_textParams.rec.z = width;
}


void CGuiLabel::setHeight(int height)
{
    assert(height >= 0);
    m_textParams.rec.w = height;
}


void CGuiLabel::setBackgroundColor(const CColor& color)
{
    m_backgroundColor = color;
}

} // Namespace TE
