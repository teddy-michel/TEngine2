/*
Copyright (C) 2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/IGuiObject.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

IGuiObject::IGuiObject() :
    m_posX { 0 },
    m_posY { 0 }
{

}


/**
 * Destructeur.
 ******************************/

IGuiObject::~IGuiObject() = default;


void IGuiObject::setPosition(int x, int y)
{
    m_posX = x;
    m_posY = y;
}

} // Namespace TE
