/*
Copyright (C) 2019-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <glm/gtc/type_ptr.hpp>

#include "Graphic/CShader.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur.
 *
 * \param vertexShader   Identifiant du vertex shader.
 * \param fragmentShader Identifiant du fragment shader.
 ******************************/

CShader::CShader(GLuint vertexShader, GLuint fragmentShader) :
    m_vertexShader       { vertexShader },
    m_fragmentShader     { fragmentShader },
    m_shaderProgram      { 0 },
    m_uniModel           { -1 },
    m_uniView            { -1 },
    m_uniProj            { -1 },
    m_useBindlessTexture { false }
{
    gApplication->log("Create CShader");
    load();
}


/**
 * Destructeur.
 ******************************/

CShader::~CShader()
{
    gApplication->log("Delete CShader");
    unload();
}


/**
 * Envoit la matrice de vue au shader.
 *
 * \param matrix Matrice de vue.
 ******************************/

void CShader::setViewMatrix(const glm::mat4& matrix)
{
    if (m_uniView >= 0)
    {
        glUniformMatrix4fv(m_uniView, 1, GL_FALSE, glm::value_ptr(matrix));
        checkOpenGLError("glUniformMatrix4fv", __FILE__, __LINE__);
    }
}


/**
 * Envoit la matrice de projection au shader.
 *
 * \param matrix Matrice de projection.
 ******************************/

void CShader::setProjectionMatrix(const glm::mat4& matrix)
{
    if (m_uniProj >= 0)
    {
        glUniformMatrix4fv(m_uniProj, 1, GL_FALSE, glm::value_ptr(matrix));
        checkOpenGLError("glUniformMatrix4fv", __FILE__, __LINE__);
    }
}


/**
 * Envoit la matrice de mod�le au shader.
 *
 * \param matrix Matrice de mod�le.
 ******************************/

void CShader::setModelMatrix(const glm::mat4& matrix)
{
    if (m_uniModel >= 0)
    {
        glUniformMatrix4fv(m_uniModel, 1, GL_FALSE, glm::value_ptr(matrix));
        checkOpenGLError("glUniformMatrix4fv", __FILE__, __LINE__);
    }
}


#ifdef T_OLD_SHADER

/**
 * Charge le shader depuis des fichiers.
 *
 * \param vertFile Nom du fichier contenant le vertex shader.
 * \param fragFile Nom du fichier contenant le fragment shader.
 * \return True si le chargement s'est bien pass�, false sinon.
 ******************************/

bool CShader::loadFromFiles(const CString& vertFile, const CString& fragFile)
{
    gApplication->log(CString("Load shader from files \"%1\" and \"%2\".").arg(vertFile).arg(fragFile));

    unload();

    char * vertexSrc = nullptr;
    char * fragmentSrc = nullptr;

    // Read vertex shader file
    {
        std::ifstream file{ vertFile.toCharArray(), std::fstream::in | std::fstream::binary };
        if (file)
        {
            file.seekg(0, std::ios::end);
            std::streampos length = file.tellg();
            file.seekg(0, std::ios::beg);
            std::streambuf * raw_buffer = file.rdbuf();
            vertexSrc = new char[static_cast<std::size_t>(length) + 1];
            raw_buffer->sgetn(vertexSrc, length);
            vertexSrc[length] = 0;
        }
        else
        {
            gApplication->log(CString("Can't open file \"%1\".").arg(vertFile), ILogger::Error);
        }
    }

    // Read fragment shader file
    {
        std::ifstream file{ fragFile.toCharArray(), std::fstream::in | std::fstream::binary };
        if (file)
        {
            file.seekg(0, std::ios::end);
            std::streampos length = file.tellg();
            file.seekg(0, std::ios::beg);
            std::streambuf * raw_buffer = file.rdbuf();
            fragmentSrc = new char[static_cast<std::size_t>(length) + 1];
            raw_buffer->sgetn(fragmentSrc, length);
            fragmentSrc[length] = 0;
        }
        else
        {
            gApplication->log(CString("Can't open file \"%1\".").arg(fragFile), ILogger::Error);
        }
    }

    // Load shaders from source
    bool res = loadFromSources(vertexSrc, fragmentSrc);

    // Pour le DEBUG
    glObjectLabel(GL_SHADER, m_vertexShader, -1, CString("Vertex Shader #%1 for program #%2 (%3)").arg(m_vertexShader).arg(m_shaderProgram).arg(vertFile).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);
    glObjectLabel(GL_SHADER, m_fragmentShader, -1, CString("Fragment Shader #%1 for program #%2 (%3)").arg(m_fragmentShader).arg(m_shaderProgram).arg(fragFile).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    // Clean buffers
    if (vertexSrc != nullptr)
    {
        delete[] vertexSrc;
    }

    if (fragmentSrc != nullptr)
    {
        delete[] fragmentSrc;
    }

    return res;
}


bool CShader::loadFromSources(const char * vertSrc, const char * fragSrc)
{
    unload();

    if (vertSrc == nullptr || fragSrc == nullptr)
    {
        return false;
    }

    // Create and compile the vertex shader
    m_vertexShader = glCreateShader(GL_VERTEX_SHADER);
    checkOpenGLError("glCreateShader", __FILE__, __LINE__);

    if (m_vertexShader == 0)
    {
        gApplication->log("Can't create vertex shader.", ILogger::Error);
        unload();
        return false;
    }

    // Pour le DEBUG
    glObjectLabel(GL_SHADER, m_vertexShader, -1, CString("Vertex Shader #%1").arg(m_vertexShader).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    {
        glShaderSource(m_vertexShader, 1, &vertSrc, NULL);
        checkOpenGLError("glShaderSource", __FILE__, __LINE__);
        glCompileShader(m_vertexShader);
        checkOpenGLError("glCompileShader", __FILE__, __LINE__);

        bool success = false;
        CString error = getShaderInfoLog(m_vertexShader, success);

        if (!success)
        {
            gApplication->log("Error when compiling vertex shader.", ILogger::Error);
            gApplication->log(error, ILogger::Error);
            unload();
            return false;
        }
    }

    // Create and compile the fragment shader
    m_fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    checkOpenGLError("glCreateShader", __FILE__, __LINE__);

    // Pour le DEBUG
    glObjectLabel(GL_SHADER, m_fragmentShader, -1, CString("Fragment Shader #%1").arg(m_fragmentShader).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    if (m_fragmentShader == 0)
    {
        gApplication->log("Can't create fragment shader.", ILogger::Error);
        unload();
        return false;
    }

    {
        glShaderSource(m_fragmentShader, 1, &fragSrc, NULL);
        checkOpenGLError("glShaderSource", __FILE__, __LINE__);
        glCompileShader(m_fragmentShader);
        checkOpenGLError("glCompileShader", __FILE__, __LINE__);

        bool success = false;
        CString error = getShaderInfoLog(m_fragmentShader, success);

        if (!success)
        {
            gApplication->log("Error when compiling fragment shader.", ILogger::Error);
            gApplication->log(error, ILogger::Error);
            unload();
            return false;
        }
    }

    // Create shader program
    m_shaderProgram = glCreateProgram();
    checkOpenGLError("glCreateProgram", __FILE__, __LINE__);

    if (m_shaderProgram == 0)
    {
        gApplication->log("Can't create shader program.");
        unload();
        return false;
    }

    // Pour le DEBUG
    glObjectLabel(GL_PROGRAM, m_shaderProgram, -1, CString("Program #%1").arg(m_shaderProgram).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glAttachShader(m_shaderProgram, m_vertexShader);
    checkOpenGLError("glAttachShader", __FILE__, __LINE__);
    glAttachShader(m_shaderProgram, m_fragmentShader);
    checkOpenGLError("glAttachShader", __FILE__, __LINE__);
    glBindFragDataLocation(m_shaderProgram, 0, "outColor");
    checkOpenGLError("glBindFragDataLocation", __FILE__, __LINE__);
    glLinkProgram(m_shaderProgram);
    checkOpenGLError("glLinkProgram", __FILE__, __LINE__);

    m_uniModel = glGetUniformLocation(m_shaderProgram, "model");
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);
    m_uniView = glGetUniformLocation(m_shaderProgram, "view");
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);
    m_uniProj = glGetUniformLocation(m_shaderProgram, "proj");
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    // Bind UBO for bindless textures
    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        bindBuffer("TextureArray", gTextureManager->getUBO(), TShaderBinding::TextureArray);
    }

    bindSSBO("LinkArray", gRenderer->getLinksSSBO(), TShaderBinding::LinkArray);
    bindSSBO("ModelArray", gRenderer->getModelsSSBO(), TShaderBinding::ModelArray);

    // Unit�s de texture
    setUniformValue("texture1", 0);
    setUniformValue("texture2", 1);
    setUniformValue("texture3", 2);
    setUniformValue("texture4", 3);

    return true;
}


CString CShader::getShaderInfoLog(GLuint shader, bool& success) const
{
    GLint status = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    checkOpenGLError("glGetShaderiv", __FILE__, __LINE__);
    success = (status == GL_TRUE);

    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    checkOpenGLError("glGetShaderiv", __FILE__, __LINE__);

    if (length == 0)
    {
        return CString();
    }

    char* error_str = new char[length];
    glGetShaderInfoLog(shader, length, &length, error_str);
    checkOpenGLError("glGetShaderInfoLog", __FILE__, __LINE__);
    error_str[length - 1] = 0; // Au cas o�...
    CString error(error_str);
    delete[] error_str;
    return error;
}

#endif


GLint CShader::getAttributeLocation(const char * name)
{
    assert(name != nullptr);

    GLint attrib = glGetAttribLocation(m_shaderProgram, name);
    checkOpenGLError("glGetAttribLocation", __FILE__, __LINE__);

    return attrib;
}


bool CShader::setUniformValue(const char * name, bool value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform1i(location, value);
        checkOpenGLError("glUniform1i", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, int value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform1i(location, value);
        checkOpenGLError("glUniform1i", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, unsigned int value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform1ui(location, value);
        checkOpenGLError("glUniform1i", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, float value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform1f(location, value);
        checkOpenGLError("glUniform1f", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, const glm::vec2& value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform2fv(location, 1, glm::value_ptr(value));
        checkOpenGLError("glUniform2fv", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, const glm::vec3& value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform3fv(location, 1, glm::value_ptr(value));
        checkOpenGLError("glUniform3fv", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, const glm::vec4& value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniform4fv(location, 1, glm::value_ptr(value));
        checkOpenGLError("glUniform4fv", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::setUniformValue(const char * name, const glm::mat4& value)
{
    bind();

    GLint location = glGetUniformLocation(m_shaderProgram, name);
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    if (location >= 0)
    {
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
        checkOpenGLError("glUniformMatrix4fv", __FILE__, __LINE__);
        return true;
    }
    else
    {
        return false;
    }
}


bool CShader::bindBuffer(const char * name, GLuint buffer, GLuint binding)
{
    bind();

    GLuint index = glGetUniformBlockIndex(m_shaderProgram, name);
    checkOpenGLError("glGetUniformBlockIndex", __FILE__, __LINE__);

    if (index == GL_INVALID_INDEX)
    {
        return false;
    }

    glBindBufferBase(GL_UNIFORM_BUFFER, binding, buffer);
    checkOpenGLError("glBindBufferBase", __FILE__, __LINE__);
    glUniformBlockBinding(m_shaderProgram, index, binding);
    checkOpenGLError("glUniformBlockBinding", __FILE__, __LINE__);

    return true;
}


bool CShader::bindSSBO(const char * name, GLuint buffer, GLuint binding)
{
    bind();

    GLuint index = glGetProgramResourceIndex(m_shaderProgram, GL_SHADER_STORAGE_BLOCK, name);
    checkOpenGLError("glGetProgramResourceIndex", __FILE__, __LINE__);

    if (index == GL_INVALID_INDEX)
    {
        return false;
    }

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, buffer);
    checkOpenGLError("glBindBufferBase", __FILE__, __LINE__);
    glShaderStorageBlockBinding(m_shaderProgram, index, binding);
    checkOpenGLError("glShaderStorageBlockBinding", __FILE__, __LINE__);

    return true;
}


void CShader::bind()
{
    gRenderer->bindShader(m_shaderProgram);
}


void CShader::unbind()
{
    gRenderer->bindShader(0);
}


void CShader::load()
{
    // Create shader program
    m_shaderProgram = glCreateProgram();
    checkOpenGLError("glCreateProgram", __FILE__, __LINE__);

    if (m_shaderProgram == 0)
    {
        gApplication->log("Can't create shader program.");
        unload();
        return;
    }

    // Pour le DEBUG
    glObjectLabel(GL_PROGRAM, m_shaderProgram, -1, CString("Program #%1").arg(m_shaderProgram).toCharArray());
    checkOpenGLError("glObjectLabel", __FILE__, __LINE__);

    glAttachShader(m_shaderProgram, m_vertexShader);
    checkOpenGLError("glAttachShader", __FILE__, __LINE__);
    glAttachShader(m_shaderProgram, m_fragmentShader);
    checkOpenGLError("glAttachShader", __FILE__, __LINE__);
    glBindFragDataLocation(m_shaderProgram, 0, "outColor");
    checkOpenGLError("glBindFragDataLocation", __FILE__, __LINE__);
    glLinkProgram(m_shaderProgram);
    checkOpenGLError("glLinkProgram", __FILE__, __LINE__);

    m_uniModel = glGetUniformLocation(m_shaderProgram, "model");
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);
    m_uniView = glGetUniformLocation(m_shaderProgram, "view");
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);
    m_uniProj = glGetUniformLocation(m_shaderProgram, "proj");
    checkOpenGLError("glGetUniformLocation", __FILE__, __LINE__);

    // Bind UBO for bindless textures
    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        bindBuffer("TextureArray", gTextureManager->getUBO(), TShaderBinding::TextureArray);
    }

    bindSSBO("LinkArray", gRenderer->getLinksSSBO(), TShaderBinding::LinkArray);
    bindSSBO("ModelArray", gRenderer->getModelsSSBO(), TShaderBinding::ModelArray);

    // Unit�s de texture
    setUniformValue("texture1", 0);
    setUniformValue("texture2", 1);
    setUniformValue("texture3", 2);
    setUniformValue("texture4", 3);
}


void CShader::unload()
{
    if (m_shaderProgram != 0)
    {
        glDeleteProgram(m_shaderProgram);
        checkOpenGLError("glDeleteProgram", __FILE__, __LINE__);
        m_shaderProgram = 0;
    }

#ifdef T_OLD_SHADER

    if (m_fragmentShader != 0)
    {
        glDeleteShader(m_fragmentShader);
        checkOpenGLError("glDeleteShader", __FILE__, __LINE__);
        m_fragmentShader = 0;
    }

    if (m_vertexShader != 0)
    {
        glDeleteShader(m_vertexShader);
        checkOpenGLError("glDeleteShader", __FILE__, __LINE__);
        m_vertexShader = 0;
    }

#endif

    m_uniModel = -1;
    m_uniView = -1;
    m_uniProj = -1;
}


void CShader::setBindlessTexture(bool use)
{
    m_useBindlessTexture = use;
}

} // Namespace TE
