/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>

#include "Graphic/CFontManager.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CFont.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Core/ILogger.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

CFontManager * gFontManager = nullptr;


/**
 * Constructeur par défaut.
 ******************************/

CFontManager::CFontManager()
{
    assert(gFontManager == nullptr);
    gFontManager = this;

#ifdef T_USE_FREETYPE

    FT_Error error = FT_Init_FreeType(&m_library);

    if (error != 0)
    {
        gApplication->log(CString("Erreur lors de l'initialisation de FreeType (code %1)").arg(error), ILogger::Error);
    }

#endif
}


/**
 * Destructeur.
 ******************************/

CFontManager::~CFontManager()
{
    gFontManager = nullptr;
    unloadFonts();

#ifdef T_USE_FREETYPE

    if (m_library)
    {
        FT_Error error = FT_Done_FreeType(m_library);

        if (error != 0)
        {
            gApplication->log(CString("Erreur lors de la fermeture de FreeType (code %1)").arg(error), ILogger::Error);
        }
    }

#endif
}


/**
 * Retourne l'identifiant de la police à partir de son nom.
 *
 * \param name Nom de la police.
 * \return Identifiant de la police si elle a été chargée, sinon retourne InvalidFont.
 ******************************/

TFontId CFontManager::getFontId(const CString& name)
{
#ifdef T_USE_FREETYPE
    return loadFont(name, CString("../../../Resources/fonts/" + name + ".ttf"));
#else
    return loadFont(name, CString("../../../Resources/fonts/" + name + ".tef"));
#endif
}


/**
 * Retourne le nom de la police associée à un identifiant.
 *
 * \param id Identifiant de la police.
 * \return Nom de la police ou chaine vide si la police n'existe pas.
 ******************************/

CString CFontManager::getFontName(const TFontId id) const
{
    if (id < m_fonts.size())
    {
#ifdef T_USE_FREETYPE
        return m_fonts[id]->getFamilyName();
#else
        return m_fonts[id].name;
#endif
    }

    // La police n'a pas été trouvée
    return CString();
}


/**
 * Charge une police de caractère.
 * Si le nom est déjà utilisé, la police n'est pas chargée.
 *
 * \param name     Nom de la police.
 * \param fileName Adresse du fichier contenant la police à charger.
 * \return Identifiant de la police
 ******************************/

TFontId CFontManager::loadFont(const CString& name, const CString& fileName)
{
    TFontId id = 0;

    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it, ++id)
    {
#ifdef T_USE_FREETYPE
        if ((*it)->getFamilyName() == name)
#else
        if (it->name == name)
#endif
        {
            return id;
        }
    }

    // Si la police n'a pas été chargée, on le fait
    return privLoadFont(name, fileName);
}


/**
 * Supprime toutes les polices.
 ******************************/

void CFontManager::unloadFonts()
{
    gApplication->log(CString::fromUTF8("Suppression des polices de caractère (%1 polices supprimées)").arg(m_fonts.size()));

#ifndef T_USE_FREETYPE
    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it)
    {
        CTextureManager::instance().deleteTexture(it->texture);
    }
#endif

    m_fonts.clear();
}


/**
 * Ajoute à un buffer le texte à afficher.
 *
 * \todo Aller automatiquement à la ligne si demandé.
 * \todo Vérifier les coordonnées de texture pour éviter de devoir retourner l'image.
 * \todo Utiliser la classe CFormattedString.
 *
 * \param buffer Buffer graphique.
 * \param params Paramètres du texte à afficher.
 *
 * Les codes ASCII suivants permettent de changer la couleur du texte :
 * \li 1 : Couleur normale.
 * \li 2 : Blanc.
 * \li 3 : Noir.
 * \li 4 : Rouge.
 * \li 5 : Bleu.
 * \li 6 : Vert.
 * \li 7 : Jaune.
 * \li 8 : Orange.
 * \li 12 : Surligné.
 ******************************/

void CFontManager::drawText(CBuffer2D * buffer, const TTextParams& params)
{
    assert(buffer != nullptr);

    if (params.text.isEmpty())
        return;

    if (params.font >= m_fonts.size())
        return;

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    font->drawText(buffer, params);
    return;
#else
    const TFont& font = m_fonts[params.font];

    // Variables
    const float ratio = params.size * 16.0f / font.width;
    float x = static_cast<float>(params.rec.x) - static_cast<float>(params.left);
    float y = static_cast<float>(params.rec.y) - static_cast<float>(params.top);
    float offset = 0.5f / font.width;

    bool highlight = false;
    bool drawChar = true;
    CColor colorActual = params.color;
    

    // On récupère les tableaux du buffer graphique
    std::vector<unsigned int> indices;
    TBufferTextureVector textures;
    std::vector<TVertex2D> vertices;


    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.cbegin(); it != params.text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        // Gestion des caractères spéciaux
        switch (c)
        {
            // Couleur normale
            case 1:
                colorActual = params.color;
                continue;

            // Blanc
            case 2:
                colorActual = CColor::White;
                continue;

            // Noir
            case 3:
                colorActual = CColor::Black;
                continue;

            // Rouge
            case 4:
                colorActual = CColor::Red;
                continue;

            // Bleu
            case 5:
                colorActual = CColor::Blue;
                continue;

            // Vert
            case 6:
                colorActual = CColor::Green;
                continue;

            // Jaune
            case 7:
                colorActual = CColor::Yellow;
                continue;

            // Orange
            case 8:
                colorActual = CColor::Orange;
                continue;

            // Surlignement
            case 12:
                highlight = !highlight;
                continue;

            default:
                break;
        }

        drawChar = ((params.rec.z  == 0 || (x < params.rec.x + params.rec.z && x + params.rec.z > static_cast<float>(params.rec.x))) &&
                    (params.rec.w == 0 || (y < params.rec.y + params.rec.w && y + params.rec.w > static_cast<float>(params.rec.y))));

        // Pourcentages d'affichage du caractère
        float perX1 = 0.0f;
        float perX2 = 1.0f;
        float perY1 = 0.0f;
        float perY2 = 1.0f;

        if (params.rec.z > 0)
        {
            // Caractère découpé à gauche
            if (x < params.rec.x &&
                x + font.size[c].x * ratio > params.rec.x)
            {
                perX1 = (params.rec.x - x) / (font.size[c].x * ratio);
            }

            // Caractère découpé à droite
            if (x < params.rec.x + params.rec.z &&
                x + font.size[c].x * ratio > params.rec.x + params.rec.z)
            {
                perX2 = (params.rec.x + params.rec.z - x) / (font.size[c].x * ratio);
            }
        }

        if (params.rec.w > 0)
        {
            // Caractère découpé en bas
            if (y < params.rec.y &&
                y + font.size[c].y * ratio > params.rec.y)
            {
                perY1 = (params.rec.y - y) / (font.size[c].y * ratio);
            }

            // Caractère découpé en bas
            if (y < params.rec.y + params.rec.w &&
                y + font.size[c].y * ratio > params.rec.y + params.rec.w)
            {
                perY2 = (params.rec.y + params.rec.w - y) / (font.size[c].y * ratio);
            }
        }

        // Surlignement du texte
        if (highlight && drawChar)
        {
            const glm::vec2 position{ x, y };
            const glm::vec2 size{ font.size[c].x * ratio * perX2, params.size * perY2 };
            const glm::vec4 color{ 1.0f, 0.5f, 0.0f, 0.9f }; // Couleur de surlignement
            buffer->addRectangle(position, size, color);
        }

        switch (c)
        {
            // Saut de ligne
            case '\n':
            case '\r':
                x = static_cast<float>(params.rec.x) - static_cast<float>(params.left);
                y += params.size;
                continue;

            // Espace
            case ' ':
                x += font.size[32].x * ratio;
                continue;

            // Tabulation horizontale
            case '\t':
                x += 4.0f * font.size[32].x * ratio;
                continue;

            default:

                if (drawChar)
                {
                    const glm::vec2 position{ x, y };
                    const glm::vec2 size{ params.size * perX2, params.size * perY2 };
                    const glm::vec2 coordsMin{ 0.0625f * ((c % 16)) + offset, 0.0625f * ((c / 16)) + offset };
                    const glm::vec2 coordsMax{ 0.0625f * ((c % 16) + perX2) - offset, 0.0625f * ((c / 16) + perY2) - offset };
                    glm::vec4 color;
                    colorActual.toFloat(&color[0]);
                    buffer->addRectangle(position, size, color, charParams.texture, coordsMin, coordsMax);
                }

                break;
        }

        // Incrémentation de la position
        x += font.size[c].x * ratio;
    }

#endif
}


/**
 * Renvoie les dimensions d'une chaîne de caractère en pixels.
 *
 * \param params Paramètres du texte à afficher.
 * \return Dimensions du texte en pixels.
 ******************************/

glm::u32vec2 CFontManager::getTextSize(const TTextParams& params)
{
    if (params.text.isEmpty() || params.font >= m_fonts.size())
    {
        return glm::u32vec2{ 0, 0 };
    }

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    return font->getTextSize(params);
#else
    const TFont& font = m_fonts[params.font];

    const float ratio = params.size * 16.0f / font.width;

    std::vector<float> lengths;
    glm::vec2 sz(0.0f, static_cast<float>(params.size));

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.cbegin(); it != params.text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lengths.push_back(sz.x);
                sz.y += params.size;
                sz.x = 0.0f;
                break;

            // Tabulation horizontale
            case '\t':
                sz.x += 4 * font.size[32].x * ratio;
                break;

            // Défaut
            default:
                sz.x += font.size[c].x * ratio;
                break;
        }
    }

    // On prend la longueur de la ligne la plus longue
    lengths.push_back(sz.x);
    sz.x = *std::max_element(lengths.begin(), lengths.end());

    return glm::u32vec2(static_cast<unsigned int>(sz.x), static_cast<unsigned int>(sz.y));

#endif
}


/**
 * Calcule la largeur de chaque caractère de chaque ligne d'un texte.
 * Seul le texte, la police, et la taille sont utilisés.
 *
 * \todo Tester.
 *
 * \param params Paramètres du texte à afficher.
 * \param lines  Structure qui contiendra la largeur de chaque caractère de
 *               chaque ligne du texte.
 ******************************/

void CFontManager::getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines)
{
    lines.clear();

    if (params.text.isEmpty() || params.font >= m_fonts.size())
    {
        return;
    }

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    font->getTextLinesSize(params, lines);
#else

    TTextLine currentLine;

    const TFont& font = m_fonts[params.font];

    const float ratio = params.size * 16.0f / font.width;
    unsigned int offset = 0;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.cbegin(); it != params.text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lines.push_back(currentLine);
                currentLine.len.clear();
                currentLine.offset = ++offset;
                break;

            // Tabulation horizontale
            case '\t':
                ++offset;
                currentLine.len.push_back(4.0f * font.size[32].x * ratio);
                break;

            // Défaut
            default:
                ++offset;
                currentLine.len.push_back(font.size[c].x * ratio);
                break;
        }
    }

    lines.push_back(currentLine);
#endif
}


/**
 * Calcule la largeur de chaque caractère d'une ligne de texte.
 * Le calcul s'arrête si un retour à la ligne est rencontré.
 * Seul le texte, la police, et la taille sont utilisés.
 *
 * \todo Tester.
 *
 * \param params Paramètres du texte à afficher.
 * \return Largeur de chaque caractère de la ligne.
 ******************************/

TTextLine CFontManager::getTextLineSize(const TTextParams& params)
{
    if (params.text.isEmpty())
        return TTextLine();

    if (params.font >= m_fonts.size())
        return TTextLine();

    TTextLine line;

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    font->getTextLineSize(params, line);
#else
    const TFont& font = m_fonts[params.font];

    const float ratio = params.size * 16.0f / font.width;

    std::vector<float> lengths;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.begin(); it != params.text.end(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\r':
            case '\n':
                return line;

            // Tabulation horizontale
            case '\t':
                line.len.push_back(4.0f * font.size[32].x * ratio);
                break;

            // Défaut
            default:
                line.len.push_back(font.size[c].x * ratio);
                break;
        }
    }
#endif

    return line;
}


/**
 * Inscrit la liste des polices dans le log.
 ******************************/

void CFontManager::logFontList() const
{
    CString txt("Liste des polices :");
    //gApplication->log("\nListe des polices :");

    // On parcourt le tableau des textures
    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it)
    {
#ifdef T_USE_FREETYPE
        txt += CString("\n - ") + (*it)->getFontName();
#else
        txt += CString("\n - %1 (texture %2, %3 x %4)").arg(it->name).arg(it->texture).arg(it->width).arg(it->height);
        //gApplication->log(CString(" - %1 (texture %2, %3 x %4)").arg(it->name).arg(it->texture).arg(it->width).arg(it->height));
#endif
    }

    gApplication->log(txt);
    //gApplication->log("\n");
}


/**
 * Charge une police de caractère.
 *
 * \param name     Nom de la police.
 * \param fileName Adresse du fichier contenant la police à charger.
 * \return Identifiant de la police
 ******************************/

TFontId CFontManager::privLoadFont(const CString& name, const CString& fileName)
{
    gApplication->log(CString::fromUTF8("Chargement de la police de caractère %1").arg(name));


#ifdef T_USE_FREETYPE

    CFont * font = new CFont();

    if (!font->loadFromFile(fileName))
    {
        delete font;
        gApplication->log(CString("CFontManager::privLoadFont : impossible de charger le fichier de police \"%1.ttf\"").arg(name), ILogger::Error);
        return InvalidFont;
    }

#else

    CLoaderTedFont loader;

    if (!loader.loadFromFile(fileName))
    {
        gApplication->log(CString("CFontManager::privLoadFont : impossible de charger le fichier de police \"%1.tef\"").arg(name), ILogger::Error);
        return InvalidFont;
    }

    TFont font;
    font.name    = name;
    font.texture = CTextureManager::instance().loadTexture(":font:" + name, loader.getImage(), CTextureManager::FilterNone);
    font.width   = loader.getImage().getWidth();
    font.height  = loader.getImage().getHeight();

    CLoaderTedFont::TCharSize sizes[256];

    loader.getCharSizes(sizes);

    // Dimensions des caractères
    for (int i = 0; i < 256; ++i)
    {
        font.size[i].X = static_cast<unsigned int>(sizes[i].width);
        font.size[i].Y = static_cast<unsigned int>(sizes[i].height);
    }

#endif

    TFontId identifiant = m_fonts.size();
    m_fonts.push_back(font);
    return identifiant;
}

} // Namespace TE
