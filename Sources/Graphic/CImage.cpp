/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

//#include <il.h>
#include <SOIL.h>
#include <fstream>
#include <cassert>

#include "Graphic/CImage.hpp"
#include "Core/CApplication.hpp"


namespace TE
{

/**
 * Constructeur par défaut.
 ******************************/

CImage::CImage() :
    m_width  { 0 },
    m_height { 0 },
    m_pixels {}
{

}


/**
 * Constructeur de copie.
 *
 * \param image Image à copier.
 ******************************/

CImage::CImage(const CImage& image) :
    m_width  { image.m_width },
    m_height { image.m_height },
    m_pixels { image.m_pixels }
{

}


/**
 * Constructeur par déplacement.
 *
 * \param image Image à déplacer.
 ******************************/

CImage::CImage(CImage&& image) :
    m_width  { image.m_width },
    m_height { image.m_height },
    m_pixels { std::move(image.m_pixels) }
{
    image.m_width = 0;
    image.m_height = 0;
}


/**
 * Constructeur avec des dimensions.
 *
 * \param width  Largeur de l'image en pixels.
 * \param height Hauteur de l'image en pixels.
 ******************************/

CImage::CImage(unsigned int width, unsigned int height) :
    m_width  { width },
    m_height { height },
    m_pixels { m_width * m_height, CColor() }
{ }


/**
 * Constructeur.
 *
 * \param width  Largeur de l'image en pixels.
 * \param height Hauteur de l'image en pixels.
 * \param pixels Tableau de pixels.
 ******************************/

CImage::CImage(unsigned int width, unsigned int height, const CColor * pixels) :
    m_width  { width },
    m_height { height },
    m_pixels { pixels, pixels + m_width * m_height }
{ }


/**
 * Accesseur pour pixels.
 *
 * \return Tableau de pixels.
 ******************************/

const CColor * CImage::getPixels() const
{
    return &m_pixels[0];
}


/**
 * Charge l'image depuis un fichier.
 *
 * \todo Utiliser les loaders d'images à la place de Devil.
 * \bug La fonction ilLoadImage plante pour certains fichiers VTF.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CImage::loadFromFile(const CString& fileName)
{
    // Test de l'existence du fichier
    std::ifstream file(fileName.toCharArray());
    if (file.fail())
    {
        return false;
    }

#ifdef USE_LIBRARY_DEVIL
    // Génération d'une nouvelle texture
    ILuint texture;
    ilGenImages(1, &texture);
    ilBindImage(texture);

    // Chargement de l'image
    if (!ilLoadImage(fileName.toCharArray()))
    {
        //CApplication::getApp()->log(CString("CImage::loadFromFile : impossible de charger l'image %1 (erreur %2)").arg(fileName).arg(ilGetError()), ILogger::Error);
        return false;
    }

    m_width  = ilGetInteger(IL_IMAGE_WIDTH);
    m_height = ilGetInteger(IL_IMAGE_HEIGHT);

    // Récupération des pixels de l'image
    CColor * firstPixel = reinterpret_cast<CColor *>(ilGetData());
    m_pixels.assign(firstPixel, firstPixel + m_width * m_height);

    // Suppression de la texture
    ilDeleteImages(1, &texture);
#else
    //unsigned char * image = SOIL_load_image(fileName.toCharArray(), &m_width, &m_height, 0, SOIL_LOAD_RGB);
    unsigned char * image = SOIL_load_image(fileName.toCharArray(), reinterpret_cast<int *>(&m_width), reinterpret_cast<int *>(&m_height), 0, SOIL_LOAD_RGBA);  // TODO: check pixel format
    CColor * firstPixel = reinterpret_cast<CColor *>(image);
    m_pixels.assign(firstPixel, firstPixel + m_width * m_height);
    SOIL_free_image_data(image);
#endif

    gApplication->log(CString("Chargement de l'image \"%1\".").arg(fileName));
    return true;
}


/**
 * Sauvegarde l'image dans un fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CImage::saveToFile(const CString& fileName) const
{
    // On crée une copie de l'image dans un format compatible avec DevIL (RGBA 32 bits)
    CImage image = *this;

    // On retourne l'image, sans quoi elle apparaîtrait à l'envers
    image.flip();

#ifdef USE_LIBRARY_DEVIL
    // Génération d'une nouvelle texture
    ILuint texture;
    ilGenImages(1, &texture);
    ilBindImage(texture);

    // Dimensionnement et remplissage de la texture avec les pixels convertis
    if (!ilTexImage(image.getWidth(), image.getHeight(), 1, 4, IL_RGBA, IL_UNSIGNED_BYTE, const_cast<void *>(reinterpret_cast<const void *>(image.getPixels()))))
    {
        gApplication->log(CString::fromUTF8("CImage::saveToFile : impossible de créer la texture de l'image dans \"%1\" (erreur %2).").arg(fileName).arg(ilGetError()));
        return false;
    }

    // Sauvegarde de la texture
    if (!ilSaveImage(fileName.toCharArray()))
    {
        gApplication->log(CString::fromUTF8("CImage::saveToFile : impossible de sauvegarder l'image \"%1\" (erreur %2).").arg(fileName).arg(ilGetError()));
        return false;
    }

    gApplication->log(CString("Sauvegarde de l'image dans \"%1\".").arg(fileName));

    // Suppression de la texture
    ilDeleteImages(1, &texture);
#else
    if (SOIL_save_image(fileName.toCharArray(), SOIL_SAVE_TYPE_BMP, image.getWidth(), image.getHeight(), 4, reinterpret_cast<const unsigned char * const>(image.getPixels())))
    {
        gApplication->log(CString::fromUTF8("CImage::saveToFile : impossible de sauvegarder l'image \"%1\".").arg(fileName));
        return false;
    }
#endif

    return true;
}


/**
 * Remplit l'image d'une couleur.
 *
 * \param color Couleur à utiliser.
 ******************************/

void CImage::fill(const CColor& color)
{
    for (auto& it : m_pixels)
    {
        it = color;
    }
}


/**
 * Change la couleur d'un pixel.
 *
 * \param x     Coordonnée horizontale du pixel.
 * \param y     Coordonnée verticale du pixel.
 * \param color Couleur du pixel.
 ******************************/

void CImage::setPixel(unsigned int x, unsigned int y, const CColor& color)
{
    assert(x < m_width);
    assert(y < m_height);

    m_pixels[x + y * m_width] = color;
}


/**
 * Récupère la couleur d'un pixel.
 *
 * \param x Coordonnée horizontale du pixel.
 * \param y Coordonnée verticale du pixel.
 * \return color du pixel.
 ******************************/

CColor CImage::getPixel(unsigned int x, unsigned int y) const
{
    assert(x < m_width);
    assert(y < m_height);

    return m_pixels[x + y * m_width];
}


/**
 * Retourne l'image verticalement. Inverse le haut et le bas.
 ******************************/

void CImage::flip()
{
    for (unsigned int row = 0; row < m_height / 2; ++row)
    {
        for (unsigned int col = 0; col < m_width; ++col)
        {
            CColor tmp = m_pixels[row * m_width + col];
            m_pixels[row * m_width + col] = m_pixels[(m_height - row - 1) * m_width + col];
            m_pixels[(m_height - row - 1) * m_width + col] = tmp;
        }
    }
}


/**
 * Retourne l'image horizontalement. Inverse la gauche et la droite.
 ******************************/

void CImage::mirror()
{
    for (unsigned int row = 0; row < m_height; ++row)
    {
        for (unsigned int col = 0; col < m_width / 2; ++col)
        {
            CColor tmp = m_pixels[row * m_width + col];
            m_pixels[row * m_width + col] = m_pixels[row * m_width + (m_width - col - 1)];
            m_pixels[row * m_width + (m_width - col - 1)] = tmp;
        }
    }
}


/**
 * Change une couleur de l'image en une autre.
 *
 * \param from Couleur à changer.
 * \param to   Nouvelle couleur.
 ******************************/

void CImage::changeColor(const CColor& from, const CColor& to)
{
    // On parcourt l'image
    for (unsigned int i = 0; i < m_width; ++i)
    {
        for (unsigned int j = 0; j < m_height; ++j)
        {
            if (getPixel(i, j) == from)
            {
                setPixel(i, j, to);
            }
        }
    }
}

/*
void CImage::insertImage(const CImage& image, const CRectangle& rec)
{
    // Rien à insérer
    if (rec.isEmpty() || rec.getX() > m_width || rec.getY() > m_height)
        return;

    const unsigned int numRows = std::min(rec.getHeight(), image.getHeight());
    const unsigned int numCols = std::min(rec.getWidth(), image.getWidth());

    if (numRows <= 0 || numCols <= 0)
        return;

    const unsigned int minRow = std::max(0, rec.getY());
    const unsigned int minCol = std::max(0, rec.getX());

    const unsigned int maxRow = std::min(rec.getY() + numRows, m_height);
    const unsigned int maxCol = std::min(rec.getX() + numCols, m_width);

    for (unsigned int row = minRow, x = 0; row < maxRow; ++row, ++x)
    {
        for (unsigned int col = minCol, y = 0; col < maxCol; ++col, ++y)
        {
            m_pixels[row * m_width + col] = image.m_pixels[x * image.m_width + y];
        }
    }
}
*/

CImage& CImage::operator=(const CImage& image)
{
    m_width = image.m_width;
    m_height = image.m_height;
    m_pixels = image.m_pixels;

    return *this;
}


CImage& CImage::operator=(CImage&& image)
{
    m_width = image.m_width;
    m_height = image.m_height;
    std::swap(m_pixels, image.m_pixels);

    image.m_width = 0;
    image.m_height = 0;
    image.m_pixels.clear();

    return *this;
}

} // Namespace TE
