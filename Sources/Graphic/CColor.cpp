/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CColor.hpp"


namespace TE
{

/*-------------------------------*
 *   Couleurs prédéfinies        *
 *-------------------------------*/

const CColor CColor::White     { 255, 255, 255 };
const CColor CColor::Grey      { 128, 128, 128 };
const CColor CColor::Black     {   0,   0,   0 };
const CColor CColor::Red       { 255,   0,   0 };
const CColor CColor::Green     {   0, 255,   0 };
const CColor CColor::DarkGreen {   0, 128,   0 };
const CColor CColor::Blue      {   0,   0, 255 };
const CColor CColor::Yellow    { 255, 255,   0 };
const CColor CColor::Cyan      {   0, 255, 255 };
const CColor CColor::Magenta   { 255,   0, 255 };
const CColor CColor::Orange    { 255, 128,   0 };


/**
 * Constructeur par défaut.
 *
 * \param color Valeur de la couleur.
 ******************************/

CColor::CColor(uint32_t color) :
    m_color { color }
{ }


/**
 * Constructeur depuis 4 entiers.
 *
 * \param r Niveau de rouge.
 * \param g Niveau de vert.
 * \param b Niveau de bleu.
 * \param a Canal alpha.
 ******************************/

CColor::CColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
    set(r, g, b, a);
}


/**
 * Accesseur pour couleur.
 *
 * \return Valeur de la couleur.
 ******************************/

uint32_t CColor::getColor() const
{
    return m_color;
}


/**
 * Comparaison ==.
 *
 * \param color Couleur à comparer.
 * \return Booléen.
 ******************************/

bool CColor::operator==(const CColor& color) const
{
    return (m_color == color.m_color);
}


/**
 * Comparaison !=.
 *
 * \param color Couleur à comparer.
 * \return Booléen.
 ******************************/

bool CColor::operator!=(const CColor& color) const
{
    return (m_color != color.m_color);
}


/**
 * Opérateur +=.
 *
 * \param color Couleur à ajouter.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator+=(const CColor& color)
{
    unsigned int r = getRed()   + color.getRed();
    unsigned int g = getGreen() + color.getGreen();
    unsigned int b = getBlue()  + color.getBlue();
    unsigned int a = getAlpha() + color.getAlpha();

    setInt(r, g, b, a);

    return *this;
}


/**
 * Opérateur -=.
 *
 * \param color Couleur à soustraire.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator-=(const CColor& color)
{
    int r = getRed()   - color.getRed();
    int g = getGreen() - color.getGreen();
    int b = getBlue()  - color.getBlue();
    int a = getAlpha() - color.getAlpha();

    setInt(r, g, b, a);

    return *this;
}


/**
 * Opérateur binaire +.
 *
 * \param color Couleur à modifier.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator+(const CColor& color) const
{
    unsigned int r = getRed()   + color.getRed();
    unsigned int g = getGreen() + color.getGreen();
    unsigned int b = getBlue()  + color.getBlue();
    unsigned int a = getAlpha() + color.getAlpha();

    CColor newc;
    newc.setInt(r, g, b, a);

    return newc;
}


/**
 * Opérateur binaire -.
 *
 * \param color Couleur à modifier.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator-(const CColor& color) const
{
    int r = getRed()   - color.getRed();
    int g = getGreen() - color.getGreen();
    int b = getBlue()  - color.getBlue();
    int a = getAlpha() - color.getAlpha();

    CColor newc;
    newc.setInt(r, g, b, a);

    return newc;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator*(float k) const
{
    int r = static_cast<int>(getRed()   * k);
    int g = static_cast<int>(getGreen() * k);
    int b = static_cast<int>(getBlue()  * k);
    int a = static_cast<int>(getAlpha() * k);

    CColor newc;
    newc.setInt(r, g, b, a);

    return newc;
}


/**
 * Opérateur *=.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator*=(float k)
{
    int r = static_cast<int>(getRed()   * k);
    int g = static_cast<int>(getGreen() * k);
    int b = static_cast<int>(getBlue()  * k);
    int a = static_cast<int>(getAlpha() * k);

    setInt(r, g, b, a);
    return *this;
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator/(float k) const
{
    return *this * (1.0f / k);
}


/**
 * Opérateur /=.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator/=(float k)
{
    return *this *= (1.0f / k);
}


/**
 * Retourne la couleur en nuance de gris.
 *
 * \return Couleur transformée en gris.
 ******************************/

uint8_t CColor::toGrey() const
{
    return static_cast<uint8_t>(getRed() * 0.30f + getGreen() * 0.59f + getBlue() * 0.11f);
}


/**
 * Retourne la couleur dans le format ARGB.
 *
 * \return Couleur en ARGB.
 ******************************/

uint32_t CColor::toARGB() const
{
#ifdef T_BIG_ENDIAN
    return (getAlpha() << 24) | (getRed() << 16) | (getGreen() << 8) | (getBlue() << 0);
#else
    return (getAlpha() << 0) | (getRed() << 8) | (getGreen() << 16) | (getBlue() << 24);
#endif
}


/**
 * Retourne la couleur dans le format ABGR.
 *
 * \return Couleur en ABGR.
 ******************************/

uint32_t CColor::toABGR() const
{
#ifdef T_BIG_ENDIAN
    return (getAlpha() << 24) | (getBlue() << 16) | (getGreen() << 8) | (getRed() << 0);
#else
    return (getAlpha() << 0) | (getBlue() << 8) | (getGreen() << 16) | (getRed() << 24);
#endif
}


/**
 * Retourne la couleur dans le format RGBA.
 *
 * \return Couleur en RGBA.
 ******************************/

uint32_t CColor::toRGBA() const
{
    return m_color;
}


/**
 * Retourne le canal rouge.
 *
 * \return Canal rouge.
 ******************************/

uint8_t CColor::getRed() const
{
#ifdef T_BIG_ENDIAN
    return static_cast<uint8_t>((m_color & 0xFF000000) >> 24);
#else
    return static_cast<uint8_t>((m_color & 0x000000FF) >> 0);
#endif
}


/**
 * Retourne le canal vert.
 *
 * \return Canal vert.
 ******************************/

uint8_t CColor::getGreen() const
{
#ifdef T_BIG_ENDIAN
    return static_cast<uint8_t>((m_color & 0x00FF0000) >> 16);
#else
    return static_cast<uint8_t>((m_color & 0x0000FF00) >> 8);
#endif
}


/**
 * Retourne le canal bleu.
 *
 * \return Canal bleu.
 ******************************/

uint8_t CColor::getBlue() const
{
#ifdef T_BIG_ENDIAN
    return static_cast<uint8_t>((m_color & 0x0000FF00) >> 8);
#else
    return static_cast<uint8_t>((m_color & 0x00FF0000) >> 16);
#endif
}


/**
 * Retourne le canal alpha.
 *
 * \return Canal alpha.
 ******************************/

uint8_t CColor::getAlpha() const
{
#ifdef T_BIG_ENDIAN
    return static_cast<uint8_t>((m_color & 0x000000FF) >> 0);
#else
    return static_cast<uint8_t>((m_color & 0xFF000000) >> 24);
#endif
}


void CColor::setRed(uint8_t red)
{
#ifdef T_BIG_ENDIAN
    m_color = ((m_color & 0x00FFFFFF) | (red << 24));
#else
    m_color = ((m_color & 0xFFFFFF00) | (red << 0));
#endif
}


void CColor::setGreen(uint8_t green)
{
#ifdef T_BIG_ENDIAN
    m_color = ((m_color & 0xFF00FFFF) | (green << 16));
#else
    m_color = ((m_color & 0xFFFF00FF) | (green << 8));
#endif
}


void CColor::setBlue(uint8_t blue)
{
#ifdef T_BIG_ENDIAN
    m_color = ((m_color & 0xFFFF00FF) | (blue << 8));
#else
    m_color = ((m_color & 0xFF00FFFF) | (blue << 16));
#endif
}


void CColor::setAlpha(uint8_t alpha)
{
#ifdef T_BIG_ENDIAN
    m_color = ((m_color & 0xFFFFFF00) | (alpha << 0));
#else
    m_color = ((m_color & 0x00FFFFFF) | (alpha << 24));
#endif
}


/**
 * Modifie la couleur à partir de 4 réels.
 *
 * \param r Pourcentage de rouge (entre 0 et 1).
 * \param g Pourcentage de vert (entre 0 et 1).
 * \param b Pourcentage de bleu (entre 0 et 1).
 * \param a Pourcentage alpha (entre 0 et 1).
 ******************************/

void CColor::set(float r, float g, float b, float a)
{
    uint8_t nr = static_cast<uint8_t>(255.0f * (r >= 0.0f ? (r <= 1.0f ? r : 1.0f) : 0.0f));
    uint8_t ng = static_cast<uint8_t>(255.0f * (g >= 0.0f ? (g <= 1.0f ? g : 1.0f) : 0.0f));
    uint8_t nb = static_cast<uint8_t>(255.0f * (b >= 0.0f ? (b <= 1.0f ? b : 1.0f) : 0.0f));
    uint8_t na = static_cast<uint8_t>(255.0f * (a >= 0.0f ? (a <= 1.0f ? a : 1.0f) : 0.0f));

    set(nr, ng, nb, na);
}


/**
 * Modifie la couleur à partir de 4 composantes.
 *
 * \param r Niveau de rouge (entre 0 et 255).
 * \param g Niveau de vert (entre 0 et 255).
 * \param b Niveau de bleu (entre 0 et 255).
 * \param a Canal alpha (entre 0 et 255).
 ******************************/

void CColor::set(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
#ifdef T_BIG_ENDIAN
    m_color = (r << 24) | (g << 16) | (b << 8) | (a << 0);
#else
    m_color = (r << 0) | (g << 8) | (b << 16) | (a << 24);
#endif
}


/**
 * Module deux couleurs.
 *
 * \param color Couleur avec laquelle moduler.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::modulate(const CColor& color) const
{
    uint8_t r = static_cast<uint8_t>(static_cast<float>(getRed()  ) * color.getRed()   / 255.0f);
    uint8_t g = static_cast<uint8_t>(static_cast<float>(getGreen()) * color.getGreen() / 255.0f);
    uint8_t b = static_cast<uint8_t>(static_cast<float>(getBlue() ) * color.getBlue()  / 255.0f);
    uint8_t a = static_cast<uint8_t>(static_cast<float>(getAlpha()) * color.getAlpha() / 255.0f);

    return CColor(r, g, b, a);
}


/**
 * Détermine l'interpolation entre deux couleurs.
 * Si le pourcentage vaut 1, \c color est retournée.
 *
 * \param color      Couleur avec laquelle interpoler.
 * \param percentage Pourcentage d'interpolation (entre 0 et 1).
 * \return Couleur interpolée.
 */

CColor CColor::interpolate(const CColor& color, float percentage) const
{
    if (percentage <= 0.0f)
        return *this;

    if (percentage >= 1.0f)
        return color;

    const float invPercentage = 1.0f - percentage;

    uint8_t r = static_cast<uint8_t>((static_cast<float>(getRed()  ) * invPercentage) + (static_cast<float>(color.getRed()  ) * percentage));
    uint8_t g = static_cast<uint8_t>((static_cast<float>(getGreen()) * invPercentage) + (static_cast<float>(color.getGreen()) * percentage));
    uint8_t b = static_cast<uint8_t>((static_cast<float>(getBlue() ) * invPercentage) + (static_cast<float>(color.getBlue() ) * percentage));
    uint8_t a = static_cast<uint8_t>((static_cast<float>(getAlpha()) * invPercentage) + (static_cast<float>(color.getAlpha()) * percentage));

    return CColor(r, g, b, a);
}


/**
 * Convertit la couleur en 4 floats RGBA. Chaque valeur est comprise entre 0 et 1.
 *
 * \param dest Tableau de destination.
 ******************************/

void CColor::toFloat(float dest[]) const
{
    dest[0] = static_cast<float>(getRed()  ) * (1.0f / 255.0f);
    dest[1] = static_cast<float>(getGreen()) * (1.0f / 255.0f);
    dest[2] = static_cast<float>(getBlue() ) * (1.0f / 255.0f);
    dest[3] = static_cast<float>(getAlpha()) * (1.0f / 255.0f);
}


/**
 * Convertit la couleur en 3 floats RGB. Chaque valeur est comprise entre 0 et 1.
 *
 * \param dest Tableau de destination.
 ******************************/

void CColor::toFloatRGB(float dest[]) const
{
    dest[0] = static_cast<float>(getRed()  ) * (1.0f / 255.0f);
    dest[1] = static_cast<float>(getGreen()) * (1.0f / 255.0f);
    dest[2] = static_cast<float>(getBlue() ) * (1.0f / 255.0f);
}


/**
 * Modifie la couleur à partir de 4 entiers.
 *
 * \param r Niveau de rouge (entre 0 et 255).
 * \param g Niveau de vert (entre 0 et 255).
 * \param b Niveau de bleu (entre 0 et 255).
 * \param a Canal alpha (entre 0 et 255).
 ******************************/

void CColor::setInt(int r, int g, int b, int a)
{
    uint8_t nr = (r >= 0) ? (r <= 255 ? r : 255) : 0;
    uint8_t ng = (g >= 0) ? (g <= 255 ? g : 255) : 0;
    uint8_t nb = (b >= 0) ? (b <= 255 ? b : 255) : 0;
    uint8_t na = (a >= 0) ? (a <= 255 ? a : 255) : 0;

    set(nr, ng, nb, na);
}


/**
 * Surcharge de l'opérateur >> entre un flux et une couleur.
 *
 * \relates CColor
 *
 * \param stream Flux d'entrée.
 * \param color Couleur.
 * \return Référence sur le flux d'entrée.
 ******************************/

std::istream& operator>>(std::istream& stream, CColor& color)
{
    int nr, ng, nb, na;
    stream >> nr >> ng >> nb >> na;

    uint8_t r = (nr >= 0) ? (nr <= 255 ? nr : 255) : 0;
    uint8_t g = (ng >= 0) ? (ng <= 255 ? ng : 255) : 0;
    uint8_t b = (nb >= 0) ? (nb <= 255 ? nb : 255) : 0;
    uint8_t a = (na >= 0) ? (na <= 255 ? na : 255) : 0;

    color.set(r, g, b, a);

    return stream;
}


/**
 * Surcharge de l'opérateur << entre un flux et une couleur.
 *
 * \relates CColor
 *
 * \param stream Flux de sortie.
 * \param color  Couleur.
 * \return Référence sur le flux de sortie.
 ******************************/

std::ostream& operator<<(std::ostream& stream, const CColor& color)
{
    return stream << static_cast<int>(color.getRed()) << " " << static_cast<int>(color.getGreen()) << " " << static_cast<int>(color.getBlue()) << " " << static_cast<int>(color.getAlpha());
}

} // Namespace TE
