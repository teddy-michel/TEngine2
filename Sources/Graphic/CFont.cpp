/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graphic/CFont.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/CApplication.hpp"


namespace TE
{
    
/**
 * Constructeur.
 ******************************/

CFont::CFont() :
#ifdef T_USE_FREETYPE
    m_face{ nullptr },
#endif
    m_hasKerning{ false },
    m_currentSize{ 0 }
{

}


/**
 * Destructeur.
 ******************************/

CFont::~CFont()
{
#ifdef T_USE_FREETYPE
    if (m_face)
    {
        FT_Error error = FT_Done_Face(m_face);
        if (error != 0)
        {
            gApplication->log(CString("Error with FreeType (FT_Done_Face: code %1)").arg(error));
        }
    }
#endif
}


/**
 * Charge une police de caractères depuis un fichier.
 *
 * \param fileName Nom du fichier contenant la police.
 * \return True en cas de succès, false en cas d'erreur.
 ******************************/

bool CFont::loadFromFile(const CString& fileName)
{
    gApplication->log(CString("Load font from file \"%1\"").arg(fileName));
#ifdef T_USE_FREETYPE
    if (m_face)
    {
        // Déjà chargé
        return false;
    }

    FT_Error error = FT_New_Face(gFontManager->m_library, fileName.toCharArray(), 0, &m_face);

    if (error == FT_Err_Unknown_File_Format)
    {
        gApplication->log(CString("Error with FreeType (FT_New_Face: unknown file format)").arg(error));
        return false;
    }
    if (error != 0)
    {
        gApplication->log(CString("Error with FreeType (FT_New_Face: code %1)").arg(error));
        return false;
    }

    m_hasKerning = FT_HAS_KERNING(m_face);

    error = FT_Set_Char_Size(m_face, 0, 16 * 64, 300, 300);
    setFontSize(16);

    if (error != 0)
    {
        gApplication->log(CString("Error with FreeType (FT_Set_Char_Size: code %1)").arg(error));
    }
#endif

    return true;
}


CString CFont::getFamilyName() const
{
#ifdef T_USE_FREETYPE
    if (m_face == nullptr)
        return CString();

    if (m_face->family_name)
        return CString(m_face->family_name);
#endif

    return CString();
}


CString CFont::getFontName() const
{
#ifdef T_USE_FREETYPE
    if (m_face == nullptr)
        return CString();

    if (m_face->family_name && m_face->style_name)
        return CString(m_face->family_name) + " " + CString(m_face->style_name);
#endif

    return CString();
}


/**
 * Ajoute à un buffer le texte à afficher.
 *
 * \todo Aller automatiquement à la ligne si demandé.
 * \todo Vérifier les coordonnées de texture pour éviter de devoir retourner l'image.
 * \todo Utiliser la classe CFormattedString.
 *
 * \param buffer Buffer graphique.
 * \param params Paramètres du texte à afficher.
 *
 * Les codes ASCII suivants permettent de changer la couleur du texte :
 * \li 1 : Couleur normale.
 * \li 2 : Blanc.
 * \li 3 : Noir.
 * \li 4 : Rouge.
 * \li 5 : Bleu.
 * \li 6 : Vert.
 * \li 7 : Jaune.
 * \li 8 : Orange.
 * \li 12 : Surligné.
 ******************************/

void CFont::drawText(CBuffer2D * buffer, const TTextParams& params)
{
    assert(buffer != nullptr);

#ifdef T_USE_FREETYPE
    if (m_face == nullptr)
    {
        return;
    }

    if (params.size < CFontManager::minFontSize || params.size > CFontManager::maxFontSize)
    {
        return;
    }

    setFontSize(params.size);

    // Variables
    float posX = static_cast<float>(params.rec.x) - static_cast<float>(params.left);
    float posY = static_cast<float>(params.rec.y) - static_cast<float>(params.top);

    bool highlight = false;
    bool drawChar = true;
    bool useKerning = (params.kerning && m_hasKerning);
    CColor colorActual = params.color;
    FT_UInt previous = 0;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        // Gestion des caractères spéciaux
        switch (c->unicode())
        {
            // Couleur normale
            case 1:
                colorActual = params.color;
                continue;

            // Blanc
            case 2:
                colorActual = CColor::White;
                continue;

            // Noir
            case 3:
                colorActual = CColor::Black;
                continue;

            // Rouge
            case 4:
                colorActual = CColor::Red;
                continue;

            // Bleu
            case 5:
                colorActual = CColor::Blue;
                continue;

            // Vert
            case 6:
                colorActual = CColor::Green;
                continue;

            // Jaune
            case 7:
                colorActual = CColor::Yellow;
                continue;

            // Orange
            case 8:
                colorActual = CColor::Orange;
                continue;

            // Surlignement
            case 12:
                highlight = !highlight;
                continue;

            default:
                break;
        }

        TCharParams charParams = getCharParams(params.size, *c);

        FT_Error error;

        drawChar = ((params.rec.z == 0 || (posX < params.rec.x + params.rec.z && posX + charParams.rect.z > static_cast<float>(params.rec.x))) &&
                    (params.rec.w == 0 || (posY < params.rec.y + params.rec.w && posY + charParams.rect.w > static_cast<float>(params.rec.y))));

        if (useKerning && previous && charParams.glyphIndex)
        {
            FT_Vector delta;
            error = FT_Get_Kerning(m_face, previous, charParams.glyphIndex, FT_KERNING_DEFAULT, &delta);

            if (error == 0)
            {
                posX += delta.x >> 6;
                posY += delta.y >> 6;
            }

            previous = charParams.glyphIndex;
        }

        // Pourcentages d'affichage du caractère
        float perX1 = 0.0f;
        float perX2 = 1.0f;
        float perY1 = 0.0f;
        float perY2 = 1.0f;

        if (params.rec.z > 0)
        {
            // Caractère découpé à gauche
            if (posX < params.rec.x && posX + charParams.rect.z > params.rec.x)
            {
                perX1 = (params.rec.x - posX) / charParams.rect.z;
            }

            // Caractère découpé à droite
            if (posX < params.rec.x + params.rec.z &&
                posX + charParams.rect.z > params.rec.x + params.rec.z)
            {
                perX2 = (params.rec.x + params.rec.z - posX) / charParams.rect.z;
            }
        }

        if (params.rec.w > 0)
        {
            // Caractère découpé en bas
            if (posY < params.rec.y &&
                posY + charParams.rect.w > params.rec.y)
            {
                perY1 = (params.rec.y - posY) / charParams.rect.w;
            }

            // Caractère découpé en bas
            if (posY < params.rec.y + params.rec.w &&
                posY + charParams.rect.w > params.rec.y + params.rec.w)
            {
                perY2 = (params.rec.y + params.rec.w - posY) / charParams.rect.w;
            }
        }

        // Surlignement du texte
        if (highlight && drawChar)
        {
            const glm::vec2 position{ posX + charParams.rect.z * perX1, posY + params.size * perY1 };
            const glm::vec2 size{ charParams.rect.z * (perX2 - perX1), params.size * (perY2 - perY1) };
            const glm::vec4 color{ 1.0f, 0.5f, 0.0f, 0.9f }; // Couleur de surlignement
            buffer->addRectangle(position, size, color);
        }

        switch (c->unicode())
        {
            // Saut de ligne
            case '\n':
            case '\r':
                posX = static_cast<float>(params.rec.x) - static_cast<float>(params.left);
                posY += charParams.rect.w;
                continue;

            // Espace
            case ' ':
                posX += charParams.rect.z;
                continue;

            // Tabulation horizontale
            case '\t':
                posX += 4.0f * charParams.rect.z; // TODO: gérer ça un peu mieux (alignement...)
                continue;

            default:

                if (drawChar)
                {
                    const glm::vec2 position{ posX + charParams.rect.x + charParams.pixmap.getWidth() * perX1,
                                              posY + params.size - charParams.rect.y + charParams.pixmap.getHeight() * perY1 };
                    const glm::vec2 size{ charParams.pixmap.getWidth() * (perX2 - perX1),
                                          charParams.pixmap.getHeight() * (perY2 - perY1)};
                    glm::vec4 color;
                    colorActual.toFloat(&color[0]);
                    buffer->addRectangle(position, size, color, charParams.texture, glm::vec2{ perX1, perY1 }, glm::vec2{ perX2, perY2 });
                }

                break;
        }

        // Incrémentation de la position
        posX += charParams.rect.z;
    }

#endif
}


glm::u32vec2 CFont::getTextSize(const TTextParams& params)
{
    std::vector<float> lengths;
    glm::vec2 sz(0.0f, static_cast<float>(params.size * 1.4f)); // TODO: d'où sort ce 1.4f ?

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        TCharParams charParams = getCharParams(params.size, *c);

        switch (c->unicode())
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lengths.push_back(sz.x);
                sz.y += charParams.rect.z;
                sz.x = 0.0f;
                break;

            // Tabulation horizontale
            case '\t':
                sz.x += 4 * charParams.rect.z;
                break;

            // Défaut
            default:
                sz.x += charParams.rect.z;
                break;
        }
    }

    // On prend la longueur de la ligne la plus longue
    lengths.push_back(sz.x);
    sz.x = *std::max_element(lengths.begin(), lengths.end());

    return glm::u32vec2(static_cast<unsigned int>(sz.x), static_cast<unsigned int>(sz.y));
}


void CFont::getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines)
{
    TTextLine currentLine;

    unsigned int offset = 0;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        TCharParams charParams = getCharParams(params.size, *c);

        switch (c->unicode())
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lines.push_back(currentLine);
                currentLine.len.clear();
                currentLine.offset = ++offset;
                break;

            // Tabulation horizontale
            case '\t':
                ++offset;
                currentLine.len.push_back(4.0f * charParams.rect.z);
                break;

            // Défaut
            default:
                ++offset;
                currentLine.len.push_back(charParams.rect.z);
                break;
        }
    }

    lines.push_back(currentLine);
}


void CFont::getTextLineSize(const TTextParams& params, TTextLine& line)
{
    std::vector<float> lengths;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        TCharParams charParams = getCharParams(params.size, *c);

        switch (c->unicode())
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\r':
            case '\n':
                return;

            // Tabulation horizontale
            case '\t':
                line.len.push_back(4.0f * charParams.rect.z);
                break;

            // Défaut
            default:
                line.len.push_back(charParams.rect.w);
                break;
        }
    }
}


CFont::TCharParams CFont::getCharParams(int size, const CChar& character)
{
    if (size < CFontManager::minFontSize || size > CFontManager::maxFontSize)
        return TCharParams();

    if (m_chars.count(size) == 0)
    {
        setFontSize(size);

        // Chargement des caractères ASCII
        for (uint32_t charCode = 0x20; charCode < 0x80; ++charCode)
        {
            m_chars[size][CChar(charCode)] = loadChar(charCode);
        }
    }

    if (m_chars[size].count(character) == 0)
    {
        setFontSize(size);

        // Chargement du caractère
        m_chars[size][character] = loadChar(character.unicode());
    }

    return m_chars[size][character];
}


void CFont::setFontSize(int size) const
{
    assert(size >= CFontManager::minFontSize && size <= CFontManager::maxFontSize);

    if (m_currentSize == size)
        return;

#ifdef T_USE_FREETYPE

    assert(m_face != nullptr);

    FT_Error error = FT_Set_Pixel_Sizes(m_face, 0, size);
    if (error == 0)
    {
        m_currentSize = size;
    }

#endif
}


CFont::TCharParams CFont::loadChar(uint32_t charCode)
{
//gApplication->log(CString("loadChar(%1, %2, %3)").arg(charCode).arg(m_currentSize).arg(getFontName()));
    TCharParams charParams;

#ifdef T_USE_FREETYPE
    assert(m_face != nullptr);

    charParams.glyphIndex = FT_Get_Char_Index(m_face, charCode);

    FT_Error error = FT_Load_Char(m_face, charCode, FT_LOAD_RENDER);
    if (error != 0)
    {
        gApplication->log(CString("Error with FreeType (FT_Load_Glyph: code %1)").arg(error));
        return charParams;
    }

    charParams.rect.x = m_face->glyph->bitmap_left;
    charParams.rect.y = m_face->glyph->bitmap_top;
    charParams.rect.z = m_face->glyph->advance.x >> 6;
    //charParams.rect.setHeight(m_face->height >> 6);
    charParams.rect.w = m_currentSize * 1.4f; // TODO: d'où sort ce 1.4f ?
/*
gApplication->log(CString("    X = %1, Y = %2, advance = %3, height = %4")
                            .arg(m_face->glyph->bitmap_left)
                            .arg(m_face->glyph->bitmap_top)
                            .arg((int)m_face->glyph->advance.x >> 6)
                            .arg(m_face->height >> 6)
                           );
gApplication->log(CString("    Metrics.width = %1, Metrics.height = %2, horiBearingX = %3, horiBearingY = %4 horiAdvance = %5")
                            .arg((int)m_face->glyph->metrics.width >> 6)
                            .arg((int)m_face->glyph->metrics.height >> 6)
                            .arg((int)m_face->glyph->metrics.horiBearingX >> 6)
                            .arg((int)m_face->glyph->metrics.horiBearingY >> 6)
                            .arg((int)m_face->glyph->metrics.horiAdvance >> 6)
                           );
*/
    if (m_face->glyph->bitmap.pixel_mode == FT_PIXEL_MODE_GRAY)
    {
        const int numRows = m_face->glyph->bitmap.rows;
        const int numCols = m_face->glyph->bitmap.width;

        if (numRows > 0 && numCols > 0)
        {
            CColor * data = new CColor[numRows * numCols];
            uint8_t * bufferPtr = m_face->glyph->bitmap.buffer;
            int offset = 0;

            //gApplication->log(CString("    img.width = %1, img.height = %2, pitch = %3").arg(numCols).arg(numRows).arg(m_face->glyph->bitmap.pitch));

            for (int row = 0; row < numRows; ++row)
            {
                for (int col = 0; col < numCols; ++col)
                {
                    data[offset++] = CColor(255, 255, 255, bufferPtr[col]);
                }

                bufferPtr += m_face->glyph->bitmap.pitch;
            }

            charParams.pixmap = CImage(numCols, numRows, data); // TODO: Copie inutile de data
            //charParams.pixmap.saveToFile(CString("font_%1_%2_%3.bmp").arg(getFontName()).arg(m_currentSize).arg(charCode));
            charParams.texture = gTextureManager->loadTexture(CString(":font:%1:%2:%3").arg(getFontName()).arg(m_currentSize).arg(charCode), charParams.pixmap);

            delete[] data;
        }
    }

#endif

    return charParams;
}

} // Namespace TE
