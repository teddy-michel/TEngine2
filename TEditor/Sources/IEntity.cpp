
#include <QGroupBox>
#include <QLineEdit>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QComboBox>

#include "IEntity.hpp"
#include "CMap.hpp"
#include "CEntityMap.hpp"


IEntity::IEntity(CMap * map, const QString& name) :
    m_map{ map },
    m_name{ name },
    m_parent{ nullptr },
    m_parentName{},
    m_position_x{ 0.0f },
    m_position_y{ 0.0f },
    m_position_z{ 0.0f },
    m_orientation_x{ 0.0f },
    m_orientation_y{ 0.0f },
    m_orientation_z{ 0.0f }
{

}


IEntity::~IEntity()
{

}


QString IEntity::getName() const
{
    return m_name;
}


void IEntity::setName(const QString& name)
{
    m_name = name;
}


IEntity * IEntity::getParent() const
{
    return m_parent;
}


bool IEntity::hasAncestor(IEntity * entity) const
{
    if (entity == this)
    {
        return true;
    }

    if (m_parent != nullptr)
    {
        return m_parent->hasAncestor(entity);
    }

    return false;
}


void IEntity::setParent(IEntity * parent)
{
    m_parent = parent;
}


float IEntity::getPositionX() const
{
    return m_position_x;
}


float IEntity::getPositionY() const
{
    return m_position_y;
}


float IEntity::getPositionZ() const
{
    return m_position_z;
}


void IEntity::setPosition(float x, float y, float z)
{
    m_position_x = x;
    m_position_y = y;
    m_position_z = z;
}


float IEntity::getOrientationX() const
{
    return m_orientation_x;
}


float IEntity::getOrientationY() const
{
    return m_orientation_y;
}


float IEntity::getOrientationZ() const
{
    return m_orientation_z;
}


void IEntity::setOrientation(float x, float y, float z)
{
    m_orientation_x = x;
    m_orientation_y = y;
    m_orientation_z = z;
}


QMap<QString, QWidget *> IEntity::initWidget(QLayout * widget)
{
    QGroupBox * group = new QGroupBox{ "entity" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // name
    QLineEdit * editName = new QLineEdit{ group };
    editName->setObjectName("entity.name");
    editName->setText(m_name);
    layout->addRow(QObject::tr("Name:"), editName);

    // parent
    QComboBox * editParent = new QComboBox{ group };
    editParent->setObjectName("entity.parent");

    if (m_map->getMapEntity() == this)
    {
        editParent->setEnabled(false);
    }

    for (auto& entity : m_map->getEntities())
    {
        if (entity != this && !entity->hasAncestor(this))
        {
            editParent->addItem(QString("%1 <%2>").arg(entity->getName()).arg(entity->getType()), QVariant::fromValue(entity));
        }
    }
    if (m_parent != nullptr)
    {
        editParent->setCurrentIndex(editParent->findData(QVariant::fromValue(m_parent)));
    }
    layout->addRow(QObject::tr("Parent:"), editParent);

    // position
    QDoubleSpinBox * editPositionX = new QDoubleSpinBox{ group };
    editPositionX->setObjectName("entity.position_x");
    editPositionX->setRange(-9999999, 9999999);
    editPositionX->setDecimals(3);
    editPositionX->setValue(static_cast<double>(m_position_x));

    QDoubleSpinBox * editPositionY = new QDoubleSpinBox{ group };
    editPositionY->setObjectName("entity.position_y");
    editPositionY->setRange(-9999999, 9999999);
    editPositionY->setDecimals(3);
    editPositionY->setValue(static_cast<double>(m_position_y));

    QDoubleSpinBox * editPositionZ = new QDoubleSpinBox{ group };
    editPositionZ->setObjectName("entity.position_z");
    editPositionZ->setRange(-9999999, 9999999);
    editPositionZ->setDecimals(3);
    editPositionZ->setValue(static_cast<double>(m_position_z));

    QHBoxLayout * layoutPosition = new QHBoxLayout{};
    layoutPosition->addWidget(editPositionX);
    layoutPosition->addWidget(editPositionY);
    layoutPosition->addWidget(editPositionZ);
    layout->addRow(QObject::tr("Position:"), layoutPosition);

    // orientation
    QDoubleSpinBox * editOrientationX = new QDoubleSpinBox{ group };
    editOrientationX->setObjectName("entity.orientation_x");
    editOrientationX->setRange(-180, 180);
    editOrientationX->setDecimals(3);
    editOrientationX->setValue(static_cast<double>(m_orientation_x));

    QDoubleSpinBox * editOrientationY = new QDoubleSpinBox{ group };
    editOrientationY->setObjectName("entity.orientation_y");
    editOrientationY->setRange(-180, 180);
    editOrientationY->setDecimals(3);
    editOrientationY->setValue(static_cast<double>(m_orientation_y));

    QDoubleSpinBox * editOrientationZ = new QDoubleSpinBox{ group };
    editOrientationZ->setObjectName("entity.orientation_z");
    editOrientationZ->setRange(-180, 180);
    editOrientationZ->setDecimals(3);
    editOrientationZ->setValue(static_cast<double>(m_orientation_z));

    QHBoxLayout * layoutOrientation = new QHBoxLayout{};
    layoutOrientation->addWidget(editOrientationX);
    layoutOrientation->addWidget(editOrientationY);
    layoutOrientation->addWidget(editOrientationZ);
    layout->addRow(QObject::tr("Orientation:"), layoutOrientation);

    QMap<QString, QWidget *> widgets;
    widgets["entity.name"] = editName;
    widgets["entity.parent"] = editParent;
    widgets["entity.position_x"] = editPositionX;
    widgets["entity.position_y"] = editPositionY;
    widgets["entity.position_z"] = editPositionZ;
    widgets["entity.orientation_x"] = editOrientationX;
    widgets["entity.orientation_y"] = editOrientationY;
    widgets["entity.orientation_z"] = editOrientationZ;
    return widgets;
}


bool IEntity::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    // name
    QLineEdit * editName = qobject_cast<QLineEdit *>(widgets["entity.name"]);
    QString name = editName->text();
    if (name.isEmpty())
    {
        return false;
    }
    // TODO: check name unicity
    m_name = name;

    // parent
    QComboBox * editParent = qobject_cast<QComboBox *>(widgets["entity.parent"]);
    m_parent = editParent->currentData().value<IEntity *>();

    // position
    QDoubleSpinBox * editPositionX = qobject_cast<QDoubleSpinBox *>(widgets["entity.position_x"]);
    m_position_x = static_cast<float>(editPositionX->value());
    QDoubleSpinBox * editPositionY = qobject_cast<QDoubleSpinBox *>(widgets["entity.position_y"]);
    m_position_y = static_cast<float>(editPositionY->value());
    QDoubleSpinBox * editPositionZ = qobject_cast<QDoubleSpinBox *>(widgets["entity.position_z"]);
    m_position_z = static_cast<float>(editPositionZ->value());

    // orientation
    QDoubleSpinBox * editOrientationX = qobject_cast<QDoubleSpinBox *>(widgets["entity.orientation_x"]);
    m_orientation_x = static_cast<float>(editOrientationX->value());
    QDoubleSpinBox * editOrientationY = qobject_cast<QDoubleSpinBox *>(widgets["entity.orientation_y"]);
    m_orientation_y = static_cast<float>(editOrientationY->value());
    QDoubleSpinBox * editOrientationZ = qobject_cast<QDoubleSpinBox *>(widgets["entity.orientation_z"]);
    m_orientation_z = static_cast<float>(editOrientationZ->value());

    return true;
}


QByteArray IEntity::loadFromBuffer(const QByteArray& data)
{
    // Qt c'est de la merde !
    size_t nameLen = strlen(data.data());
    char * name = new char[nameLen + 1];
    memset(name, 0, nameLen + 1);
    strncpy(name, data.data(), nameLen);
    m_name = name;
    delete[] name;

    if (data.size() < m_name.length() + 1 + 12 + 12)
    {
        return QByteArray();
    }

    // Qt c'est de la merde !
    size_t parentLen = strlen(data.data() + nameLen + 1);
    char * parentName = new char[parentLen + 1];
    memset(parentName, 0, parentLen + 1);
    strncpy(parentName, data.data() + nameLen + 1, parentLen);
    m_parentName = parentName;
    delete[] parentName;

    if (data.size() < nameLen + 1 + parentLen + 1 + 12 + 12)
    {
        return QByteArray();
    }

    const char * dataPtr = data.data() + nameLen + 1 + parentLen + 1;
    m_position_x = *(reinterpret_cast<const float *>(dataPtr));
    m_position_y = *(reinterpret_cast<const float *>(dataPtr + 4));
    m_position_z = *(reinterpret_cast<const float *>(dataPtr + 8));
    m_orientation_x = *(reinterpret_cast<const float *>(dataPtr + 12));
    m_orientation_y = *(reinterpret_cast<const float *>(dataPtr + 16));
    m_orientation_z = *(reinterpret_cast<const float *>(dataPtr + 20));

    return data.mid(nameLen + 1 + parentLen + 1 + 12 + 12);
}


QByteArray IEntity::writeToBuffer() const
{
    QByteArray data;
    data += m_name.toLocal8Bit();
    data += '\0';
    if (m_parent != nullptr)  // TODO: remove?
    {
        data += m_parent->m_name.toLocal8Bit();
    }
    data += '\0';
    data += QByteArray(reinterpret_cast<const char *>(&m_position_x), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_position_y), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_position_z), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_orientation_x), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_orientation_y), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_orientation_z), 4);
    return data;
}


const char * IEntity::getType() const
{
    return "entity";
}
