
#include "CDialogEditEntity.hpp"
#include "IEntity.hpp"
#include "CEntityPlayerStart.hpp"
#include "CEntityBox.hpp"
#include "CMap.hpp"
#include "ui_DialogEditEntity.h"


CDialogEditEntity::CDialogEditEntity(IEntity * entity, CMap * map, QWidget * parent) :
    QDialog{ parent },
    m_ui{ new Ui::DialogEditEntity },
    m_entity{ entity },
    m_map{ map }
{
    m_ui->setupUi(this);

    m_widgets = m_entity->initWidget(m_ui->layout);
}


CDialogEditEntity::~CDialogEditEntity()
{
    delete m_ui;
}


void CDialogEditEntity::accept()
{
    if (m_entity->loadFromWidgets(m_widgets))
    {
        emit entityChanged(m_entity);
    }

    QDialog::accept();
}
