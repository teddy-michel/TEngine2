
#pragma once

#include <QString>
#include <QList>


class IEntity;
class CEntityMap;


class CMap
{
public:

    CMap();
    ~CMap();

    bool loadFromFile(const QString& filename);
    bool saveToFile(const QString& filename);

    QList<IEntity *> getEntities() const;
    IEntity * getEntityByName(const QString& name) const;
    void addEntity(IEntity * entity);
    void removeEntity(IEntity * entity);
    QString getValidName(const QString& prefix = "entity") const;
    CEntityMap * getMapEntity() const;

private:

    struct THeader
    {
        char magic[4];     ///< Numéro magique ("TMAP").
        uint32_t numLumps; ///< Nombre de lumps.

        THeader() : magic{ 'T', 'M', 'A', 'P' }, numLumps{0} {}
    };

    struct TLumpDescription
    {
        uint32_t type;   ///< Type de données stockées.
        uint32_t offset; ///< Offset depuis le début du fichier.
        uint32_t size;   ///< Taille totale en octets.
        uint32_t count;  ///< Nombre d'éléments.

        TLumpDescription() : type{0}, offset{0}, size{0}, count{0} {}
    };

    enum TLumpType
    {
        LumpEntities = 1
    };

    struct TEntity
    {
        uint32_t offset;
        uint32_t size;
        char type[24];
    };

    QByteArray buildLumpEntities(uint32_t offset, uint32_t * count) const;

    QList<IEntity *> m_entities;
    CEntityMap * m_mapEntity;
};
