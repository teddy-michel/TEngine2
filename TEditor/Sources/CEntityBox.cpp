
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QFormLayout>

#include "CEntityBox.hpp"


CEntityBox::CEntityBox(CMap * map, const QString& name) :
    IGraphicEntity{ map, name },
    m_size_x{ 100.0f },
    m_size_y{ 100.0f },
    m_size_z{ 100.0f },
    m_texture{},
    m_solid{ true },
    m_dynamic{ false },
    m_mass{ 1.0f }
{

}


CEntityBox::~CEntityBox()
{

}


float CEntityBox::getSizeX() const
{
    return m_size_x;
}


float CEntityBox::getSizeY() const
{
    return m_size_y;
}


float CEntityBox::getSizeZ() const
{
    return m_size_z;
}


void CEntityBox::setSize(float x, float y, float z)
{
    m_size_x = (x < 0.0f ? 0.0f : x);
    m_size_y = (y < 0.0f ? 0.0f : y);
    m_size_z = (z < 0.0f ? 0.0f : z);
}


bool CEntityBox::isSolid() const
{
    return m_solid;
}


void CEntityBox::setSolid(bool solid)
{
    m_solid = solid;
}


bool CEntityBox::isDynamic() const
{
    return m_dynamic;
}


void CEntityBox::setDynamic(bool dynamic)
{
    m_dynamic = dynamic;
}


float CEntityBox::getMass() const
{
    return m_mass;
}


void CEntityBox::setMass(float mass)
{
    m_mass = mass;
}


QString CEntityBox::getTexture() const
{
    return m_texture;
}


void CEntityBox::setTexture(const QString& texture)
{
    m_texture = texture;
}


QMap<QString, QWidget *> CEntityBox::initWidget(QLayout * widget)
{
    QMap<QString, QWidget *> widgets = IGraphicEntity::initWidget(widget);

    QGroupBox * group = new QGroupBox{ "box" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // size
    QDoubleSpinBox * editSizeX = new QDoubleSpinBox{ group };
    editSizeX->setObjectName("box.size_x");
    editSizeX->setRange(-999999, 999999);
    editSizeX->setDecimals(3);
    editSizeX->setValue(static_cast<double>(m_size_x));

    QDoubleSpinBox * editSizeY = new QDoubleSpinBox{ group };
    editSizeY->setObjectName("box.size_y");
    editSizeY->setRange(-999999, 999999);
    editSizeY->setDecimals(3);
    editSizeY->setValue(static_cast<double>(m_size_y));

    QDoubleSpinBox * editSizeZ = new QDoubleSpinBox{ group };
    editSizeZ->setObjectName("box.size_z");
    editSizeZ->setRange(-999999, 999999);
    editSizeZ->setDecimals(3);
    editSizeZ->setValue(static_cast<double>(m_size_z));

    QHBoxLayout * layoutSize = new QHBoxLayout{ group };
    layoutSize->addWidget(editSizeX);
    layoutSize->addWidget(editSizeY);
    layoutSize->addWidget(editSizeZ);
    layout->addRow(QObject::tr("Size:"), layoutSize);

    // texture
    QLineEdit * editTexture = new QLineEdit{ group };
    editTexture->setObjectName("box.texture");
    editTexture->setText(m_texture);
    layout->addRow(QObject::tr("Texture:"), editTexture);

    // solid
    QCheckBox * editSolid = new QCheckBox{ QObject::tr("box is solid"), group };
    editSolid->setObjectName("box.solid");
    editSolid->setChecked(m_solid);
    layout->addRow(QObject::tr("Solid:"), editSolid);

    // dynamic
    QCheckBox * editDynamic = new QCheckBox{ QObject::tr("box is dynamic"), group };
    editDynamic->setObjectName("box.dynamic");
    editDynamic->setChecked(m_dynamic);
    layout->addRow(QObject::tr("Dynamic:"), editDynamic);
/*
    if (!m_solid)
    {
        editDynamic->setEnabled(false);
    }

    editSolid->connect(editSolid, SIGNAL(toggled(bool)), editDynamic, SLOT(setEnabled(bool)));
*/
    // mass
    QDoubleSpinBox * editMass = new QDoubleSpinBox{ group };
    editMass->setObjectName("box.mass");
    editMass->setRange(0, 999999);
    editMass->setDecimals(3);
    editMass->setValue(static_cast<double>(m_mass));
    layout->addRow(QObject::tr("Mass:"), editMass);
/*
    if (!m_solid || !m_dynamic)
    {
        editMass->setEnabled(false);
    }

    editDynamic->connect(editDynamic, SIGNAL(toggled(bool)), editMass, SLOT(setEnabled(bool)));
*/
    widgets["box.size_x"] = editSizeX;
    widgets["box.size_y"] = editSizeY;
    widgets["box.size_z"] = editSizeZ;
    widgets["box.texture"] = editTexture;
    widgets["box.solid"] = editSolid;
    widgets["box.dynamic"] = editDynamic;
    widgets["box.mass"] = editMass;
    return widgets;
}


bool CEntityBox::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    if (!IGraphicEntity::loadFromWidgets(widgets))
    {
        return false;
    }

    // size
    QDoubleSpinBox * editSizeX = qobject_cast<QDoubleSpinBox *>(widgets["box.size_x"]);
    m_size_x = static_cast<float>(editSizeX->value());
    QDoubleSpinBox * editSizeY = qobject_cast<QDoubleSpinBox *>(widgets["box.size_y"]);
    m_size_y = static_cast<float>(editSizeY->value());
    QDoubleSpinBox * editSizeZ = qobject_cast<QDoubleSpinBox *>(widgets["box.size_z"]);
    m_size_z = static_cast<float>(editSizeZ->value());

    // texture
    QLineEdit * editTexture = qobject_cast<QLineEdit *>(widgets["box.texture"]);
    m_texture = editTexture->text();

    // solid
    QCheckBox * editSolid = qobject_cast<QCheckBox *>(widgets["box.solid"]);
    m_solid = editSolid->isChecked();

    // dynamic
    QCheckBox * editDynamic = qobject_cast<QCheckBox *>(widgets["box.dynamic"]);
    m_dynamic = editDynamic->isChecked();

    // mass
    QDoubleSpinBox * editMass = qobject_cast<QDoubleSpinBox *>(widgets["box.mass"]);
    m_mass = static_cast<float>(editMass->value());

    return true;
}


QByteArray CEntityBox::loadFromBuffer(const QByteArray& data)
{
    QByteArray newData = IGraphicEntity::loadFromBuffer(data);

    if (newData.size() < 12 + 1 + 1 + 1 + 4)
    {
        return QByteArray();
    }

    const char * dataPtr = data.data();
    m_size_x = *(reinterpret_cast<const float *>(dataPtr));
    m_size_y = *(reinterpret_cast<const float *>(dataPtr + 4));
    m_size_z = *(reinterpret_cast<const float *>(dataPtr + 8));
    dataPtr += 12;

    // Qt c'est de la merde !
    size_t textureLen = strlen(data.data());
    char * texture = new char[textureLen + 1];
    memset(texture, 0, textureLen + 1);
    strncpy(texture, data.data(), textureLen);
    m_texture = texture;
    delete[] texture;
    dataPtr += textureLen + 1;

    m_solid = *(dataPtr);
    m_dynamic = *(dataPtr + 1);
    m_mass = *(reinterpret_cast<const float *>(dataPtr + 1 + 1));

    return newData.mid(12 + textureLen + 1 + 1 + 1 + 4);
}


QByteArray CEntityBox::writeToBuffer() const
{
    QByteArray data = IGraphicEntity::writeToBuffer();

    data += QByteArray(reinterpret_cast<const char *>(&m_size_x), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_size_y), 4);
    data += QByteArray(reinterpret_cast<const char *>(&m_size_z), 4);
    data += m_texture.toLocal8Bit();
    data += '\0';
    data += (m_solid ? '\1' : '\0');
    data += (m_dynamic ? '\1' : '\0');
    data += QByteArray(reinterpret_cast<const char *>(&m_mass), 4);

    return data;
}


const char * CEntityBox::getType() const
{
    return "box";
}
