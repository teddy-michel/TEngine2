
#pragma once

#include "IEntity.hpp"


class CEntityTrackPoint;


class CEntityTrack : public IEntity
{
public:

    explicit CEntityTrack(CMap * map, const QString& name = QString());
    virtual ~CEntityTrack() override;

    void setPoints(const QList<CEntityTrackPoint *>& points);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget) override;
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets) override;

    virtual const char * getType() const override;

private:

    QList<CEntityTrackPoint *> m_points;
    bool m_loop;
    float m_speed;
};
