
#include "CDialogCreateEntity.hpp"
#include "IEntity.hpp"
#include "CEntityBox.hpp"
#include "CEntityCamera.hpp"
#include "CEntityCameraFreeFly.hpp"
#include "CEntityMesh.hpp"
#include "CEntityPlayerStart.hpp"
#include "CEntityScreen.hpp"
#include "CEntitySphere.hpp"
#include "CEntityTrack.hpp"
#include "CEntityTrackPoint.hpp"
#include "CEntityTrigger.hpp"
#include "CMap.hpp"
#include "ui_DialogCreateEntity.h"


CDialogCreateEntity::CDialogCreateEntity(CMap * map, QWidget * parent) :
    QDialog{ parent },
    m_ui{ new Ui::DialogCreateEntity },
    m_map{ map }
{
    m_ui->setupUi(this);
    m_ui->treeWidget->expandAll();
}


CDialogCreateEntity::~CDialogCreateEntity()
{
    delete m_ui;
}


void CDialogCreateEntity::accept()
{
    auto itemsList = m_ui->treeWidget->selectedItems();
    if (itemsList.size() != 1)
    {
        QDialog::accept();
        return;
    }

    QString entityType = itemsList[0]->text(0);
    QString entityName = m_map->getValidName(entityType);
    IEntity * entity = nullptr;

    if (entityType == "player_start")
    {
        entity = new CEntityPlayerStart{ m_map, entityName };
    }
    else if (entityType == "box")
    {
        entity = new CEntityBox{ m_map, entityName };
    }
    else if (entityType == "mesh")
    {
        entity = new CEntityMesh{ m_map, entityName };
    }
    else if (entityType == "camera")
    {
        entity = new CEntityCamera{ m_map, entityName };
    }
    else if (entityType == "camera_freefly")
    {
        entity = new CEntityCameraFreeFly{ m_map, entityName };
    }
    else if (entityType == "trigger")
    {
        entity = new CEntityTrigger{ m_map, entityName };
    }
    else if (entityType == "screen")
    {
        entity = new CEntityScreen{ m_map, entityName };
    }
    else if (entityType == "sphere")
    {
        entity = new CEntitySphere{ m_map, entityName };
    }
    else if (entityType == "track")
    {
        entity = new CEntityTrack{ m_map, entityName };
    }
    else if (entityType == "track_point")
    {
        entity = new CEntityTrackPoint{ m_map, entityName };
    }
    else if (entityType == "spot_light")
    {
        //entity = new CEntitySpotLight{ m_map, entityName };
    }
    else if (entityType == "point_light")
    {
        //entity = new CEntityPointLight{ m_map, entityName };
    }
    else if (entityType == "directional_light")
    {
        //entity = new CEntityDirectionalLight{ m_map, entityName };
    }
    else if (entityType == "player")
    {
        // TODO: error
    }
    else if (entityType == "map")
    {
        // TODO: error
    }
    else if (entityType == "light")
    {
        //entity = new IEntityLight{ m_map, entityName }; // TODO: remove cause we can't instantiate abstract class
    }
    else
    {
        //entity = new IEntity{ m_map, entityName }; // TODO: remove cause we can't instantiate abstract class
    }

    if (entity != nullptr)
    {
        emit entityCreated(entity);
    }

    QDialog::accept();
}
