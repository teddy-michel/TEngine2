
#pragma once

#include <QMainWindow>
#include <QMap>


class CMap;
class IEntity;
class QTreeWidgetItem;

namespace Ui
{
    class MainWindow;
}


class CMainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit CMainWindow(QWidget * parent = nullptr);
    virtual ~CMainWindow() override;

public Q_SLOTS:

    void newMap();
    bool loadMap();
    bool saveMap();
    bool saveMapAs();
    bool closeMap();
    void createEntity();

protected:

    virtual void closeEvent(QCloseEvent * event) override;

private slots:

    void on_treeEntity_customContextMenuRequested(const QPoint &pos);
    void on_actionEditEntity_triggered();
    void onEntityCreated(IEntity * entity);
    void onEntityChanged(IEntity * entity);

    void on_actionRemoveEntity_triggered();

private:

    void initWindow();

    Ui::MainWindow * m_ui;
    QString m_filename;
    CMap * m_map;
    QMap<IEntity *, QTreeWidgetItem *> m_items;
};
