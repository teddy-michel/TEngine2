
#pragma once

#include "IGraphicEntity.hpp"


class CEntityCamera;


class CEntityScreen : public IGraphicEntity
{
public:

    explicit CEntityScreen(CMap * map, const QString& name = QString());
    virtual ~CEntityScreen() override;

    CEntityCamera * getCamera() const;
    void setCamera(CEntityCamera * camera);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget) override;
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets) override;

    virtual const char * getType() const override;

private:

    CEntityCamera * m_camera;
    bool m_playerCamera;
};
