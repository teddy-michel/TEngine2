
#include <QGroupBox>
#include <QFormLayout>
#include <QComboBox>

#include "CEntityScreen.hpp"
#include "CMap.hpp"
#include "CEntityCamera.hpp"


CEntityScreen::CEntityScreen(CMap * map, const QString& name) :
    IGraphicEntity{ map, name },
    m_camera{ nullptr },
    m_playerCamera{ false }
{

}


CEntityScreen::~CEntityScreen()
{

}


CEntityCamera * CEntityScreen::getCamera() const
{
    return m_camera;
}


void CEntityScreen::setCamera(CEntityCamera * camera)
{
    m_camera = camera;
}


QMap<QString, QWidget *> CEntityScreen::initWidget(QLayout * widget)
{
    QMap<QString, QWidget *> widgets = IGraphicEntity::initWidget(widget);

    QGroupBox * group = new QGroupBox{ "screen" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // camera
    QComboBox * editCamera = new QComboBox{ group };
    editCamera->setObjectName("screen.camera");
    editCamera->addItem("No camera");
    editCamera->addItem("Player camera");
    editCamera->insertSeparator(2);

    for (auto& entity : m_map->getEntities())
    {
        CEntityCamera * camera = qobject_cast<CEntityCamera *>(entity);
        if (camera != nullptr)
        {
            editCamera->addItem(QString("%1 <%2>").arg(camera->getName()).arg(camera->getType()), QVariant::fromValue(camera));
        }
    }
    if (m_playerCamera)
    {
        editCamera->setCurrentIndex(1);
    }
    else if (m_camera != nullptr)
    {
        editCamera->setCurrentIndex(editCamera->findData(QVariant::fromValue(m_camera)));
    }
    layout->addRow(QObject::tr("Camera:"), editCamera);

    widgets["screen.camera"] = editCamera;
    return widgets;
}


bool CEntityScreen::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    if (!IGraphicEntity::loadFromWidgets(widgets))
    {
        return false;
    }

    // camera
    QComboBox * editCamera = qobject_cast<QComboBox *>(widgets["screen.camera"]);
    if (editCamera->currentIndex() == 0)
    {
        m_camera = nullptr;
        m_playerCamera = false;
    }
    else if (editCamera->currentIndex() == 1)
    {
        m_camera = nullptr;
        m_playerCamera = true;
    }
    else
    {
        m_camera = editCamera->currentData().value<CEntityCamera *>();
        m_playerCamera = false;
    }

    return true;
}


const char * CEntityScreen::getType() const
{
    return "screen";
}
