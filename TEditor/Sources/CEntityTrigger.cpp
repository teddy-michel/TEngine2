
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QFormLayout>

#include "CEntityTrigger.hpp"


CEntityTrigger::CEntityTrigger(CMap * map, const QString& name) :
    IEntity{ map, name },
    m_size_x{ 100.0f },
    m_size_y{ 100.0f },
    m_size_z{ 100.0f }
{

}


CEntityTrigger::~CEntityTrigger()
{

}


float CEntityTrigger::getSizeX() const
{
    return m_size_x;
}


float CEntityTrigger::getSizeY() const
{
    return m_size_y;
}


float CEntityTrigger::getSizeZ() const
{
    return m_size_z;
}


void CEntityTrigger::setSize(float x, float y, float z)
{
    m_size_x = (x < 0.0f ? 0.0f : x);
    m_size_y = (y < 0.0f ? 0.0f : y);
    m_size_z = (z < 0.0f ? 0.0f : z);
}


QMap<QString, QWidget *> CEntityTrigger::initWidget(QLayout * widget)
{
    QMap<QString, QWidget *> widgets = IEntity::initWidget(widget);

    QGroupBox * group = new QGroupBox{ "trigger" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // size
    QDoubleSpinBox * editSizeX = new QDoubleSpinBox{ group };
    editSizeX->setObjectName("trigger.size_x");
    editSizeX->setRange(-999999, 999999);
    editSizeX->setDecimals(3);
    editSizeX->setValue(static_cast<float>(m_size_x));

    QDoubleSpinBox * editSizeY = new QDoubleSpinBox{ group };
    editSizeY->setObjectName("trigger.size_y");
    editSizeY->setRange(-999999, 999999);
    editSizeY->setDecimals(3);
    editSizeY->setValue(static_cast<float>(m_size_y));

    QDoubleSpinBox * editSizeZ = new QDoubleSpinBox{ group };
    editSizeZ->setObjectName("trigger.size_z");
    editSizeZ->setRange(-999999, 999999);
    editSizeZ->setDecimals(3);
    editSizeZ->setValue(static_cast<float>(m_size_z));

    QHBoxLayout * layoutSize = new QHBoxLayout{ group };
    layoutSize->addWidget(editSizeX);
    layoutSize->addWidget(editSizeY);
    layoutSize->addWidget(editSizeZ);
    layout->addRow(QObject::tr("Size:"), layoutSize);

    widgets["trigger.size_x"] = editSizeX;
    widgets["trigger.size_y"] = editSizeY;
    widgets["trigger.size_z"] = editSizeZ;
    return widgets;
}


bool CEntityTrigger::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    if (!IEntity::loadFromWidgets(widgets))
    {
        return false;
    }

    // size
    QDoubleSpinBox * editSizeX = qobject_cast<QDoubleSpinBox *>(widgets["trigger.size_x"]);
    m_size_x = static_cast<float>(editSizeX->value());
    QDoubleSpinBox * editSizeY = qobject_cast<QDoubleSpinBox *>(widgets["trigger.size_y"]);
    m_size_y = static_cast<float>(editSizeY->value());
    QDoubleSpinBox * editSizeZ = qobject_cast<QDoubleSpinBox *>(widgets["trigger.size_z"]);
    m_size_z = static_cast<float>(editSizeZ->value());

    return true;
}


const char * CEntityTrigger::getType() const
{
    return "trigger";
}
