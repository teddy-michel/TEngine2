
#pragma once

#include "IEntity.hpp"


class CEntityMap : public IEntity
{
public:

    explicit CEntityMap(CMap * map, const QString& name = QString());
    virtual ~CEntityMap() override;

    virtual const char * getType() const override;
};
