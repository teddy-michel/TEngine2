
#pragma once

#include "IEntity.hpp"


class CEntityTrigger : public IEntity
{
public:

    explicit CEntityTrigger(CMap * map, const QString& name = QString());
    virtual ~CEntityTrigger() override;

    float getSizeX() const;
    float getSizeY() const;
    float getSizeZ() const;
    void setSize(float x, float y, float z);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget) override;
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets) override;

    virtual const char * getType() const override;

private:

    float m_size_x;
    float m_size_y;
    float m_size_z;
};
