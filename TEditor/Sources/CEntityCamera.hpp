
#pragma once

#include "IEntity.hpp"


class CEntityCamera : public IEntity
{
    Q_OBJECT

public:

    explicit CEntityCamera(CMap * map, const QString& name = QString());
    virtual ~CEntityCamera() override;

    virtual const char * getType() const override;
};
