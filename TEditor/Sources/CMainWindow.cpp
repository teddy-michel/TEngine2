/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

#include "CMainWindow.hpp"
#include "ui_MainWindow.h"
#include "CMap.hpp"
#include "CDialogCreateEntity.hpp"
#include "CDialogEditEntity.hpp"
#include "CEntityMap.hpp"


CMainWindow::CMainWindow(QWidget * parent) :
    QMainWindow{ parent },
    m_ui{ new Ui::MainWindow },
    m_filename{},
    m_map{ nullptr }
{
    m_ui->setupUi(this);
    initWindow();
}


CMainWindow::~CMainWindow()
{
    delete m_map;
    delete m_ui;
}


void CMainWindow::newMap()
{
    if (m_map != nullptr)
    {
        if (!closeMap())
        {
            return;
        }
    }

    m_map = new CMap{};
    m_filename.clear();
    initWindow();
}


bool CMainWindow::loadMap()
{
    if (m_map != nullptr)
    {
        if (!closeMap())
        {
            return false;
        }
    }

    m_filename = QFileDialog::getOpenFileName(this, tr("Open map"), "", tr("TEngine Map file (*.tmap);;All Files (*)"));

    if (m_filename.isEmpty())
    {
        return false;
    }

    // Chargement de la map
    m_map = new CMap{};
    if (!m_map->loadFromFile(m_filename))
    {
        QMessageBox::warning(this, tr("Error"), tr("Can't load map %1").arg(m_filename));
        delete m_map;
        m_map = nullptr;
        m_filename.clear();
        return false;
    }

    initWindow();
    return true;
}


bool CMainWindow::saveMap()
{
    if (m_map == nullptr)
    {
        return false;
    }

    if (m_filename.isEmpty())
    {
        return saveMapAs();
    }

    // Sauvegarde de la map
    if (!m_map->saveToFile(m_filename))
    {
        QMessageBox::warning(this, tr("Error"), tr("Can't save map %1").arg(m_filename));
        return false;
    }

    m_ui->actionSave->setEnabled(true);
    return true;
}


bool CMainWindow::saveMapAs()
{
    if (m_map == nullptr)
    {
        return false;
    }

    // Choix du nom de fichier
    m_filename = QFileDialog::getSaveFileName(this, tr("Save map"), "", tr("TEngine Map file (*.tmap);;All Files (*)"));

    if (m_filename.isEmpty())
    {
        return false;
    }

    // Sauvegarde de la map
    if (!m_map->saveToFile(m_filename))
    {
        QMessageBox::warning(this, tr("Error"), tr("Can't save map %1").arg(m_filename));
        return false;
    }

    m_ui->actionSave->setEnabled(true);
    return true;
}


bool CMainWindow::closeMap()
{
    if (m_map == nullptr)
    {
        return true;
    }

    QMessageBox::StandardButton button = QMessageBox::question(this, tr("Save the map"), tr("Do you want to save the map?"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

    if (button == QMessageBox::Yes)
    {
        bool res = saveMap();
        if (res)
        {
            delete m_map;
            m_map = nullptr;
            m_filename.clear();
            initWindow();
        }
        return res;
    }
    else if (button == QMessageBox::No)
    {
        delete m_map;
        m_map = nullptr;
        m_filename.clear();
        initWindow();
        return true;
    }
    else
    {
        return false;
    }
}


void CMainWindow::createEntity()
{
    if (m_map == nullptr)
    {
        return;
    }

    // Boite de dialogue pour créer une entité
    CDialogCreateEntity * dialog = new CDialogCreateEntity{ m_map, this };
    connect(dialog, SIGNAL(entityCreated(IEntity *)), this, SLOT(onEntityCreated(IEntity *)));
    dialog->show();
}


void CMainWindow::closeEvent(QCloseEvent * event)
{
    if (closeMap())
    {
        event->accept();
    }
    else
    {
        event->ignore();
    }
}


void CMainWindow::initWindow()
{
    m_ui->treeEntity->clear();
    m_items.clear();

    if (m_map == nullptr)
    {
        m_ui->treeEntity->setEnabled(false);
        m_ui->viewWidget->setEnabled(false);
        m_ui->buttonCreateEntity->setEnabled(false);

        m_ui->actionSave->setEnabled(false);
        m_ui->actionSaveAs->setEnabled(false);
        m_ui->actionClose->setEnabled(false);
    }
    else
    {
        m_ui->treeEntity->setEnabled(true);
        m_ui->viewWidget->setEnabled(true);
        m_ui->buttonCreateEntity->setEnabled(true);

        m_ui->actionSave->setEnabled(!m_filename.isEmpty());
        m_ui->actionSaveAs->setEnabled(true);
        m_ui->actionClose->setEnabled(true);

        // Création de l'entité racine
        CEntityMap * mapEntity = m_map->getMapEntity();

        QTreeWidgetItem * mapItem = new QTreeWidgetItem{ m_ui->treeEntity };
        mapItem->setFlags(Qt::ItemIsEnabled);
        mapItem->setText(0, mapEntity->getName());
        m_items[mapEntity] = mapItem;

        // Remplissage du tree view avec les entités
        QList<IEntity *> entities = m_map->getEntities();
        for (auto& entity : entities)
        {
            if (mapEntity != entity)
            {
                QTreeWidgetItem * item = new QTreeWidgetItem{ mapItem };
                item->setFlags(Qt::ItemIsEnabled);
                item->setText(0, entity->getName());
                m_items[entity] = item;
            }
        }

        // Création de l'arborescence
        for (auto& entity : entities)
        {
            if (mapEntity != entity)
            {
                IEntity * parent = entity->getParent();
                m_items[parent]->addChild(m_items[entity]);
            }
        }
    }

    m_ui->treeEntity->expandAll();
}


void CMainWindow::on_treeEntity_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);
    menu.addAction(m_ui->actionEditEntity);
    menu.addAction(m_ui->actionRemoveEntity);

    QTreeWidgetItem * item = m_ui->treeEntity->itemAt(pos);
    IEntity * entity = m_items.key(item, nullptr);

    if (entity != nullptr)
    {
        m_ui->actionEditEntity->setData(QVariant::fromValue(entity));
        m_ui->actionRemoveEntity->setData(QVariant::fromValue(entity));
        m_ui->actionRemoveEntity->setEnabled(entity != m_map->getMapEntity());

        menu.exec(m_ui->treeEntity->mapToGlobal(pos));
    }
}


void CMainWindow::onEntityCreated(IEntity * entity)
{
    if (m_map == nullptr || entity == nullptr)
    {
        return;
    }

    m_map->addEntity(entity);

    QTreeWidgetItem * item = new QTreeWidgetItem{};
    item->setFlags(Qt::ItemIsEnabled);
    item->setText(0, entity->getName());
    m_items[entity] = item;

    IEntity * parent = entity->getParent();
    m_items[parent]->addChild(item);
}


void CMainWindow::onEntityChanged(IEntity * entity)
{
    if (m_map == nullptr || entity == nullptr)
    {
        return;
    }

    QTreeWidgetItem * item = m_items[entity];

    if (item == nullptr)
    {
        return;
    }

    // Mise-à-jour de la vue
    item->setText(0, entity->getName());
    IEntity * parent = entity->getParent();
    QTreeWidgetItem * itemCurrentParent = item->parent();
    QTreeWidgetItem * itemParent = nullptr;

    if (parent != nullptr)
    {
        itemParent = m_items[parent];
    }

    if (itemCurrentParent != itemParent)
    {
        if (itemCurrentParent != nullptr)
        {
            itemCurrentParent->removeChild(item);
        }

        if (itemParent != nullptr)
        {
            itemParent->addChild(item);
            itemParent->setExpanded(true);
        }
    }
}


void CMainWindow::on_actionEditEntity_triggered()
{
    if (m_map == nullptr)
    {
        return;
    }

    IEntity * entity = m_ui->actionEditEntity->data().value<IEntity *>();

    if (entity == nullptr)
    {
        return;
    }

    // Boite de dialogue pour modifier une entité
    CDialogEditEntity * dialog = new CDialogEditEntity{ entity, m_map, this };
    connect(dialog, SIGNAL(entityChanged(IEntity *)), this, SLOT(onEntityChanged(IEntity *)));
    dialog->show();
}


void CMainWindow::on_actionRemoveEntity_triggered()
{
    if (m_map == nullptr)
    {
        return;
    }

    IEntity * entity = m_ui->actionEditEntity->data().value<IEntity *>();

    if (entity == nullptr || entity == m_map->getMapEntity())
    {
        return;
    }

    // TODO: demande de confirmation
    // TODO: suppression de l'entité
    // m_items.remove(entity);
    // TODO: mise-à-jour de la vue
}
