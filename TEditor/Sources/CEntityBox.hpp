
#pragma once

#include "IGraphicEntity.hpp"


class CEntityBox : public IGraphicEntity
{
public:

    explicit CEntityBox(CMap * map, const QString& name = QString());
    virtual ~CEntityBox() override;

    float getSizeX() const;
    float getSizeY() const;
    float getSizeZ() const;
    void setSize(float x, float y, float z);
    bool isSolid() const;
    void setSolid(bool solid);
    bool isDynamic() const;
    void setDynamic(bool dynamic);
    float getMass() const;
    void setMass(float mass);
    QString getTexture() const;
    void setTexture(const QString& texture);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget) override;
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets) override;

    virtual QByteArray loadFromBuffer(const QByteArray& data) override;
    virtual QByteArray writeToBuffer() const override;

    virtual const char * getType() const override;

private:

    float m_size_x;
    float m_size_y;
    float m_size_z;
    QString m_texture;
    bool m_solid;
    bool m_dynamic;
    float m_mass;
};
