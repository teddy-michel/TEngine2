
#include <QGroupBox>
#include <QFormLayout>

#include "CEntityTrack.hpp"


CEntityTrack::CEntityTrack(CMap * map, const QString& name) :
    IEntity{ map, name },
    m_loop{ true },
    m_speed{ 100.0f }
{

}


CEntityTrack::~CEntityTrack()
{

}


void CEntityTrack::setPoints(const QList<CEntityTrackPoint *>& points)
{
    m_points = points;
}

QMap<QString, QWidget *> CEntityTrack::initWidget(QLayout * widget)
{
    QMap<QString, QWidget *> widgets = IEntity::initWidget(widget);

    QGroupBox * group = new QGroupBox{ "track" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // TODO

    return widgets;
}


bool CEntityTrack::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    if (!IEntity::loadFromWidgets(widgets))
    {
        return false;
    }

    // TODO

    return true;
}


const char * CEntityTrack::getType() const
{
    return "track";
}
