
#include "CEntityPlayerStart.hpp"


CEntityPlayerStart::CEntityPlayerStart(CMap * map, const QString& name) :
    IEntity{ map, name }
{

}


CEntityPlayerStart::~CEntityPlayerStart()
{

}


const char * CEntityPlayerStart::getType() const
{
    return "player_start";
}
