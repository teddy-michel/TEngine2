
#pragma once

#include <QString>
#include <QLayout>
#include <QMap>


class CMap;


class IEntity : public QObject
{
    Q_OBJECT

public:

    explicit IEntity(CMap * map, const QString& name = QString());
    virtual ~IEntity() = 0;

    QString getName() const;
    void setName(const QString& name);
    IEntity * getParent() const;
    QString getParentName() const { return m_parentName; }
    bool hasAncestor(IEntity * entity) const;
    void setParent(IEntity * parent);
    float getPositionX() const;
    float getPositionY() const;
    float getPositionZ() const;
    void setPosition(float x, float y, float z);
    float getOrientationX() const;
    float getOrientationY() const;
    float getOrientationZ() const;
    void setOrientation(float x, float y, float z);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget);
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets);

    virtual QByteArray loadFromBuffer(const QByteArray& data);
    virtual QByteArray writeToBuffer() const;

    virtual const char * getType() const;

protected:

    CMap * m_map;

private:

    QString m_name;
    IEntity * m_parent;
    QString m_parentName;
    float m_position_x;
    float m_position_y;
    float m_position_z;
    float m_orientation_x;
    float m_orientation_y;
    float m_orientation_z;
    // TODO: add scale
};

Q_DECLARE_METATYPE(IEntity *)
