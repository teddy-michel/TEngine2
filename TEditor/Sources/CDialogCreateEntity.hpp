
#pragma once

#include <QDialog>


class CMap;
class IEntity;

namespace Ui
{
    class DialogCreateEntity;
}


class CDialogCreateEntity : public QDialog
{
    Q_OBJECT

public:

    explicit CDialogCreateEntity(CMap * map, QWidget * parent = nullptr);
    virtual ~CDialogCreateEntity() override;

    virtual void accept() override;

Q_SIGNALS:

    void entityCreated(IEntity * entity);

private:

    Ui::DialogCreateEntity * m_ui;
    CMap * m_map;
};
