
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QFormLayout>

#include "CEntitySphere.hpp"


CEntitySphere::CEntitySphere(CMap * map, const QString& name) :
    IGraphicEntity { map, name },
    m_radius       { 100.0f },
    m_texture      {},
    m_solid        { true },
    m_dynamic      { false },
    m_mass         { 1.0f }
{

}


CEntitySphere::~CEntitySphere()
{

}


float CEntitySphere::getRadius() const
{
    return m_radius;
}


void CEntitySphere::setRadius(float radius)
{
    m_radius = (radius < 0.0f ? 0.0f : radius);
}


bool CEntitySphere::isSolid() const
{
    return m_solid;
}


void CEntitySphere::setSolid(bool solid)
{
    m_solid = solid;
}


bool CEntitySphere::isDynamic() const
{
    return m_dynamic;
}


void CEntitySphere::setDynamic(bool dynamic)
{
    m_dynamic = dynamic;
}


float CEntitySphere::getMass() const
{
    return m_mass;
}


void CEntitySphere::setMass(float mass)
{
    m_mass = mass;
}


QString CEntitySphere::getTexture() const
{
    return m_texture;
}


void CEntitySphere::setTexture(const QString& texture)
{
    m_texture = texture;
}


QMap<QString, QWidget *> CEntitySphere::initWidget(QLayout * widget)
{
    QMap<QString, QWidget *> widgets = IGraphicEntity::initWidget(widget);

    QGroupBox * group = new QGroupBox{ "sphere" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // size
    QDoubleSpinBox * editRadius = new QDoubleSpinBox{ group };
    editRadius->setObjectName("sphere.radius");
    editRadius->setRange(-999999, 999999);
    editRadius->setDecimals(3);
    editRadius->setValue(static_cast<double>(m_radius));

    QHBoxLayout * layoutSize = new QHBoxLayout{ group };
    layoutSize->addWidget(editRadius);
    layout->addRow(QObject::tr("Size:"), layoutSize);

    // texture
    QLineEdit * editTexture = new QLineEdit{ group };
    editTexture->setObjectName("sphere.texture");
    editTexture->setText(m_texture);
    layout->addRow(QObject::tr("Texture:"), editTexture);

    // solid
    QCheckBox * editSolid = new QCheckBox{ QObject::tr("box is solid"), group };
    editSolid->setObjectName("sphere.solid");
    editSolid->setChecked(m_solid);
    layout->addRow(QObject::tr("Solid:"), editSolid);

    // dynamic
    QCheckBox * editDynamic = new QCheckBox{ QObject::tr("box is dynamic"), group };
    editDynamic->setObjectName("sphere.dynamic");
    editDynamic->setChecked(m_dynamic);
    layout->addRow(QObject::tr("Dynamic:"), editDynamic);
/*
    if (!m_solid)
    {
        editDynamic->setEnabled(false);
    }

    editSolid->connect(editSolid, SIGNAL(toggled(bool)), editDynamic, SLOT(setEnabled(bool)));
*/
    // mass
    QDoubleSpinBox * editMass = new QDoubleSpinBox{ group };
    editMass->setObjectName("sphere.mass");
    editMass->setRange(0, 999999);
    editMass->setDecimals(3);
    editMass->setValue(static_cast<double>(m_mass));
    layout->addRow(QObject::tr("Mass:"), editMass);
/*
    if (!m_solid || !m_dynamic)
    {
        editMass->setEnabled(false);
    }

    editDynamic->connect(editDynamic, SIGNAL(toggled(bool)), editMass, SLOT(setEnabled(bool)));
*/
    widgets["sphere.radius"] = editRadius;
    widgets["sphere.texture"] = editTexture;
    widgets["sphere.solid"] = editSolid;
    widgets["sphere.dynamic"] = editDynamic;
    widgets["sphere.mass"] = editMass;
    return widgets;
}


bool CEntitySphere::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    if (!IGraphicEntity::loadFromWidgets(widgets))
    {
        return false;
    }

    // size
    QDoubleSpinBox * editRadius = qobject_cast<QDoubleSpinBox *>(widgets["sphere.radius"]);
    m_radius = static_cast<float>(editRadius->value());

    // texture
    QLineEdit * editTexture = qobject_cast<QLineEdit *>(widgets["sphere.texture"]);
    m_texture = editTexture->text();

    // solid
    QCheckBox * editSolid = qobject_cast<QCheckBox *>(widgets["sphere.solid"]);
    m_solid = editSolid->isChecked();

    // dynamic
    QCheckBox * editDynamic = qobject_cast<QCheckBox *>(widgets["sphere.dynamic"]);
    m_dynamic = editDynamic->isChecked();

    // mass
    QDoubleSpinBox * editMass = qobject_cast<QDoubleSpinBox *>(widgets["sphere.mass"]);
    m_mass = static_cast<float>(editMass->value());

    return true;
}


QByteArray CEntitySphere::loadFromBuffer(const QByteArray& data)
{
    QByteArray newData = IGraphicEntity::loadFromBuffer(data);

    if (newData.size() < 4 + 1 + 1 + 1 + 4)
    {
        return QByteArray();
    }

    const char * dataPtr = data.data();
    m_radius = *(reinterpret_cast<const float *>(dataPtr));
    dataPtr += 4;

    // Qt c'est de la merde !
    size_t textureLen = strlen(data.data());
    char * texture = new char[textureLen + 1];
    memset(texture, 0, textureLen + 1);
    strncpy(texture, data.data(), textureLen);
    m_texture = texture;
    delete[] texture;
    dataPtr += textureLen + 1;

    m_solid = *(dataPtr);
    m_dynamic = *(dataPtr + 1);
    m_mass = *(reinterpret_cast<const float *>(dataPtr + 1 + 1));

    return newData.mid(12 + textureLen + 1 + 1 + 1 + 4);
}


QByteArray CEntitySphere::writeToBuffer() const
{
    QByteArray data = IGraphicEntity::writeToBuffer();

    data += QByteArray(reinterpret_cast<const char *>(&m_radius), 4);
    data += m_texture.toLocal8Bit();
    data += '\0';
    data += (m_solid ? '\1' : '\0');
    data += (m_dynamic ? '\1' : '\0');
    data += QByteArray(reinterpret_cast<const char *>(&m_mass), 4);

    return data;
}


const char * CEntitySphere::getType() const
{
    return "sphere";
}
