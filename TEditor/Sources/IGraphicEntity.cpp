
#include <QGroupBox>
#include <QLineEdit>
#include <QFormLayout>

#include "IGraphicEntity.hpp"


IGraphicEntity::IGraphicEntity(CMap * map, const QString& name) :
    IEntity{ map, name },
    m_vertexShader{},
    m_fragmentShader{}
{

}


IGraphicEntity::~IGraphicEntity()
{

}


QString IGraphicEntity::getVertexShader() const
{
    return m_vertexShader;
}


void IGraphicEntity::setVertexShader(const QString& shader)
{
    m_vertexShader = shader;
}


QString IGraphicEntity::getFragmentShader() const
{
    return m_fragmentShader;
}


void IGraphicEntity::setFragmentShader(const QString& shader)
{
    m_fragmentShader = shader;
}


QMap<QString, QWidget *> IGraphicEntity::initWidget(QLayout * widget)
{
    QMap<QString, QWidget *> widgets = IEntity::initWidget(widget);

    QGroupBox * group = new QGroupBox{ "graphic_entity" };
    widget->addWidget(group);
    QFormLayout * layout = new QFormLayout{ group };

    // shaders
    QLineEdit * editVertexShader = new QLineEdit{ group };
    editVertexShader->setObjectName("graphic_entity.vertex_shader");
    editVertexShader->setText(m_vertexShader);
    layout->addRow(QObject::tr("Vertex shader:"), editVertexShader);

    QLineEdit * editFragmentShader = new QLineEdit{ group };
    editFragmentShader->setObjectName("graphic_entity.fragment_shader");
    editFragmentShader->setText(m_fragmentShader);
    layout->addRow(QObject::tr("Fragment shader:"), editFragmentShader);

    widgets["graphic_entity.vertex_shader"] = editVertexShader;
    widgets["graphic_entity.fragment_shader"] = editFragmentShader;
    return widgets;
}


bool IGraphicEntity::loadFromWidgets(const QMap<QString, QWidget *>& widgets)
{
    if (!IEntity::loadFromWidgets(widgets))
    {
        return false;
    }

    // shaders
    QLineEdit * editVertexShader = qobject_cast<QLineEdit *>(widgets["graphic_entity.vertex_shader"]);
    m_vertexShader = editVertexShader->text();
    QLineEdit * editFragmentShader = qobject_cast<QLineEdit *>(widgets["graphic_entity.fragment_shader"]);
    m_fragmentShader = editFragmentShader->text();

    return true;
}


QByteArray IGraphicEntity::loadFromBuffer(const QByteArray& data)
{
    QByteArray newData = IEntity::loadFromBuffer(data);

    if (newData.size() < 2)
    {
        return QByteArray();
    }

    // Qt c'est de la merde !
    const size_t vertexShaderLen = strlen(data.data());
    char * vertexShader = new char[vertexShaderLen + 1];
    memset(vertexShader, 0, vertexShaderLen + 1);
    strncpy(vertexShader, data.data(), vertexShaderLen);
    m_vertexShader = vertexShader;
    delete[] vertexShader;

    // Qt c'est de la merde !
    const size_t fragmentShaderLen = strlen(data.data());
    char * fragmentShader = new char[fragmentShaderLen + 1];
    memset(fragmentShader, 0, fragmentShaderLen + 1);
    strncpy(fragmentShader, data.data() + vertexShaderLen + 1, fragmentShaderLen);
    m_fragmentShader = fragmentShader;
    delete[] fragmentShader;

    return newData.mid(vertexShaderLen + 1 + fragmentShaderLen + 1);
}


QByteArray IGraphicEntity::writeToBuffer() const
{
    QByteArray data = IEntity::writeToBuffer();
    data += m_vertexShader.toLocal8Bit();
    data += '\0';
    data += m_fragmentShader.toLocal8Bit();
    data += '\0';
    return data;
}


const char * IGraphicEntity::getType() const
{
    return "graphic_entity";
}
