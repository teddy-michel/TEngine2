
#include "CEntityCamera.hpp"


CEntityCamera::CEntityCamera(CMap * map, const QString& name) :
    IEntity{ map, name }
{

}


CEntityCamera::~CEntityCamera()
{

}


const char * CEntityCamera::getType() const
{
    return "camera";
}
