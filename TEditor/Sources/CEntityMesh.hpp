
#pragma once

#include "IGraphicEntity.hpp"


class CEntityMesh : public IGraphicEntity
{
public:

    explicit CEntityMesh(CMap * map, const QString& name = QString());
    virtual ~CEntityMesh() override;

    virtual const char * getType() const override;
};
