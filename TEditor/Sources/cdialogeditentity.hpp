
#pragma once

#include <QDialog>
#include <QMap>


class CMap;
class IEntity;

namespace Ui
{
    class DialogEditEntity;
}


class CDialogEditEntity : public QDialog
{
    Q_OBJECT

public:

    explicit CDialogEditEntity(IEntity * entity, CMap * map, QWidget * parent = nullptr);
    virtual ~CDialogEditEntity() override;

    virtual void accept() override;

Q_SIGNALS:

    void entityChanged(IEntity * entity);

private:

    Ui::DialogEditEntity * m_ui;
    IEntity * m_entity;
    CMap * m_map;
    QMap<QString, QWidget *> m_widgets;
};
