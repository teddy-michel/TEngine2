
#pragma once

#include "CEntityCamera.hpp"


class CEntityCameraFreeFly : public CEntityCamera
{
public:

    explicit CEntityCameraFreeFly(CMap * map, const QString& name = QString());
    virtual ~CEntityCameraFreeFly() override;

    virtual const char * getType() const override;
};
