
#include "CEntityTrackPoint.hpp"


CEntityTrackPoint::CEntityTrackPoint(CMap * map, const QString& name) :
    IEntity{ map, name }
{

}


CEntityTrackPoint::~CEntityTrackPoint()
{

}


const char * CEntityTrackPoint::getType() const
{
    return "track_point";
}
