
#include <QFile>
#include <QVector>

#include "CMap.hpp"
#include "CEntityBox.hpp"
#include "CEntityCamera.hpp"
#include "CEntityCameraFreeFly.hpp"
#include "CEntityMap.hpp"
#include "CEntityMesh.hpp"
#include "CEntityPlayerStart.hpp"
#include "CEntityScreen.hpp"
#include "CEntityTrack.hpp"
#include "CEntityTrackPoint.hpp"
#include "CEntityTrigger.hpp"


CMap::CMap() :
    m_entities{},
    m_mapEntity{ nullptr }
{
    m_mapEntity = new CEntityMap{ this, "map" };
    addEntity(m_mapEntity);
}


CMap::~CMap()
{
    qDeleteAll(m_entities);
}


bool CMap::loadFromFile(const QString& filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        // TODO
        return false;
    }

    THeader header;
    if (file.read(reinterpret_cast<char *>(&header), sizeof(THeader)) != sizeof(THeader))
    {
        // TODO
        return false;
    }

    if (header.magic[0] != 'T' || header.magic[1] != 'M' || header.magic[2] != 'A' || header.magic[3] != 'P')
    {
        // TODO
        return false;
    }

    if (header.numLumps == 0 || header.numLumps > 1000)
    {
        // TODO
        return false;
    }

    QVector<TLumpDescription> lumps;
    lumps.resize(static_cast<int>(header.numLumps));
    if (file.read(reinterpret_cast<char *>(lumps.data()), header.numLumps * sizeof(TLumpDescription)) != header.numLumps * sizeof(TLumpDescription))
    {
        // TODO
        return false;
    }

    {
        IEntity * entity = m_mapEntity;
        m_mapEntity = nullptr;
        removeEntity(entity);
        delete entity;
    }

    for (auto& lump : lumps)
    {
        if (lump.type == LumpEntities)
        {
            for (unsigned int entry = 0; entry < lump.count; ++entry)
            {
                IEntity * entity = nullptr;

                TEntity entityHeader;
                memset(&entityHeader, 0, sizeof(TEntity));

                file.seek(lump.offset + entry * sizeof(TEntity));
                if (file.read(reinterpret_cast<char *>(&entityHeader), sizeof(TEntity)) != sizeof(TEntity))
                {
                    // TODO
                    return false;
                }

                file.seek(entityHeader.offset);
                QByteArray entityData = file.read(entityHeader.size);

                if (strncmp(entityHeader.type, "box", 24) == 0)
                {
                    entity = new CEntityBox{ this };
                }
                else if (strncmp(entityHeader.type, "camera", 24) == 0)
                {
                    entity = new CEntityCamera{ this };
                }
                else if (strncmp(entityHeader.type, "camera_freefly", 24) == 0)
                {
                    entity = new CEntityCameraFreeFly{ this };
                }
                else if (strncmp(entityHeader.type, "map", 24) == 0)
                {
                    entity = new CEntityMap{ this };
                    if (m_mapEntity == nullptr)
                    {
                        m_mapEntity = static_cast<CEntityMap *>(entity);
                    }
                    else
                    {
                        // TODO: warning (une seule entité CMap par map)
                    }
                }
                else if (strncmp(entityHeader.type, "mesh", 24) == 0)
                {
                    entity = new CEntityMesh{ this };
                }
                else if (strncmp(entityHeader.type, "player_start", 24) == 0)
                {
                    entity = new CEntityPlayerStart{ this };
                }
                else if (strncmp(entityHeader.type, "screen", 24) == 0)
                {
                    entity = new CEntityScreen{ this };
                }
                else if (strncmp(entityHeader.type, "track", 24) == 0)
                {
                    entity = new CEntityTrack{ this };
                }
                else if (strncmp(entityHeader.type, "track_point", 24) == 0)
                {
                    entity = new CEntityTrackPoint{ this };
                }
                else if (strncmp(entityHeader.type, "trigger", 24) == 0)
                {
                    entity = new CEntityTrigger{ this };
                }
                else
                {
                    // TODO
                    return false;
                }

                if (entity != nullptr)
                {
                    entity->loadFromBuffer(entityData);
                    addEntity(entity);
                }
            }
            // TODO
        }
        // TODO
    }

    for (auto& entity : m_entities)
    {
        IEntity * parent = getEntityByName(entity->getParentName());
        if (parent != nullptr && parent != entity)
        {
            entity->setParent(parent);
        }
    }

    // TODO

    return true;
}


bool CMap::saveToFile(const QString& filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        // TODO
        return false;
    }

    // Header
    THeader header;
    header.numLumps = 1;

    if (file.write(reinterpret_cast<const char *>(&header), sizeof(THeader)) != sizeof(THeader))
    {
        // TODO
        return false;
    }

    uint32_t offset = sizeof(THeader) + sizeof(TLumpDescription);

    // Lumps header
    TLumpDescription lumpEntities;
    lumpEntities.type = LumpEntities;
    lumpEntities.offset = offset;
    QByteArray lumpEntitiesData = buildLumpEntities(offset, &lumpEntities.count);
    lumpEntities.size = static_cast<unsigned int>(lumpEntitiesData.size());

    if (file.write(reinterpret_cast<const char *>(&lumpEntities), sizeof(TLumpDescription)) != sizeof(TLumpDescription))
    {
        // TODO
        return false;
    }

    if (file.write(lumpEntitiesData) != lumpEntities.size)
    {
        // TODO
        return false;
    }

    // TODO

    return true;
}


QByteArray CMap::buildLumpEntities(uint32_t offset, uint32_t * count) const
{
    assert(count != nullptr);
    *count = static_cast<unsigned int>(m_entities.size());

    QByteArray header;
    QByteArray data;

    offset += *count * sizeof(TEntity); // TODO: lump offset + header

    for (auto& entity : m_entities)
    {
        QByteArray entityData = entity->writeToBuffer();
        data += entityData;

        TEntity entityHeader;
        memset(&entityHeader, 0, sizeof(TEntity));
        entityHeader.offset = offset;
        entityHeader.size = static_cast<unsigned int>(entityData.size());
        strncpy(entityHeader.type, entity->getType(), 24);
        header += QByteArray(reinterpret_cast<const char *>(&entityHeader), sizeof(TEntity));

        offset += static_cast<unsigned int>(entityData.size());
    }

    return header + data;
}


QList<IEntity *> CMap::getEntities() const
{
    return m_entities;
}


IEntity * CMap::getEntityByName(const QString& name) const
{
    for (auto& entity : m_entities)
    {
        if (entity->getName() == name)
        {
            return entity;
        }
    }

    return nullptr;
}


void CMap::addEntity(IEntity * entity)
{
    if (entity == nullptr)
    {
        return;
    }

    if (m_entities.contains(entity))
    {
        return;
    }

    m_entities.append(entity);

    if (entity != m_mapEntity && entity->getParent() == nullptr)
    {
        entity->setParent(m_mapEntity);
    }
}


void CMap::removeEntity(IEntity * entity)
{
    if (entity == nullptr || entity == m_mapEntity)
    {
        return;
    }

    m_entities.removeOne(entity);
}


QString CMap::getValidName(const QString& prefix) const
{
    int num = 1;

    while (true)
    {
        QString name = prefix + QString("_%1").arg(num);
        if (getEntityByName(name) == nullptr)
        {
            return name;
        }
        ++num;
    }
}


CEntityMap * CMap::getMapEntity() const
{
    return m_mapEntity;
}
