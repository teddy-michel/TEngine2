
#pragma once

#include "IEntity.hpp"


class IGraphicEntity : public IEntity
{
public:

    explicit IGraphicEntity(CMap * map, const QString& name = QString());
    virtual ~IGraphicEntity() override = 0;

    QString getVertexShader() const;
    void setVertexShader(const QString& shader);
    QString getFragmentShader() const;
    void setFragmentShader(const QString& shader);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget) override;
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets) override;

    virtual QByteArray loadFromBuffer(const QByteArray& data) override;
    virtual QByteArray writeToBuffer() const override;

    virtual const char * getType() const override;

private:

    QString m_vertexShader;
    QString m_fragmentShader;
};
