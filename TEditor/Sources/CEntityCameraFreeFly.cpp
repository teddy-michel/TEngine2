
#include "CEntityCameraFreeFly.hpp"


CEntityCameraFreeFly::CEntityCameraFreeFly(CMap * map, const QString& name) :
    CEntityCamera{ map, name }
{

}


CEntityCameraFreeFly::~CEntityCameraFreeFly()
{

}


const char * CEntityCameraFreeFly::getType() const
{
    return "camera_freefly";
}
