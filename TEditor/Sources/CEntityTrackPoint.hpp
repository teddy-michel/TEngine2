
#pragma once

#include "IEntity.hpp"


class CEntityTrackPoint : public IEntity
{
public:

    explicit CEntityTrackPoint(CMap * map, const QString& name = QString());
    virtual ~CEntityTrackPoint() override;

    virtual const char * getType() const override;
};
