
#pragma once

#include "IGraphicEntity.hpp"


class CEntitySphere : public IGraphicEntity
{
public:

    explicit CEntitySphere(CMap * map, const QString& name = QString());
    virtual ~CEntitySphere() override;

    float getRadius() const;
    void setRadius(float radius);
    bool isSolid() const;
    void setSolid(bool solid);
    bool isDynamic() const;
    void setDynamic(bool dynamic);
    float getMass() const;
    void setMass(float mass);
    QString getTexture() const;
    void setTexture(const QString& texture);

    virtual QMap<QString, QWidget *> initWidget(QLayout * widget) override;
    virtual bool loadFromWidgets(const QMap<QString, QWidget *>& widgets) override;

    virtual QByteArray loadFromBuffer(const QByteArray& data) override;
    virtual QByteArray writeToBuffer() const override;

    virtual const char * getType() const override;

private:

    float m_radius;
    QString m_texture;
    bool m_solid;
    bool m_dynamic;
    float m_mass;
};
