
#include "CEntityMesh.hpp"


CEntityMesh::CEntityMesh(CMap * map, const QString& name) :
    IGraphicEntity{ map, name }
{

}


CEntityMesh::~CEntityMesh()
{

}


const char * CEntityMesh::getType() const
{
    return "mesh";
}
