
#pragma once

#include "IEntity.hpp"


class CEntityPlayerStart : public IEntity
{
public:

    explicit CEntityPlayerStart(CMap * map, const QString& name = QString());
    virtual ~CEntityPlayerStart() override;

    virtual const char * getType() const override;
};
