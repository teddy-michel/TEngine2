#-------------------------------------------------
#
# Project created by QtCreator 2019-02-22T09:52:15
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = TEditor
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        Sources/CEntityBox.cpp \
        Sources/CEntityMap.cpp \
        Sources/CEntityPlayerStart.cpp \
        Sources/CMainWindow.cpp \
        Sources/CMap.cpp \
        Sources/IEntity.cpp \
        Sources/main.cpp \
        Sources/CDialogCreateEntity.cpp \
        Sources/CDialogEditEntity.cpp \
        Sources/IGraphicEntity.cpp \
        Sources/CEntityCamera.cpp \
        Sources/CEntityCameraFreeFly.cpp \
        Sources/CEntityMesh.cpp \
        Sources/CEntityScreen.cpp \
        Sources/CEntitySphere.cpp \
        Sources/CEntityTrack.cpp \
        Sources/CEntityTrackPoint.cpp \
        Sources/CEntityTrigger.cpp

HEADERS += \
        Sources/CEntityBox.hpp \
        Sources/CEntityMap.hpp \
        Sources/CEntityPlayerStart.hpp \
        Sources/CMainWindow.hpp \
        Sources/CMap.hpp \
        Sources/IEntity.hpp \
        Sources/CDialogCreateEntity.hpp \
        Sources/CDialogEditEntity.hpp \
        Sources/IGraphicEntity.hpp \
        Sources/CEntityCamera.hpp \
        Sources/CEntityCameraFreeFly.hpp \
        Sources/CEntityMesh.hpp \
        Sources/CEntityScreen.hpp \
        Sources/CEntitySphere.hpp \
        Sources/CEntityTrack.hpp \
        Sources/CEntityTrackPoint.hpp \
        Sources/CEntityTrigger.hpp

FORMS += \
        Forms/DialogCreateEntity.ui \
        Forms/DialogEditEntity.ui \
        Forms/MainWindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
