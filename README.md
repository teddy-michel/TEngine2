# TEngine2

TEngine2 est un moteur de jeu vidéo 3D développé à des fins didactiques.

Le code est écrit en C++17.


## Fonctionnalités

* graphe de scène contenant différents types d'entités.
* simulation physique réaliste (merci PhysX !).
* ombres dynamiques.
* joueur à la première personne, avec gestion des collisions.
* système de rendu optimisé : MegaBuffer, bindless texture.
* chargement de maps et de modèles de Half-Life.


## Bibliothèques utilisées

* OpenGL : API graphique.
* glm : bibliothèque utilitaire de OpenGL.
* glew : gestion des extensions de OpenGL.
* FreeType : chargement des polices de caractères.
* SOIL (https://github.com/paralin/soil) : chargement des images.
* SFML (https://www.sfml-dev.org/index-fr.php) : fenêtrage et gestion des évènements.
* PhysX (https://developer.nvidia.com/physx-sdk) : moteur physique.
* Qt : framework graphique utilisé par l'éditeur de maps.
* nlohmann_json (https://github.com/nlohmann/json) : parsing de fichiers JSON.
* libzip (https://libzip.org/) : gestion des fichiers ZIP.
* zlib : gestion des fichiers ZIP.

Dans les prochaines versions :

* ResIL : pour remplacer SOIL qui n'est plus maintenu depuis 7 ans.
* OpenAL ou FMOD pour le son.
* sol2 (https://github.com/ThePhD/sol2) : scripting en Lua.


## Compilation

Le dossiers "Includes" doit contenir les en-têtes des bibliothèques externes :

* "ft2build.h" et "freetype/"
* "json.hpp"
* "SOIL.h"
* "GL/"
* "glm/"
* "PhysX/"
* "SFML/"
* "zip/"

Le dossier "Lib" doit contenir les bibliothèques précompilées.
Deux dossiers "x32" et "x64", eux-mêmes décomposés en deux dossiers "Debug" et "Release", servent à distinguer les différentes versions d'une même bibliothèque.


## Shaders

Plusieurs shaders ont été écrits pour tester différentes fonctionnalités.
À terme, le moteur devrait générer dynamiquement le code des shaders selon les fonctionnalités demandées.

* alpha : deux textures avec coordonnées différentes et une couleur.
* colored : une seule texture et une couleur.
* heightmap : 4 textures avec coordonnées différentes et ratio d'utilisation.
* lightmap_only : une seule texture sur l'unité 2.
* lightmaps : deux textures avec coordonnées différentes.
* lightmaps_bt : deux textures avec coordonnées différentes, bindless texture.
* multitexturing : deux textures (mêmes coordonnées).
* normals : affiche une couleur correspondant au vecteur normal.
* shader2d : utilisé pour l'interface graphique.
* shadowmap : utilisé pour le calcul des shadow maps.
* texturing : une seule texture.
* texturing_bt : une seule texture, bindless texture.
* texturing_shadow : gestion des lumières dynamiques, une seule texture.
* texturing_shadow_bt : gestion des lumières dynamiques, une seule texture, bindless texture.
* wireframe : affichage en mode fil de fer.
