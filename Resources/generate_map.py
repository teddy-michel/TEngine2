
import json


map = {
    "type": "map",
    "name": "Map de test #2",
    "children": [
        {
            "type": "box",
            "name": "Floor",
            "shader": {
                "vertex": "texturing_bt.vert",
                "fragment": "texturing_bt.frag",
                "bindless_texture": True
            },
            "texture": "sol.bmp",
            "solid": True,
            "dynamic": False,
            "size": { "x": 5000, "y": 5000, "z": 5 },
            "position": { "x": 0, "y": 0, "z": -5 }
        },
        {
            "type": "player_start",
            "name": "Player start",
            "position": { "x": 0, "y": 0, "z": 100 }
        }
    ]
}

x = -500
y = 300
z = 0
textures = ["test2.bmp", "sample.png", "sample2.png"]

for i in range(20):
    for j in range(20):
        for k in range(10):
            map["children"].append({
                "type": "box",
                "name": "Box #%02d-%02d-%02d" % (i, j, k),
                "shader": {
                    "vertex": "texturing_bt.vert",
                    "fragment": "texturing_bt.frag",
                    "bindless_texture": True
                },
                "texture": textures[i % len(textures)],
                "solid": True,
                "dynamic": False,
                "mass": 10,
                "size": { "x": 50, "y": 50, "z": 50 },
                "position": { "x": x + 60 * i, "y": y + 60 * j, "z": z + 25 + 60 * k }
            })

with open("entities_multiple.json", "w") as f:
    f.write(json.dumps(map))
