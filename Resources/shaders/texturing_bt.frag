#version 450 core
#extension GL_ARB_bindless_texture : enable

// Input variables
flat in int id_frag;
in vec2 textcoord1_frag;

// Output variables
out vec4 outColor;

// Uniform variables
uniform sampler2D texture1;


layout(shared, binding=0) uniform TextureArray
{
    uvec2 textures[2048];
};


struct Link
{
    uint model;
    uint texture[8];
};

layout(shared, binding=1) buffer LinkArray
{
    Link links[];
};


layout(shared, binding=2) buffer ModelArray
{
    mat4 models[];
};


void main()
{
    if (links[id_frag].texture[0] != 0)
    {
        outColor = texture(sampler2D(textures[links[id_frag].texture[0]]), textcoord1_frag);
    }
    else
    {
        outColor = texture(texture1, textcoord1_frag);
    }
}
