#version 150 core

in vec2 position;
in vec2 textcoord1;
in vec4 color;

out vec2 textcoord1_frag;
out vec4 color_frag;

uniform mat4 model;
uniform mat4 proj;

void main()
{
    textcoord1_frag = textcoord1;
    color_frag = color;
    gl_Position = proj * model * vec4(position, 1.0, 1.0);
}
