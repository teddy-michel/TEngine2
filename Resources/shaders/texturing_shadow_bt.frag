#version 450 core
#extension GL_ARB_bindless_texture : enable

const int shadow_count = 4;

// Input variables
in vec3 position_frag;
flat in int id_frag;
in vec3 normal_frag;
in vec2 textcoord1_frag;
in vec4 light_coords[shadow_count * shadow_count];

// Output variables
out vec4 outColor;

// Uniform variables
uniform sampler2D texture1;
uniform sampler2DShadow shadowmap;


layout(shared, binding=0) uniform TextureArray
{
    uvec2 textures[2048];
};


struct Link
{
    uint model;
    uint texture[8];
};

layout(shared, binding=1) buffer LinkArray
{
    Link links[];
};


layout(shared, binding=2) buffer ModelArray
{
    mat4 models[];
};


struct Light
{
    mat4 matrix;
    vec4 color;  // alpha = light type (see TLightType in CShadowMap.hpp)
    vec4 position; // alpha = intensity
    vec4 attenuation;
};

layout(shared, binding=3) uniform LightArray
{
    Light lights[shadow_count * shadow_count];
};


uint GetLightType(int row, int col)
{
    return floatBitsToUint(lights[row * shadow_count + col].color.a);
}


bool IsLightEnabled(int row, int col)
{
    uint flags = floatBitsToUint(lights[row * shadow_count + col].color.a);
    return ((flags & 0x1000u) == 0x1000u);
}


float ComputeLightVisibility(int row, int col, vec3 proj_coords)
{
    const float bias = 0.000005;
    const vec3 poissonDisk[4] = vec3[](
        vec3( -0.94201624, -0.39906216, 0.0 ),
        vec3( 0.94558609, -0.76890725, 0.0 ),
        vec3( -0.094184101, -0.92938870, 0.0 ),
        vec3( 0.34495938, 0.29387760, 0.0 )
    );

    float visibility = 0.0;
    proj_coords = proj_coords * 0.5 + 0.5; // => [0,1]

    if (proj_coords.x >= 0.0 && proj_coords.x <= 1.0 &&
        proj_coords.y >= 0.0 && proj_coords.y <= 1.0 &&
        proj_coords.z >= 0.0 && proj_coords.z <= 1.0)
    {
        proj_coords.z += bias;

        proj_coords.x += col;
        proj_coords.x /= shadow_count;
        proj_coords.y += row;
        proj_coords.y /= shadow_count;

        for (int i = 0; i < 4; i++)
        {
            visibility += texture(shadowmap, proj_coords + poissonDisk[i] / 700.0);
        }
    }

    return visibility / 4;
}


vec3 computeLight(int row, int col)
{
    vec3 light_color = vec3(lights[row * shadow_count + col].color);
    vec4 shadow_coord = light_coords[row * shadow_count + col];
    uint light_type = GetLightType(row, col);

    if ((light_type & 0x1000u) == 0u) // disabled light
    {
        return vec3(0.0, 0.0, 0.0);
    }

    light_type = (light_type & ~0x1000u);

    vec3 posrel = position_frag - vec3(lights[row * shadow_count + col].position);

    if (light_type == 3u) // x neg
    {
        if (posrel.x > 0 || abs(posrel.x) < abs(posrel.y) || abs(posrel.x) < abs(posrel.z))
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }
    else if (light_type == 4u) // x pos
    {
        if (posrel.x < 0 || abs(posrel.x) < abs(posrel.y) || abs(posrel.x) < abs(posrel.z))
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }
    else if (light_type == 5u) // y neg
    {
        if (posrel.y > 0 || abs(posrel.y) < abs(posrel.x) || abs(posrel.y) < abs(posrel.z))
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }
    else if (light_type == 6u) // y pos
    {
        if (posrel.y < 0 || abs(posrel.y) < abs(posrel.x) || abs(posrel.y) < abs(posrel.z))
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }
    else if (light_type == 7u) // z neg
    {
        if (posrel.z > 0 || abs(posrel.z) < abs(posrel.x) || abs(posrel.z) < abs(posrel.y))
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }
    else if (light_type == 8u) // z pos
    {
        if (posrel.z < 0 || abs(posrel.z) < abs(posrel.x) || abs(posrel.z) < abs(posrel.y))
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }

    if (shadow_coord.w <= 0)
    {
        return vec3(0.0, 0.0, 0.0);
    }

    vec3 proj_coords = shadow_coord.xyz / shadow_coord.w; // => [-1,1]
    float visibility = 0.0;

    if (light_type == 1u)
    {
        // Spot light is a circle
        float spot_angle = proj_coords.x * proj_coords.x + proj_coords.y * proj_coords.y;
        if (spot_angle < 1.0)
        {
            visibility = ComputeLightVisibility(row, col, proj_coords);
            if (spot_angle > lights[row * shadow_count + col].attenuation.w)
            {
                // Gradient between inner and outer angles
                visibility *= (1.0 - spot_angle) / (1.0 - lights[row * shadow_count + col].attenuation.w);
            }
        }
        else
        {
            return vec3(0.0, 0.0, 0.0);
        }
    }
    else
    {
        visibility = ComputeLightVisibility(row, col, proj_coords);
    }

    float intensity = lights[row * shadow_count + col].position.a;

    // Attenuation
    if (light_type == 1u || (light_type >= 3u && light_type <= 8u))
    {
        float distance = length(posrel);
        if (distance > lights[row * shadow_count + col].attenuation.x)
        {
            intensity = 0.0;
        }
        else
        {
            float distanceNorm = distance / lights[row * shadow_count + col].attenuation.x;
            intensity *= (1.0 - distanceNorm);
        }
    }

    return visibility * intensity * light_color;
}


vec3 ComputeLights()
{
    vec3 light_color = vec3(0.0, 0.0, 0.0);
    for (int row = 0; row < shadow_count; ++row)
    {
        for (int col = 0; col < shadow_count; ++col)
        {
            light_color += computeLight(row, col);
        }
    }
    return light_color;
}


void main()
{
    vec3 light_color = ComputeLights();
    vec4 material_diffuse_color;

    if (links[id_frag].texture[0] != 0)
    {
        material_diffuse_color = 0.5 * texture(sampler2D(textures[links[id_frag].texture[0]]), textcoord1_frag); // TODO 0.5 = ambiant parameter
    }
    else
    {
        material_diffuse_color = 0.5 * texture(texture1, textcoord1_frag); // TODO 0.5 = ambiant parameter
    }

    outColor = material_diffuse_color + vec4(light_color, 1.0);
}
