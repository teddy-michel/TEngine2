#version 150 core

in vec2 textcoord1_frag;
in vec2 textcoord2_frag;
in vec2 textcoord3_frag;
in vec2 textcoord4_frag;
in float textratio_frag[4];

out vec4 outColor;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;
uniform sampler2D texture4;

void main()
{
    vec4 t1 = texture(texture1, textcoord1_frag);
    vec4 t2 = texture(texture2, textcoord2_frag);
    vec4 t3 = texture(texture3, textcoord3_frag);
    vec4 t4 = texture(texture4, textcoord4_frag);

    outColor = t1 * textratio[0]_frag + t2 * textratio[1]_frag + t3 * textratio[2]_frag + t4 * textratio[3]_frag;
}
