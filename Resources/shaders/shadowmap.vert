#version 450 core

// Input variables
in vec3 position;
in int id;

// Uniform variables
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;


struct Link
{
    uint model;
    uint texture[8];
};

layout(shared, binding=1) buffer LinkArray
{
    Link links[];
};


layout(shared, binding=2) buffer ModelArray
{
    mat4 models[];
};


void main()
{
    mat4 model_2;

    if (links[id].model == 0)
    {
        model_2 = model;
    }
    else
    {
        model_2 = models[links[id].model];
    }

    gl_Position = proj * view * model_2 * vec4(position, 1.0);
}
