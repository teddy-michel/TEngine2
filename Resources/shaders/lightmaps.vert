#version 150

in vec3 position;
in vec2 textcoord1;
in vec2 textcoord2;

out vec2 textcoord1_frag;
out vec2 textcoord2_frag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    textcoord1_frag = textcoord1;
    textcoord2_frag = textcoord2;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
