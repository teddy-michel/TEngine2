#version 150 core

in vec3 position;
in vec2 textcoord1;
in vec2 textcoord2;
in vec2 textcoord3;
in vec2 textcoord4;
in float textratio[4];

out vec2 textcoord1_frag;
out vec2 textcoord2_frag;
out vec2 textcoord3_frag;
out vec2 textcoord4_frag;
out float textratio_frag[4];

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    textcoord1_frag = textcoord1;
    textcoord2_frag = textcoord2;
    textcoord3_frag = textcoord3;
    textcoord4_frag = textcoord4;

    textratio[0]_frag = textratio[0];
    textratio[1]_frag = textratio[1];
    textratio[2]_frag = textratio[2];
    textratio[3]_frag = textratio[3];

    gl_Position = proj * view * model * vec4(position, 1.0);
}
