#version 450 core

const int shadow_count = 4;

// Input variables
in vec3 position;
in int id;
in vec3 normal;
in vec2 textcoord1;

// Output variables
out vec3 position_frag;
flat out int id_frag;
out vec3 normal_frag;
out vec2 textcoord1_frag;
out vec4 light_coords[shadow_count * shadow_count];

// Uniform variables
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;


layout(shared, binding=0) uniform TextureArray
{
    uvec2 textures[2048];
};


struct Link
{
    uint model;
    uint texture[8];
};

layout(shared, binding=1) buffer LinkArray
{
    Link links[];
};


layout(shared, binding=2) buffer ModelArray
{
    mat4 models[];
};


struct Light
{
    mat4 matrix;
    vec4 color;  // alpha = light type (see TLightType in CShadowMap.hpp)
    vec4 position; // alpha = intensity
    vec4 attenuation;
};

layout(shared, binding=3) uniform LightArray
{
    Light lights[shadow_count * shadow_count];
};


uint GetLightType(int row, int col)
{
    return floatBitsToUint(lights[row * shadow_count + col].color.a);
}


bool IsLightEnabled(int row, int col)
{
    uint flags = floatBitsToUint(lights[row * shadow_count + col].color.a);
    return ((flags & 0x1000u) == 0x1000u);
}


void main()
{
    mat4 model_2;

    if (links[id].model == 0)
    {
        model_2 = model;
    }
    else
    {
        model_2 = models[links[id].model];
    }

    vec4 model_position = model_2 * vec4(position, 1.0);

    gl_Position = proj * view * model_position;

    position_frag = vec3(model_position);
    id_frag = id;
    normal_frag = mat3(model_2) * normal;
    textcoord1_frag = textcoord1;

    for (int row = 0; row < shadow_count; ++row)
    {
        for (int col = 0; col < shadow_count; ++col)
        {
            if (IsLightEnabled(row, col))
            {
                light_coords[row * shadow_count + col] = lights[row * shadow_count + col].matrix * model_position;
            }
        }
    }
}
