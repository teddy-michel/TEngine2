#version 150 core

in vec3 position;
in vec2 textcoord2;

out vec2 textcoord2_frag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    textcoord2_frag = textcoord2;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
