#version 150

in vec2 textcoord1_frag;
in vec2 textcoord2_frag;

out vec4 outColor;

uniform vec4 color;
uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
    outColor = color * mix(texture(texture1, textcoord1_frag), texture(texture2, textcoord2_frag), 0.5);
}
