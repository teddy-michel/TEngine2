#version 330 core

// Input variables
in vec3 position;
in vec3 normal;

// Output variables
out vec3 normal_frag;

// Uniform variables
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;


void main()
{
    normal_frag = mat3(model) * normal;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
