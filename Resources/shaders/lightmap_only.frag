#version 150 core

in vec2 textcoord2_frag;

out vec4 outColor;

uniform sampler2D texture2;

void main()
{
    outColor = texture(texture2, textcoord2_frag);
}
