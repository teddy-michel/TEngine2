#version 150 core

in vec3 position;
in vec2 textcoord1;

out vec2 textcoord1_frag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    textcoord1_frag = textcoord1;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
