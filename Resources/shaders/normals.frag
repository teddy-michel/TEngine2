#version 330 core

// Input variables
in vec3 normal_frag;

// Output variables
out vec4 outColor;


void main()
{
    outColor = vec4(normal_frag * 0.5 + 0.5, 1.0);
}
