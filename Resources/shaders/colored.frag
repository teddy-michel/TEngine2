#version 150 core

in vec2 textcoord1_frag;
in vec4 color_frag;

out vec4 outColor;

uniform sampler2D texture1;

void main()
{
    outColor = color_frag * texture(texture1, textcoord1_frag);
}
