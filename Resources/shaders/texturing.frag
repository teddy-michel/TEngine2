#version 450 core

// Input variables
flat in int id_frag;
in vec2 textcoord1_frag;

// Output variables
out vec4 outColor;

// Uniform variables
uniform sampler2D texture1;


void main()
{
	outColor = texture(texture1, textcoord1_frag);
}
