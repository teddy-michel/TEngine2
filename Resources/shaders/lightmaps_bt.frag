#version 450 core
#extension GL_ARB_bindless_texture : enable

// Input variables
flat in int id_frag;
in vec2 textcoord1_frag;
in vec2 textcoord2_frag;

// Output variables
out vec4 outColor;

// Uniform variables
uniform sampler2D texture1;
uniform sampler2D texture2;


layout(shared, binding=0) uniform TextureArray
{
    uvec2 textures[2048];
};


struct Link
{
    uint model;
    uint texture[8];
};

layout(shared, binding=1) buffer LinkArray
{
    Link links[];
};


layout(shared, binding=2) buffer ModelArray
{
    mat4 models[];
};


void main()
{
    sampler2D t1 = texture1;
    sampler2D t2 = texture2;

    if (links[id_frag].texture[0] != 0)
    {
        t1 = sampler2D(textures[links[id_frag].texture[0]]);
    }

    if (links[id_frag].texture[1] != 0)
    {
        t2 = sampler2D(textures[links[id_frag].texture[1]]);
    }

    outColor = mix(texture(t1, textcoord1_frag), texture(t2, textcoord2_frag), 0.5);
}
