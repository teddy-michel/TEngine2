/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/matrix_transform.hpp>

#include "CActivityMenu.hpp"
#include "CActivityGame.hpp"

#include "Game/CMap.hpp"
//#include "Core/Events.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CGuiLabel.hpp"
#include "Entities/CMapEntity.hpp"
#include "Entities/CCamera.hpp"


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

CActivityMenu::CActivityMenu() :
    IActivity      {},
    m_activityGame { nullptr },
    m_displayMap   { true },
    m_gui          {},
    m_labelFPS     { std::make_shared<CGuiLabel>() }
{
    auto bg = std::make_shared<CGuiLabel>();
    bg->setSize(800, 600); // TODO: get window size
    bg->setBackgroundColor(CColor{ 0, 0, 0, 150 });
    m_gui.addObject(bg);

    auto title = std::make_shared<CGuiLabel>("TEngine2");
    title->setPosition(120, 100);
    title->setSize(500, 60);
    title->setFontSize(32);
    title->setFont(gFontManager->getFontId("arial"));
    title->setColor(CColor::Yellow);
    m_gui.addObject(title);

    auto rect1 = std::make_shared<CGuiLabel>();
    rect1->setPosition(100, 200);
    rect1->setSize(300, 50);
    rect1->setBackgroundColor(CColor{200, 0, 0, 200});
    m_gui.addObject(rect1);

    auto rect2 = std::make_shared<CGuiLabel>();
    rect2->setPosition(100, 275);
    rect2->setSize(300, 50);
    rect2->setBackgroundColor(CColor{ 200, 0, 0, 200 });
    m_gui.addObject(rect2);

    auto rect3 = std::make_shared<CGuiLabel>();
    rect3->setPosition(100, 350);
    rect3->setSize(300, 50);
    rect3->setBackgroundColor(CColor{ 200, 0, 0, 200 });
    m_gui.addObject(rect3);

    m_labelFPS->setColor(CColor::Orange);
    m_labelFPS->setFont(gFontManager->getFontId("arial"));
    m_labelFPS->setPosition(30, 30);
    m_labelFPS->setSize(150, 40);
    m_labelFPS->setFontSize(24);
    m_gui.addObject(m_labelFPS);

}


/**
 * M�thode appell�e � chaque frame.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CActivityMenu::frame(float frameTime)
{
    m_labelFPS->setText(CString("FPS: %1").arg(gGameApplication->getFPS()));
    m_gui.update(frameTime);

    gRenderer->startFrame();

    // Affichage de la map
    if (m_displayMap)
    {
        CMap * map = m_activityGame->getMap();
        auto camera = m_activityGame->getCamera();

        if (map != nullptr && camera != nullptr)
        {
            map->renderMap(camera);
        }
    }

    // Affichage de l'interface utilisateur
    m_gui.draw();

    gRenderer->endFrame();
}


void CActivityMenu::onEvent(const CKeyboardEvent& event)
{
    if (event.type == KeyPressed)
    {
        if (event.code == TKey::Key_Escape)
        {
            gGameApplication->setActivity(m_activityGame);
        }
    }
}

} // Namespace TE
