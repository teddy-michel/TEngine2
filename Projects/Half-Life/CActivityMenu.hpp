/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Game/IActivity.hpp"
#include "Graphic/CGuiManager.hpp"


namespace TE
{

class CActivityGame;
class CGuiLabel;


/**
 * \class   CActivityMenu
 * \ingroup Game
 * \brief   Activit� pour g�rer le menu principal.
 ******************************/

class CActivityMenu : public IActivity
{
public:

    CActivityMenu();
    virtual ~CActivityMenu() = default;

    virtual void frame(float frameTime) override;

    virtual void onEvent(const CKeyboardEvent& event) override;

    void setActivityGame(CActivityGame * activity)
    {
        m_activityGame = activity;
    }

private:

    CActivityGame * m_activityGame;
    bool m_displayMap;
    CGuiManager m_gui;
    std::shared_ptr<CGuiLabel> m_labelFPS;
};

} // Namespace TE
