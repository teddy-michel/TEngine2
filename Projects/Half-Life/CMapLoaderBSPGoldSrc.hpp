/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <map>
#include <cstdint>
#include <memory>
#include <fstream>
#include <glm/glm.hpp>

#include "Game/IMapLoader.hpp"
#include "Core/CFlags.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CTextureManager.hpp"


namespace TE
{

class CShapeTriMesh;
class CMapEntity;
struct TVertex3D;


struct TModel
{
    std::shared_ptr<CBuffer<TVertex3D>> buffer;
    std::shared_ptr<CShapeTriMesh> trimesh;
    mutable bool used;

    explicit TModel(const std::shared_ptr<CBuffer<TVertex3D>>& buffer = nullptr, const std::shared_ptr<CShapeTriMesh>& trimesh = nullptr, bool used = false) :
        buffer  { buffer },
        trimesh { trimesh },
        used    { used }
    { }
};


/**
 * \class   CMapLoaderBSPGoldSrc
 * \ingroup Game
 * \brief   Chargement d'une map BSP du moteur GoldSrc.
 ******************************/

class CMapLoaderBSPGoldSrc : public IMapLoader
{
public:

    // Constructeur et destructeur
    CMapLoaderBSPGoldSrc();
    virtual ~CMapLoaderBSPGoldSrc();

    virtual CMap * loadFromFile(const CString& filename) override;

    // Méthodes publiques
    //virtual bool loadData(const std::string& fileName);
    void unloadData();
    //virtual IMap * clone();
    //void addLightmap(unsigned short face, unsigned short unit);
    //void removeLightmap(unsigned short face, unsigned short unit);

private:

    std::ifstream m_file; ///< Fichier contenant la map à charger.
    CMapEntity * m_map;   ///< Pointeur sur la map en cours de chargement.

    // Constantes pour les valeurs maximales
    static const unsigned int MaxModels       = 256;    ///< Nombre maximal de modèles dans une map.
    static const unsigned int MaxBrushes      = 4096;   ///< Nombre maximal de volumes dans une map.
    static const unsigned int MaxEntities     = 1024;   ///< Nombre maximal d'entités dans une map.
    static const unsigned int MaxEntitiesSize = 65536;  ///< Longueur maximal du lump contenant les entités.
    static const unsigned int MaxPlanes       = 32767;  ///< Nombre maximal de plans dans une map.
    static const unsigned int MaxNodes        = 32767;  ///< Nombre maximal de noeuds dans un arbre BSP.
    static const unsigned int MaxClipNodes    = 32767;
    static const unsigned int MaxLeafs        = 32767;  ///< Nombre maximal de feuilles dans un arbre BSP.
    static const unsigned int MaxVertices     = 65535;  ///< Nombre maximal de sommets par map.
    static const unsigned int MaxFaces        = 65535;  ///< Nombre maximal de faces par map.
    static const unsigned int MaxMarkSurfaces = 65535;
    static const unsigned int MaxTextInfo     = 4096;   ///< Nombre maximal de textures.
    static const unsigned int MaxEdges        = 256000; ///< Nombre maximal d'arrêtes par map.
    static const unsigned int MaxSurfEdges    = 512000; ///< Nombre maximal d'arrêtes utilisées par les surfaces.
    static const unsigned int MaxTextures     = 512;    ///< Nombre maximal de textures.
    static const unsigned int MaxMipTex       = 0x200000;
    static const unsigned int MaxLighting     = 0x100000;
    static const unsigned int MaxVisibility   = 0x100000;
    static const unsigned int MaxPortals      = 65536;  ///< Nombre maximal de portails par map.
    static const unsigned int NumLightmaps    = 4;      ///< Nombre de lightmaps par surface.
    static const unsigned int NumHulls        = 4;      ///< Nombre d'enveloppes (arbres BSP) pour chaque modèle.
    static const unsigned int NumAmbients     = 4;

    // Numéros des lumps
    static const unsigned int LumpEntities      =  0;
    static const unsigned int LumpPlanes        =  1;
    static const unsigned int LumpTextures      =  2;
    static const unsigned int LumpVertexes      =  3;
    static const unsigned int LumpVisibility    =  4;
    static const unsigned int LumpNodes         =  5;
    static const unsigned int LumpTexInfo       =  6;
    static const unsigned int LumpFaces         =  7;
    static const unsigned int LumpLighting      =  8;
    static const unsigned int LumpClipNodes     =  9;
    static const unsigned int LumpLeafs         = 10;
    static const unsigned int LumpMarkSurfaces  = 11;
    static const unsigned int LumpEdges         = 12;
    static const unsigned int LumpSurfEdges     = 13;
    static const unsigned int LumpModels        = 14;

    /*-------------------------------*
     *   Énumérations                *
     *-------------------------------*/

    /// Types de contenu pour les feuilles de l'arbre BSP.
    enum TContentType
    {
        ContentEmpty        = -1,  ///< Feuille vide.
        ContentSolid        = -2,  ///< Feuille solide.
        ContentWater        = -3,  ///< Eau.
        ContentSlime        = -4,  ///< Vase.
        ContentLava         = -5,  ///< Lave.
        ContentSky          = -6,  ///< Ciel.
        ContentOrigin       = -7,  ///< Sert à indique l'axe d'une rotation.
        ContentClip         = -8,  ///< Bloque le joueur.
        ContentCurrent0     = -9,
        ContentCurrent90    = -10,
        ContentCurrent180   = -11,
        ContentCurrent270   = -12,
        ContentCurrentUp    = -13,
        ContentCurrentDown  = -14,
        ContentTransluscent = -15  ///< Feuille transparente.
    };

    /// Ambient.
    enum TAmbientType
    {
        AmbientWater  = 0,  ///< Eau.
        AmbientSky    = 1,  ///< Ciel.
        AmbientSlime  = 2,  ///< Vase.
        AmbientLava   = 3   ///< Lave.
    };

    /// Types de dégat.
    enum TDamageType
    {
        DamageGeneric      = 0x000000, ///< Dégât générique.
        DamageCrush        = 0x000001,
        DamageBullet       = 0x000002, ///< Dégât causé par une balle.
        DamageSlash        = 0x000004,
        DamageBurn         = 0x000008, ///< Dégât causé par le feu.
        DamageFreeze       = 0x000010, ///< Dégât causé par le froid.
        DamageFall         = 0x000020, ///< Dégât causé par une chute.
        DamageBlast        = 0x000040,
        DamageClub         = 0x000080,
        DamageShock        = 0x000100,
        DamageSonic        = 0x000200,
        DamageEnergyBeam   = 0x000400,
        DamageDrown        = 0x004000,
        DamageParalyse     = 0x008000,
        DamageNervegas     = 0x010000,
        DamagePoison       = 0x020000, ///< Dégât causé par du poison.
        DamageRadiation    = 0x040000, ///< Dégât causé par des radiations.
        DamageDrownRecover = 0x080000,
        DamageChemical     = 0x100000, ///< Dégât causé par des produits chimiques.
        DamageSlowBurn     = 0x200000,
        DamageSlowFreeze   = 0x400000
    };

    T_DECLARE_FLAGS(TDamageTypes, TDamageType)
    T_DECLARE_OPERATORS_FOR_PRIVATE_FLAGS(TDamageTypes)


    /*-------------------------------*
     *   Structures des fichiers BSP *
     *-------------------------------*/

//#include "Core/struct_alignment_start.h"
#pragma pack(push,1)

    /// Structure d'un lump.
    struct TLump
    {
        uint32_t fileofs; ///< Offset du lump (en octets).
        uint32_t filelen; ///< Longueur du lump (en octets).
    };

    /// Structure d'un plan.
    struct TBSPPlane
    {
        glm::vec3 normale; ///< Vecteur normal.
        float distance;    ///< Distance par rapport à l'origine.
        int32_t type;      ///< Plane axis identifier.
    };

    /// Structure d'un noeud de l'arbre BSP.
    struct TBSPNode
    {
        uint32_t planenum;   ///< Identifiant du plan.
        int16_t children[2]; ///< Noeuds enfants.
        int16_t mins[3];     ///< Point A du volume englobant.
        int16_t maxs[3];     ///< Point B du volume englobant.
        uint16_t firstface;  ///< Première face.
        uint16_t numfaces;   ///< Nombre de faces.
    };

    /// Structure d'une texture.
    struct TBSPTexinfo
    {
        float textureVecs[2][4]; ///< [s/t][xyz offset].
        int32_t texture_id;      ///< Pointer to texture name.
        int32_t flags;           ///< Flags (0 ou 1 pour une texture spéciale).
    };

    /// Structure d'une face.
    struct TBSPFace
    {
        uint16_t planenum;            ///< Index of the plane the face is parallel to.
        uint16_t side;                ///< Set if the normal is parallel to the plane normal
        uint32_t firstedge;           ///< Index of the first edge (in the face edge array).
        uint16_t numedges;            ///< Number of consecutive edges (in the face edge array).
        uint16_t texture_info;        ///< Index of the texture info structure.
        int8_t lightmap_styles[NumLightmaps]; ///< Styles (bit flags) for the lightmaps.
        int32_t lightmap_offset;              ///< Offset of the lightmap (in bytes) in the lightmap lump.
    };

    /// Structure d'un modèle.
    struct TBSPModel
    {
        glm::vec3 mins;             ///< Point A du volume englobant.
        glm::vec3 maxs;             ///< Point B du volume englobant.
        glm::vec3 origin;           ///< Origine du modèle.
        int32_t headnode[NumHulls]; ///< Arbres BSP du modèle (un pour chaque boite AABB prédéfinie).
        int32_t visleafs;           ///< Visibilité.
        uint32_t firstface;         ///< Index de la première face.
        uint32_t numfaces;          ///< Nombre de faces.
    };

    /// Structure d'une feuille de l'arbre BSP.
    struct TBSPLeaf
    {
       int32_t contents;                 ///< Volume.
       int32_t visofs;                   ///< Visibilité.
       int16_t mins[3];                  ///< Point A du volume englobant.
       int16_t maxs[3];                  ///< Point B du volume englobant.
       uint16_t firstmarksurface;        ///< Numéro de la première surface de la feuille.
       uint16_t nummarksurfaces;         ///< Nombre de surfaces de la feuille.
       uint8_t ambient_level[NumAmbients];
    };

    struct TBSPEdge
    {
        uint16_t v0;
        uint16_t v1;
    };

//#include "Core/struct_alignment_end.h"
#pragma pack(pop)

    // Méthodes privées
    void loadEntities();
    //void loadPlanes();
    void loadTextures();
    void loadVertices();
    void loadNodes();
    void loadLeafs();
    void loadModels();
    //void loadGeometry();
    //void loadGeometryCheckNode(const CBSPTreeNode * const bsp_node, const std::list<CPlane>& plans);
    //void loadGeometryCreateVolume(const std::list<CPlane>& plans);
    //CBSPTreeNode * LoadBSPTreeNode(const unsigned int node);
    //CBSPTreeLeaf * LoadBSPTreeLeaf(const unsigned int leaf);

    //static unsigned int ConvertStringNumModel(const char * str);

protected:

    /// Informations sur une texture.
    struct TTextureInfos
    {
        unsigned int width;  ///< Largeur de l'image en pixels.
        unsigned int height; ///< Hauteur de l'image en pixels.
        TTextureId id;       ///< Identifiant de la texture.
    };

    typedef std::vector<TTextureInfos> TTextureInfosVector;

private:

    // Données privées
    TTextureId m_lightmaps[NumLightmaps]; ///< Identifiant de la texture contenant la lightmap.
    TLump m_lumps[15];                 ///< Offset et tailles des lumps.
    TTextureInfosVector m_textinfos;   ///< Tableau contenant toutes les textures utilisées.
    //TBufferTextureVector m_faces;      ///< Tableau contenant les textures utilisées pour chaque face.
    //std::vector<CPlane> m_planes;      ///< Lump 01 : liste des plans.
    std::vector<glm::vec3> m_vertices; ///< Lump 03 : liste des vertices.
    std::vector<TBSPNode> m_nodes;     ///< Lump 05 : liste des noeuds de l'arbre BSP.
    std::vector<TBSPLeaf> m_leafs;     ///< Lump 10 : liste des leafs.
    std::vector<TModel> m_models;
};

T_DECLARE_OPERATORS_FOR_FLAGS(CMapLoaderBSPGoldSrc::TDamageTypes)

} // Namespace TE
