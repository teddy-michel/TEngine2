/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "Game/IModelData.hpp"
#include "Core/CFlags.hpp"


namespace TE
{

/**
 * \enum    TSequenceActivity
 * \ingroup Game
 * \brief   Actions des séquences des fichiers MDL du moteur GoldSource.
 *
 * \todo Vérifier les valeurs et compléter la liste.
 ******************************/

enum TSequenceActivity
{
    ActIdle           =  1, ///< Default behavior when nothing else is going on (loop).
    ActWalk           =  3, ///< Walk (loop).
    ActRun            =  4, ///< Run (loop).
    ActFly            =  5, ///< Fly (lx loop).
    ActSwim           =  6, ///< Swim (loop).
    ActSmallFlinch    = 26, ///< Small reaction to non-specific hit.
    ActBigFlinch      = 27, ///< Large reaction to non-specific hit.
    ActEat            = 35, ///< Monster chewing on a large food item (loop).
    ActDieSimple      = 36, ///< Death animation.
    ActDieBackward    = 37, ///< Die falling backwards.
    ActDieForward     = 38, ///< Die falling forwards.
    ActInspectFloor   = 45, ///< For active idles, look at something on or near the floor.
    ActInspectWall    = 46, ///< For active idles, look at something directly ahead of you (doesn't have to be a wall or on a wall).
    ActDieHeadshot    = 66, ///< Die hit in head.
    ActDieGutshot     = 68, ///< Die hit in gut.
    ActFlinchLeftarm  = 73, ///< Flinch from left arm hit.
    ActFlinchLeftleg  = 75, ///< Flinch from left leg hit.
    ActFlinchRightarm = 74, ///< Flinch from right arm hit.
    ActFlinchRightleg = 76, ///< Flinch from right leg hit.

    ActArm,                ///< Activate weapon (e.g. draw gun).
    ActBarnacleChew,       ///< Barnacle is holding the monster in its mouth (loop).
    ActBarnacleChomp,      ///< Barnacle latches on to the monster.
    ActBarnacleHit,        ///< Barnacle tongue hits a monster.
    ActBarnaclePull,       ///< Barnacle is lifting the monster (loop).
    ActBite,               ///< This plays one time eat loops for large monsters which can eat small things in one bite. (30 ou 31)
    ActCombatIdle,         ///< Agitated idle, played when monster expects to fight.
    ActCower,              ///< Display a fear behavior.
    ActCrouch,             ///< The act of crouching down from a standing position. (18 ou 19)
    ActCrouchIdle,         ///< Hold body in crouched position (loop). (18 ou 19)
    ActDetectScent,        ///< This means the monster smells a scent carried by the air.
    ActDieBackshot,        ///< Die hit in back.
    ActDieChestshot,       ///< Die hit in chest.
    ActDieViolent,         ///< Exaggerated death animation.
    ActDisarm,             ///< Put away weapon (e.g. re-holster gun).
    ActExcited,            ///< For some reason monster is excited. Sees something he really likes to eat or whatever.
    ActFall,               ///< Looping animation for falling monster.
    ActFearDisplay,        ///< Monster just saw something that it is afraid of.
    ActFlinchChest,        ///< Flinch from chest hit. (26, 27)
    ActFlinchHead,         ///< Flinch from head hit. (26, 27)
    ActFlinchStomach,      ///< Flinch from stomach hit. (26, 27)
    ActFlyLeft,            ///< Turn left in flight.
    ActFlyRight,           ///< Turn right in flight.
    ActGlide,              ///< Fly without wing movement (lx loop).
    ActGuard,              ///< Defend an area.
    ActHop,                ///< Vertical jump.
    ActHover,              ///< Idle while in flight (loop).
    ActIdleAngry,          ///< Alternate idle animation in which the monster is clearly agitated. (loop).
    ActLand,               ///< End of a jump.
    ActLeap,               ///< Long forward jump.
    ActMeleeAttack1,       ///< Attack at close range.
    ActMeleeAttack2,       ///< Alternate close range attack.
    ActRangeAttack1,       ///< Attack with ranged weapon.
    ActRangeAttack2,       ///< Alternate ranged attack.
    ActReload,             ///< Reload weapon.
    ActRollLeft,           ///< Tuck and roll left. (15 ?)
    ActRollRight,          ///< Tuck and roll right. (16 ?)
    ActRunHurt,            ///< Limp (loop).
    ActRunScared,          ///< Run displaying fear (loop).
    ActSignal1,            ///< Signal.
    ActSignal2,            ///< Alternate signal.
    ActSignal3,            ///< Alternate signal.
    ActSleep,              ///< Sleep (loop).
    ActSniff,              ///< This is the act of actually sniffing an item in front of the monster.
    ActSpecialAttack1,     ///< Monster specific special attack.
    ActSpecialAttack2,     ///< Monster specific special attack.
    ActStand,              ///< The act of standing from a crouched position. (18 ?)
    ActStrafeLeft,         ///< Sidestep left while maintaining facing (loop). (15 ?)
    ActStrafeRight,        ///< Sidestep right while maintaining facing (loop). (16 ?)
    ActThreatDisplay,      ///< Without attacking monster demonstrates that it is angry.
    ActTurnLeft,           ///< Turn in place quickly left. (15 ?)
    ActTurnRight,          ///< Turn in place quickly right. (16 ?)
    ActTwitch,             ///< Twitch.
    ActUse,                ///< Use item.
    ActVictoryDance,       ///< Victory display after killing player.
    ActWalkHurt,           ///< Limp or wounded walk (loop).
    ActWalkScared          ///< Walk with fear display (loop).
};


/**
 * \class   CModelDataMDLGoldSrc
 * \ingroup Game
 * \brief   Chargement des fichiers MDL du moteur GoldSource.
 *
 * Le format MDL a été crée par idSoftware pour le jeu Quake en 1996.
 * Il a ensuite été adapté par Valve Corporation pour Half-Life et le moteur
 * GoldSource, puis pour le Source Engine.
 *
 * \todo Utiliser toutes les fonctionnalités (attachments, transitions, pivots, events, etc.).
 ******************************/

class CModelDataMDLGoldSrc : public IModelData
{
public:

    // Constructeur et destructeur
    CModelDataMDLGoldSrc();
    virtual ~CModelDataMDLGoldSrc();

    // Méthodes publiques
    virtual unsigned int getMemorySize() const override;
    virtual unsigned int getNumSequences() const override;
    virtual unsigned int getNumSkins() const override;
    virtual unsigned int getNumGroups() const override;
    unsigned int getNumHitBoxes() const;
    glm::vec3 getEyesPosition() const;
    //virtual CBoundingBox getBoundingBox(unsigned int sequence = 0) const override;
    bool isSequenceLoop(unsigned int sequence) const;
    unsigned int getNumFrames(unsigned int sequence) const;
    float getFrameRate(unsigned int sequence) const;
    glm::vec3 getLinearMovement(unsigned int sequence) const;
    CString getSequenceName(unsigned int sequence) const;
    virtual unsigned int getSequenceNumber(const CString& name) const override;
    unsigned int getSequenceByActivity(TSequenceActivity activity) const;
    unsigned int getAttachmentByName(const CString& name) const;
    glm::vec3 getAttachmentPosition(unsigned int attachment) const;
    void getControllerRange(unsigned int index, float * start, float * end) const;

    virtual void updateParams(CModel::TModelParams& params, float time) const override;
    virtual void updateBuffer(CBuffer<TVertex3D> * buffer, CModel::TModelParams& params) override;
    virtual bool loadFromFile(const CString& fileName) override;
    bool saveToFile(const CString& fileName) const;

    // Méthodes statiques publiques
    static bool isCorrectFormat(const CString& fileName);

#ifdef T_NO_COVARIANT_RETURN
    static Ted::IModelData * createInstance(const CString& fileName);
#else
    static CModelDataMDLGoldSrc * createInstance(const CString& fileName);
#endif

    // Nombre maximal de données
    static const unsigned int MaxTriangles   = 20000; ///< Nombre maximal de triangles.
    static const unsigned int MaxVertices    = 2048;  ///< Nombre maximal de sommets.
    static const unsigned int MaxBones       = 128;   ///< Nombre maximal d'os utilisé.
    static const unsigned int MaxSequences   = 256;   ///< Nombre maximal de séquences.
    static const unsigned int MaxSkins       = 100;   ///< Nombre maximal de skins.
    static const unsigned int MaxSrcBones    = 512;   ///< Nombre maximal d'os autorisé.
    static const unsigned int MaxModels      = 32;    ///< Nombre maximal de sous-modèles.
    static const unsigned int MaxBodyParts   = 32;    ///< Nombre maximal de sous-modèles.
    static const unsigned int MaxGroups      = 4;     ///< Nombre maximal de groupes
    static const unsigned int MaxAnimations  = 512;   ///< Nombre maximal d'animations par séquence.
    static const unsigned int MaxMeshes      = 256;   ///< Nombre maximal de volumes (par sous-modèle ?).
    static const unsigned int MaxEvents      = 1024;  ///< Nombre maximal d'évènements.
    static const unsigned int MaxPivots      = 256;   ///< Nombre maximal de pivots.
    static const unsigned int MaxControllers = 8;     ///< Nombre maximal de contrôleurs.

private:

    /**
     * \enum    TLightType
     * \ingroup Game
     * \brief   Types d'éclairage d'un modèle.
     ******************************/

    enum TLightType
    {
        LightFlatShade  = 0x0001,
        LightChrome     = 0x0002,
        LightFullBright = 0x0004
    };

    T_DECLARE_FLAGS(TLightTypes, TLightType)
    T_DECLARE_OPERATORS_FOR_PRIVATE_FLAGS(TLightTypes)


    /**
     * \enum    TTransitionType
     * \ingroup Game
     * \brief   Types de transition entre les séquences.
     ******************************/

    enum TTransitionType
    {
        TransitionX     = 0x0001,
        TransitionY     = 0x0002,
        TransitionZ     = 0x0004,
        TransitionXR    = 0x0008,
        TransitionYR    = 0x0010,
        TransitionZR    = 0x0020,
        TransitionLX    = 0x0040,
        TransitionLY    = 0x0080,
        TransitionLZ    = 0x0100,
        TransitionAX    = 0x0200,
        TransitionAY    = 0x0400,
        TransitionAZ    = 0x0800,
        TransitionAXR   = 0x1000,
        TransitionAYR   = 0x2000,
        TransitionAZR   = 0x4000,
        TransitionTypes = 0x7FFF,
        TransitionRLoop = 0x8000
    };

    T_DECLARE_FLAGS(TTransitionTypes, TTransitionType)
    T_DECLARE_OPERATORS_FOR_PRIVATE_FLAGS(TTransitionTypes)


/*-------------------------------*
 *   Structures internes         *
 *-------------------------------*/

#include "Core/struct_alignment_start.h"

    /// En-tête du fichier.
    struct THeader
    {
        int32_t ident;                  ///< Identifiant (IDST ou IDSQ).
        int32_t version;                ///< Version du fichier MDL (10).
        char name[64];                  ///< Nom du modèle.
        uint32_t length;                ///< Longueur du fichier.
        glm::vec3 eyeposition;          ///< Position des yeux.
        glm::vec3 min;                  ///< Ideal movement hull size.
        glm::vec3 max;                  ///< Ideal movement hull size.
        glm::vec3 bbmin;                ///< Clipping bounding box.
        glm::vec3 bbmax;                ///< Clipping bounding box.
        int32_t flags;                  ///< Flags.
        uint32_t num_bones;             ///< Nombre d'os.
        uint32_t bone_index;            ///< Index des os.
        uint32_t num_bone_controllers;  ///< Bone controllers.
        uint32_t bone_controller_index; ///< Index des bone controllers.
        uint32_t num_hitboxes;          ///< Complex bounding boxes.
        uint32_t hitbox_index;          ///< Index des hitboxes.
        uint32_t num_seq;               ///< Nombre de séquences d'animation.
        uint32_t seq_index;             ///< Index des séquences d'animation.
        uint32_t num_seq_groups;        ///< Demand loaded sequences.
        uint32_t seq_group_index;       ///< Index des groupes de séquence.
        uint32_t num_textures;          ///< Raw textures.
        uint32_t texture_index;         ///< Index des texture.
        uint32_t texture_data_index;    ///< Index des données de texture.
        uint32_t num_skin_ref;          ///< Replaceable textures.
        uint32_t num_skin_families;     ///< Nombre de familles de skins.
        uint32_t skin_index;            ///< Index des skins.
        uint32_t num_bodyparts;         ///< Nombre de bodyparts.
        uint32_t bodypart_index;        ///< Index des bodyparts.
        uint32_t num_attachments;       ///< Queryable attachable points.
        uint32_t attachment_index;      ///< Index des attachments.
        uint32_t sound_table;           ///< Table des sons.
        uint32_t sound_index;           ///< Index des sons.
        uint32_t sound_groups;          ///< Groupes de sons.
        uint32_t sound_group_index;     ///< Index des groupes de sons.
        uint32_t num_transitions;       ///< Animation node to animation node transition graph.
        uint32_t transition_index;      ///< Index des transitions.
    };

    /// En-tête d'une séquence.
    struct TSeqHeader
    {
        int32_t id;                     ///< Identifiant.
        int32_t version;                ///< Version.
        char name[64];                  ///< Nom de la séquence.
        uint32_t length;                ///< Longueur en octets.
    };

    /// Groupe de séquences.
    struct TSeqGroup
    {
        char label[32];                 ///< Textual name.
        char name[64];                  ///< Filename.
        void * cache;                   ///< Cache index pointer. \bug Le type void * a une taille variable.
        int32_t data;                   ///< Hack for group 0.
    };

    /// Description d'une séquence.
    struct TSeqDescription
    {
        char name[32];                  ///< Nom de la séquence.
        float timing;                   ///< Nombre de frames par seconde.
        int32_t flags;                  ///< Flags (lecture en boucle ou pas).
        int32_t activity;               ///< Activité de la séquence (0 par défaut).
        uint32_t actweight;             ///< Importance de la séquence parmi les séquences ayant la même activité.
        uint32_t num_events;            ///< Nombre d'évènements.
        int32_t event_index;            ///< Offset des évènements.
        uint32_t num_frames;            ///< Nombre de frames.
        uint32_t num_pivots;            ///< Number of foot pivots.
        int32_t pivot_offset;           ///< Offset des pivots.
        int32_t motion_type;
        int32_t motion_bone;
        glm::vec3 linear_movement;      ///< Déplacement linéaire au cours de cette séquence.
        int32_t auto_move_pos_index;
        int32_t auto_move_angle_index;
        glm::vec3 min;                  ///< Point minimal de la boite englobante.
        glm::vec3 max;                  ///< Point maximal de la boite englobante.
        uint32_t num_blends;
        int32_t anim_offset;            ///< TAnimation pointer relative to start of sequence group.
                                        ///< data[blend][bone][X, Y, Z, XR, YR, ZR]
        int32_t blend_type[2];          ///< X, Y, Z, XR, YR, ZR
        float blend_start[2];           ///< Starting value.
        float blend_end[2];             ///< Ending value.
        int32_t	blend_parent;
        int32_t	seq_group;              ///< Sequence group for demand loading.
        int32_t	entry_node;             ///< Transition node at entry.
        int32_t	exit_node;              ///< Transition node at exit.
        int32_t	node_flags;             ///< Transition rules.
        int32_t	next_seq;               ///< Séquence suivante.
    };

    /// Os.
    struct TBone
    {
        char name[32];                  ///< Bone name for symbolic links.
        int32_t parent;                 ///< Parent bone.
        int32_t flags;                  ///< Flags.
        int32_t bone_controller[6];     ///< Bone controller index, -1 == none.
        float value[6];                 ///< Default DoF values.
        float scale[6];                 ///< Scale for delta DoF values.
    };

    /// Contrôleur d'un os.
    struct TBoneController
    {
        int32_t bone;                   ///< -1 == 0.
        int32_t type;                   ///< X, Y, Z, XR, YR, ZR, M.
        float start;                    ///< Valeur minimale du contrôleur.
        float end;                      ///< Valeur maximale du contrôleur.
        int32_t rest;                   ///< Byte index value at rest.
        uint32_t index;                 ///< 0-3 user set controller, 4 mouth.
    };

    /// Animation.
    struct TAnimation
    {
        uint16_t offset[6];
    };

    /// Frame d'une animation.
    union TAnimFrame
    {
        struct _v
        {
            unsigned char valid;
            unsigned char total;
        } v;

        int16_t value;
    };

    /// Groupe.
    struct TBodyPart
    {
        char name[64];              ///< Nom du groupe.
        uint32_t num_models;        ///< Nombre de sous-modèles associés.
        int32_t base;
        uint32_t model_offset;      ///< Index into models array.
    };

    /// Texture.
    struct TTexture
    {
        char name[64];              ///< Nom de la texture.
        int32_t flags;              ///< Flags.
        uint32_t width;             ///< Largeur de l'image en pixels.
        uint32_t height;            ///< Hauteur de l'image en pixels.
        int32_t index;
    };

    /// Sous-modèle.
    struct TModel
    {
        char name[64];              ///< Nom du sous-modèle.
        int32_t type;               ///< Type de modèle.
        float bounding_radius;      ///< Rayon de la sphère englobante.
        uint32_t num_meshes;        ///< Nombre de meshes.
        int32_t mesh_offset;        ///< Offset des meshes.
        uint32_t num_vertices;      ///< Nombre de sommets.
        int32_t vertex_info_offset; ///< Vertex bone info.
        int32_t vertex_offset;      ///< Vertex offset.
        uint32_t num_normals;       ///< Nombre de normales.
        int32_t normal_info_offset; ///< Normal bone info.
        int32_t normal_offset;      ///< Normal offset.
        uint32_t num_groups;        ///< Deformation groups.
        int32_t group_offset;       ///< Offset des groupes.
    };

    /// Mesh.
    struct TMesh
    {
        uint32_t num_triangles;     ///< Nombre de triangles.
        uint32_t triangle_offset;   ///< Offset des triangles.
        int32_t skin_reference;
        uint32_t num_normals;       ///< Nombre de normales.
        uint32_t normal_offset;     ///< Offset des normales.
    };

    /// HitBox.
    struct THitBox
    {
        int32_t bone;               ///< Numéro de l'os.
        int32_t group;              ///< Intersection group.
        glm::vec3 bbmin;            ///< Point minimal de la boite englobante.
        glm::vec3 bbmax;            ///< Point maximal de la boite englobante.
    };

    /// Évènement.
    struct TEvent
    {
        uint32_t frame;             ///< Numéro de la frame.
        int32_t event;              ///< Identifiant de l'évènement.
        int32_t type;               ///< Type d'évènement.
        char options[64];           ///< Options.
    };

    /// Pivot.
    struct TPivot
    {
        float org[3];               ///< Pivot point.
        int32_t start;
        int32_t end;
    };

    /// Point d'attache.
    struct TAttachment
    {
        char name[32];              ///< Nom de l'attachement.
        int32_t type;               ///< Type d'attachement.
        int32_t bone;               ///< Os lié au point d'attache.
        glm::vec3 org;              ///< Attachment point.
        glm::vec3 vectors[3];
    };

#include "Core/struct_alignment_end.h"

private:

    // Méthodes privées
    THeader * loadModel(const CString& fileName);
    TSeqHeader * loadDemandSequences(const CString& fileName);
    glm::quat calcBoneQuaternion(unsigned short frame, float interp, const TBone * bone, TAnimation * anim);
    glm::vec3 calcBonePosition(unsigned short frame, float interp, const TBone * bone, TAnimation * anim);
    void calcBoneAdj();
    void calcRotations(glm::vec3 * pos, glm::quat * q, const TSeqDescription * description, TAnimation * anim, unsigned short frame, float interp);
    TAnimation * getAnim(TSeqDescription * description);
    void slerpBones(glm::quat q1[], glm::vec3 pos1[], glm::quat q2[], const glm::vec3 pos2[], float value);
    void setupBones(unsigned short sequence, unsigned short frame, float interp);
    float lighting(unsigned int bone, int flags, const glm::vec3& normal);
    void setupLighting();
    TModel * setupModel(unsigned short num, unsigned short group = 0);
    void updateSubModel(CBuffer<TVertex3D> * buffer, TModel * model, std::vector<TVertex3D>& vertices, unsigned short skin = 0);
    void uploadTexture(TTexture * texture, unsigned char * data, unsigned char * palette);
    void freeMemory();
    float setController(unsigned int index, float value);
    float setBlending(int blender, float value, unsigned short sequence);

private:

    // Données privées
    THeader * m_header;                                ///< En-tête du fichier.
    THeader * m_textureHeader;                         ///< En-tête de la texture.
    unsigned char * m_fileBuffer;                      ///< Buffer contenant l'intégralité du fichier.
    TSeqHeader * m_animationHeader[32];                ///< En-tête de chaque animation. \todo Pourquoi 32 ? Utiliser un std::vector à la place ?
    unsigned char m_blending[2];                       ///< Animation blending. \todo Rendre statique ?

    // Données statiques protégées
    static unsigned char m_controller[MaxControllers]; ///< Valeurs des controlleurs.
    static float m_adj[MaxControllers];                ///< Utilisé pour les controlleurs.
    static glm::vec3 * m_transform_vert_ptr;           ///< Pointeur utilisé pour parcourir la liste des sommets.
    static glm::vec3 * m_light_values_ptr;             ///< Pointeur utilisé pour parcourir la liste des lumières.
    static glm::vec3 m_transformVertices[MaxVertices]; ///< Transformed vertices.
    static glm::vec3 m_lightValues[MaxVertices];       ///< Light surface normals.
    static float m_boneTransforms[MaxBones][3][4];     ///< Bone transformation matrix. (modifié dans SetupBones).
    static glm::vec3 m_lightVector;                    ///< Light vector in model reference frame (vaut toujours (0,0,-1)).
    static int m_ambientLightColor;                    ///< Ambient world light (vaut toujours 32).
    static float m_shadeLight;                         ///< Direct world light (vaut toujours 192).
    static glm::vec3 m_lightColor;                     ///< Couleur de la lumière (vaut toujours (1,1,1) : lumière blanche).
    static glm::vec3 m_boneLightVector[MaxBones];      ///< Light vectors in bone reference frames.
};


T_DECLARE_OPERATORS_FOR_FLAGS(CModelDataMDLGoldSrc::TLightTypes)

T_DECLARE_OPERATORS_FOR_FLAGS(CModelDataMDLGoldSrc::TTransitionTypes)

} // Namespace TE
