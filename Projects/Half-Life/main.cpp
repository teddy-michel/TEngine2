/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>

#include "Game/CGameApplication.hpp"
#include "CActivityGame.hpp"
#include "CActivityMenu.hpp"

//#include "CMapLoaderBSPGoldSrc.hpp"

using namespace TE;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    CGameApplication game{};
    game.initWindow("TEngine2 - Half-Life");

    if (!game.init())
    {
#ifdef T_SYSTEM_WINDOWS
#  ifdef T_DEBUG
        // FIXME: bug entre les DLL de SFML en debug et les DLL du driver graphique qui cause un crash
        TerminateProcess(GetCurrentProcess(), -1);
#  endif
#endif

        return EXIT_FAILURE;
    }

    CActivityMenu * activityMenu = new CActivityMenu{};
    CActivityGame * activityGame = new CActivityGame{};
    activityMenu->setActivityGame(activityGame);
    activityGame->setActivityMenu(activityMenu);

    //CMapLoaderBSPGoldSrc mapLoader;
    //mapLoader.loadFromFile("../../../Resources/maps/c0a0a.bsp");

    game.setActivity(activityMenu);
    game.mainLoop();

    delete activityGame;
    delete activityMenu;

    game.release();

#ifdef T_SYSTEM_WINDOWS
#  ifdef T_DEBUG
    // FIXME: bug entre les DLL de SFML en debug et les DLL du driver graphique qui cause un crash
    TerminateProcess(GetCurrentProcess(), 0);
#  endif
#endif

    return 0;
}
