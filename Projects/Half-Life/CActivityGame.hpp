/*
Copyright (C) 2019-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "Game/IActivity.hpp"
#include "Graphic/CGuiManager.hpp"


namespace TE
{

class CMap;
class CCamera;
class CCameraFreeFly;
class CActivityMenu;
class CPlayer;
class CGuiLabel;


/**
 * \class   CActivityGame
 * \ingroup Game
 * \brief   Activit� pour g�rer le jeu.
 ******************************/

class CActivityGame : public IActivity
{
public:

    CActivityGame();
    virtual ~CActivityGame() = default;

    virtual void frame(float frameTime) override;

    virtual void onEvent(const CKeyboardEvent& event) override;

    void setActivityMenu(CActivityMenu * activity)
    {
        m_activityMenu = activity;
    }
/*
    void setMap(CMap * map)
    {
        m_map = map;
    }

    void setCamera(CCamera * camera)
    {
        m_camera = camera;
    }

    void setPlayer(CPlayer * player)
    {
        m_player = player;
    }
*/
    inline CMap * getMap() const noexcept
    {
        return m_map;
    }

    inline CCamera * getCamera() const noexcept
    {
        return m_camera;
    }

private:

    CMap * m_map;
    CCamera * m_camera;
    CCameraFreeFly * m_cameraFreeFly;
    CPlayer * m_player;
    CActivityMenu * m_activityMenu;
    CGuiManager m_gui;
    std::shared_ptr<CGuiLabel> m_labelLife;
    std::shared_ptr<CGuiLabel> m_labelFPS;
    std::shared_ptr<CGuiLabel> m_labelInteraction;
};

} // Namespace TE
