/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>
#include <limits>
#include <sstream>

#include "CMapLoaderBSPGoldSrc.hpp"
#include "CModelDataMDLGoldSrc.hpp"

#include "Game/CMap.hpp"
#include "Entities/CMapEntity.hpp"
#include "Entities/CModelEntity.hpp"
#include "Entities/CMesh.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CMetaTexture.hpp"
#include "Graphic/CShaderManager.hpp"
#include "Graphic/VertexFormat.hpp"
#include "Game/CGameApplication.hpp"
#include "Entities/CPlayerStart.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Physic/CShapeTriMesh.hpp"
#include "Core/utils.hpp"


namespace TE
{

typedef bool(*EntityLoaderFct)(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models);


/**
 * Constructeur par défaut.
 ******************************/

CMapLoaderBSPGoldSrc::CMapLoaderBSPGoldSrc() :
    IMapLoader  {},
    m_file      {},
    m_map       { nullptr },
    m_lightmaps { 0 }
{

}


/**
 * Destructeur.
 ******************************/

CMapLoaderBSPGoldSrc::~CMapLoaderBSPGoldSrc()
{
    unloadData();
}


/**
 * Chargement des données.
 *
 * \param fileName Nom du fichier contenant la map à charger.
 * \return Pointeur sur une nouvelle map, ou nullptr en cas d'erreur.
 ******************************/

CMap * CMapLoaderBSPGoldSrc::loadFromFile(const CString& filename)
{
    gApplication->log(CString("Load BSP GoldSrc map \"%1\".").arg(filename));

    CMap * map = nullptr;

    m_file.open(filename.toCharArray(), std::fstream::in | std::fstream::binary);
    if (m_file)
    {
        map = new CMap{};
        m_map = map->getMapEntity();

        //m_file.seekg(0, std::ios_base::end);
        //int filesize = m_file.tellg();

        int ident = 0;
        //m_file.seekg(0);
        m_file.read(reinterpret_cast<char *>(&ident), 4);

        // Format BSP de Half-Life
        if (ident != 30)
        {
            m_file.close();
            gApplication->log("Invalid identifier for BSP GoldSrc map.");
            return nullptr;
        }

        // Liste des lumps
        m_file.read(reinterpret_cast<char *>(&m_lumps), 15 * sizeof(TLump));

        // Chargement des données
        //loadPlanes();
        loadTextures();
        loadVertices();
        //loadNodes();
        //loadLeafs();
        loadModels();
        //loadGeometry();
        loadEntities();

        m_file.close();

        m_map = nullptr;
        unloadData();
    }

    return map;
}


/**
 * Supprime toutes les données chargées en mémoire.
 ******************************/

void CMapLoaderBSPGoldSrc::unloadData()
{
    //CEntityManager::Instance().DeleteEntities();
/*
    if (Game::physicEngine->getBSPTree() == m_bsp)
    {
        Game::physicEngine->setBSPTree(nullptr);
    }
*/
    //gTextureManager->deleteTexture(m_lightmap);  // TODO: move to CMapBSPGoldSrc
    for (int lightmap_index = 0; lightmap_index < NumLightmaps; ++lightmap_index)
    {
        m_lightmaps[lightmap_index] = 0;
    }

    for (unsigned int i = 0; i < 15; ++i)
    {
        m_lumps[i].fileofs = 0;
        m_lumps[i].filelen = 0;
    }

    for (auto& model : m_models)
    {
        if (!model.used)
        {
            if (model.trimesh != nullptr)
            {
                //model.trimesh->release();
                //delete model.trimesh;
            }
        }
    }

    m_models.clear();

    m_textinfos.clear();
    //m_faces.clear();
    //m_planes.clear();
    m_vertices.clear();
    m_nodes.clear();
    m_leafs.clear();
/*
    // Pour ne pas supprimer le joueur
    if (m_player)
    {
        m_player->setParent(nullptr);
    }

    IMap::unloadData();
*/
}


/**
 * Ajoute un message dans le log et dans la console lorsqu'un paramètre manque pour une entité.
 *
 * \param entity Nom de l'entité.
 * \param param  Nom du paramètre.
 ******************************/

void warningEntityParamMissing(const CString& entity, const CString& param)
{
    gApplication->log(CString("Parameter \"%1\" is missing for entity \"%2\".").arg(param).arg(entity), ILogger::Warning);
}

// Fonction par défaut pour charger une entité
bool loadEntityNotImplemented(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    gApplication->log(CString("Loading entity \"%1\" is not yet implemented.").arg(classname), ILogger::Warning);
    return false;
}

// Fonction de chargement des entités avec un model associé
bool loadEntityWithModel(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    if (params.find("model") == params.end())
    {
        warningEntityParamMissing(classname, "model");
        return false;
    }

    // TODO

    return false;
}

// Fonction de chargement des entités "monster_barney"
bool loadEntityMonsterBarney(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    CModelDataMDLGoldSrc * modelData = new CModelDataMDLGoldSrc{}; // TODO: use resource manager
    modelData->loadFromFile("../../../Projects/Half-Life/models/barney.mdl");
    CModelEntity * model = new CModelEntity{ modelData, map, "monster_barney" };

    //auto shaderModel = std::make_shared<CShader>();
    std::shared_ptr<CShader> shaderModel;

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        //shaderModel->loadFromFiles("../../../Resources/shaders/texturing_bt.vert", "../../../Resources/shaders/texturing_bt.frag");
        shaderModel = gShaderManager->loadFromFiles("texturing_bt.vert", "texturing_bt.frag");
        shaderModel->setBindlessTexture(true);
    }
    else
    {
        //shaderModel->loadFromFiles("../../../Resources/shaders/texturing.vert", "../../../Resources/shaders/texturing.frag");
        shaderModel = gShaderManager->loadFromFiles("texturing.vert", "texturing.frag");
    }

    model->setShader(shaderModel);

    // TODO: certains models se retrouvent dans le sol

    if (classname == "monster_barney")
    {
        model->setSequence(modelData->getSequenceByActivity(ActIdle));
        // La position des modèles n'est pas au centre mais en bas
        model->translate({ 0.0f, 0.0f, -18.0f * 2.54f });
    }
    else if (classname == "monster_barney_dead")
    {
        model->setSequence(modelData->getSequenceNumber("lying_on_back"));
        // "lying_on_back", "lying_on_stomach", "dead_sitting", "dead_hang", "dead_table1", "dead_table2", "dead_table3"
    }

    // TODO: use "target_name"
    // TODO: modelData->getEyesPosition() => INPC

    //model->setSequence(1);

    // Body
    auto body = params.find("body");
    if (body != params.end())
    {
        int body_int = body->second.toInt16();

        if (body_int == -1)
        {
            body_int = RandInt(0, modelData->getNumGroups());
        }

        model->setGroup(body_int);
    }

    // Position de l'entité
    auto origin = params.find("origin");
    if (origin != params.end())
    {
        std::istringstream iss(origin->second.toCharArray());
        glm::vec3 v;
        iss >> v.x >> v.y >> v.z;
        model->translate(v * 2.54f);
    }

    // Angle selon Z en degrés
    // On considère que si l'angle est nul, on regarde en X croissant
    // Sens anti-horaire
    auto angle = params.find("angle");
    if (angle != params.end())
    {
        //float a = std::stof(angle->second.toCharArray());
        float a = angle->second.toFloat();
        model->rotate(glm::angleAxis(glm::radians(a), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    }

    return false;
}

// Fonction de chargement des entités "monster_scientist"
bool loadEntityMonsterScientist(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    CModelDataMDLGoldSrc * modelData = new CModelDataMDLGoldSrc{}; // TODO: use resource manager
    modelData->loadFromFile("../../../Projects/Half-Life/models/Scientist.mdl");
    CModelEntity * model = new CModelEntity{ modelData, map, "monster_scientist" };

    //auto shaderModel = std::make_shared<CShader>();
    std::shared_ptr<CShader> shaderModel;

    if (gRenderer->support(TFunctionnality::BindlessTexture))
    {
        //shaderModel->loadFromFiles("../../../Resources/shaders/texturing_bt.vert", "../../../Resources/shaders/texturing_bt.frag");
        shaderModel = gShaderManager->loadFromFiles("texturing_bt.vert", "texturing_bt.frag");
        shaderModel->setBindlessTexture(true);
    }
    else
    {
        //shaderModel->loadFromFiles("../../../Resources/shaders/texturing.vert", "../../../Resources/shaders/texturing.frag");
        shaderModel = gShaderManager->loadFromFiles("texturing.vert", "texturing.frag");
    }

    model->setShader(shaderModel);

    // TODO: certains models se retrouvent dans le sol

    if (classname == "monster_scientist")
    {
        model->setSequence(modelData->getSequenceByActivity(ActIdle));
        // La position des modèles n'est pas au centre mais en bas
        model->translate({ 0.0f, 0.0f, -18.0f * 2.54f });
    }
    else if (classname == "monster_scientist_dead")
    {
        model->setSequence(modelData->getSequenceNumber("lying_on_back"));
        // "lying_on_back", "lying_on_stomach", "dead_sitting", "dead_hang", "dead_table1", "dead_table2", "dead_table3"
    }
    else if (classname == "monster_sitting_scientist")
    {
        model->setSequence(modelData->getSequenceNumber("sitlookleft"));
        // pev->sequence = m_baseSequence + RANDOM_LONG(0,4);
        // La position des modèles n'est pas au centre mais en bas
        model->translate({ 0.0f, 0.0f, -24.0f * 2.54f });
    }

    // TODO: use "target_name"
    // TODO: modelData->getEyesPosition() => INPC

    //model->setSequence(1);

    // Body
    auto body = params.find("body");
    if (body != params.end())
    {
        int body_int = body->second.toInt16();

        if (body_int == -1)
        {
            body_int = RandInt(0, modelData->getNumGroups());
        }

        model->setGroup(body_int);

        if (body_int == 2)
        {
            model->setSkin(1); // Le body 2 ("Luther") a la peau noire
        }
    }

    // Position de l'entité
    auto origin = params.find("origin");
    if (origin != params.end())
    {
        std::istringstream iss(origin->second.toCharArray());
        glm::vec3 v;
        iss >> v.x >> v.y >> v.z;
        model->translate(v * 2.54f);
    }

    // Angle selon Z en degrés
    // On considère que si l'angle est nul, on regarde en X croissant
    // Sens anti-horaire
    auto angle = params.find("angle");
    if (angle != params.end())
    {
        //float a = std::stof(angle->second.toCharArray());
        float a = angle->second.toFloat();
        model->rotate(glm::angleAxis(glm::radians(a), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    }

    return false;
}

// Fonction de chargement des entités "func_wall"
bool loadEntityMesh(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    auto model_param = params.find("model");
    if (model_param == params.end())
    {
        warningEntityParamMissing(classname, "model");
        return false;
    }

    CString model_value = model_param->second;
    if (model_value[0] == '*')
    {
        uint32_t model_num = model_value.subString(1).toUnsignedInt32();
        gApplication->log(CString::fromUTF8("Création de l'entité '%1' avec le model %2.").arg(classname).arg(model_num));
        if (model_num < models.size())
        {
            std::shared_ptr<CBuffer<TVertex3D>> buffer = models[model_num].buffer;
            models[model_num].used = true;

            gApplication->log("Create entity CMesh.", ILogger::Debug);
            CMesh * mesh = new CMesh{ map, classname };

            //auto shader = std::make_shared<CShader>();
            std::shared_ptr<CShader> shader;

            auto alpha_param = params.find("renderamt");
            if (alpha_param == params.end())
            {
                if (gRenderer->support(TFunctionnality::BindlessTexture))
                {
                    //shader->loadFromFiles("../../../Resources/shaders/lightmaps_bt.vert", "../../../Resources/shaders/lightmaps_bt.frag");
                    shader = gShaderManager->loadFromFiles("lightmaps_bt.vert", "lightmaps_bt.frag");
                    shader->setBindlessTexture(true);
                }
                else
                {
                    //shader->loadFromFiles("../../../Resources/shaders/lightmaps.vert", "../../../Resources/shaders/lightmaps.frag");
                    shader = gShaderManager->loadFromFiles("lightmaps.vert", "lightmaps.frag");
                }
            }
            else
            {
                //shader->loadFromFiles("../../../Resources/shaders/alpha.vert", "../../../Resources/shaders/alpha.frag");
                shader = gShaderManager->loadFromFiles("alpha.vert", "alpha.frag");
                shader->setUniformValue("color", { 1.0f, 1.0f, 1.0f, static_cast<float>(alpha_param->second.toInt8()) / 255.0f });
                buffer->setTransparent(true);
            }

            mesh->setShader(shader);
            mesh->setBuffer(buffer);
            buffer->update();

            // Position de l'entité
            auto origin = params.find("origin");
            if (origin != params.end())
            {
                std::istringstream iss(origin->second.toCharArray());
                glm::vec3 v;
                iss >> v.x >> v.y >> v.z;
                mesh->translate(v * 2.54f);
            }

            mesh->setTriMesh(models[model_num].trimesh);
        }

        return true;
    }

    return false;
}

// Fonction de chargement des entités "trigger_change_level"
bool loadEntityChangeLevel(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    if (params.find("model") == params.end())
    {
        warningEntityParamMissing("trigger_changelevel", "model");
        return false;
    }

    if (params.find("map") == params.end())
    {
        warningEntityParamMissing("trigger_changelevel", "model");
        return false;
    }

    // TODO

    return false;
}

// Fonction de chargement des entités "player_start"
bool loadEntityPlayerStart(const CString& classname, const std::map<CString, CString>& params, CMapEntity * map, const std::vector<TModel>& models)
{
    CPlayerStart * entity = new CPlayerStart{ map };

    // Position de l'entité
    auto origin = params.find("origin");
    if (origin != params.end())
    {
        std::istringstream iss(origin->second.toCharArray());
        glm::vec3 v;
        iss >> v.x >> v.y >> v.z;
        entity->translate(v * 2.54f);
    }

    // Angle selon Z en degrés
    // On considère que si l'angle est nul, on regarde en X croissant
    // Sens anti-horaire
    auto angle = params.find("angle");
    if (angle != params.end())
    {
        //float a = std::stof(angle->second.toCharArray());
        float a = angle->second.toFloat();
        entity->rotate(glm::angleAxis(glm::radians(a), glm::vec3{ 0.0f, 0.0f, 1.0f }));
    }

    return false;
}


/**
 * Création des entités.
 * Doit être appellée après LoadModels.
 ******************************/

void CMapLoaderBSPGoldSrc::loadEntities()
{
    CString entities;
    std::vector<std::map<CString, CString> > m_entities;

    entities.reserve(m_lumps[LumpEntities].filelen);
    m_file.seekg(m_lumps[LumpEntities].fileofs);

    for (unsigned int i = 0 ; i < m_lumps[LumpEntities].filelen ; ++i)
    {
        unsigned char entity_char;
        m_file.read(reinterpret_cast<char *>(&entity_char), 1);
        entities += entity_char;
    }

    std::map<CString, CString> params;
    std::stringstream buffer_entity;
    buffer_entity << entities;
    bool read_value, read_param;
    std::string param, value, texte;

    while (!buffer_entity.eof())
    {
        getline(buffer_entity, texte);

        if (texte == "{")
        {
            params.clear();
        }
        else if (texte == "}")
        {
            m_entities.push_back(params);
        }
        else
        {
            param = "";
            value = "";
            read_value = false;
            read_param = false;

            // On parcourt la ligne caractère par caractère
            for (unsigned int i = 0 ; texte[i] != '\n' && texte[i] != '\r' && texte[i] != '\0' ; ++i)
            {
                if (texte[i] == '"')
                {
                    if (read_param)
                    {
                        read_param = false;
                    }
                    else if (read_value)
                    {
                        read_value = false;
                    }
                    else if (param.size() > 0)
                    {
                        read_value = true;
                    }
                    else
                    {
                        read_param = true;
                    }
                }
                else if (read_param)
                {
                    param += texte[i];
                }
                else if (read_value)
                {
                    value += texte[i];
                }
            }

            params[param.c_str()] = value.c_str();
        }
    }

    // Initialisation des chargeurs d'entités
    // TODO: créer une variable de classe ?
    std::map<CString, EntityLoaderFct> entityLoaders;
    entityLoaders["aiscripted_sequence"] = loadEntityNotImplemented;
    entityLoaders["ambient_generic"] = loadEntityNotImplemented;
    entityLoaders["ammo_357"] = loadEntityNotImplemented;
    entityLoaders["ammo_9mmAR"] = loadEntityNotImplemented;
    entityLoaders["ammo_9mmclip"] = loadEntityNotImplemented;
    entityLoaders["ammo_ARgrenades"] = loadEntityNotImplemented;
    entityLoaders["ammo_buckshot"] = loadEntityNotImplemented;
    entityLoaders["ammo_crossbow"] = loadEntityNotImplemented;
    entityLoaders["ammo_gaussclip"] = loadEntityNotImplemented;
    entityLoaders["ammo_glockclip"] = loadEntityNotImplemented;
    entityLoaders["ammo_mp5clip"] = loadEntityNotImplemented;
    entityLoaders["ammo_rpgclip"] = loadEntityNotImplemented;
    entityLoaders["cycler"] = loadEntityNotImplemented;
    entityLoaders["env_beam"] = loadEntityNotImplemented;
    entityLoaders["env_beverage"] = loadEntityNotImplemented;
    entityLoaders["env_blood"] = loadEntityNotImplemented;
    entityLoaders["env_bubbles"] = loadEntityNotImplemented;
    entityLoaders["env_explosion"] = loadEntityNotImplemented;
    entityLoaders["env_fade"] = loadEntityNotImplemented;
    entityLoaders["env_funnel"] = loadEntityNotImplemented;
    entityLoaders["env_global"] = loadEntityNotImplemented;
    entityLoaders["env_glow"] = loadEntityNotImplemented;
    entityLoaders["env_laser"] = loadEntityNotImplemented;
    entityLoaders["env_message"] = loadEntityNotImplemented;
    entityLoaders["env_render"] = loadEntityNotImplemented;
    entityLoaders["env_shooter"] = loadEntityNotImplemented;
    entityLoaders["env_sound"] = loadEntityNotImplemented;
    entityLoaders["env_shake"] = loadEntityNotImplemented;
    entityLoaders["env_spark"] = loadEntityNotImplemented;
    entityLoaders["env_sprite"] = loadEntityNotImplemented;
    entityLoaders["func_breakable"] = loadEntityMesh; // TODO: create breakable mesh
    entityLoaders["func_conveyor"] = loadEntityNotImplemented;
    entityLoaders["func_button"] = loadEntityMesh; // TODO
    entityLoaders["func_door"] = loadEntityMesh; // TODO
    entityLoaders["func_door_rotating"] = loadEntityMesh; // TODO
    entityLoaders["func_friction"] = loadEntityWithModel;
    entityLoaders["func_healthcharger"] = loadEntityWithModel;
    entityLoaders["func_illusionary"] = loadEntityMesh; // TODO
    entityLoaders["func_ladder"] = loadEntityNotImplemented;
    entityLoaders["func_monsterclip"] = loadEntityWithModel;
    entityLoaders["func_pendulum"] = loadEntityNotImplemented;
    entityLoaders["func_platrot"] = loadEntityNotImplemented;
    entityLoaders["func_pushable"] = loadEntityNotImplemented;
    entityLoaders["func_recharge"] = loadEntityNotImplemented;
    entityLoaders["func_rotating"] = loadEntityWithModel;
    entityLoaders["func_rot_button"] = loadEntityNotImplemented;
    entityLoaders["func_tank_controls"] = loadEntityNotImplemented;
    entityLoaders["func_tank_mortar"] = loadEntityNotImplemented;
    entityLoaders["func_tanklaser"] = loadEntityNotImplemented;
    entityLoaders["func_trackautochange"] = loadEntityNotImplemented;
    entityLoaders["func_trackchange"] = loadEntityNotImplemented;
    entityLoaders["func_tracktrain"] = loadEntityMesh; // TODO
    entityLoaders["func_train"] = loadEntityWithModel;
    entityLoaders["func_traincontrols"] = loadEntityNotImplemented;
    entityLoaders["func_wall"] = loadEntityMesh;
    entityLoaders["func_wall_toggle"] = loadEntityWithModel;
    entityLoaders["func_water"] = loadEntityNotImplemented;
    entityLoaders["gibshooter"] = loadEntityNotImplemented;
    entityLoaders["infodecal"] = loadEntityNotImplemented;
    entityLoaders["info_bigmomma"] = loadEntityNotImplemented;
    entityLoaders["info_intermission"] = loadEntityNotImplemented;
    entityLoaders["info_landmark"] = loadEntityNotImplemented;
    entityLoaders["info_node"] = loadEntityNotImplemented;
    entityLoaders["info_node_air"] = loadEntityNotImplemented;
    entityLoaders["info_player_deathmatch"] = loadEntityNotImplemented;
    entityLoaders["info_player_start"] = loadEntityPlayerStart;
    entityLoaders["info_target"] = loadEntityNotImplemented;
    entityLoaders["info_teleport_destination"] = loadEntityNotImplemented;
    entityLoaders["item_battery"] = loadEntityNotImplemented;
    entityLoaders["item_healthkit"] = loadEntityNotImplemented;
    entityLoaders["item_longjump"] = loadEntityNotImplemented;
    entityLoaders["light"] = loadEntityNotImplemented;
    entityLoaders["light_environment"] = loadEntityNotImplemented;
    entityLoaders["light_spot"] = loadEntityNotImplemented;
    entityLoaders["momentary_door"] = loadEntityNotImplemented;
    entityLoaders["momentary_rot_button"] = loadEntityNotImplemented;
    entityLoaders["monster_alien_grunt"] = loadEntityNotImplemented;
    entityLoaders["monster_alien_slave"] = loadEntityNotImplemented;
    entityLoaders["monster_apache"] = loadEntityNotImplemented;
    entityLoaders["monster_barnacle"] = loadEntityNotImplemented;
    entityLoaders["monster_barney"] = loadEntityMonsterBarney;
    entityLoaders["monster_barney_dead"] = loadEntityMonsterBarney;
    entityLoaders["monster_bullchicken"] = loadEntityNotImplemented;
    entityLoaders["monster_cockroach"] = loadEntityNotImplemented;
    entityLoaders["monster_furniture"] = loadEntityNotImplemented;
    entityLoaders["monster_gargantua"] = loadEntityNotImplemented;
    entityLoaders["monster_generic"] = loadEntityNotImplemented;
    entityLoaders["monster_gman"] = loadEntityNotImplemented;
    entityLoaders["monster_headcrab"] = loadEntityNotImplemented;
    entityLoaders["monster_hevsuit_dead"] = loadEntityNotImplemented;
    entityLoaders["monster_houndeye"] = loadEntityNotImplemented;
    entityLoaders["monster_hgrunt_dead"] = loadEntityNotImplemented;
    entityLoaders["monster_human_grunt"] = loadEntityNotImplemented;
    entityLoaders["monster_ichthyosaur"] = loadEntityNotImplemented;
    entityLoaders["monster_leech"] = loadEntityNotImplemented;
    entityLoaders["monster_miniturret"] = loadEntityNotImplemented;
    entityLoaders["monster_scientist"] = loadEntityMonsterScientist;
    entityLoaders["monster_scientist_dead"] = loadEntityMonsterScientist;
    entityLoaders["monster_sentry"] = loadEntityNotImplemented;
    entityLoaders["monster_sitting_scientist"] = loadEntityMonsterScientist;
    entityLoaders["monster_tentacle"] = loadEntityNotImplemented;
    entityLoaders["monster_tripmine"] = loadEntityNotImplemented;
    entityLoaders["monster_turret"] = loadEntityNotImplemented;
    entityLoaders["monster_zombie"] = loadEntityNotImplemented;
    entityLoaders["monstermaker"] = loadEntityNotImplemented;
    entityLoaders["multi_manager"] = loadEntityNotImplemented;
    entityLoaders["multisource"] = loadEntityNotImplemented;
    entityLoaders["path_corner"] = loadEntityNotImplemented;
    entityLoaders["path_track"] = loadEntityNotImplemented;
    entityLoaders["player_loadsaved"] = loadEntityNotImplemented;
    entityLoaders["scripted_sentence"] = loadEntityNotImplemented;
    entityLoaders["scripted_sequence"] = loadEntityNotImplemented;
    entityLoaders["speaker"] = loadEntityNotImplemented;
    entityLoaders["target_cdaudio"] = loadEntityNotImplemented;
    entityLoaders["trigger_auto"] = loadEntityNotImplemented;
    entityLoaders["trigger_autosave"] = loadEntityWithModel;
    entityLoaders["trigger_cdaudio"] = loadEntityNotImplemented;
    entityLoaders["trigger_changelevel"] = loadEntityChangeLevel;
    entityLoaders["trigger_gravity"] = loadEntityNotImplemented;
    entityLoaders["trigger_hurt"] = loadEntityNotImplemented;
    entityLoaders["trigger_monsterjump"] = loadEntityNotImplemented;
    entityLoaders["trigger_multiple"] = loadEntityNotImplemented;
    entityLoaders["trigger_once"] = loadEntityNotImplemented;
    entityLoaders["trigger_push"] = loadEntityNotImplemented;
    entityLoaders["trigger_relay"] = loadEntityNotImplemented;
    entityLoaders["trigger_teleport"] = loadEntityNotImplemented;
    entityLoaders["trigger_transition"] = loadEntityNotImplemented;
    entityLoaders["weapon_357"] = loadEntityNotImplemented;
    entityLoaders["weapon_9mmAR"] = loadEntityNotImplemented;
    entityLoaders["weapon_crossbow"] = loadEntityNotImplemented;
    entityLoaders["weapon_crowbar"] = loadEntityNotImplemented;
    entityLoaders["weapon_egon"] = loadEntityNotImplemented;
    entityLoaders["weapon_gauss"] = loadEntityNotImplemented;
    entityLoaders["weapon_glock"] = loadEntityNotImplemented;
    entityLoaders["weapon_handgrenade"] = loadEntityNotImplemented;
    entityLoaders["weapon_hornetgun"] = loadEntityNotImplemented;
    entityLoaders["weapon_mp5"] = loadEntityNotImplemented;
    entityLoaders["weapon_rpg"] = loadEntityNotImplemented;
    entityLoaders["weapon_satchel"] = loadEntityNotImplemented;
    entityLoaders["weapon_shotgun"] = loadEntityNotImplemented;
    entityLoaders["weapon_snark"] = loadEntityNotImplemented;
    entityLoaders["weapon_tripmine"] = loadEntityNotImplemented;
    entityLoaders["weaponbox"] = loadEntityNotImplemented;
    entityLoaders["worldspawn"] = loadEntityNotImplemented;
    entityLoaders["world_items"] = loadEntityNotImplemented;
    entityLoaders["xen_hair"] = loadEntityNotImplemented;
    entityLoaders["xen_plantlight"] = loadEntityNotImplemented;
    entityLoaders["xen_spore_medium"] = loadEntityNotImplemented;
    entityLoaders["xen_spore_small"] = loadEntityNotImplemented;
    entityLoaders["xen_tree"] = loadEntityNotImplemented;

    // Pour chaque entité
    for (std::vector<std::map<CString, CString> >::iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        // On cherche la clé classname
        if (it->find("classname") == it->end())
        {
            gApplication->log("Entity without classname.");
            continue;
        }

        CString classname = (*it)["classname"];
        if (entityLoaders.find(classname) == entityLoaders.end())
        {
            gApplication->log(CString("Unknown entity for GoldSrc (%1).").arg((*it)["classname"]));
        }
        else
        {
            entityLoaders[classname](classname, *it, m_map, m_models);
        }
    }
}


/**
 * Lecture de la liste des plans.
 ******************************/
/*
void CMapLoaderBSPGoldSrc::loadPlanes()
{
    m_planes.reserve(m_lumps[LumpPlanes].filelen / sizeof(TBSPPlane));
    m_file.seekg(m_lumps[LumpPlanes].fileofs);

    // Pour chaque plan
    for (unsigned int i = 0; i < m_lumps[LumpPlanes].filelen / sizeof(TBSPPlane); ++i)
    {
        TBSPPlane plan;
        m_file.read(reinterpret_cast<char *>(&plan), sizeof(TBSPPlane));
        m_planes.push_back(CPlane(plan.normale, plan.distance));
    }
}
*/

/**
 * Lecture de la liste des textures.
 ******************************/

void CMapLoaderBSPGoldSrc::loadTextures()
{
    gApplication->log("CMapLoaderBSPGoldSrc::loadTextures");

    // Nombre de textures
    unsigned int nbr_textures = 0;
    m_file.seekg(m_lumps[LumpTextures].fileofs);
    m_file.read(reinterpret_cast<char *>(&nbr_textures), 4);

    std::vector<unsigned int> offsets;
    offsets.reserve(nbr_textures);

    // Longueur du nom de chaque texture
    for (unsigned int i = 0; i < nbr_textures; ++i)
    {
        unsigned int offset = 0;
        m_file.read(reinterpret_cast<char *>(&offset), 4);
        offsets.push_back(offset);
    }

    gApplication->log(CString("Found %1 textures.").arg(nbr_textures));
    m_textinfos.reserve(nbr_textures);

    // Pour chaque texture
    for (unsigned int i = 0; i < nbr_textures; ++i)
    {
        char name[41]; // Les noms des textures font moins de 40 caractères
        memset(name, 0, 41);

        TTextureInfos texture_infos = { 64 , 64 , 1 };  // TODO: get default texture params

        m_file.seekg(m_lumps[LumpTextures].fileofs + offsets[i]);

        // On lit le nom de la texture
        m_file.read(reinterpret_cast<char *>(&name), 40);

        if (!m_file) {
            //std::cerr << "Can't read texture name." << std::endl;
            m_textinfos.push_back(texture_infos);
            continue;
        }

        // On modifie le nom
        CString texture_name = CString(name).toLowerCase() + ".bmp";

        // On essaye d'ouvrir le fichier contenant l'image de la texture
        std::ifstream texture_file(("../../../Projects/Half-Life/textures/" + texture_name).toCharArray(), std::ios::in | std::ios::binary); // TODO: configure the path in CTextureManager

        if (texture_file)
        {
            texture_file.close();

            // Chargement de la texture
            CImage image;

            // On tente de charger la texture
            if (image.loadFromFile("../../../Projects/Half-Life/textures/" + texture_name)) // TODO: configure the path in CTextureManager
            {
                texture_infos.width = image.getWidth();
                texture_infos.height = image.getHeight();

                // Texture transparente : le bleu devient transparent
                if (texture_name[0] == '{')
                {
                    image.changeColor(CColor::Blue, CColor(255, 255, 255, 0 ));
                }

                texture_infos.id = gTextureManager->loadTexture("hl/" + texture_name, image);
            }
        }

        // On ajoute la texture à la liste
        m_textinfos.push_back(texture_infos);
    }
}


/**
 * Chargement des vertices.
 ******************************/

void CMapLoaderBSPGoldSrc::loadVertices()
{
    m_vertices.clear();
    m_vertices.reserve(m_lumps[LumpVertexes].filelen / 12);

    m_file.seekg(m_lumps[LumpVertexes].fileofs);

    for (unsigned int i = 0 ; i < m_lumps[LumpVertexes].filelen / 12 ; ++i)
    {
        glm::vec3 v;
        m_file.read(reinterpret_cast<char *>(&v), 12);

        m_vertices.push_back(v);
    }
}


/**
 * Chargement des nodes de l'arbre BSP.
 ******************************/

void CMapLoaderBSPGoldSrc::loadNodes()
{
    m_nodes.clear();
    m_nodes.reserve(m_lumps[LumpNodes].filelen / sizeof(TBSPNode));

    m_file.seekg(m_lumps[LumpNodes].fileofs);

    for (unsigned int i = 0; i < m_lumps[LumpNodes].filelen / sizeof(TBSPNode); ++i)
    {
        TBSPNode node;
        m_file.read(reinterpret_cast<char *>(&node), sizeof(TBSPNode));

        m_nodes.push_back(node);
    }
}


/**
 * Chargement des feuilles de l'arbre BSP.
 ******************************/

void CMapLoaderBSPGoldSrc::loadLeafs()
{
    m_leafs.clear();
    m_leafs.reserve(m_lumps[LumpLeafs].filelen / sizeof(TBSPLeaf));

    m_file.seekg(m_lumps[LumpLeafs].fileofs);

    for (unsigned int i = 0; i < m_lumps[LumpLeafs].filelen / sizeof(TBSPLeaf); ++i)
    {
        TBSPLeaf leaf;
        m_file.read((char *)&leaf, sizeof(TBSPLeaf));

        m_leafs.push_back(leaf);
    }
}


/// Fonction utilitaire pour calculer les coordonnées de texture.
inline float computeTextCoord(const float vecs[4], const glm::vec3& v)
{
    return (vecs[0] * v.x + vecs[1] * v.y + vecs[2] * v.z + vecs[3]);
}


constexpr unsigned short LightmapTextureSize = 512; ///< Dimensions des lightmaps.


/**
 * Lecture des modèles.
 *
 * \todo Calculer les normales.
 ******************************/

void CMapLoaderBSPGoldSrc::loadModels()
{
    const unsigned int nbr_models = m_lumps[LumpModels].filelen / sizeof(TBSPModel);
    m_models.reserve(nbr_models);

    // Lecture de données depuis le fichier
    std::vector<TBSPFace> faces(m_lumps[LumpFaces].filelen / sizeof(TBSPFace));
    m_file.seekg(m_lumps[LumpFaces].fileofs);
    m_file.read(reinterpret_cast<char *>(&faces[0]), m_lumps[LumpFaces].filelen);

    std::vector<TBSPTexinfo> textinfos(m_lumps[LumpTexInfo].filelen / sizeof(TBSPTexinfo));
    m_file.seekg(m_lumps[LumpTexInfo].fileofs);
    m_file.read(reinterpret_cast<char *>(&textinfos[0]), m_lumps[LumpTexInfo].filelen);

    std::vector<int32_t> surfedges(m_lumps[LumpSurfEdges].filelen / sizeof(int32_t));
    m_file.seekg(m_lumps[LumpSurfEdges].fileofs);
    m_file.read(reinterpret_cast<char *>(&surfedges[0]), m_lumps[LumpSurfEdges].filelen);

    std::vector<TBSPEdge> edges(m_lumps[LumpEdges].filelen / sizeof(TBSPEdge));
    m_file.seekg(m_lumps[LumpEdges].fileofs);
    m_file.read(reinterpret_cast<char *>(&edges[0]), m_lumps[LumpEdges].filelen);

    std::vector<char> lighting(m_lumps[LumpLighting].filelen);
    m_file.seekg(m_lumps[LumpLighting].fileofs);
    m_file.read(&lighting[0], m_lumps[LumpLighting].filelen);

    std::vector<TBSPModel> model_datas(m_lumps[LumpModels].filelen / sizeof(TBSPModel));
    m_file.seekg(m_lumps[LumpModels].fileofs);
    m_file.read(reinterpret_cast<char *>(&model_datas[0]), sizeof(TBSPModel));

#ifdef T_CHECK_LIGHTMAPS_LOADING
    // Vérification du chargement des lightmap
    //
    // Pour chaque pixel : <offset de début de l'image, taille de l'image en pixels>
    // Ce tableau permet de vérifier que les lightmaps chargées ne se chevauchent pas, et donc que l'algorithme est correct.
    // On stocke l'offset et la taille et pas seulement le nombre d'utilisations, car parfois on retombe sur une lightmap déjà chargée précédemment.
    // Deux explications possibles :
    // - La même face peut être utilisée par différents modèles, ou plusieurs fois dans un même modèle.
    // - Une même image de lightmap peut être utilisée par différentes faces.
    std::vector<std::pair<int, int>> lighting_check(m_lumps[LumpLighting].filelen / 3);
#endif // T_CHECK_LIGHTMAPS_LOADING

    // Texture contenant les 4 lightmaps
    CMetaTexture lightmaps[NumLightmaps];
    for (int lightmap_index = 0; lightmap_index < NumLightmaps; ++lightmap_index)
    {
        // les dimensions par défaut sont déjà égales à LightmapTextureSize (512), inutile de créer 4 nouveaux objets
        //lightmaps[lightmap_index] = CMetaTexture(LightmapTextureSize, LightmapTextureSize);
        m_lightmaps[lightmap_index] = lightmaps[lightmap_index].getTextureId();
    }

    // Pour chaque modèle
    for (unsigned int i_model = 0; i_model < nbr_models; ++i_model)
    {
        // On crée un nouveau buffer graphique
        auto buffer = std::make_shared<CBuffer<TVertex3D>>();

        std::vector<unsigned int>& indices = buffer->getIndices();
        std::vector<glm::vec3> vertices;
        std::vector<TTextureId> text_faces;

        // Coordonnées de texture
        std::vector<glm::vec2> text_coords0;
        std::vector<glm::vec2> text_coords1;

        const TBSPModel model_data = model_datas[i_model];

        // Liste des surfaces
        for (unsigned int i_face = model_data.firstface; i_face < model_data.firstface + model_data.numfaces; ++i_face)
        {
            const TBSPFace face = faces[i_face];
            const TBSPTexinfo textinfo = textinfos[face.texture_info];

            // Texture spéciale (skybox)
            if (textinfo.flags & 1)
            {
                continue;
            }

            // Pour le calcul des lightmaps
            glm::vec2 min(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
            glm::vec2 max(-std::numeric_limits<float>::max(), -std::numeric_limits<float>::max());
            unsigned int lightmap_size_x = 0;
            unsigned int lightmap_size_y = 0;

            // Coordonnées des lightmaps
            unsigned int lightmap_coord_x = 0;
            unsigned int lightmap_coord_y = 0;

            // Lightmap
            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
            {
                //////////////////////////////////////////////////
                // On calcule les valeurs de min et max
                for (unsigned int j = 0; j < face.numedges; ++j)
                {
                    int32_t surfedge = surfedges[j + face.firstedge];

                    glm::vec3 vtmp;

                    if (surfedge < 0)
                    {
                        vtmp = m_vertices[edges[-surfedge].v1];
                    }
                    else
                    {
                        vtmp = m_vertices[edges[surfedge].v0];
                    }

                    const float val1 = computeTextCoord(textinfo.textureVecs[0], vtmp);
                    if (val1 < min.x)
                        min.x = val1;
                    if (val1 > max.x)
                        max.x = val1;

                    const float val2 = computeTextCoord(textinfo.textureVecs[1], vtmp);
                    if (val2 < min.y)
                        min.y = val2;
                    if (val2 > max.y)
                        max.y = val2;
                }

                // On calcul les dimensions des lightmaps de cette face
                lightmap_size_x = static_cast<unsigned int>(std::ceil(max.x / 16) - std::floor(min.x / 16)) + 1;
                lightmap_size_y = static_cast<unsigned int>(std::ceil(max.y / 16) - std::floor(min.y / 16)) + 1;
                // TODO: vérifier que le calcul donne le bon résultat (pas d'overlap entre deux lightmaps)

                CImage lightmap_default_img;

                for (int lightmap_index = 0; lightmap_index < NumLightmaps; ++lightmap_index)
                {
                    CImage lightmap_img;

                    // TODO: vérifier la signification de face.lightmap_styles (-1, 0, 0x20=?)

                    if (face.lightmap_styles[lightmap_index] == -1)
                    {
                        // On utilise la première lightmap
                        lightmap_img = lightmap_default_img;
                    }
                    else
                    {
                        CColor * lightmap = new CColor[lightmap_size_x * lightmap_size_y];
                        // TODO: lire directement le tableau de pixel, puis faire la conversion au niveau de CImage (voir pas de conversion !)
                        assert(lightmap != nullptr);

                        // Pour chaque luxel
                        for (unsigned int j = 0; j < lightmap_size_x * lightmap_size_y; ++j)
                        {
                            uint8_t color[3];
                            color[0] = lighting[face.lightmap_offset + 3 * j + 0];
                            color[1] = lighting[face.lightmap_offset + 3 * j + 1];
                            color[2] = lighting[face.lightmap_offset + 3 * j + 2];
                            lightmap[j] = CColor(color[0], color[1], color[2]);

#ifdef T_CHECK_LIGHTMAPS_LOADING
                            if (lighting_check[face.lightmap_offset / 3 + j].second == 0)
                            {
                                lighting_check[face.lightmap_offset / 3 + j].first = face.lightmap_offset;
                                lighting_check[face.lightmap_offset / 3 + j].second = lightmap_size_x * lightmap_size_y;
                            }
                            else if (lighting_check[face.lightmap_offset / 3 + j].first != face.lightmap_offset ||
                                     lighting_check[face.lightmap_offset / 3 + j].second != lightmap_size_x * lightmap_size_y)
                            {
                                gApplication->log("Lightmaps overlap", ILogger::Warning);
                            }
#endif // T_CHECK_LIGHTMAPS_LOADING
                        }

                        // Création d'une image contenant les pixels de l'image
                        lightmap_img = CImage(lightmap_size_x, lightmap_size_y, lightmap);
                        delete[] lightmap;
                    }

                    const unsigned int lightmapNum = lightmaps[lightmap_index].addImage(lightmap_img);
                    if (lightmap_index == 0)
                    {
                        lightmap_coord_x = lightmaps[0].getX(lightmapNum);
                        lightmap_coord_y = lightmaps[0].getY(lightmapNum);
                        lightmap_default_img = lightmap_img;
                    }
                }
            }


            // Création du tableau d'indices
            unsigned int first = 0;

            // On parcourt la liste des arrêtes de la face
            for (unsigned int i_edge = 0; i_edge < face.numedges; ++i_edge)
            {
                int32_t surfedge = surfedges[i_edge + face.firstedge];

                if (surfedge < 0)
                {
                    // Lecture des deux indices des sommets
                    TBSPEdge v = edges[-surfedge];

                    if (i_edge == 0)
                    {
                        first = v.v1;

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v1] * 2.54f);

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v0] * 2.54f);

                        if (textinfo.texture_id >= 0)
                        {
                            float textC0 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].width;
                            float textC1 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].height;
                            float textC2 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].width;
                            float textC3 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].height;

                            text_coords0.push_back(glm::vec2(textC0, textC1));
                            text_coords0.push_back(glm::vec2(textC2, textC3));

                            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
                            {
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC0 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC1 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC2 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC3 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                            }
                            else
                            {
                                text_coords1.push_back({ 0.0f, 0.0f });
                                text_coords1.push_back({ 0.0f, 0.0f });
                            }
                        }
                        else
                        {
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_faces.push_back(CTextureManager::NoTexture);
                        }
                    }
                    else if (i_edge == 1)
                    {
                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v0] * 2.54f);

                        if (textinfo.texture_id >= 0)
                        {
                            float textC0 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].width;
                            float textC1 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].height;

                            text_coords0.push_back(glm::vec2(textC0, textC1));

                            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
                            {
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC0 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC1 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                            }
                            else
                            {
                                text_coords1.push_back({ 0.0f, 0.0f });
                            }

                            text_faces.push_back(m_textinfos[textinfo.texture_id].id);
                        }
                        else
                        {
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_faces.push_back(CTextureManager::NoTexture);
                        }
                    }
                    else if (face.numedges > i_edge + 1)
                    {
                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[first] * 2.54f);

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v1] * 2.54f);

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v0] * 2.54f);

                        if (textinfo.texture_id >= 0)
                        {
                            float textC0 = computeTextCoord(textinfo.textureVecs[0], m_vertices[first]) / m_textinfos[textinfo.texture_id].width;
                            float textC1 = computeTextCoord(textinfo.textureVecs[1], m_vertices[first]) / m_textinfos[textinfo.texture_id].height;
                            float textC2 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].width;
                            float textC3 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].height;
                            float textC4 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].width;
                            float textC5 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].height;

                            text_coords0.push_back(glm::vec2(textC0, textC1));
                            text_coords0.push_back(glm::vec2(textC2, textC3));
                            text_coords0.push_back(glm::vec2(textC4, textC5));

                            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
                            {
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC0 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC1 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC2 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC3 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC4 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC5 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                            }
                            else
                            {
                                text_coords1.push_back({ 0.0f, 0.0f });
                                text_coords1.push_back({ 0.0f, 0.0f });
                                text_coords1.push_back({ 0.0f, 0.0f });
                            }

                            text_faces.push_back(m_textinfos[textinfo.texture_id].id);
                        }
                        else
                        {
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_faces.push_back(CTextureManager::NoTexture);
                        }
                    }
                }
                else
                {
                    TBSPEdge v = edges[surfedge];

                    if (i_edge == 0)
                    {
                        first = v.v0;

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v0] * 2.54f);

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v1] * 2.54f);

                        if (textinfo.texture_id >= 0)
                        {
                            float textC0 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].width;
                            float textC1 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].height;
                            float textC2 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].width;
                            float textC3 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].height;

                            text_coords0.push_back(glm::vec2(textC0, textC1));
                            text_coords0.push_back(glm::vec2(textC2, textC3));

                            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
                            {
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC0 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC1 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC2 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC3 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                            }
                            else
                            {
                                text_coords1.push_back({ 0.0f, 0.0f });
                                text_coords1.push_back({ 0.0f, 0.0f });
                            }
                        }
                        else
                        {
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_faces.push_back(CTextureManager::NoTexture);
                        }
                    }
                    else if (i_edge == 1)
                    {
                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v1] * 2.54f);

                        if (textinfo.texture_id >= 0)
                        {
                            float textC0 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].width;
                            float textC1 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].height;

                            text_coords0.push_back(glm::vec2(textC0, textC1));

                            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
                            {
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC0 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC1 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                            }
                            else
                            {
                                text_coords1.push_back({ 0.0f, 0.0f });
                            }

                            text_faces.push_back(m_textinfos[textinfo.texture_id].id);
                        }
                        else
                        {
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_faces.push_back(CTextureManager::NoTexture);
                        }
                    }
                    else if (face.numedges > i_edge + 1)
                    {
                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[first] * 2.54f);

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v0] * 2.54f);

                        indices.push_back(vertices.size());
                        vertices.push_back(m_vertices[v.v1] * 2.54f);

                        if (textinfo.texture_id >= 0)
                        {
                            float textC0 = computeTextCoord(textinfo.textureVecs[0], m_vertices[first]) / m_textinfos[textinfo.texture_id].width;
                            float textC1 = computeTextCoord(textinfo.textureVecs[1], m_vertices[first]) / m_textinfos[textinfo.texture_id].height;
                            float textC2 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].width;
                            float textC3 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v0]) / m_textinfos[textinfo.texture_id].height;
                            float textC4 = computeTextCoord(textinfo.textureVecs[0], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].width;
                            float textC5 = computeTextCoord(textinfo.textureVecs[1], m_vertices[v.v1]) / m_textinfos[textinfo.texture_id].height;

                            text_coords0.push_back(glm::vec2(textC0, textC1));
                            text_coords0.push_back(glm::vec2(textC2, textC3));
                            text_coords0.push_back(glm::vec2(textC4, textC5));

                            if (face.lightmap_styles[0] != -1 && face.lightmap_offset >= 0)
                            {
                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC0 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC1 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));

                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC2 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC3 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));

                                text_coords1.push_back(glm::vec2((lightmap_coord_x + (lightmap_size_x * 0.5f + (textC4 * m_textinfos[textinfo.texture_id].width - (min.x + max.x) / 2) / 16)) / LightmapTextureSize,
                                    (lightmap_coord_y + (lightmap_size_y * 0.5f + (textC5 * m_textinfos[textinfo.texture_id].height - (min.y + max.y) / 2) / 16)) / LightmapTextureSize));
                            }
                            else
                            {
                                text_coords1.push_back({ 0.0f, 0.0f });
                                text_coords1.push_back({ 0.0f, 0.0f });
                                text_coords1.push_back({ 0.0f, 0.0f });
                            }

                            text_faces.push_back(m_textinfos[textinfo.texture_id].id);
                        }
                        else
                        {
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords0.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_coords1.push_back({ 0.0f, 0.0f });
                            text_faces.push_back(CTextureManager::NoTexture);
                        }
                    }
                } // if surfedge < 0
            } // for each edge
        } // for each face

        assert(vertices.size() == text_coords0.size());
        assert(vertices.size() == text_coords1.size());

        // Aucun sommet (skybox)
        if (vertices.size() == 0)
        {
            continue;
        }

        // Création du trimesh physique
        std::shared_ptr<CShapeTriMesh> trimesh = std::make_shared<CShapeTriMesh>(vertices, indices);

        // Remplissage du buffer graphique
        std::vector<TVertex3D> data(vertices.size());

        for (unsigned int i = 0; i < vertices.size(); ++i)
        {
            data[i].position = vertices[i];
            //TODO: normales
            data[i].coords[0] = text_coords0[i];
            data[i].coords[1] = text_coords1[i];
        }

        buffer->setData(std::move(data));

        // Textures
        TBufferTextureVector& textinfos = buffer->getTextures();
        textinfos.reserve(text_faces.size());

        // Pour chaque face du modèle
        for (std::vector<TTextureId>::const_iterator it = text_faces.begin(); it != text_faces.end(); ++it)
        {
            TBufferTexture tmp;
            tmp.texture[0] = *it;
            tmp.texture[1] = lightmaps[0].getTextureId();
            tmp.nbr = 3;
            textinfos.push_back(tmp);
        }

        // Création de l'entité pour le monde
        if (i_model == 0)
        {
            gApplication->log("Create entity CMesh for world.", ILogger::Debug);
            CMesh * mesh = new CMesh{ m_map };

            //auto shader = std::make_shared<CShader>();
            std::shared_ptr<CShader> shader;

            if (gRenderer->support(TFunctionnality::BindlessTexture))
            {
                //shader->loadFromFiles("../../../Resources/shaders/lightmaps_bt.vert", "../../../Resources/shaders/lightmaps_bt.frag");
                shader = gShaderManager->loadFromFiles("lightmaps_bt.vert", "lightmaps_bt.frag");
                shader->setBindlessTexture(true);
            }
            else
            {
                //shader->loadFromFiles("../../../Resources/shaders/lightmaps.vert", "../../../Resources/shaders/lightmaps.frag");
                shader = gShaderManager->loadFromFiles("lightmaps.vert", "lightmaps.frag");
            }

            mesh->setShader(shader);

            mesh->setBuffer(buffer);
            mesh->setTriMesh(trimesh);
            //mesh->setBoundingBox(CBoundingBox(model_data.mins , model_data.maxs));

            buffer->update();
            m_models.push_back(TModel{ buffer, trimesh, true });
        }
        else
        {
            m_models.push_back(TModel{ buffer, trimesh });
        }
    } // for each model

    // Mise-à-jour de la texture des lightmaps
    for (int lightmap_index = 0; lightmap_index < NumLightmaps; ++lightmap_index)
    {
        lightmaps[lightmap_index].update();
    }

}


/**
 * Lit un noeud de l'arbre BSP.
 *
 * \param node Numéro du noeud dans le lump node.
 * \return Pointeur sur le noeud.
 ******************************/
#if 0
CBSPTreeNode * CMapLoaderBSPGoldSrc::loadBSPTreeNode(unsigned int node)
{
    CBSPTreeNode * node_ptr = new CBSPTreeNode();

    node_ptr->setPlane(m_planes[m_nodes[node].planenum]);

    if (m_nodes[node].children[0] < 0)
    {
        node_ptr->setChildFront(loadBSPTreeLeaf(- m_nodes[node].children[0] - 1));
    }
    else
    {
        node_ptr->setChildFront(loadBSPTreeNode(m_nodes[node].children[0]));
    }

    if (m_nodes[node].children[1] < 0)
    {
        node_ptr->setChildBack(loadBSPTreeLeaf(-m_nodes[node].children[1] - 1));
    }
    else
    {
        node_ptr->setChildBack(loadBSPTreeNode(m_nodes[node].children[1]));
    }

    return node_ptr;
}
#endif


/**
 * Lit une feuille de l'arbre BSP.
 *
 * \param leaf Numéro de la feuille dans le lump leaf.
 * \return Pointeur sur la feuille.
 ******************************/
#if 0
CBSPTreeLeaf * CMapLoaderBSPGoldSrc::loadBSPTreeLeaf(const unsigned int leaf)
{
    CBSPTreeLeaf * leaf_ptr = new CBSPTreeLeaf();

    switch (m_leafs[leaf].contents)
    {
        default:
        case ContentEmpty:
        case ContentSky:
            leaf_ptr->setType(Empty);
            break;

        case ContentSolid:
        case ContentClip:
            leaf_ptr->setType(Solid);
            break;

        case ContentWater:
        case ContentLava:
            leaf_ptr->setType(Water);
            break;
    }

    return leaf_ptr;
}
#endif

} // Namespace TE
