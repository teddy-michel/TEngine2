/*
Copyright (C) 2008-2020 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include "CModelDataMDLGoldSrc.hpp"

#include "Core/CApplication.hpp"
#include "Core/utils.hpp"
#include "Graphic/CBuffer.hpp"


namespace TE
{

// Variables statiques
unsigned char CModelDataMDLGoldSrc::m_controller[MaxControllers] = { 0 };
float CModelDataMDLGoldSrc::m_adj[MaxControllers] = { 0.0f };
glm::vec3 * CModelDataMDLGoldSrc::m_transform_vert_ptr = nullptr;
glm::vec3 * CModelDataMDLGoldSrc::m_light_values_ptr = nullptr;
glm::vec3 CModelDataMDLGoldSrc::m_transformVertices[MaxVertices] = { { 0.0f, 0.0f, 0.0f } };
glm::vec3 CModelDataMDLGoldSrc::m_lightValues[MaxVertices] = { { 0.0f, 0.0f, 0.0f } };
float CModelDataMDLGoldSrc::m_boneTransforms[MaxBones][3][4] = { { { 0.0f } } };
glm::vec3 CModelDataMDLGoldSrc::m_lightVector = { 0.0f, 0.0f, 0.0f };
int CModelDataMDLGoldSrc::m_ambientLightColor = 0;
float CModelDataMDLGoldSrc::m_shadeLight = 0.0f;
glm::vec3 CModelDataMDLGoldSrc::m_lightColor = { 0.0f, 0.0f, 0.0f };
glm::vec3 CModelDataMDLGoldSrc::m_boneLightVector[MaxBones] = { { 0.0f, 0.0f, 0.0f } };


// Fonctions utilisées pour calculer les transformations
void quaternionMatrix(const glm::quat& quaternion, float (*matrix)[4]);
glm::quat quaternionSlerp(const glm::quat& in1, const glm::quat& in2, float time);
void concatTransforms(const float in1[3][4], const float in2[3][4], float out[3][4]);
void vectorIRotate(const glm::vec3& in1, const float in2[3][4], glm::vec3& out);
void vectorTransform(const glm::vec3& in1, const float in2[3][4], glm::vec3& out);


/**
 * Constructeur par défaut.
 ******************************/

CModelDataMDLGoldSrc::CModelDataMDLGoldSrc() :
    IModelData{},
    m_header{ nullptr },
    m_textureHeader{ nullptr },
    m_fileBuffer{ nullptr }
{
    for (unsigned int i = 0; i < 32; ++i)
    {
        m_animationHeader[i] = nullptr;
    }

    m_blending[0] = m_blending[1] = 0;
}


/**
 * Destructeur.
 ******************************/

CModelDataMDLGoldSrc::~CModelDataMDLGoldSrc()
{
    freeMemory();
}


unsigned int CModelDataMDLGoldSrc::getMemorySize() const
{
    return sizeof(CModelDataMDLGoldSrc);
}


/**
 * Donne le nombre de séquences du modèle.
 *
 * \return Nombre de séquences, ou 0 si le modèle n'est pas chargé.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getNumSequences() const
{
    if (m_header)
        return m_header->num_seq;
    return 0;
}


/**
 * Donne le nombre de skins du modèle.
 *
 * \return Nombre de skins, ou 0 si le modèle n'est pas chargé.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getNumSkins() const
{
    if (m_textureHeader)
        return m_textureHeader->num_skin_families;
    return 0;
}


/**
 * Donne le nombre de groupes du modèle.
 *
 * \return Nombre de groupes, ou 0 si le modèle n'est pas chargé.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getNumGroups() const
{
    unsigned int nbr = 0;

    if (m_header)
    {
        TBodyPart * body_part_ptr = reinterpret_cast<TBodyPart *>(m_fileBuffer + m_header->bodypart_index);

        // On parcourt la liste des bodyparts
        for (unsigned int i = 0 ; i < m_header->num_bodyparts ; ++i, ++body_part_ptr)
        {
            if (body_part_ptr->num_models > nbr)
            {
                nbr = body_part_ptr->num_models;
            }
        }
    }

    return nbr;
}


/**
 * Donne le nombre de hitbox du modèle.
 *
 * \return Nombre de hitbox, ou 0 si le modèle n'est pas chargé.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getNumHitBoxes() const
{
    if (m_header)
        return m_header->num_hitboxes;
    return 0;
}


/**
 * Donne la position des yeux du modèle par rapport à son centre de gravité.
 * Attention, ce paramètre n'a aucune utilité si le modèle ne représente pas
 * un personnage.
 *
 * \return Position des yeux du modèle, ou le vecteur nul si le modèle n'est pas chargé.
 ******************************/

glm::vec3 CModelDataMDLGoldSrc::getEyesPosition() const
{
    if (m_header)
        return m_header->eyeposition * 2.54f; // Conversion foot -> meters
    return { 0.0f, 0.0f, 0.0f };
}


/**
 * Calcule la boite englobante du modèle pour une séquence donnée.
 *
 * \param sequence Numéro de la séquence.
 * \return Boite englobante du modèle.
 ******************************/
/*
CBoundingBox CModelDataMDLGoldSrc::getBoundingBox(unsigned int sequence) const
{
    if (m_header == nullptr || sequence >= m_header->num_seq)
    {
        return CBoundingBox(Origin3F, Origin3F);
    }

    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    return CBoundingBox(seq_desc[sequence].min, seq_desc[sequence].max);
}
*/

/**
 * Indique si une séquence doit être jouée en boucle.
 *
 * \param sequence Numéro de la séquence.
 * \return Booléen, true si le modèle n'est pas chargé ou que le numéro de la
 *         séquence est incorrect.
 ******************************/

bool CModelDataMDLGoldSrc::isSequenceLoop(unsigned int sequence) const
{
    if (m_header == nullptr || sequence >= m_header->num_seq)
    {
        return true;
    }

    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    return (seq_desc[sequence].flags == 1);
}


/**
 * Donne le nombre de frames d'une séquence.
 *
 * \param sequence Numéro de la séquence.
 * \return Nombre de frames, ou 0 si le modèle n'est pas chargé ou que le numéro
 *         de la séquence est incorrect.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getNumFrames(unsigned int sequence) const
{
    if (m_header == nullptr || sequence >= m_header->num_seq)
    {
        return 0;
    }

    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    return seq_desc[sequence].num_frames;
}


/**
 * Donne le nombre de frames par seconde d'une séquence.
 *
 * \param sequence Numéro de la séquence.
 * \return Nombre de frames à afficher par seconde, ou 0 si le modèle n'est pas
 *         chargé ou que le numéro de la séquence est incorrect.
 ******************************/

float CModelDataMDLGoldSrc::getFrameRate(unsigned int sequence) const
{
    if (m_header == nullptr || sequence >= m_header->num_seq)
    {
        return 0.0f;
    }

    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    return seq_desc[sequence].timing;
}


/**
 * Donne le déplacement linéaire du modèle au cours d'une séquence.
 *
 * \param sequence Numéro de la séquence.
 * \return Déplacement du modèle, ou le vecteur nul si le modèle n'est pas
 *         chargé ou qu'il ne se déplace pas.
 ******************************/

glm::vec3 CModelDataMDLGoldSrc::getLinearMovement(unsigned int sequence) const
{
    if (m_header == nullptr || sequence >= m_header->num_seq)
    {
        return { 0.0f, 0.0f, 0.0f };
    }

    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    return seq_desc[sequence].linear_movement * 2.54f; // Conversion foot -> meters
}


/**
 * Donne le nom d'une séquence.
 *
 * \param sequence Numéro de la séquence.
 * \return Nom de la séquence, ou chaine vide si le modèle n'est pas chargé ou
 *         que le numéro de la séquence est incorrect.
 ******************************/

CString CModelDataMDLGoldSrc::getSequenceName(unsigned int sequence) const
{
    if (m_header == nullptr || sequence >= m_header->num_seq)
    {
        return CString();
    }

    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    return seq_desc[sequence].name;
}


/**
 * Cherche le numéro d'une séquence à partir de son nom.
 *
 * \param name Nom de la séquence.
 * \return Numéro de la séquence, ou -1 si elle n'existe pas ou si le modèle
 *         n'est pas chargé.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getSequenceNumber(const CString& name) const
{
    if (m_header == nullptr)
    {
        return static_cast<unsigned int>(-1);
    }

    const TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);

    // On parcourt la liste des séquences
    for (unsigned int i = 0 ; i < m_header->num_seq ; ++i)
    {
        if (name == seq_desc[i].name)
        {
            return i;
        }
    }

    return static_cast<unsigned int>(-1);
}


/**
 * Donne le numéro d'une séquence à partir d'une activité.
 * Si plusieurs séquences sont disponibles pour cette activité, la sélection se
 * fait aléatoirement en tenant compte de l'importance de chaque séquence.
 *
 * \param activity Activité demandée.
 * \return Numéro de la séquence, ou -1 si le modèle n'est pas chargé ou que
 *         l'activité n'est pas disponible.
 *
 * \todo L'importance d'une séquence doit être inférieure à 100, est-ce cohérent ?
 ******************************/

unsigned int CModelDataMDLGoldSrc::getSequenceByActivity(TSequenceActivity activity) const
{
    if (m_header == nullptr)
    {
        return static_cast<unsigned int>(-1);
    }

    const TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer +  m_header->seq_index);
    std::vector<unsigned int> sequences;

    // On parcourt la liste des séquences
    for (unsigned int i = 0 ; i < m_header->num_seq ; ++i)
    {
        if (seq_desc->activity == activity)
        {
            // Si l'importance de l'activité est précisée et que sa valeur est cohérente...
            if (seq_desc->actweight > 0 && seq_desc->actweight < 100)
            {
                // On ajoute le numéro de la séquence au tableau autant de fois que nécessaire
                for (unsigned int j = 0 ; j < seq_desc->actweight ; ++j)
                {
                    sequences.push_back(i);
                }
            }
            // On ajoute le numéro de la séquence au tableau
            else
            {
                sequences.push_back(i);
            }
        }
    }

    if (sequences.size() > 0)
    {
        // On prend un nombre aléatoire pour sélectionner la séquence
        unsigned int rd = RandInt(0, sequences.size() - 1);
        return sequences[rd];
    }

    return static_cast<unsigned int>(-1);
}


/**
 * Donne le numéro d'un attachement à partir de son nom.
 *
 * \param name Nom de l'attachement.
 * \return Numéro de l'attachement, ou -1 s'il n'a pas été trouvé.
 ******************************/

unsigned int CModelDataMDLGoldSrc::getAttachmentByName(const CString& name) const
{
    if (m_header == nullptr)
    {
        return static_cast<unsigned int>(-1);
    }

    TAttachment * attachment = reinterpret_cast<TAttachment *>(m_fileBuffer + m_header->attachment_index);

    // On parcourt la liste des attachments
    for (unsigned int i = 0; i < m_header->num_attachments; ++i, ++attachment)
    {
        if (name == attachment->name)
        {
            return i;
        }
    }

    return static_cast<unsigned int>(-1);
}


/**
 * Donne la position d'un attachement.
 *
 * \param attachment Numéro de l'attachement.
 * \return Position de l'attachement.
 *
 * \todo Implémentation.
 * \todo Ajouter un argument TModelParams ?
 ******************************/

glm::vec3 CModelDataMDLGoldSrc::getAttachmentPosition(unsigned int attachment) const
{
    if (m_header == nullptr || attachment >= m_header->num_attachments)
    {
        return { 0.0f, 0.0f, 0.0f };
    }

    TAttachment * a = reinterpret_cast<TAttachment *>(m_fileBuffer + m_header->attachment_index) + attachment;

    return a->org * 2.54f; // Conversion foot -> meters
    //return Origin3F;
}


/**
 * Récupère les valeurs minimale et maximale d'un contrôleur.
 * Si le numéro du contrôleur est incorrect ou que le modèle n'est pas chargé,
 * les valeurs récupérées seront nulles.
 *
 * \param index Numéro du contrôleur.
 * \param start Pointeur sur un flottant qui contiendra la valeur minimale du contrôleur.
 * \param end   Pointeur sur un flottant qui contiendra la valeur maximale du contrôleur.
 ******************************/

void CModelDataMDLGoldSrc::getControllerRange(unsigned int index, float * start, float * end) const
{
    assert(start != nullptr);
    assert(end  != nullptr);

    *start = *end = 0.0f;

    if (m_header == nullptr)
        return;

    TBoneController * controller = reinterpret_cast<TBoneController *>(m_fileBuffer + m_header->bone_controller_index);

    // On parcourt la liste des contrôleurs pour trouver le bon
    for (unsigned int i = 0 ; i < m_header->num_bone_controllers ; ++i, ++controller)
    {
        if (controller->index == index)
        {
            *start = controller->start;
            *end = controller->end;
        }
    }
}


/**
 * Met-à-jour les paramètres d'un modèle.
 *
 * \param params Paramètres du modèle.
 * \param timeP  Durée écoulée depuis la dernière mise-à-jour en secondes.
 *
 * \todo Remplacer le calcul de time_before_next_frame par le calcul direct de l'interpolation.
 ******************************/

void CModelDataMDLGoldSrc::updateParams(CModel::TModelParams& params, float timeP) const
{
    unsigned int time = static_cast<unsigned int>(timeP * 1000);

    bool loop = (params.mode == CModel::Loop || (params.mode == CModel::Auto && isSequenceLoop(params.sequence)));
    bool next = (params.mode == CModel::Next || (params.mode == CModel::Auto && !loop));

    unsigned int nbr_frames = getNumFrames(params.sequence);
    float frameRate = getFrameRate(params.sequence);
    assert(frameRate > 0.0f);
    unsigned int frame_time = static_cast<unsigned int>(1000.0f / frameRate);
    float time_before_next_frame2 = (1.0f - params.interpolation) * frame_time;
    unsigned int time_before_next_frame = static_cast<unsigned int>(time_before_next_frame2);

    // Si la séquence doit être jouée en boucle, ou que l'on n'a pas fini la séquence
    if (loop || params.frame < nbr_frames - 1)
    {
        // On met à jour le compteur de temps
        if (time_before_next_frame > time)
        {
            time_before_next_frame -= time;
        }
        else
        {
            unsigned int nbr_sup_frame = 1;

            // On calcule de combien de frames on a avancé
            if (time > frame_time)
            {
                nbr_sup_frame += (time - 1) / frame_time;
                time -= (nbr_sup_frame - 1) * frame_time;
            }

            params.frame += nbr_sup_frame;

            // On a dépassé la dernière frame
            if (params.frame > nbr_frames - 1)
            {
                // Lecture en boucle
                if (loop)
                {
                    params.frame -= static_cast<unsigned int>(params.frame / (nbr_frames - 1)) * (nbr_frames - 1);
                    time_before_next_frame += frame_time - time;
                }
                // Lecture de la séquence suivante
                else if (next)
                {
                    // On recherche le numéro de la séquence suivante
                    if (m_header != nullptr)
                    {
                        TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index);
                        params.sequence = seq_desc[params.sequence].next_seq;
                    }
                    else
                    {
                        params.sequence = 0;
                    }

                    frame_time = static_cast<unsigned int>(1000.0f / getFrameRate(params.sequence));
                    nbr_frames = getNumFrames(params.sequence);

                    params.frame = 0;
                    time_before_next_frame = frame_time;
                }
                // On reste sur la dernière frame
                else
                {
                    params.frame = nbr_frames - 1;
                    time_before_next_frame = frame_time;
                }
            }
            else if (!loop && params.frame == nbr_frames - 1)
            {
                time_before_next_frame = frame_time;
            }
            else
            {
                time_before_next_frame += frame_time - time;
            }
        }
    }

    params.interpolation = 1.0f - static_cast<float>(time_before_next_frame) / static_cast<float>(frame_time);
}


/**
 * Modifie le buffer graphique pour l'affichage du modèle.
 *
 * \param buffer Pointeur sur le buffer graphique à mettre à jour.
 * \param params Paramètres du modèle.
 ******************************/

void CModelDataMDLGoldSrc::updateBuffer(CBuffer<TVertex3D> * buffer, CModel::TModelParams& params)
{
    // Buffer invalide ou fichier non chargé
    if (buffer == nullptr || m_header == nullptr)
    {
        return;
    }

    buffer->clear();

    // Numéro de séquence invalide
    if (params.sequence >= m_header->num_seq)
    {
        params.sequence = 0;
    }

    m_transform_vert_ptr = &m_transformVertices[0];
    m_light_values_ptr = &m_lightValues[0];

    // Le modèle ne contient aucun groupe
    if (m_header->num_bodyparts == 0)
    {
        return;
    }

    // Controlleurs
    for (unsigned int i = 0 ; i < CModel::TModelParams::NumControllers ; ++i)
    {
        params.controllers[i] = setController(i, params.controllers[i]);
    }

    // Blending
    params.blending[0] = setBlending(0, params.blending[0], params.sequence);
    params.blending[1] = setBlending(1, params.blending[1], params.sequence);

    setupBones(params.sequence, params.frame, params.interpolation);
    setupLighting();

    std::vector<TVertex3D> vertices(m_header->num_bodyparts * 100);

    // Affichage de chaque groupe
    for (unsigned int i = 0 ; i < m_header->num_bodyparts ; ++i)
    {
        TModel * model = setupModel(i, params.group);
        updateSubModel(buffer, model, vertices, params.skin);
    }

    buffer->setData(std::move(vertices));
    buffer->update();
}


/**
 * Charge le modèle depuis un fichier.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataMDLGoldSrc::loadFromFile(const CString& fileName)
{
    gApplication->log(CString::fromUTF8("Chargement du modèle %1").arg(fileName));

    // Suppression des anciennces données
    freeMemory();

    // Chargement du modèle
    m_header = loadModel(fileName.toCharArray());
    m_fileBuffer = reinterpret_cast<unsigned char *>(m_header);

    if (m_header == nullptr)
    {
        return false;
    }

    m_fileName = fileName;
    m_name = fileName;

    unsigned int length = m_fileName.getSize() - 4;

    if (m_header->num_textures == 0)
    {
        m_textureHeader = loadModel(fileName.subString(0, length) + "T.mdl");
    }
    else
    {
        m_textureHeader = m_header;
    }

    if (m_header->num_seq_groups > 1)
    {
        char * seq_groupname = new char[length + 7];

        for (unsigned int i = 1; i < m_header->num_seq_groups; ++i)
        {
            strncpy(seq_groupname, fileName.toCharArray(), length);
            sprintf(&seq_groupname[length], "%02d.mdl", i);

            m_animationHeader[i] = loadDemandSequences(seq_groupname);
        }

        delete[] seq_groupname;
    }

    return true;
}


/**
 * Enregistre le modèle dans un fichier.
 *
 * \todo Vérifier la taille du buffer avant d'écrire le fichier.
 *
 * \param fileName Nom du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataMDLGoldSrc::saveToFile(const CString& fileName) const
{
    std::ofstream file(fileName.toCharArray(), std::ios::out | std::ios::binary);

    if (!file)
    {
        gApplication->log(CString("CModelDataMDLGoldSrc::saveToFile : impossible d'ouvrir le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    // Écriture du contenu du fichier
    file.write(reinterpret_cast<char *>(m_fileBuffer), sizeof(m_fileBuffer));

    if (file.fail())
    {
        file.close();
        return false;
    }

    file.close();
    return true;
}


/**
 * Indique si le fichier contient un modèle pouvant être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CModelDataMDLGoldSrc::isCorrectFormat(const CString& fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    // Chargement de l'en-tête
    THeader header;

    // Le fichier ne peut pas être lu
    if (!file.read(reinterpret_cast<char *>(&header), sizeof(header)))
    {
        file.close();
        return false;
    }

    file.close();

    // Vérification du numéro magique et du numéro de version
    return ((header.ident == ('I' + ('D'<<8) + ('S'<<16) + ('T'<<24)) && header.version == 10) ||
             header.ident == ('I' + ('D'<<8) + ('S'<<16) + ('Q'<<24)));
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le modèle ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
IModelData * CModelDataMDLGoldSrc::createInstance(const CString& fileName)
#else
CModelDataMDLGoldSrc * CModelDataMDLGoldSrc::createInstance(const CString& fileName)
#endif
{
    return (CModelDataMDLGoldSrc::isCorrectFormat(fileName) ? new CModelDataMDLGoldSrc() : nullptr);
}


/**
 * Charge un modèle MDL depuis un fichier.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une zone mémoire contenant le fichier.
 ******************************/

CModelDataMDLGoldSrc::THeader * CModelDataMDLGoldSrc::loadModel(const CString& fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        gApplication->log(CString("CModelDataMDLGoldSrc::loadModel : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return nullptr;
    }

    file.seekg(0, std::ios_base::end);
    std::size_t fileSize = file.tellg();

    if (fileSize < sizeof(THeader))
    {
        file.close();
        gApplication->log(CString("CModelDataMDLGoldSrc::loadModel : le fichier %1 a une taille trop petite").arg(fileName), ILogger::Error);
        return nullptr;
    }

    file.seekg(0, std::ios_base::beg);

    // Chargement du fichier dans un buffer
    unsigned char * buffer = new unsigned char[fileSize];

    if (!file.read(reinterpret_cast<char *>(buffer), fileSize))
    {
        file.close();
        gApplication->log(CString("CModelDataMDLGoldSrc::loadModel : impossible de lire le contenu du fichier %1").arg(fileName), ILogger::Error);
        delete[] buffer;
        return nullptr;
    }

    file.close();

    THeader * header = reinterpret_cast<THeader *>(buffer);

    // Vérification du numéro magique et du numéro de version
    if ((header->ident != ( 'I' + ('D'<<8) + ('S'<<16) + ('T'<<24)) || header->version != 10) &&
         header->ident != ( 'I' + ('D'<<8) + ('S'<<16) + ('Q'<<24)))
    {
        gApplication->log(CString("CModelDataMDLGoldSrc::loadModel : le fichier %1 n'est pas valide").arg(fileName), ILogger::Error);
        return nullptr;
    }

    if (header->texture_index != 0)
    {
        TTexture * texture = reinterpret_cast<TTexture *>(buffer + header->texture_index);

        for (unsigned int i = 0; i < header->num_textures; ++i)
        {
            uploadTexture(&texture[i], buffer + texture[i].index, buffer + texture[i].width * texture[i].height + texture[i].index);
        }
    }

    return header;
}


/**
 * Charge une séquence depuis un fichier.
 *
 * \param fileName Adresse du fichier contenant la séquence.
 * \return Pointeur sur une zone mémoire contenant le fichier.
 ******************************/

CModelDataMDLGoldSrc::TSeqHeader * CModelDataMDLGoldSrc::loadDemandSequences(const CString& fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        gApplication->log(CString("CModelDataMDLGoldSrc::loadDemandSequences : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return nullptr;
    }

    file.seekg(0, std::ios_base::end);
    std::size_t size = file.tellg();

    if (size < sizeof(TSeqHeader))
    {
        file.close();
        gApplication->log(CString("CModelDataMDLGoldSrc::loadDemandSequences : le fichier %1 a une taille trop petite").arg(fileName), ILogger::Error);
        return nullptr;
    }

    file.seekg(0, std::ios_base::beg);

    // Chargement du fichier dans un buffer
    unsigned char * buffer = new unsigned char[size];

    if (!file.read(reinterpret_cast<char *>(buffer), size))
    {
        file.close();
        gApplication->log(CString("CModelDataMDLGoldSrc::loadDemandSequences : impossible de lire le contenu du fichier %1").arg(fileName), ILogger::Error);
        delete[] buffer;
        return nullptr;
    }

    file.close();

    return reinterpret_cast<TSeqHeader *>(buffer);
}


/**
 * Calcule le quaternion pour la rotation d'un os.
 *
 * \param frame  Numéro de la frame.
 * \param interp Interpolation avec la frame suivante (entre 0 et 1).
 * \param bone   Pointeur sur l'os.
 * \param anim   Pointeur sur l'animation (?).
 * \return Quaternion représentant la rotation de l'os.
 ******************************/

glm::quat CModelDataMDLGoldSrc::calcBoneQuaternion(unsigned short frame, float interp, const TBone * bone, TAnimation * anim)
{
    assert(bone != nullptr);
    assert(anim != nullptr);

    glm::vec3 angle1;
    glm::vec3 angle2;

    for (int i = 0 ; i < 3 ; ++i)
    {
        if (anim->offset[i + 3] == 0)
        {
            angle2[i] = angle1[i] = bone->value[i + 3];
        }
        else
        {
            TAnimFrame * anim_value = reinterpret_cast<TAnimFrame *>(reinterpret_cast<unsigned char*>(anim) + anim->offset[i + 3]);

            unsigned int index = frame;

            while (anim_value->v.total <= index)
            {
                index -= anim_value->v.total;
                anim_value += anim_value->v.valid + 1;
            }

            if (anim_value->v.valid > index)
            {
                angle1[i] = anim_value[index + 1].value;

                if (anim_value->v.valid > index + 1)
                {
                    angle2[i] = anim_value[index + 2].value;
                }
                else
                {
                    if (anim_value->v.total > index + 1)
                    {
                        angle2[i] = angle1[i];
                    }
                    else
                    {
                        angle2[i] = anim_value[anim_value->v.valid + 2].value;
                    }
                }
            }
            else
            {
                angle1[i] = anim_value[anim_value->v.valid].value;

                if ( anim_value->v.total > index + 1 )
                {
                    angle2[i] = angle1[i];
                }
                else
                {
                    angle2[i] = anim_value[anim_value->v.valid + 2].value;
                }
            }

            angle1[i] = bone->value[i + 3] + angle1[i] * bone->scale[i + 3];
            angle2[i] = bone->value[i + 3] + angle2[i] * bone->scale[i + 3];
        }

        if (bone->bone_controller[i + 3] != -1)
        {
            angle1[i] += m_adj[bone->bone_controller[i + 3]];
            angle2[i] += m_adj[bone->bone_controller[i + 3]];
        }
    }

    if (angle1 != angle2)
    {
        glm::quat q1{ angle1 }; // TODO: attention x / y /z
        glm::quat q2{ angle2 }; // TODO: attention x / y /z
        return quaternionSlerp(q1, q2, interp);
    }
    else
    {
        glm::quat q{ angle1 }; // TODO: attention x / y /z
        return q;
    }
}


/**
 * Calcule la position d'un os.
 *
 * \param frame  Numéro de la frame.
 * \param interp Interpolation avec la frame suivante (entre 0 et 1).
 * \param bone   Pointeur sur l'os.
 * \param anim   Pointeur sur l'animation (?).
 * \return Position de l'os.
 ******************************/

glm::vec3 CModelDataMDLGoldSrc::calcBonePosition(const unsigned short frame, const float interp, const TBone * bone, TAnimation * anim)
{
    assert(bone != nullptr);
    assert(anim != nullptr);

    glm::vec3 pos(bone->value[0], bone->value[1], bone->value[2]);

    for (int i = 0; i < 3; ++i)
    {
        if (anim->offset[i] != 0)
        {
            TAnimFrame * anim_value = reinterpret_cast<TAnimFrame *>(reinterpret_cast<unsigned char *>(anim) + anim->offset[i]);

            unsigned int index = frame;

            while (anim_value->v.total <= index)
            {
                index -= anim_value->v.total;
                anim_value += anim_value->v.valid + 1;
            }

            if (anim_value->v.valid > index)
            {
                if (anim_value->v.valid > index + 1)
                {
                    pos[i] += (anim_value[index + 1].value * (1.0f - interp) + interp * anim_value[index + 2].value) * bone->scale[i];
                }
                else
                {
                    pos[i] += anim_value[index + 1].value * bone->scale[i];
                }
            }
            else if (anim_value->v.total <= index + 1)
            {
                pos[i] += (anim_value[anim_value->v.valid].value * (1.0f - interp) + interp * anim_value[anim_value->v.valid + 2].value) * bone->scale[i];
            }
            else
            {
                pos[i] += anim_value[anim_value->v.valid].value * bone->scale[i];
            }
        }

        if ( bone->bone_controller[i] != -1 )
        {
            pos[i] += m_adj[bone->bone_controller[i]];
        }
    }

    return pos;
}


void CModelDataMDLGoldSrc::calcBoneAdj()
{
    if (m_header == nullptr)
        return;

    float value;

    TBoneController * bone_controller = reinterpret_cast<TBoneController *>(m_fileBuffer + m_header->bone_controller_index);

    for (unsigned int j = 0; j < m_header->num_bone_controllers; ++j)
    {
        int i = bone_controller[j].index;

        // Mouth
        if (i == 4)
        {
            value = m_controller[i] / 64.0f;
            if (value > 1.0f) value = 1.0f;
            value = (1.0f - value) * bone_controller[j].start + value * bone_controller[j].end;
        }
        // Check for 360% wrapping
        else if (bone_controller[j].type & TransitionRLoop)
        {
            value = m_controller[i] * (360.0f / 256.0f) + bone_controller[j].start;
        }
        else
        {
            value = m_controller[i] / 255.0f;
            if (value < 0.0f) value = 0.0f;
            else if (value > 1.0f) value = 1.0f;
            value = (1.0f - value) * bone_controller[j].start + value * bone_controller[j].end;
        }

        switch (bone_controller[j].type & TransitionTypes)
        {
            case TransitionXR:
            case TransitionYR:
            case TransitionZR:
                m_adj[j] = value * (Pi / 180.0f);
                break;

            case TransitionX:
            case TransitionY:
            case TransitionZ:
                m_adj[j] = value;
                break;

            default:
                m_adj[j] = 0.0f;
                break;
        }
    }
}


/**
 * Calcule une rotation (?).
 *
 * \param pos         Tableau de vecteurs (?).
 * \param q           Tableau de quaternions (?).
 * \param description Description de la séquence.
 * \param anim        Pointeur sur le tableau des animations.
 * \param frame       Numéro de la frame.
 * \param interp      Interpolation avec la frame suivante (entre 0 et 1).
 *
 * \todo Transformer les arguments en style C++ (std::vector).
 ******************************/

void CModelDataMDLGoldSrc::calcRotations(glm::vec3 * pos, glm::quat * q, const TSeqDescription * description, TAnimation * anim, unsigned short frame , float interp)
{
    assert(pos != nullptr);
    assert(q != nullptr);
    assert(description != nullptr);
    assert(anim != nullptr);

    if (m_header == nullptr)
        return;

    calcBoneAdj();

    const TBone * bone = reinterpret_cast<TBone *>(m_fileBuffer + m_header->bone_index);

    for (unsigned int i = 0; i < m_header->num_bones; ++i, ++bone, ++anim)
    {
        q[i] = calcBoneQuaternion(frame, interp, bone, anim);
        pos[i] = calcBonePosition(frame, interp, bone, anim);
    }

    if (description->motion_type & TransitionX)
    {
        pos[description->motion_bone].x = 0.0f;
    }

    if (description->motion_type & TransitionY)
    {
        pos[description->motion_bone].y = 0.0f;
    }

    if (description->motion_type & TransitionZ)
    {
        pos[description->motion_bone].z = 0.0f;
    }
}


/**
 * Donne un pointeur sur un tableau d'animations correspondant à une séquence.
 *
 * \param description Description de la séquence à utiliser.
 * \return Pointeur sur le tableau des animations de la séquence.
 ******************************/

CModelDataMDLGoldSrc::TAnimation * CModelDataMDLGoldSrc::getAnim(TSeqDescription * description)
{
    assert(description != nullptr);

    if (m_header == nullptr)
        return nullptr;

    TSeqGroup * pseqgroup = reinterpret_cast<TSeqGroup *>(m_fileBuffer + m_header->seq_group_index) + description->seq_group;

    if (description->seq_group == 0)
    {
        return reinterpret_cast<TAnimation *>(m_fileBuffer + pseqgroup->data + description->anim_offset);
    }

    return reinterpret_cast<TAnimation *>( reinterpret_cast<unsigned char *>(m_animationHeader[description->seq_group]) + description->anim_offset);
}


/**
 * ?
 *
 * \param q1    ?
 * \param pos1  ?
 * \param q2    ?
 * \param pos2  ?
 * \param value ? (entre 0 et 1).
 * \return ?
 ******************************/

void CModelDataMDLGoldSrc::slerpBones(glm::quat q1[], glm::vec3 pos1[], glm::quat q2[], const glm::vec3 pos2[], float value)
{
    assert(value >= 0.0f && value <= 1.0f);

    if (m_header == nullptr)
        return;

    const float inverse = 1.0f - value;

    for (unsigned int i = 0 ; i < m_header->num_bones ; ++i)
    {
        q1[i] = quaternionSlerp(q1[i], q2[i], value);

        pos1[i] *= inverse;
        pos1[i] += pos2[i] * value;
    }
}


/**
 * Calcule la position des os.
 *
 * \param sequence Numéro de la séquence à utiliser.
 * \param frame    Numéro de la frame.
 * \param interp   Interpolation avec la frame suivante (entre 0 et 1).
 *
 * \todo Les variables statiques prennent beaucoup de place inutile :
 *       8 * 128 * 3 * 4 = 12288 octets. Et ces données doivent-elles être
 *       partagées entre plusieurs modèles ?
 * \todo Convertir en C++ (remplacer memcpy).
 */

void CModelDataMDLGoldSrc::setupBones(unsigned short sequence, unsigned short frame, float interp)
{
    if (m_header == nullptr)
        return;

    static glm::vec3 pos1[MaxBones];
    static glm::quat q1[MaxBones];
    static glm::vec3 pos2[MaxBones];
    static glm::quat q2[MaxBones];
    static glm::vec3 pos3[MaxBones];
    static glm::quat q3[MaxBones];
    static glm::vec3 pos4[MaxBones];
    static glm::quat q4[MaxBones];

    if (sequence >= m_header->num_seq)
        sequence = 0;

    // Ce pointeur est invalide !
    TSeqDescription * seq_desc = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index) + sequence;
    TAnimation * anim = getAnim(seq_desc);

    if (anim == nullptr)
        return;

    // Si on est dans la dernière frame, on ne tient pas compte de l'interpolation
    if (frame == getNumFrames(sequence) - 1)
        interp = 0.0f;

    calcRotations(pos1, q1, seq_desc, anim, frame, interp);

    if (seq_desc->num_blends > 1)
    {
        anim += m_header->num_bones;
        calcRotations(pos2, q2, seq_desc, anim, frame, interp);

        float value = static_cast<float>(m_blending[0]) * (1.0f / 255.0f);
        slerpBones(q1, pos1, q2, pos2, value);

        if (seq_desc->num_blends == 4)
        {
            anim += m_header->num_bones;
            calcRotations(pos3, q3, seq_desc, anim, frame, interp);

            anim += m_header->num_bones;
            calcRotations(pos4, q4, seq_desc, anim, frame, interp);

            value = static_cast<float>(m_blending[0]) * (1.0f / 255.0f);
            slerpBones(q3, pos3, q4, pos4, value);

            value = static_cast<float>(m_blending[1]) * (1.0f / 255.0f);
            slerpBones(q1, pos1, q3, pos3, value);
        }
    }

    TBone * bones = reinterpret_cast<TBone *>(m_fileBuffer + m_header->bone_index);

    for (unsigned int i = 0; i < m_header->num_bones; ++i)
    {
        float matrix[3][4];

        quaternionMatrix(q1[i], matrix);
        //TMatrix3F m = q1[i].toRotationMatrix();
        //for (int a1 = 0 ; a1 < 3 ; ++a1) for (int a2 = 0 ; a2 < 3 ; ++a2) matrix[a1][a2] = m_fdg1sfdgsd4f5(a1, a2);

        matrix[0][3] = pos1[i].x;
        matrix[1][3] = pos1[i].y;
        matrix[2][3] = pos1[i].z;

        if (bones[i].parent == -1)
        {
            memcpy(m_boneTransforms[i], matrix, sizeof(float) * 12);
        }
        // Il faut que l'os parent soit déclaré avant cet os.
        else
        {
            concatTransforms(m_boneTransforms[bones[i].parent], matrix, m_boneTransforms[i]);
        }
    }
}


float CModelDataMDLGoldSrc::lighting(unsigned int bone, int flags, const glm::vec3& normal)
{
    float illumination = static_cast<float>(m_ambientLightColor);

    if (flags & LightFlatShade)
    {
        illumination += m_shadeLight * 0.8f;
    }
    else
    {
        float cosine = glm::dot(normal, m_boneLightVector[bone]);

        if (cosine > 1.0f)
        {
            cosine = 1.0f;
        }

        illumination += m_shadeLight;
        cosine = (cosine + 0.5f) / 1.5f;

        if (cosine > 0.0f)
            illumination -= m_shadeLight * cosine;

        if (illumination < 0.0f)
            illumination = 0.0f;
    }

    if (illumination > 255.0f)
        illumination = 255.0f ;

    return illumination / 255.0f;
}


/// Initialise la lumière.
void CModelDataMDLGoldSrc::setupLighting()
{
    if (m_header == nullptr)
    {
        return;
    }

    m_ambientLightColor = 32;
    m_shadeLight = 192.0f;

    m_lightVector.x = 0.0f;
    m_lightVector.y = 0.0f;
    m_lightVector.z = -1.0f;

    m_lightColor.x = 1.0f;
    m_lightColor.y = 1.0f;
    m_lightColor.z = 1.0f;

    for (unsigned int i = 0; i < m_header->num_bones; ++i)
    {
        vectorIRotate(m_lightVector, m_boneTransforms[i], m_boneLightVector[i]);
    }
}


/**
 * Initialise le sous-modèle à afficher.
 *
 * \param num   Numéro du sous-modèle à afficher.
 * \param group Numéro du groupe à utiliser (0 par défaut).
 * \return Pointeur sur le sous-modèle.
 ******************************/

CModelDataMDLGoldSrc::TModel * CModelDataMDLGoldSrc::setupModel(unsigned short num, unsigned short group)
{
    if (m_header == nullptr || num >= m_header->num_bodyparts)
    {
        return nullptr;
    }

    TBodyPart * body_part_ptr = reinterpret_cast<TBodyPart *>(m_fileBuffer + m_header->bodypart_index) + num;

    unsigned short index = (group / body_part_ptr->base) % body_part_ptr->num_models;

    return reinterpret_cast<TModel *>(m_fileBuffer + body_part_ptr->model_offset) + index;
}


/**
 * Dessine un sous-modèle.
 *
 * \param buffer Pointeur sur le buffer graphique à utiliser.
 * \param model  Pointeur sur le sous-modèle à afficher.
 * \param skin   Numéro du skin à utiliser.
 *
 * \todo Calculer les normales.
 ******************************/

void CModelDataMDLGoldSrc::updateSubModel(CBuffer<TVertex3D> * buffer, TModel * model, std::vector<TVertex3D>& vertices, unsigned short skin)
{
    assert(buffer != nullptr);
    assert(model != nullptr);

    if (m_header == nullptr)
    {
        return;
    }

    unsigned char * bone_vertex_ptr = (m_fileBuffer + model->vertex_info_offset);
    unsigned char * bone_normal_ptr = (m_fileBuffer + model->normal_info_offset);

    TTexture * texture = reinterpret_cast<TTexture *>(reinterpret_cast<unsigned char *>(m_textureHeader) + m_textureHeader->texture_index);
    short * skin_ref = reinterpret_cast<short *>(reinterpret_cast<unsigned char *>(m_textureHeader) + m_textureHeader->skin_index);

    glm::vec3 * vertices_ptr = reinterpret_cast<glm::vec3 *>(m_fileBuffer + model->vertex_offset);
    glm::vec3 * normales_ptr = reinterpret_cast<glm::vec3 *>(m_fileBuffer + model->normal_offset);

    if (skin != 0 && skin < m_textureHeader->num_skin_families)
    {
        skin_ref += (skin * m_textureHeader->num_skin_ref);
    }

    for (unsigned int i = 0; i < model->num_vertices; ++i)
    {
        vectorTransform(vertices_ptr[i], m_boneTransforms[bone_vertex_ptr[i]], m_transform_vert_ptr[i]);
    }

    glm::vec3 * light_value = m_light_values_ptr;

    // Pour chaque volume du sous-modèle
    for (unsigned int i = 0; i < model->num_meshes; ++i)
    {
        TMesh * mesh_ptr = reinterpret_cast<TMesh *>(m_fileBuffer + model->mesh_offset) + i;
        const int flags = texture[skin_ref[mesh_ptr->skin_reference]].flags;

        for (unsigned int j = 0; j < mesh_ptr->num_normals; ++j, ++light_value, ++normales_ptr, ++bone_normal_ptr)
        {
            const float temp = lighting(*bone_normal_ptr, flags, *normales_ptr);
            *light_value = temp * m_lightColor;
        }
    }

    std::vector<unsigned int>& buf_indices = buffer->getIndices();
    TBufferTextureVector& buf_textures = buffer->getTextures();

    // Pour chaque volume du sous-modèle
    for (unsigned int i = 0; i < model->num_meshes; ++i)
    {
        TMesh * mesh_ptr = reinterpret_cast<TMesh *>(m_fileBuffer + model->mesh_offset) + i;
        short * triangles = reinterpret_cast<short *>(m_fileBuffer + mesh_ptr->triangle_offset);

        float u = 1.0f / static_cast<float>(texture[ skin_ref[mesh_ptr->skin_reference] ].width);
        float v = 1.0f / static_cast<float>(texture[ skin_ref[mesh_ptr->skin_reference] ].height);

        int num_triangles;
        unsigned int indice_tri1 = 0; // Indice du sommet servant pour les autres triangles
        unsigned int indice_tri2 = 0;
        unsigned int nbr_tri = 0;
        bool tri_strip = false;

        while ((num_triangles = *(triangles++)))
        {
            if (num_triangles < 0)
            {
                num_triangles = -num_triangles;
                indice_tri1 = vertices.size();
                tri_strip = false;
            }
            else
            {
                tri_strip = true;

            }

            for (unsigned int j = 0; num_triangles > 0; --num_triangles, ++j, triangles += 4)
            {
                glm::vec4 color = { m_light_values_ptr[triangles[1]], 1.0f };
                glm::vec3 vertex = m_transform_vert_ptr[triangles[0]] * 2.54f; // Conversion foot -> meters

                if (j >= 2)
                    ++nbr_tri;

                if (j > 2)
                {
                    buf_indices.push_back(indice_tri1);
                    buf_indices.push_back(indice_tri2);
                }

                unsigned int indice_tmp = vertices.size();

                if (tri_strip)
                {
                    if (j == 2)
                    {
                        indice_tri2 = indice_tmp - 1;
                        indice_tri1 = indice_tmp;
                    }
                    else if (j > 2)
                    {
                        unsigned int tmp = indice_tri2 + 1;
                        indice_tri2 = indice_tri1 + 1;
                        indice_tri1 = tmp;

                    }
                }
                else
                {
                    indice_tri2 = indice_tmp;
                }

                buf_indices.push_back(indice_tmp);
                // TODO: compute normal vector
                vertices.push_back(TVertex3D{ vertex, 0, glm::vec3{}, color, {{triangles[2] * u, triangles[3] * v}} });
            }
        }

        // Ajout de la texture au buffer
        TBufferTexture t;
        t.texture[0] = texture[ skin_ref[mesh_ptr->skin_reference] ].index;
        t.nbr = nbr_tri * 3;
        buf_textures.push_back(t);
    }
}


/**
 * Crée une texture.
 *
 * \param texture Pointeur sur la texture à charger.
 * \param data    Pixels de l'image.
 * \param palette Palette de couleurs.
 *
 * \todo Ne plus se préoccuper des dimensions de la texture (?).
 ******************************/

void CModelDataMDLGoldSrc::uploadTexture(TTexture * texture, unsigned char * data, unsigned char * palette)
{
    assert(texture != nullptr);
    assert(data != nullptr);
    assert(palette != nullptr);

    unsigned int row1[256];
    unsigned int row2[256];

    unsigned int column1[256];
    unsigned int column2[256];


    // On transforme les dimensions de l'image en puissances de 2.
    // Glu peut le faire pour nous !
/*
    unsigned int out_width;
    for (out_width = 1; out_width < texture->width; out_width <<= 1);

    if (out_width > 256)
        out_width = 256;

    unsigned int out_height;
    for (out_height = 1; out_height < texture->height; out_height <<= 1);

    if (out_height > 256)
        out_height = 256;
*/
    unsigned int out_width = smallestPowerOfTwoGreaterOrEqual(texture->width);
    if ( out_width > 256 ) out_width = 256;
    else if ( out_width == 0 ) out_width = 1;

    unsigned int out_height = smallestPowerOfTwoGreaterOrEqual(texture->height);
    if ( out_height > 256 ) out_height = 256;
    else if ( out_height == 0 ) out_height = 1;


    CColor * textureBuffer = new CColor[out_width * out_height];

    CColor * bufferPtr = textureBuffer;

    for (unsigned int i = 0; i < out_width; ++i)
    {
        column1[i] = static_cast<unsigned int>((static_cast<float>(i) + 0.25f) * (static_cast<float>(texture->width) / static_cast<float>(out_width)));
        column2[i] = static_cast<unsigned int>((static_cast<float>(i) + 0.75f) * (static_cast<float>(texture->width) / static_cast<float>(out_width)));
    }

    for (unsigned int i = 0; i < out_height; ++i)
    {
        row1[i] = static_cast<unsigned int>((static_cast<float>(i) + 0.25f) * (static_cast<float>(texture->height) / static_cast<float>(out_height))) * texture->width;
        row2[i] = static_cast<unsigned int>((static_cast<float>(i) + 0.75f) * (static_cast<float>(texture->height) / static_cast<float>(out_height))) * texture->width;
    }

    for (unsigned int i = 0; i < out_height; ++i)
    {
        for (unsigned int j = 0; j < out_width; ++j, ++bufferPtr)
        {
            unsigned char * pixel1 = &palette[data[row1[i] + column1[j]] * 3];
            unsigned char * pixel2 = &palette[data[row1[i] + column2[j]] * 3];
            unsigned char * pixel3 = &palette[data[row2[i] + column1[j]] * 3];
            unsigned char * pixel4 = &palette[data[row2[i] + column2[j]] * 3];

            *bufferPtr = CColor((pixel1[0] + pixel2[0] + pixel3[0] + pixel4[0]) >> 2,
                                (pixel1[1] + pixel2[1] + pixel3[1] + pixel4[1]) >> 2,
                                (pixel1[2] + pixel2[2] + pixel3[2] + pixel4[2]) >> 2);
        }
    }

    // Création de la texture
    CImage image(out_width, out_height, textureBuffer);

    static unsigned int count = 0;
    texture->index = gTextureManager->loadTexture(":mdl_gold_src_texture:" + CString::fromNumber(count), image);
    ++count;

    // Libération de la mémoire
    delete[] textureBuffer;
}


/**
 * Libère la mémoire allouée pour le modèle.
 ******************************/

void CModelDataMDLGoldSrc::freeMemory()
{
    if (m_textureHeader != m_header)
        delete[] m_textureHeader;
    m_textureHeader = nullptr;

    delete[] m_header;
    m_header = nullptr;

    for (unsigned int i = 0; i < 32; ++i)
    {
        delete[] m_animationHeader[i];
        m_animationHeader[i] = nullptr;
    }
}


/**
 * Modifie la valeur d'un contrôleur.
 *
 * \param index Numéro du contrôleur.
 * \param value Valeur du contrôleur.
 * \return Valeur correcte du contrôleur.
 ******************************/

float CModelDataMDLGoldSrc::setController(unsigned int index, float value)
{
    if (m_header == nullptr)
    {
        return 0.0f;
    }

    unsigned int loop;
    TBoneController * bone_controller = reinterpret_cast<TBoneController *>(m_fileBuffer + m_header->bone_controller_index);

    for (loop = 0; loop < m_header->num_bone_controllers; ++loop, ++bone_controller)
    {
        if (bone_controller->index == index)
        {
            break;
        }
    }

    if (loop >= m_header->num_bone_controllers)
    {
        return value;
    }

    if (bone_controller->type & (TransitionXR | TransitionYR | TransitionZR))
    {
        if (bone_controller->end < bone_controller->start)
        {
            value = -value;
        }

        if (bone_controller->start + 359.0f >= bone_controller->end)
        {
            if (value > ((bone_controller->start + bone_controller->end) * 0.5f) + 180.0f)
            {
                value -= 360.0f;
            }

            if (value < ((bone_controller->start + bone_controller->end) * 0.5f) - 180.0f)
            {
                value += 360.0f;
            }
        }
        else
        {
            if (value > 360.0f)
            {
                value -= static_cast<int>(value / 360.0f) * 360.0f;
            }
            else if (value < 0.0f)
            {
                value += static_cast<int>((value / -360.0f) + 1.0f) * 360.0f;
            }
        }
    }

    int setting = static_cast<int>(255.0f * (value - bone_controller->start) / (bone_controller->end - bone_controller->start));

    if (setting < 0)
    {
        setting	= 0;
    }
    else if (setting > 255)
    {
        setting = 255;
    }

    m_controller[index] = static_cast<unsigned char>(setting);

    return (setting * (1.0f / 255.0f) * (bone_controller->end - bone_controller->start ) + bone_controller->start);
}


/**
 * Modifie la valeur d'un blending.
 *
 * \param blender  Numéro du blending à modifier (0 ou 1).
 * \param value    Valeur du blending.
 * \param sequence Numéro de la séquence.
 * \return Valeur correcte du blending.
 ******************************/

float CModelDataMDLGoldSrc::setBlending(int blender, float value, unsigned short sequence)
{
    if (m_header == nullptr)
        return 0.0f;

    TSeqDescription * seq_description = reinterpret_cast<TSeqDescription *>(m_fileBuffer + m_header->seq_index) + sequence;

    if (seq_description->blend_type[blender] == 0)
        return value;

    if (seq_description->blend_type[blender] & (TransitionXR | TransitionYR | TransitionZR))
    {
        if (seq_description->blend_end[blender] < seq_description->blend_start[blender])
        {
            value = -value;
        }

        if (seq_description->blend_start[blender] + 359.0f >= seq_description->blend_end[blender])
        {
            if (value > ((seq_description->blend_start[blender] + seq_description->blend_end[blender]) * 0.5f ) + 180.0f)
            {
                value = value - 360.0f;
            }

            if (value < ((seq_description->blend_start[blender] + seq_description->blend_end[blender]) * 0.5f ) - 180.0f)
            {
                value = value + 360.0f;
            }
        }
    }

    int setting = static_cast<int>( 255.0f * (value - seq_description->blend_start[blender]) / (seq_description->blend_end[blender] - seq_description->blend_start[blender]));

    if (setting < 0)
        setting	= 0;
    else if (setting > 255)
        setting = 255;

    m_blending[blender] = static_cast<unsigned char>(setting);

    return ( setting * (1.0f / 255.0f) * (seq_description->blend_end[blender] - seq_description->blend_start[blender]) + seq_description->blend_start[blender] );
}


/**
 * Convertit un quaternion en matrice.
 *
 * \todo Utiliser une fonction de glm.
 *
 * \param quaternion Quaternion à convertir.
 * \param matrix     Résultat.
 ******************************/

void quaternionMatrix(const glm::quat& quaternion, float (*matrix)[4])
{
    // A => x, B => y, C => z, D => w

    matrix[0][0] = 1.0f - 2.0f * ( quaternion.y * quaternion.y + quaternion.z * quaternion.z );
    matrix[1][0] =        2.0f * ( quaternion.x * quaternion.y + quaternion.w * quaternion.z );
    matrix[2][0] =        2.0f * ( quaternion.x * quaternion.z - quaternion.w * quaternion.y );

    matrix[0][1] =        2.0f * ( quaternion.x * quaternion.y - quaternion.w * quaternion.z );
    matrix[1][1] = 1.0f - 2.0f * ( quaternion.x * quaternion.x + quaternion.z * quaternion.z );
    matrix[2][1] =        2.0f * ( quaternion.y * quaternion.z + quaternion.w * quaternion.x );

    matrix[0][2] =        2.0f * ( quaternion.x * quaternion.z + quaternion.w * quaternion.y );
    matrix[1][2] =        2.0f * ( quaternion.y * quaternion.z - quaternion.w * quaternion.x );
    matrix[2][2] = 1.0f - 2.0f * ( quaternion.x * quaternion.x + quaternion.y * quaternion.y );
}


/**
 * ???
 *
 * \param in1  ?
 * \param in2  ?
 * \param time ?
 * \return Quaternion.
 ******************************/

glm::quat quaternionSlerp(const glm::quat& in1, const glm::quat& in2, float time)
{
    glm::quat in2_cpy = in2;
    glm::quat out;

    // Decide if one of the quaternions is backwards
    float value1 = 0.0f;
    float value2 = 0.0f;

    for (int i = 0; i < 4; ++i)
    {
        value1 += (in1[i] - in2_cpy[i]) * (in1[i] - in2_cpy[i]);
        value2 += (in1[i] + in2_cpy[i]) * (in1[i] + in2_cpy[i]);
    }

    if (value1 > value2)
    {
        for (int i = 0; i < 4; ++i)
        {
            in2_cpy[i] = -in2_cpy[i];
        }
    }

    float cos_angle = (in1.x * in2_cpy.x + in1.y * in2_cpy.y + in1.z * in2_cpy.z + in1.w * in2_cpy.w); //VectorDot ( in1 , in2_cpy );

    float slerp1;
    float slerp2;

    if ((1.0f + cos_angle) > std::numeric_limits<float>::epsilon())
    {
        if ((1.0f - cos_angle) > std::numeric_limits<float>::epsilon())
        {
            float omega = std::acos(cos_angle);
            float sinomega = 1.0f / std::sin(omega);

            slerp1 = std::sin((1.0f - time) * omega) * sinomega;
            slerp2 = std::sin(time * omega) * sinomega;
        }
        else
        {
            slerp1 = 1.0f - time;
            slerp2 = time;
        }

        for (int i = 0; i < 4; ++i)
        {
            out[i] = slerp1 * in1[i] + slerp2 * in2_cpy[i];
        }
    }
    else
    {
        out[0] = -in1[1];
        out[1] = in1[0];
        out[2] = -in1[3];
        out[3] = in1[2];

        slerp1 = std::sin((1.0f - time) * 0.5f * Pi);
        slerp2 = std::sin(time * 0.5f * Pi);

        for (int i = 0; i < 3; ++i)
        {
            out[i] = slerp1 * in1[i] + slerp2 * out[i];
        }
    }

    return out;
}


/**
 * ?
 *
 * \param in1 ?
 * \param in2 ?
 * \param out Résultat.
 ******************************/

void concatTransforms(const float in1[3][4], const float in2[3][4], float out[3][4])
{
    out[0][0] = in1[0][0] * in2[0][0] + in1[0][1] * in2[1][0] + in1[0][2] * in2[2][0];
    out[0][1] = in1[0][0] * in2[0][1] + in1[0][1] * in2[1][1] + in1[0][2] * in2[2][1];
    out[0][2] = in1[0][0] * in2[0][2] + in1[0][1] * in2[1][2] + in1[0][2] * in2[2][2];
    out[0][3] = in1[0][0] * in2[0][3] + in1[0][1] * in2[1][3] + in1[0][2] * in2[2][3] + in1[0][3];

    out[1][0] = in1[1][0] * in2[0][0] + in1[1][1] * in2[1][0] + in1[1][2] * in2[2][0];
    out[1][1] = in1[1][0] * in2[0][1] + in1[1][1] * in2[1][1] + in1[1][2] * in2[2][1];
    out[1][2] = in1[1][0] * in2[0][2] + in1[1][1] * in2[1][2] + in1[1][2] * in2[2][2];
    out[1][3] = in1[1][0] * in2[0][3] + in1[1][1] * in2[1][3] + in1[1][2] * in2[2][3] + in1[1][3];

    out[2][0] = in1[2][0] * in2[0][0] + in1[2][1] * in2[1][0] + in1[2][2] * in2[2][0];
    out[2][1] = in1[2][0] * in2[0][1] + in1[2][1] * in2[1][1] + in1[2][2] * in2[2][1];
    out[2][2] = in1[2][0] * in2[0][2] + in1[2][1] * in2[1][2] + in1[2][2] * in2[2][2];
    out[2][3] = in1[2][0] * in2[0][3] + in1[2][1] * in2[1][3] + in1[2][2] * in2[2][3] + in1[2][3];
}


/**
 * Transforme un vecteur avec une matrice de transformation.
 *
 * \param in1 Vecteur à transformer.
 * \param in2 Inverse de la matrice de transformation.
 * \param out Vecteur transformé.
 *
 * \todo Utiliser les classes CMatrix4 et CVector4.
 ******************************/

void vectorIRotate(const glm::vec3& in1, const float in2[3][4], glm::vec3& out)
{
    out.x = in1.x * in2[0][0] + in1.y * in2[1][0] + in1.z * in2[2][0];
    out.y = in1.x * in2[0][1] + in1.y * in2[1][1] + in1.z * in2[2][1];
    out.z = in1.x * in2[0][2] + in1.y * in2[1][2] + in1.z * in2[2][2];
}


/**
 * Multiplie un vecteur par une matrice de transformation.
 *
 * \todo Utiliser les objets du module Maths.
 *
 * \param in1 Vecteur à transformer.
 * \param in2 Matrice de transformation.
 * \param out Résultat de l'opération.
 ******************************/

void vectorTransform(const glm::vec3& in1, const float in2[3][4], glm::vec3& out)
{
    // VectorDot => glm::dot
    out.x = glm::dot(in1, glm::vec3(in2[0][0], in2[0][1], in2[0][2])) + in2[0][3];
    out.y = glm::dot(in1, glm::vec3(in2[1][0], in2[1][1], in2[1][2])) + in2[1][3];
    out.z = glm::dot(in1, glm::vec3(in2[2][0], in2[2][1], in2[2][2])) + in2[2][3];
}

} // Namespace TE
