/*
Copyright (C) 2008-2023 Teddy Michel

This file is part of TEngine2.

TEngine2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glm/gtc/matrix_transform.hpp>
#include <cassert>

#include "CActivityGame.hpp"
#include "CActivityMenu.hpp"
#include "CMapLoaderBSPGoldSrc.hpp"

#include "Game/CMap.hpp"
#include "Game/CMapLoader.hpp"
#include "Entities/CMapEntity.hpp"
#include "Entities/CPlayer.hpp"
#include "Entities/CWeaponLaser.hpp" // DEBUG
#include "Entities/CWeaponSphere.hpp" // DEBUG
#include "Entities/CScreen.hpp" // DEBUG
#include "Entities/CSpotLight.hpp" // DEBUG
#include "Entities/CCameraFreeFly.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Graphic/CRenderParams.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CGuiLabel.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CBuffer.hpp" // TODO: pourquoi on doit include ce fichier ici ?????


namespace TE
{

/**
 * Constructeur par d�faut.
 ******************************/

CActivityGame::CActivityGame() :
    IActivity          {},
    m_map              { nullptr },
    m_camera           { nullptr },
    m_cameraFreeFly    { nullptr },
    m_player           { nullptr },
    m_activityMenu     { nullptr },
    m_gui              {},
    m_labelLife        { std::make_shared<CGuiLabel>() },
    m_labelFPS         { std::make_shared<CGuiLabel>() },
    m_labelInteraction { std::make_shared<CGuiLabel>() }
{
    m_labelLife->setColor(CColor::Blue);
    m_labelLife->setFont(gFontManager->getFontId("arial"));
    m_labelLife->setPosition(50, gApplication->getHeight() - 50 - 30);
    m_labelLife->setSize(60, 35);
    m_labelLife->setFontSize(24);
    m_gui.addObject(m_labelLife);

    m_labelFPS->setColor(CColor::Orange);
    m_labelFPS->setFont(gFontManager->getFontId("arial"));
    m_labelFPS->setPosition(30, 30);
    m_labelFPS->setSize(150, 40);
    m_labelFPS->setFontSize(24);
    m_gui.addObject(m_labelFPS);

    m_labelInteraction->setColor(CColor::White);
    m_labelInteraction->setFont(gFontManager->getFontId("arial"));
    m_labelInteraction->setPosition(200, 30);
    m_labelInteraction->setSize(600, 40);
    m_labelInteraction->setFontSize(20);
    m_gui.addObject(m_labelInteraction);

    // Load map
    gApplication->log("Load map.");
    //auto time_load = getElapsedTime();

    CMapLoaderBSPGoldSrc mapLoader;
    m_map = mapLoader.loadFromFile("../../../Projects/Half-Life/maps/c1a1b.bsp");

    //gApplication->log(CString("Map loaded in %1 milliseconds.").arg(getElapsedTime() - time_load));

    if (m_map != nullptr)
    {
        // Player
        gApplication->log("Create player entity", ILogger::Debug);
        m_player = new CPlayer{ m_map, "player" };
        m_camera = m_player->getCamera();
        gGameApplication->setReceiver(m_player);

        // Weapon
        gApplication->log("Create weapon entity for the player.");
        //auto weapon = new CWeaponLaser{ m_map, "weapon_laser" };
        auto weapon = new CWeaponSphere{ m_map, "weapon_sphere" };
        m_player->addWeapon(weapon);

        gPhysicEngine->setCurrentScene(m_map->getScene());

        // TODO: add player to map
        //m_player->sendToMap(m_map);
        m_map->addPlayer(m_player);

/*
        // DEBUG: light
        CSpotLight * light = new CSpotLight{ m_player->getCamera(), "light" };
        light->setColor(CColor(200, 200, 0));
        light->setAngle(15.0f);
        light->translate(glm::vec3{ 35.0f, 0.0f, 0.0f });
        m_map->addLight(light);
*/

        // DEBUG: camera
        m_cameraFreeFly = new CCameraFreeFly{ m_map->getMapEntity(), "camera_free_fly" };
        //m_cameraFreeFly->setDirection(m_camera->getDirection());
        m_cameraFreeFly->setWorldMatrix(m_player->getWorldMatrix());
        gGameApplication->setReceiver(m_cameraFreeFly);
        m_camera = m_cameraFreeFly;


        //showMouseCursor(false);
        //m_gameActive = true;
        //m_receiver->setEventsEnable(m_gameActive);

        gGameApplication->showMouseCursor(true);
        //m_gameActive = false;
        //m_receiver->setEventsEnable(m_gameActive);


        // DEBUG: utilisation d'une cam�ra fixe de la map comme cam�ra par d�faut
        /*
        for (auto& child : m_map->getChildren())
        {
            auto camera = dynamic_cast<CCamera *>(child);
            if (camera != nullptr)
            {
                m_camera = camera;
                break;
            }
        }
        */

        m_map->getMapEntity()->initEntities(); // TODO: move to CMapLoader?
    }
}


/**
 * M�thode appell�e � chaque frame.
 *
 * \param frameTime Dur�e de la frame en secondes.
 ******************************/

void CActivityGame::frame(float frameTime)
{
    if (m_map != nullptr)
    {
        m_map->update(frameTime);
    }

    gPhysicEngine->update(frameTime);

    if (m_player != nullptr)
    {
        m_labelLife->setText(CString::fromFloat(m_player->getLife(), 0));

        CPlayer::TInteraction interaction;
        if (m_player->getInteraction(interaction))
        {
            m_labelInteraction->setText(CString("[%1] %2").arg(GetKeyName(interaction.key)).arg(interaction.message));
        }
        else
        {
            m_labelInteraction->setText(CString{});
        }
    }

    m_labelFPS->setText(CString("FPS: %1").arg(gGameApplication->getFPS()));
    m_gui.update(frameTime);

    gRenderer->startFrame();

    // Affichage de la map
    if (m_map != nullptr)
    {
        m_map->renderMap(m_camera);
    }

    // Affichage de l'interface utilisateur
    m_gui.draw();

    gRenderer->endFrame();
}


void CActivityGame::onEvent(const CKeyboardEvent& event)
{
    if (event.type == KeyPressed)
    {
        if (event.code == TKey::Key_Escape)
        {
            gGameApplication->setActivity(m_activityMenu);
        }
        else if (event.code == TKey::Key_N)
        {
            if (m_camera == m_cameraFreeFly)
            {
                gGameApplication->setReceiver(m_player);
                m_camera = m_player->getCamera();
                m_cameraFreeFly->setEventsEnable(false);
                m_player->setEventsEnable(true);
            }
            else
            {
                gGameApplication->setReceiver(m_cameraFreeFly);
                m_camera = m_cameraFreeFly;
                m_cameraFreeFly->setEventsEnable(true);
                m_player->setEventsEnable(false);
            }
        }
    }
}

} // Namespace TE
